<?php
/**
* Standard Index Page
*/

// don't show this page to logged in users

//make this a secure page
$secure = false;

//include basic stuff
include ("shared/startPage.php");

if (isLoggedIn())
{
    headerLocation($rootUrl);
}

$pageFiles->addFunc('content');
$pageFiles->addFunc('email');


$pageFiles->includePhp();


//specify page content
$title = "Forgotten Password?";		//the <title> tag and <h1> page title
$content = "forgotPassword.php"; 
if (buttonClicked("submit")) {
	$to =  getStrFromRequest("email");
	$passwd = new clsUserPassword($homePath, $password_arr);
	$msg = $passwd->emailPassword($to, $con, $adminMail);
	if ($msg == PASSWORD_SENT) $content = "";
	$messages[] = $msg;
}
 
//include layout.
include ("layout.php");

?>