<?php
/**
* This defines the messages used in the site so that Mark can 
* easily change the wording. Please comment where/when the message is used.
*/

//General Messages
Define("PERMISSIONS_ERROR","Sorry, you do not have permission to do that");
Define("SAVED","Changes Saved");
Define("DELETED","Record Deleted");
Define("ERROR_SAVED","Error saving changes");
Define("ERROR_DELETE","Error Deleting Record");


//Logon Messages (securePage.php)
Define("USER_NOT_FOUND","Sorry, the username and password could not be found on the system.<br> If you have forgotten your password, please refer to the link below the login button, thank you.");
Define("LOGOUT","Thanks for using the system.");
Define("AUTO_LOGOUT","You have been automatically logged out due to inactivity.  Please login again.");
Define("CANT_REMEMBER","Can't seem to remember your details correctly?!");

//new user reg (users.php) 
Define("USERNAME_IN_USE","We are sorry but the email is already being used, please choose another.");

//forgotten password and change password
Define("PASSWORD_SENT","A new password has been sent to this user's email address.");
Define("PASSWORD_MESSAGE","Your password is ");
Define("PASSWORD_CHANGED","Your password has been changed.");
Define("PASSWORD_SHORT","The new password must between 8-16 characters and contain at least 5 letters (at least 1 uppercase and 1 lowercase), at least 1 number and may contain 2 symbols from #_-!=[]*~");
Define("PASSWORD_WRONG","The old password is incorrect.");
Define("PASSWORD_NOT_MATCHED","The two passwords must match.");


//Hacking/ unauthorized attempts
Define("NOT_EDIT_USER","You may not edit this user.");
define('EXCEEDED_MAX_LOGINS', 'You have attempted to log in too many times');

//names in use  
Define("TASKLISTNAME_IN_USE","We are sorry but the task list name is already being used, please choose another.");
Define("ACCOUNTNAME_IN_USE","We are sorry but the account name is already being used, please choose another.");
Define("CATEGORYNAME_IN_USE_IN_USE","We are sorry but the category name is already being used, please choose another.");
define("JOBROLETITLE_IN_USE", "We are sorry but that Job Role title is already being used, please choose another.");
define('COURSETITLE_IN_USE', 'We are sorry but that Course Title is already being used, please choose another.');

//activating account and tasks
Define("TASKS_SUPPLIED_WITH","Tasks have been added with ");
Define("ERRORS"," errors.");
Define("TASKS_SUPPLIED","Tasks have been added");
Define("LICENCE_ACTIVE","The licence is now active");
Define("HOLIDAY_END_ERROR","The holiday end date is before the start date!");
Define("UPDATED_ACCOUNT_TASKS","Updated account tasks");

//account admin section
Define("NONE_SELECTED","No tasks were selected.");

//information boxes
Define("MULTISELECT_INFO","Select multiple tasks, using the check boxes below, and then apply an 'Action date', 'Assigned to' user or state to your selection by ticking the appropriate check boxes on the left.");
Define("MULTISELECT_TITLE","Multiple Task Selection.");
Define("SELECT_ALL","Select All Tasks");

Define("LICENCE_FORM_INFO","Enter a start date for the account licence. This can be edited until tasks are supplied to the licence.");
Define("LICENCE_FORM_TITLE","Account Licence");

Define("ACCOUNTS_FORM_INFO","Edit the account details here.");
Define("ACCOUNTS_FORM_TITLE","Account Details");

Define("HOLIDAY_FORM_INFO","Add or edit a holiday period for this licence. You may add as many holiday periods as you wish.");
Define("HOLIDAY_FORM_TITLE","Licence Holiday Period");

Define("USERS_FORM_INFO","Add or edit user details here.");
Define("USERS_FORM_TITLE","User Details");

Define("CATEGORY_FORM_INFO","Add or edit a task category name here.");
Define("CATEGORY_FORM_TITLE","Edit Task Category Name");

Define("TASKADMIN_FORM_INFO","Add or edit task details here. If the required category is not listed, you must add it first using the 'Task Categories' form.");
Define("TASKADMIN_FORM_TITLE","Edit Task Details");

Define("TASK_FORM_INFO","Add or edit task details here. The 'Action date' is the date when you wish the task to be completed, and the 'Assigned to' user is who you wish to be responsible for completing the task. <br/><br/>Once a task has <strong>state: COMPLETED</strong> it can't be edited any more. If you change the <strong>state</strong> back to <strong>ACTIVE</strong> or <strong>INACTIVE</strong> the task may be edited but must then be completed again by the assigned user.");
Define("TASK_FORM_TITLE","Edit Task Details");

Define("TASKLIST_FORM_INFO","Add or edit the task list details here.");
Define("TASKLIST_FORM_TITLE","Task List Details");

Define("FORGOT_FORM_INFO","Please enter your email below and we will send you your password.");
Define("FORGOT_FORM_TITLE","Forgotten your password");

Define("PASSWORD_FORM_INFO","Enter your new password below.");
Define("PASSWORD_FORM_TITLE","Change your password");

Define("MYTASK_FORM_INFO","Edit the task notes or tick the 'Task Completed' box once you have completed the task.  Do not forget to click 'save' to save this information to the database");
Define("MYTASK_FORM_TITLE","Task Information");


Define("UPLOAD_FORM_INFO","Upload a file from your computer to the server. <strong>BROWSE</strong> to select a new file from your computer.");
Define("UPLOAD_FORM_TITLE","Upload a file");
Define("UPLOAD_FORM_INFO_EDIT", "Edit");

Define("LINKS_FORM_INFO","Please include the <strong>http:// </strong> bit in the url field. ");
Define("LINKS_FORM_TITLE","Editing a Link");

Define("MESSAGE_FORM_INFO","Use this form to send an email to HandSam");
Define("MESSAGE_FORM_TITLE","Email HandSaM Form");

define("JOBROLES_FORM_TITLE", "Edit the Job Role's title");
define("JOBROLES_FORM_INFO", "Add or edit a Job Role");

// upload messages
Define("FILES_ATTACHED","File(s) successfully attached");
Define("FILES_UNATTACHED","File(s) successfully unattached");
Define("ERROR_FILE_DELETE","Error deleting file from server");
Define("ERROR_FILE_UPLOAD","Error uploading file to server");
Define("FILE_ATTACHED_TO_TASK","This file couldn't be deleted because it is still attached to some tasks");
Define("FILE_ATTACHED_TO_PAGE","This file couldn't be deleted because it is still attached to a page");
Define("LINK_ATTACHED_TO_TASK","This link couldn't be deleted because it is still attached to some tasks");
Define("LINK_ATTACHED_TO_PAGE","This link couldn't be deleted because it is still attached to a page");

define('LINK_ATTACHED', "The link was successfully attached.");
define('LINK_UNATTACHED', "The link was removed.");

// email form
Define("EMAIL_ERROR","There was an error whilst trying to send your message. Please try again or contact HandSaM by another means.");
Define("EMAIL_SENT","Your message has been sent and will be dealt with as soon as possible.");

// FORM NOTES INFO BOXES
Define("STATIC_CONTENT_FORM_INFO","Edit a content item.  Do not forget to click 'save' to save this information to the database");
Define("STATIC_CONTENT_FORM_TITLE","Edit Content Item"); 
 
//Email alerts
Define("ALERT_LATE_TASK_TITLE","Late Handsam Task");

/* LOtC APP CONSTANTS */
Define("PLAN_FORM_INFO","Add or edit a plan here.  <strong>Please remember to save your changes.</strong>");
Define("PLAN_FORM_TITLE","Plan Details");

Define("FORM_FORM_INFO","Add or edit a form here.  <strong>Please remember to save your changes.</strong>");
Define("FORM_FORM_TITLE","Form Details");

Define("FIELD_FORM_INFO","Add or edit a field here.  <strong>Please remember to save your changes.</strong>");
Define("FIELD_FORM_TITLE","Field Details");

Define("PLAN_IN_USE","We are sorry but the plan title is already being used, please choose another.");
Define("FORM_IN_USE","We are sorry but the form name is already being used in this plan, please choose another one.");
Define("FIELD_IN_USE","We are sorry but the field name is already being used in this form, please choose another one.");

Define("APPROVED","Stage approved");
Define("DECLINED","Stage declined");

/*training log*/
Define("EDIT_STATE","Update to Edit State");
Define("COMPLETED_STATE","Update to Completed State");
