<?php
/** Site Name */
$siteName="HandSaM";

$rootUrl = "http://www.handsam.org.uk";
//windows
set_include_path(".;..");

//linux
//set_include_path(".:..:$rootPath");

/** some file locations */
$logDir="/home/handsam/logs";

/** Database connection */
/*
$dbhost="BARBARELLA\SQLEXPRESS";
$dbname="handsam";
$dbuser="handsam";
$password="password";
*/
$dbhost="localhost";
$dbname="handsam";
$dbuser="handsam";
$password="v509jfm";

//expiry leeway/execution days of grace
$daysOfGrace = 28;

/** Email address for Administrative Things */
$adminMail="it@handsam.co.uk";
$alertsMail = "alerts@handsam.co.uk";

/**  Show Debug Information? */
$debug = false;

/** Should we actually send emails? */
$sendMail = true;

//this only has effect if using PHP built in error handling (i.e. $debug = false)
//error_reporting(E_ERROR | E_WARNING | E_PARSE | E_NOTICE); //for testing
//error_reporting(2047);

/** How to format Dates */
$dateFormat = "%d %b %y";
$dateFormatSmall = "%d %b";
$datetimeFormat = "%d/%m/%Y - %H:%M";

/** Where to backup database */
$backupPath = "/hsphere/local/home/firefly1/firefly1.co.uk/Handsam/src/backup";

/** Max Login Attempts before Disable */
$max_login_attempts = 10;

/** months array*/

$months = array(1 => "January", 2 => "February", 3 => "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December", 13 => "All Months");

//upload docs and links categories
$docCategory = array("QGP" => "QGP and other Guidance", "LAW" => "Law &amp; HSE", "USERGUIDE" => "HandSaM User Guide");
$userdocs_fp = "/home/handsam/handsam_docs/userDocs/";
$admindocs_fp = "/home/handsam/handsam_docs/adminDocs/";
$handsamdocs_fp = "/home/handsam/handsam_docs/handsamDocs/";
$accountLotcDocs_fp = "/home/handsam/handsam_docs/aLotcDocs/";
$accountTrainingDocs_fp = "$homePath/handsam_docs/aTrainingDocs/";
$fckWidth = 500;
?>