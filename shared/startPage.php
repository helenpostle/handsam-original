<?php
//this file must be included in every page first.
//it includes all the general stuff, configures the site, creates a connection etc etc.
require("config.php");
require("messages.php");
require("$rootPath/handsam_core/library/funcError/funcError.php");
//require("library/funcGeneral.php");

require("$rootPath/handsam_core/library/funcDb/funcDb.mySQL.php");
require("$rootPath/handsam_core/library/funcError/funcDebugLog.php");

//include("shared/hasCredit.php");#######not in use anymore
//initialise some variables
$errorFlag = ""; 
$message = "";
//any errors which occur to be handled by this function
if (!$debug) {
	set_error_handler("myErrorHandler");
	//register_shutdown_function('handleShutdown');
}

//make a connection to the database
$con = getConnection($dbhost,$dbname,$dbuser,$password);

//load module startpage if we are within the modul directory. 
//This will load any module config and messages needed
if (isset($ajaxModule)) {
	$this_mod_dir = $ajaxModule;
} else {
	$this_mod_dir = basename(dirname($_SERVER['PHP_SELF']));
}

if (!in_array($this_mod_dir, $installed_modules)) {
	if ($this_mod_dir == "web"
		|| $this_mod_dir == "user"
		|| $this_mod_dir == 'trainingresources'
		|| $this_mod_dir == "tools"
        || $this_mod_dir == "reports"
        || $this_mod_dir == "ajax"
		|| $this_mod_dir == "admin"
		) $this_mod_dir = "";
	
	//if we're not in a module, then we're basically in the core
	if ($this_mod_dir != "handsam_core" && trim($this_mod_dir) != "") {
		header("Location: $rootUrl/index.php?mod");
		die();
	}
}

/*
//are we in handsam core?
if (basename(dirname($_SERVER['PHP_SELF'])) == "handsam_core") {
	//so content file path from handsam_core/content
	$core_folder = "handsam_core";
} else {
	//so content file path from /content
	$core_folder = "";
}
*/

##this class will load all files for this page
require_once("handsam_core/library/clsPage/clsPageLoadFiles.php");
//initiate class for this page
$pageFiles = new clsLoadPageFiles($rootPath, $rootUrl, $jQuery, $this_mod_dir);
//$pageFiles = new clsLoadPageFiles($rootPath, $rootUrl, $jQuery, $core_folder);
//add a core func for this page

//these files need adding now so they can be used in secure page
require_once("$rootPath/handsam_core/library/htmlpurifier-4.3.0-standalone/HTMLPurifier.standalone.php");
include("$rootPath/shared/htmlpurifier.config.php");
require_once("$rootPath/handsam_core/library/funcPage/funcGeneral.php");

require_once("$rootPath/handsam_core/library/security/clsXssSecurity.php");
require_once("$rootPath/handsam_core/library/funcPage/funcRequest.php");

// hooks functionality
require_once("$rootPath/handsam_core/library/funcHooks/funcHooks.php");



$allow_html_purifier = new HTMLPurifier($html_purifier_config);
$nohtml_page_purifier = new HTMLPurifier($nohtml_purifier_config);
$_SERVER["PHP_SELF"] = cleanUrl($nohtml_page_purifier->purify($_SERVER["PHP_SELF"]));
$get_req = new requestGet();
$post_req = new requestPost();
$cookie_req = new requestCookie();	

if (isset($_SERVER['HTTP_REFERER'])) $_SERVER['HTTP_REFERER'] = cleanUrl($nohtml_page_purifier->purify($_SERVER["HTTP_REFERER"]));
	
/*in the future maybe add these before secure page so we can use them there too*/
/*
require_once("$rootPath/handsam_core/library/clsDb/clsDbAction.php");
require_once("$rootPath/handsam_core/library/clsDb/clsDbUpdate.php");
require_once("$rootPath/handsam_core/library/clsDb/clsDbInsert.php");
require_once("$rootPath/handsam_core/library/clsDb/clsDbDelete.php");
*/

$pageFiles->addFunc('display');

require_once("$rootPath/handsam_core/library/security/securePage.php");
require_once("$rootPath/handsam_core/library/security/clsCsrfSecurity.php");

//add more core funcs like this here
if (isLoggedIn() && $autologout && $debug == false && $core_conf['remember_me'] === false && !isset($_COOKIE['cooktoken'])) 
{
  $pageFiles->addFunc('auto_logout');
}

$pageFiles->addFunc('database');
$pageFiles->addFunc('page');
$pageFiles->addFunc('form');

if ($this_mod_dir != "handsam_core" && $this_mod_dir != "") {
	require_once ("$rootPath/modules/$this_mod_dir/shared/modStart.php");
	
} else {
	require_once ("$rootPath/handsam_core/shared/coreStart.php");

	if (dirname($_SERVER['PHP_SELF']) == "$rootPath/index.php" && isLoggedIn()) {
		//install all config pages because index page displays all modules
		foreach ($installed_modules as $mod) {
			require_once ("$rootPath/modules/$mod/shared/modStart.php");
		}
	}
}

if (isLoggedIn()) $pageFiles->autoLoadJquery("$rootUrl/handsam_core/library/jquery/plugins/jquery.csrf.js");


if (isset($ajaxModule) && $ajaxModule != '' && isLoggedIn()) {
	//require_once("$rootPath/handsam_core/library/security/csrf_ajax.php");
}