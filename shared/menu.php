<?php
$web_order = "";
if (!isLoggedIn()) {
	$clsMenu->addMenuItem("/index.php", "HOME", "", "");
	//$clsMenu->addMenuItem("/about.php", "ABOUT", "", "");
	//$clsMenu->addMenuItem("/resources.php", "HINTS &amp; RESOURCES", "", "");
} else {
	$web_order = $clsMenu->addMenuItem("/index.php", "HOME", "", $web_order);
	if (isAllowed("viewDashboard") || isAllowed("viewAccountDashboard")) {
		$clsMenu->addMenuItem("/index.php?homepage=1", "HOME", "", $web_order);
		if (isAllowed("viewDashboard")) $clsMenu->addMenuItem("/handsam_core/dashboard.php", "SYSTEM DASHBOARD", "", $web_order);
		if (isAllowed("viewAccountDashboard")) $clsMenu->addMenuItem("/handsam_core/account_dashboard.php", "ACCOUNT DASHBOARD", "", $web_order);
		if ($clsUS->accountid == 1) $clsMenu->addMenuItem("/modules/admin/menu.php", "ADMIN SECTION", "", $web_order);
    if ((isAllowed("viewNewsAuditSA") && $core_conf['unread_news_audit_SA']) || (isAllowed("viewNewsAudit") && $core_conf['unread_news_audit_account_admin']))
    {
      $clsMenu->addMenuItem("/handsam_core/news_audit.php", "MESSAGES AUDIT", "", $web_order);
      $clsMenu->addMenuItem("/news.php", "MESSAGES", "", $web_order);
    }
		if (isAllowed("superAdminModules"))$clsMenu->addMenuItem("/handsam_core/alerts.php", "ALERTS", "", $web_order);
	}
    
}
if (isset($subpageof)) $clsMenu->setThisPage($subpageof);

?>