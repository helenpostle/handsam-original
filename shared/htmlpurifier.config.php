<?php
$html_purifier_config = HTMLPurifier_Config::createDefault();
$html_purifier_config->set('Core.Encoding', 'utf-8');
$html_purifier_config->set('Attr.EnableID', true);
$html_purifier_config->set('Attr.AllowedFrameTargets', array('_blank'));
$html_purifier_config->set('HTML.Trusted', true);

$nohtml_purifier_config = HTMLPurifier_Config::createDefault();
$nohtml_purifier_config->set('Core.Encoding', 'utf-8');
$nohtml_purifier_config->set('Attr.EnableID', true);
$nohtml_purifier_config->set('Attr.AllowedFrameTargets', array('_blank'));
$nohtml_purifier_config->set('HTML.Trusted', false);




?>