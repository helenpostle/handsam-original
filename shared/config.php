<?php
//$restrict_ip = array('78.33.75.169', '46.236.8.105', '212.159.112.101');

$version = "2.16.2";

/**css append - appends this querystring to the end of all css files. Change to refresh css**/
$css_version = "1.0";

$homePath = dirname(dirname(dirname(__FILE__)));

//need to work out if we're on http or https:
$protocol = "http";
if (isset($_SERVER['HTTPS']) && strtolower($_SERVER['HTTPS'] != 'off' && $_SERVER['HTTPS']) != '') $protocol = "https";

$rootPath = "$homePath/www";

$installation = "original";

/** some file locations */
$logDir="$homePath/logs";
$logPath = $logDir;
$logDaysToKeep = 30;

//if php 5 set time zone for strto time functions
if (phpversion() > '5.0.0') date_default_timezone_set('Europe/London');

//expiry leeway/execution days of grace
$daysOfGrace = 28;

/**  Show Debug Information? */
$debug = false;

/** Should we actually send emails? */
$sendMail = true;

/** array of arror levels for error reporting **/
$errorlevels = array(
        2048 => 'E_STRICT',
        2047 => 'E_ALL',
        1024 => 'E_USER_NOTICE',
        512 => 'E_USER_WARNING',
        256 => 'E_USER_ERROR',
        128 => 'E_COMPILE_WARNING',
        64 => 'E_COMPILE_ERROR',
        32 => 'E_CORE_WARNING',
        16 => 'E_CORE_ERROR',
        8 => 'E_NOTICE',
        4 => 'E_PARSE',
        2 => 'E_WARNING',
        1 => 'E_ERROR');
		
/** How to format Dates */
$dateFormat = "%d/%m/%Y";
$dateFormatSmall = "%d %b";
$datetimeFormat = "%d/%m/%Y - %H:%M";
$lotcDateFormat = "%a, %d %b %Y";
$lotcDateTimeFormat = "%a, %d %b %Y %H:%M";


/** Where to backup database */
$backupPath = "$homePath/backup";

//how many days to keep db backups for
$backupDaysToKeep = 7;

//rotate logs config
$logDaysToKeep  = 7;
$logSizeToRotate = 1024;


/** Max Login Attempts before Disable */
$max_login_attempts = 10;

/** months array*/
$months = array(1 => "January", 2 => "February", 3 => "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December", 13 => "All Months");

//upload docs and links categories
$userdocs_fp = "$homePath/handsam_docs/userDocs/";
$admindocs_fp = "$homePath/handsam_docs/adminDocs/";
$handsamdocs_fp = "$homePath/handsam_docs/handsamDocs/";
$accountLotcDocs_fp = "$homePath/handsam_docs/aLotcDocs/";
$accountLotcUserDocs_fp = "$homePath/handsam_docs/aLotcUserDocs/";
$accountTrainingDocs_fp = "$homePath/handsam_docs/aTrainingDocs/";
$accountIncidentDocs_fp = "$homePath/handsam_docs/aIncidentDocs/";
$accountdocs_fp = "$homePath/handsam_docs/accountDocs/";


$fckWidth = 500;

$jquery = array();
$jquery_plugin = array();


//thumbs array describes width, height, folder name
$swf_thumbnails = array();
$swf_thumbnails['logo'] = array(150, 150, "$rootPath/account_uploads/account_logo");

//jquery filename version

$jQuery = "jquery1.7.2.js";

//array of containing home page content pulled form each installed module
$index_modules_content = array();

//email alerts config
$send_email_alerts = true;

//define some common variables
$menuPage = "";
$messages = array();

$sex_arr = array ("MALE" => "male", "FEMALE" => "female");

//declare this for HTMLpurifier to function
$html_purifier_config = "";

//password validation array
$password_arr = array(
	'minLength'      => 8,
	'maxLength'      => 16,
	'minNumbers'     => 1,
	'minLetters'     => 5,
	'minLowerCase'   => 1,
	'minUpperCase'   => 1,
	'minSymbols'     => 0,
	'maxSymbols'     => 2,
	'allowedSymbols' => array('#', '_', '-', '!', '[', ']', '=', '~', '*'),
);

//include instalation configs
include("$rootPath/installations/$installation/config/server.config.php");
include("$rootPath/installations/default/config/core.config.php");
include("$rootPath/installations/default/config/mods.config.php");
include("$rootPath/installations/$installation/config/core.config.php");
include("$rootPath/installations/$installation/config/mods.config.php");

// label constants
include("$rootPath/installations/$installation/labels/core.labels.php");
// user email constants
include("$rootPath/installations/$installation/templates/emails.php");

if ($server_os == "win") {
	//windows
	set_include_path(".;..;$rootPath");
} else {
//linux
	set_include_path(".:..:$rootPath");
}

//used in handsam_core/shared/menu.php
$menu_url = $_SERVER["PHP_SELF"];

?>
