<?php
//define modules available for this installation
$installed_modules = array();
$installed_modules[1] = "tasklist";
$installed_modules[2] = "lotc";
$installed_modules[3] = "accidents";
$installed_modules[4] = "training";
$installed_modules[5] = "qgp";
$installed_modules[6] = "admin";


//Functionality to tag incident log sites with a predefined tag from the database.
$accidents_conf['site_tags'] = true;
//The site MUST be tagged if this is true
$accidents_conf['site_tags_required'] = true;
