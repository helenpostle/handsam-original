<?php
/*installation config*/
/** Site Name */
$siteName="Handsam";
$installation_ref = "handsam";

//not in use yet
//$smtpServer = "semantise.com";
//$smtpPort = 25;


/** Email address for Administrative Things */
//admin mail sends password reminders
$adminMail="admin@handsam.org.uk";
//alertsmail sends email notifications
$alertsMail = "alerts@handsam.org.uk";
//logmail gets sent logs
$logMail = "it@handsam.co.uk";
/**  Show Debug Information? */

//upload document categories
$docCategory = array("QGP" => "QGP", "LAW" => "Law &amp; HSE", "USERGUIDE" => "HandSaM User Guide", "RISK_ASSESSMENT" => "Risk Assessment", "GUIDANCE" => "Other Guidance", "DOCUMENT" => "Documents");


$meta_title = "Handsam Health and Safety Management System";
$meta_keywords = "Handsam, Health and Safety, Health and Safety Management";
$meta_description = "Handsam Health and Safety Management System";


//set to true for autologout
$autologout = true;

//set this to log debugging in debugging table
$debug_logging = false;
$debug_logging_userids = array();
$debug_logging_accountids = array();


//account type arrays
$school_type = array("PRIMARY" => "Primary", "JUNIOR" => "Junior", "INFANTS" => "Infants", "PREP" => "Prep", "MIDDLE" => "Middle", "SECONDARY" => "Secondary", "SECONDARY SIXTH" => "Secondary with Sixth Form", "SIXTH FORM" => "Sixth Form College", "UNKNOWN" => "Unknown");
$school_status = array("COMMUNITY" => "Community", "VOLUNTARY CONTROLLED" => "Voluntary Controlled (VC)", "VOLUNTARY AIDED" => "Voluntary Aided (VA)" , "FOUNDATION" => "Foundation", "GRANT MAINTAINED" => "Grant Maintained", "ACADEMY" => "Academy", "INDEPENDENT" => "Independent", "UNKNOWN" => "Unknown");
$school_pupils = array("CO-ED" => "Co-educational", "BOYS" => "Boys", "GIRLS" => "Girls", "UNKNOWN" => "Unknown");
$school_cluster = array();


//online courses webapp connection
$online_course_mode = "";


//this is so we can have more than one logo for an installation - so we can group accounts together
$installation_logo_url = "$rootUrl/installations/$installation/images/handsam_logo.jpg";



// 2nd account manager field
$core_conf['acc_manager_2'] = FALSE;

// Job Roles functionality
$core_conf['jobroles'] = TRUE;

//$core_conf['crypt'] = "md5";

//$core_conf['email_alert_mode'] = "TEST";
$core_conf['email_alert_test_addr'] = "it@handsam.co.uk";
