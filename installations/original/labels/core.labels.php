<?php
/**
 * web/installations/AET/labels/core.labels.php
 *
 * Text labels for handsam_core
 */
define("ACCOUNTS_ACCOUNT_MANAGER", 'Account Manager');
define("ACCOUNTS_ACCOUNT_MANAGER_2", 'Regional DE');

define("ACCOUNTBILLING_DATE", 'Billing Dates');

define("ACCOUNT_TYPE_1", 'School Type');
define("ACCOUNT_TYPE_2", 'Account Status');
define("ACCOUNT_TYPE_3", 'School Pupils');
define("ACCOUNT_TYPE_4", 'Cluster');


// library
define('DOCLIB_MENU_LINK_ADMIN', 'Library Admin');
define('DOCLIB_MENU_LINK_DOCS', 'Library Documents');

// documents
define('DOCUMENT_MENU_LINK_ADMIN', 'Documents Admin');
define('DOCUMENT_MENU_LINK_DOCS', 'Documents');
define('DOCUMENT_MENU_LINK_CAT', 'Document Categories');
