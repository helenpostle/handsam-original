<?php

/*
 * Constants used in emails
 * 
 */

//newe email password
define("NEW_PASSWORD_SUBJECT", "Handsam System (ECH) Password");
define("NEW_PASSWORD_1", "Private & Confidential<br/>The web address for this system can be found at: - <a href=\"http://www.handsam.co.uk\">http://www.handsam.co.uk</a><br/> You have been set up as a user of the  Handsam System.<br/>");
define("NEW_PASSWORD_2", "<br/><br/>For security purposes, you are advised to change the password once you logon to the system for the first time.<br/><br/>Yours Sincerely<br/><br/>The Handsam Team");
define("NEW_PASSWORD_USERNAME", "<br/>Username: ");
define("NEW_PASSWORD_PASSWORD", "<br/>Password: ");
?>
