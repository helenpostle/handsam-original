<?php
  $cluster_qry="";
  if(isset($clsUS->account_clusters) && count($clsUS->account_clusters)>0 && isset($core_conf['account_cluster_SA_access']) && $core_conf['account_cluster_SA_access'])
  {
    $cluster_qry = " and account.cluster_id in (".implode(',',$clsUS->account_clusters).") ";
  }
  
  if(isset($params['school_cluster']) && $params['school_cluster']>0)
  {
    $cluster_qry = " and account.cluster_id = {$params['school_cluster']} ";
  }
  
	if ($report_id == 0) {
        // tab 1 - tasklist
        
        $tabs['tasklist'] = "Tasklist Reports";
        //set up rows and cols for the report table
        $cols = array();
        $rows = array();
        $cols['tasklist'][1] = array('account_status' => ACCOUNT_TYPE_2);
        $cols['tasklist'][2] = array('accountname' => 'Account Name');
        
        $sql = "select group_id, title from task_category_group order by group_id asc";
        $rs =  getRS($con, $sql);
        while ($rw = getRow($rs)) {
            if ($core_conf['dashboard_group_select']) {
                $group_options = "<option value =\"0\">All</option>";
                $sql_grp = "select * from taskcategory where state = 'ACTIVE' and group_id = ".$rw['group_id']." order by customtext asc";
                $rs_grp = getRS($con, $sql_grp);
                while ($row_grp = getRow($rs_grp)) {
                    $group_options .= "<option title=\"{$row_grp['customtext']}\" value =\"{$row_grp['categoryid']}\">{$row_grp['customtext']}</option>";
                }
                $cols['tasklist'][1][$rw['group_id']] = "<div class=\"group_txt\">".$rw['title']."</div><select class=\"{$rw['group_id']}\">$group_options</select>";
            } else {
                $cols['tasklist'][1][$rw['group_id']] = $rw['title'];
            }
            $cols['tasklist'][2][$rw['group_id']] = $rw['title'];
        }                
        $rows['tasklist'][1] = array(
            'ALL' => 'All'
        );
        
        //var_dump($params);
        //get school types for table rows
        $row_school_status = array_slice($school_status, 1);
        $accountname_qry1 = "";
        if ($params['school_pupils'] != 'ALL') {
            $qryRow = "select distinct school_status from account where state = 'ACTIVE' $cluster_qry";
            $rsRow = getRS($con, $qryRow);
            $row_school_status = array();
            while ($row = getRow($rsRow)) {
                if (array_key_exists($row['school_status'], $school_status)) {
                    $row_school_status[$row['school_status']] = $school_status[$row['school_status']];
                }
            }
            $accountname_qry1 = " and school_pupils = '{$params['school_pupils']}' ";
        }
        $accountname_qry2 = "";
        if ($params['school_type'] != 'ALL') {
            $accountname_qry2 = " and school_type = '{$params['school_type']}' ";
        }
        
        
        $rows['tasklist'][1] = array_merge($rows['tasklist'][1], $row_school_status);
 
        
        //now get the accountnames for the table rows
        $sql = "select accountid, accountname from account where state = 'ACTIVE' $accountname_qry1 $accountname_qry2 $cluster_qry order by accountname asc";
        $rs =  getRS($con, $sql);
        while ($rw = getRow($rs)) {
            $rows['tasklist'][2][$rw['accountid']] = $rw['accountname'];
        }

        //now initialise report with this table layout
        $dashboard_report = array();
        $report = array();
        
        for ($i=1; $i<6; $i++) {
            $dashboard_report[] = array('tab' => 'tasklist', 'report_id' => $i, 'params' => $params, 'installation' => $installation, 'rows' => $rows['tasklist'], 'cols' => $cols['tasklist']);
            $report[] = '/installations/'.$installation.'/reports/ajax/account_tasklist_overview.php';
        }
        
        
        //tab 2 - incidents
        //all rows and cols are the same here. We're just in a different tab
       
        $tabs['incidents'] = "Incident Reports";
        $rows['incidents'] = $rows['tasklist'];
        $cols['incidents'][1] = array('account_status' => ACCOUNT_TYPE_2, 'non-riddor' => 'Open Non-Riddor Incidents', 'riddor' => 'Open Riddor Incidents', 'all_incidents' => 'All Incidents');
        $cols['incidents'][2] = array('accountname' => 'Account Name', 'non-riddor' => 'Open Non-Riddor Incidents', 'riddor' => 'Open Riddor Incidents', 'all_incidents' => 'All Incidents');
        $dashboard_report[] = array('tab' => 'incidents', 'report_id' => $i, 'params' => $params, 'installation' => $installation, 'rows' => $rows['incidents'], 'cols' => $cols['incidents']);
        $report[] = '/installations/'.$installation.'/reports/ajax/testaccount_non-riddor_overview.php';        

        $dashboard_report[] = array('tab' => 'incidents', 'report_id' => $i, 'params' => $params, 'installation' => $installation, 'rows' => $rows['incidents'], 'cols' => $cols['incidents']);
        $report[] = '/installations/'.$installation.'/reports/ajax/testaccount_riddor_overview.php';        

        $dashboard_report[] = array('tab' => 'incidents', 'report_id' => $i, 'params' => $params, 'installation' => $installation, 'rows' => $rows['incidents'], 'cols' => $cols['incidents']);
        $report[] = '/installations/'.$installation.'/reports/ajax/testaccount_incident_overview.php';        


        //key tasks report
        if ($core_conf['key_task']) {
            //key tasks report cols
            $cols['tasklist'][1]['key_tasks'] = "Key Tasks";
            $cols['tasklist'][2]['key_tasks'] = "Key Tasks";
            $dashboard_report[] = array('tab' => 'tasklist', 'params' => $params, 'installation' => $installation, 'rows' => $rows['tasklist'], 'cols' => $cols['tasklist']);
            $report[] = '/installations/'.$installation.'/reports/ajax/account_key_tasks_overview.php';
        }        
        
        $pageFiles->addJsVar('dashboard_report', json_encode($dashboard_report));
        $pageFiles->addJsVar('report', json_encode($report));
        
		$content = "rep.default_dashboard_view.php";
		
		
		$pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.params.php");
		
	}
?>