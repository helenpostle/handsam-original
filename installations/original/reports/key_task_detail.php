<?php 
	if ($report_id > 0 && $core_conf['key_task']) {
        $tabs['tasklist_detail'] = "Key Tasks Detail";
        //set up rows and cols for the report table
        $cols = array();
        $rows = array();
        $cols['tasklist_detail'][1] = array('actiondate' => 'Date', 'task_cat' => 'Task Category', 'tasktext_usr' => 'Description', 'accountname' => 'Account');
          
        $rows['tasklist_detail'][1][0] = ""; 
        //now initialise report with this table layout
        $dashboard_report = array();
        $report = array();
               
        $dashboard_report[] = array('tab' => 'tasklist_detail', 'report_id' => $report_id, 'accountid' => $accountid, 'params' => $params, 'installation' => $installation, 'rows' => $rows['tasklist_detail'], 'cols' => $cols['tasklist_detail']);
        $report[] = '/installations/'.$installation.'/reports/ajax/account_key_task_detail.php';
 
        $pageFiles->addJsVar('dashboard_report', json_encode($dashboard_report));
        $pageFiles->addJsVar('report', json_encode($report));
 
		$content = "rep.default_dashboard_view.php";
		
		$pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.detail_params.php");
		$pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.dashboard_links.php");
	}
?>