<?php 
	
$tabs['incident_detail'] = "Incident Detail";
//set up rows and cols for the report table
$cols = array();
$rows = array();

$cols['incident_detail'][1] = array('id' => 'ID', 'injured_party' => 'Injured Party', 'status' => 'Status', 'site' => 'Site Name', 'type' => 'Type', 'state' => 'State', 'injured' => 'Injured', 'ambulance' => 'Ambulance', 'cost' => 'Cost');


$rows['incident_detail'][1][0] = "";

//now initialise report with this table layout
$dashboard_report = array();
$report = array();

$dashboard_report[] = array('tab' => 'incident_detail', 'report_id' => $report_id, 'accountid' => $accountid, 'params' => $params, 'installation' => $installation, 'rows' => $rows['incident_detail'], 'cols' => $cols['incident_detail']);
$report[] = '/installations/'.$installation.'/reports/ajax/account_incident_detail.php';
//echo "<br />the report id is ".$report_id;
//echo "<br />the account id is ".$accountid;
//echo "<br />the params are ".print_r($params);
$pageFiles->addJsVar('dashboard_report', json_encode($dashboard_report));
$pageFiles->addJsVar('report', json_encode($report));

$content = "testrep.default_dashboard_view.php";

$pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.detail_params.php");
//$pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.dashboard_links.php");

?>