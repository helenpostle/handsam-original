<?php 
	if ($report_id > 0) {
        if ($accountid > 0) {
            //also get latest licence for this account and use in link
            $lic_qry = "select licenceid from accountlicence right join tasklist on accountlicence.tasklistid = tasklist.tasklistid left join account on  accountlicence.accountid = account.accountid where (start_date < '".formatDatabaseInputDate($params['enddate'])."' and end_date > '".formatDatabaseInputDate($params['startdate'])."') and accountlicence.accountid = $accountid and accountlicence.state='ACTIVE' and appid=1 order by end_date desc limit 0,1";
            $lic_rs = getRS($con,$lic_qry);
            $lic_row = getRow($lic_rs);
        }
        
        $tabs['tasklist'] = "Group Detail";
        //set up rows and cols for the report table
        $cols = array();
        $rows = array();
        $cols['tasklist'][1] = array('task_cat' => 'Task Category', 'completed1' => '% completed', 'completed2' => 'number completed', 'total' => 'total number', 'allocated' => '% allocated');
          
        $sql = "select customtext as categoryname, categoryid from taskcategory where state = 'ACTIVE' and group_id = $report_id order by categoryname asc";
        $rs =  getRS($con, $sql);
        
        while ($rw = getRow($rs)) {
            if ($accountid > 0 && isset($lic_row['licenceid']) && $lic_row['licenceid'] > 0) {
                $rows['tasklist'][1][$rw['categoryid']] = "<a class=\"no_bg\" href=\"$rootUrl/modules/tasklist/tasks.php?accountid=$accountid&amp;licenceid={$lic_row['licenceid']}&amp;category={$rw['categoryid']}\">{$rw['categoryname']}</a>";
            } else {
                $rows['tasklist'][1][$rw['categoryid']] = $rw['categoryname'];
            }
        }

        //now initialise report with this table layout
        $dashboard_report = array();
        $report = array();
               
        $dashboard_report[] = array('tab' => 'tasklist', 'report_id' => $report_id, 'accountid' => $accountid, 'params' => $params, 'installation' => $installation, 'rows' => $rows['tasklist'], 'cols' => $cols['tasklist']);
        $report[] = '/installations/'.$installation.'/reports/ajax/account_tasklist_detail.php';
 
        $pageFiles->addJsVar('dashboard_report', json_encode($dashboard_report));
        $pageFiles->addJsVar('report', json_encode($report));
 
		$content = "rep.default_dashboard_view.php";
		
		$pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.ac_params.php");
		$pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.ac_dashboard_links.php");
	}
?>