<?php

	    	
$this->arrayParams["startdate"] = getParam("startdate",getStartLastMonth());
$this->arrayParams["enddate"] = getParam("enddate",getEndLastMonth());
$this->arrayParams["school_type"] = getParam("school_type",'ALL');

$arrayParams = $this->arrayParams;

if ($arrayParams["group_id"] == "") $arrayParams["group_id"] = 1;

$this->title['Date Range'] = $arrayParams["startdate"]." to ".$arrayParams["enddate"];

if ($this->arrayParams["school_type"] == 'ALL') {
	$school_type_qry = "";
	$this->title['School Type'] = "All School Types";
} else  {
	$school_type_qry = " and acc.school_type = '".$this->arrayParams["school_type"]."' ";
	$this->title['School Type'] = ucfirst(strtolower($this->arrayParams["school_type"]));
}

//account manager
$this->arrayParams["account_manager"] = getParam("account_manager",0);
if ($this->arrayParams["account_manager"] > 0) {
	$manager_qry = " and acc.account_manager = ".$this->arrayParams["account_manager"];
	$this->title['Accounts managed by'] = "Me";
} else {
	$manager_qry = "";
	$this->title['Accounts managed by'] = "All managers";
}

$include_file_sql = "SELECT 'all' AS id, COUNT(accidentid) AS result, 5 AS link_report, 'All' AS accountname, 'All' AS accountid, 'aa' AS this_order  FROM acc_accident left join account acc on acc_accident.accountid = acc.accountid   WHERE acc_accident.accountid != 0 and accident_time  >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND accident_time <= '".formatDatabaseInputDate($arrayParams["enddate"])."' and acc_accident.state='IN PROGRESS' and riddor = 'RIDDOR' $school_type_qry $manager_qry and acc.state = 'ACTIVE' ";

$include_file_sql .= " UNION SELECT acc.school_status AS id, COUNT(accidentid) AS result, 5 AS link_report, acc.school_status AS accountname, acc.school_status AS accountid, 'aaaaa' AS this_order FROM acc_accident RIGHT JOIN account acc ON acc_accident.accountid = acc.accountid and accident_time  >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND accident_time <= '".formatDatabaseInputDate($arrayParams["enddate"])."'  and acc_accident.state='IN PROGRESS' and riddor = 'RIDDOR' WHERE acc.accountid != 0 $school_type_qry $manager_qry and acc.state = 'ACTIVE' GROUP by acc.school_status ";

$include_file_sql .= " UNION SELECT acc.accountid AS id, COUNT(accidentid) AS result, 5 AS link_report, acc.accountname AS accountname, acc.accountid AS accountid, acc.accountname AS this_order FROM acc_accident RIGHT JOIN account acc ON acc_accident.accountid = acc.accountid and accident_time  >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND accident_time <= '".formatDatabaseInputDate($arrayParams["enddate"])."'  and acc_accident.state='IN PROGRESS'  and riddor = 'RIDDOR' WHERE acc.accountid != 0 $school_type_qry $manager_qry and acc.state = 'ACTIVE' GROUP by acc.accountid, accountname ORDER BY this_order ASC";
	

?>