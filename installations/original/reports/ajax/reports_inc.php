<?php

//adds plurals to strings
$Inflector = new Inflector();

$report = $_POST['dashboard_report'];
$tab = $report['tab'];

$response = array();
$response['title'] = array();
$response['data'][$tab] = array();


//echo json_encode(array(2 => 'hi'));

if (isset($report['params'])) {
    $arrayParams = $report['params'];
} else {
    $arrayParams = array();
}

//this report uses the school status array in config.php

$school_status_str = "'".implode("','",array_flip($school_status))."'";

if ($arrayParams["startdate"] == '') $arrayParams["startdate"] = getStartLastWeek();
if ($arrayParams["enddate"]  == '') $arrayParams["enddate"] = getEndLastWeek();
if (array_key_exists("school_type", $arrayParams) && $arrayParams["school_type"]  == '') $arrayParams["school_type"] = 'ALL';
if (array_key_exists("school_pupils", $arrayParams) && $arrayParams["school_pupils"]  == '') $arrayParams["school_pupils"] = 'ALL';
if (array_key_exists("school_status", $arrayParams) && isset($arrayParams["school_status"]) && $arrayParams["school_status"]  == '' || !isset($arrayParams["school_status"])) $arrayParams["school_status"] = 'ALL';

if (array_key_exists("school_cluster", $arrayParams) && $arrayParams["school_cluster"]  == '') $arrayParams["school_cluster"] = 'ALL';

if (!isset($arrayParams["catid"]) || (isset($arrayParams["catid"]) && $arrayParams["catid"]  == 0)) $arrayParams["catid"] = 'ALL';

$response['title']['Date Range'] = $arrayParams["startdate"]." to ".$arrayParams["enddate"];

if (!array_key_exists("school_type", $arrayParams)) {
    $school_type_qry = "";
} else if ($arrayParams["school_type"] == 'ALL') {
	$school_type_qry = "";
	$response['title']['School Type'] = "All School Types";
} else if ($arrayParams["school_type"] != 'ALL')  {
	$school_type_qry = " and act.school_type = '".$arrayParams["school_type"]."' ";
	$response['title']['School Type'] = ucfirst(strtolower($arrayParams["school_type"]));
} else {
    $school_type_qry = "";
}

if (!array_key_exists("school_pupils", $arrayParams)) {
    $school_pupils_qry = "";
} else if ($arrayParams["school_pupils"] == 'ALL') {
	$school_pupils_qry = "";
	$response['title'][ACCOUNT_TYPE_3] = "All ".$Inflector->pluralize(ACCOUNT_TYPE_3);
} else if($arrayParams["school_pupils"] != 'ALL')  {
	$school_pupils_qry = " and act.school_pupils = '".$arrayParams["school_pupils"]."' ";
	$response['title'][ACCOUNT_TYPE_3] = ucfirst(strtolower($arrayParams["school_pupils"]));
} else {
    $school_pupils_qry = "";
}

//account manager
$arrayParams["account_manager"] = getParam("account_manager",0);
if ($arrayParams["account_manager"] > 0) {
    $manager_qry = "";
	//$manager_qry = " and act.account_manager = ".$arrayParams["account_manager"];
	//$response['title']['Accounts managed by'] = "Me";
} else {
	$manager_qry = "";
	//$response['title']['Accounts managed by'] = "All managers";
}

if ($core_conf['school_cluster'] && isset($arrayParams["school_cluster"]) && $arrayParams["school_cluster"] == 'ALL') {
	$school_cluster_qry = "";
	$response['title'][ACCOUNT_TYPE_4] = "All ".$Inflector->pluralize(ACCOUNT_TYPE_4);
} else if ($core_conf['school_cluster'] && isset($arrayParams["school_cluster"])) {
	//$school_cluster_qry = " and act.school_cluster = '".$arrayParams["school_cluster"]."' ";
	$school_cluster_qry = " and act.cluster_id = '".$arrayParams["school_cluster"]."' ";
  //get cluster title
  $sql = "select title from account_cluster where id=".$arrayParams["school_cluster"];
  $rsx = getRS($con,$sql);
  $rowx = getRow($rsx);
	$response['title'][ACCOUNT_TYPE_4] = ucfirst(strtolower($rowx["title"]));
} else {
    $school_cluster_qry = "";
}

//cluster manager query
if(isset($clsUS->account_clusters) && count($clsUS->account_clusters)>0 && isset($core_conf['account_cluster_SA_access']) && $core_conf['account_cluster_SA_access'])
{
  $school_cluster_qry .= " and act.cluster_id in (".implode(',',$clsUS->account_clusters).") "; 
}

if ($arrayParams["catid"] == 'ALL') {
	$cat_qry = "";
} else  {
	$cat_qry = " and act.categoryid = '".$arrayParams["catid"]."' ";
}
?>