<?php
ini_set('max_execution_time', '0'); // 0 = no limit.

####make this a secure page
$secure = true;

require("../../../../shared/startPage.php");
$pageFiles->addFunc('dashboard');
$pageFiles->includePhp();

require('reports_inc.php');

///get permissions for account part of query
if (isAllowed('viewDashboard')) {
    $account_qry = "";
} else if (isAllowed('viewAccountDashboard')) {
    $account_qry = " and acttask.accountid=".$clsUS->accountid." ";
} else {
    die();
}

$params_serialized = urlencode(serialize($arrayParams));

$sql = "SELECT act.school_status, act.school_type, act.accountid as id, SUM(CASE WHEN acttask.state='COMPLETED' THEN 1 ELSE 0 END) as result4, ROUND(SUM(CASE WHEN acttask.state='COMPLETED' THEN 1 ELSE 0 END) / COUNT(acttask.accounttaskid) *100 ,1) AS result, 2 AS link_report, act.accountname AS accountname, act.accountid, act.accountname AS this_order, COUNT(acttask.accounttaskid) AS result3, SUM(CASE WHEN acttask.completedate <= acttask.actiondate THEN 1 ELSE 0 END) AS result2 
    FROM account act LEFT JOIN accounttask acttask ON act.accountid = acttask.accountid 
    WHERE  acttask.key_task = 1 AND acttask.actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND acttask.actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."' and act.accountid != 1 $school_type_qry $school_pupils_qry $manager_qry $school_cluster_qry $account_qry GROUP by act.accountid, accountname ORDER BY this_order ASC";

//echo $sql;

$all_rs = array('result3' => 0, 'result4' => 0); // array for all results
$account_type_rs = array();  //array for account type grouping
$rs = getRS($con, $sql);

while ($row = getRow($rs)) {
    //gets individual account info.
    $response['data'][$tab][2][$row['id']]['key_tasks']['result'] = $row['result'];
    $response['data'][$tab][2][$row['id']]['key_tasks']['cell_class'] = percentClass($row['result']);
    $all_rs['result3'] += $row['result3'];
    $all_rs['result4'] += $row['result4'];
    $st = str_replace(" ", "_", $row['school_status']);
    if (!isset($account_type_rs[$st]['result3'])) $account_type_rs[$st]['result3'] = 0;
    if (!isset($account_type_rs[$st]['result4'])) $account_type_rs[$st]['result4'] = 0;
    if (!isset($account_type_rs[$st]['orig_st'])) $account_type_rs[$st]['orig_st'] = $row['school_status'];
    $account_type_rs[$st]['result3'] += $row['result3'];
    $account_type_rs[$st]['result4'] += $row['result4'];
    $response['data'][$tab][2][$row['id']]['key_tasks']['link'] = "$rootUrl/handsam_core/dashboard_key_task_detail.php?report_id=2&accountid=".$row['accountid']."&params=".$params_serialized;
}

if ($all_rs['result3'] > 0) {
    $response['data'][$tab][1]['ALL']['key_tasks']['result'] =  round(($all_rs['result4'] / $all_rs['result3']) * 100, 1);
    $response['data'][$tab][1]['ALL']['key_tasks']['cell_class'] =  percentClass(round(($all_rs['result4'] / $all_rs['result3']) * 100, 1));
    $response['data'][$tab][1]['ALL']['key_tasks']['link'] = "$rootUrl/handsam_core/dashboard_key_task_detail.php?report_id=2&accountid=ALL&params=".$params_serialized;
}

foreach ($account_type_rs as $key => $at) {
    if ($at['result3'] > 0) {
        $response['data'][$tab][1][$key]['key_tasks']['cell_class'] =  percentClass(round(($at['result4'] / $at['result3']) * 100, 1));
        $response['data'][$tab][1][$key]['key_tasks']['result'] =  round(($at['result4'] / $at['result3']) * 100, 1);
        $response['data'][$tab][1][$key]['key_tasks']['link'] = "$rootUrl/handsam_core/dashboard_key_task_detail.php?report_id=2&accountid=".$account_type_rs[$key]['orig_st']."&params=".$params_serialized;
    }
}

echo json_encode($response);
	    	
?>