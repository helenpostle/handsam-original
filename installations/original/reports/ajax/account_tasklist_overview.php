<?php
ini_set('max_execution_time', '0'); // 0 = no limit.

####make this a secure page
$secure = true;

require("../../../../shared/startPage.php");
$pageFiles->addFunc('dashboard');
$pageFiles->includePhp();

require('reports_inc.php');

$params_serialized = urlencode(serialize($arrayParams));

if (isset($report['report_id']) && $report['report_id'] > 0) {
    $group_id = $report['report_id'];
} else {
    $group_id = 1;
}

//get permissions for account part of query
if (isAllowed('viewDashboard')) {
    $account_qry = "";
} else if (isAllowed('viewAccountDashboard')) {
    $account_qry = " and act.accountid=".$clsUS->accountid." ";
} else {
    die();
}


$sql = "SELECT act.school_status, act.school_type, act.accountid as id, SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) as result4, ROUND(SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS result, 2 AS link_report, act.accountname AS accountname, act.accountid, act.accountname AS this_order, COUNT(act.accounttaskid) AS result3, SUM(CASE WHEN act.completedate <= act.actiondate THEN 1 ELSE 0 END) AS result2 FROM  rep_acc_task_cat_group act WHERE act.group_id = $group_id AND act.actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND act.actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."'  $school_type_qry $school_pupils_qry $manager_qry $school_cluster_qry $cat_qry $account_qry AND act.state !='INACTIVE' AND act.state !='DELETED'  AND act.state !='DELETED1' GROUP by act.accountid, accountname ORDER BY this_order ASC";

//echo $sql;



$all_rs = array('result3' => 0, 'result4' => 0); // array for all results
$account_type_rs = array();  //array for account type grouping
$rs = getRS($con, $sql);

while ($row = getRow($rs)) {
    //gets individual account info.
    $response['data'][$tab][2][$row['id']][$group_id]['result'] = $row['result'];
    $response['data'][$tab][2][$row['id']][$group_id]['cell_class'] = percentClass($row['result']);
    $all_rs['result3'] += $row['result3'];
    $all_rs['result4'] += $row['result4'];
    $st = str_replace(" ", "_", $row['school_status']);
    if (!isset($account_type_rs[$st]['result3'])) $account_type_rs[$st]['result3'] = 0;
    if (!isset($account_type_rs[$st]['result4'])) $account_type_rs[$st]['result4'] = 0;
    if (!isset($account_type_rs[$st]['orig_st'])) $account_type_rs[$st]['orig_st'] = $row['school_status'];
    $account_type_rs[$st]['result3'] += $row['result3'];
    $account_type_rs[$st]['result4'] += $row['result4'];
    $response['data'][$tab][2][$row['id']][$group_id]['link'] = "$rootUrl/handsam_core/dashboard_tasklist_detail.php?report_id=".$group_id."&accountid=".$row['accountid']."&params=".$params_serialized;
}

if (isAllowed('viewDashboard')) {
    if ($all_rs['result3'] > 0) {
        $this_res = round(($all_rs['result4'] / $all_rs['result3']) * 100, 1);
        $this_class = percentClass($this_res);
        $response['data'][$tab][1]['ALL'][$group_id]['cell_class'] = $this_class;
        $response['data'][$tab][1]['ALL'][$group_id]['result'] =  $this_res;    
        $response['data'][$tab][1]['ALL'][$group_id]['link'] = "$rootUrl/handsam_core/dashboard_tasklist_detail.php?report_id=".$group_id."&accountid=ALL&params=".$params_serialized;
    }

    foreach ($account_type_rs as $key => $at) {
        if ($at['result3'] > 0) {
            $this_res = round(($at['result4'] / $at['result3']) * 100, 1);
            $this_class = percentClass($this_res);
            $response['data'][$tab][1][$key][$group_id]['cell_class'] =  $this_class;
            $response['data'][$tab][1][$key][$group_id]['result'] =  $this_res;
            $response['data'][$tab][1][$key][$group_id]['link'] = "$rootUrl/handsam_core/dashboard_tasklist_detail.php?report_id=".$group_id."&accountid=".$account_type_rs[$key]['orig_st']."&params=".$params_serialized;
        }
    }
}

echo json_encode($response);
	    	
?>