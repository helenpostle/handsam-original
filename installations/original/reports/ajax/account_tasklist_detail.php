<?php
ini_set('max_execution_time', '0'); // 0 = no limit.

####make this a secure page
$secure = true;

require("../../../../shared/startPage.php");
$pageFiles->addFunc('dashboard');
$pageFiles->includePhp();

//adds plurals to strings
$Inflector = new Inflector();

$report = $_POST['dashboard_report'];
$tab = $report['tab'];

$response = array();
$response['title'] = array();
$response['data'][$tab] = array();

if (isset($report['params'])) {
    $arrayParams = $report['params'];
} else {
    $arrayParams = array();
}

if (isset($report['report_id']) && $report['report_id'] > 0) {
    $group_id = $report['report_id'];
} else {
    $group_id = 1;
}

//get permissions for account part of query
if (!isAllowed('viewDashboard') && isAllowed('viewAccountDashboard')) {
    $report['accountid'] = $clsUS->accountid;
} else if (!isAllowed('viewDashboard') && !isAllowed('viewAccountDashboard')) {
    die();
}

//process the accountid
//school_status value is passed as an accountid. Has 3 values - ALL, AET, LAET
$status_sql = "";
if (isset($report['accountid']) && $report['accountid'] > 0) {
    //accountid is a real accountid
    $accountid = $report['accountid'];
    //get accountname
    $sql = "select accountname from account where accountid = $accountid";
    $rs = getRS($con,$sql);
    $row = getRow($rs);
    $response['title']['Account'] = $row['accountname'];
} else if(isset($report['accountid']) && is_string($report['accountid']) && $report['accountid'] != 'null') {
    //accountid is a school_status
     if ($report['accountid'] != 'ALL') {
        $accountid = $report['accountid'];
        $status_sql = " and act.school_status = '$accountid'";     
        //get accountname
        foreach ($school_status as $key => $val) {
            if ($accountid == $key) {
                $response['title']['Account'] = "'$val' Accounts";
            }
        }
    } else {
        $response['title']['Account'] = "All Accounts";
        $accountid = $report['accountid'];
    }
    
} else {
    //no accountid
    $accountid = 0;
}

if ($arrayParams["startdate"] == '') $arrayParams["startdate"] = getStartLastWeek();
if ($arrayParams["enddate"]  == '') $arrayParams["enddate"] = getEndLastWeek();
if (array_key_exists("school_type", $arrayParams) && $arrayParams["school_type"]  == '') $arrayParams["school_type"] = 'ALL';
if (array_key_exists("school_pupils", $arrayParams) && $arrayParams["school_pupils"]  == '') $arrayParams["school_pupils"] = 'ALL';
if (array_key_exists("school_cluster", $arrayParams) && $arrayParams["school_cluster"]  == '') $arrayParams["school_cluster"] = 'ALL';

$response['title']['Date Range'] = $arrayParams["startdate"]." to ".$arrayParams["enddate"];
	    	
//get group name:
$sql = "select * from task_category_group where group_id = $group_id";
$rs = getRS($con,$sql);
$row = getRow($rs);

if (isset($arrayParams["school_type"]) && $arrayParams["school_type"] != 'ALL') {
	$school_type_qry = " and act.school_type = '".$arrayParams["school_type"]."' ";
	$response['title']['School Type'] = ucfirst(strtolower($arrayParams["school_type"]));
} else if (isset($arrayParams["school_type"]) && $arrayParams["school_type"] == 'ALL') {
    $school_type_qry = "";
    $response['title']['School Type'] = "All School Types";
} else {
    $school_type_qry = "";
}

if (isset($arrayParams["school_pupils"]) && $arrayParams["school_pupils"] != 'ALL') {
	$school_pupils_qry = " and act.school_pupils = '".$arrayParams["school_pupils"]."' ";
	$response['title'][ACCOUNT_TYPE_3] = ucfirst(strtolower($arrayParams["school_pupils"]));
} else if (isset($arrayParams["school_pupils"]) && $arrayParams["school_pupils"] == 'ALL') {
    $school_pupils_qry = "";
    $response['title'][ACCOUNT_TYPE_3] = "All ".$Inflector->pluralize(ACCOUNT_TYPE_3);
} else {
    $school_pupils_qry = "";
}

$group_sql = "";
if ($group_id > 0) {
	$group_sql = " and tc.group_id = $group_id ";
	//get groupname
	$group_name_sql = "select * from task_category_group where group_id = $group_id";
	$group_name_rs = getRS($con, $group_name_sql);
	$group_row = getRow($group_name_rs);
	$response['title']['Group'] = $group_row['title'];
} else {
	$response['title']['Group'] = "All Groups";
}
	
$manager_qry = "";

if ($core_conf['school_cluster'] && isset($arrayParams["school_cluster"]) && $arrayParams["school_cluster"] != 'ALL') {
    $school_cluster_qry = " and acc.school_cluster = '".$arrayParams["school_cluster"]."' ";
    $response['title'][ACCOUNT_TYPE_4] = ucfirst(strtolower($arrayParams["school_cluster"]));
} else if($core_conf['school_cluster'] && isset($arrayParams["school_cluster"]) && $arrayParams["school_cluster"] == 'ALL') {
    $school_cluster_qry = "";
    $response['title'][ACCOUNT_TYPE_4] = "All ".$Inflector->pluralize(ACCOUNT_TYPE_4);
}  else {
    $school_cluster_qry = "";
}

if (in_array($accountid, array_flip($school_status))) {
	
	//change to school_type
	$include_file_sql = "SELECT tc.categoryid AS id, tc.customtext as category_name, ROUND(SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS on_time, COUNT(act.accounttaskid) AS total, SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) AS num_on_time, ROUND(SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS completed, SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) AS num_completed, ROUND(SUM(CASE WHEN act.assignedto > 0  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS allocated, licenceid FROM  rep_acc_task_cat_group act RIGHT JOIN taskcategory tc ON act.categoryid = tc.categoryid WHERE   act.actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND act.actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."' AND act.state !='INACTIVE' AND act.state !='DELETED'  AND act.state !='DELETED1' $school_type_qry $manager_qry $group_sql $school_cluster_qry $status_sql group by category_name, id order by category_name ASC";
	
} else if ($accountid == "ALL") {
	//both types hard coded
	$include_file_sql = "SELECT tc.categoryid AS id, tc.customtext as category_name, ROUND(SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS on_time, ROUND(SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS completed, COUNT(act.accounttaskid) AS total, SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) AS num_on_time, SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) AS num_completed, ROUND(SUM(CASE WHEN act.assignedto > 0  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS allocated, licenceid FROM rep_acc_task_cat_group act RIGHT JOIN taskcategory tc ON act.categoryid = tc.categoryid WHERE   act.actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND act.actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."' AND act.state !='INACTIVE' AND act.state !='DELETED'  AND act.state !='DELETED1' $school_type_qry $manager_qry $group_sql $school_cluster_qry group by category_name, id order by category_name ASC";
	
} else {
	$accountid = intval($accountid);
	$include_file_sql = "SELECT tc.categoryid AS id, tc.customtext as category_name, ROUND(SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS on_time, ROUND(SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS completed, COUNT(act.accounttaskid) AS total, SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) AS num_on_time, SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) AS num_completed, ROUND(SUM(CASE WHEN act.assignedto > 0  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS allocated, licenceid FROM rep_acc_task_cat_group act RIGHT JOIN taskcategory tc ON act.categoryid = tc.categoryid WHERE   act.actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND act.actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."' AND act.accountid = ".$accountid." $school_pupils_qry $school_type_qry $manager_qry $group_sql $school_cluster_qry  AND act.state !='INACTIVE' AND act.state !='DELETED'  AND act.state !='DELETED1' group by category_name, id order by category_name ASC";

} 
//  echo $include_file_sql;

$rs = getRS($con, $include_file_sql);

while ($row = getRow($rs)) {
    $response['data'][$tab][1][$row['id']]['completed1']['result'] = $row['completed'];
    $response['data'][$tab][1][$row['id']]['completed1']['cell_class'] = percentClass($row['completed']);
    $response['data'][$tab][1][$row['id']]['completed2']['result'] = $row['num_completed'];
    $response['data'][$tab][1][$row['id']]['total']['result'] = $row['total'];
    $response['data'][$tab][1][$row['id']]['allocated']['result'] = $row['allocated'];
    $response['data'][$tab][1][$row['id']]['allocated']['cell_class'] = percentClass($row['allocated']);

    if ($accountid > 0)
    {
        $row_link = "$rootUrl/modules/tasklist/tasks.php?accountid=$accountid&amp;licenceid={$row['licenceid']}&amp;category={$row['id']}";
        $response['data'][$tab][1][$row['id']]['completed1']['link'] = $row_link;
        $response['data'][$tab][1][$row['id']]['completed2']['link'] = $row_link;
        $response['data'][$tab][1][$row['id']]['total']['link'] = $row_link;
        $response['data'][$tab][1][$row['id']]['allocated']['link'] = $row_link;
    }    
}

echo json_encode($response);
