<?php
ini_set('max_execution_time', '0'); // 0 = no limit.

####make this a secure page
$secure = true;

require("../../../../shared/startPage.php");
$pageFiles->addFunc('dashboard');
$pageFiles->includePhp();

//adds plurals to strings
$Inflector = new Inflector();

$report = $_POST['dashboard_report'];
$tab = $report['tab'];

$response = array();
$response['title'] = array();
$response['data'][$tab] = array();

if (isset($report['params'])) {
    $arrayParams = $report['params'];
} else {
    $arrayParams = array();
}

//get permissions for account part of query
if (!isAllowed('viewDashboard') && isAllowed('viewAccountDashboard')) {
    $report['accountid'] = $clsUS->accountid;
} else if (!isAllowed('viewDashboard') && !isAllowed('viewAccountDashboard')) {
    die();
}

if (isset($report['report_id']) && $report['report_id'] == 0) {
	$response['title']['Riddor Reportable'] = "Open Non-riddor incidents";
	$riddor_sql = " and acc_accident.riddor = 'NON-RIDDOR' and acc_accident.state = 'IN PROGRESS' ";
} else if (isset($report['report_id']) && $report['report_id'] == 1){
	$response['title']['Riddor Reportable'] = "Open Riddor incidents";
	$riddor_sql = " and acc_accident.riddor = 'RIDDOR' and acc_accident.state = 'IN PROGRESS' ";
} else {
	$riddor_sql = "";
	$response['title']['Riddor Reportable'] = "All Riddor and Non-riddor incidents";
	
}

$acc_sql = "";
if (isset($report['accountid']) && $report['accountid'] > 0) {
    //accountid is a real accountid
    $accountid = $report['accountid'];
    //get accountname
    $sql = "select accountname from account where accountid = $accountid";
    $rs = getRS($con,$sql);
    $row = getRow($rs);
    $response['title']['Account'] = $row['accountname'];
    $acc_sql = " and acc_accident.accountid = ".$accountid;
} else if(isset($report['accountid']) && is_string($report['accountid']) && $report['accountid'] != 'null') {
    //accountid is a school_status
     if ($report['accountid'] != 'ALL') {
        $accountid = $report['accountid'];
        $acc_sql = " and account.school_status = '".$report['accountid']."'";    
        //get accountname
        foreach ($school_status as $key => $val) {
            if ($accountid == $key) {
                $response['title']['Account'] = "'$val' Accounts";
            }
        }
    } else {
        $response['title']['Account'] = "All Accounts";
        $accountid = $report['accountid'];
    }
    
} else {
    //no accountid
    $accountid = 0;
}

if ($arrayParams["startdate"] == '') $arrayParams["startdate"] = getStartLastWeek();
if ($arrayParams["enddate"]  == '') $arrayParams["enddate"] = getEndLastWeek();
if (array_key_exists("school_type", $arrayParams) && $arrayParams["school_type"]  == '') $arrayParams["school_type"] = 'ALL';
if (array_key_exists("school_pupils", $arrayParams) && $arrayParams["school_pupils"]  == '') $arrayParams["school_pupils"] = 'ALL';
if (array_key_exists("school_cluster", $arrayParams) && $arrayParams["school_cluster"]  == '') $arrayParams["school_cluster"] = 'ALL';

$response['title']['Date Range'] = $arrayParams["startdate"]." to ".$arrayParams["enddate"];

if (isset($arrayParams["school_type"]) && $arrayParams["school_type"] != 'ALL') {
    $school_type_qry = " and account.school_type = '".$arrayParams["school_type"]."' ";
	$response['title']['School Type'] = ucfirst(strtolower($arrayParams["school_type"]));
} else if (isset($arrayParams["school_type"]) && $arrayParams["school_type"] == 'ALL'){
    $school_type_qry = "";
    $response['title']['School Type'] = "All School Types";
} else {
    $school_type_qry = "";
}

if (isset($arrayParams["school_pupils"]) && $arrayParams["school_pupils"] != 'ALL') {
    $school_pupils_qry = " and account.school_pupils = '".$arrayParams["school_pupils"]."' ";
	$response['title'][ACCOUNT_TYPE_3] = ucfirst(strtolower($arrayParams["school_pupils"]));
} else if (isset($arrayParams["school_pupils"]) && $arrayParams["school_pupils"] == 'ALL'){
    $school_pupils_qry = "";
    $response['title'][ACCOUNT_TYPE_3] = "All ".$Inflector->pluralize(ACCOUNT_TYPE_3);
} else {
    $school_pupils_qry = "";
}



if ($core_conf['school_cluster'] && isset($arrayParams["school_cluster"]) && $arrayParams["school_cluster"] != 'ALL') {
    $school_cluster_qry = " and account.school_cluster = '".$arrayParams["school_cluster"]."' ";
	$response['title'][ACCOUNT_TYPE_4] = ucfirst(strtolower($arrayParams["school_cluster"]));
} else if ($core_conf['school_cluster'] && isset($arrayParams["school_cluster"]) && $arrayParams["school_cluster"] == 'ALL'){
    $school_cluster_qry = "";
	$response['title'][ACCOUNT_TYPE_4] = "All ".$Inflector->pluralize(ACCOUNT_TYPE_4);
} else {
    $school_cluster_qry = "";
}

if (isset($arrayParams['cluster_qry']){
   $school_cluster_qry = $arrayParams['cluster_qry'];
}

$include_file_sql = "SELECT accountname, school_status, acc_accident.accidentid as id, acc_accident.state, acc_accident.accountid as accountid, (CASE WHEN acc_accident.injured_usertype=2 THEN CONCAT(ua.lastname, ', ', ua.firstname) ELSE CONCAT(u.firstname, ', ',u.lastname) END) AS injured_party, status, acc_accident_site.site_name, accident_type, (CASE when riddor = 'NON-RIDDOR'  THEN 'No' ELSE 'Yes' END) as riddor, (CASE when ambulance = 0  THEN 'No' ELSE 'Yes' END) as ambulance, (CASE when party_injured = 0  THEN 'No' ELSE 'Yes' END) as injured, (CASE when first_aid = 0  THEN 'No' ELSE 'Yes' END) as first_aid, days_off, cost ";

$include_file_sql .= "FROM acc_accident LEFT JOIN acc_injured_party ON acc_accident.injuredid = acc_injured_party.injuredid
						LEFT JOIN usr u ON acc_accident.userid = u.userid
						LEFT JOIN usr_assoc AS ua ON acc_accident.assoc_userid = ua.assoc_userid 
						LEFT JOIN acc_accident_site ON acc_accident.siteid = acc_accident_site.siteid
						LEFT JOIN account ON acc_accident.accountid = account.accountid ";

$include_file_sql .= "WHERE  CAST(accident_time AS DATE) >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND CAST(accident_time AS DATE) <= '".formatDatabaseInputDate($arrayParams["enddate"])."' $school_type_qry $school_pupils_qry $riddor_sql $acc_sql $manager_qry $school_cluster_qry";

$include_file_sql .= " ORDER BY \"Injured Party\"";


$manager_qry = "";



echo $include_file_sql;

?>