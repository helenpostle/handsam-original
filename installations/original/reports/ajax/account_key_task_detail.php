<?php
ini_set('max_execution_time', '0'); // 0 = no limit.

####make this a secure page
$secure = true;

require("../../../../shared/startPage.php");
$pageFiles->addFunc('dashboard');
$pageFiles->includePhp();

//adds plurals to strings
$Inflector = new Inflector();

if ($core_conf['key_task']) {
    
    $report = $_POST['dashboard_report'];
    $tab = $report['tab'];

    $response = array();
    $response['title'] = array();
    $response['data'][$tab] = array();

    if (isset($report['params'])) {
        $arrayParams = $report['params'];
    } else {
        $arrayParams = array();
    }

    //get permissions for account part of query
    if (!isAllowed('viewDashboard') && isAllowed('viewAccountDashboard')) {
        $report['accountid'] = $clsUS->accountid;
    } else if (!isAllowed('viewDashboard') && !isAllowed('viewAccountDashboard')) {
        die();
    }
    
    //get accountid/school_status
    $status_sql = "";
    if (isset($report['accountid']) && $report['accountid'] > 0) {
        $accountid = $report['accountid'];
        $sql = "select accountname from account where accountid = $accountid";
        $rs = getRS($con,$sql);
        $row = getRow($rs);
        $response['title']['Account'] = $row['accountname'];

    } else if(isset($report['accountid']) && is_string($report['accountid']) && $report['accountid'] != 'null') {
        if ($report['accountid'] != 'ALL') {
            $accountid = $report['accountid'];
            $arrayParams["school_status"] = $report['accountid'];
            $status_sql = " and acc.school_status = '$accountid'";
            foreach ($school_status as $key => $val) {
                if ($accountid == $key) {
                    $response['title']['Account'] = "'$val' Accounts";
                }
            }
        } else {
            $accountid = $report['accountid'];
            $response['title']['Account'] = "All Accounts";
        }
    } else {
        $accountid = 0;
    }
    
    
    if ($arrayParams["startdate"] == '') $arrayParams["startdate"] = getStartLastWeek();
    if ($arrayParams["enddate"]  == '') $arrayParams["enddate"] = getEndLastWeek();
    if (array_key_exists("school_type", $arrayParams) && $arrayParams["school_type"]  == '') $arrayParams["school_type"] = 'ALL';
    if (array_key_exists("school_pupils", $arrayParams) && $arrayParams["school_pupils"]  == '') $arrayParams["school_pupils"] = 'ALL';
    if (array_key_exists("school_cluster", $arrayParams) && $arrayParams["school_cluster"]  == '') $arrayParams["school_cluster"] = 'ALL';

    $response['title']['Date Range'] = $arrayParams["startdate"]." to ".$arrayParams["enddate"];
    
    if (isset($arrayParams["school_type"]) && $arrayParams["school_type"] != 'ALL') {
        $school_type_qry = " and acc.school_type = '".$arrayParams["school_type"]."' ";
        $response['title']['School Type'] = ucfirst(strtolower($arrayParams["school_type"]));
    } else if (isset($arrayParams["school_type"]) && $arrayParams["school_type"] == 'ALL'){
        $school_type_qry = "";
        $response['title']['School Type'] = "All School Types";
    } else {
        $school_type_qry = "";
    }

    if (isset($arrayParams["school_pupils"]) && $arrayParams["school_pupils"] != 'ALL') {
        $school_pupils_qry = " and acc.school_pupils = '".$arrayParams["school_pupils"]."' ";
        $response['title'][ACCOUNT_TYPE_3] = ucfirst(strtolower($arrayParams["school_pupils"]));
    } else if (isset($arrayParams["school_pupils"]) && $arrayParams["school_pupils"] == 'ALL'){
        $school_pupils_qry = "";
        $response['title'][ACCOUNT_TYPE_3] = "All ".$Inflector->pluralize(ACCOUNT_TYPE_3);    
    } else {
        $school_pupils_qry = "";
    }
    
    if ($core_conf['school_cluster'] && isset($arrayParams["school_cluster"]) && $arrayParams["school_cluster"] != 'ALL') {
        $school_cluster_qry = " and acc.school_cluster = '".$arrayParams["school_cluster"]."' ";
        $response['title'][ACCOUNT_TYPE_4] = ucfirst(strtolower($arrayParams["school_cluster"]));
    } else if($core_conf['school_cluster'] && isset($arrayParams["school_cluster"]) && $arrayParams["school_cluster"] == 'ALL') {
        $school_cluster_qry = "";
        $response['title'][ACCOUNT_TYPE_4] = "All ".$Inflector->pluralize(ACCOUNT_TYPE_4);
    }  else {
        $school_cluster_qry = "";
    }
    
    $manager_qry = "";

    if (in_array($accountid, array_flip($school_type))) {


    $sql = "SELECT act.accounttaskid AS id, CASE WHEN tc.categoryname = '' THEN tc.customtext ELSE tc.categoryname END as category_name, act.*, usr.username, acc.accountname 
        FROM accounttask act left join taskcategory tc ON act.categoryid = tc.categoryid 
        left join account acc on act.accountid = acc.accountid 
        left join usr on act.assignedto = usr.userid 
        WHERE  act.key_task = 1 AND act.actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND act.actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."' and act.accountid != 1 $school_type_qry $school_pupils_qry $manager_qry  $school_cluster_qry $status_sql ORDER BY act.actiondate ASC";   
    
    } else if ($accountid == "ALL") {

     $sql = "SELECT act.accounttaskid as id, CASE WHEN tc.categoryname = '' THEN tc.customtext ELSE tc.categoryname END as category_name, act.*, usr.username, acc.accountname 
        FROM accounttask act left join taskcategory tc ON act.categoryid = tc.categoryid 
        left join account acc on act.accountid = acc.accountid 
        left join usr on act.assignedto = usr.userid 
        WHERE  act.key_task = 1 AND act.actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND act.actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."' and act.accountid != 1 $school_type_qry $school_pupils_qry $manager_qry $school_cluster_qry $status_sql ORDER BY act.actiondate ASC";   


    } else {
        $accountid = intval($accountid);
        $sql = "SELECT act.accounttaskid AS id, CASE WHEN tc.categoryname = '' THEN tc.customtext ELSE tc.categoryname END as category_name, act.*, usr.username, acc.accountname 
        FROM accounttask act left join taskcategory tc ON act.categoryid = tc.categoryid 
        left join account acc on act.accountid = acc.accountid 
        left join usr on act.assignedto = usr.userid 
        WHERE  act.key_task = 1 AND act.actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND act.actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."' and act.accountid = $accountid $school_type_qry $school_pupils_qry $manager_qry $school_cluster_qry $status_sql ORDER BY act.actiondate ASC";   

    } 

    //echo $sql;

    $rs = getRS($con, $sql);

    while ($row = getRow($rs)) {
        $response['data'][$tab][1][$row['id']]['actiondate']['result'] = displayDate($row['actiondate']);
        //work out class
        $class = "";
        if (strtotime($row["actiondate"]) < time() && $row["state"] != "COMPLETED") {
            $class = "late";
        } else if (($row['completedate'] > $row['actiondate']) && $row["state"] == "COMPLETED") {
            $class = "late_complete";
        } else if($row["state"] == "COMPLETED")  {
            $class = "complete";
        }
        $response['data'][$tab][1][$row['id']]['task_cat']['cell_class'] = $class;
        $response['data'][$tab][1][$row['id']]['task_cat']['result'] = $row['category_name'];
        $response['data'][$tab][1][$row['id']]['tasktext_usr']['result'] = $row['customtext'];
        $response['data'][$tab][1][$row['id']]['accountname']['result'] = $row['accountname'];
        
    }
    $response['type'] = "create";
    echo json_encode($response);
}
?>