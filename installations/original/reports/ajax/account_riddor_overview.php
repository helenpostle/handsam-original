<?php
ini_set('max_execution_time', '0'); // 0 = no limit.

####make this a secure page
$secure = true;

require("../../../../shared/startPage.php");
$pageFiles->addFunc('dashboard');
$pageFiles->includePhp();

require('reports_inc.php');

//get permissions for account part of query
if (isAllowed('viewDashboard')) {
    $account_qry = "";
} else if (isAllowed('viewAccountDashboard')) {
    $account_qry = " and act.accountid=".$clsUS->accountid." ";
} else {
    die();
}

$params_serialized = urlencode(serialize($arrayParams));

/*
$include_file_sql .= " SELECT act.accountid AS id, COUNT(accidentid) AS result, 5 AS link_report, act.accountname AS accountname, act.accountid AS accountid, act.accountname AS this_order FROM acc_accident RIGHT JOIN account acc ON acc_accident.accountid = act.accountid and accident_time  >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND accident_time <= '".formatDatabaseInputDate($arrayParams["enddate"])."'  and acc_accident.state='IN PROGRESS'  and riddor = 'RIDDOR' WHERE act.accountid != 1 $school_type_qry $school_pupils_qry $manager_qry and act.state = 'ACTIVE' GROUP by act.accountid, accountname ORDER BY this_order, accountname ASC";
*/

$include_file_sql = " SELECT act.school_status, act.school_type, act.accountid AS id, COUNT(accidentid) AS result, 5 AS link_report, act.accountname AS accountname, act.accountid AS accountid, act.accountname AS this_order FROM acc_accident RIGHT JOIN account act ON acc_accident.accountid = act.accountid and accident_time  >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND accident_time <= '".formatDatabaseInputDate($arrayParams["enddate"])."'  and acc_accident.state='IN PROGRESS' and riddor = 'RIDDOR' WHERE act.state = 'ACTIVE' $school_type_qry $school_pupils_qry $manager_qry $school_cluster_qry $account_qry  GROUP by act.accountid, accountname ORDER BY this_order, accountname ASC";

//echo $include_file_sql;

$all_rs = array('result' => 0); // array for all results
$account_type_rs = array();  //array for account type grouping
$rs = getRS($con, $include_file_sql);

while ($row = getRow($rs)) {
    //gets individual account info.
    $response['data'][$tab][2][$row['id']]['riddor']['result'] = $row['result'];
    $response['data'][$tab][2][$row['id']]['riddor']['cell_class'] = incidentClass($row['result']);
    $all_rs['result'] += $row['result'];
    $st = str_replace(" ", "_", $row['school_status']);
    if (!isset($account_type_rs[$st]['result'])) $account_type_rs[$st]['result'] = 0;
    if (!isset($account_type_rs[$st]['orig_st'])) $account_type_rs[$st]['orig_st'] = $row['school_status'];
    $account_type_rs[$st]['result'] += $row['result'];
    $response['data'][$tab][2][$row['id']]['riddor']['link'] = "$rootUrl/handsam_core/dashboard_incident_detail.php?report_id=1&accountid=".$row['accountid']."&params=".$params_serialized;
}

if ($all_rs['result'] > 0) {
    $response['data'][$tab][1]['ALL']['riddor']['result'] =  $all_rs['result'];
    $response['data'][$tab][1]['ALL']['riddor']['cell_class'] =  incidentClass($all_rs['result']);
    $response['data'][$tab][1]['ALL']['riddor']['link'] = "$rootUrl/handsam_core/dashboard_incident_detail.php?report_id=1&accountid=ALL&params=".$params_serialized;
}

foreach ($account_type_rs as $key => $at) {
    if ($at['result'] > 0) {
        $response['data'][$tab][1][$key]['riddor']['result'] =  $at['result'];
        $response['data'][$tab][1][$key]['riddor']['cell_class'] =  incidentClass($at['result']);
        $response['data'][$tab][1][$key]['riddor']['link'] = "$rootUrl/handsam_core/dashboard_incident_detail.php?report_id=1&accountid=".$account_type_rs[$key]['orig_st']."&params=".$params_serialized;
    }
}

echo json_encode($response);
?>