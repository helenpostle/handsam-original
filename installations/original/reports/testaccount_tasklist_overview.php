<?php
//this report uses the school status array in config.php

$school_status_str = "'".implode("','",$school_status)."'";

$this->arrayParams["startdate"] = getParam("startdate",getStartLastMonth());
$this->arrayParams["enddate"] = getParam("enddate",getEndLastMonth());
$this->arrayParams["school_type"] = getParam("school_type",'ALL');

if ($this->arrayParams["group_id"] == "") $this->arrayParams["group_id"] = 1;

$this->title['Date Range'] = $this->arrayParams["startdate"]." to ".$this->arrayParams["enddate"];

if ($this->arrayParams["school_type"] == 'ALL') {
	$school_type_qry = "";
	$this->title['School Type'] = "All School Types";
} else  {
	$school_type_qry = " and acc.school_type = '".$this->arrayParams["school_type"]."' ";
	$this->title['School Type'] = ucfirst(strtolower($this->arrayParams["school_type"]));
}

//account manager
$this->arrayParams["account_manager"] = getParam("account_manager",0);
if ($this->arrayParams["account_manager"] > 0) {
	$manager_qry = " and acc.account_manager = ".$this->arrayParams["account_manager"];
	$this->title['Accounts managed by'] = "Me";
} else {
	$manager_qry = "";
	$this->title['Accounts managed by'] = "All managers";
}


$arrayParams = $this->arrayParams;

$include_file_sql = "SELECT 'all' as id, ROUND(SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS result, 2 AS link_report, 'All' AS accountname, 'All' AS accountid, 'aa' AS this_order, COUNT(act.accounttaskid) AS result3, SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) AS result2 FROM account acc LEFT JOIN (SELECT school_status, accountid, state, group_id, actiondate, completedate, accounttaskid FROM rep_acc_task_cat_group WHERE group_id = ".$arrayParams["group_id"]." AND actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."' ) act ON acc.accountid = act.accountid WHERE acc.accountid != 0 $school_type_qry $manager_qry and acc.state = 'ACTIVE' ";

$include_file_sql .= " UNION SELECT acc.school_status as id, ROUND(SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS result, 2 AS link_report, acc.school_status AS accountname,  acc.school_status AS accountid, 'aaaaa' AS this_order, COUNT(act.accounttaskid) AS result3, SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) AS result2 FROM account acc LEFT JOIN (SELECT school_status, accountid, state, group_id, actiondate, completedate, accounttaskid FROM rep_acc_task_cat_group WHERE group_id = ".$arrayParams["group_id"]." AND actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."' ) act ON acc.accountid = act.accountid WHERE acc.accountid != 0 $school_type_qry $manager_qry and acc.state = 'ACTIVE' GROUP by acc.school_status "; 

$include_file_sql .= "UNION SELECT acc.accountid as id, ROUND(SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS result, 2 AS link_report, acc.accountname AS accountname, acc.accountid, acc.accountname AS this_order, COUNT(act.accounttaskid) AS result3, SUM(CASE WHEN act.state='COMPLETED' THEN 1 ELSE 0 END) AS result2 FROM account acc LEFT JOIN (SELECT school_status,accountid, state, group_id, actiondate, completedate, accounttaskid FROM rep_acc_task_cat_group WHERE  group_id = ".$arrayParams["group_id"]." AND actiondate >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND actiondate <= '".formatDatabaseInputDate($arrayParams["enddate"])."') act ON acc.accountid = act.accountid WHERE acc.accountid != 0 $school_type_qry $manager_qry AND act.school_status IN ($school_status_str) and acc.state = 'ACTIVE' GROUP by acc.accountid, accountname ORDER BY this_order ASC";

?>