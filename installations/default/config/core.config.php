<?php
/*installation config*/
/** Site Name */
$siteName="Handsam AET";
$installation_ref = "handsam_aet";

//not in use yet
//$smtpServer = "semantise.com";
//$smtpPort = 25;


/** Email address for Administrative Things */
//admin mail sends password reminders
$adminMail="admin@handsam.org.uk";
//alertsmail sends email notifications
$alertsMail = "alerts@handsam.org.uk";
//logmail gets sent logs
$logMail = "it@handsam.co.uk";
/**  Show Debug Information? */

//upload document categories
$docCategory = array("QGP" => "QGP", "LAW" => "Law &amp; HSE", "USERGUIDE" => "HandSaM User Guide", "RISK_ASSESSMENT" => "Risk Assessment", "GUIDANCE" => "Other Guidance");

$meta_title = "Handsam Health and Safety Management System";
$meta_keywords = "Handsam, Health and Safety, Health and Safety Management";
$meta_description = "Handsam Health and Safety Management System";


//set to true for autologout
$autologout = true;

$debug_logging = false;

$debug_logging_userids = array();
$debug_logging_accountids = array();

//account type arrays
$school_type = array("PRIMARY" => "Primary",
						"JUNIOR" => "Junior",
						"INFANTS" => "Infants",
						"PREP" => "Prep",
						"MIDDLE" => "Middle",
						"SECONDARY" => "Secondary",
						"SECONDARY SIXTH" => "Secondary with Sixth Form",
						"SIXTH FORM" => "Sixth Form College",
						"OFFICE" => "Office",
						"SPECIAL SCHOOL" => "Special School",
						"SPRINGBOARD CENTRE" => "Springboard Centre",
						"FREE SCHOOL" => "Free School",
						"STUDIO SCHOOL" => "Studio School",
						"UNKNOWN" => "Unknown");

//$school_status = array("COMMUNITY" => "Community", "VOLUNTARY CONTROLLED" => "Voluntary Controlled (VC)", "VOLUNTARY AIDED" => "Voluntary Aided (VA)" , "FOUNDATION" => "Foundation", "GRANT MAINTAINED" => "Grant Maintained", "ACADEMY" => "Academy", "INDEPENDENT" => "Independent", "UNKNOWN" => "Unknown");
$school_status = array("AET" => "AET", "LAET" => "LAET");
$school_pupils = array("CO-ED" => "Co-educational", "BOYS" => "Boys", "GIRLS" => "Girls", "UNKNOWN" => "Unknown");

//online courses webapp connection
$online_course_mode = "";


//this is so we can have more than one logo for an installation - so we can group accounts together
$installation_logo_url = array("AET" => "$rootUrl/installations/$installation/images/aet_logo.jpg", "LAET" => "$rootUrl/installations/$installation/images/aet_logo.jpg");

$email_signature = "The Handsam System\r\nThis is an automated message";

$core_conf = array();

//jquery loading page gif appears on all pages
$core_conf['loading_page_img'] = true;

// 2nd account manager field
$core_conf['acc_manager_2'] = FALSE;

// Job Roles functionality
$core_conf['jobroles'] = FALSE;


//allow superAdmins to add accounts as well as handsam superadmins
$core_conf['add_accounts'] = FALSE;


$core_conf['crypt'] = "blowfish";

$core_conf['yes_no_arr'] = array("1" => "Yes", "0" => "No");

//email alert test mode. LIVE = sending all alerts to right users and mark alerts as sent; TEST = all alerts sent to next config option and alerts not marked as sent.
$core_conf['email_alert_mode'] = "LIVE";
$core_conf['email_alert_test_addr'] = "it@handsam.co.uk";

//visit history box on home page
$core_conf['visit_history'] = true;

// premises key holder - emergency contact
$core_conf['key_holder'] = false;

//display an editable box on the logged in home page only
$core_conf['disp_home_box_1'] = false;

//display an editable box  on the logged out and logged in home page
$core_conf['disp_home_box_2'] = false;

//handsam docs url
$core_conf['handsam_docs_url'] = "http://sync.handsam.org.uk/handsam_doc.php";
//$core_conf['handsam_docs_url'] = "http://127.0.0.1/semantise/handsam/handsam_sync/web/handsam_doc.php";

// unread news popup 
$core_conf['unread_news_able'] = true;
$core_conf['unread_news_items'] = 3;
$core_conf['unread_news_must_read'] = false;
$core_conf['unread_news_audit_SA'] = false; // can super admins see the audit
$core_conf['unread_news_audit_account_admin'] = false; //can account admins see who has read what

// key tasks
$core_conf['key_task'] = true;
// key task date lock
$core_conf['key_task_lock'] = true;

// account cluster field
$core_conf['school_cluster'] = true;

// account cluster field restrict access for superadmins who are cluster managers to only that cluster
$core_conf['account_cluster_SA_access'] = true;

// account cluster field , allow handsam super admin users to be cluster managers
$core_conf['account_cluster_handsamSA'] = false;


//dashboard_group_select drop down lists fro filter of dashboard tasklist cols
$core_conf['dashboard_group_select'] = false;

//news menu link
$core_conf['news_menu_link'] = false;

//select users 'associated users' form field validation
$core_conf['select_assoc_users_required'] = array('firstname', 'lastname', 'address1', 'postcode', 'tel1', 'age', 'sex');

//select users  'users' form field validation
$core_conf['select_users_required'] = array('firstname', 'lastname', 'address1', 'postcode', 'tel1', 'age', 'sex');

// use the account's cost code field
$core_conf['cost_code'] = false;

// user's payroll field
$core_conf['user_payroll'] = false;

//account page list filters
$core_conf['account_name_filter'] = true;
$core_conf['account_cluster_filter'] = true;

$core_conf['compliance'] = array();

//remember me cookie and login tickbox
$core_conf['remember_me'] = true;

// unread news popup 
$core_conf['unread_news_able'] = false;
$core_conf['unread_news_items'] = 3;
$core_conf['unread_news_must_read'] = false;
$core_conf['unread_news_audit_SA'] = false; // can super admins see the audit
$core_conf['unread_news_audit_account_admin'] = false; //can account admins see who has read what
