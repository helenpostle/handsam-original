<?php
//define modules available for this installation
$installed_modules = array();
$installed_modules[] = "tasklist";
$installed_modules[] = "lotc";
$installed_modules[] = "accidents";
$installed_modules[] = "training";
$installed_modules[] = "qgp";

$handsam_core_conf = array();

$accidents_conf = array();

//Functionality to tag incident log sites with a predefined tag from the database.
$accidents_conf['site_tags'] = false;
//The site MUST be tagged if this is true
$accidents_conf['site_tags_required'] = false;

//send out incident alerts
$accidents_conf['send_alerts'] = true;

//send out incident alerts
$accidents_conf['investigation_late_min'] = 1;

//investigation issues box
$accidents_conf['investigation_issues_fld'] = true;

//incident form dropdown options:
$accidents_conf['user_status'] = array("STUDENT" => "student", "VISITOR" => "visitor", "EMPLOYEE" => "employee", "SUB-CONTRACTOR" => "sub-contractor", "OTHER" => "other");

$accidents_conf['user_sex'] = array ("MALE" => "male", "FEMALE" => "female");
$accidents_conf['yes_no_arr'] = array("No", "Yes");
$accidents_conf['no_yes_arr'] = array("Yes", "No");
$accidents_conf['riddor'] = array("RIDDOR" => "yes", "NON-RIDDOR" => "No");
// show the huge riddor box when starting a new accident report
$accidents_conf['show_riddor_box'] = true;
$accidents_conf['acc_type'] = array("NEAR MISS" =>"Near miss", "ACCIDENT" => "Accident");

$accidents_conf['illness_type'] = true;

$accidents_conf['restraint_type'] = true;

$accidents_conf['about_accident_arr'] = array(
	"machinery" => "Contact with moving machinery or material being machined", 
	"object" => "Hit by a moving, flying or falling object", 
	"vehicle" => "Hit by a moving vehicle", 
	"hit" => "Hit something fixed or stationary", 
	"handling" => "Injured while handling, lifting or carrying", 
	"slip" => "Slipped, tripped or fell on the same level", 
	"fall" => "Fell from a height",
	"trapped" => "Trapped by something collapsing", 
	"drowned" => "Drowned or asphyxiated", 
	"substance" => "Exposed to, or in contact with, a harmful substance", 
	"fire" => "Exposed to fire", 
	"explosion" => "Exposed to an explosion",
	"electricity" => "Contact with electricity or an electrical discharge", 
	"animal" => "Injured by an animal", 
	"assault" => "Physically assaulted by a person", 
	"sharp work object " => "Injured by sharp object (being used for work at the time)",
	"sharp non-work object" => "Injured by sharp object (not part of work implements at the time)",
	"voice loss" => "Voice loss",
	"stress" => "Stress",
	"rsi" => "RSI",
	"visual impairment" => "Visual Impairment",
	"back pain" => "Back pain",
	"cold/flu" => "Cold/flu symptoms",
	"infectious disease" => "Infectious disease",
	"neurological symptoms" => "Neurological symptoms",
	"viral infection" => "Viral infection",
	"bullying" => "Bullying"
);
//order alphabetically
sort($accidents_conf['about_accident_arr']);
//add to end of alphabetical array
$accidents_conf['about_accident_arr']['other'] = "Other - describe below";

$accidents_conf['injury_Q1'] = array(
	"fatality" => "A fatality",
	"major" => "A major injury or condition",
	"7 day" => "An injury to an employee or self-employed person which prevented them doing their normal work for more than 7 days",
	"hospital" => "An injury to a member of the public which meant they had to be taken from the scene of the incident to a hospital for treatment",
	"none" => "None of the above."
	
);

$accidents_conf['injury_Q2'] = array(
	"unconcious" => "They became unconscious",
	"resuscitation" => "They needed resuscitation",
	"hospitalised" => "They remained in hospital for more than 24 hours",
	"none" => "None of the above."
);

$accidents_conf['injury_part'] = array(
	"Ear - Left" => "Ear - Left", 
	"Ear - Right" => "Ear - Right", 
	"Eye - Left" => "Eye - Left", 
	"Eye - Right" => "Eye - Right", 
	"Nose" => "Nose", 
	"Teeth" => "Teeth",
	"Mouth" => "Mouth",
	"Head" => "Head",
	"Face" => "Face",
	"Neck" => "Neck",
	"Upper Arm - Left" => "Upper Arm - Left",
	"Upper Arm - Right" => "Upper Arm - Right",
	"Elbow - Left" => "Elbow - Left",
	"Elbow - Right" => "Elbow - Right",
	"Lower Arm - Left" => "Lower Arm - Left",
	"Lower Arm - Right" => "Lower Arm - Right",
	"Wrist - Left" => "Wrist - Left",
	"Wrist - Right" => "Wrist - Right",
	"Hand - Left" => "Hand - Left",
	"Hand - Right" => "Hand - Right",
	"Finger - Left" => "Finger(s) - Left",
	"Finger - Right" => "Finger(s) - Right",
	"Thumb - Left" => "Thumb - Left",
	"Thumb - Right" => "Thumb - Right",
	"Shoulder - Left" => "Shoulder - Left",
	"Shoulder - Right" => "Shoulder - Right",
	"Upper Back Area" => "Upper Back Area",
	"Lower Back Area" => "Lower Back Area",
	"Chest" => "Chest (Including Ribs, Sternum, & SoftTissue)",
	"Sacrum and Coccyx" => "Sacrum and Coccyx (Tailbone)",
	"Pelvis" => "Pelvis",
	"Internal Organs" => "Internal Organs",
	"Hip - Left" => "Hip - Left",
	"Hip - Right" => "Hip - Right",
	"Upper Leg - Left" => "Upper Leg - Left",
	"Upper Leg - Right" => "Upper Leg - Right",
	"Knee - Left" => "Knee - Left",
	"Knee - Right" => "Knee - Right",
	"Lower Leg - Left" => "Lower Leg - Left",
	"Lower Leg - Right" => "Lower Leg - Right",
	"Ankle - Left" => "Ankle - Left",
	"Ankle - Right" => "Ankle - Right",
	"Foot - Left" => "Foot - Left",
	"Foot - Right" => "Foot - Right",
	"Toe - Left" => "Toe(s) - Left",
	"Toe - Right" => "Toe(s) - Right",
	"Lungs/Respiratory System" => "Lungs/Respiratory System",
	"Abdomen Including Groin" => "Abdomen Including Groin",
	"Buttocks" => "Buttocks",
);
//order alphabetically
asort($accidents_conf['injury_part']);

$accidents_conf['injury_type'] = array(
	"Amputation" => "Amputation",
    "Bite (skin broken)" => "Bite (skin broken)",
    "Bite Mark" => "Bite Mark",
	"Chest" => "Chest Pain", 
	"Chemical Burn" => "Chemical Burn", 
	"Thermal Burn" => "Thermal Burn",
    "Contact/red mark" => "Contact/red mark",
	"Contusion" => "Contusion (Bruise)", 
	"Dislocation" => "Dislocation", 
	"Electric Shock" => "Electric Shock", 
	"Foreign Body" => "Foreign Body",
	"Fracture" => "Fracture", 
	"Freezing" => "Freezing", 
	"Hearing Impairment" => "Hearing Impairment", 
	"Heat Stress" => "Heat Stress",
	"Inflammation" => "Inflammation",
    "Insect Bite" => "Insect Bite",
	"Laceration" => "Laceration (cut)", 
	"Abrasion" => "Abrasion (Scrape)",
	"Puncture" => "Puncture", 
	"Sprain" => "Sprain", 
	"Strain" => "Strain", 
    "Swelling" => "Swelling",
	"Loss of Consciousness" => "Loss of Consciousness", 
	"Vision Impairment" => "Vision Impairment", 
	"Other Injuries" => "All Other Injuries, Not Otherwise Classified", 
	"Respiratory Irritation or Impairment" => "Respiratory Irritation or Impairment", 
	"Exposure" => "Exposure (Chemical, Radiation, Asbestos, etc)", 
	"Dermatitis/Skin Irritation" => "Dermatitis/Skin Irritation", 
	"Repetitive Strain Injury" => "Repetitive Strain Injury"
);
asort($accidents_conf['injury_type']);
//add to end of alphabetical array
$accidents_conf['injury_type']['Other Occupational Illnesses'] = "All Other Occupational Illnesses, Not Otherwise Classified";

$accidents_conf['investigation_optional'] = true;

$training_conf = array();

//adds the usertype 'training staff' to all users in this account when the first licence is initially supplied. Also selects training staff as user type by default for new users on users.php page if their account has this licence.
$training_conf['new_user_training_staff'] = true;

$training_conf['ext_app_courses_url'] = 'http://handsam.etrainingcentre.org';
$training_conf['ext_app_courses'] = "http://handsam.etrainingcentre.org/api/modules/all.json";
$training_conf['ext_app_course_def_reminder'] = '1 month';
$training_conf['ext_app_course_def_renewal'] = '1 year';

//assign training matrix page link
$training_conf['training_multi_assign'] = true;

//new global training providers func
$training_conf['global_provider'] = false; //global_provider && global_location must both be set to true to appear in multi assign form

//new global training location func
$training_conf['global_location'] = false; //global_provider && global_location must both be set to true to appear in multi assign form

//new contractor qualifications func
$training_conf['contractor_quals'] = false;

//whether to display system course files to users
$training_conf['display_system_course_files'] = false;

//new editable course type 
$training_conf['course_type_2'] = false;

//new manage account courses form
$training_conf['manage_account_courses'] = false;

//whether to display account course files to users
$training_conf['display_account_course_files'] = false;


$tasklist_conf = array();

//collapse the resources download text on the mytasks.php page.
$tasklist_conf['collapse_mytask_resources'] = false;

//collapse the resources download text on the tasks.php page.
$tasklist_conf['collapse_managetask_resources'] = true;

//late task box on home page.
$tasklist_conf['home_page_late_task_alert'] = true;

//box.editedTaskAlert.php enabled or not on user home page
$tasklist_conf['home_page_edited_task_alert'] = true;

// email the assigned user when a task 
$tasklist_conf['task_email_user'] = true;
$tasklist_conf['task_email_user_default_on'] = true;

// ability to create repeat tasks
$tasklist_conf['create_repeat_tasks'] = false;

// ability to create repeat accounttasks
$tasklist_conf['create_repeat_accounttasks'] = true;

//allow task category to be assigned to group on edit page
$tasklist_conf['category_group_assign'] = true;

//allow tasktext to be searched on tasks.php
$tasklist_conf['task_search_box'] = true;

//allow tasktext to be searched on mytasks.php
$tasklist_conf['mytask_search_box'] = true;

//allow actiondate to be set outside licence
$tasklist_conf['date_outside_licence'] = false;
$tasklist_conf['save_outside_licence'] = false;
$tasklist_conf['new_task_date_filter'] = false;


$qgp_conf = array();

$lotc_conf = array();

$lotc_conf['finance_approval_field'] = true;
$lotc_conf['finance_approval_field_name'] = 'Finance User Approval';
$lotc_conf['principal_approval_field'] = true;
$lotc_conf['principal_approval_field_name'] = 'Principal User Approval';

$lotc_conf['deputy_principal_approval_field'] = true;
$lotc_conf['deputy_principal_approval_field_name'] = 'Deputy Principal User Approval';

$lotc_conf['governor_approval_field'] = true;
$lotc_conf['governor_approval_field_name'] = 'Governor User Approval';


$lotc_conf['evc_no_alerts_usr'] = true;
$lotc_conf['finance_usr'] = true;
$lotc_conf['principal_usr'] = true;
$lotc_conf['deputy_principal_usr'] = true;
$lotc_conf['governor_usr'] = true;


$library_conf = array();
$library_conf['file_ref'] = false;

// documents module
$documents_conf['task_costcode_filter'] = false;
$documents_conf['task_cat_filter'] = false;
$documents_conf['module_filter'] = true;
$documents_conf['author_filter'] = true;
$documents_conf['file_upload_location'] = false;
$documents_conf['file_upload_year'] = false;



