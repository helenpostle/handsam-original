<?php
/**
* processes content/admin.editcontent.php form info for updating content
*/
//include basic stuff
//make this a secure page
$secure = true;
require_once("shared/startPage.php");


$pageFiles->addFunc('content');


$pageFiles->includePhp();

if (isAllowed("editContent")) {

	$contentid = getIntFromRequest("id");
	$parentid = getIntFromRequest("parentid");
	$page = getIntFromRequest("page");
	$rsContent = getRS($con,"select * from content where contentid = $contentid");
	$rowContent = getRow($rsContent); 
    if ($rowContent['mid'] > 0) headerLocation(formReturn($con, $contentid, $parentid, "page=".$page), $messages); 
	if (buttonClicked("cancel")) {
		#return to pre edit page
		if ($rowContent['box_parent'] != "") {
			headerLocation($rootUrl."/".$rowContent['box_parent'], $messages);
		} else {
			//simplify this? make it more handsam specific.
			#return to pre edit page
			headerLocation(formReturn($con, $contentid, $parentid, "page=".$page), $messages);
		}
	} else {
		if ($contentid !== null) {
			
			#display edit form
			$content = "admin.editcontent.php";
			if (buttonClicked("save")) {
				if ($contentid == 0) {
						$qry = new dbInsert("content");
				} else {
					$qry = new dbUpdate("content");
					$qry->setParam("contentid",$contentid);
				}
				if (isset($qry)) {
					$qry->setReqStringVal("title",$_POST["title"],"Content Title");
					$qry->setReqHtmlVal("contenttext",$_POST["contenttext"],"Content Text");
					$qry->setReqStringVal("state",$_POST["state"],"state");
					$qry->setStringVal("meta_title",$_POST["meta_title"],"Meta Title");
					$qry->setStringVal("meta_keywords",$_POST["meta_keywords"],"Meta Keywords");
					$qry->setStringVal("meta_desc",$_POST["meta_desc"],"Meta Description");
					if ($parentid != "") {
						$qry->setNumberVal("parentid", $parentid, "parent id");
					}
					$qry->setAudit($clsUS->userid);
					
					//echo "SQL: ".$qry->getSQL();
					
					if ($qry->execute($con)) {
						if ($contentid == 0) {
							$contentid = $qry->getNewID();	
						}
						$messages[] = SAVED;
						
						if ($rowContent['box_parent'] != "") {
							headerLocation($rootUrl."/".$rowContent['box_parent'], $messages);
						} else {
							//simplify this? make it more handsam specific.
							#return to pre edit page
							headerLocation(formReturn($con, $contentid, $parentid, "page=".$page), $messages);
						}
					} else {
						$messages[] = $qry->getError();	
					}
				}
			} else if (buttonClicked("delete")) {
				$qry = new dbUpdate("content");
				$qry->setParam("contentid",$contentid);
				$qry->setReqStringVal("state","DELETED","State");
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#change content to list
					$messages[] = DELETED;
					#return to pre edit page
					$contentid = $parentid;
					if ($rowContent['box_parent'] != "") {
						headerLocation($rootUrl."/".$rowContent['box_parent'], $messages);
					} else {
						//simplify this? make it more handsam specific.
						#return to pre edit page
						headerLocation(formReturn($con, $contentid, $parentid, "page=".$page), $messages);
					}
				}
			
			}

		 } else {
			#return to pre edit page
			//headerLocation(formReturn($con, $contentid, $_POST['parentid']), false);
		 }
	 }

	
} else {
	trigger_error("Access Denied",E_USER_WARNING);

}
//include layout.
include ("layout.php");

?>