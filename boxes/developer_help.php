<?php 
/**
 * Standard index page box - used in website resources page
 */
boxTop("Developer Help");

$parser = new UAParser;
$result = $parser->parse($_SERVER['HTTP_USER_AGENT']);

?>
<script>
	$(function(){
		$('#js_check').html('on');
		});
</script>

<p>If you ever have trouble with this system, you may be asked to give these details:</p>

<p>Operating System: <strong><?php echo $result->os->toString; ?></strong></p>
<p>Browser: <strong><?php echo $result->ua->toString;   ?></strong></p>
<p>Javascript is turned <strong id="js_check">off</strong>.</p>