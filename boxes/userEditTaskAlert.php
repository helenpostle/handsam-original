<?php 
/**
 * Handsam task changes - used in index page
 */
 	$taskState = array("NEW" => "New Tasks", "EDITED" => "Edited Tasks", "DELETED1" => "Deleted Tasks");

 
 	$qry = "SELECT * FROM  accounttask WHERE accountid = $accountid AND state = 'EDITED' and assignedto = '$userid' ORDER BY edited desc";
	$result = getRS($con, $qry);
	if (getResultRows($result) > 0) {

		boxTop("Recently Edited");
		echo "<p class=\"info\">The details of these tasks which are assigned to you have recently been edited.</p>";		
		echo "<div class=\"vertical_nav\">\n";
		$last_state = "";
		while ($row = getRow($result)) {
			if ($row['state'] != $last_state) {
				
				if ($last_state != "") {
					echo "</ul>\n";
				}
				echo "<h3>".$taskState[$row['state']]."</h3>\n";
				echo "<ul>\n";

			}
			$last_state = $row['state'];
			echo "<li><a href=\"$rootUrl/modules/tasklist/mytasks.php?accounttaskid=".$row['accounttaskid']."&return=index.php\">".textSummary($row['tasktext'], 20)."</a></li>\n";
	
		}
		echo "</ul>\n";
		echo "</div>\n";	
	
		boxBottom();
	}
?>