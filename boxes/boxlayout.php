<?php 
/**
*template for the layout of boxes
*/

/**
 * set the box title layout
 */
function boxTop($title, $class="") { 
	echo "<div class=\"box clearfix $class\">";
	echo "<h2>$title</h2>";
} 

function lotcBoxTop($title) { 
	echo "<div class=\"lotc_box_header clearfix \">";
	echo "<h2>$title</h2>";
	echo "</div>";
	echo "<div class=\"lotc_box clearfix\">";

} 

function accidentBoxTop($title) { 
	echo "<div class=\"acc_box_header clearfix \">";
	echo "<h2>$title</h2>";
	echo "</div>";
	echo "<div class=\"acc_box clearfix\">";

} 

function trainingBoxTop($title) { 
	echo "<div class=\"trn_box_header clearfix \">";
	echo "<h2>$title</h2>";
	echo "</div>";
	echo "<div class=\"trn_box clearfix\">";

} 

/**
 * layout for the bottom of the box
 */
function boxBottom() { 
	echo "</div>";
}


function lotcBoxBottom($text="") { 
	echo "</div>";
	echo "<div class=\"lotc_box_footer\">$text</div>";
}

function accidentBoxBottom($text="") { 
	echo "</div>";
	echo "<div class=\"acc_box_footer\">$text</div>";
}

function trainingBoxBottom($text="") { 
	echo "</div>";
	echo "<div class=\"trn_box_footer\">$text</div>";
}

?>