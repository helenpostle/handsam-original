<?php 
/**
 * Standard index page box - used in website resources page
 */
	boxTop("Downloads &amp; Links");
?>
	<p class="info">Here are some useful files to download:</p>
	<?php
	#list files 
	if (isAllowed("editContent")) {
		echo "<p><a href = \"#\" class=\"iconAddDocument\" onclick='openWin(\"$rootUrl/handsam_core/docList.php?filetype=handsamDoc&amp;edit=page&amp;contentid=".$contentid."\",640,800,\"fileBrowser\")'>Attach a document to this page</a>\n</p>";
	}

	$qryDocs = "SELECT content_files.pagefileid, files.fileid, files.description, files.title, files.filecategory as cat_id FROM files left join content_files on files.fileid = content_files.fileid WHERE content_files.contentid = $contentid order by files.filecategory asc;";
	$rsDocs = getRS($con, $qryDocs);
	
	
	$docCat = "SAUSAGES";
	$ulClose = false;
	while ($rowDocs = getRow($rsDocs)) {
		if ($rowDocs['cat_id'] != $docCat) {
			if ($ulClose) {
				echo "</ul>\n";
			}
			if ($rowDocs['cat_id'] != "") {
				echo "<h3 class=\"docList\">".$docCategory[$rowDocs['cat_id']]."</h3>";
			}
			echo "<ul class=\"docList\">\n";
			$ulClose = true;
		
		}
		$docCat = $rowDocs['cat_id'];
		echo "<li><a href=\"$rootUrl/handsam_core/downloadDocument.php?id=".$rowDocs['fileid']."\">".$rowDocs['title']."</a>";
		if (trim($rowDocs['description']) != "") {
			echo displayText("&nbsp;~&nbsp;".$rowDocs['description']);
		}
		if (isAllowed("editContent")) {
			echo "<img onclick=\"removeItem('".$rootUrl."', ".$rowDocs['pagefileid'].", 'file', ".$contentid.");\" src=\"images/delete.png\" alt=\"remove file from page\"/>";
		}
		echo "</li>";
	}
	echo "</ul>\n";			
			
	?>
	<p class="info">Here are some useful Links:</p>

	<?php		
	#list links
	if (isAllowed("editContent")) {
		echo "<p><a href = \"#\" class=\"iconAddLink\" onclick='openWin(\"$rootUrl/handsam_core/linksList.php?filetype=handsamDoc&amp;edit=page&amp;contentid=".$contentid."\",640,800,\"fileBrowser\")'>Attach a link to this page</a>\n</p>";
	}
			
	$qryLinks = "SELECT content_links.pagelinkid, links.linkid, links.url, links.description, links.title, links.linkcategory as cat_id FROM links left join content_links on links.linkid = content_links.linkid WHERE content_links.contentid = $contentid order by links.linkcategory asc;";
	$rsLinks = getRS($con, $qryLinks);
	
	
	$linkCat = "SAUSAGES";
	$ulClose = false;
	while ($rowLinks = getRow($rsLinks)) {
		if ($rowLinks['cat_id'] != $linkCat) {
			if ($ulClose) {
				echo "</ul>\n";
			}
			if ($rowLinks['cat_id'] != "") {
				echo "<h3 class=\"docsList\">".$docCategory[$rowLinks['cat_id']]."</h3>";
			}
			echo "<ul class=\"linkList\">\n";
			$ulClose = true;
		
		}
		$linkCat = $rowLinks['cat_id'];
		echo "<li><a href=\"".$rowLinks['url']."\">".$rowLinks['title']."</a>";
		if (trim($rowLinks['description']) != "") {
			echo displayText("&nbsp;~&nbsp;".$rowLinks['description']);
		}
		if (isAllowed("editContent")) {
			echo "<img onclick=\"removeItem('".$rootUrl."', ".$rowLinks['pagelinkid'].", 'link', ".$contentid.");\" src=\"images/delete.png\" alt=\"remove file from page\"/>";
		}
		echo "</li>";
	}
	echo "</ul>\n";			
	echo addCsrfField();
	boxBottom();
?>
