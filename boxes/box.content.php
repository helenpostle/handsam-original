<?php
/**
 * Shows content in a box
*/
while ($rowboxContent = getRow($rsBoxContent)) {
	boxTop($rowboxContent['title']);
	echo "<div class=\"boxContent\">\n";
	echo displayEdit($rowboxContent);
	echo displayText($rowboxContent['contenttext']);
	echo "</div>";
	boxBottom();
} 

?>