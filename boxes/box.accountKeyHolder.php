<?php 
/**
 * list of users in this account.
 */
$qry = "SELECT usr.lastname, usr.firstname, usr.userid, usr.username, usr.tel1 FROM usr where accountid = ".$clsUS->accountid."  and usr.state = 'ACTIVE' and key_holder=1  order by usr.lastname asc";
$result = getRS($con, $qry);
if (getResultRows($result)>0) {
	boxTop("Emergency Contacts");
	?>
	<table id="user_list" class="boxList">
		<caption class="hidden">Emergency Contact Users</caption>
		<thead>
			<tr>
				<th class="username"> Firstname</th>
				<th class="state">Lastname</th>
				<th class="state">Tel</th>
			</tr>
		</thead>
		<tbody>

	<?php
	$i = 0;
	$last_usr = array();
	while ($row = getRow($result)) {
		$open_lt = "<a class=\"\" title=\"".$row["username"]."\" href=\"handsam_core/users.php?userid=".$row["userid"]."\">";
		?>
		<tr>
			<td class="username"><?php echo $open_lt.textSummary($row["username"],10); ?></a></td>
			<td class="state"><?php echo $open_lt.displayText($row["lastname"]) ?></a></td>
			<td class="state"><?php echo $open_lt.displayText($row["tel1"]) ?></a></td>
		</tr>
	<?php 

	} ?>
		</tbody>
		<tfoot>
			<tr>
				<td colspan="3"></td>
			</tr>
		</tfoot>
	</table>



	<?php
		boxBottom();
}
?>
	