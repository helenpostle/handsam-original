<?php

/*
 * the news page admin menu
 * 
 */

if (isAllowed("editContent")) {

	boxTop("$news_title Admin Menu");
	?>
	<ul>
	<?php 
	##### Add blog entry #####
	if (isset($blogid) && ($blogid > 0 )) {?>
		<li><a class="iconAdd" href="<?php echo "$rootUrl/admin.editcontent.php?id=0&amp;parentid=$blogid";?>" title="add an item">Add <?php echo $news_title;?> Item</a></li>
		<?php
	}
    
    boxBottom();
}
?>
