<?php 
/**
 * shows licences ending in next 28 days - used in index page
 */
if(isAllowed("editAccount")) {
	$qry = "SELECT accountlicence.*, account.accountname FROM accountlicence LEFT JOIN account ON accountlicence.accountid = account.accountid where end_date < DATE_ADD(Now(), INTERVAL 28 DAY) and end_date > NOW()  order by end_date asc";
	$result = getRS($con, $qry);
	if (getResultRows($result) > 0) {
		boxTop("Licences expiring within 28 days");
		?>
		<p class="info">Select a licence to view that account.</p>
		<table class="boxList">
			<caption class="hidden"><?php echo $title;?> Licences Expiring within 28 days</caption>
			<thead>
				<tr>
					<th class="state nolink">Account</th>
					<th class="licence nolink">End</th>
				</tr>
			</thead>
			<tbody>
				<?php
				while ($row = getRow($result)) {
						?>
						<tr>
							<td class="licence"><a href="<?php echo $rootUrl;?>/handsam_core/accounts.php?accountid=<?php echo $row["accountid"]; ?>"><?php echo textSummary($row['accountname'],28); ?></a></td>
							<td class="licence"><a href="<?php echo $rootUrl;?>/handsam_core/accounts.php?accountid=<?php echo $row["accountid"]; ?>"><?php echo displaySmallDate($row['end_date']); ?></a></td>
						</tr>
					<?php 
		
				}
				?> 
			</tbody>
		</table>
		<?php
			boxBottom();
	}
}
?>
	