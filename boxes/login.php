<?php 
/**
 * Standard index page box
 */
  
	boxTop("LOGIN");
?>
	<p class="info">Please logon to use this section of the site. By ticking 'remember me', 
your username and password will be remembered on this computer, to save you logging on in future - 
DO NOT tick this box if this is a shared computer!</p>
<p><a href="<?php echo $rootUrl;?>/forgot.php">[ Click here if you have forgotten your password ]</a></p>

<?php
	boxBottom();
?>
	