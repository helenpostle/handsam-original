<?php

/**
 * Standard index page box
 */
boxTop("Browser compatibility");
?>

<p><img src="images/firefox.gif" alt=""/> <a href="http://www.mozilla.com/en-US/firefox/landing/better/" title="download Firefox here - its free!">Mozilla Firefox</a></p>

<p><img src="images/chrome.gif" width="32" height="32" alt="Google Chrome Browser" /> <a href="http://www.google.com/intl/en_uk/chrome/browser/">Google Chrome</a></p>

<p><img src="images/safari.gif" alt=""/> Safari</p>

<p><img src="images/ie.gif" alt=""/> Internet Explorer versions 7 &ndash; 10 (Windows only)</p>


<p>Javascript must be enabled for full system functionality. </p>

<?php boxBottom(); ?>
