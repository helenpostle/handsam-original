<?php
/**
 * Shows list of forms for download in a box
*/	
$blog_nav = "";
if (getResultRows($rsBlogSideNav) > 0) {
	
	$blog_nav = "<ul class=\"all\">";
	$blog_nav .= "<li class=\"yr\">";
	$blog_month = "";
	$blog_yr = 0;
	$cnt = 0;
	while ($rowBlogNav = getRow($rsBlogSideNav)) {
		$new_blog_month = date('F', strtotime($rowBlogNav['created']));
		$mon = date('n', strtotime($rowBlogNav['created']));;
		$new_blog_yr = date('Y', strtotime($rowBlogNav['created']));
		if ($new_blog_yr != $blog_yr) {
			if ($cnt > 0) {
				$blog_nav .= "</ul></li></ul></li><li class=\"yr\"><a href=\"".displayLink("news", 0, "y=$new_blog_yr", $rowBlogNav['title'])."\">$new_blog_yr</a><ul><li class=\"mon\"><a href=\"".displayLink("news", 0, "y=$new_blog_yr&amp;m=$mon", "All $new_blog_yr news items")."\">$new_blog_month</a><ul>";
			} else {
				$blog_nav .= "<a href=\"".displayLink("news", 0, "y=$new_blog_yr", "All $new_blog_yr news items")."\">$new_blog_yr</a><ul><li class=\"mon\"><a href=\"".displayLink("news", 0, "y=$new_blog_yr&amp;m=$mon", "All $new_blog_yr news items")."\">$new_blog_month</a><ul>";
			}			
		} else if ($new_blog_month != $blog_month){
			if ($cnt > 0) {
				$blog_nav .= "</ul></li><li class=\"mon\"><a href=\"".displayLink("news", 0, "y=$new_blog_month&amp;m=$mon", "All $new_blog_yr news items")."\">$new_blog_month</a><ul>";
			}
		}
		
		$blog_nav .= "<li class=\"\"><a href=\"".displayLink("news", $rowBlogNav['contentid'], "", $rowBlogNav['title'])."\">".$rowBlogNav['title']."</a></li>";
		
		$cnt++;
		$blog_month = $new_blog_month;
		$blog_yr = $new_blog_yr;
	}
	$blog_nav .= "</ul></li></ul></li></ul>";
}

if ($blog_nav != "") {
	boxTop($news_title);
	echo $blog_nav;
	boxBottom();
}
?>