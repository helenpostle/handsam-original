<?php
/**
* Standard Index Page
*/
$this_page = "index.php";
 
$content = "";
//include basic stuff
//make this a secure page
$secure = false;
include ("shared/startPage.php");
$pageFiles->addFunc('content');

//comment out for demo
if (isLoggedIn() && isAllowed("viewDashboard")) {
	$homepage = getIntFromRequest("homepage");
    $urn = '';
    if ($unreadnews) $urn = '?urn=1';
	if ($homepage !== 1) header("Location: $rootUrl/handsam_core/dashboard.php".$urn);
	//header("Location: http://www.handsam.co.uk");
}

$pageFiles->addFunc('history');
$pageFiles->includePhp();

$title = "";

if (isLoggedIn() && isAllowed('editAccount') || !isLoggedIn()) {

	$qry = "SELECT content.*, usr.username FROM content inner join usr on content.created_by = usr.userid where content.state = 'PUBLISHED'  and superpage = 'index' order by content.created desc";
	
	$rsContent = getRS($con, $qry);
	$rowContent = getRow($rsContent);
	if ($rowContent['meta_title']) {
		$meta_title = displayText($rowContent['meta_title']);
	}
	if ($rowContent['meta_keywords']) {
		$meta_keywords = displayText($rowContent['meta_keywords']);
	}
	if ($rowContent['meta_desc']) {
		$meta_description = displayText($rowContent['meta_desc']);
	}
	$fertile = $rowContent['fertile'];
	$contentid = $rowContent['contentid'];
	$superpage = $rowContent['superpage'];
	//$title = $rowContent['title']; 

	resetRS($rsContent);
	$content = "page.content.php";
	
	//new hot topics box
	if ($core_conf['disp_home_box_1'] || $core_conf['disp_home_box_2']) {
		//get box content
		if ($core_conf['disp_home_box_1']) $qry_arr[] = "'homebox1'";
		if ($core_conf['disp_home_box_2'] && isLoggedIn()) {	
			$qry_arr[] = "'homebox2'";
			$body_class = "home";
		}
		
		$qry = "select * from content where superpage in (".implode(',',$qry_arr).") and state = 'PUBLISHED'";
		$rsBoxContent = getRS($con, $qry);
		if (getResultRows($rsBoxContent) > 0) $box[] = "box.content.php";
	}
	
	if(isAllowed('editAccount'))  {
		$box[]="admin.licence-expire.list.php";
		$box[]="admin.licence-expired.list.php";
		$box[]="lastvisit.php";
	} else {
		$box[] = "browsers.php";
	}	
	//if (isAllowed("viewOurlicences")) {
	//	$accountid = $clsUS->accountid;
	//	$box[] = "accountlicenceList.php";
	//}
    if (isAllowed('editAccount')) {
        foreach ($installed_modules as $mod) {
            if ($mod = 'tasklist') require_once("$rootPath/modules/$mod/shared/index_inc.php");	
        }
    }
    
}

if (!isAllowed('editAccount' ) && isLoggedIn()) {
    $content = "index_modules.php"; //this is the default page, but can be changed in the modules shared/index_inc.php files if needed.
	if ($core_conf['disp_home_box_1'] || $core_conf['disp_home_box_2']) {
		//get box content
		if ($core_conf['disp_home_box_1']) $qry_arr[] = "'homebox1'";
		if ($core_conf['disp_home_box_2']  && isAllowed('editOurTasks')) {
            $qry_arr[] = "'homebox2'";
            $body_class = "home";
        }
		
		$qry = "select * from content where superpage in (".implode(',',$qry_arr).") and state = 'PUBLISHED'";
		$rsBoxContent = getRS($con, $qry);
		if (getResultRows($rsBoxContent) > 0) $box[] = "box.content.php";
	}
    if ($core_conf['key_holder']) $box[] = "box.accountKeyHolder.php";
    
	if($core_conf['visit_history']) $box[]="lastvisit.php";
	foreach ($installed_modules as $mod) {
		require_once("$rootPath/modules/$mod/shared/index_inc.php");
	}
}

//include layout.
include ("layout.php");

?>