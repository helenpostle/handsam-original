<?php
/**
* training log editing functionality
*
*/


 
 ini_set("display_errors", 1); 
 error_reporting(E_ALL);

 $error = error_get_last();
 if ($error) {
 var_dump($error);
}


/*
flags in the table
0 = record not updated
1 = record in training log updated
2 = user not found in user table
3 = course not found in training_course
*/

require("../../shared/startPage.php");

$pageFiles->includePhp();
$sql = "select score, uid, pass_score, shortname, firstname, lastname, a.accountid, tc.course_id"; 
$sql .= " from e_training_log tl join account a on a.accountname = tl.account ";
$sql .= "join training_course tc on tl.shortname = tc.module where tl.updated = 0 ";
$sql .= " order by account, firstname, lastname, shortname;";
echo $sql;

$result = mysql_query($sql ,$con);

$num_rows = mysql_num_rows($result);
echo "<br />there are ".$num_rows. " rows to process";


while ($row = mysql_fetch_array($result)){
 // var_dump($row);
  $firstname = htmlspecialchars($row['firstname']);
  $lastname = htmlspecialchars($row['lastname']);
 // echo "<br />looking for the user id for ".$firstname. " ".$lastname;
  //get the id for this user
  $user_sql = 'select userid from usr where firstname = "'.htmlspecialchars($row['firstname']).'" and lastname = "'.htmlspecialchars($row['lastname']).'" and accountid = '.$row['accountid'];
  //echo "<br />".$user_sql;
  $user_result = mysql_query($user_sql, $con);
  if (mysql_num_rows($user_result) < 1){
	 //couldn't find the user, set flag to 2
	 echo "<br />couldnt find the user id for ".htmlspecialchars($row['firstname']).'" and lastname = "'.htmlspecialchars($row['lastname']);
	 update_training_log(htmlspecialchars($row['firstname']), htmlspecialchars($row['lastname']), $row['shortname'], 2);
  } else {
     //we did find the user
	 $userRow = mysql_fetch_array($user_result);
	// echo "<br />found the user id for ".htmlspecialchars($row['firstname']).'" and lastname = "'.htmlspecialchars($row['lastname']);
	$course_sql = "select * from training_log where trainee_id = ".$userRow['userid']." and course_id = ".$row['course_id']." and state = 'IN PROGRESS'";
	echo "<br />".$course_sql;
	$course_result = mysql_query($course_sql, $con);
	if (mysql_num_rows($course_result) > 0){
	  echo "<br />found a training log entry that needs updating";
	  //this user has a training record that has not been updated
	  $certificate = "http://handsam.etrainingcentre.org/certificate/".$row['uid']."/".$row['shortname'];
		   $up_sql = "update training_log set state = 'COMPLETE', pass = 1, score = ".$row['score'].", max_score = ".$row['pass_score'];
		   $up_sql .= ", certificate = '".$certificate;
		   $up_sql .= "' where trainee_id = ".$userRow['userid'];
		   $up_sql .= " and accountid = ".$row['accountid']." and course_id = ".$row['course_id'].";";
		   echo "<br />".$up_sql;
	} else {
	  //the course has already been updated, set the flag in e training log
	  echo "<br />this row in training_log has already been updated";
	  $sql = 'update e_training_log set updated = 1 where firstname = "'.$firstname. '" and lastname = "'.$lastname.'" and shortname = "'.$row['shortname'].'"';
      executeSql($sql);
	  echo "<br />".$sql;
	  //update_training_log(htmlspecialchars($row['firstname']), htmlspecialchars($row['lastname']), $row['shortname'], 1);
	}
  }
  
}  //end of loop through rows
 
function update_training_log($firstname, $lastname, $shortname, $updated){
   $sql = 'update e_training_log set updated = ';
   $sql = $updated.' where firstname "'.$firstname. '" and lastname = "'.$lastname.'" and shortname = "'.$shortname.'"';
   executeSql($sql);
   
}


//$pageVars = new loadVars($clsUS, $con);

echo "<p></p><p></p>This is the update the training log page<br /><br />";
   
   

?>