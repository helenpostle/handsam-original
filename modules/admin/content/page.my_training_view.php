<?php
echo startFormTable($_SERVER["PHP_SELF"],"clearfix", "", "Training Log Item", "");
echo frmHiddenField($pageVars->training_id,"training_id");
echo frmHiddenField($pageVars->accountid,"accountid");
echo frmHiddenField(htmlspecialchars(serialize($qs_base)),"filter");

//Personal info
echo "<div id=\"trainee\" class=\"frmSection clearfix\">";
echo "<h3>Part 1: Trainee Details</h3>";
	//display select box of victims
		echo "<div id=\"trainee_detail\">";
			echo "<div class=\"col\">";
				echo frmTextAreaNonEdit($formDB,"firstname","First Name");
				echo frmTextAreaNonEdit($formDB,"lastname","Last Name");
				echo frmTextAreaNonEdit($formDB,"email","Email");
			echo "</div>";
			echo "<div class=\"col\">";
				echo frmTextAreaNonEdit($formDB,"job_title","Job Title");
				echo frmTextAreaNonEdit($formDB,"job_desc","Job Description");
			echo "</div>";
		echo "</div>";
echo "</div>";

//training course details
echo "<div id=\"trainingCourseDetails\" class=\"frmSection\">";
echo "<h3>Part 2: Training Course Details</h3>";
	echo "<div id=\"course_detail_view\" class=\"clearfix\">";
		echo "<div class=\"col\">";
			echo frmTextAreaNonEdit($formDB, "title","Course Title");
			echo frmTextAreaNonEditArray($formDB,"course_type","Course type", $trn_type_arr);
			echo frmTextAreaNonEdit($formDB,"qual_title","Qualification");
			echo frmTextAreaNonEdit($formDB,"qual_desc","Qualification description");
		echo "</div>";		
		echo "<div class=\"col\">";
				
			echo frmTextAreaNonEdit($formDB,"renewal","Full renewal period");
			echo frmTextAreaNonEdit($formDB,"renewal_reminder", "Reminder period");
		echo "</div>";
	echo "</div>";
	echo displayParagraphs("You have passed this course with a score of: ".$formDB['score']."/".$formDB['max_score'].". Well done!"  ,'frm_info clearfix');
	echo displayParagraphs("<a target=\"_blank\" href=\"".$formDB['certificate']."\">View the course certificate here.</a>"  ,'frm_info clearfix');
	//echo displayParagraphs("Your most recent online score for this course was: ".$formDB['score']."/".$formDB['max_score'].""  ,'frm_info clearfix');
	//echo displayParagraphs("<a class=\"online_course\" href=\"$rootUrl/modules/training/online_course.php?training_id={$pageVars->training_id}\">Click here to proceed to the online course: {$formDB['title']}</a>"  ,'frm_info clearfix');
	
echo "</div>";

//training course details
echo "<div id=\"trainingProviderDetails\" class=\"frmSection\">";
echo "<h3>Part 3: Training Provider Details</h3>";
	echo "<div id=\"provider_detail_view\" class=\"clearfix\">";
		if ($formDB['provider_id'] > 0) {
			echo "<div class=\"col\">";
				echo frmTextAreaNonEdit($formDB,"provider_name","Provider name");
				echo frmTextAreaNonEdit($formDB,"provider_address1","Address 1");
				echo frmTextAreaNonEdit($formDB,"provider_address2","Address 2");
				echo frmTextAreaNonEdit($formDB,"provider_address3","Address 3");
				echo frmTextAreaNonEdit($formDB,"provider_city","City");
			echo "</div>";		
			echo "<div class=\"col\">";	
				echo frmTextAreaNonEdit($formDB,"provider_postcode","Postcode");
				echo frmTextAreaNonEdit($formDB,"provider_tel1","Tel 1");
				echo frmTextAreaNonEdit($formDB,"provider_ref","Site Ref");
				echo frmTextAreaNonEdit($formDB,"provider_email","Email");
				echo frmTextAreaNonEdit($formDB,"provider_web","Website");
			echo "</div>";
			echo frmTextAreaNonEdit($formDB,"provider_comments","Comments", "full");
		} else {
			echo "<div class=\"frm_row clearfix \"><span class=\"label\">Provider name</span><span class=\"value\"><span id=\"provider_name\" class=\"provider_name\">Handsam Online Course</span></span></div>";
		}
		
	echo "</div>";
echo "</div>";




echo "<div id=\"specificDetails\" class=\"frmSection\">";
	echo "<h3>Part 4: Specific Details</h3>";
	echo frmDateTimeNonEditFormat($formDB,"course_date","Course Date", "%a, %d %b %Y");
	echo frmDateTimeNonEditFormat($formDB, "renewal_date", "Renewal date", "%a, %d %b %Y");
	echo frmDateTimeNonEditFormat($formDB, "renewal_reminder_date", "Renewal reminder", "%a, %d %b %Y");
	
	echo frmTextAreaNonEdit($formDB,"cost","Cost(GBP)");
	echo frmTextAreaNonEdit($formDB, "comments","Comments");
echo "</div>";

echo "<div id=\"fileUploads\" class=\"frmSection\">";
	echo "<h3>Part 5: Uploaded Files</h3>";	
	$uploads = getUploads($con, $pageVars);
	echo "<div id=\"uploads_list\">";
		if ($uploads->cnt > 0) {
			echo "<ul>";
			foreach ($uploads->rows as $row) {
				echo "<li><a class=\"".strtolower(substr(strrchr($row['filename'],'.'),1))."\" href=\"$rootUrl/modules/training/downloadTrnDocument.php?id={$row['fileid']}\">{$row['title']}</a></li>";
			}
			echo "<ul>";
		} else {
			echo "<p>No Uploaded Files</p>";
		}
	echo "</div>";
echo "</div>";

echo frmButtonHelp("Return","return", "Click here to return to the Training Log");

echo endFormTable();

?>