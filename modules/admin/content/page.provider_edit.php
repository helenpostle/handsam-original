<?php
echo startFormTable($_SERVER["PHP_SELF"],"clearfix", "", "Training Log", "Please edit the details of the course provider below.  Don't forget to click the 'save' button to save your changes");

echo "<div id=\"providerDetails\" class=\"frmSection clearfix\">";
echo "<h3>Edit Provider Details</h3>";
echo frmHiddenField($pageVars->provider_id,"provider_id");

echo "<div class=\"col\">";
	echo frmTextField($formDB->row,"provider_name",100,"Provider Name",true);
	echo frmTextField($formDB->row,"provider_address1",100,"Address 1",false);
	echo frmTextField($formDB->row,"provider_address2",100,"Address 2",false);
	echo frmTextField($formDB->row,"provider_address3",100,"Address 3",false);
	echo frmTextField($formDB->row,"provider_city",100,"City",false);
echo "</div>";
echo "<div class=\"col\">";
	echo frmTextField($formDB->row,"provider_postcode",100,"Postcode",false);
	echo frmTextField($formDB->row,"provider_tel1",100,"Telephone",true);
	echo frmTextField($formDB->row,"provider_ref",100,"Reference",false);
	echo frmTextField($formDB->row,"provider_email",100,"Email",true);
	echo frmTextField($formDB->row,"provider_web",100,"Website",false);
echo "</div>";
echo frmTextArea($formDB->row,"provider_comments",5,"Comments",false, "full");
echo "</div>";

echo frmButtonHelp("Save","save","Click here to Save");
echo frmButtonHelp("Cancel","cancel","Click here to Cancel");
echo frmButtonConfirmHelp("Delete","delete", "Are you sure you want to delete this provider?","Click here to delete");
echo frmHiddenField($tkn,"tkn");

echo endFormTable();
?>