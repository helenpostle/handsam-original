<?php
//variables for select box qry
$tid = $formDB['trainee_id'];
if ($tid == "") $tid = 0;

$cid = $formDB['course_id'];
if ($cid == "") $cid = 0;

$pid = $formDB['provider_id'];
if ($pid == "") $pid = 0;

//arrays for select boxes:

//echo "<div id=\"trn_form_header\" class=\"clearfix\">";
//echo "<h2>Training Log Item</h2>";
//echo displayParagraphs("Please enter the details of the accident below. If more than one person was injured, then a seperate report should be filled in for that person.  To make this easier, this report may be copied, so that all of the information, apart from the personal details, are already filled in."  ,'frm_info');
//echo "</div>";

//echo "<div id=\"trn_form\">";
echo startFormTable($_SERVER["PHP_SELF"],"clearfix", "", "Training Log Item", "");
echo frmHiddenField($pageVars->training_id,"training_id");
echo frmHiddenField($pageVars->accountid,"accountid");
echo frmHiddenField(htmlspecialchars(serialize($qs_base)),"filter");
//Personal info
echo "<div id=\"trainee\" class=\"frmSection clearfix\">";
echo "<h3>Part 1: Trainee Details</h3>";
	//display select box of victims
		$usr_qry = "select userid as a, Concat(firstname, ' ', lastname) as b from usr where ((state = 'ACTIVE' and userid != $tid) or userid = $tid ) and accountid = ".$pageVars->accountid." order by lastname";
		echo "<div class=\"clearfix\">";
			echo frmExSelectQuery($formDB,"trainee_id",$usr_qry,$con,"Select a trainee",true);
		echo "</div>";
		echo "<div id=\"trainee_detail\">";
			echo "<div class=\"col\">";
				echo frmTextAreaNonEdit($formDB,"firstname","First name");
				echo frmTextAreaNonEdit($formDB,"lastname","Last name");
				echo frmTextAreaNonEdit($formDB,"email","Email");
			echo "</div>";
			echo "<div class=\"col\">";
				echo frmTextAreaNonEdit($formDB,"job_title","Job title");
				echo frmTextAreaNonEdit($formDB,"job_desc","Job description");
			echo "</div>";
		echo "</div>";
echo "</div>";

//training course details
echo "<div id=\"trainingCourseDetails\" class=\"frmSection\">";
echo "<h3>Part 2: Training Course Details</h3>";
//new conditional to check if e-training-enabled for this account:
if ($clsUS->conf['training']['e_training_enabled'] == 1) {
	$course_qry = "select course_id as a, Concat(CASE online when 0 then '' WHEN 1 then 'Handsam Online Course: ' END, title) as b, online from training_course where ((state != 'DELETED' and course_id != $cid) or course_id = $cid ) and (accountid = ".$pageVars->accountid." or accountid=0) and (renewal like '% day' or  renewal like '% week' or  renewal like '% month' or  renewal like '% year') and (renewal_reminder like '% day' or renewal_reminder like '% week' or  renewal_reminder like '% month' or  renewal_reminder like '% year') order by online desc,title";
} else {
	$course_qry = "select course_id as a, Concat(CASE online when 0 then '' WHEN 1 then 'Handsam Online Course: ' END, title) as b, online from training_course where ((state != 'DELETED' and course_id != $cid) or course_id = $cid ) and (accountid = ".$pageVars->accountid." or accountid=0) and (renewal like '% day' or  renewal like '% week' or  renewal like '% month' or  renewal like '% year') and (renewal_reminder like '% day' or renewal_reminder like '% week' or  renewal_reminder like '% month' or  renewal_reminder like '% year') and  (online = 0 or (online = 1 and accountid = ".$pageVars->accountid.")) order by online desc,title";
}
echo displayParagraphs("The full renewal period is the expiry date for the qualification. <br/>The reminder periods are the length of time, prior to the expiry period being reached, that a reminder will be emailed to the user and the Training Log administrator."  ,'frm_info');

echo frmExSelectQuery($formDB,"course_id",$course_qry,$con,"Select a training course",true);
	echo "<div id=\"course_detail\" class=\"clearfix\">";
		echo "<div class=\"col\">";
			echo frmTextAreaNonEdit($formDB, "title","Course title");
			echo frmTextAreaNonEditArray($formDB,"course_type","Course type", $trn_type_arr);
			echo frmTextAreaNonEdit($formDB,"qual_title","Qualification");
			echo frmTextAreaNonEdit($formDB,"qual_desc","Qualification description");
		echo "</div>";		
		echo "<div class=\"col\">";
			echo frmHiddenField($formDB['pid'],"pid");	
			echo frmTextAreaNonEdit($formDB,"renewal","Full renewal period");
			echo frmTextAreaNonEdit($formDB,"renewal_reminder", "Renewal reminder period");
		echo "</div>";
	echo "</div>";
echo "</div>";
//training course details
echo "<div id=\"trainingProviderDetails\" class=\"frmSection\">";
echo "<h3>Part 3: Training Provider Details</h3>";
$prov_qry = "select provider_id as a, provider_name as b from training_provider where ((state != 'DELETED' and provider_id != $pid) or provider_id = $pid ) and (accountid = ".$pageVars->accountid." or accountid = 0) order by provider_name";
echo frmExSelectQuery($formDB,"provider_id",$prov_qry,$con,"Select a training provider",true);
	echo "<div id=\"provider_detail\" class=\"clearfix\">";
		echo "<div class=\"col\">";
			echo frmTextAreaNonEdit($formDB,"provider_name","Provider name");
			echo frmTextAreaNonEdit($formDB,"provider_address1","Address 1");
			echo frmTextAreaNonEdit($formDB,"provider_address2","Address 2");
			echo frmTextAreaNonEdit($formDB,"provider_address3","Address 3");
			echo frmTextAreaNonEdit($formDB,"provider_city","City");
		echo "</div>";		
		echo "<div class=\"col\">";	
			echo frmTextAreaNonEdit($formDB,"provider_postcode","Postcode");
			echo frmTextAreaNonEdit($formDB,"provider_tel1","Tel 1");
			echo frmTextAreaNonEdit($formDB,"provider_ref","Site ref");
			echo frmTextAreaNonEdit($formDB,"provider_email","Email");
			echo frmTextAreaNonEdit($formDB,"provider_web","Website");
		echo "</div>";
		echo frmTextAreaNonEdit($formDB,"provider_comments","Comments",  "full");
		
	echo "</div>";
echo "</div>";



echo "<div id=\"specificDetails\" class=\"frmSection\">";
	echo "<h3>Part 4: Specific Details</h3>";
	echo displayParagraphs("The renewal  date below is calculated automatically from the course date and the training course renewal date."  ,'frm_info');
	echo frmTrainingCourseDateField($formDB,"course_date","Course Date",true);
	echo "<div id=\"dates_update\">";
	echo displayTrainingEditDateField($formDB, "renewal_date", "Renewal date");
	echo displayTrainingEditDateField($formDB, "renewal_reminder_date", "Renewal reminder date");
	echo "</div>";
	
	echo frmExTextField($formDB,"","cost",100,"Cost",false, "(GBP)");
	echo frmTextArea($formDB, "comments",5,"Comments",false);
echo "</div>";

echo "<div id=\"fileUploads\" class=\"frmSection\">";
	echo "<h3>Part 5: Uploaded Files</h3>";
	echo displayParagraphs("Please upload any relevant files here."  ,'frm_info');
	
	$uploads = getUploads($con, $pageVars);
	echo "<div id=\"uploads_list\">";
		if ($uploads->cnt > 0) {
			echo "<ul class=\"clearfix\">";
			foreach ($uploads->rows as $row) {
				echo "<li><a class=\"".strtolower(substr(strrchr($row['filename'],'.'),1))."\" href=\"$rootUrl/modules/training/downloadTrnDocument.php?id={$row['fileid']}\">{$row['title']}</a><a class=\"edit_file\" href=\"{$row['fileid']}\">[ edit file ]</a></li>";
			}
			echo "</ul>";
		} else {
			echo "<p>No Uploaded Files</p>";
		}
		echo "<input id=\"bt_upload_file\" name=\"bt_upload_file\" type=\"submit\" value=\"[ upload file]\"/>";
	echo "</div>";
echo "</div>";



echo frmButtonHelp("Save Changes","save_draft", "Click here to update changes");
echo frmButtonConfirmHelp("Assign to User", "save_submit", "Do you wish to assign this training course to this User? If so click OK.","Click to save and assign the course to an user.");
echo frmButtonConfirmHelp("Delete","delete", "Are you sure you want to delete this report?","Click here to delete");

echo frmButtonHelp("Cancel","cancel", "Click here to cancel");

echo endFormTable();
//echo "</div>";
//echo "<div id=\"trn_form_footer\"></div>";
?>