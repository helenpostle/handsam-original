<?php
/***
** Shows the list of lotc plans
** 
***/



if ($pageDb->rsExists()) {
   
   ?>
   <table  id="trn_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
			    <th class="start">Account</th>
				<th class=" ">Staff Name</th>
				<th class="">Course Title</th>
				<th class="">Course Date</th>
				
				<th class="end">Course State</th>
			</tr>
		</thead>
		<tbody>
		
		<?php
		
		  foreach ($pageDb->rows as $row) {
		     if ($row["state"] == "IN PROGRESS") {
			    if (strtotime($row['course_date']) < strtotime("today")) {
				   $training_icon = "<img src=\"$rootUrl/modules/training/images/icon_error_red.gif\"/>";
				} else {
				   $training_icon = "<img src=\"$rootUrl/modules/training/images/icon_error.gif\"/>";
				}
			 }
			 if ($row["state"] == "COMPLETE") $training_icon = "<img src=\"$rootUrl/modules/training/images/tick.gif\"/>";
			 $course_url = $rootUrl;
			 $course_url .= "/modules/admin/view_training_course.php?accountid=".$row['accountid']."&amp;training_id=".$row['training_id'];
			 
		  ?>
		    <tr>
			   <td class="start"><?php echo displayText($row["accountname"]); ?></td>
			   <td class=""><span id="userid<?php echo $row["userid"]; ?>" class="hidden"><?php echo displayText($row["usr"]); ?></span></td>
			   <td class=""><a href="<?php echo $course_url;?>"><?php echo displayText($row["title"]); ?></a></td>
			   <td class=""><?php echo displayDate($row["course_date"]); ?></td>
			   <td class="end"><?php echo $training_icon.displayText($trn_state[$row["state"]]); ?></td>
			</tr>
		  
		  <?php
		  }
		
		?>
		</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td></td>
			<td ></td>
			
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
  <?php
} else {
	echo displayParagraphs("There are no Training Log Reports to view");
	
}


?>