<?php

/**
 * CRUD form for system courses
 *
 */

echo startFormTable($_SERVER["PHP_SELF"], "clearfix", "", "System Wide Training Course", "Please ".$action." the details of the course below. Don't forget to click the 'Save' button to save your changes.");

echo frmHiddenField($course_id, "course_id");

echo "<div id=\"courseDetails\" class=\"frmSection clearfix\">";
echo "<h3>Edit Course Details</h3>";

echo frmTextField($formDB->row, "title", 100, "Title", true);

echo frmSelectArray($formDB->row, "course_type", $trn_type_arr, "Course Type", true);

echo frmTextField($formDB->row, "qual_title", 100, "Qualification", false);

echo frmTextField($formDB->row, "qual_desc", 100, "Description", false);

echo frmDateAddField($formDB->row, "renewal", $trn_date_arr, "Renewal Period", true);

echo frmDateAddField($formDB->row, "renewal_reminder", $trn_date_arr, "Renewal Reminder Period", true);

// checkbox for the course url
// some JS adds show/hide to the course_url input

$courseurl_select = ($formDB->row['online'] == '1' || (isset($_POST['online']) && $_POST['online'] == '1') ) ? ' checked' : '';
if ($formDB->row['online'] == 1) echo frmRow('<input disabled=\"disabled\" type="checkbox" name="online" id="online" value="1" '.$courseurl_select.' />', 'Is this an online course?', 'has_url', false);

//echo frmTextField($formDB->row, "course_url", 100, "The course <acronym title=\"Uniform Resource Locator, or web address\">URL</acronym>", false);
// end of course_url

// add job role(s) to the course
$sql = "SELECT role_id AS a, role_title AS b FROM core_jobrole WHERE state <> 'DELETED' ORDER BY created ASC;";
echo frmMultiSelectQuery($roleDB, 'role_id', $sql, $con, 'Job Roles auto-assign', false);

addFileField($fileDB);

addLinkField($linkDB);
	
echo "</div>";

echo frmButtonHelp("Save", "save", "Click here to Save");

echo frmButtonHelp("Cancel", "cancel", "Click here to Cancel");

echo frmButtonConfirmHelp("Delete", "delete", "Are you sure you want to delete this course? All uncomplete training log entries will also be deleted.", "Click here to delete this course");

if($formDB->row['accountid'] == $clsUS->accountid) echo frmButtonConfirmHelp("Delete", "delete", "Are you sure you want to delete this course?", "Click here to delete");

echo endFormTable();