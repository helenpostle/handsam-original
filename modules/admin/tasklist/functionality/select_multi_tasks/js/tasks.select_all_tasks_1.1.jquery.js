$(window).ready( function() {
	
	//$('.toggleInput').parent().prev().append('<div class=\'inputCover\'></div>');
	$('.cover').each(function(index) {
		//var el_width = 0;
		//if ($(this).children('input').length)	el_width = $(this).children('input').width() + 20;
		//if ($(this).children('select').length)	el_width = $(this).children('select').width() + 20;
		//$(this).append('<div class=\'inputCover cover_' + index + '\' style=\'width: ' + el_width + 'px;\'></div>');
		
		
		$(this).append('<div class=\'inputCover cover_' + index + '\'></div>');
	});

	$('#selectAllTasks').click(function(event){
		if ($(this).is(':checked')) {
			$('.selectTask').attr('checked', true);
		} else {
			$('.selectTask').attr('checked', false);
		}
	});

	
	$('#selectAllBoxes').click(function(event){
		if ($(this).is(':checked')) {
			$('.multiSelectCheck').attr('checked', true);
		} else {
			$('.multiSelectCheck').attr('checked', false);
		}
	});
	
	
	$('.toggleInput').each(function(index) {
		$(this).click(function(event){
			$('.cover_' + index).toggle();
			if ($.browser.msie && $.browser.version.substr(0,1)<7) $(this).parent().prev().children('select').toggle();
		});
		
	});
});