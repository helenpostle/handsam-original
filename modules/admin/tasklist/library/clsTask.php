<?php

/**
 * clsTask kind of a simple crud class.  
 * 
 */

class task {
    
    public $taskid; //taskid
    public $rs; //the result set row - this is used to integrate with older functionality and can be removed when that is updated.
    public $row; //same as above - can be removed later
    public $categoryid;
    public $mid;
    private $clsUS;
    public $edited_by;
    public $repeatingtaskid;
    public $day_of_year;
    
    
    public function __construct($clsUS, $taskid=0) {
        $this->taskid = $taskid;
        $this->clsUS = $clsUS;
    }
    
    public function get($con) {
        $qry = "select * from task where taskid = {$this->taskid}";
        $rs = getRS($con, $qry);
        $row = getRow($rs);
        $this->rs = $rs;
        $this->row = $row;
        $this->categoryid = $row['categoryid'];
        $this->mid = $row['mid'];
        $this->edited_by = $row['edited_by'];
        $this->repeatingtaskid = $row['repeatingtaskid'];
        $year  = date('Y');
        $days = array(1=>"monday", 2=>"tuesday", 3=>"wednesday", 4=>"thursday", 5=>"friday", 6=>"saturday", 7=>"sunday");
        
        if($row['week_num'] > 0 && $row['day_of_week'] > 0) {
            $day = date("z", strtotime($year."W".str_pad($row['week_num'], 2, '0', STR_PAD_LEFT).$row['day_of_week']));
        } else if($this->row['position_in_month'] !== null && $row['position_in_month'] >= 0 && $row['day_of_week'] > 0 && $row['taskmonth'] > 0) {
            $day = date("z",strtotime($row['position_in_month'].$days[$row['day_of_week']]." ".$year."-0".$row['taskmonth']));
        } else {
            $day = date("z",strtotime($year."-".str_pad($row['taskmonth'], 2, '0', STR_PAD_LEFT)."-".$row['taskday']));
        }
        $this->day_of_year = $day;
    }
    
    public function insert($con, $val_arr) {
        global $core_conf;
        global $messages;
        $qry = new dbInsert("task");
        $qry->setReqStringVal("customtext", $val_arr["customtext"], "Custom Task description"); 
        $qry->setStringVal("tasktext", $val_arr["customtext"]);    
        $qry->setReqStringVal("state", $val_arr["state"], "State");
       
        $qry->setReqNumberVal("tasklistid", $val_arr["tasklistid"], "Task List");
        $qry->setReqNumberVal("categoryid", $val_arr["categoryid"], "Category");
        
        if($core_conf['key_task']) {
            if(isset($val_arr['key_task']) && $val_arr['key_task'] == 1) {
                $qry->setNumberVal('key_task', 1, 'Key Task');
            } else {
                $qry->setNumberVal('key_task', 0, 'Key Task');
            }
        }        
        
        if($core_conf['key_task'] && $core_conf['key_task_lock']) {
            // can only be set if it's a key task
            if(isset($val_arr['key_task']) && $val_arr['key_task'] == 1) {
                if(isset($val_arr['key_task_lock']) && $val_arr['key_task_lock'] == 1) {
                    $qry->setNumberVal('key_task_lock', 1, 'Key Task Date Lockdown');
                } else {
                    $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
                }          
            } else {
                $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
            } 
        }   
        
        //calendar data
        if (isset($val_arr["repeatingtaskid"])) {
            $qry->setReqNumberVal("repeatingtaskid", $val_arr["repeatingtaskid"], "Repeating Task Id");
        } else {
            $qry->setReqNumberVal("repeatingtaskid", 0, "Repeating Task Id");
        }
        if (isset($val_arr["taskmonth"])) {
            $qry->setReqNumberVal("taskmonth", $val_arr["taskmonth"], "Month");
        }
        if (isset($val_arr["taskday"])) {
            $last_day = date('t', strtotime('01-'.$val_arr["taskmonth"].'-'.date("Y")));
            if ($val_arr["taskday"] > $last_day ) $val_arr["taskday"] = $last_day ;
            $qry->setReqNumberVal("taskday", $val_arr["taskday"], "Day");
        }
        if (isset($val_arr["day_of_week"]) && $val_arr["day_of_week"] > 0) {
            $qry->setReqNumberVal("day_of_week", $val_arr["day_of_week"], "Day of Week");
        } else if(isset($val_arr["day_of_week"]) && $val_arr["day_of_week"] == 0) {
            $qry->setNullVal("day_of_week");
        }
        if (isset($val_arr["week_num"]) && $val_arr["week_num"] > 0) {
            $qry->setReqNumberVal("week_num", $val_arr["week_num"], "Week Number");
        } else if(isset($val_arr["week_num"]) && $val_arr["week_num"] == 0) {
            $qry->setNullVal("week_num");
        }
        if (isset($val_arr["position_in_month"]) && $val_arr["position_in_month"] != 99) {
            $qry->setReqNumberVal("position_in_month", $val_arr["position_in_month"], "Day");
        } else if(isset($val_arr["position_in_month"]) && $val_arr["position_in_month"] == 99) {
           $qry->setNullVal("position_in_month"); 
        }
        
        if(isset($core_conf['compliance']) && count($core_conf['compliance']) > 0)
        {
            $qry->setReqStringVal('compliance', $val_arr['compliance'], 'Compliance');
        }
        
        $qry->setAudit($this->clsUS->userid);
        if ($qry->execute($con)) {
            $this->taskid = $qry->getNewID();
            return true;   
        } else {
            $messages[] = $qry->getError();
            return false;  
        }
                      
    }
    
    public function update($con, $val_arr) {
        global $core_conf;
        global $messages;
        $qry = new dbUpdate("task");
        if($this->mid > 0) {
            $qry->setStringVal("customtext", $val_arr["customtext"], "Custom Task description");
        } else {
            //not a handsam task so need some text!
            $qry->setReqStringVal("customtext", $val_arr["customtext"], "Custom Task description");
        }        
        $qry->setParam("taskid", $this->taskid);
        $qry->setReqStringVal("state", $val_arr["state"], "State");
        
        $qry->setReqNumberVal("tasklistid", $val_arr["tasklistid"], "Task List");
        $qry->setReqNumberVal("categoryid", $val_arr["categoryid"], "Category");
        
        if($core_conf['key_task']) {
            if(isset($val_arr['key_task']) && $val_arr['key_task'] == 1) {
                $qry->setNumberVal('key_task', 1, 'Key Task');
            } else {
                $qry->setNumberVal('key_task', 0, 'Key Task');
            }
        }        
        
        if($core_conf['key_task'] && $core_conf['key_task_lock']) {
            // can only be set if it's a key task
            if(isset($val_arr['key_task']) && $val_arr['key_task'] == 1) {
                if(isset($val_arr['key_task_lock']) && $val_arr['key_task_lock'] == 1) {
                    $qry->setNumberVal('key_task_lock', 1, 'Key Task Date Lockdown');
                } else {
                    $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
                }          
            } else {
                $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
            } 
        } 
        
        //now validate the sync changes check boxes. Nothing is added to db, but any boxes must be checked before the form can validate
        if (isset($val_arr['sync_change'])) {
            $check_cnt = 0;

            for ($i=0; $i<=count($val_arr['sync_change']); $i++) {
                if (isset($val_arr['sync_check_'.$i])) $check_cnt++;
            }
            if (isset($val_arr['sync_change']) && count($val_arr['sync_change']) > $check_cnt) {
                $qry->failError .= "Please confirm that you have checked all of the Handsam update changes by ticking ALL of the 'CHANGES ACCEPTED' boxes.\r\n";
            }
        }        
        
        //calendar data
        if (isset($val_arr["repeatingtaskid"])) {
            $qry->setReqNumberVal("repeatingtaskid", $val_arr["repeatingtaskid"], "Repeating Task Id");
        }
        if (isset($val_arr["taskmonth"])) {
            $qry->setReqNumberVal("taskmonth", $val_arr["taskmonth"], "Month");
        }
        if (isset($val_arr["taskday"])) {
            //need to check if day is greater than last day of month
            $last_day = date('t', strtotime('1-'.$val_arr["taskmonth"].'-'.date("Y")));
            if ($val_arr["taskday"] > $last_day ) $val_arr["taskday"] = $last_day ;
            $qry->setReqNumberVal("taskday", $val_arr["taskday"], "Day");
        }
        if (isset($val_arr["day_of_week"]) && $val_arr["day_of_week"] > 0) {
            $qry->setReqNumberVal("day_of_week", $val_arr["day_of_week"], "Day of Week");
        } else if(isset($val_arr["day_of_week"]) && $val_arr["day_of_week"] == 0) {
            $qry->setNullVal("day_of_week");
        }
        if (isset($val_arr["week_num"]) && $val_arr["week_num"] > 0) {
            $qry->setReqNumberVal("week_num", $val_arr["week_num"], "Week Number");
        } else if(isset($val_arr["week_num"]) && $val_arr["week_num"] == 0) {
            $qry->setNullVal("week_num");
        }
        if (isset($val_arr["position_in_month"]) && $val_arr["position_in_month"] != 99) {
            $qry->setReqNumberVal("position_in_month", $val_arr["position_in_month"], "Day");
        } else if(isset($val_arr["position_in_month"]) && $val_arr["position_in_month"] == 99) {
           $qry->setNullVal("position_in_month"); 
        }
        
        if(isset($core_conf['compliance']) && count($core_conf['compliance']) > 0)
        {
            $qry->setReqStringVal('compliance', $val_arr['compliance'], 'Compliance');
        }
        
        $qry->setAudit($this->clsUS->userid);
        if ($qry->execute($con)) {
            return true;
        } else {
            $messages[] = $qry->getError();
            return false;
        }
    }

    public function delete($con) 
    {
        global $messages; 
        
        $qry = new dbUpdate("task");
        $qry->setParam("taskid", $this->taskid);
        $qry->setReqStringVal("state", "DELETED", "State");
        $qry->setAudit($this->clsUS->userid);
        
        if(!$qry->execute($con)) {
            $messages[] = $qry->getError();
            return false;
        } else {
            return true;
        }
    }
    
    public function updateTaskDates($con, $val_arr) {
        global $core_conf;
        global $messages;
        $qry = new dbUpdate("task");  
        $qry->setParam("taskid", $this->taskid);
        if (isset($val_arr["repeatingtaskid"])) $qry->setReqNumberVal("repeatingtaskid", $val_arr["repeatingtaskid"], "Repeating Task Id");
        if(isset($val_arr["state"])) $qry->setStringVal("state", $val_arr["state"], "State");
        if (isset($val_arr["taskmonth"])) {
            $qry->setReqNumberVal("taskmonth", $val_arr["taskmonth"], "Month");
        } else {
            $qry->setNumberVal("taskmonth", 0, "task month");
        }
        if (isset($val_arr["taskday"])) {
            $last_day = date('t', strtotime('1-'.$val_arr["taskmonth"].'-'.date("Y")));
            if ($val_arr["taskday"] > $last_day ) $val_arr["taskday"] = $last_day ;
            $qry->setReqNumberVal("taskday", $val_arr["taskday"], "Day");
        } else {
            $qry->setNumberVal("taskday", 0, "task day");
        }
        if (isset($val_arr["day_of_week"])) {
            $qry->setReqNumberVal("day_of_week", $val_arr["day_of_week"], "Day of Week");
        } else {
            $qry->setNullVal("day_of_week");
        }
        if (isset($val_arr["week_num"])) {
            $qry->setReqNumberVal("week_num", $val_arr["week_num"], "Week Number");
        } else {
            $qry->setNullVal("week_num");
        }
        if (isset($val_arr["position_in_month"])) {
            $qry->setReqNumberVal("position_in_month", $val_arr["position_in_month"], "Day");
        } else {
            $qry->setNullVal("position_in_month");
        }
        
        $qry->setAudit($this->clsUS->userid);
        if ($qry->execute($con, false)) {
            return true;
        } else {
            $messages[] = $qry->getError();
            return false;
        }        
    }
    
}
?>