<?php
/**
 * save multiple tasks from the select multi tasks form 
 */
 
function saveMultiTasks($con, $clsUS) {
    global $messages;
    $multiSaveError = false;
    foreach($_POST['selectMulti'] as $selectId)
    {
        $qry = new dbUpdate("task");
        $qry->setParam("taskid", $selectId);
        #if checkbox checked update state
        if(isset($_POST['editState']))
        {
            $qry->setReqStringVal("state", $_POST["state"], "State");
        }
        #if checkbox checked update day
        if(isset($_POST['editDay']))
        {
            $qry->setReqNumberVal("taskday", $_POST["taskday"], "Day Number");
        }
        #if checkbox checked update month
        if(isset($_POST['editMonth']))
        {
            $qry->setReqNumberVal("taskmonth", $_POST["taskmonth"], "Month Number");
        }
        #if checkbox checked update category
        if(isset($_POST['editCategory']))
        {
            $qry->setReqNumberVal("categoryid", $_POST["categoryid"], "Category");
        }
        $qry->setAudit($clsUS->userid);
        if($qry->execute($con))
        {
            $multiSaveError = false;
            //now update accounttasks
            $qry = new dbUpdate("accounttask");
            $qry->setParam("taskid", $selectId);
            $qry->setStringParamNotEqual("state", "COMPLETED");
            if(isset($_POST['editCategory'])) $qry->setNumberVal("categoryid", $_POST["categoryid"], "Category");
            if(isset($_POST['editState'])) $qry->setReqStringVal("state", $_POST["state"], "State");
            $qry->setReqStringVal("flag", "EDITED", "flag");
            $qry->setAudit($clsUS->userid);
            if(!$qry->execute($con, false)) {
                $messages[] = $qry->getError();
                $multiSaveError = true;
            }
        }
        else
        {
            $multiSaveError = true;
            $messages[] = $qry->getError();
        }
    }
    return $multiSaveError;
}


/**
 * save multiple tasks from the select multi tasks form 
 */ 
function saveMultiAccounttasks($con, $clsUS, $licence_end, $accountid, $tasklistid) {
    global $messages;
    $multiSaveError = false;
	$multi_message = array();
    foreach($_POST['selectMulti'] as $selectId) {
		//for sending task alerts - need old data to compare
		$accounttask = new accounttask($clsUS, $selectId);
        $accounttask->get($con);
        $rowAccountTask = $accounttask->row;
		
        $qry = new dbUpdate("accounttask");
        $qry->setParam("accounttaskid",$selectId);
        $qry->setStringParamNotEqual("state","COMPLETED");
        #if checkbox checked update state
        if (isset($_POST['editState'])) {
            $qry->setReqStringVal("state", $_POST["state"], "State");
        }
        #if checkbox checked update actiondate
        if (isset($_POST['editActionDate'])) {
            if (strtotime($_POST["actiondate"]) <= strtotime($licence_end)) {
                $qry->setReqDateVal("actiondate", $_POST["actiondate"], "Action Date");
                $qry->setNumberVal("actiondate_flag", 1, "action date flag");
            } else {
                $qry->setReqDateVal("actiondate", "DATE_LATE", "Action Date", $licence_end);
            }
        }
        #if checkbox checked update assigned to
        if (isset($_POST['editAssignedto'])) {
            if (!check_user_task_list($accountid, $tasklistid, $_POST["assignedto"], $con) || $_POST["assignedto"] == 0 || $_POST["assignedto"] == "" ) {
                $qry->setNullVal("assignedto");
            } else {
                $qry->setNumberVal("assignedto",$_POST["assignedto"],"Assigned to");
            }
        }
        $qry->setAudit($clsUS->userid);
        if ($qry->execute($con)) {
            $multiSaveError = false;
			$post = $_POST;
			if (!isset($post['editAssignedto'])) $post['assignedto'] = $rowAccountTask['assignedto'];
			$row['assignedto'] = 0;
			$multi_message = emailAssignedUsers($rowAccountTask, $post, $selectId, $multi_message);
			
        } else {
            $multiSaveError = true;
            $messages[] = $qry->getError();	
        }
    }
    //echo json_encode($multi_message);
	//die();
    // if an user has just bee assigned new tasks, send an email with number of tasks
    if (is_array($multi_message) && count($multi_message) > 0) {
        emailAssignedUserMultitask($multi_message);
	}
        
    return $multiSaveError;
}
