<?php

/**
 * RepeatingAccounttask CRUD class 
 * 
 * methods: save(), getRow(), delete()
 * 
 * Create a new object with new RepeatingAccounttask($clsUS), also can take the accounttask_repeating table id
 *
 */

class RepeatingAccounttask {
    
   
    /**
     * row id
     * @var int
     */
    protected $id;
    
    /**
     * DB recordset object
     * @var object
     */
    protected $row = array();
    
    /**
     * Current User Class
     * @var obj
     */
    protected $clsUS;
    
    /**
     *
     * @var int number of tasks with this repeating id 
     */
    public $num_repeats;
    
    
    /**
     *
     * @array containing all taskids with this repeatingtaskid 
     */
    public $all_task_dates = array();
    
    
    /*
     * 
     * array of weeks that this repeating task covers (1-52)
     */
    public $weeks_arr;
      
    /*
     * 
     * array of months that this repeating task covers (1-12)
     */
    public $months_arr;
    
    /*
     * loop array is set as weeks_arr or months_arr
     */
    public $loop_arr = array();
    
    /*
     * var type of repeat - weekly, monthly_by_date, monthly_by_day
     */
    private $repeat_type;
    
    /*
     * var the week day for a weekly repeat
     */
    private $weekly;
    
    /*
     * var the month date for a monthly by date repeat
     */
    private $monthly_by_date;
    
    /*
     * var the 'first' or 'last' part of a monthly by day repeat
     */
    private $position_in_month;
    
    /*
     * var - hte day of week for a monthly by day repeat
     */
    private $month_day;
    
    /*
     * var mid
     */
    private $mid;
    
    /*
     * array of task theoretical task dates within this repeating task series
     */
    
    public $taskDates;
    
    
    public $lic_start;
    
    public $lic_end;
    
    public $start_year;
    
    public $end_year;
    
    public $start_date;
    
    public $end_date;
    
    public $accountid;
    
    public $repeatingtaskid;
    
    public $licenceid;
    
    
    public function __construct($con,$clsUS,$id=null,$lic_start=null,$lic_end=null,$accountid = null, $repeatingtaskid =0,$licenceid=null)
    {
        $this->clsUS = $clsUS;
        $this->accountid = $accountid;
        $this->licenceid = $licenceid;
        $this->repeatingtaskid = $repeatingtaskid;
        if (!is_null($lic_start) && !is_null($lic_end)) $this->setLicence($lic_start, $lic_end);
        if(!is_null($id) && $id > 0)
        {
            $this->id = $id;
            
            $this->get($con);
        }
    }
    
    /*
     * sets the licence dates
     */
    public function setLicence($licence_start, $licence_end) {
        $this->lic_start = $licence_start;
        $this->lic_end = $licence_end;
    }
    
    /**
     * Saves the recordset after a form submission
     * 
     * @global array $messages
     * @return boolean
     */
    public function save($con, $flag=false, $row2=false, $get=true)
    {
        global $messages;
        $db = ($this->id > 0) ? $this->update() : $this->insert();
        
        if($row2 == true) {
          $vars = $row2;
        } else {
          $vars = $_POST;
        }
        if(isset($vars['weekly'])) {
            $db->setReqNumberVal('weekly', $vars['weekly'], 'Weekly Repeating Task - Week Day');
        } else {
            $db->setNullVal('weekly');
        }
        if(isset($vars['month_date'])) {
            $db->setReqNumberVal('month_date', $vars['month_date'], 'Monthly by Date Repeating Task - Month Date');
        } else {
            $db->setNullVal('month_date');
        }
        if(isset($vars['position_in_month'])) {
            $db->setReqNumberVal('position_in_month', $vars['position_in_month'], 'Monthly by Day Repeating Task - First or Last');
        } else {
            $db->setNullVal('position_in_month');
        }
        if(isset($vars['month_day'])) {
            $db->setReqNumberVal('month_day', $vars['month_day'], 'Monthly by Day Repeating Task - Month Day');
        } else {
            $db->setNullVal('month_day');
        }
        
        //need last day of month of end month for next calcs
        $year = date("Y", strtotime($this->lic_start));
        $month = 12;
        if (isset($vars['repeat_end_month'])) $month = $vars['repeat_end_month'];
        $last_day_of_month = date('t', strtotime("$year-$month-01"));
        
        if($flag) {
            $db->setNumberVal('edit_flag', 1, 'edit flag');
        }
        
        
        //must either be repeat_duration or save repeat_start_month etc
        if(isset($vars['repeat_duration']) && $vars['repeat_duration'] > 0) {
            $db->setNumberVal('repeat_duration', $vars['repeat_duration'], 'Repeat Duration');
            $db->setNumberVal('repeat_start_month', date("m", strtotime($this->lic_start)), 'Start Month');
            $db->setNumberVal('repeat_start_day', date("d", strtotime($this->lic_start)), 'Start Day');
            $db->setNumberVal('repeat_end_month', date("m", strtotime($this->lic_end)), 'End Month');
            $db->setNumberVal('repeat_end_day', date("m", strtotime($this->lic_end)), 'End Day');
            $db->setNumberVal('repeat_start_year', date("Y", strtotime($this->lic_start)), 'Start Year');
            $db->setNumberVal('repeat_end_year', date("Y", strtotime($this->lic_end)), 'End Year');
            
        } else {
            $db->setNumberVal('repeat_duration', 0, 'Repeat Duration');
            if(isset($vars['repeat_start_month'])) $db->setNumberVal('repeat_start_month', $vars['repeat_start_month'], 'Start Month');
            if(isset($vars['repeat_end_month'])) $db->setNumberVal('repeat_end_month', $vars['repeat_end_month'], 'ENd Month');
            if (isset($vars['weekly']) && $vars['weekly'] > 0) {
                if(isset($vars['repeat_start_day'])) $db->setNumberVal('repeat_start_day', $vars['repeat_start_day'], 'Start Day');
                
                //if end day > last day of month, enter last day of month:
                if ($vars['repeat_end_day'] > $last_day_of_month) $vars['repeat_end_day'] = $last_day_of_month;
                if(isset($vars['repeat_end_day'])) $db->setNumberVal('repeat_end_day', $vars['repeat_end_day'], 'End Day');
                $this->getYearValues($vars['repeat_start_day'], $vars['repeat_start_month'], $vars['repeat_end_day'], $vars['repeat_end_month']);
            } else {
                $db->setNumberVal('repeat_start_day', 1, 'Start Day');
                $db->setNumberVal('repeat_end_day', $last_day_of_month, 'End Day');
                $this->getYearValues(1, $vars['repeat_start_month'], $last_day_of_month, $vars['repeat_end_month']);
            }
            //echo $this->start_year;
            $db->setNumberVal('repeat_start_year', $this->start_year, 'Start Year');
            $db->setNumberVal('repeat_end_year', $this->end_year, 'End Year');
        
        }
        
        $db->setNumberVal('accountid', $this->accountid, 'Accountid');
        $db->setNumberVal('licenceid', $this->licenceid, 'licenceid');

        if ($this->repeatingtaskid > 0) {
            $db->setNumberVal('repeatingtaskid', $this->repeatingtaskid, 'Repeating task Id');
        } else {
            //$db->setNullVal('repeatingtaskid');
        }
        
        if ($db->execute($con))
        {
            //set weeks/months arr
            if($flag && $this->id > 0) {
                //also update accounttask.actiondate_flag = 1 for this $this->id
                $qry = new dbUpdate('accounttask');
                $qry->setParam("accounttask_repeatingid", $this->id);
                $qry->setNumberVal('actiondate_flag', 1, 'Ationdate flag');
                if (!$qry->execute($con)) $messages[] = $qry->getError();
            }
            
            if($get) {
                if ($this->id == null) $this->id = $db->getNewId();
                $this->get($con);
                $this->setDateCriteria();
                return $this->id;
            }
        }
             
        $messages[] = $db->getError();
        //echo $db->getError();
        return false;
    }
    
    /*
     * sets the array of week numbers
     */
    protected function getYearValues($start_day, $start_month, $end_day, $end_month) {
        
        $lic_start_yr = date("Y", strtotime($this->lic_start));
        $lic_end_yr = date("Y", strtotime($this->lic_end));
        
        //2 possible start dates for repeat:
        $sd1 = date("Y-m-d", strtotime("$lic_start_yr-$start_month-$start_day"));
        $sd2 = date("Y-m-d", strtotime("$lic_end_yr-$start_month-$start_day"));
    
        if ($this->lic_start < $sd1 && $sd1 < $this->lic_end) {
            $this->start_year = $lic_start_yr;
        } else {
            $this->start_year = $lic_end_yr;
        }
        
        //2 poss dates for end year
        $ed1 = date("Y-m-d", strtotime("$lic_start_yr-$end_month-$end_day"));
        $ed2 = date("Y-m-d", strtotime("$lic_end_yr-$end_month-$end_day"));
        
        if ($this->lic_start < $ed1 && $ed1 < $this->lic_end) {
            $this->end_year = $lic_start_yr;
        } else {
            $this->end_year = $lic_end_yr;
        }
    }

    
    /*
     * sets the date vars
     */
    private function setDateCriteria() {
        if(isset($this->row['weekly']) && $this->row['weekly'] > 0) {
            $this->repeat_type = "weekly";
            $this->weekly = $this->row['weekly'];
            $this->setWeeks();
        }
        if(isset($this->row['month_date'])&& $this->row['month_date'] > 0) {
            $this->repeat_type = "monthly_by_date";
            $this->setMonths();
            $this->monthly_by_date = $this->row['month_date'];
        }
        if(isset($this->row['month_day']) && $this->row['month_day'] > 0) {
            $this->repeat_type = "monthly_by_day";
            $this->setMonths();
            $this->month_day = $this->row['month_day'];
        }
        if (isset($this->row['position_in_month'])) $this->position_in_month = $this->row['position_in_month'];
       
    }
    
    /**
     * Returns the db row as an associated array
     * @return array
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set the current repeating task to delete
     * 
     * @return bool true on success
     */
    public function delete($con)
    {
        global $messages;
        
        if(!$this->id) 
            return false;
        
        $db = $this->update();
        $db->setReqStringVal("state", "DELETED", "State");
        $db->setAudit($this->clsUS->userid);
        
        if($db->execute($con)) 
            return true;
        
        $messages[] = $db->getError();
        return false;
    }
    
    /**
     * Returns a DB field
     * 
     * @param string $field field name
     * @return string
     */
    public function __get($field)
    {
        // returns the array key or an empty string
        return $this->row[$field] ? :'';
    }
    
    /**
     * Sets the row id and retrieves the record
     * 
     * @param int $id
     */
    public function setId($id, $con)
    {
        $this->id = $id;
        
        $this->get($con);
    }
        
    /**
     * gets the row id
     * 
     * @param int $id
     */
    public function getId()
    {
        return $this->id;
    }
        
    /**
     * Return a DB Insert object
     * Sets `state` to ACTIVE
     * sets the audit
     * 
     * @return \dbInsert 
     */
    protected function insert()
    {
        $db = new dbInsert('accounttask_repeating');
        $db->setReqStringVal("state", "ACTIVE", "State");
        $db->setAudit($this->clsUS->userid);
        
        return $db;
    }
    
    /**
     * Return a DB Update object,
     * Sets the row id
     * sets the audit trail
     * 
     * @return \dbUpdate
     */
    protected function update()
    {
        $db = new dbUpdate('accounttask_repeating');
        $db->setParam('id', $this->id);
        $db->setAudit($this->clsUS->userid);
        
        return $db;
    }
    
    /**
     * Fill the object with the db row
     */
    protected function get($con)
    {
        $sql = "SELECT * FROM accounttask_repeating WHERE id = '".mysql_real_escape_string($this->id)."';"; 
        $rs = getRS($con, $sql);
        $this->row = getRow($rs);
        $this->start_date = date("Y-m-d", strtotime($this->row['repeat_start_year']."-".$this->row['repeat_start_month']."-".$this->row['repeat_start_day']));
        $this->end_date = date("Y-m-d", strtotime($this->row['repeat_end_year']."-".$this->row['repeat_end_month']."-".$this->row['repeat_end_day']));
        $this->setDateCriteria();
        //change this when we start syncing repeatin tasks
        $this->mid = 0;  
        
        if ($this->id > 0) {
            $sql = "select accounttaskid, actiondate from accounttask where accounttask_repeatingid = {$this->id} and state NOT IN('DELETED', 'DELETED1', 'DELETED2')";
            $rs = getRS($con, $sql);
            $this->num_repeats = getResultRows($rs);
            while($row = getRow($rs)) {
                $this->all_task_dates[$row['accounttaskid']] = $row['actiondate'];
            }  
        }
        return $this;
    }
    
    
    function update_num_repeats($con) {
        $sql = "select accounttaskid from accounttask where accounttask_repeatingid = {$this->id} and state NOT IN('DELETED', 'DELETED1', 'DELETED2')";
        $rs = getRS($con, $sql);
        $this->num_repeats = getResultRows($rs);
    }
    
    /*
     * sets the array of week numbers
     */
    protected function setWeeks() {
        if (isset($this->row['repeat_duration']) && $this->row['repeat_duration'] == 1) {
            $this->weeks_arr = range(1, 52 ,1);
        } else {
            $days = array(1=>"monday", 2=>"tuesday", 3=>"wednesday", 4=>"thursday", 5=>"friday", 6=>"saturday", 7=>"sunday");
            $this->weeks_arr = getWeekNumbers($this->start_date, $this->end_date);
            $week_num = end($this->weeks_arr);
            if($week_num < 10) $week_num = "0".$week_num;
            $d=date("Y", strtotime($this->end_date))."W".$week_num.$this->row['weekly'];
            $last_actiondate = date("Y-m-d",strtotime($d));
            
            $week_num  = reset($this->weeks_arr);
            if($week_num < 10) $week_num = "0".$week_num;
            $d=date("Y", strtotime($this->start_date))."W".$week_num.$this->row['weekly'];
            $first_actiondate =  date("Y-m-d",strtotime($d));
            if ($last_actiondate > $this->end_date) array_pop($this->weeks_arr);
            if ($first_actiondate < $this->start_date) array_shift($this->weeks_arr);
        }        
    }

    /*
     * sets the array of month numbers
     */
    protected function setMonths() {
        if (isset($this->row['repeat_duration']) && $this->row['repeat_duration'] == 1) {
            $this->months_arr = range(1, 12 ,1);
        } else {
            $this->months_arr = getMonthNumbers($this->start_date, $this->end_date);
        }      
    }
    
    /*
     * 
     */
    
    public function setLoopArr() {
        if (count($this->weeks_arr) > 0) {
            $this->loop_arr = $this->weeks_arr;
        } else if(count($this->months_arr) > 0) {
            $this->loop_arr = $this->months_arr;
        }
    }
    
    /*
     * 
     */
    public function setPostVars($post_vals, $val) {
        switch($this->repeat_type) {
            case 'weekly':
                $post_vals['taskmonth'] = 0; 
                $post_vals['taskday'] = 0;
                $post_vals['position_in_month'] = 99; 
                $post_vals['day_of_week'] = $this->weekly;
                $post_vals['week_num'] = $val;
                break;
            case 'monthly_by_date':
                $post_vals['day_of_week'] = 0;
                $post_vals['week_num'] = 0;
                $post_vals['position_in_month'] = 99;
                $post_vals['taskday'] = $this->monthly_by_date;
                $post_vals['taskmonth'] = $val;
                break;
            case 'monthly_by_day':
                $post_vals['taskday'] = 0;
                $post_vals['week_num'] = 0;
                $post_vals['day_of_week'] = $this->month_day;
                $post_vals['position_in_month'] = $this->position_in_month;
                $post_vals['taskmonth'] = $val;
                break;
        }
        
        return $post_vals;
    }
    
    
    public function getActionDates($vals, $start, $end) {
        if (isset($vals['week_num']) && $vals['week_num'] > 0) {
            $actiondate = getWeeklyDates($vals, $start, $end);
        } else if (isset($vals['position_in_month']) && $vals['position_in_month'] >= 0 && $vals['position_in_month'] <= 4 ){
            $actiondate = getDayInMonth($vals, $start, $end);
        } else if (isset($vals['taskmonth']) && isset($vals['taskday'])) {
            $actiondate = getMonthDate($vals, $start, $end);
        }
        return strftime("%a, %d %b %Y",strtotime($actiondate));
    }
    /*
     * delete all or just future/current tasks
     * 
     */
    public function deleteAllAccountTasks($con, $min_taskid) {
        global $messages; 
        $qry = new dbUpdate("accounttask");
        $qry->setParam("accounttask_repeatingid", $this->id);
        $qry->setParamGT('accounttaskid', $min_taskid);
        $qry->setParam('accounttaskid',$min_taskid, "or");
        $qry->setReqStringVal("state", "DELETED", "State");
        $qry->setAudit($this->clsUS->userid);
        if(!$qry->execute($con, false)) {
            $messages[] = $qry->getError();
            return false;
        } else {
            return true;
        }        
    }
    
    
    /*
     * method to get the repeating tasks in this series, and convert the calendar values to an actiondate, using the present year.
     * don't get DELETED tasks, but get inactive and active ones and we'll make them all active
     * 
     */
    public function getCurrentTaskDates($con) {     
        $sql = "SELECT actiondate, accounttaskid  FROM accounttask WHERE state NOT IN('DELETED', 'DELETED1', 'DELETED2') and accounttask_repeatingid = ".mysql_real_escape_string($this->id). " order by actiondate asc";
        $rs = getRS($con, $sql);
        $tasks = array();
        
        switch($this->repeat_type) {
            case 'weekly':
                while($row = getRow($rs)) {
                    $tasks[$row['accounttaskid']] = intval(date("W", strtotime($row['actiondate'])));
                }
                break;
            case 'monthly_by_date':
                while($row = getRow($rs)) {
                    $tasks[$row['accounttaskid']] = date("n", strtotime($row['actiondate']));
                }
                break;
            case 'monthly_by_day':
                while($row = getRow($rs)) {
                    $tasks[$row['accounttaskid']] = date("n", strtotime($row['actiondate']));
                }
                break;            
        }
        return $tasks;
    }
    
    
   /*
     * get an array of new dates
     */
    function getTaskDates() {
        $task_dates = array(); // calculated dates
        switch($this->repeat_type) {
            case 'weekly':
                foreach ($this->weeks_arr as $k => $week) {
                    $task_dates[] = $week;
                }
                break;
            case 'monthly_by_date':
                $task_dates =  $this->months_arr;
                break;
            case 'monthly_by_day':
                $task_dates =  $this->months_arr;
                break;
        }
        return $task_dates;
    }
    
    public function getDateVals($val) {
        $post_vals = array();
        switch($this->repeat_type) {
            case 'weekly':
                $post_vals['day_of_week'] = $this->weekly;
                $post_vals['week_num'] = $val;
                break;
            case 'monthly_by_date' :
                $post_vals['taskday'] = $this->monthly_by_date;
                $post_vals['taskmonth'] = $val;
                break;
            case 'monthly_by_day':
                $post_vals['day_of_week'] = $this->month_day;
                $post_vals['position_in_month'] = $this->position_in_month;
                $post_vals['taskmonth'] = $val;
                break;
        }
        return $post_vals;        
    }
    
    /*
     * when editing an accounttask that is in a repeating task series, the user can move the day part of the task.
     */
    public function updateAccounttaskDays($con, $vars, $accounttask, $end, $start) {
        $sql = "select t.*, act.accounttaskid from task t left join accounttask act on t.taskid = act.taskid where act.licenceid = {$accounttask->licenceid} and t.repeatingtaskid = {$accounttask->repeatingtaskid}";
         $rs = getRS($con, $sql);
        switch($this->repeat_type) {
        case 'weekly':
            //get min week_num of tasks were going to change
            //$week_num = getWeekNumbers($start, $end);
            if ($vars['edit_type'] == 2 || $vars['edit_type'] == 0) {
                //need to get array of future week numbers
                //if ($vars['edit_type'] == 2) $week_num = getWeekNumbers($accounttask->actiondate, $end);
                while ($row = getRow($rs)) {
                    $row['day_of_week'] = $vars['weekly'];
                    //update accounttask date
                    updateAccountTaskDates($con, $this->clsUS, $row['accounttaskid'], $row, $start, $end, false);
                }
            } else {
                //select all this task week_num
                $sql = "select t.*, act.accounttaskid from task t left join accounttask act on t.taskid = act.taskid where act.accounttaskid = {$accounttask->accounttaskid}";
                $rs = getRS($con, $sql);
                $row = getRow($rs);
                $row['day_of_week'] = $vars['weekly'];
                //update accounttask date
                updateAccountTaskDates($con, $this->clsUS, $accounttask->accounttaskid, $row, $start, $end, false);
            }
            break;
        case 'monthly_by_date' :
            if ($vars['edit_type'] == 2 || $vars['edit_type'] == 0) {
                while ($row = getRow($rs)) {
                    $row['taskday'] = $vars['month_date'];
                    //update accounttask date
                    updateAccountTaskDates($con, $this->clsUS, $row['accounttaskid'], $row, $start, $end, false);
                }
            } else {
                $sql = "select t.*, act.accounttaskid from task t left join accounttask act on t.taskid = act.taskid where act.accounttaskid = {$accounttask->accounttaskid}";
                $rs = getRS($con, $sql);
                $row = getRow($rs);
                $row['taskday'] = $vars['month_date'];
                //update accounttask date
                updateAccountTaskDates($con, $this->clsUS, $accounttask->accounttaskid, $row, $start, $end, false);
            }            
            
            break;
        case 'monthly_by_day':         
            if ($vars['edit_type'] == 2 || $vars['edit_type'] == 0) {
                while ($row = getRow($rs)) {
                    $row['day_of_week'] = $vars['month_day'];
                    $row['position_in_month'] = $vars['position_in_month'];
                    //update accounttask date
                    updateAccountTaskDates($con, $this->clsUS, $row['accounttaskid'], $row, $start, $end, false);
                }
            } else {
                $sql = "select t.*, act.accounttaskid from task t left join accounttask act on t.taskid = act.taskid where act.accounttaskid = {$accounttask->accounttaskid}";
                $rs = getRS($con, $sql);
                $row = getRow($rs);
                $row['day_of_week'] = $vars['month_day'];
                $row['position_in_month'] = $vars['position_in_month'];
                //update accounttask date
                updateAccountTaskDates($con, $this->clsUS, $accounttask->accounttaskid, $row, $start, $end, false);
            }                  
            
            
            break;
        }    
    }
} // EOC