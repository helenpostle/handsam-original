<?php

/**
 * Return all superadmin Files associated with a task
 */
function getTaskSuperAdminFiles($taskid, $con)
{
	$sql = "SELECT tf.taskfileid AS module_fileid, f.fileid, f.filename, f.title, f.filetype, f.filecategory FROM task_files AS tf JOIN files AS f ON tf.fileid = f.fileid WHERE tf.taskid = '$taskid'";
	
	return getRS($con, $sql);
}

/**
 * Return all superadminLinks associated with a task
 */
function getTaskSuperAdminLinks($taskid, $con)
{
	$sql = "SELECT tl.tasklinkid AS module_linkid, l.linkid, l.title, l.url, l.linktype, l.linkcategory FROM task_links AS tl JOIN links AS l ON tl.linkid = l.linkid WHERE tl.taskid = '$taskid'";
	return getRS($con, $sql);
}


/**
 * Return all superadmin Files associated with a task
 */
function getTaskAdminFiles($taskid, $con)
{
	$sql = "SELECT tf.taskfileid AS module_fileid, f.fileid, f.filename, f.title, f.filetype, f.filecategory FROM task_files AS tf JOIN files AS f ON tf.fileid = f.fileid WHERE (tf.accounttaskid > 0 AND tf.accounttaskid = '$taskid') and f.filetype='adminDoc'";
	
	return getRS($con, $sql);
}

/**
 * Return all superadminLinks associated with a task
 */
function getTaskAdminLinks($taskid, $con)
{
	$sql = "SELECT tl.tasklinkid AS module_linkid, l.linkid, l.title, l.url, l.linktype, l.linkcategory FROM task_links AS tl JOIN links AS l ON tl.linkid = l.linkid WHERE tl.accounttaskid = '$taskid'";
	return getRS($con, $sql);
}


/**
 * Return all superadmin Files associated with a task
 */
function getTaskUserFiles($userid, $taskid, $con)
{	
	$sql = "SELECT tf.taskfileid AS module_fileid, f.fileid, f.filename, f.title, f.filetype, f.filecategory FROM task_files AS tf JOIN files AS f ON tf.fileid = f.fileid WHERE tf.accounttaskid = '$taskid' and f.filetype='userDoc' and f.userid=$userid";
	return getRS($con, $sql);
}

/**
 * Return all superadminLinks associated with a task
 */
function getTaskUserLinks($userid, $taskid, $con)
{
	$sql = "SELECT tl.tasklinkid AS module_linkid, l.linkid, l.title, l.url, l.linktype, l.linkcategory FROM task_links AS tl JOIN links AS l ON tl.linkid = l.linkid WHERE tl.accounttaskid = '$taskid'  and l.filetype='userDoc' and l.userid=$userid";
	return getRS($con, $sql);
}
?>