<?php

#### returns next day up to and including end date

function datePeriod($thisDate, $endDate, $datesInRange) {
	
	#first date = start date
	//$datesInRange[] = date("j-n-Y", strtotime($thisDate));
	$datesInRange[] = date("Y-n-j", strtotime($thisDate));
	#loop through all dates in range
	#put date in dd-mm-yyyy format
	//$thisDate = date("j-n-Y", strtotime($thisDate));
	//$endDate = date("j-n-Y", strtotime($endDate));
	$thisDate = date("Y-n-j", strtotime($thisDate));
	$endDate = date("Y-n-j", strtotime($endDate));
	while (strtotime($thisDate) < strtotime($endDate)) {
		#break up date into days, month and year
		$thisMonth = date("n", strtotime($thisDate));
		$thisDay = date("j", strtotime($thisDate));
		$thisYear = date("Y", strtotime($thisDate));
		#gets days in this month
		$daysinmonth = date("t", mktime(0, 0, 0, $thisMonth, 1, $thisYear));
		#add one to day and get next day. modify month and year if at end of either.
		if ($thisDay >= $daysinmonth) {
			$nextDay = 1;
			$nextMonth = $thisMonth + 1;
		} else {
			$nextDay = $thisDay + 1;
			$nextMonth = $thisMonth;
		}
		if ($nextMonth > 12) {
			$nextMonth = 1;
			$nextYear = $thisYear + 1;
		} else {
			$nextYear = $thisYear;
		}
		//$datesInRange[] = $nextDay."-".$nextMonth."-".$nextYear;
		$datesInRange[] = $nextYear."-".$nextMonth."-".$nextDay;
		//$thisDate = $nextDay."-".$nextMonth."-".$nextYear;
		$thisDate = $nextYear."-".$nextMonth."-".$nextDay;
	}
	return $datesInRange;
}
	
#### returns array of available dates in the month
function availableDates($thisYear, $thisMonth, $thisDay, $holidayPeriods, $daysOff) {
	//echo "<br/>day: ".$thisDay." - month: ".$thisMonth." - year: ".$thisYear;
	//echo "<br/><br/> holiday periods: ";
	//var_dump($holidayPeriods);
	//echo "<br/><br/>";
	$available_dates = array();
	$daysinmonth = date("t", mktime(0, 0, 0, $thisMonth, 1, $thisYear));
	while ($thisDay < $daysinmonth) {
		//$dayofweek = date("l", strtotime($thisDay."-".$thisMonth."-".$thisYear));
		$dayofweek = date("l", strtotime($thisYear."-".$thisMonth."-".$thisDay));
		//if (!in_array($dayofweek, $daysOff) && !in_array($thisDay."-".$thisMonth."-".$thisYear, $holidayPeriods)) {
		if (!in_array($dayofweek, $daysOff) && !in_array($thisYear."-".$thisMonth."-".$thisDay, $holidayPeriods)) {
			//$available_dates[] = $thisDay."-".$thisMonth."-".$thisYear;
			$available_dates[] = $thisYear."-".$thisMonth."-".$thisDay;
		}
		$thisDay++;
	}
return	$available_dates;	
}


/*
 * get taskday and taskmonth from weekly repeat criteria
 */
function getWeeklyDates($vals, $start, $end) {
    
    //get year of start and end
    $y_start = date('Y',strtotime($start));
    $y_end = date('Y',strtotime($end));
    if ($vals['week_num'] < 10) $vals['week_num'] = "0".$vals['week_num'];
    
    //echo "<br/>week: ".$vals['week_num'];
    //gets first day in week 1 and then adds n weeks 
    $taskday_1 = date("Y-m-d", strtotime($y_start."W".$vals['week_num'].$vals['day_of_week']));
    //get date in year 2
    $taskday_2 = date("Y-m-d", strtotime($y_end."W".$vals['week_num'].$vals['day_of_week']));
    //compare to find which date is in the licence period
    //echo "<br/>".$taskday_1;
    //echo "<br/>".$taskday_2;
    //die();
    if ($start <= $taskday_1 && $taskday_1 <= $end) {
        //echo "<br/>".$taskday_1." wk: ".strftime('%W', strtotime($taskday_1));
        return $taskday_1;
    } else {
        //echo "<br/>".$taskday_2." wk: ".strftime('%W', strtotime($taskday_2));
        return $taskday_2;
    }
}

/*
 * get taskday and taskmonth from first in month repeat criteria
 */
function getDayInMonth($vals, $start, $end) {
    //uses $vals['day_of_week'], $vals['position_in_month'], $vals['taskmonth'], licence year- $y_start and $y_end to get the exact date
    $days = array(1=>"monday", 2=>"tuesday", 3=>"wednesday", 4=>"thursday", 5=>"friday", 6=>"saturday", 7=>"sunday");
    $position = array('first', 'second', 'third', 'fourth', 'last');
    if (isset($position[$vals['position_in_month']])) {
        $position_txt = $position[$vals['position_in_month']];
    } else {
        $position_txt = "first";
    }
    //get year of start and end
    $y_start = date('Y',strtotime($start));
    $y_end = date('Y',strtotime($end));
    
    //gets first day of month
    $taskday_1 = date("Y-m-d",strtotime(" $position_txt ".$days[$vals['day_of_week']]." of ".$y_start."-".$vals['taskmonth']));
    $taskday_2 = date("Y-m-d",strtotime(" $position_txt ".$days[$vals['day_of_week']]." of ".$y_end."-".$vals['taskmonth']));
     
    if ($start <= $taskday_1 && $taskday_1 <= $end) {
        return $taskday_1;
    } else {
        return $taskday_2;
    } 
}


/*
 * get taskday and taskmonth from last in month repeat criteria
 */
function getMonthDate($vals, $start, $end) {
    $y_start = date('Y',strtotime($start));
    $y_end = date('Y',strtotime($end));

    $day1 = $vals['taskday'];
    $day2 = $vals['taskday'];
    
    //gets last day of month
    $last_date_1 = date('t', strtotime($y_start."-".str_pad($vals['taskmonth'], 2, '0', STR_PAD_LEFT)."-01"));
    $last_date_2 = date('t', strtotime($y_end."-".str_pad($vals['taskmonth'], 2, '0', STR_PAD_LEFT)."-01"));
    
    if ($last_date_1 < $day1) $day1 = $last_date_1;
    if ($last_date_2 < $day2) $day2 = $last_date_2;
    
    $taskday_1 = $y_start."-".str_pad($vals['taskmonth'], 2, '0', STR_PAD_LEFT)."-".$day1;
    $taskday_2 = $y_end."-".str_pad($vals['taskmonth'], 2, '0', STR_PAD_LEFT)."-".$day2;

    if ($start <= $taskday_1 && $taskday_1 <= $end) {
        return $taskday_1;
    } else {
        return $taskday_2;
    } 
}


function getDateWithYear($day, $month, $start, $end) {
    
    $Y1 = date("Y", strtotime($start));
    $Y2 = date("Y", strtotime($end));
    
    $date1 = date("Y-m-d", strtotime("$Y1-$month-$day"));
    $date2 = date("Y-m-d", strtotime("$Y2-$month-$day"));
    
    if ($start <= $date1 && $date1 <= $end) {
        return $date1;
    } else {
        return $date2;
    }    
}