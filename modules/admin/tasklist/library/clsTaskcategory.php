<?php

/**
 * clsTaskcategory kind of a simple crud class.  
 * 
 */

class taskcategory {
    
    public $categoryid; //taskid
    public $rs; //the result set row - this is used to integrate with older functionality and can be removed when that is updated.
    public $row; //same as above - can be removed later
    public $categoryname; //
    public $customtext;
    

    
    public function __construct($categoryid=0) {
        $this->categoryid = $categoryid;
    }
    
    public function get($con) {
        $qry = "select * from taskcategory where categoryid = {$this->categoryid}";
        $rs = getRS($con, $qry);
        $row = getRow($rs);
        $this->rs = $rs;
        $this->row = $row;
        $this->categoryname = $row['categoryname'];
        $this->customtext = $row['customtext'];
        
    }
    
    public function getCatName() {
        if ($this->customtext != '') return $this->customtext;
        return $this->categoryname;
    }
}
?>