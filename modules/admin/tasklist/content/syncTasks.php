<?php 
/**
 * Alert super admin users of sync task changes
 */

if (isAllowed('editAccount')) {
    echo "<h2>Recently Synchronised Tasks</h2>";
    if (count($sync_list) > 0 ) {
        
		$taskListTable = "<table class=\"acc_list\">";
		$taskListTable .= "<thead>";
		$taskListTable .= "<tr>";
		$taskListTable .= "<th class=\"date nolink start\">Date Updated</th>";
		$taskListTable .= "<th class=\"alerttasktext nolink end\">Task Description</th>";
		$taskListTable .= "</tr>";
		$taskListTable .= "</thead>";
		$taskListTable .= "<tbody>";

		//stuff for hilighting  alternate days in month and category view
		foreach ($sync_list as $rowSync) {
            $rowClassHighlight = "";
			#sort out action date into month year and day


            $link1 = "$rootUrl/modules/tasklist/taskadmin.php?taskid=".$rowSync["taskid"]."&tasklistid=".$rowSync["tasklistid"]."&return=index.php";
            $taskListTable .= "<tr class=\"\">";
            $taskListTable .= "<td class=\"start day\"><a href=\"".$link1."\">".displayDate($rowSync["edited"])."</a></td>";
            $taskListTable .= "<td class=\"alerttasktext flag end\"><a href=\"".$link1."\">".showAbstract($rowSync["tasktext"], 100)."</a></td>";		
            // $taskListTable .= "<td class=\"\"></td>";
            $taskListTable .= "</tr>";
		
		} 
		$taskListTable .= "</tbody>";
		$taskListTable .= "<tfoot>";
		$taskListTable .= "<tr>";
		$taskListTable .= "<td class=\"start\"></td>";
		$taskListTable .= "<td class=\"end\" colspan=\"1\"></td>";
		$taskListTable .= "</tr>";
		$taskListTable .= "</tfoot>";
		$taskListTable .= "</table>";
        echo $taskListTable;
	} else {
        echo "<p>There are no updates from Handsam Sync.</p>";
    }
}