<?php
/***
** Shows the list of task categories in the system
** 
** 
***/

$order = "categoryname";
//if(isset($_GET["order"])){
//	$order = $_GET["order"];
//}

$qry = "SELECT * FROM taskcategory where state != 'DELETED' and tasklistid = $tasklistid order by ".$order;
/* don't do pages for now! 
if (!isset($HTTP_GET_VARS["page"])) $page = 1;
else $page = $HTTP_GET_VARS["page"];

$limit = 40;
$count = getResultRows(getRS($con, $qry));

$start = ($page - 1) * $limit;
$pages = (($count % $limit) == 0) ? $count / $limit : floor($count / $limit) + 1;
$result = getRS($con, $qry." limit $start, $limit");

echo pageNavigator("users.php", "order=".$order, $page, $pages);
		//echo "<center>".$pageNav."</center>";
*/

$result = getRS($con, $qry);

		
?>
<table class="acc_list">
	<caption class="hidden"><?php echo $title;?></caption>
	<thead>
		<tr>
			<th class="start ">Category name</th>
			<th class="end ">State</th>
		</tr>
	</thead>
	<tbody>
<?php
while ($row = getRow($result)) {
	$itemname = trim($row["customtext"]);
	if ($itemname == '') $itemname = $row['categoryname'];
?>
  <tr>
	<td class="start catname"><a href="taskcategory.php?categoryid=<?php echo $row["categoryid"] ?>&amp;tasklistid=<?php echo $tasklistid;?>"><?php echo $itemname ?></a></td>
	<td class="end state"><?php echo displayText($row["state"]) ?></td>
  </tr>
<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"><a href="taskcategory.php?categoryid=0&amp;tasklistid=<?php echo $tasklistid;?>">Add a new task category</a></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>

</table>
