<?php 
/**
 * Shows a form for updating a user
 */
if (isset($accountid)) {
		
	$rsAccount = getRS($con,"select account.*, accountlicence.tasklistid as tasklistid from account left join accountlicence on account.accountid = accountlicence.accountid where account.accountid = $accountid");
	$rowAccount = getRow($rsAccount); 
	
	if ($rowAccount['accountlogo'] != "" && file_exists("$rootPath/account_uploads/account_logo/".$rowAccount['accountlogo'])) {
		$thumb = "<img src=\"$rootUrl/account_uploads/account_logo/".$rowAccount['accountlogo']."\"/><a class=\"deleteThumb\" href=\"?accountid=$accountid\">[delete]</a>";
	} else {
		$thumb = "";
	}	

	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	//$tasklist_qry = "select tasklistid as a, tasklistname as b from tasklist";
	echo startFormTable($_SERVER["PHP_SELF"]);
	echo frmLegend('Edit Account');
	echo formNotes(ACCOUNTS_FORM_TITLE, ACCOUNTS_FORM_INFO);
	//if ($accountid == 0) {
	//echo frmMultiSelectQuery($rsAccount,"tasklistid",$tasklist_qry,$con,"Task Lists",true);
	//	echo frmSelectQuery($rowAccount,"tasklistid","select tasklistid as a, tasklistname as b from tasklist",$con,"Task list",true);
	//}
	echo frmHiddenField($accountid,"accountid");
	echo frmHiddenField($tasklist,"tasklist");
	echo frmHiddenField("United Kingdom","country");
	echo frmTextField($rowAccount,"accountname",50,"Account name",true);
	echo frmTextField($rowAccount,"address1",50,"Address 1",true);
	echo frmTextField($rowAccount,"address2",50,"Address 2",false);
	echo frmTextField($rowAccount,"town",30,"Town",true);
	echo frmTextField($rowAccount,"county",30,"County",true);
	echo frmTextField($rowAccount,"postcode",10,"Post Code",true);
	echo frmTextField($rowAccount,"email",100,"Email address",true);
	echo frmTextField($rowAccount,"phone",20,"Telephone",true);
	echo frmSelectArray($rowAccount, "state" , $state, "State" , true);
	if ($accountid > 0) {
		echo "<div id=\"accountlogo\">";
		echo "<div class=\"optional\"><label for=\"upload_logo\">Upload logo</label><div id=\"upload_logo\" class=\"swfupload-control\"><span id=\"spanButtonPlaceholder\"></span></div></div>";
		echo "<div id=\"divFileProgressContainer\"></div>";
		echo "<div id=\"thumbnail\">$thumb</div>";
		echo "</div>";
	}
	
	if ($accountid != 0 && isAllowed("editAccount")) {
		echo frmShowAudit($rowAccount,$con);	
	}
	
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($accountid != 0 && isAllowed("editAccount"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this account?", "Click here to delete this account");
	
	echo endFormTable();
} 

?>