<?php
/***
** Shows the list of users in the system
** @todo make it so clicking on colunm headings reorders list? or add dropdown to view only a certain usertype? or alphabetical listing?
** i.e. make it easier for when there are loads of users
***/

$order = "usr_usrtype.usertype, lastname";
//if(isset($_GET["order"])){
//	$order = "usr_usrtype.usertype, ".$_GET["order"];
//}
//$order = "usertype";
$qry = "SELECT usr.*, usr_usrtype.usertype FROM usr left join account on usr.accountid = account.accountid left join usr_usrtype on usr.userid = usr_usrtype.userid where account.accountid = ".$clsUS->accountid." and usr.state != 'DELETED'  order by ".$order;

/* don't do pages for now! 
if (!isset($HTTP_GET_VARS["page"])) $page = 1;
else $page = $HTTP_GET_VARS["page"];

$limit = 40;
$count = getResultRows(getRS($con, $qry));

$start = ($page - 1) * $limit;
$pages = (($count % $limit) == 0) ? $count / $limit : floor($count / $limit) + 1;
$result = getRS($con, $qry." limit $start, $limit");

echo pageNavigator("users.php", "order=".$order, $page, $pages);
		//echo "<center>".$pageNav."</center>";
*/

$result = getRS($con, $qry);

		
?>
<table  id="user_list" class="list">
	<caption class="hidden"><?php echo $title;?></caption>
	<thead>
		<tr>
			<th class="start "><a href="?order=username">Username</a></th>
			<th class=""><a href="?order=firstname">Name</a></th>
			<th class=""><a href="">User Type</a></th>
			<th class="state">Email Alerts</th>
			<th class="end "width="80"><a href="?order=state">State</a></th>
		</tr>
	</thead>
	<tbody>

<?php
$last_usr = array();
while ($row = getRow($result)) {
		if (!in_array($row['userid'],$last_usr)) {
		$last_usr[] = $row['userid'];
		$usertype = displayText($row["usertype"]);
		if ($row["late_task_email_alert"] == 1) {
			$checked = " checked ";
		} else {
			$checked = "";
		}
	?>
			<tr>
				<td class="start"><a href="users.php?userid=<?php echo $row["userid"] ?>"><?php echo $row["username"] ?></a></td>
				<td class=""><?php echo displayText($row["firstname"]) ?> <?php echo displayText($row["lastname"]) ?></td>
				<td class=""><?php echo displayText($row["usertype"]) ?></td>
				<td class="state"><input class="table_check_box" type="checkbox" value="<?php echo $row["userid"]; ?>" name="email_alert[]" <?php echo $checked; ?>/></td>
				<td class="end"><?php echo displayText($row["state"]) ?></td>
			</tr>
	<?php 
	}
} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"><a href="users.php?userid=0">Add a new user</a></td>
			<td colspan="3"></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>

</table>

