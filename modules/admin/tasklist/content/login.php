<?php
/*
 * login page
 */
 
?>
<form method="post" action="<?php echo($_SESSION["cameFrom"])?>" name="loginform">
	<fieldset>
			<div class="required">
				<label for="f_login">Username</label>
				<input title="username" class="inputText"  id="f_login" type="text" name="f_login"/>
			</div>
			<div class="required"> 
				<label for="f_password" >Password</label>
				<input  title="password" class="inputText" type="password" name="f_password"/>
			</div>
			<div class="optional">
				<label for="rememberme"> Remember Me </label>
				<input title="remember me" class="inputCheckbox" type="checkbox" name="rememberme" value="yes"/>
				<input title="submit"  class="inputSubmit" type="submit" name="submit" value="log in"/>
			</div>
			<div>
			<a class="page formM" href="forgot.php" title="Have you forgotten your password?">Forgotten your password?</a>
			</div>
	</fieldset>
</form>

<script language="JavaScript" type="text/JavaScript">
document.forms[0].f_login.focus();
</script>

