<?php 
/**
 * Shows a form for editing a task
 * 
 * Task CRUD form
 * 
 */

#create users array to include 'no user'

if (isset($taskid)) { 
	if ($taskid > 0 || $categoryid > 0) { 
		$box[] = "box.adminTasksInCategory.php";
	}
    
    $sync_change_cnt = 1;
    
	$rsTask = getRS($con,"select * from task where task.taskid = $taskid");
	$rowTask = getRow($rsTask);
	if (!isset($categoryid)) {
		$categoryid = $rowTask['categoryid'];
	}
	if (!isset($month)) {
		$month =  $rowTask['taskmonth'];
	}
    
    //check for sync changes for the form class and info/title
    $sync_class = "";
    $frm_title = TASKADMIN_FORM_TITLE;
    $frm_info = TASKADMIN_FORM_INFO;
    if (isset($task_hist) && ($rowTask['tasktext'] != $task_hist['tasktext'] || $rowTask['taskday'] != $task_hist['taskday'] || $rowTask['taskmonth'] != $task_hist['taskmonth'] || $rowTask['categoryid'] != $task_hist['categoryid'] || $rowTask['state'] != $task_hist['state'])) {
        $sync_class = "task_sync";
        $frm_title = "Updated Task details.";
        $frm_info = "This task contains recent Handsam updates. You must tick the ALL of 'Changes Accepted' boxes before this form can be saved. You must save this form to remove it from the home page alert list.";
    }
    
	echo startFormTable($_SERVER["PHP_SELF"], $sync_class, "", $frm_title, $frm_info);
        
	echo frmHiddenField($taskid,"taskid");
	echo frmHiddenField($option,"option");
	echo frmHiddenField($month,"month");
	echo frmHiddenField($category,"category");
	echo frmHiddenField($multi,"multi");
    if (isset($return)) echo frmHiddenField($return,"return");
	echo frmHiddenField($tasklistid,"tasklistid");

    if(buttonClicked('edit_repeat')) {
        include ("$rootPath/modules/tasklist/content/admintaskedit_disabled.php");
        $edit_task = false;
    } else {
        include ("$rootPath/modules/tasklist/content/admintaskedit_enabled.php");
        $edit_task = true;
    }
    /**
     * repeating tasks
     */
    
    if(isset($Task) && $Task->repeatingtaskid > 0 && !buttonClicked('edit_repeat')) {
       
        include ("$rootPath/modules/tasklist/content/admintaskrepeat_disabled.php");
    } else {
        
        include ("$rootPath/modules/tasklist/content/admintaskrepeat_enabled.php");
    }


	//show audit if not new task
	if ($taskid != 0 && isAllowed("editTask")) {
		echo frmShowAudit($rowTask,$con);	
	}	
		
	if ($edit_task) {
        echo frmButtonHelp("Save","save", "Save your changes");
    } else {
        echo frmButtonHelp("Save","save_repeating", "Save repetition changes");
    }
    
	echo frmButtonHelp("Cancel","cancel", "Cancel and return");
        
	if ($taskid != 0 && $edit_task) {
        echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this task?", "Delete this task");
    }
    
    if(isset($Task) && $Task->repeatingtaskid > 0 && $edit_task) {
        echo frmButtonConfirmHelp("Delete All Tasks","delete_all_rpt","Are you sure you want to delete ALL repeating tasks in this series?", "Delete ALL repeating task");
        echo frmButtonConfirmHelp("Delete FUTURE Tasks","delete_future_rpt","Are you sure you want to delete THIS and FUTURE repeating tasks in this series?", "Delete FUTURE repeating task");
    }
        
	echo endFormTable();
} 
