<?php 
/**
 * Shows a form for completeing task and adding notes
 * 
 */
$edit_form = true;

$rsAccountTask = getRS($con,"select a.*, t.customtext as category from accounttask a left join taskcategory t on a.categoryid = t.categoryid where a.accounttaskid = $accounttaskid and a.accountid = $accountid and a.assignedto = $userid and (a.state = 'ACTIVE' || a.state = 'EDITED' || a.state = 'COMPLETED')");

if (getResultRows($rsAccountTask) > 0) {
	$rowAccountTask = getRow($rsAccountTask);
    $frm_class = "";
    $key_task_title = "";
    if ($rowAccountTask['key_task'] == 1) {
        $frm_class = "key_task";
        $key_task_title = ": <span>THIS IS A KEY TASK</span>";
    }

	$licenceid = $rowAccountTask['licenceid'];
	#now gt licence end date:

	$qry = "SELECT licenceid, end_date FROM accountlicence where licenceid = ".$licenceid;
	$result = getRS($con, $qry);
	$row = getRow($result);
	$licence_end = $row['end_date'];

				
	if ($rowAccountTask['state'] == "COMPLETED") {
		$edit_form = false;		
	}
	
	echo "<h2>Category: ".$rowAccountTask['category']."</h2>";
	
	echo startFormTable($_SERVER["PHP_SELF"], $frm_class, "", MYTASK_FORM_TITLE.$key_task_title, MYTASK_FORM_INFO);
	echo frmLegend('Task Details');
	//echo formTopNotes(MYTASK_FORM_TITLE, MYTASK_FORM_INFO);
	echo frmHiddenField($accounttaskid,"accounttaskid");
	echo frmHiddenField($option,"option");
	echo frmHiddenField($month,"month");
	echo frmHiddenField($year,"year");
	echo frmHiddenField($return,"return");
	echo frmHiddenField($licenceid,"licenceid");
  echo frmHiddenField($task_search,"task_search");

	//enter task list name 
	echo frmTextAreaNonEdit($rowAccountTask,"customtext","Task");	
	
	//Handsam task documents goes through category array set in config. Add new cat to config to add a new category to this and file admin forms

	
	$cnt = 0;
	if ($rowAccountTask['taskid'] > 0) { 

		foreach($docCategory as $k => $aCat) {
			if ($cnt == 1) {
				if ($tasklist_conf['collapse_mytask_resources'] === true) echo "<h2 id=\"task_resources_title\">Click to show task resources <span class=\"task_show_hide\">+</span><span style=\"display: none;\" class=\"task_show_hide\">-</span></h2>";
				echo "<div id=\"task_resources\">";	
			}
			$cnt ++;
			echo "<div class=\"frm_row clearfix\" >\n";
			echo "<span class=\"label\">".$aCat." Resources</span>\n";
			echo "<span class=\"value\">\n<span class=\"docsList\">\n<ul>";
			$qryAttachedFiles = "SELECT task_files.taskfileid, files.fileid, files.title FROM files left join task_files on files.fileid = task_files.fileid WHERE task_files.taskid = ".$rowAccountTask['taskid']." and files.filetype='handsamDoc' and filecategory='$k'";
			//echo $qryAttachedFiles;
			$resultAttachedFiles = getRS($con, $qryAttachedFiles);
			while ($rowAttachedFile = getRow($resultAttachedFiles)) {
				echo "<li class=\"attachment\"><a href=\"$rootUrl/handsam_core/downloadDocument.php?id=".$rowAttachedFile['fileid']."&amp;accounttaskid=$accounttaskid\">".$rowAttachedFile['title']."</a></li>";
			}
			$qryAttachedLinks = "SELECT task_links.tasklinkid, links.linkid, links.url, links.title FROM links left join task_links on links.linkid = task_links.linkid WHERE task_links.taskid = ".$rowAccountTask['taskid']." and links.linktype='handsamDoc' and linkcategory='$k'";
			$resultAttachedLinks = getRS($con, $qryAttachedLinks);
			while ($rowAttachedLink = getRow($resultAttachedLinks)) {
				echo "<li class=\"link\"><a target=\"_blank\" href=\"".$rowAttachedLink['url']."\">".$rowAttachedLink['title']."</a></li>";
			}
			
			
			echo "</ul>\n</span>\n</span>\n";
			echo "</div>\n";
		}
        if ($cnt > 0) echo "</div>";
		
	}
	
	// your admin account documents	
	echo "<div class=\"frm_row clearfix\">\n";
	echo "<span class=\"label\">Local Procedures</span>\n";
	echo "<span class=\"value\">\n<span class=\"docsList\">\n<ul>";
	$qryAttachedFiles = "SELECT task_files.taskfileid, files.fileid, files.title FROM files left join task_files on files.fileid = task_files.fileid WHERE task_files.accounttaskid = $accounttaskid and files.filetype='adminDoc'";
	$resultAttachedFiles = getRS($con, $qryAttachedFiles);
	while ($rowAttachedFile = getRow($resultAttachedFiles)) {
		echo "<li class=\"attachment\" id=\"doc".$rowAttachedFile['taskfileid']."\"><a href=\"$rootUrl/handsam_core/downloadDocument.php?id=".$rowAttachedFile['fileid']."&amp;accounttaskid=$accounttaskid\">".$rowAttachedFile['title']."</a></li>";
	}	
	echo "</ul>\n</span>\n</span>\n";
	echo "</div>\n";	
	
	echo frmTextAreaNonEdit($rowAccountTask, "budget", "Budget (&pound;)");
	echo frmTextAreaNonEdit($rowAccountTask,"notes","Additional Notes");
	
	//echo "</div>"; // closing tag for div#task_resources

	if ($edit_form) {
		
		$fileDB = getTaskUserFiles($userid, $accounttaskid, $con);
		addFileField($fileDB, false);
		//attach docs link
		if (strtotime($licence_end) > time() && ($accountid == 0 || $accountid == $clsUS->accountid)) { #only display if licence still not expired

			echo "<div class=\"frm_row noLabel\"><a href = \"#\" onclick='openWin(\"$rootUrl/handsam_core/docList.php?filetype=userDoc\",840,600,\"fileBrowser\")'>Attach a feedback document to this task</a></div>";
		}
		
		
		echo frmTextArea($rowAccountTask,"feedback",5,"Feedback",false);
		$booleanTrue = "COMPLETED";
		echo frmCheckBox($rowAccountTask,"state","Task Completed");
		$booleanTrue = 1;
	} else {
		echo frmDateNonEdit($rowAccountTask, "completedate", "Date Completed");
		if ($rowAccountTask['completedate'] > $rowAccountTask['actiondate']) {
			echo "<p class=\"forminfo red\"><img src=\"$rootUrl/images/icon_error_red.gif\" alt=\"\"/>This task was completed late</p>";
		}	
		echo frmDateNonEdit($rowAccountTask, "actiondate", "Action Date");

		echo "<div class=\"frm_row clearfix\">\n";
		echo "<span class=\"label\">Feedback Documents</span>\n";
		echo "<span class=\"value\">\n<span class=\"docsList\">\n<ul>";
			$qryAttachedFiles = "SELECT task_files.taskfileid, files.fileid, files.title FROM files left join task_files on files.fileid = task_files.fileid WHERE task_files.accounttaskid = $accounttaskid and files.filetype='userDoc' and files.userid=$userid";
		$resultAttachedFiles = getRS($con, $qryAttachedFiles);
		while ($rowAttachedFile = getRow($resultAttachedFiles)) {
			echo "<li  class=\"attachment\" id=\"doc".$rowAttachedFile['taskfileid']."\"><a href=\"$rootUrl/handsam_core/downloadDocument.php?id=".$rowAttachedFile['fileid']."&amp;accounttaskid=$accounttaskid\">".$rowAttachedFile['title']."</a></li>";
		}
		echo "</ul>\n</span>\n</span>\n";
		echo "</div>\n";
		echo frmTextAreaNonEdit($rowAccountTask, "feedback", "Feedback");
	}
	
	
	?>
	<?php	
	if ($accounttaskid != 0 && isAllowed("editTask")) {
		echo frmShowAudit($rowAccountTask,$con);	
	}?>
	
	
	<?php
    
	if ($edit_form) {
		if (strtotime($licence_end) > time()) { #only display if licence still not expired
			echo frmButtonHelp("Save","save", "Click here to save changes");
		}
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	} else {
		echo frmButtonHelp("Back","back", "Click here to go Back");
	}
	
	echo endFormTable();
}
?>

