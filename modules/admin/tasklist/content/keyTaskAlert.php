<?php 
/**
 * Alert users of late and coming soon tasks
 */
 
//if (!isAllowed('editTask') && isAllowed('myTasks')) {
if (isAllowed('myTasks')) {
	if ($display_my_list) {
	
		$taskListTable = "<table class=\"acc_list\">";
		$taskListTable .= "<caption>".$caption."</caption>";
		$taskListTable .= "<thead>";
		$taskListTable .= "<tr>";
		$taskListTable .= "<th class=\"date nolink start\">Action Date</th>";
		$taskListTable .= "<th class=\"icon\"></th>";
		$taskListTable .= "<th class=\"alerttasktext nolink end\">Task Description</th>";
		//$taskListTable .= "<th class=\"nolink end\"></th>";
			
		$taskListTable .= "</tr>";
		$taskListTable .= "</thead>";
		$taskListTable .= "<tbody>";
		$daynum = 0;
		$rowClassOdd = "";
		$rowcnt = 0;
		$trid = "";
		$todayTask = 0;
		
		$lateTask = 0;
		
		//stuff for hilighting  alternate days in month and category view
		while ($row = getRow($tasksresult)) {
			#sort out action date into month year and day
			$actionyear = date("Y", strtotime($row["actiondate"]));
			$actionmonth = date("n", strtotime($row["actiondate"]));
			$actionday = date("jS", strtotime($row["actiondate"]));
			if (date("j n Y", strtotime($row["actiondate"])) == date("j n Y", time())) {
				$alertImg = "<img src=\"$rootUrl/images/icon_error.gif\" alt=\"This task is late\"/>";
				$alertClass = "today";
				$todayTask ++;
			} else if (strtotime($row["actiondate"]) < time()) {
				$alertImg = "<img src=\"$rootUrl/images/icon_error_red.gif\" alt=\"This task is late\"/>";
				$alertClass = "late";
				$lateTask ++;
			} else {
				$alertImg = "";
				$alertClass = "";
			}
				#set lastmonth for first loop											
				if ($rowcnt == 0) {
					$lastmonth = 0;
					$daynum = 0;
				}
				
				//changes class of alternate day rows in table ijnboth month and category view
				if ($daynum != $actionday && $rowcnt != 0) {
					if($rowClassOdd == "odd") {
						$rowClassOdd = "";
					} else {
						$rowClassOdd = "";
					}
				} 
				$daynum = $actionday;
				//if ($lastmonth != $actionmonth) { 
				//	$taskListTable .=  "<tr id=\"month".$actionmonth."\"class=\"headingRow\"><td class=\"centre start end\" colspan=\"4\">".$months[$actionmonth]." ".$actionyear."</td></tr>";
				//}
				
		
				$lastmonth = $actionmonth;
				$rowcnt++;
				
					$link1 = "$rootUrl/modules/tasklist/mytasks.php?accounttaskid=".$row["accounttaskid"]."&month=".$actionmonth."&year=".$actionyear."&return=index.php";
					$taskListTable .= "<tr ".$trid." class=\"".$rowClassOdd." ".$alertClass."\">";
					$taskListTable .= "<td class=\"start day\"><a href=\"".$link1."\">".displayDate($row["actiondate"])."</a></td>";
					$taskListTable .= "<td class=\"icon\"><a href=\"".$link1."\">".$alertImg."</a></td>";
					$taskListTable .= "<td class=\"alerttasktext end\"><a href=\"".$link1."\"><strong>".$row["categoryname"].":</strong> ".showAbstract($row["customtext"], 100)."</a></td>";		
					// $taskListTable .= "<td class=\"\"></td>";
				 	$taskListTable .= "</tr>";
		
			} 
		$taskListTable .= "</tbody>";
		$taskListTable .= "<tfoot>";
		$taskListTable .= "<tr>";
		$taskListTable .= "<td class=\"start\"></td>";
		$taskListTable .= "<td class=\"end\" colspan=\"2\"></td>";
		//$taskListTable .= "<td class=\"end\"></td>";
		$taskListTable .= "</tr>";
		$taskListTable .= "</tfoot>";
		$taskListTable .= "</table>";
	} else {
		$taskListTable = "";
		$todayTask = 0;
		$lateTask = 0;
	}
	#display account users urgent tasks too if account admin
	
	if (isAllowed("editOurTasks")) {
		$tasksqry = "SELECT usr.username, accounttask.*, taskcategory.categoryname 
            FROM taskcategory 
            LEFT JOIN accounttask ON taskcategory.categoryid = accounttask.categoryid
            left join usr on accounttask.assignedto = usr.userid
            WHERE (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') 
                AND accounttask.accountid='$accountid'
                AND accounttask.assignedto != '$userid'
                AND accounttask.key_task = 1
                AND accounttask.licenceid IN ($licence_qry) 
            ORDER BY accounttask.actiondate;";
        
        
		
		//left join usr_usrtype on usr.userid = usr_usrtype.userid and usr_usrtype = 'Administrator'
		//echo $tasksqry;
		$tasksresult = getRS($con, $tasksqry);
		if (getResultRows($tasksresult) > 0) {
		
			$taskListTable .= "<table class=\"acc_list \">";
			$taskListTable .= "<caption>Other Users' Key Tasks</caption>";
			$taskListTable .= "<thead>";
			$taskListTable .= "<tr>";
			$taskListTable .= "<th class=\"date nolink start\">Action Date</th>";
			$taskListTable .= "<th class=\"icon\"></th>";
			$taskListTable .= "<th class=\" nolink\">Task Description</th>";
			$taskListTable .= "<th class=\"alertusername nolink end\">Username</th>";
				
			$taskListTable .= "</tr>";
			$taskListTable .= "</thead>";
			$taskListTable .= "<tbody>";
			$daynum = 0;
			$rowClassOdd = "";
			$rowcnt = 0;
			$trid = "";
			
			
			//stuff for hilighting  alternate days in month and category view
			while ($row = getRow($tasksresult)) {
				#sort out action date into month year and day
				$actionyear = date("Y", strtotime($row["actiondate"]));
				$actionmonth = date("n", strtotime($row["actiondate"]));
				$actionday = date("jS", strtotime($row["actiondate"]));
				if (date("j n Y", strtotime($row["actiondate"])) == date("j n Y", time())) {
					$alertImg = "<img src=\"$rootUrl/images/icon_error.gif\" alt=\"This task is late\"/>";
					$alertClass = "today";
				} else if (strtotime($row["actiondate"]) < time()) {
					$alertImg = "<img src=\"$rootUrl/images/icon_error_red.gif\" alt=\"This task is late\"/>";
					$alertClass = "late";
				} else {
					$alertImg = "";
					$alertClass = "";
				}
					#set lastmonth for first loop											
					if ($rowcnt == 0) {
						$lastmonth = 0;
						$daynum = 0;
					}
					
					//changes class of alternate day rows in table ijnboth month and category view
					if ($daynum != $actionday && $rowcnt != 0) {
						if($rowClassOdd == "odd") {
							$rowClassOdd = "";
						} else {
							$rowClassOdd = "";
						}
					} 
					$daynum = $actionday;
					//if ($lastmonth != $actionmonth) { 
					//	$taskListTable .=  "<tr id=\"month".$actionmonth."\"class=\"headingRow\"><td class=\"centre start end\" colspan=\"4\">".$months[$actionmonth]." ".$actionyear."</td></tr>";
					//}
					
			
					$lastmonth = $actionmonth;
					$rowcnt++;
					
						$link1 = "$rootUrl/modules/tasklist/tasks.php?accounttaskid=".$row["accounttaskid"]."&month=".$actionmonth."&year=".$actionyear."&return=index.php";
						$taskListTable .= "<tr ".$trid." class=\"".$rowClassOdd." ".$alertClass."\">";
						$taskListTable .= "<td class=\"start date\"><a href=\"".$link1."\">".displayDate($row["actiondate"])."</a></td>";
						$taskListTable .= "<td class=\"icon\"><a href=\"".$link1."\">".$alertImg."</a></td>";
						$taskListTable .= "<td class=\"\"><a href=\"".$link1."\"><strong>".$row["categoryname"].":</strong> ".showAbstract($row["customtext"], 100)."</a></td>";		
						$taskListTable .= "<td class=\"alertusername end\"><a href=\"".$link1."\">".textSummary($row["username"], 16)."</a></td>";
					 	$taskListTable .= "</tr>";
			
				} 
			$taskListTable .= "</tbody>";
			$taskListTable .= "<tfoot>";
			$taskListTable .= "<tr>";
			$taskListTable .= "<td class=\"start\"></td>";
			$taskListTable .= "<td class=\"\"colspan=\"2\"></td>";
			$taskListTable .= "<td class=\"end\"></td>";
			$taskListTable .= "</tr>";
			$taskListTable .= "</tfoot>";
			$taskListTable .= "</table>";	
		} else {
			#no tasks
		}
		//now get links to all current licences and display in a menu
		$qry = "SELECT distinct  l.licenceid, ut.tasklistid, tl.tasklistname FROM usr_usrtype ut ";
		$qry .= "LEFT JOIN usr u ON u.userid = ut.userid LEFT JOIN account a ON a.accountid = u.accountid ";
		$qry .= "LEFT JOIN accountlicence l ON a.accountid = l.accountid AND l.tasklistid = ut.tasklistid ";
		$qry .="left join tasklist tl on tl.tasklistid = ut.tasklistid  ";
		$qry .= " WHERE ut.userid = ".$clsUS->userid." AND ut.usertype = 'Administrator'  and l.end_date > NOW() and l.start_date < NOW() ";
			
	} else {
		//now get links to all current licences and display in a menu
		/*
		$qry = "SELECT l.licenceid, ut.tasklistid, tl.tasklistname FROM usr_usrtype ut ";
		$qry .= "LEFT JOIN usr u ON u.userid = ut.userid LEFT JOIN account a ON a.accountid = u.accountid ";
		$qry .= "LEFT JOIN accountlicence l ON a.accountid = l.accountid AND l.tasklistid = ut.tasklistid ";
		$qry .="left join tasklist tl on tl.tasklistid = ut.tasklistid  ";
		$qry .= " WHERE ut.userid = ".$clsUS->userid." AND ut.usertype = 'User' ";
		*/
		$qry = "SELECT DISTINCT tasklist.tasklistname, tasklist.tasklistid, accountlicence.licenceid ";
		$qry .= "FROM tasklist LEFT JOIN task ON tasklist.tasklistid = task.tasklistid LEFT JOIN accounttask ON task.taskid = accounttask.taskid ";
		$qry .= "left join accountlicence on accounttask.licenceid = accountlicence.licenceid ";
		$qry .= "WHERE accounttask.assignedto = $userid AND accounttask.state = 'ACTIVE' ";
		$qry .= "GROUP by tasklist.tasklistname, tasklist.tasklistid, accountlicence.licenceid ";
		
	}
	
	$rsTaskList = getRS($con, $qry);
	$cnt = 0;
	if (getResultRows($rsTaskList) > 0) {
		echo "<p>View Tasklists: <a href=\"index.php\">All current task lists</a> | ";
		while ($rowList = getRow($rsTaskList)) {
			if (isAllowed("myTasks")) {
				if($cnt > 0) echo " | ";
				echo "<a href=\"index.php?licenceid=".$rowList['licenceid']."\">".$rowList['tasklistname']."</a>";
				$cnt ++;
			}
		}
		echo "</p>";
	}
	echo $alert_text ;
	
	echo $taskListTable;
	
	if (isset($box) && $todayTask > 0 && !in_array('todayTaskAlert.php', $box)) {
		array_unshift($box,"todayTaskAlert.php");
	}
	if (isset($box) && $lateTask > 0 && !in_array('lateTaskAlert.php', $box) && $tasklist_conf['home_page_late_task_alert']) {
		array_unshift($box,"lateTaskAlert.php");
	}
}

