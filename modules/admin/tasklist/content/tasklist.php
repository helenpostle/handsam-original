<?php
/***
** account task list for school admin or super admin
** 
** 
***/

$taskListTable = "";
$mult_select_frm = "";
$monthsNav = "";
if ($multi == 'yes') {
	$mult_select_frm .= startFormTable($_SERVER["PHP_SELF"], "multiSelect", "",  MULTISELECT_TITLE, '');
	//$taskListTable .= frmLegend('Edit Selected Tasks');
	
	$usersArray = array("No User");
	$qry = "select usr.userid as a, concat(usr.firstname,' ',usr.lastname) as b from  usr left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.accountid = $accountid and (state != 'DELETED' and state != 'INACTIVE') and usr_usrtype.tasklistid = $tasklistid order by usr.username asc";
	$result = getRS($con, $qry);
	while ($row = getRow($result)) {
		$usersArray[$row['a']] = $row['b'];
	}
	
	$startDate = time();
	$mult_select_frm .= frmHiddenField($option,"option");
	$mult_select_frm .= frmHiddenField($month,"month");
	$mult_select_frm .= frmHiddenField($year,"year");
	$mult_select_frm .= frmHiddenField($category,"category");
	$mult_select_frm .= frmHiddenField($multi,"multi");
	$mult_select_frm .=  frmHiddenField($licenceid,"licenceid");
  $mult_select_frm .=  frmHiddenField($task_search,"task_search");

	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	
        $mult_select_frm .= '<div id="multitabs"><ul style="list-style-image:none;margin:0px;"><li><a href="#multitab-1">Action Date</a></li><li><a href="#multitab-2">Assigned To</a></li><li><a href="#multitab-3">State</a></li></ul>';
        
        $mult_select_frm .= '<div id="multitab-1">';	
        $mult_select_frm .= '<label class="labelCheckbox" for="editActionDate">Check to Edit</label><input class="toggleInput checkbox" type="checkbox" name="editActionDate" value="ctl\\.actiondate"/>'; // <- double backslash as jquery doesn't like periods in id attributes
        $mult_select_frm .= frmDateFieldNonDb($startDate, "actiondate","Action Date",false, "cover slim");
        $mult_select_frm .= '</div>';
        
        $mult_select_frm .= '<div id="multitab-2">';
        $mult_select_frm .= '<label class="labelCheckbox" for="editAssignedto">Check to Edit</label><input class="toggleInput checkbox" type="checkbox" name="editAssignedto" value="assignedto"/>';
        $mult_select_frm .= frmSelectArrayNonDb("assignedto",$usersArray,"Assigned To",false, "", "cover slim");
     
     $mult_select_frm .= '</div>';
        
        $mult_select_frm .= '<div id="multitab-3">';     
        $mult_select_frm .= '<label class="labelCheckbox" for="editState">Check to Edit</label><input class="toggleInput checkbox" type="checkbox" name="editState" value="state"/>';
	$mult_select_frm .= frmSelectArrayNonDb("state" , $state, "State" , false, "", "cover slim");	
    $mult_select_frm .= '</div></div>';

    // select all
	$mult_select_frm .= "<div class=\"select_all clearfix\"><label id=\"select_all\" for=\"select_all\"class=\"last\">".SELECT_ALL."</label><input id=\"selectAllBoxes\" type=\"checkbox\" name=\"select_all\" class=\"checkbox\"/></div>";

	$mult_select_frm .= frmButtonConfirmHelp("Save","saveMulti", "Are you sure you want to change all of these tasks?", "Click here to save changes");
	$mult_select_frm .= frmButtonHelp("Reset","cancelMulti", "Click here to cancel");
	$mult_select_frm .= frmEndFormNow();
}
$taskListTable .= "<table class=\"acc_list\">";
$taskListTable .= "<caption>".$listTitle."</caption>";
$taskListTable .= "<thead>";
$taskListTable .= "<tr>";
$taskListTable .= "<th class=\"day nolink start\">Date</th>";
$taskListTable .= "<th class=\"tasktext_usr nolink\">Task Description</th>";
if ($multi == 'yes') {
	$taskListTable .= "<th class=\"nolink feedback\">Docs</th>";
	$taskListTable .= "<th class=\"state nolink user\">User</th>";
	$taskListTable .= "<th class=\"nolink end check\"></th>";
} else {
	$taskListTable .= "<th class=\"feedback nolink\">Docs</th>";
	$taskListTable .= "<th class=\"state nolink user\">User</th>";
	$taskListTable .= "<th class=\"nolink end check\"></th>";

}	
$taskListTable .= "</tr>";
$taskListTable .= "</thead>";
$taskListTable .= "<tbody>";

$daynum = 0;
$rowClassOdd = "";
$rowcnt = 0;
$trid = "";
#set licennceid param if not a superadmin
if (isAllowed("editTask")){
	$licenceidParam = "&licenceid=".$licenceid;
} else {
	$licenceidParam = "&licenceid=".$licenceid;
}

if ($month == 0 && $current_licence) {
		$monthsNav = "<li><a  class=\"active\" href=\"tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=0&year=0&option=".$option."&user=".$user."&category=".$category."&group_id=".$group_id."&multi=".$multi.$licenceidParam."\">Now</a></li>";
} else if ($current_licence){
	$monthsNav = "<li><a href=\"tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=0&year=0&option=".$option."&user=".$user."&category=".$category."&group_id=".$group_id."&multi=".$multi.$licenceidParam."\">Now</a></li>";
}
if ($month == 13) {
		$monthsNav .= "<li><a  class=\"active\" href=\"tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=13&year=0&option=".$option."&user=".$user."&category=".$category."&group_id=".$group_id."&multi=".$multi.$licenceidParam."\">All Months</a></li>";
} else {
	$monthsNav .= "<li><a href=\"tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=13&year=0&option=".$option."&user=".$user."&category=".$category."&group_id=".$group_id."&multi=".$multi.$licenceidParam."\">All Months</a></li>";
}

while ($row = getRow($monthresult)) {
	$actionmonth = $row['month'];
	$actionyear = $row['year'];
	$monthLink = "tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;year=".$actionyear."&multi=".$multi.$licenceidParam."&option=".$option."&user=".$user."&group_id=".$group_id."&category=".$category."&month=";
	$monthsNav .= displayMonthListLink($months[$actionmonth], $year, $month, $actionyear, $actionmonth,  $monthLink);
}
		
$todayTask = 0;
$lateTask = 0;
//stuff for hilighting  alternate days in month and category view
while ($row = getRow($optionresult)) {
    $rowClassHighlight = "";
	#sort out action date into month year and day
	#$displaydate = completedate if viewing only uncompleted tasks otherwise $displaydate = actiondate - NOT IMPLIMENTED YET
	$actionyear = date("Y", strtotime($row[$displaydate]));
	$actionmonth = date("n", strtotime($row[$displaydate]));
		$actionday = date("jS", strtotime($row[$displaydate]));
	if (date("j n Y", strtotime($row["actiondate"])) == date("j n Y", time()) && $row["state"] != "COMPLETED") {
		$alertImg = "<img src=\"images/icon_error.gif\" alt=\"This task is late\"/>";
		$alertClass = "today";
		$todayTask ++;
	} else if (strtotime($row["actiondate"]) < time() && $row["state"] != "COMPLETED") {
		$alertImg = "<img src=\"images/icon_error_red.gif\" alt=\"This task is late\"/>";
		$alertClass = "late";
		$lateTask ++;
		
	} else if (($row['completedate'] > $row['actiondate']) && $row["state"] == "COMPLETED") {
		$alertImg = "<img src=\"images/tick_red.gif\" alt=\"This task was completed late\"/>";
		$alertClass = "completed";
	} else if($row["state"] == "COMPLETED")  {
		$alertImg = "<img src=\"images/tick.gif\" alt=\"This task was completed on time\"/>";
		$alertClass = "completed";
	} else {
		$alertImg = "";
		$alertClass = "";
	}
	
	##display blue tick if feedback has been left!	
	$feedbackImg = "";
	//we should also check for feedback docs and also display tick
	
	if ((trim($row["feedback"])) != "" || (trim($row["notes"])) != "" || $row['userDocs'] > 0 || $row['adminDocs'] > 0) {
		if ( $row['adminDocs'] > 0 || trim($row["notes"]) != ""){
			$feedbackImg = "<img src=\"images/tick_orange.gif\" alt=\"This task has additional notes or documents\"/>";
		} else {
			$feedbackImg = "<div class=\"tick\"></div>";
		}
		if ( $row['userDocs'] > 0 || trim($row["feedback"]) != "")	{
			$feedbackImg .= "<img src=\"images/tick_blue.gif\" alt=\"This task has feedback\"/>";
		} else {
		}
	} else if (strtotime($row["actiondate"]) < time() && $row["state"] != "COMPLETED") {
		$feedbackImg = "";
	}
	
	#set lastmonth for first loop											
	if ($rowcnt == 0) {
		$lastmonth = 0;
		$lastyear = 0;
		$daynum = 0;
	}
	
	//changes class of alternate day rows in table ijnboth month and category view
	if ($daynum != $actionday && $rowcnt != 0) {
		if($rowClassOdd == "odd") {
			$rowClassOdd = "";
		} else {
			$rowClassOdd = "odd";
		}
	} 
	$daynum = $actionday;
	if (($lastmonth != $actionmonth && ($month == 13 || $month == 0)) || $lastyear != $actionyear) { 
		$taskListTable .=  "<tr id=\"month".$actionmonth."\"class=\"headingRow\"><td class=\"centre start end\" colspan=\"5\">".$months[$actionmonth]." ".$actionyear."</td></tr>";
	}
	$lastmonth = $actionmonth;
	$lastyear = $actionyear;
	$rowcnt++;
    if ($row['key_task'] == 1) {
        $rowClassHighlight = "highlight";
    }
	if (($month == 13 && $year == 0) || ($month == 0 && $year == 0) || ($actionyear = $year && $actionmonth == $month)) {
		$link1 = "tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;accounttaskid=".$row["accounttaskid"]."&month=".$month."&year=".$year."&user=".$user."&option=".$option."&category=".$category."&multi=".$multi."&group_id=".$group_id;
		$taskListTable .= "<tr ".$trid." class=\"".$rowClassHighlight." ".$rowClassOdd."\">";
		$taskListTable .= "<td class=\"start day\"><a href=\"".$link1."\">".$actionday."</a></td>";
		$taskListTable .= "<td class=\"\"><a class=\"view_task\" href=\"".$link1."\">".$alertImg."<strong>".displayText($row["categoryname"]).":</strong> ".showAbstract(displayText($row["customtext"]), 100)."</a></td>";
		if ($multi == 'yes' && $row["state"] != 'COMPLETED') {
			$taskListTable .= "<td class=\"state\"><a href=\"".$link1."\">$feedbackImg</a></td>";
			$taskListTable .= "<td class=\"state\"><a title=\"{$row["username"]}\" href=\"".$link1."\">".textSummary($row["firstname"]. " ".$row['lastname'], 25)."</a></td>";
			$taskListTable .= "<td class=\"end check\"><input class=\"multiSelectCheck\" id='checkBox".$rowcnt."'type='checkbox' name='selectMulti[]' value='".$row["accounttaskid"]."' /></td>";
		} else {
			$taskListTable .= "<td class=\"state\"><a href=\"".$link1."\">$feedbackImg</a></td>";
			$taskListTable .= "<td class=\"flag\"><a title=\"{$row["username"]}\" href=\"".$link1."\">&nbsp;".textSummary($row["firstname"]. " ".$row['lastname'], 25)."</a></td>";
			$taskListTable .= "<td class=\"end\"></td>";
		}
	 	$taskListTable .= "</tr>";
 	}					

} 
$taskListTable .= "</tbody>";
$taskListTable .= "<tfoot>";
$taskListTable .= "<tr>";
$taskListTable .= "<td class=\"start\"></td>";
$taskListTable .= "<td colspan=\"3\"></td>";
$taskListTable .= "<td class=\"end\"></td>";
$taskListTable .= "</tr>";
$taskListTable .= "</tfoot>";
$taskListTable .= "</table>";
if ($multi == 'yes') {
	$taskListTable .= endFormTable2();
}

echo $mult_select_frm;

//now get links to all current licences and display in a menu

$qry = "select tasklist.tasklistname, tasklist.tasklistid, accountlicence.licenceid from tasklist left join accountlicence on tasklist.tasklistid = accountlicence.tasklistid where accountlicence.accountid = $accountid and accountlicence.state = 'ACTIVE' and accountlicence.end_date > NOW() and accountlicence.start_date < NOW()";
$rsTaskList = getRS($con, $qry);
if (getResultRows($rsTaskList) > 0) {
	echo "<p>View Current Tasklists: ";
	$cnt = 0;
	while ($rowList = getRow($rsTaskList)) {
		if (isAllowed("editAccount") || isAllowed("editOurTasks", 1,$rowList['tasklistid'])) {
			if($cnt > 0) echo " | ";
			echo "<a href=\"tasks.php?licenceid=".$rowList['licenceid']."\">".$rowList['tasklistname']."</a>";
			$cnt ++;
		}
	}
	echo "</p>";
}
 
echo $taskListTable;
?>