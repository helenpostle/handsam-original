<?php 
#state select box options depend on if the task has been completed or not
if ($rowAccountTask['state'] == "COMPLETED") {
	$state = array("COMPLETED" => "COMPLETED", "ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
} else if ($rowAccountTask['state'] != "DELETED1" && $rowAccountTask['state'] != "DELETED2") {
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
} else {
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE", "DELETED1" => "DELETED");
} 
//if not a custom task
if (!$newCustomTask && !$customTask) { 
	echo frmHiddenField($category,"category");
	if (trim($rowAccountTask['customtext']) == "" ) {
		echo frmTextAreaNonEdit($rowAccountTask,"tasktext","Task description");
	} else {
		echo frmTextAreaNonEdit($rowAccountTask,"customtext","Task description");
	}
	//Handsam task documents - goes through category array set in config. Add new cat to config to add a new category to this and file admin forms
	$cnt = 0;
	foreach($docCategory as $k => $aCat) {
		if ($cnt == 1) {
			if ($tasklist_conf['collapse_managetask_resources'] === true) echo "<h2 id=\"task_resources_title\">Click to show task resources <span class=\"task_show_hide\">+</span><span style=\"display: none;\" class=\"task_show_hide\">-</span></h2>";
			echo "<div id=\"task_resources\">";	
		}
		$cnt++;
		echo "<div class=\"frm_row clearfix\">\n";
		echo "<span class=\"label\">".$aCat." Resources</span>\n";
		echo "<span class=\"value\">\n<span class=\"docsList\">\n<ul>";
		$qryAttachedFiles = "SELECT task_files.taskfileid, files.fileid, files.title FROM files left join task_files on files.fileid = task_files.fileid WHERE task_files.taskid = ".$rowAccountTask['taskid']." and files.filetype='handsamDoc' and filecategory='$k'";
		$resultAttachedFiles = getRS($con, $qryAttachedFiles);
		while ($rowAttachedFile = getRow($resultAttachedFiles)) {
			echo "<li class=\"attachment\"><a href=\"$rootUrl/handsam_core/downloadDocument.php?id=".$rowAttachedFile['fileid']."&amp;accounttaskid=$accounttaskid\">".$rowAttachedFile['title']."</a></li>";
		}
		$qryAttachedLinks = "SELECT task_links.tasklinkid, links.linkid, links.url, links.title FROM links left join task_links on links.linkid = task_links.linkid WHERE task_links.taskid = ".$rowAccountTask['taskid']." and links.linktype='handsamDoc' and linkcategory='$k'";
		$resultAttachedLinks = getRS($con, $qryAttachedLinks);
		while ($rowAttachedLink = getRow($resultAttachedLinks)) {
			echo "<li class=\"link\"><a target=\"_blank\" href=\"".$rowAttachedLink['url']."\">".$rowAttachedLink['title']."</a></li>";
		}
		echo "</ul>\n</span>\n</span>\n";
		echo "</div>\n";
	}
	echo "</div>"; // closing tag for div#task_resources
} else {
	#a custom task
	echo frmTextArea($rowAccountTask,"customtext",5,"Task description",false);
	$catQry = "select categoryid as a, case when taskcategory.customtext = '' then taskcategory.categoryname when taskcategory.customtext is null then taskcategory.categoryname else taskcategory.customtext end as b from taskcategory where tasklistid = $tasklistid";

	//echo $catQry;
	echo frmSelectQuery($rowAccountTask,"categoryid",$catQry,$con,"Task Category",true);

}

#Additional task notes
if ($edit_form) {
	echo frmTextArea($rowAccountTask,"notes",5,"Additional Notes",false);
} else {
	echo frmTextAreaNonEdit($rowAccountTask,"notes","Additional Notes");
}
#task state
echo frmSelectArray($rowAccountTask, "state" , $state, "State" , true);	

#task date field
if ($edit_form) {
	if (isset($datefield)) {
		echo frmDateFieldNonDb ($datefield, "actiondate", "Action Date", true);
		
		echo frmInfoRow("You have now selected a new &quot;Action date&quot; and &quot;Assigned to user&quot;. To return these to their original values, please click <a href=\"?accounttaskid=".$accounttaskid."\">here</a>.");
	} else {
		if ($accounttaskid == 0) {
			echo frmDateFieldNonDb(time(),"actiondate","Action Date",true);
		} else {
        // if key task date lock is set
        if($rowAccountTask['key_task_lock'] == 1)
        {
            echo '<span style="font-weight:bold;color:red">This is a mandatory final completion date and cannot be changed.</span>';
            echo frmHiddenField(strftime("%a, %d %b %Y",strtotime($rowAccountTask['actiondate'])), "actiondate");
            echo frmDateNonEdit($rowAccountTask, "actiondate", "Action Date");

        }
        else
        {
            echo frmDateField($rowAccountTask,"actiondate","Action Date",true);
        }
		}
	}
} else {
	echo frmDateNonEdit($rowAccountTask, "completedate", "Date Completed");
  
	if (strtotime($rowAccountTask['completedate']) > strtotime($rowAccountTask['actiondate'])) {
		echo "<p class=\"forminfo red\"><img src=\"$rootUrl/images/icon_error_red.gif\" alt=\"\"/>This task was completed late</p>";
	}	
	echo frmDateNonEdit($rowAccountTask, "actiondate", "Action Date");
	echo frmHiddenField(strftime("%a, %d %b %Y",strtotime($rowAccountTask['actiondate'])),"actiondate");
}


#assigned to user
if ($edit_form) {
  echo frmSelectArray($rowAccountTask,"assignedto",$usersArray,"Assigned To",false);
	#assigned to multi  user
	if ($rowAccountTask['accounttask_repeatingid'] == null || $rowAccountTask['accounttask_repeatingid'] == 0) echo "<div class=\"frm_row clearfix\" id=\"extra_users\"><div class=\"addUser\"><a href = \"#\">Click here to assign this task to an additional user</a></div></div>";

	echo frmTextField($rowAccountTask,"budget",10,"Budget (&pound;)",false);
} else {
	$qryUsername = "select username, firstname, lastname from  usr where userid = ".$rowAccountTask['assignedto'];
	$resultUsername = getRS($con, $qryUsername);
	$rowUsername = getRow($resultUsername);
	$rowUsername['username'];

	echo "<div class=\"frm_row clearfix\"><span class=\"label\">Completed By</span><span class=\"value\"><div class=\"username\">".$rowUsername['firstname']." ".$rowUsername['lastname']." (".$rowUsername['username'].")</div></span></div>";
	echo frmTextAreaNonEdit($rowAccountTask, "budget", "Budget (&pound;)");
}

if (!$newCustomTask) { 
	#not shown on a new custom task form
	//feedback notes from user
	echo frmTextAreaNonEdit($rowAccountTask,"feedback","Feedback");
	
	//feedback docs list
	
	echo "<div class=\"frm_row clearfix\" >\n";
	echo "<span class=\"label\">Feedback Documents</span>\n";
	echo "<span class=\"value\">\n<span class=\"docsList\">\n<ul>";
	$qryAttachedFiles = "SELECT task_files.taskfileid, files.fileid, files.title FROM files left join task_files on files.fileid = task_files.fileid WHERE task_files.accounttaskid = $accounttaskid and files.filetype='userDoc'";
	$resultAttachedFiles = getRS($con, $qryAttachedFiles);
	while ($rowAttachedFile = getRow($resultAttachedFiles)) {
		echo "<li class=\"attachment\"><a href=\"$rootUrl/handsam_core/downloadDocument.php?id=".$rowAttachedFile['fileid']."&amp;accounttaskid=$accounttaskid\">".$rowAttachedFile['title']."</a></li>";
	}
	
	
	echo "</ul>\n</span>\n</span>\n";
	echo "</div>\n";
}

$fileDB = getTaskAdminFiles($accounttaskid, $con);
addFileField($fileDB, false);

//add admin docs link
if (isAllowed("editOurTasks") && $edit_form && strtotime($licence_end) > time()) {
	echo "<div class=\"frm_row noLabel\"><a href = \"#\" onclick='openWin(\"$rootUrl/handsam_core/docList.php?filetype=adminDoc\",840,600,\"fileBrowser\")'>Attach a document to this task</a></div>";
} else if (isAllowed("editTask") && strtotime($licence_end) > time()) {
	echo "<div class=\"noLabel\"><a href = \"#\" onclick='openWin(\"$rootUrl/handsam_core/docList.php?filetype=adminDoc&accountid=".$accountid."\",840,600,\"fileBrowser\")'>Attach a document to this task</a></div>";
}


// email alerts for tasks option
if($tasklist_conf['task_email_user'] == true)
{
    $email_user['task_email_user'] = $rowAccountTask['task_email_user'];
    // set the value to the default if null
    if(is_null($email_user['task_email_user']) && $tasklist_conf['task_email_user_default_on'] == true) $email_user['task_email_user'] = 1;
    echo frmExRadio($email_user, "task_email_user", array('0'=>'No','1'=>'Yes'), "Send email alerts to assigned Users", '');
}


?>