<?php
/***
** Shows the home page content items
** 
** 
***/
while ($rowContent = getRow($rsContent)) {
	echo "<div class=\"item\">";
	echo displayEdit($rowContent);
	echo displayText($rowContent['contenttext']);
	echo "</div>";
} 
?>
