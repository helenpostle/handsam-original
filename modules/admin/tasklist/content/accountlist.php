<?php
/***
** Shows the list of accounts in the system
** 
** 
***/


/* don't do pages for now! 
if (!isset($HTTP_GET_VARS["page"])) $page = 1;
else $page = $HTTP_GET_VARS["page"];

$limit = 40;
$count = getResultRows(getRS($con, $qry));

$start = ($page - 1) * $limit;
$pages = (($count % $limit) == 0) ? $count / $limit : floor($count / $limit) + 1;
$result = getRS($con, $qry." limit $start, $limit");

echo pageNavigator("users.php", "order=".$order, $page, $pages);
		//echo "<center>".$pageNav."</center>";
*/


//now get links to all current licences and display in a menu

$qry = "select tasklist.tasklistname, tasklist.tasklistid from tasklist ";
$rsTaskList = getRS($con, $qry);
if (getResultRows($rsTaskList) > 0) {
	echo "<p>View Tasklists: ";
	$cnt = 0;
	echo "<a href=\"accounts.php\">All current tasklists</a> | ";
	while ($rowList = getRow($rsTaskList)) {
		if($cnt > 0) echo " | ";
		echo "<a href=\"accounts.php?tasklist=".$rowList['tasklistid']."\">".$rowList['tasklistname']."</a>";
		$cnt ++;
	}
	echo "</p>";
}
 
	
?>

<p><a href="accounts.php?accountid=0&amp;tasklist=<?php echo $tasklist;?>">>> Add a new account</a></p>
<table class="list">
	<caption class="hidden"><?php echo $title;?></caption>
	<thead>
		<tr>
			<th class="start accountname"><a href="?order=account.accountname">Account name</a></th>
			<th class="address"><a href="?order=account.town">Address</a></th>
			<th class="type"><a href="?order=tasklist.tasklistname">Type</a></th>
			<th class="state"><a href="?order=account.state">State</a></th>
			<th class="end licence"><a href="?order=licence_end">licence</a></th>
		</tr>
	</thead>
	<tbody>

<?php
$last_accid = array();
while ($row = getRow($result)) {
	if (!in_array($row['accountid'], $last_accid)) {
		$last_accid[] = $row['accountid'];
	
		?>
		<tr>
			<td class="start accountname"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist;  ?>"><?php echo displayText($row["accountname"]) ?></a></td>
			<td class="address"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist; ?>"><?php echo displayText($row["address1"]) ?>, <?php echo displayText($row["town"]) ?>, <?php echo displayText($row["postcode"]) ?> </a></td>
			<td class="type"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist;  ?>"><?php echo displayText($row["tasklistname"]) ?></a></td>
			<td class="state"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist;  ?>"><?php echo displayText($row["state"]) ?></a></td>
			<td class="end licence"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist;  ?>"><?php echo displayDate($row["licence_end"]) ?></a></td>
		</tr>
		<?php 
	} 
}?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"><a href="accounts.php?accountid=0&amp;tasklist=<?php echo $tasklist;?>">Add a new account</a></td>
			<td colspan="3"></td>
 			<td class="end"></td>
 		</tr>
  	</tfoot>

</table>

