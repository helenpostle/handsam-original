<?php 
/**
 * lists all task categories in the selected month
 * click on category to display that category
 */

	if ($month > 0 && $month < 13) {
		boxTop($months[$month]."'s Categories");
		$qry = "SELECT taskcategory.customtext, taskcategory.categoryid, COUNT(taskcategory.customtext) AS cnt FROM taskcategory INNER JOIN accounttask ON taskcategory.categoryid = accounttask.categoryid WHERE accounttask.licenceid = '$licenceid' AND MONTH(accounttask.actiondate) = '$month' AND YEAR(accounttask.actiondate) = '$year' AND accounttask.assignedto = $userid $category_list_task_state  $task_search_sql   GROUP by taskcategory.customtext, taskcategory.categoryid ORDER BY taskcategory.customtext ASC";
	} else if ($month == 13) {
		boxTop("Categories");
		$qry = "SELECT taskcategory.customtext, taskcategory.categoryid, COUNT(taskcategory.customtext) AS cnt FROM taskcategory INNER JOIN accounttask ON taskcategory.categoryid = accounttask.categoryid WHERE accounttask.licenceid = '$licenceid' AND accounttask.assignedto = $userid $category_list_task_state  $task_search_sql  GROUP by taskcategory.customtext, taskcategory.categoryid ORDER BY taskcategory.customtext ASC";
	} else {
		boxTop("Categories");
		$qry = "SELECT taskcategory.customtext, taskcategory.categoryid, COUNT(taskcategory.customtext) AS cnt FROM taskcategory INNER JOIN accounttask ON taskcategory.categoryid = accounttask.categoryid WHERE accounttask.licenceid = '$licenceid' AND accounttask.assignedto = $userid $category_list_task_state and accounttask.actiondate <= DATE_ADD(NOW(), INTERVAL 7 DAY)  $task_search_sql  GROUP by taskcategory.customtext, taskcategory.categoryid ORDER BY taskcategory.customtext ASC";
	}
	
	//if ($clsUS->userid == 2) echo $qry;
	
	//echo $qry;
	
	$result = getRS($con, $qry);
	if(isAllowed("editAccount")) {
		$categoryLink = "mytasks.php?task_search=$task_search_url&amp;licenceid=".$licenceid."&month=".$month."&year=".$year."&option=".$option."&category=";
	} else {
		$categoryLink = "mytasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=".$month."&year=".$year."&option=".$option."&category=";
	}

?>
<div class="vertical_nav">

<ul>
	<?php 
	if ($category == 0) {
		echo "<li class=\"active\">Show all categories</li>";
	} else {
		echo "<li><a href=\"".$categoryLink."0\" title=\"\">Show all categories</a></li>";
	}
		
	while ($row = getRow($result)) {
		echo displayListLink(displayText($row["customtext"])."&nbsp;(".$row['cnt'].")", $category, $row["categoryid"], $categoryLink);
	}
	?>
</ul>
</div>

<?php
	boxBottom();
?>