<?php 
/**
 * lists all task categories in the selected month
 * click on category to display that category
 */
	if(!isset($grpSQL)) $grpSQL = "";
	if ($month > 0 && $month < 13) {
		boxTop($months[$month]."'s Categories");
		$qry = "SELECT taskcategory.customtext, taskcategory.categoryid, COUNT(taskcategory.customtext) AS cnt FROM taskcategory INNER JOIN accounttask ON taskcategory.categoryid = accounttask.categoryid WHERE  accounttask.licenceid = '$licenceid' {$listTasks->monthSQL} {$listTasks->searchSQL} $category_list_task_state $grpSQL GROUP by taskcategory.customtext, taskcategory.categoryid ORDER BY taskcategory.customtext ASC";
	} else if ($month == 13) {
		boxTop("Categories");
		$qry = "SELECT taskcategory.customtext, taskcategory.categoryid, COUNT(taskcategory.customtext) AS cnt FROM taskcategory INNER JOIN accounttask ON taskcategory.categoryid = accounttask.categoryid WHERE accounttask.licenceid = '$licenceid' $category_list_task_state $grpSQL {$listTasks->monthSQL} {$listTasks->searchSQL} GROUP by taskcategory.customtext, taskcategory.categoryid ORDER BY taskcategory.customtext ASC";
	} else {
		boxTop("Categories");
		$qry = "SELECT taskcategory.customtext, taskcategory.categoryid, COUNT(taskcategory.customtext) AS cnt FROM taskcategory INNER JOIN accounttask ON taskcategory.categoryid = accounttask.categoryid WHERE accounttask.licenceid = '$licenceid' {$listTasks->monthSQL} {$listTasks->searchSQL} $category_list_task_state $grpSQL GROUP by taskcategory.customtext, taskcategory.categoryid ORDER BY taskcategory.customtext ASC";
	}
	
	//if ($clsUS->userid == 2) echo $qry;
	
	$result = getRS($con, $qry);
	if(isAllowed("editAccount")) {
		$categoryLink = "tasks.php?task_search=$task_search_url&amp;licenceid=".$licenceid."&month=".$month."&year=".$year."&option=".$option."&user=".$user."&multi=".$multi."&group_id=".$group_id."&category=";
	} else {
		$categoryLink = "tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=".$month."&year=".$year."&option=".$option."&user=".$user."&multi=".$multi."&group_id=".$group_id."&category=";
	}

?>
<div class="vertical_nav">

<ul>
	<?php 
	if ($category == 0) {
		echo "<li class=\"active\">Show all categories</li>";
	} else {
		echo "<li><a href=\"".$categoryLink."0\" title=\"\">Show all categories</a></li>";
	}
		
	while ($row = getRow($result)) {
		echo displayListLink(displayText($row["customtext"])."&nbsp;(".$row['cnt'].")", $category, $row["categoryid"], $categoryLink);
	}
	?>
</ul>
</div>

<?php
	boxBottom();
?>