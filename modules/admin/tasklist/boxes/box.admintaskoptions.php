<?php 
/**
 * lists options for viewing accoun tasks list
 * 
 */
	boxTop("List Options");
	$optionLink = "taskadmin.php?month=".$month."&tasklistid=".$tasklistid."&category=".$category."&multi=".$multi."&option=";
?>
<div class="vertical_nav">

<ul>
	<?php 
	
	echo displayListLink("Show active", $option, "active", $optionLink);
	//echo displayListLink("Show completed", $option, "completed", $optionLink);
	//echo displayListLink("Show uncompleted", $option, "uncompleted", $optionLink);
	echo displayListLink("Show inactive", $option, "inactive", $optionLink);
    echo displayListLink("Show key tasks", $option, "key_task", $optionLink);
	if (isAllowed("undelete")) {
		echo displayListLink("Show Deleted", $option, "deleted", $optionLink);
	}
	if ($multi == 'yes') {
		echo "<li class=\"topMargin\"><a href=\"taskadmin.php?month=0&option=".$option."&category=".$category."&tasklistid=".$tasklistid."\">Hide 'Select Multiple Tasks'</a></li>";
	} else {
		echo "<li class=\"topMargin\"><a href=\"taskadmin.php?month=0&option=".$option."&category=".$category."&multi=yes&tasklistid=".$tasklistid."\">Show 'Select Multiple Tasks'</a></li>";
	}
		echo "<li class=\"topMargin\"><a href=\"taskadmin.php?month=0&option=".$option."&category=".$category."&multi=".$multi."&taskid=0&tasklistid=".$tasklistid."\">Add New Task</a></li>";

	?>
</ul>
</div>
<?php
	boxBottom();
?>
	