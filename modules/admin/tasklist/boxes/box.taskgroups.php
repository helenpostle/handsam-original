<?php 
/**
 * lists all task categories in the selected month
 * click on category group to display that category group
 */
	


    boxTop("Category Group");
    $qry = "SELECT distinct task_category_group.* FROM task_category_group left join taskcategory on task_category_group.group_id = taskcategory.group_id INNER JOIN accounttask ON taskcategory.categoryid = accounttask.categoryid {$listTasks->monthSQL} {$listTasks->searchSQL} {$listTasks->category_list_sql} WHERE accounttask.licenceid = '$licenceid'";


//if ($clsUS->userid == 2) echo $qry;

$result = getRS($con, $qry);
if(isAllowed("editAccount")) {
    $groupLink = "tasks.php?task_search=$task_search_url&amp;licenceid=".$licenceid."&month=".$month."&year=".$year."&option=".$option."&user=".$user."&multi=".$multi."&category=$category&group_id=";
} else {
    $groupLink = "tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=".$month."&year=".$year."&option=".$option."&user=".$user."&multi=".$multi."&category=$category&group_id=";
}

?>
<div class="vertical_nav">

<ul>
	<?php 
	if ($group_id == 0) {
		echo "<li class=\"active\">Show all groups</li>";
	} else {
		echo "<li><a href=\"".$groupLink."0\" title=\"\">Show all groups</a></li>";
	}
		
	while ($row = getRow($result)) {
		echo displayListLink(displayText($row["title"]), $group_id, $row["group_id"], $groupLink);
	}
	?>
</ul>
</div>

<?php
	boxBottom();
?>