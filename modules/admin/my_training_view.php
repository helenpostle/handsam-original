<?php
/**
* training log editing functionality
*
*/
####make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc("training_edit");
$pageFiles->addModFunc("training_popup");

$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->includePhp();

$menuPage = "my_training.php";

$pageVars = new loadVars($clsUS,$con);
if ($pageVars->accountid==null) $pageVars->accountid=$clsUS->accountid;

if ($pageVars->hacking) headerLocation("$rootUrl/index.php");
$qs = array();

$pageFiles->addJsVar('accountid', $pageVars->accountid);
$pageFiles->addJsVar('training_id', $pageVars->training_id);
$pageFiles->addJsVar('sessionid', session_id());

if (isAllowed("viewTraining")) {

	//get filter array querystring
	
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;
	
	$formDB = getTraining($con,  $pageVars, $clsUS->userid);

	if ($formDB['state'] == "IN PROGRESS" || $formDB['state'] == "COMPLETE") {
		$title = "View My Training Log Item";
		$content = "page.my_training_view.php";
        if (isset($formDB) && $formDB['course_url'] != "" && $formDB['trainee_id'] == $clsUS->userid && $formDB['online'] == 1 && $clsUS->conf['training']['e_training_enabled'] == 1) $box[] = 'box.e-training.php';
		if (buttonClicked("return")) {
			headerLocation("$rootUrl/modules/training/my_training.php?filter=".urlencode(serialize($qs)),$messages);
		}		
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>