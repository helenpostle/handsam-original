<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'training';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
if (isset($pageVars->training_id)) {
	//need to select current course for this training_id
	$qry = "select trainee_id from training_log where training_id = ".$pageVars->training_id;
	$rs = getRS($con,$qry);
	$row = getRow($rs);
	$id = $row['trainee_id'];
	
	if ($id == "") $id = 0;
} else {
	$id = 0;
}	

$qry = "select userid as a, Concat(lastname, ', ', firstname) as b from usr where ((state != 'DELETED' and userid != $id) or userid = $id ) and accountid = ".$pageVars->accountid." order by lastname";
$formDB = new pageQry($con, $qry);
$formDB->rsList();
echo "<option value=\"\">Select a Trainee</option>";
//echo "<option>$qry</option>";
//echo "<option>{$pageVars->training_id}</option>";
//echo "<option>$id</option>";
foreach ($formDB->rows as $row) {
	echo "<option value=\"{$row['a']}\"";
	if ($row['a'] ==  $pageVars->trainee_id) {
		echo " selected ";
	}
	echo ">{$row['b']}</option>";
}
?>