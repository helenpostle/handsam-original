<?php

/**
* training log get select list of providers 
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'training';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

//need to select current course for this training_id
$qry = "select course_id from training_log where training_id = ".$pageVars->training_id;
$rs = getRS($con,$qry);
$row = getRow($rs);
$id = $row['course_id'];
if ($id == "") $id = 0;

if ($clsUS->conf['training']['e_training_enabled'] == 1) {
	$qry = "select course_id as a, Concat(CASE online when 0 then '' WHEN 1 then 'Handsam Online Course: ' END, title) as b, online from training_course where ((state != 'DELETED' and course_id != $id) or course_id = $id ) and (accountid = ".$pageVars->accountid." or accountid=0) and (renewal like '% day' or  renewal like '% week' or  renewal like '% month' or  renewal like '% year') and (renewal_reminder like '% day' or renewal_reminder like '% week' or  renewal_reminder like '% month' or  renewal_reminder like '% year') order by online desc,title";
} else {
	$qry = "select course_id as a, Concat(CASE online when 0 then '' WHEN 1 then 'Handsam Online Course: ' END, title) as b, online from training_course where ((state != 'DELETED' and course_id != $id) or course_id = $id ) and (accountid = ".$pageVars->accountid." or accountid=0) and (renewal like '% day' or  renewal like '% week' or  renewal like '% month' or  renewal like '% year') and (renewal_reminder like '% day' or renewal_reminder like '% week' or  renewal_reminder like '% month' or  renewal_reminder like '% year') and  (online = 0 or (online = 1 and accountid = ".$pageVars->accountid.")) order by online desc,title";
}

//echo $qry;
$formDB = new pageQry($con, $qry);
$formDB->rsList();

echo "<option value=\"\">Pick a course</option>";
foreach ($formDB->rows as $row) {
	echo "<option value=\"{$row['a']}\"";
	if ($row['a'] ==  $pageVars->course_id) {
		echo " selected ";
	}
	echo ">{$row['b']}</option>";
}


?>