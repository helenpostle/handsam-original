<?php
/**
* training log editing functionality
*
*/

####make this a secure page
ini_set('max_execution_time', '0'); // 0 = no limit.
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc("training_assign");
//$pageFiles->addModFunc("training_edit");
//$pageFiles->addModFunc("training_popup");

$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->includePhp();

$menuPage = "assign.php";

$pageVars = new loadVars($clsUS, $con);
$selected_courses = array('Training Course 1', 'Training Course 2', 'Training Course 3', 'Training Course 4', 'Training Course 5');
$pageVars->checkCourseIdArr($clsUS,$con, count($selected_courses));
$course_id = $pageVars->course_id_arr;
$course_date = $pageVars->course_date_arr;
$course_dates_validated = true;

//global variable used in library/funcTrainingAssign.php
$update_results = array();

$accountid = $pageVars->accountid;
if($accountid == null) $accountid = 0;
$group = getStrFromRequest("group");

if ($training_conf['training_multi_assign'] === false) {
  headerLocation($rootUrl."/index.php");
}

$checked_all = array();

switch ($group)
{
  case 'year':
    include('shared/funcGroup_year.php');
    break;
  case 'month':
    include('shared/funcGroup_month.php');
    break;
  case 'week':
    include('shared/funcGroup_week.php');
    break;
  case 'day':
    include('shared/funcGroup_day.php');
    break;
  default:
    include('shared/funcGroup_day.php');  
}

if ($group == false) $group = "day";

if (isAllowed("recordTraining") || isAllowed("superAdminTraining")) {
  $title = "Assign Training";
  $contentId = "contentWide";
	$content = "page.assign.php";
  $date_cols = array();
  //var_dump($course_date);
  //var_dump($course_id);
  
  
  $courses = getCourseArr($course_id, $con, $accountid, $course_date);
  
//get course renewal/reminder dates
  $course_renewal = array();
  $course_reminder = array();
   
  //check for non-online courses
  $provider = false;
  foreach($courses as $aCourse) 
  {
    $course_renewal[$aCourse['course_id']] = $aCourse['renewal'];
    $course_reminder[$aCourse['course_id']] = $aCourse['renewal_reminder'];
    $course_online[$aCourse['course_id']] = $aCourse['online'];
    $course_title[$aCourse['course_id']] = $aCourse['title'];
    if($aCourse['online'] == 0)
    {
      $provider = true;
    }
  }
  
  foreach($courses as $aCourse)
  {
    if (isset($_POST['select_all-'.$aCourse['course_id']]))
    {
      $checked_all[$aCourse['course_id']] = " checked=checked ";
    }
    else
    {
      $checked_all[$aCourse['course_id']] = "";
    }
  }
  
  
  if (count($course_id) > 0)
  {
    $dBCourse = getAccountCourse($con, $accountid, $course_id);    
  }
  
  
  $dbUsers = getAccountUsers($con, $accountid);
	  
  if(buttonClicked('reset')) {
    headerLocation($rootUrl."/modules/training/assign.php?course_id=".$course_id.'#assigncouresetabs');
  }
  if(buttonClicked('submit_criteria')) {
    //validate courses and dates
    for ($n=0; $n<count($selected_courses); $n++)
    {
      if ($_POST['course_id-'.$n] > 0 && $_POST['course_date_id-'.$n] == '') 
      {
        $course_dates_validated = false;
        $messages[] = "Some of the courses have no date entered.";
      }
    }
  } else if(buttonClicked('assign')) {
      
    if (count($course_id) == 0)
    {
      $messages[] = 'Please select a course.';
    }
    
    //if ($provider === true && ($_POST['provider_id'] == false || $_POST['provider_id'] == 0))
    //{
     // $messages[] = 'Please select a course provider.';
    //}

    if ($_POST['course_date'] == false || $_POST['course_date'] == '')
    {
      $messages[] = 'Please select a date.';
    }
    
    if (count($messages) == 0)
    {
      //create courses for each selected user.
      $cnt = 0;
      $cnt2 = 0;
      foreach($_POST['new_course'] as $key=>$val) {
        
        $vals = $_POST;
       
        //key is [userid][course_id]
        foreach ($val as $this_course_id=>$v)
        {
          $cnt2++;
          
          if ($v == '1') 
          {
            if (isset($_POST['old_course']) && isset($_POST['old_course'][$key][$this_course_id]))
            {
              
              //update this training_id
              updateTrainingLog($con,$_POST['old_course'][$key][$this_course_id], $vals, $this_course_id, $key, $course_title[$this_course_id], $course_online[$this_course_id], $course_renewal, $course_reminder);
            }
            else
            {
              assignToCourse($con,$key,$accountid, $vals, $this_course_id, $course_title[$this_course_id], $course_renewal, $course_reminder);
            }
            $cnt++;
          } 
        }
      }
      if ($cnt == 0)
      {
        $messages[] = 'Please select some users.'; 
      }
      else 
      {
        
        $content = 'page.assign.report.php';
        //headerLocation($rootUrl."/modules/training/assign.php?accountid=".$accountid);
      }
    }
  }
  
  if(buttonClicked('delete') && isset($_POST['course'])) 
  {
    //set course state to deleted for each deselected user.
      $active_courses = array_keys($_POST['course']); // training ids are the keys, value is 1
      
      // for each user
      foreach($dbUsers as $user)
      {
          // if the course has that user
          if(isset($dBCourse[$user['userid']]))
          {
//              for each course
              foreach($dBCourse[$user['userid']] as $course_date => $userCourse) 
              {
                  // if the training id has not been sent from the page, delete
                  if(!in_array($userCourse['training_id'], $active_courses))
                  {
                      deleteFromCourse($con, $userCourse['training_id'], $clsUS->accountid);
                  }
              }
          }
      }
      
    headerLocation($rootUrl."/modules/training/assign.php");
  }
  
  
} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}

$messages = array_unique($messages);
include("../../layout.php");
?>