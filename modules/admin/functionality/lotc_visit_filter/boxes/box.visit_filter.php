<?php 
/**
 * lists lotc changes in form state 
 * 
 */
 
lotcBoxTop("Filter LOtC List");

	//$filter_array = array();//this is now set on parent pages  - evc.php, finance.php, index.php, principal.php
	if (isAllowed("editVisits")) {
	   //get account list
	   $filter_array['account'] = array();
	   $account_qry = "select distinct a.accountid as a, accountname as b from account a where a.state != 'DELETED' order by accountname";
	   $rsSelect = getRS($con,$account_qry);
	   $filter_array['account'] = array();	
	   while ($row = getRow($rsSelect)) {
		  $filter_array['account'][$row['a']] = $row['b'];
	   } 
	//get user list	array
		$filter_array['user'] = array();
		//$qry = "SELECT u.userid AS a, u.username, CONCAT(u.firstname, ' ', u.lastname) AS b FROM  usr u LEFT JOIN usr_usrtype uut ON u.userid = uut.userid LEFT JOIN usrtype ut ON uut.usertype = ut.usrtype WHERE u.accountid = {$pageVars->accountid} AND (u.state != 'DELETED' AND u.state != 'INACTIVE') AND ut.appid = 2 and ut.usrtype = 'visit planner' ORDER BY u.username ASC";
		$qry = "SELECT DISTINCT u.userid AS a, u.username, CONCAT(u.firstname, ' ', u.lastname) AS b FROM usr u LEFT JOIN a_visit av ON u.userid = av.leaderid WHERE av.accountid = {$pageVars->accountid} AND (u.state != 'DELETED' AND u.state != 'INACTIVE')";
		$result = getRS($con, $qry);
		while ($row = getRow($result)) {
			$filter_array['user'][$row['a']] = $row['b'];
		}	
	}

	//$filter_array['state'] = array("PLANNING" => "In planning", "APPROVAL" => "For EVC approval", "COMPLETED" => "Completed", "DELETED" => "Deleted"); //this is now set on parent pages  - evc.php, finance.php, index.php, principal.php
	//$filter_array['date'] = array("created" => "Date created", "visit_date" => "Visit Date");
	$filter_array['month'] = array(1 => "January", 2 => "February",3 =>  "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");
	
	
	//year array
	$this_year = (date('Y', time())) + 2;
	$start_year = 2010;
	//create year and month drop downs
	$filter_array['year'] = array();
	while ($start_year <= $this_year) {
		$filter_array['year'][$start_year] = $start_year;
		$start_year++;
	}

	$selected = "";
	foreach ($filter_array as $filter_name => $filter_name_array) {
		$qs = $qs_base;
		echo "<div>";
		
		echo "<label for=\"$filter_name\">$filter_name</label>";
	
		echo "<select id=\"$filter_name\">";
		
		$qs[$filter_name] = 0;
		$filter_link = "?filter=".rawurlencode(serialize($qs));
		if ($filter_name != 'date') {
			echo "<option value=\"$filter_link\">All {$filter_name}s</option>";
		}
		foreach ($filter_array[$filter_name] as $filter_key => $filter_value) {
			$selected = "";
			$qs[$filter_name] = $filter_key;
			if (isset($qs_base[$filter_name]) && strval($qs_base[$filter_name]) == strval($filter_key)) { 
				$selected = "selected";
				echo $filter_name." : ".$qs_base[$filter_name]." : ".$filter_key;
			} else {
				$selected = "";
			}
			$filter_link = "?filter=".rawurlencode(serialize($qs));
			echo "<option $selected value=\"$filter_link\">$filter_value</option>";
		}
		echo "</select>";
		echo "</div>";
	}
	
lotcBoxBottom();
?>
	