<?php
####make this a secure page
$secure = true;
$ajaxModule = "training";

require("../../../../../shared/startPage.php");

$pageFiles->addModFunc("training_edit");
$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
$tkn = getStrFromRequest("tkn");
$ajax_message = getStrFromRequest("action");

if (isAllowed("recordTraining")) {
	if ($pageVars->course_id !== null) {
		$qry = "select * from training_course where course_id = {$pageVars->course_id}";
		$formDB = new pageQry($con, $qry);
				
		$load = getStrFromRequest("load");
		
		$formDB->rsItem();
		
		
		if ($formDB->cnt > 0 && $formDB->row['accountid'] == 0) {
			$edit_course = false;
		} else {
			$edit_course = true;
		}
				
			if (buttonClicked("save") && $edit_course) {
				
				if ($pageVars->course_id == 0) {
					$qry = new dbInsert("training_course");
					$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
				} else {
					$qry = new dbUpdate("training_course");
					$qry->setParam("course_id", $pageVars->course_id);
				}		
				//course details
				
				
				
				$qry->setReqStringVal("title",$_POST["title"],"Course title");
				$qry->setStringVal("qual_title",$_POST["qual_title"],"Qualifiaction title");
				$qry->setStringVal("qual_desc",$_POST["qual_desc"],"Description");
				//this function vailidates the special input and select box for the dateAdd fields
				processDateAdd ("renewal", "Renewal time period", true, $qry);
				//processDateAdd ("refresher", "Refresher time period", false, $qry);
				processDateAdd ("renewal_reminder", "Renewal reminder time period", true, $qry);
				//processDateAdd ("refresher_reminder", "Refresher reminder time period", false, $qry);
				$qry->setReqStringVal("course_type",$_POST["course_type"],"Course type");
				if (isset($_POST["related_id"])) $qry->setNumberVal("related_id",$_POST["related_id"],"Related id");
				$qry->setAudit($clsUS->userid);
				if ($qry->execute($con)) {
					if ($pageVars->course_id == 0) {
						$pageVars->course_id = $qry->getNewID();	
					}
					//echo any parameters here that will be passed back to this page when successful
					echo "&course_id=".$pageVars->course_id;
				} else {
					echo "##ERROR: ";
					echo $qry->getError();
				}			
				
				
			} else if (buttonClicked("cancel")) {
				
				echo "cancel";
				
			} else if (buttonClicked("delete") && $edit_course){
				
				//check if this course is being used
				$check_qry = "select training_id from training_log where course_id = ".$pageVars->course_id;
				$rs = getRS($con,$check_qry);
				$row = getRow($rs);
				if ($row['training_id']) {
					//update state to deleted because its in use
					$qry = new dbUpdate("training_course");
					$qry->setParam("course_id", $pageVars->course_id);
					$qry->setStringVal("state","DELETED","state");
					$qry->setAudit($clsUS->userid);
					if ($qry->execute($con)) {
	
						echo "deleted";
						
					} else {
						echo "##ERROR: ";
						echo $qry->getError();
					}			
					
				} else {
					//delete properly
					$qry = new dbDelete("training_course");
					$qry->setParam("course_id", $pageVars->course_id);
					$qry->setParam("accountid", $pageVars->accountid);
					if ($qry->execute($con)) {
						
						echo "deleted";
						
					} else {
						echo "##ERROR: ";
						echo $qry->getError();
					}			
				}
	
			
				
			} else {
				if ($load == 'yes') {
					$qry = "select tc.*, tc2.title as related from training_course tc left join training_course tc2 on tc.related_id = tc2.course_id where tc.course_id = {$pageVars->course_id}";
					$formDB = new pageQry($con, $qry);
					$formDB->rsItem();
					
					echo "<div class=\"col\">";
						echo frmTextAreaNonEdit($formDB->row, "title","Course title");
						echo frmTextAreaNonEditArray($formDB->row,"course_type","Course type", $trn_type_arr);
						echo frmTextAreaNonEdit($formDB->row,"qual_title","Qualification");
						echo frmTextAreaNonEdit($formDB->row,"qual_desc","Qualification description");
					echo "</div>";		
					echo "<div class=\"col\">";	
						//remove this line below if stable.
						//echo frmHiddenField($formDB->row['pid'],"pid");
						echo frmTextAreaNonEdit($formDB->row,"renewal","Full renewal period");
						//echo frmTextAreaNonEdit($formDB->row,"refresher","Refresher period");
						echo frmTextAreaNonEdit($formDB->row,"renewal_reminder", "Renewal reminder period");
						//echo frmTextAreaNonEdit($formDB->row,"refresher_reminder", "Refresher reminder period");
						if ($formDB->row['course_type'] == 'FULL') {
							//echo frmTextAreaNonEditLink($formDB->row,"related", "Refresher course", "$rootUrl/modules/training/index.php");
						} else if ($formDB->row['course_type'] == 'REFRESHER'){
							//echo frmTextAreaNonEditLink($formDB->row,"related", "Full course", "$rootUrl/modules/training/index.php");
						}
						echo frmHiddenField($pageVars->course_id,"edited_id");
						echo frmHiddenField($ajax_message,"ajax_message");
					echo "</div>";
				
				} else {			
					//show form
					
					include("$rootPath/modules/training/content/page.course_edit.php");
				}
			}
	}
}
?>