<?php
####make this a secure page
$secure = true;
$ajaxModule = "training";

require("../../../../../shared/startPage.php");

$pageFiles->addModFunc("training_edit");
$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$ajax_message = getStrFromRequest("ajax_message");
$email = getStrFromRequest("email");
$tkn = getStrFromRequest("tkn");

if (isAllowed("recordTraining")) {
	if ($pageVars->trainee_id !== null) {
		$qry = "select usr.*, usr.username as email from usr where userid = {$pageVars->trainee_id}";
		$formDB = new pageQry($con, $qry);
		$formDB->rsItem(); 
		
		$load = getStrFromRequest("load");

		if (buttonClicked("save")) {
			
			if ($pageVars->trainee_id == 0) {
				#check that a user doesn't exist with this name
				$rsCheck = getRS($con,"select * from usr where username = '".$email."'");
				if (getRow($rsCheck)) {
					echo "##ERROR: ";
					echo USERNAME_IN_USE;	
				} else {	
							
					$qry = new dbInsert("usr");
					$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
					$pw = generatePassword();
					$qry->setStringVal("password",$pw,"Password");
					
				}		
			} else {
				$qry = new dbUpdate("usr");
				$qry->setParam("userid", $pageVars->trainee_id);
			}		
			//personal details
			$qry->setReqStringVal("firstname",$_POST["firstname"],"First name");
			$qry->setReqStringVal("lastname",$_POST["lastname"],"Last name");
			$qry->setReqStringVal("username",$_POST["email"],"Email");
			$qry->setReqStringVal("email",$_POST["email"],"Email");
			$qry->setReqStringVal("job_title",$_POST["job_title"],"Job title");
			$qry->setStringVal("job_desc",$_POST["job_desc"],"Job description");
			
			$qry->setAudit($clsUS->userid);
			
			
			if ($qry->execute($con)) {
				if ($pageVars->trainee_id == 0) {
					$pageVars->trainee_id = $qry->getNewID();	
					//now insert into usertype
					$utQry = new dbInsert('usr_usrtype');
					$utQry->setNumberVal("userid", $pageVars->trainee_id, "userid");
					$utQry->setNumberVal("tasklistid", 4, "tasklistid");
					$utQry->setStringVal("usertype","Training Staff","usertype");
					if (!$utQry->execute($con)) {
						echo $utQry->getError;
					}
				}
				//echo any parameters here that will be passed back to this page when successful
				echo "&trainee_id=".$pageVars->trainee_id;
			} else {
				echo "##ERROR: ";
				echo $qry->getError();
			}			
			
			
		} else if (buttonClicked("cancel")) {
			
			echo "cancel";
			
		} else if (buttonClicked("delete")) {
			//check usertyopes for this user. if they have other usertypes other than Training Staff, then don't delete.
			$usr_qry = "select * from usr_usrtype where userid = {$pageVars->trainee_id} and usertype != 'Training Staff'";
			$usr_rs = getRS($con,$usr_qry);
			if (getResultRows($usr_rs) == 0) {
				$qry = new dbUpdate("usr");
				$qry->setParam("userid", $pageVars->trainee_id);
				$qry->setReqStringVal("state","DELETED","state");
				
				if ($qry->execute($con)) {
			
					echo "&trainee_id=".$pageVars->trainee_id."&ajax_message=User%20deleted";
				} else {
					echo "##ERROR: ";
					echo $qry->getError();
				}
			}
			
		} else {
			if ($load == 'yes') {
				$qry = "select * from usr where userid = ".$pageVars->trainee_id;
				$formDB = new pageQry($con, $qry);
				$formDB->rsItem();
				echo "<div class=\"col\">";
					echo frmTextAreaNonEdit($formDB->row,"firstname","First Name");
					echo frmTextAreaNonEdit($formDB->row,"lastname","Last Name");
					echo frmTextAreaNonEdit($formDB->row,"email","Email");
				echo "</div>";
				echo "<div class=\"col\">";
					echo frmTextAreaNonEdit($formDB->row,"job_title","Job Title");
					echo frmTextAreaNonEdit($formDB->row,"job_desc","Job Description");
					echo frmHiddenField($pageVars->trainee_id,"edited_id");
					echo frmHiddenField($ajax_message,"ajax_message");

				echo "</div>";
			
			} else {			
				//show form
				
				include("$rootPath/modules/training/content/page.user_edit.php");
			}
		}
	}
}
?>