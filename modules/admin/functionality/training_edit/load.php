<?php
$this->addModuleLibPhp("funcTrainingEdit.php");
$this->addCoreLibPhp("security/funcPassword.php");
$this->addModuleFuncJs("trn.get_user.jquery.js");
$this->addModuleFuncJs("trn.get_course.jquery.js");
$this->addModuleFuncJs("trn.get_provider.jquery.js");
$this->addModuleFuncJs("trn.course_date_calcs.jquery.js");
$this->addModuleFuncJs("trn.upload_file_submit.js");
$this->addModuleFuncJs("trn.edit_system_course.js");

$this->addCoreLibPhp("clsDb/clsDbMultiAssign.php");

$this->csrf = 'ajax';

?>