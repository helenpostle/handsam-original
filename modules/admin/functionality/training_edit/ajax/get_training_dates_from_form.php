<?php
####make this a secure page
$secure = true;
$ajaxModule = "training";

require("../../../../../shared/startPage.php");

$pageFiles->addModFunc("training_edit");
//$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");

$pageFiles->includePhp();

//$pageVars = new loadVars($clsUS,$con);
 

$renewal = getStrFromRequest('renewal');
$refresher = getStrFromRequest('refresher');
$renewal_reminder = getStrFromRequest('renewal_reminder');
$refresher_reminder = getStrFromRequest('refresher_reminder');

$course_date = strtotime(getStrFromRequest('course_date'));
$course_date = strftime("%Y-%m-%d",  $course_date);

if (isAllowed("recordTraining")) {
	echo displayTrainingDateField($renewal, "", "renewal", "Renewal Date", $course_date);	
	//echo displayTrainingDateField($refresher, "", "refresher", "Refresher Date", $course_date);
	echo displayTrainingDateField($renewal_reminder, $renewal, "renewal_reminder", "Renewal Reminder Date", $course_date);
	//echo displayTrainingDateField($refresher_reminder, $refresher, "refresher_reminder", "Refresher Reminder Date", $course_date);		
}


function displayTrainingDateField($period1, $period2, $field, $title, $course_date) {
	//now calculate dates:
	$new_date = "";
	if ($field == "renewal" || $field == "refresher") {
		if ($period1 > 0) $new_date = strtotime("$course_date + $period1");
	} else if ($field == "renewal_reminder"){
		if ($period1 > 0 && $period2 > 0) $new_date = strtotime("$course_date + $period2 -$period1");
	} else if ($field == "refresher_reminder") {
		if ($period1 > 0 && $period2 > 0) $new_date = strtotime("$course_date + $period2 -$period1");
	}
	if ($new_date != "") $new_date = strftime("%a, %d %b %Y",$new_date);	
	$str = "";
	
	$str .= "<input type=\"hidden\" name=\"".$field."_date\" value=\"$new_date\"/>";

	$str .= "<div class=\"view_date clearfix\">";
	$str .= "<span class=\"label\">$title </span>";
	$str .= "<span class=\"value\" id=\"".$field."_date_a\"><span>$period1</span></span>";

	$str .= "<span class=\"value\" id=\"".$field."_date_b\"><span>$new_date</span></span>";
	$str .= "</div>";
	
	return $str;
}

?>