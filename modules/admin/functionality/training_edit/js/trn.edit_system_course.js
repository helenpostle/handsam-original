
// show/hide functionality for the 'Is online course' checkbox
// also sets the cours_url as required if it's selected

$(document).ready(function(){
	
	var checkbox = $('#online');
	var urlbox = $('#course_url').parent('div');
	
	// check that the element exists, some browsers t
	if(checkbox)
	{
		urlbox.hide();	
		if(checkbox.is(':checked'))
		{
			urlbox.show();
			urlbox.addClass('required');
		}
		
		checkbox.change(function(){
			urlbox.toggle(250);
			urlbox.toggleClass(function(){
					if($(this).hasClass('required'))
						return 'optional';
					else if($(this).hasClass('optional'))
						return 'required';
				});
		});
	}
	
});