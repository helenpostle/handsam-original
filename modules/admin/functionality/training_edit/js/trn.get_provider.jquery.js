$(document).ready(function(){
	if ($("#provider_id").length > 0) {
		var provider_id = $("#provider_id").val();
		
		$.post(siteRoot + "/modules/training/ajax/get_provider.php", {provider_id: provider_id}, function(data){
  			$("#provider_detail").html(data);
		});	
			
		$("#provider_id").change(function(event){
			var provider_id = $(this).val();
			$.post(siteRoot + "/modules/training/ajax/get_provider.php", {provider_id: provider_id}, function(data){
	  			$("#provider_detail").html(data);
			});	
	
		});
	}

});