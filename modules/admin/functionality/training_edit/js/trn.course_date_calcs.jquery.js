$(document).ready(function(){
	
		if ( $.browser.msie && $.browser.version < 7) {
			$('input.date').datepicker({
				dateFormat: 'D, dd M yy',
				onSelect: function(dateText, inst) {
					updateCourseDates(dateText)
				}
			});
			$('span.clear_date').live('click', function() {
				$('#renewal_reminder_date_a span').html('');
				$('#renewal_reminder_date_b span').html('');
				$('#renewal_date_a span').html('');
				$('#renewal_date_b span').html('');
				$('input[name="renewal_date"]').val('');
				$('input[name="renewal_reminder_date"]').val('');
				
			});
		} else {
			$('input.date').datepicker({
				dateFormat: 'D, dd M yy',
				onSelect: function(dateText, inst) {
					updateCourseDates(dateText)
				}
			}).focus(function (event) {
				jQuery.datepicker.afterShow(event);
			});
		}



	function updateCourseDates(cal) {
		var course_date = $('[name=course_date]').val();
		var course_date = cal;
		var renewal = $('#renewal').html();
		var refresher = $('#refresher').html();
		var renewal_reminder = $('#renewal_reminder').html();
		var refresher_reminder = $('#refresher_reminder').html();
		
		$.post(siteRoot + "/modules/training/functionality/training_edit/ajax/get_training_dates_from_form.php", {renewal: renewal, course_date:course_date, refresher:refresher, renewal_reminder: renewal_reminder, refresher_reminder:refresher_reminder}, function (data) {
			$("#dates_update").html(data);
		});	
	}
});