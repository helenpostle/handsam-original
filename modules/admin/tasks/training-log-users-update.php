<?php

/**
 * Get all user accounts with the Training Log module
 * Ensure all user have the Training Staff usertype
 *
 */
// db logons here...
require("../../../shared/config.php");

$sql = "SELECT u.userid, ut.usertype FROM usr AS u
					JOIN usr_usrtype AS ut
						ON u.userid = ut.userid
					WHERE u.accountid IN (SELECT accountid FROM account where state = 'ACTIVE') AND u.state <> 'DELETED';";
					
$con = mysql_connect($dbhost, $dbuser, $password) or die("can't connect to MySQL");
mysql_select_db($dbname,$con) or die('Can\'t connect to DB');

$res = mysql_query($sql, $con) or die(mysql_error());

$id = 0;
while ($row = mysql_fetch_assoc($res))
{
	if($row['userid'] != $id)
	{
		$id = $row['userid'];
		$user_types[$id] = array();
		array_push($user_types[$id], $row['usertype']);
	}
	else
	{
		array_push($user_types[$id], $row['usertype']);
	}
}

// check if they have Staff Training usrtype
foreach($user_types as $id_k => $ut)
{
	// if not Training Staff, add it
	if(in_array('Training Staff', $ut) == false)
	{
		$sql = "INSERT INTO usr_usrtype (usertype, userid) VALUES ('Training Staff', $id_k);";
		mysql_query($sql, $con);
		doLog('INFO', 'ADDED THE TRAINING STAFF USERTYPE TO USER ID'.$id_k);
	}
}

exit();

// end of file ./trunk/web/modules/training/tasks/training-log-users-update.php 