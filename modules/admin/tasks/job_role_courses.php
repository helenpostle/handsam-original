<?php

/**
 * job_role_courses.php
 *
 * Check each user is assigned the corect courses for their job roles
 *
 * Called daily in ./tasks.php 
 *
 * Author: Mei Gwilym
 *
 */
// get list of account's users and their job roles
// get list of job roles and their courses
// compare the user's courses vs what they should have based on their job roles
// - add any courses that they should have that aren't
// - mark any courses that they have but shouldn't have with NO JOB ROLE
// training_log `flag` field MANUAL (DEFAULT) | NEW | JOB ROLE | NO JOB ROLE
// DB stuff
doLog('INFO', 'Checking user\'s courses match their job role requirements');
// libs/functions etc
require("$rootPath/handsam_core/library/funcJobRole/funcJobRole.php");
require("$rootPath/modules/training/library/funcUser.php");
require("$rootPath/modules/training/library/funcTrainingList.php");

// users and their job roles
$user_jobroles = getUsersJobRoles();

// users and their courses
$user_courses = getUsersCourses(array('IN PLANNING', 'IN PROGRESS'));

// job roles and their courses
$jobroles = getJobRoleCourses();

$active_users = array();
//create active users array - only send alerts to users in this array
$active_qry = "select userid from usr u left join account a on u.accountid = a.accountid where u.state='ACTIVE' and a.state='ACTIVE'";
$active_rs = getRS($con, $active_qry);
While ($active_row = getRow($active_rs)) {
	$active_users[] = $active_row['userid'];
}


foreach ($user_jobroles as $k => $uj) {
    // $k is the user_id
	//only process if user is active
	if (in_array($k, $active_users)) {	
	
		// get a list of courses they should have
		$should_have_course = array(); // worst.variable name.ever
		foreach ($uj['jobroles'] as $jr) 
		{
			if (isset($jobroles[$jr])) 
			{
				foreach ($jobroles[$jr]['training_course'] as $tc) 
				{
					$should_have_course[] = $tc;
				}
			}
		}
		if (isset($user_courses[$k]) && $uc = $user_courses[$k]) { // deliberate use of single = 
			// so that $uc == $uj
			// list of courses they already have
			$present_courses = $uc['training_courses'];
			// list of courses they have that are extra to what they should have
			$overflow_courses = array_diff($present_courses, $should_have_course);

			if (count($overflow_courses) > 0) {
				// check course flag if JOB ROLE then change to NO JOB ROLE
				foreach ($overflow_courses as $ovc) {
					$course_log = getCourseFlag($k, $ovc);
					if ($course_log['flag'] == 'JOB ROLE') {
						doLog('INFO', 'Found a overflow course, training_id: ' . $course_log['training_id']);
						setCourseFlag($course_log['training_id'], 'NO JOB ROLE');
					}
				}
			}
			// list of courses that the user *should* have
			$missing_courses = array_diff($should_have_course, $present_courses);

			if (count($missing_courses) > 0) {
				// add these courses to the user
				foreach ($missing_courses AS $mc) {
					// user_id, course_id, flag
					
					saveUserCourse($k, $mc, 'JOB ROLE');
					doLog('INFO', 'Job role course check: Added the missing course, id: ' . $mc);
				}
			}
		}
	}
} // user loop end

