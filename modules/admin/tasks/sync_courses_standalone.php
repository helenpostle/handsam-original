<?php
/**
* nightly tasks page
*/
ini_set('max_execution_time', '0'); // 0 = no limit.
error_reporting(E_ALL);
ini_set("display_errors", 1); 
$secure = true;
require(dirname(dirname(dirname(dirname(__FILE__))))."/shared/config.php");
require("$rootPath/shared/messages.php");

require("$rootPath/handsam_core/library/funcError/funcError.php");

//any errors which occur to be handled by this function
if (!$debug) {
	set_error_handler("myErrorHandler");
}

require("$rootPath/handsam_core/library/funcPage/funcGeneral.php");
require("$rootPath/handsam_core/library/funcDb/funcDb.mySQL.php");
include("$rootPath/handsam_core/library/funcMail/funcMail.php");
require_once("$rootPath/handsam_core/library/security/clsXssSecurity.php");
include("$rootPath/handsam_core/library/funcPage/funcRequest.php");
include("$rootPath/handsam_core/library/funcDisplay/funcDisplay.php");
include("$rootPath/handsam_core/library/funcDb/funcDbEdit.php");	
require_once("$rootPath/handsam_core/library/funcError/funcLog.php");
require("$rootPath/handsam_core/library/funcError/funcDebugLog.php");

require_once("$rootPath/handsam_core/library/clsPage/clsPageLoadFiles.php");

require_once("$rootPath/handsam_core/library/htmlpurifier-4.3.0-standalone/HTMLPurifier.standalone.php");
include("$rootPath/shared/htmlpurifier.config.php");

$allow_html_purifier = new HTMLPurifier($html_purifier_config);
$nohtml_page_purifier = new HTMLPurifier($nohtml_purifier_config);

$_SERVER["PHP_SELF"] = cleanUrl($nohtml_page_purifier->purify($_SERVER["PHP_SELF"]));
$get_req = new requestGet();
$post_req = new requestPost();
$cookie_req = new requestCookie();	

$errorFlag = ""; 
$message = "";
$varOut="";

//make a connection to the database
$con = getConnection($dbhost,$dbname,$dbuser,$password);

$this_mod_dir = 'training';

require_once("handsam_core/library/clsPage/clsPageLoadFiles.php");
//initiate class for this page
$pageFiles = new clsLoadPageFiles($rootPath, $rootUrl, $jQuery, $this_mod_dir);

require_once("$rootPath/handsam_core/library/security/securePage.php");
require_once("$rootPath/handsam_core/library/security/clsCsrfSecurity.php");

/**
 * include any tasks to process below here.
 * available variables:
 * $con - as usual.
 * $messages[] - add to this array any result to send in final admin email.
 */

$pageFiles->includePhp();
if (isAllowed("editAccount"))
{
  include("$rootPath/modules/training/tasks/sync_courses.php");
  //$messages = array('Courses Synced');
  $body = "";  
  if (isset($messages) && is_array($messages)) {
    foreach($messages as $key => $value) {
      //echo nl2br($value)."<br>"; //echo to screen
      $body .= nl2br($value)."<br>";

    }
  }
}

#Include layout page

include("$rootPath/layout.php");
?>



	





