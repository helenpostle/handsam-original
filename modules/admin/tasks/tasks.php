<?php

/**
 * Tasks page is called by cron
 */

// set the module name
$this_mod_dir = 'training';

include("$rootPath/modules/training/tasks/training_log_reminders.php");
// sync online courses via the ext courses app  api
include("$rootPath/modules/training/tasks/sync_courses.php");

// check if each user has the correct courses correlating to their job roles
include("$rootPath/modules/training/tasks/job_role_courses.php");
