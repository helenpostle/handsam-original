<?php

if ($send_email_alerts) { 
	doLog("INFO","TRAINING LOG REMINDERS:");
	

	#go through all users which have training log reminders due
	
	$qry = "select distinct u.*, tl.*, tc.title";
	$qry .= " from training_log tl left join usr u on tl.trainee_id = u.userid left join training_course tc on tl.course_id = tc.course_id left join account a on u.accountid = a.accountid where  tl.user_renewal_email = 0 and  tl.state = 'IN PROGRESS'  and renewal_reminder_date <= NOW() and tl.state = 'IN PROGRESS' and tl.renewal_reminder_date is not null and tl.renewal_reminder_date != '' and u.state='ACTIVE' and a.state = 'ACTIVE' ORDER BY tl.accountid, tl.trainee_id";
	echo $qry;
	$result = getRS($con, $qry);
	$user_email_arr = array();
	$admin_email_arr = array();
	$userid = 0;
	$user_txt = "";
	//$admin_txt = "";
	$renewal_id = array();
	while ($row = getRow($result)) {
		
		$userid = $row['trainee_id'];
		$name = $row['firstname']." ".$row['lastname'];
		$accountid = $row['accountid'];
		$email = $row['username'];
		$training_id = $row['training_id'];
		
		//if not already sent a reminder to user add to user email arr
		if ($row['user_renewal_email'] == 0) {
			$user_email_arr[$userid]['training'][$training_id] = "Renewal reminder - {$row['title']} expires on  ".date("l, jS F Y",strtotime($row['renewal_date']))." \r\n";
			$user_email_arr[$userid]['email'] = $email;
			$user_email_arr[$userid]['name'] = $name;
			
		}
		
		//if not already sent an admin email, add this to admin email arr
		if ($row['admin_renewal_email'] == 0) {
			//get all admin userids
			$admin_qry  = "select u.* from usr u left join usr_usrtype ut on u.userid = ut.userid where ut.usertype = 'Training Admin' and u.accountid = ".$accountid;
			$admin_rs = getRS($con,$admin_qry);
			while ($arow = getRow($admin_rs)) {
				$adminid = $arow['userid'];
				$admin_email_arr[$adminid]['training'][$training_id] = "Renewal reminder - {$row['title']} expires on  ".date("l, jS F Y",strtotime($row['renewal_date']))." - user: $name - email: $email \r\n";
				$admin_email_arr[$adminid]['email'] = $arow['email'];
				$admin_email_arr[$adminid]['name'] = $arow['firstname']." ". $arow['lastname'];
			}
		}
	}
	
	//now we just loop through each array and send the email and update the training_log as mail sent
	// first the user emails
	doLog("INFO","TRAINING USER REMINDERS:");
	foreach ($user_email_arr as $a_id => $a_user) {
		$renewal_id = array();
		// @todo we may need to check here in the future if this user has email alerts on or off.
		$msg = "";
		foreach ($a_user['training'] as $a_tid => $a_training) {
			$renewal_id[] =  $a_tid;	
			$msg .= $a_training;
		}
                
                $msg = getEmailTemplate('training_user_reminder', $msg);		
		
		if (sendMail($a_user['email'], $alertsMail, "HandSaM Training Log Course Renewal Reminder", $msg)) {
			#now update training_log to mail sent for this item
			
			doLog("INFO","Username: ".$a_user['email']." :: userid: $a_id  :: EMAIL SENT");
			
			//update renewal email sent in database
			$qry = new dbUpdate("training_log");
			$qry->setParamInArray("training_id",$renewal_id);
			$qry->setReqNumberVal("user_renewal_email",1,"user_renewal_email");
					
			if ($qry->execute($con)) {
				doLog("INFO","training_id: ".implode(',',$renewal_id)." :: Updated: user_renewal_email: ".$qry->getSql());
			} else  {
				doLog("INFO","training_id: ".implode(',',$renewal_id)." :: ERROR UPDATING: user_renewal_email: ".$qry->getSql()." :: ".$qry->getError());
			}

				
	
		} else {
			doLog("INFO","Username: ".$a_user['email']." :: userid: $a_id :: training_id: ".implode(',',$renewal_id)." :: EMAIL FAILURE");
		}
				
	}
		
	//and now the same for the admin emails
	
	doLog("INFO","TRAINING ADMIN REMINDERS:");
	foreach ($admin_email_arr as $a_id => $a_user) {
		$renewal_id = array();
		//we may need to check here in the future if this user has email alerts on or off.
		$msg = "";
		foreach ($a_user['training'] as $a_tid => $a_training) {
			$renewal_id[] =  $a_tid;
			$msg .= $a_training;
		}
                
                $msg = getEmailTemplate('training_admin_reminder', $msg);	
		
		if (sendMail($a_user['email'], $alertsMail, "HandSaM Training Log Course Renewal Reminder", $msg)) {
			#now update training_log to mail sent for this item
			
			doLog("INFO","Username: ".$a_user['email']." :: userid: $a_id :: EMAIL SENT");
			
			//update renewal email sent in database
			$qry = new dbUpdate("training_log");
			$qry->setParamInArray("training_id",$renewal_id);
			$qry->setReqNumberVal("admin_renewal_email",1,"admin_renewal_email");
					
			if ($qry->execute($con)) {
				doLog("INFO","training_id: ".implode(',',$renewal_id)." :: Updated: user_renewal_email: ".$qry->getSql());
			} else  {
				doLog("INFO","training_id: ".implode(',',$renewal_id)." :: ERROR UPDATING: user_renewal_email: ".$qry->getSql()." :: ".$qry->getError());
			}

				
	
		} else {
			doLog("INFO","Username: ".$a_user['email']." :: userid: $a_id :: training_id: ".implode(',',$renewal_id)." :: EMAIL FAILURE");
		}
			
	}
}