<?php

/** 
 * Receives the request from Roger. 
 *
 *   
 */
 //log all errors as the text displayed on this page is logged back in roger
error_reporting(E_ALL);
ini_set("display_errors", 1); 
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');
// default header is internal error
header("HTTP/1.1 500 Internal Server Error", true, 500);

//todo

//1. better logging of queries in array or json obj TICK
//2. tidy up all code so far in taskadmin, hooks, clsReceive TICK
//3. add multi select hooks to task admin

?>

If you are training tonight (Friday 8 September 2017) you have come to this page because I am trying to find 
out why the Training App is not talking to the Handsam App.
<p>Be assured that your course results have been saved on the Training App but have not been passed
back to the Handsam App yet.  This is currently only happening once a day instead of immediately</p>
<p>Please ignore the rest of this page and <a href="https://handsam.org.uk/modules/training/my_training.php"> click here</a> to go back to the Handsam App</p>