<?php
/*
 * format the course date for month grouping
 */

function formatGroupDate($date)
{
  return strftime('%Y-%m',strtotime($date));
}

function formatColHeading($date)
{
  return strftime('%Y - %b',strtotime($date));
}
?>