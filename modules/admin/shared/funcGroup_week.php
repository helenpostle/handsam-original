<?php
/*
 * format the course date for week grouping
 */

function formatGroupDate($date)
{
  return strftime('%Y-%U',strtotime($date));
}

function formatColHeading($date)
{
  return strftime('%Y - Week %U',strtotime($date));
}
?>