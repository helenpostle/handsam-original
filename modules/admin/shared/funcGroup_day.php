<?php
/*
 * format the course date for day grouping
 */

function formatGroupDate($date)
{
  return strftime('%Y-%j',strtotime($date));
}

function formatColHeading($date)
{
  return displayDate($date);
}
?>