<?php
$trn_menu = "";
if (isAllowed("recordTraining")) {
	$trn_menu = $clsMenu->addMenuItem("/modules/training/index.php", "TRAINING LOG", "", $trn_menu);
} else if(isAllowed("viewTraining")) {
	$trn_menu = $clsMenu->addMenuItem("/modules/training/my_training.php", "TRAINING LOG", "", $trn_menu);
} else if(isAllowed("superAdminTraining")) { 
	$trn_menu = $clsMenu->addMenuItem("/modules/training/list_system_courses.php", "TRAINING LOG", "", $trn_menu);
}

//training app menu items

if (isAllowed("viewTraining")) { 
	$clsMenu->addMenuItem("/modules/training/my_training.php", "MY TRAINING", "", $trn_menu);
}

if (isAllowed("recordTraining")) { 
	$clsMenu->addMenuItem("/modules/training/index.php", "TRAINING ADMIN", "", $trn_menu);
}

if ((isAllowed("recordTraining") || isAllowed("superAdminTraining")) && $training_conf['training_multi_assign']) { 
	$clsMenu->addMenuItem("/modules/training/assign.php", "ASSIGN TRAINING", "", $trn_menu);
}

if (isAllowed("superAdminTraining")) { 
	$clsMenu->addMenuItem("/modules/training/list_system_courses.php", "SYSTEM COURSES", "", $trn_menu);
}

if (isset($subpageof)) $clsMenu->setThisPage($subpageof);

?>