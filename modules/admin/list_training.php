<?php
/* List training
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");

$pageFiles->addModFunc("training_list");
$pageFiles->addModFunc("training_popup");

$pageFiles->includePhp();
//priority for module functionality boxes over page boxes
$pageFiles->addBoxPriority();
$pageVars = new loadVars($clsUS,$con);
if ($pageVars->accountid==null) $pageVars->accountid=$clsUS->accountid;
if (isAllowed("recordTraining")) {

	if (!isset($pageVars->training_id)) $pageFiles->addModFunc("training_filter");
	//if (!isset($pageVars->accidentid)) $pageFiles->addModFunc("accident_list");
	$qs = array();
	//$lotc_highlight = "PLANNING";


	//get filter array querystring
	
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	
	if (!isset($qs['date']))  $qs['date'] = 'course_date';
	
	//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;
	$filter_title = array();
	$filter_qry = array();
	$filter_date_field = " tl.course_date ";
	//process filters
	//state filter
	if (is_array($qs) && array_key_exists('state', $qs)) { 
		switch ($qs['state']) {
			case "ALL":
			$filter_qry['state'] = " tl.state != 'DELETED' ";
			$filter_title['state'] = "Completed &amp; Draft Forms";
			break;
		
			case "IN PLANNING":
			$filter_qry['state'] = " tl.state = 'IN PLANNING' ";
			$filter_title['state'] = "Draft Training";
			break;
						
			case "IN PROGRESS":
			$filter_qry['state'] = " tl.state = 'IN PROGRESS' ";
			$filter_title['state'] = "Assigned Training";
			break;
    
      case "COMPLETE":
			$filter_qry['state'] = " tl.state = 'COMPLETE' ";
			$filter_title['state'] = "Completed Training";
			break;

			case "DELETED":
			$filter_qry['state'] = " tl.state = 'DELETED' ";
			$filter_title['state'] = "Deleted Training";
			break;
							
			default: 
			$filter_qry['state'] = " tl.state != 'DELETED' ";
			$filter_title['state'] = "Completed &amp; Draft Forms";
			break;
		}
	} else {
		$filter_qry['state'] = " tl.state != 'DELETED' ";
		$filter_title['state'] = "Completed &amp; Draft Forms";
	}
	
	
	//choose if date created ot visit date filtered
	if (is_array($qs) && array_key_exists('date', $qs)) { 
		switch ($qs['date']) {
			case "course_date":
			$filter_date_field = " tl.course_date ";
			$filter_title['date'] = "Course Date";
			break;
			
			case "renewal_date":
			$filter_date_field = " tl.renewal_date ";
			$filter_title['date'] = "Renewal Date";
			break;
			/*
			case "refresher_date":
			$filter_title['date'] = "Refresher Date";
			break;
			*/	
			default: 
			$filter_date_field = " tl.course_date ";
			$filter_title['date'] = "Course Date";
			break;
		}
	}	

	//month filter
	if (is_array($qs) && array_key_exists('month', $qs) && $qs['month'] >= 1 && ($qs['date'] == "renewal_date" || $qs['date'] == "course_date")) {
		settype($qs['month'],"integer");
		$filter_qry['month'] = " MONTH($filter_date_field) = '{$qs['month']}' ";
		$filter_title['month'] = date( 'F', mktime(0, 0, 0, $qs['month']) ); 
	} else {
		$filter_title['month'] = "All months"; 

	}	
	
	//year filter
	if (is_array($qs) && array_key_exists('year', $qs) && $qs['year'] >= 1 && ($qs['date'] == "renewal_date" || $qs['date'] == "course_date")) {
		settype($qs['year'],"integer");
		$filter_qry['year'] = " YEAR($filter_date_field) = '{$qs['year']}' ";
		$filter_title['year'] = $qs['year']; 
	} else {
		$filter_title['year'] = "All Years"; 

	}	
	
	//account filter
	if (is_array($qs) && array_key_exists('account', $qs) && $qs['account'] >= 1 && ($qs['date'] == "renewal_date" || $qs['date'] == "course_date")) {
		settype($qs['account'],"integer");
		$filter_qry['account'] = " tl.accountid = '".$qs['account']."' ";
		$filter_title['account'] = "Account:";
	} else {
		$filter_title['account'] = "All Accounts"; 

	}	
	
	//course filter
	if (is_array($qs) && array_key_exists('course', $qs) && $qs['course'] > 0) { 
		settype($qs['course'],"integer");
		$filter_qry['course'] = " tl.course_id = '".$qs['course']."' ";
		//$filter_title['course'] = "Course:";
	} else {
		///$filter_title['course'] = "All Courses";
	}

	//user filter
	if (is_array($qs) && array_key_exists('user', $qs) && $qs['user'] > 0) { 
		settype($qs['user'],"integer");
		$filter_qry['user'] = " tl.trainee_id = '".$qs['user']."' ";
	}

	//provider filter
	if (is_array($qs) && array_key_exists('provider', $qs) && $qs['provider'] > 0) { 
		settype($qs['provider'],"integer");
		$filter_qry['provider'] = " tl.provider_id = '".$qs['provider']."' ";
		//$filter_title['course'] = "Provider:";
	} else {
		///$filter_title['course'] = "All Providers";
	}
		
	
	//now implode filter title
	
	$filter_title_str = ": ".implode(": ",$filter_title);
			
	if ($pageVars->training_id !== null) {
	} else {
		
		//list training courses initially order by renewal/expire date
		$title = "Account Training Log: $filter_title_str";
		$content = "page.alltraining_list.php";
		//$box[] = "box.user_edit.php";
		$pageDb = getTrainingList($pageVars, $con, $filter_qry);
		//echo $pageDb;
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>