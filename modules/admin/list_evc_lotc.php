<?php
/**
* Plan editing functionality
*
*/
error_reporting(E_ALL); 
 ini_set("display_errors", 1); 
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");
require("../../modules/lotc/library/funcLotcDisplayVisit.php");
require("../../modules/lotc/library/funcLotcVisits.php");
require("../../modules/lotc/library/clsUserApproval.php");
require("../../modules/lotc/library/funcLotcDisplayForm.php");
require("../../modules/lotc/library/funcLotcProcessForm.php");
$pageFiles->addModFunc("lotc_visit_filter");
//include_once("../../modules/lotc/functionality/lotc_visit_filter/js/lotc.visit_filter_1.0.jquery.js");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
if ($pageVars->hacking) headerLocation("$rootUrl/index.php");

$filter_array = array();
$filter_array['state'] = array("GOVERNOR APPROVAL" => "Governor approval", "DEPUTY PRINCIPAL APPROVAL" => "Deputy Principal approval", "PRINCIPAL APPROVAL" => "Principal approval", "FINANCE APPROVAL" => "Finance approval", "PLANNING" => "In planning", "APPROVAL" => "EVC approval", "COMPLETED" => "Completed", "DELETED" => "Deleted");
	
$qs = array();
	$filter_title_str = "";
	
	//get filter array querystring
	
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;
	$filter_title = array();
	$filter_qry = array();
	$filter_date_field = " a_visit.created ";

	//set the crumbtrail first link back to the evc page with list of visits
	$crumb_back = "<a href=\"evc.php?accountid={$pageVars->accountid}&amp;rand=".rand()."&amp;filter=".urlencode(serialize($qs))."\" title=\"EVC LOtC List\">EVC LOtC list</a>";
	//process filters
	//state filter
  
	if (is_array($qs) && array_key_exists('state', $qs)) { 
		switch ($qs['state']) {
      case '0': 
			$filter_qry['state'] = " a_visit.state != 'DELETED' ";
			$filter_title['state'] = "All";
			break;
    
      case "DEFAULT":
			$filter_qry['state'] = " (a_visit.state = 'PLANNING' or  a_visit.state = 'APPROVAL') ";
			$filter_title['state'] = "Planning &amp; Approval";
			break;
    
			case "ALL":
			$filter_qry['state'] = " a_visit.state != 'DELETED' ";
			$filter_title['state'] = "All States";
			break;
		
			case "PLANNING":
			$filter_qry['state'] = " a_visit.state = 'PLANNING' ";
			$filter_title['state'] = "Planning";
			break;
			
      case "FINANCE APPROVAL":
			$filter_qry['state'] = " a_visit.state = 'FINANCE APPROVAL' ";
			$filter_title['state'] = "Finance Approval";
			break;
    
			case "PRINCIPAL APPROVAL":
			$filter_qry['state'] = " a_visit.state = 'PRINCIPAL APPROVAL' ";
			$filter_title['state'] = "Principal Approval";
			break;
    
			case "DEPUTY PRINCIPAL APPROVAL":
			$filter_qry['state'] = " a_visit.state = 'DEPUTY PRINCIPAL APPROVAL' ";
			$filter_title['state'] = "Deputy Principal Approval";
			break;
    
			case "GOVERNOR APPROVAL":
                $filter_qry['state'] = " a_visit.state = 'GOVERNOR APPROVAL' ";
                $filter_title['state'] = "Governor Approval";
                break;
            
            case "APPROVAL":
			$filter_qry['state'] = " a_visit.state = 'APPROVAL' ";
			$filter_title['state'] = "Approval";
			break;
			
			case "COMPLETED":
			$filter_qry['state'] = " a_visit.state = 'COMPLETED' ";
			$filter_title['state'] = "Completed";
			break;

			case "DELETED":
			$filter_qry['state'] = " a_visit.state = 'DELETED' ";
			$filter_title['state'] = "Deleted";
			break;
							
				
			default: 
			$filter_qry['state'] = " (a_visit.state = 'PLANNING' or  a_visit.state = 'APPROVAL') ";
			$filter_title['state'] = "Planning &amp; Approval";
      $qs_base['state'] = 'DEFAULT';
			break;
		}
	} else {
		$filter_qry['state'] = " (a_visit.state = 'PLANNING' or  a_visit.state = 'APPROVAL') ";
		$filter_title['state'] = "Planning &amp; Approval";
    $qs_base['state'] = 'DEFAULT';
	}
	//choose if date created ot visit date filtered
	if (is_array($qs) && array_key_exists('date', $qs)) { 
		switch ($qs['date']) {
			case "visit_date":
			$filter_date_field = " a_visit.visit_date ";
			$filter_title['date'] = "Visit Date";
			break;
				
			default: 
			//$filter_date_field = " a_visit.created ";
			//$filter_title['date'] = "Date Created";
			$filter_date_field = " a_visit.visit_date ";
			$filter_title['date'] = "Visit Date";
			break;
		}
	} else {
		$filter_date_field = " a_visit.visit_date ";
		$filter_title['date'] = "Visit Date";
	}

	//month filter
	if (is_array($qs) && array_key_exists('month', $qs) && $qs['month'] >= 1) {
		settype($qs['month'], 'string');
		$filter_qry['month'] = " MONTH($filter_date_field) = '{$qs['month']}' ";
		$filter_title['month'] = date( 'F', mktime(0, 0, 0, $qs['month']) );
	} else {
		$filter_title['month'] = "All months"; 

	}	
	
	//year filter
	if (is_array($qs) && array_key_exists('year', $qs) && $qs['year'] >= 1) {
		settype($qs['year'], 'int');
		$filter_qry['year'] = " YEAR($filter_date_field) = '{$qs['year']}' ";
		$filter_title['year'] = $qs['year']; 
	} else {
		$filter_title['year'] = "All Years"; 

	}
	
	//user filter
	if (is_array($qs) && array_key_exists('user', $qs) && $qs['user'] >= 1) {
		settype($qs['user'], 'int');
		//need to get the planer name
		$planner_qry = "select CONCAT(u.firstname, ' ', u.lastname) AS name from usr u where u.userid = {$qs['user']}";
		$plannerRS = getRS($con, $planner_qry);
		$plannerRow = getRow($plannerRS);
		$filter_qry['user'] = " leaderid = {$qs['user']} ";
		$filter_title['user'] = $plannerRow['name'];
	} else {
		$filter_title['user'] = "All Users"; 

	}	
	//account filter
	//var_dump($qs);
	if (is_array($qs) && array_key_exists('account', $qs)) {
		settype($qs['account'],"integer");
		$filter_qry['account'] = " a_visit.accountid = '".$qs['account']."' ";
		$filter_title['account'] = "Account:";
	} else {
		$filter_title['account'] = "All Accounts"; 

	}	
	//now implode filter title
	
	$filter_title_str = ": ".implode(": ",$filter_title);
	
	if ($pageVars->visitid !== null) {
	   //we have a visit id, show the visit
	   
	} else {
	   //we have no visit id, show the list
	   $title = "LOtC: Visit List $filter_title_str";
	   $content = "page.evc_visit_list.php";
	   $pageDb = getAdminVisitList($pageVars, $con, $filter_qry, true);
	}
	

include("../../layout.php");
?>