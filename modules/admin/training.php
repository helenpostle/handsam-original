<?php
/**
* training log editing functionality
*
*/

####make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc("training_edit");
$pageFiles->addModFunc("training_popup");

$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->includePhp();

$menuPage = "index.php";

$pageVars = new loadVars($clsUS, $con);

if ($pageVars->hacking) headerLocation("$rootUrl/index.php");
$qs = array();

$pageFiles->addJsVar('accountid', $pageVars->accountid);
$pageFiles->addJsVar('training_id', $pageVars->training_id);
$pageFiles->addJsVar('sessionid', session_id());

if (isAllowed("recordTraining")) {

	//get filter array querystring
	
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;
	
	$formDB = getTraining($con, $pageVars);

	if ($formDB['state'] == "IN PROGRESS" || $formDB['state'] == "DELETED")
	{
		$title = "View Training Log Item";
		$content = "page.training_view.php";
		if (buttonClicked("return")) {
			headerLocation("$rootUrl/modules/training/index.php?filter=".urlencode(serialize($qs)),$messages);
		}  else if (buttonClicked("edit")) {
			$updateState = changeTrainingState($con, $pageVars, $clsUS, "IN PLANNING");
			if ($updateState == "" ) {
				$messages[] = EDIT_STATE;
				headerLocation("$rootUrl/modules/training/training.php?training_id=".$pageVars->training_id."&filter=".urlencode(serialize($qs)),$messages);
			} else {
				$messages[] = $updateState;
			}
		}
		else if (buttonClicked("trn_complete")) 
		{
			// mark training course as complete
			$updateState = changeTrainingState($con, $pageVars, $clsUS, "COMPLETE");
			if ($updateState == "")
			{
				$messages[] = COMPLETED_STATE;
				
				// create a new instance of the course (i.e. the refresher course)
				// same courseid and traineeid, renewal date is start date, calc new renewal date
				$saveForm = createRefresherCourse($con, $pageVars, $clsUS, $formDB);
				/*
				// modify $pageVars
				$pageVars->training_id = 0;
				$pageVars->trainee_id = $formDB['trainee_id'];
				$pageVars->provider_id = $formDB['provider_id'];
				$pageVars->course_id = $formDB['course_id'];
				
				// set POST values artificially
				$_POST['training_id'] = 0; // override just in case
				$_POST['course_date'] = date('D, d M Y', strtotime($formDB['renewal_date'], strtotime($formDB['course_date'])) ); // old_course_date + old_renewal_date; Sat, 05 Jan 2012
				$_POST['renewal_date'] = $formDB['renewal_date']; 
				$_POST['renewal_reminder_date'] = $formDB['renewal_reminder_date']; // new_renewal_date - old_renewal_reminder_date
				$_POST["comments"] = $formDB['comments'];
				$_POST["cost"] = $formDB['cost'];
				
				$saveForm = saveCompleteForm($con, $pageVars, $clsUS, "IN PLANNING");
				*/
				if ($saveForm == "" ) {
					$messages[] = SAVED;
				}
				else
				{
					$messages[] = $saveForm;
				}
				headerLocation("$rootUrl/modules/training/index.php?filter=".urlencode(serialize($qs)),$messages);
			}
			else
			{
				$messages[] = $updateState;
			}
		}
		else if (buttonClicked("undelete")) {
			
			$updateState = changeTrainingState($con, $pageVars, $clsUS, "IN PLANNING");
			if ($updateState == "" ) {
				$messages[] = EDIT_STATE;
				headerLocation("$rootUrl/modules/training/training.php?training_id=".$pageVars->training_id."&filter=".urlencode(serialize($qs)),$messages);
			} else {
				$messages[] = $updateState;
			}
			
		}
		
	} else if(isAllowed("recordTraining") && ($formDB['state'] == "IN PLANNING" || $formDB['state'] == "")) {
		$title = "Edit Training Log Item";
		$content = "page.training_edit.php";
		
		if (buttonClicked("save_draft") || buttonClicked("upload_file") || isset($_POST['upload_file_id'])) {
			$saveForm = saveDraftForm($con, $pageVars, $clsUS, $formDB['state']);
			if ($saveForm == "" ) {
				$messages[] = SAVED;
				if (buttonClicked("upload_file") || isset($_POST['upload_file_id'])) {
					if (isset($_POST['upload_file_id'])) {
						$fileid = $_POST['upload_file_id']; 
					} else {
						$fileid = 0;
					}
					headerLocation("$rootUrl/modules/training/upload_file.php?fileid=$fileid&training_id=".$pageVars->training_id."&filter=".urlencode(serialize($qs)),$messages);
					die();
				} else {
					headerLocation("$rootUrl/modules/training/index.php?filter=".urlencode(serialize($qs)),$messages);
					die();
				}
			} else {
				$messages[] = $saveForm;
			}
				
		} else if (buttonClicked("save_submit")) {
			$saveForm = saveCompleteForm($con, $pageVars, $clsUS, "IN PROGRESS");
			if ($saveForm == "" ) {
				$messages[] = SAVED;
				headerLocation("$rootUrl/modules/training/index.php?filter=".urlencode(serialize($qs)),$messages);
			} else {
				$messages[] = $saveForm;
			}
		} else if (buttonClicked("cancel")) {
			headerLocation("$rootUrl/modules/training/index.php?filter=".urlencode(serialize($qs)),$messages);
		} else if (buttonClicked("delete")) {
			
			$updateState = changeTrainingState($con, $pageVars, $clsUS, "DELETED");
			if ($updateState == "" ) {
				$messages[] = DELETED;
				headerLocation("$rootUrl/modules/training/index.php?filter=".urlencode(serialize($qs)),$messages);
			} else {
				$messages[] = $updateState;
			}
			
		}

	}
	elseif($formDB['state'] == 'COMPLETE')
	{
		if (buttonClicked("return")) {
			headerLocation("$rootUrl/modules/training/index.php?filter=".urlencode(serialize($qs)),$messages);
		}
		$title = "View Training Log Item";
		$content = "page.training_view.php";
		
	}
	
} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>