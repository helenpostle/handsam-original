<?php
/**
* training log editing functionality
*
*/
####make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc("training_edit");
$pageFiles->addModFunc("training_popup");

$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->includePhp();

$menuPage = "my_training.php";

$pageVars = new loadVars($clsUS,$con);
if ($pageVars->accountid==null) $pageVars->accountid=$clsUS->accountid;

if ($pageVars->hacking) headerLocation("$rootUrl/index.php");
$qs = array();

$pageFiles->addJsVar('accountid', $pageVars->accountid);
$pageFiles->addJsVar('training_id', $pageVars->training_id);
$pageFiles->addJsVar('sessionid', session_id());

//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
$qs_base = $qs;
	
$formDB = getTraining($con,  $pageVars, $clsUS->userid);
//var_dump($formDB);
	
		$title = "View Training Log Item for ".$formDB['firstname']. " ".$formDB['lastname'];
		$content = "page.my_training_view.php";
        	
	


include("../../layout.php");
?>