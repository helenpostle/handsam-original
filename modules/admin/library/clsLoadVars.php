<?php
/**
 * Class to load variables from GET, POST, user session depending on security
 */
class loadVars {

	var $userid;
	var $trainee_id;	
	var $accountid;
	var $training_id;
	var $course_id;
	var $course_id_arr = array();
  var $course_date_arr = array();
	var $provider_id;
	var $fileid;
	var $hacking = false;
	
	
	function loadVars($clsUS,$con) {
		if (isAllowed("editAccounts") || isAllowed("superAdminTraining")) {
			$this->userid = getIntFromRequest("userid");
      $accountid = getIntFromRequest("accountid");
      if ($accountid > 0)
      {
        $this->accountid = getIntFromRequest("accountid");
      }
      else
      {
        $this->accountid = $clsUS->accountid;
      }
			
		} else {
			$this->userid = $clsUS->userid;
			$this->accountid = $clsUS->accountid;
		} 
		$this->training_id = $this->checkTrainingId($clsUS,$con);
		$this->trainee_id = $this->checkTrainineeId($clsUS,$con);
		$this->course_id = $this->checkCourseId($clsUS,$con);
		$this->provider_id = $this->checkProviderId($clsUS,$con);
		$this->fileid = $this->checkFileId($clsUS,$con);
	}
	
	function checkTrainingId($clsUS,$con) {
		$id = getIntFromRequest("training_id");
		if ($id === 0) return 0;
		if (isAllowed("adminTraining")  || isAllowed("superAdminTraining")) {
			return $id;
		} else if ($id > 0) {
			$qry = "select accountid, created_by, trainee_id from training_log where training_id = $id";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordTraining") && $row['accountid'] == $clsUS->accountid){
				return $id;
			} else if (isAllowed("viewTraining") && $row['accountid'] == $clsUS->accountid && $row['trainee_id'] == $clsUS->userid){
				return $id;
			}
		}
		$this->hacking = true;
		return null;
	}
	
	function checkTrainineeId($clsUS,$con) {
		$id = getIntFromRequest("trainee_id");
		if ($id === 0) return 0;
		if ($id === "") return "";
		if ($id === null) return null;
		if (isAllowed("adminTraining")  || isAllowed("superAdminTraining")) {
			return $id;
		} else if ($id > 0) {
			$qry = "select accountid from usr where userid = $id";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordTraining") && $row['accountid'] == $clsUS->accountid){
				return $id;
			} else if (isAllowed("viewTraining") && $row['accountid'] == $clsUS->accountid && $row['userid'] == $clsUS->userid){
				return $id;
			}
		}
		$this->hacking = true;
		return null;
	}
	

	function checkCourseId($clsUS,$con) {
		$id = getIntFromRequest("course_id");
		if ($id === 0) return 0;
		if ($id === "") return "";
		if ($id === null) return null;
		if (isAllowed("adminTraining") || isAllowed("superAdminTraining")) {
			return $id;
		} else if ($id > 0) {
			$qry = "select accountid from training_course where course_id = $id";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordTraining") && ($row['accountid'] == $clsUS->accountid || $row['accountid'] == 0 || $row['accountid'] == false)){
				return $id;
			}
		}
		$this->hacking = true;
		return null;
	}
	
  function checkCourseIdArr($clsUS,$con, $num) {
    $ids = array();
    if (isset($_POST['course_id-0']))
    {
      $arr = $_POST;
    }
    else if(isset($_GET['course_id-0']))
    {
      $arr = $_GET;
    }
    else
    {
      for ($n=0; $n<$num; $n++)
      {
        $this->course_id_arr[$n]=0;
      }
      return true;
    }
    
    for ($n=0; $n<$num; $n++)
    {
      $this->course_id_arr[$n]=0;
      if ($arr['course_id-'.$n] === 0) $this->course_id_arr[$n] = 0;
      if ($arr['course_id-'.$n] === "") $this->course_id_arr[$n] =  "";
      if ($arr['course_id-'.$n] === null) $this->course_id_arr[$n] =  null;
      if (!in_array($arr['course_id-'.$n], $ids))
      {
        if(isset($arr['course_date_id-'.$n]))
        {
          if ($arr['course_date_id-'.$n] == '') 
          {
            $this->course_date_arr[$n] = '';
          }
          else
          {
            $this->course_date_arr[$n] = $arr['course_date_id-'.$n];
          }
        }
        else
        {
          $this->course_date_arr[$n] = '';
        }
        $qry = "select accountid from training_course where course_id = ".$arr['course_id-'.$n];
        $rs = getRS($con,$qry);
        $row = getRow($rs);
        if (isAllowed("adminTraining") || (isAllowed("superAdminTraining") && $row['accountid'] != $this->accountid)) {
          $this->course_id_arr[$n] = $arr['course_id-'.$n];
        } else if ($arr['course_id-'.$n] > 0) {
          if ((isAllowed("superAdminTraining") && $row['accountid'] == $this->accountid) || isAllowed("recordTraining") && ($row['accountid'] == $clsUS->accountid || $row['accountid'] == 0 || $row['accountid'] == false)){
            $this->course_id_arr[$n] = $arr['course_id-'.$n];
          }
          else
          {
            $this->hacking = true;
          }
        }
        $ids[] = $arr['course_id-'.$n];
      }
    }
		return null;
	}
	

	function checkProviderId($clsUS,$con) {
		$id = getIntFromRequest("provider_id");
		if ($id === 0) return 0;
		if ($id === "") return "";
		if ($id === null) return null;
		if (isAllowed("adminTraining") || isAllowed("superAdminTraining")) {
			return $id;
		} else if ($id > 0) {
			$qry = "select accountid from training_provider where provider_id = $id";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordTraining") && ($row['accountid'] == $clsUS->accountid || $row['accountid'] == 0)){
				return $id;
			}
		}
		$this->hacking = true;
		return null;
	}

	
	function checkFileId($clsUS,$con) {
		$id = getIntFromRequest("fileid");
		if ($id === 0) return 0;
		if ($id === "") return "";
		if ($id === null) return null;
		if (isAllowed("adminTraining") || isAllowed("superAdminTraining")) {
			return $id;
		} else if ($id > 0) {
			$qry = "select accountid from files where fileid = $id";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordTraining") && $row['accountid'] == $clsUS->accountid){
				return $id;
			}
		}
		$this->hacking = true;
		return null;
	}
		

	
}
	
?>