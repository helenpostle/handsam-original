<?php
/**
 * Functions for the Training module
 *
 */
 
/**
 * Gets a training course
 */
function getTraining($con, $pageVars, $userid = 0)
{	
	$qry = "SELECT u.firstname, u.lastname, u.username as email, u.userid, u.job_title, u.job_desc, u.accountid as user_accountid, 
			tc.course_id, tc.title, tc.qual_title, tc.accountid, tc.provider_id as pid, tc.qual_desc,tc.online, tc.course_url,tc.module, tc.renewal, tc.renewal_reminder, tc.course_type, tc.related_id,
			tc2.title as related,
			tp.provider_id, tp.provider_name, tp.provider_address1, tp.provider_address2, tp.provider_address3, tp.provider_city, tp.provider_postcode, tp.provider_tel1, tp.provider_ref, tp.provider_email, tp.provider_web, tp.provider_comments,
			tl.trainee_id, tl.course_date, tl.renewal_date, tl.renewal_reminder_date, tl.cost, tl.comments, tl.state, tl.score, tl.pass, tl.max_score, tl.certificate ";
	$qry .= "FROM training_log tl LEFT JOIN usr u ON tl.trainee_id = u.userid ";
	$qry .= "LEFT JOIN training_course tc ON tl.course_id = tc.course_id ";
	$qry .= "LEFT JOIN training_course tc2 ON tc.related_id = tc2.course_id ";
	$qry .= "LEFT JOIN training_provider tp ON tl.provider_id = tp.provider_id ";
	$qry .= "WHERE tl.training_id = ".$pageVars->training_id." AND tl.accountid = ".$pageVars->accountid;
	//return $qry;
	$pageDb = new pageQry($con, $qry);
	$pageDb->rsItem();
	return $pageDb->row;
}


function frmDateAddField($rs, $field, $array, $title, $req) {
	$req_span = "";
	if ($req == true) {
		$class = "required";
		$req_span = "<span>*required</span>";
	} else {
		$class = "optional";
	}
	if (isset($_POST[$field."_part"])) {
		$val = array();
		$val[0] = $_POST[$field."_num"];
		$val[1] = $_POST[$field."_part"];
	} else if(isset($_GET[$field."_part"])) {
		$val[0] = $_GET[$field."_num"];
		$val[1] = $_GET[$field."_part"];		
	} else {
		//always strip slashes from database
		$val = explode(" ",stripslashes($rs[$field]));
	}
	$str = "	<div class='frm_row dateAdd clearfix ".$class."'>";
	
	$str .= "<label for=\"".$field."_num\">$title $req_span</label>";

	$str .= "<input type=\"text\" class=\"inputDateAdd\" name=\"".$field."_num\" id=\"".$field."_num\" value=\"".$val[0]."\" maxlength=\"4\"/>"; 
	
	$str .= "<select name=\"".$field."_part\"  id=\"".$field."_part\" class=\"$class ".frmClass($req,$field)."\">\n";
	$str .= "<option value=\"\">Pick date part</option>";
	foreach ($array as $key => $text) {
		$str .= "<option value=\"".$key."\"";
		if (isset($val[1]) && $key == $val[1]) {
			$str .= " selected";
		}
		$str .= ">".$text."</option>";
	  }

	$str .= "</select>";
	
	$str .= "</div>\n";
	return displayText($str);
	
}	

function processDateAdd ($field, $title, $req, $qry) {
	//php 4 needs this to be global
	global $qry;
	//$cnt = count($messages);
	$valid = true;
	if (isset($_POST[$field.'_num'])) {
		if($_POST[$field.'_num'] == "" && !$req) {
			return true;
		} else if (is_numeric($_POST[$field.'_num']) && $_POST[$field.'_num'] > 0) {
			$num = $_POST[$field.'_num'];
		} else {
			$valid = false;
			$qry->failError .= "Please enter a number for $title.\r\n";
		}
		
	} else if($req){
		$valid = false;
		if ($req) $qry->failError .= "Please enter a number for $title.\r\n";
	}
	
	if (isset($_POST[$field.'_part']) && $_POST[$field.'_part'] != "") {
		$part = $_POST[$field.'_part'];
	} else if($req){
		$valid = false;
		if ($req) $qry->failError .= "Please select days, weeks, months or years for $title.\r\n";
	}
	if ($valid) {
		$prepval = "'".escapeString(trim($num." ".$part))."'";
		$qry->addValue($field,$prepval);
	}
	return true;
		

}

//function to save a draft form with few required fields
function saveDraftForm($con, $pageVars, $clsUS, $state) {
	if ($state == "" || $state == "IN PLANNING") {
		//GLOBAL $pageVars;
		if ($pageVars->training_id == 0) {
			$qry = new dbInsert("training_log");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
		} else {
			$qry = new dbUpdate("training_log");
			$qry->setParam("training_id", $pageVars->training_id);
		}
				
		if ($pageVars->trainee_id === 0) $pageVars->trainee_id = "";
		$qry->setReqNumberVal("trainee_id",$pageVars->trainee_id,"Trainee");
		
		if ($pageVars->course_id === 0) {
			$qry->setNullVal("course_id");
		} else {
			$qry->setNumberVal("course_id",$pageVars->course_id,"Training Course");
		}
		if ($pageVars->provider_id === 0) {
			$qry->setNullVal("provider_id");
		} else {
			if ($_POST['online'] == 1) $pageVars->provider_id = 0;
			$qry->setNumberVal("provider_id",$pageVars->provider_id,"Course Provider");
		}
		
		$course_date = $_POST['course_date'];

		$qry->setDateVal("course_date",$course_date,"Course Date");
		$qry->setDateVal("renewal_date",$_POST['renewal_date'],"Renewal Date");		
		$qry->setDateVal("renewal_reminder_date",$_POST['renewal_reminder_date'],"Renewal Reminder Date");
		
		$qry->setStringVal("comments",$_POST["comments"],"Comments");
		if (trim($_POST['cost']) == "") {
			$qry->setNullVal("cost");
		} else {
			$qry->setNumberVal("cost",$_POST["cost"],"Cost");
		}
		
		$qry->setReqStringVal("state","IN PLANNING","State");
		$qry->setAudit($clsUS->userid);
		
		//echo  $qry->getSql();
		//die();
		if ($qry->execute($con)) {
			if ($pageVars->training_id == 0) {
				$pageVars->training_id = $qry->getNewID();	
			}
			return "";
		} else {
			return $qry->getError();
		}
	} else {
		return;
	}
}	


/**
 * Save a draft form with few required fields
 */
function saveCompleteForm($con, $pageVars, $clsUS, $state) {
	if ($state != "IN PLANNING") $state = 'IN PROGRESS';
	
		
	//GLOBAL $pageVars;
	if ($pageVars->training_id == 0) {
		$qry = new dbInsert("training_log");
		$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
	} else {
		$qry = new dbUpdate("training_log");
		$qry->setParam("training_id", $pageVars->training_id);
	}
	
	if ($pageVars->trainee_id === 0) $pageVars->trainee_id = "";
	$qry->setReqNumberVal("trainee_id",$pageVars->trainee_id,"Trainee");

	if ($pageVars->course_id === 0) $pageVars->course_id = "";
	$qry->setReqNumberVal("course_id",$pageVars->course_id,"Training Course");
	
	if ($pageVars->provider_id === 0) $pageVars->provider_id = "";
	if (isset($_POST['online']) && $_POST['online'] == 1) $pageVars->provider_id = 0;
	$qry->setReqNumberVal("provider_id",$pageVars->provider_id,"Course Provider");			
	
	$course_date = $_POST['course_date'];

	$qry->setReqDateVal("course_date",$course_date,"Course Date");
	$qry->setReqDateVal("renewal_date",$_POST['renewal_date'],"Renewal Date");
	$qry->setReqDateVal("renewal_reminder_date",$_POST['renewal_reminder_date'],"Renewal Reminder Date");
	
	$qry->setStringVal("comments",$_POST["comments"],"Comments");

	$qry->setNumberVal("cost",$_POST["cost"],"Cost");
	
	$qry->setReqStringVal("state",$state,"State");
	$qry->setAudit($clsUS->userid);
	if ($qry->execute($con)) {
		if ($pageVars->training_id == 0) {
			$pageVars->training_id = $qry->getNewID();	
		}
		return "";
	} else {
		return $qry->getError();
	} 
	
	
	return false;		
}


/**
 * Create a new instance of the course, with the start date set to coincide with the old course's renewal date.
 *
 * Implements saveCompleteForm()
 *
 * @var object DB connection
 * @var object pageVars
 * @var object clsUS
 * @var object training_log record
 * @return 
 */
function createRefresherCourse($con, $pageVars, $clsUS, $formDB)
{		
	// modify $pageVars
	$pageVars->training_id = 0;
	$pageVars->trainee_id = $formDB['trainee_id'];
	$pageVars->provider_id = $formDB['provider_id'];
	$pageVars->course_id = $formDB['course_id'];
	
	// set POST values artificially
	$_POST['training_id'] = 0; // override just in case
	//$_POST['course_date'] = date('D, d M Y', strtotime($formDB['renewal_date'], strtotime($formDB['course_date'])) ); // old_course_date + old_renewal_date; Sat, 05 Jan 2012
	$_POST['course_date'] = date('D, d M Y', strtotime($formDB['renewal_date']) ); // old_course_date + old_renewal_date; Sat, 05 Jan 2012

	$_POST['renewal_date'] = date('D, d M Y', strtotime($formDB['renewal'], strtotime($_POST['course_date'])) ); 
	
	$_POST['renewal_reminder_date'] = date('D, d M Y', strtotime('-'.$formDB['renewal_reminder'], strtotime($_POST['renewal_date'])) ); // new_renewal_date - old_renewal_reminder_date
	$_POST["comments"] = $formDB['comments'];
	$_POST["cost"] = $formDB['cost'];

	return saveCompleteForm($con, $pageVars, $clsUS, "IN PLANNING");
}


function changeTrainingState($con, $pageVars, $clsUS, $state) {
	if ($pageVars->training_id > 0)
	{
		$qry = new dbUpdate("training_log");
		$qry->setParam("training_id", $pageVars->training_id);
		$qry->setReqStringVal("state", $state, "State");
		$qry->setAudit($clsUS->userid);
		
		if ($qry->execute($con)) {
			return "";
		} else {
			return $qry->getError();
		}
	}
	
}	


/**
 * A date special  field which uses the jquery ui datebicker
 */
function frmTrainingCourseDateField($rs,$field,$title,$req) {
	global $rootUrl; 
	if ($rs[$field] == "") {
		$date = time();
	} else {
		$date = strtotime($rs[$field]);
	}
	$dcheck =  strftime("%a, %d %b %Y",$date);
	$ret = "<input name=\"$field\" id=\"ctl.$field\" readonly=\"true\" class=\"date\" value=\"$dcheck\"/>	";
	$ret .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.$field\"/>";

	return frmRow($ret,$title,$field,$req);	
}

//get uploads for a form or a whole visit.
function getUploads($con, $pageVars) {
	$sql = "SELECT af.fileid, f.filename,  f.title, '' AS url, f.filecategory as category, 'file' AS resource FROM training_files af LEFT JOIN files f ON f.fileid = af.fileid WHERE f.accountid = {$pageVars->accountid} and filetype = 'accountTrainingDoc' and training_id = {$pageVars->training_id} order by title asc";					
	//echo $sql;
	$result = new  pageQry($con, $sql);
	$result->rsList();
	return $result;
}


function displayTrainingEditDateField($row, $field, $title) {
	$str = "";
	$course_field = str_replace("_date", "", $field);
	
	
	
	if ($row[$field] == "") {
		$val = $row[$course_field];
	} else {
		$val = $row[$field];
	}
	
	
	$str .= "<input type=\"hidden\" name=\"$field\" value=\"$val\"/>";
	
	$str .= "<div class=\"view_date clearfix\">";
	$str .= "<span class=\"label\">$title </span>";
	$str .= "<span class=\"value\" id=\"".$field."_a\"><span>$val</span></span>";
	//now calculate dates:
	if ($val == "") {
		$new_date = "";
	} else 	if ($field == "renewal_date") {
		$new_date = strtotime($row['course_date']." + $val");
	} else if ($field == "renewal_reminder_date"){
		$new_date = strtotime($row['course_date']." + ".$row['renewal_date']." -".$row['renewal_reminder_date']);
	}
	if ($new_date != "") $new_date = strftime("%a, %d %b %Y",$new_date);
	$str .= "<span class=\"value\" id=\"".$field."_b\"><span>$new_date</span></span>";
	$str .= "<input type=\"hidden\" name=\"".$field."_display\" value=\"$new_date\"/>";
	$str .= "<input type=\"hidden\" name=\"$field\" value=\"$val\"/>";
	$str .= "</div>";
	
	return $str;
}
	
function displayTrainingViewDateField($row, $field, $title) {
	$str = "";
	$course_field = str_replace("_date", "", $field);
	
	
	
	if ($row[$field] == "") {
		$val = $row[$course_field];
	} else {
		$val = $row[$field];
	}
	
	
	//$str .= "<input type=\"hidden\" name=\"$field\" value=\"$val\"/>";
	
	$str .= "<div class=\"frm_row clearfix\">";
	$str .= "<span class=\"label\">$title </span>";
	//$str .= "<span class=\"value\">$val</span>";
	//now calculate dates:
	if ($val == "") {
		$new_date = "";
	} else 	if ($field == "renewal_date") {
		$new_date = strtotime($row['course_date']." + $val");
	} else if ($field == "renewal_reminder_date"){
		$new_date = strtotime($row['course_date']." + ".$row['renewal_date']." -".$row['renewal_reminder_date']);
	}
	if ($new_date != "") $new_date = strftime("%a, %d %b %Y",$new_date);
	$str .= "<span class=\"value\"><span>$new_date</span></span>";
	$str .= "</div>";
	
	return $str;
}	
