<?php

/**
 * list_system_courses.php
 *
 * List all the system wide courses.
 *
 */
 error_reporting(E_ALL); 
 ini_set("display_errors", 1);


$secure = true;

require("../../shared/startPage.php");

// get course_id from GET
$course_id = getIntFromRequest('course_id');
$action = getStrFromRequest('action', 'edit');

$pageFiles->addModFunc("training_edit");
$pageFiles->addModFunc("training_list");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS, $con);

if(!isAllowed("superAdminTraining"))
{
	trigger_error("Access Denied", E_USER_WARNING);
	headerLocation("$rootUrl/index.php", false);
}
else
{
	if(($course_id === null && $action == 'edit') || buttonClicked("cancel"))
	{
		// show a list of system wide training courses
		// list training courses initially order by renewal/expire date
		$title = "System Wide Training Courses";
		$content = "page.syscourse_list.php";
	}
	else
	{
		// show the create/edit form
		$title = ucfirst($action)." a System Wide Training Course";
		$content = "page.syscourse_edit.php";
		
		// CRUD to DB
		if(buttonClicked("save"))
		{
			// insert or save
			if($course_id === 0)
			{
				$db = new dbInsert('training_course');
				// online checkbox. only 1 for synched courses from 22blue
				$db->setReqNumberVal('online', 0, 'online');
				$db->setReqNumberVal('accountid', 0, 'accountid');
				$db->setNullVal('course_url');
			}
			else
			{
				$db = new dbUpdate('training_course');
				$db->setParam('course_id', $course_id);
			}

			// required values
			$db->setReqStringVal('title', $_POST['title'], 'Course Title');
			$db->setReqStringVal('course_type', $_POST['course_type'], 'Course Type');
			
			//renewal = $_POST['renewal_num'] + $_POST['renewal_part']
			//$_POST['renewal_num'] must be a number
			
			if (is_numeric($_POST['renewal_num']) && array_key_exists($_POST['renewal_part'], $trn_date_arr)) {
				$db->setReqStringVal('renewal', $_POST['renewal_num'].' '.$_POST['renewal_part'], 'Renewal Date');
			} else {
				$db->failError .= "Please enter the renewal date\n";
			}
			if (is_numeric($_POST['renewal_reminder_num']) && array_key_exists($_POST['renewal_reminder_part'], $trn_date_arr)) {
				$db->setReqStringVal('renewal_reminder', $_POST['renewal_reminder_num'].' '.$_POST['renewal_reminder_part'], 'Renewal Reminder Date');
			} else {
				$db->failError .= "Please enter the renewal reminder date\n";
			}
			
			
			
			
			// optional values
			$db->setStringVal('qual_title', $_POST['qual_title']);
			$db->setStringVal('qual_desc', $_POST['qual_desc']);
			
			
			/**
			 * course_url
			 * Check if it's a valid url (if anything's been sent)
			 * 
			 * If online is ticked, it then decides if it's required or not
			 */
			 
			 //this is disabled for now, as only 22blue can add online courses that are then synched 
			 
			 /*
			if($_POST['online'] == '1')
			{
				$db->setReqStringVal('course_url', $training_conf['ext_app_courses_url'], 'Course URL');
			} else {
				$db->setNullVal('course_url');
			}
			*/
			
			
			// createdon etc
			$db->setAudit($clsUS->userid);
			
			if ($db->execute($con))
			{
				$messages[] = SAVED;
				// job role is saved
				
				if($course_id === 0)$course_id = $db->getNewID();
			
				// as the above is OK, write to the JOIN table.
				// role_title
				$role_id = (isset($_POST['role_id'])) ? $_POST['role_id'] : false;
				if(is_array($role_id) && count($role_id) > 0)
				{
					// save to the `trainingcourse_jobrole` table.
					// check for existence
					$indb = array();
					if($course_id !== NULL)
					{
						$sql = "SELECT id, core_jobrole_id 
								FROM trainingcourse_jobrole
								WHERE training_course_id = '$course_id';";
						$rs  = getRS($con, $sql);
						while($row = getRow($rs))
							$indb[$row['id']] = $row['core_jobrole_id'];
					
						// delete the following
						$delete = array_diff($indb, $role_id);
						if(is_array($delete) && count($delete) > 0)
						{
							$sql = "DELETE FROM trainingcourse_jobrole WHERE id IN (".implode(',' , array_keys($delete)).")";
							execSQL($con, $sql);
						}
					}
					
					// insert these ones
					$new = array_diff($role_id, $indb);
					foreach($new as $job_id)
					{
						$sql = "INSERT INTO trainingcourse_jobrole (training_course_id, core_jobrole_id) VALUES ($course_id, $job_id);";
						execSQL($con, $sql);
					}
				}  else {
					//no job roles so delete all for this course
					$sql = "DELETE FROM trainingcourse_jobrole WHERE training_course_id = $course_id";
					execSQL($con, $sql);

				}
				
				// save the files
				$fileIds 			= (isset($_POST['docAddId'])) ? $_POST['docAddId'] : array();
				$docDelIds 			= (isset($_POST['docDelId'])) ? $_POST['docDelId'] : array();
				$linkAddFilenames	= (isset($_POST['linkAddFilename'])) ? $_POST['linkAddFilename'] : array();
				
				foreach($fileIds as $fileId)
				{
					#insert into database 
					$qryTaskFiles = new dbInsert("systemcourse_files");
					$qryTaskFiles->setReqNumberVal("fileid", $fileId, "File Id");
					$qryTaskFiles->setReqNumberVal("training_id", $course_id, "Course ID");
					$qryTaskFiles->setAudit($clsUS->userid);

					if ($qryTaskFiles->execute($con)) {
						$messages[] = FILES_ATTACHED;
					} else {
						$messages[] = "Attaching file: ".$fileId." ".$qry->getError();
					}
				}
				#now look at delDocIds and unattach these docs from this task
				if (count($docDelIds) > 0)
				{
					foreach ($docDelIds as $deleteSCFileId)
					{
						$qry = new dbDelete("systemcourse_files");
						$qry->setParam("training_id", $course_id);
						$qry->setParam("systemcourse_fileid", $deleteSCFileId);
						
						if ($qry->execute($con)) {
							$messages[] = FILES_UNATTACHED;
						} else {
							$messages[] = "Removing systemcourse_file id: ".$deleteSCFileId." ".$qry->getError();
						}
					}
				}
				
				// save the links
				$linkIds		= (isset($_POST['linkAddId'])) ? $_POST['linkAddId'] : array();
				$linkDelId		= (isset($_POST['linkDelId'])) ? $_POST['linkDelId'] : array();
				$linkAddTitle	= (isset($_POST['linkAddFilename'])) ? $_POST['linkAddFilename'] : array();
				
				foreach($linkIds as $linkId)
				{
					#insert into database 
					$qryTaskFiles = new dbInsert("systemcourse_links");
					$qryTaskFiles->setReqNumberVal("linkid", $linkId, "Link Id");
					$qryTaskFiles->setReqNumberVal("training_id", $course_id, "Course ID");
					$qryTaskFiles->setAudit($clsUS->userid);

					if ($qryTaskFiles->execute($con)) {
						$messages[] = LINK_ATTACHED;
					} else {
						$messages[] = "Attaching file: ".$fileId." ".$qry->getError();
					}
				}
				
				// check if any ids have been sent for deletion ($linkDelId)
				if (count($linkDelId) > 0)
				{
					foreach ($linkDelId as $deleteSCLinkId)
					{
						$qry = new dbDelete("systemcourse_links");
						$qry->setParam("training_id", $course_id);
						$qry->setParam("systemcourse_linkid", $deleteSCLinkId);
						
						if ($qry->execute($con)) {
							$messages[] = LINK_UNATTACHED;
						} else {
							$messages[] = "Removing systemcourse_link id: ".$deleteSCFileId." ".$qry->getError();
						}
					}
				}

				// all done, just show the list.
				$title = "System Wide Training Courses";
				$content = "page.syscourse_list.php";			
			}
			else
			{
				$messages[] = $db->getError();	
			}
		}
		elseif(buttonClicked("delete"))
		{
			// destroy him...
      $db = new dbUpdate('training_course');
      $db->setParam('course_id', $course_id);
      $db->setAudit($clsUS->userid);
      $db->setReqStringVal('state', 'DELETED', 'state');
      if ($db->execute($con))
			{
        $messages[] = 'System Course Deleted';
        //now update training log items to deleted to
        $qry = new dbUpdate("training_log");
        $qry->setParam("course_id", $course_id);
        $qry->setStringParamNotEqual("state", 'COMPLETE');
        $qry->setReqStringVal("state", 'DELETED', "State");
        $qry->setAudit($clsUS->userid);
        if ($qry->execute($con)) {
          $messages[] = 'Uncompleted Training log items Deleted';
          $title = "System Wide Training Courses";
          $content = "page.syscourse_list.php";
        } else {
          $messages[] = $qry->getError();
        }
      }
      else
			{
				$messages[] = $db->getError();	
			}
		}
		$formDB = getSystemCourse($course_id, $con);
		$roleDB = getSystemCourseJobroles($course_id, $con);
		$fileDB = getSystemCourseFiles($course_id, $con);
		$linkDB = getSystemCourseLinks($course_id, $con);
	}
}


include("../../layout.php");