<?php 
/**
 * link to e-training course
 * 
 */
if (isset($formDB) && $formDB['course_url'] != "" && $formDB['trainee_id'] == $clsUS->userid && $formDB['online'] == 1 && $clsUS->conf['training']['e_training_enabled'] == 1 && $formDB['pass'] != 1) {
        
    TrainingBoxTop("Handsam E-Training Centre");
       
    $elink = "<div id=\"e-training\">";
    $elink .= "<a class=\"\" href=\"$rootUrl/modules/training/online_course.php?training_id={$pageVars->training_id}\">Click here to proceed to the online course: <strong>{$formDB['title']}</strong></a>";
    $elink .= "</div>";

    echo $elink;       
    trainingBoxBottom();        
        
}
?>
	