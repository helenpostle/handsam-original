<?php

/**
* Plan editing functionality for super admin
*
*/

/**
 * Include basic stuff
 */
//make this a secure page
$secure = false;
 
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_admin');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

if (isAllowed("editMasterPlan")) {

	if ($pageVars->planid !== null) {
		//edit /create plan
		$title = "LOtC: Edit Plan";
		$content = "page.plan_edit.php";
		$qry = "select * from m_plan where planid = {$pageVars->planid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
	
		if (buttonClicked("save")) {
			#check that a plan doesn't exist with this name
			$rsCheck = getRS($con,"select * from m_plan where planname = '".mysql_real_escape_string($_POST['planname'])."' and planid != {$pageVars->planid}");
			if (getRow($rsCheck)) {
				$messages[] = PLAN_IN_USE;	
			} else {
							
				if ($pageVars->planid == 0) {
					$qry = new dbInsert("m_plan");
				} else {
					$qry = new dbUpdate("m_plan");
					$qry->setParam("planid",$pageVars->planid);
				}
			
				if (isset($qry)) {
					$qry->setReqStringVal("planname",$_POST["planname"],"plan name");
					$qry->setStringVal("plantext",$_POST["plantext"],"plan text");
					$qry->setReqStringVal("state",$_POST["state"],"state");
					
					$qry->setAudit($clsUS->userid);
					
					if ($qry->execute($con)) {
						if ($pageVars->planid == 0) {
							$pageVars->planid = $qry->getNewID();	
						}
						$messages[] = SAVED;
						//list plans
						$title = "LOtC: Plan List";
						$content = "page.plan_list.php";
						$box[] = "box.guidance.php";
						$pageDb = getPlanList($pageVars, $con);
						$pageFiles->addModFunc("lotc_tables");
						
					} else {
						$messages[] = $qry->getError();	
					}
				}
			}
		} else if (buttonClicked("delete")) {
			
				$qry = new dbUpdate("m_plan");
				$qry->setParam("planid",$pageVars->planid);
				$qry->setReqStringVal("state","DELETED","State");
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#change content to list
					$messages[] = DELETED;
					//list plans
					$title = "LOtC: Plan List";
					$content = "page.plan_list.php";
					$box[] = "box.guidance.php";
					$pageDb = getPlanList($pageVars, $con);
					$pageFiles->addModFunc("lotc_tables");
				}
		} else if (buttonClicked("cancel")) {
			//list plans
			$title = "LOtC: Plan List";
			$content = "page.plan_list.php";
			$box[] = "box.guidance.php";
			$pageDb = getPlanList($pageVars, $con);
			$pageFiles->addModFunc("lotc_tables");
		}
	
				
	} else {
		//list plans
		$title = "LOtC: Plan List";
		$content = "page.plan_list.php";
		$box[] = "box.guidance.php";
		$pageDb = getPlanList($pageVars, $con);
		$pageFiles->addModFunc("lotc_tables");
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>