<?php
if (isAllowed("planVisits") && !isAllowed("editAccount")) {
	if ($title != "") $title .= " &amp; ";
	$pageFiles->addCss("$rootUrl/modules/lotc/css/lotc_1.1.css");
	$pageFiles->addCss("$rootUrl/modules/lotc/css/customise.css");
	$title .= "Current LOtC plans";
	include("$rootPath/modules/lotc/library/clsPageQry.php");
	include("$rootPath/modules/lotc/library/funcLotcVisits.php");
	include("$rootPath/modules/lotc/library/clsLoadVars.php");
	$pageVars = new loadVars($clsUS,$con);
	$qs = "";
	$filter_qry = array();
	$filter_qry['state'] = " a_visit.state = 'PLANNING' ";
	$lotc_highlight = "PLANNING";
	$pageDb = getVisitList($pageVars, $con, $filter_qry);
} else if (isAllowed("editVisits") && !isAllowed("editAccount")) {
	if ($title != "") $title .= " &amp; ";
	$pageFiles->addCss("$rootUrl/modules/lotc/css/lotc_1.1.css");
	$pageFiles->addCss("$rootUrl/modules/lotc/css/customise.css");
	$title .= "LOtC plans for approval";
	include("$rootPath/modules/lotc/library/clsPageQry.php");
	include("$rootPath/modules/lotc/library/funcLotcVisits.php");
	include("$rootPath/modules/lotc/library/clsLoadVars.php");
	$pageVars = new loadVars($clsUS,$con);
	$qs = "";
	$filter_qry = array();
	$filter_qry['state'] = " a_visit.state = 'APPROVAL' ";
	$pageDb = getVisitList($pageVars, $con, $filter_qry);
	$lotc_highlight = "APPROVAL";
	
}	

ob_start();


if (isAllowed("planVisits") && isset($pageDb)) {
	echo "<h2>LOtC visits requiring planning.</h2>";
	include("$rootPath/modules/lotc/content/page.visit_list.php");	
} else if (isAllowed("editVisits") && isset($pageDb)) {
	echo "<h2>LOtC visits requiring approval.</h2>";
	include("$rootPath/modules/lotc/content/page.evc_visit_list.php");	
}

$index_modules_content[] = ob_get_contents();
ob_end_clean();

?>