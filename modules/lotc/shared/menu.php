<?php
$lotc_order = "";
if (isAllowed("editMasterPlan")) {
	$lotc_order = $clsMenu->addMenuItem("/modules/lotc/plan.php", "LOTC ADMIN", "", $lotc_order);
} else if (isAllowed("planVisits") || isAllowed("editVisits")) { 
	$lotc_order = $clsMenu->addMenuItem("/modules/lotc/index.php", "LOTC", "", $lotc_order);
} else if (isAllowed("financeApproval")) { 
	$lotc_order = $clsMenu->addMenuItem("/modules/lotc/finance.php", "LOTC", "", $lotc_order);
} else if (isAllowed("principalApproval")) { 
	$lotc_order = $clsMenu->addMenuItem("/modules/lotc/principal.php", "LOTC", "", $lotc_order);
} else if (isAllowed("deputyPrincipalApproval")) { 
	$lotc_order = $clsMenu->addMenuItem("/modules/lotc/deputy_principal.php", "LOTC", "", $lotc_order);
} else if (isAllowed("governorApproval")) { 
	$lotc_order = $clsMenu->addMenuItem("/modules/lotc/governor.php", "LOTC", "", $lotc_order);
} /*else if (isAllowed("governorView")) { 
	$lotc_order = $clsMenu->addMenuItem("/modules/lotc/view.php", "LOTC", "", $lotc_order);
}*/
//lotc app menu items
if (isAllowed("planVisits")) { 
	$clsMenu->addMenuItem("/modules/lotc/index.php", "MY LOTC VISITS", "", $lotc_order);
}

if (isAllowed("editVisits") && !isAllowed("editMasterPlan")) { 
	$clsMenu->addMenuItem("/modules/lotc/a_plan.php", "LOTC ADMIN", "", $lotc_order);
}

if (isAllowed("editVisits") && !isAllowed("editMasterPlan")) { 
	$clsMenu->addMenuItem("/modules/lotc/evc.php", "LOTC EVC", "", $lotc_order);
}

if (isAllowed("editMasterPlan")) { 
	$clsMenu->addMenuItem("/modules/lotc/plan.php", "LOTC ADMIN", "", $lotc_order);
}

if (isAllowed("financeApproval")) { 
	$clsMenu->addMenuItem("/modules/lotc/finance.php", "LOTC FINANCE", "", $lotc_order);
}

if (isAllowed("principalApproval")) { 
	$clsMenu->addMenuItem("/modules/lotc/principal.php", "LOTC PRINCIPAL", "", $lotc_order);
}

if (isAllowed("deputyPrincipalApproval")) { 
	$clsMenu->addMenuItem("/modules/lotc/deputy_principal.php", "LOTC DEPUTY PRINCIPAL", "", $lotc_order);
}

if (isAllowed("governorApproval")) { 
	$clsMenu->addMenuItem("/modules/lotc/governor.php", "LOTC GOVERNOR", "", $lotc_order);
}
/*
if (isAllowed("governorView")) { 
	$clsMenu->addMenuItem("/modules/lotc/view.php", "LOTC VIEW", "", $lotc_order);
}
*/

if (isset($subpageof)) $clsMenu->setThisPage($subpageof);
?>