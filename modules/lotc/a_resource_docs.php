<?php

/**
* Plan editing functionality for super admin
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
 
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_admin');
$pageFiles->addModFunc('lotc_file_upload');

$pageFiles->includePhp();

$subpageof = "a_plan.php";

$pageVars = new loadVars($clsUS,$con);
$to_file_path = $accountLotcDocs_fp;

if (isAllowed("editVisits")) {
	if ($pageVars->fileid !== null) {


		$title = "LOtC: Account Resources Edit Document";
		$content = "page.a_resource_docs_edit.php";
		$box[] = "box.a_guidance.php";

		//get file data and count if this file is used in handsam tasks and lotc
		$qry = "select f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by";
		$qry .= " FROM files f where f.fileid = {$pageVars->fileid} ";
				
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		
		if (buttonClicked("save")) {
			$fileid = lotcUploadFile($clsUS, $pageVars, $con, $to_file_path, "accountLotcDoc");
			if ($fileid) {
				//first delete this file from lotc_files - no chance of duplicates!
				$qry = new dbDelete("a_lotc_files");
				$qry->setParam("fileid",$fileid);
				$qry->setParam("accountid",$pageVars->accountid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				}
				//now add the fileid to lotc_files
				$qry = new dbInsert("a_lotc_files");
				$qry->setReqNumberVal("fileid",$fileid,"fileid");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
				$qry->setAudit($clsUS->userid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					headerLocation("a_resource_docs.php?rand=".rand(),$messages);
				}
			}
			
		} else if (buttonClicked("cancel")) {
			
			headerLocation("a_resource_docs.php?rand=".rand(),$messages);
			
		} else if (buttonClicked("delete")) {
			if ($pageVars->fileid != 0 && $pageDb->row['cnt'] == 0) {
				$fileid = lotcDeleteFile($pageVars, $con, $to_file_path);
				if ($fileid) {
					//first delete this file from lotc_files - no chance of duplicates!
					$qry = new dbDelete("a_lotc_files");
					$qry->setParam("fileid",$fileid);
					$qry->setParam("accountid",$pageVars->accountid);
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					} else {
						headerLocation("a_resource_docs.php?rand=".rand(),$messages);
					}
				}

			}
			
		}
		
	} else {		
			
		//list guidance attached to lotc
		$title = "LOtC: Account Resources Documents List";
		$content = "page.a_resource_docs_list.php";
		$box[] = "box.a_guidance.php";
		$pageDb = getResourceDocsList($con, $pageVars->accountid);
		$pageFiles->addModFunc("lotc_tables");

	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>