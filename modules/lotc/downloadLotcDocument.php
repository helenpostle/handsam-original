<?php
/**
* Securely Download a file from the database
*/


//MUST ADD SECURITUY AND CHECK FILE EXISTS
$content = "";
//make this a secure page
$secure = false;
//include basic stuff
include ("../../shared/startPage.php");

$download = false;
//specify page content
$title = "Download Fails";		//the <title> tag and <h1> page title
$pageFiles->addModFunc('lotc_visits');
$pageFiles->csrf_form = '';
$pageFiles->includePhp();
//get the id 
$id = getIntFromRequest("id");
if (!isset($clsUS)) {
	$token = getStrFromRequest('emailtoken');
	$visitid = getIntFromRequest('visitid');
	$userid = getIntFromRequest('userid');
	if (checkEmailVisitToken($con, $token, $userid, $visitid)) {
		$download = true;
	}
} else {
	$qry = "select * from a_lotc_files where fileid = $id and accountid = {$clsUS->accountid}";
	//echo $qry;
	//die(); 
	$rsCheckContent = getRS($con,$qry);
	if (getRow($rsCheckContent)) {
		$download = true;
	}
}

if ($download) {

	//get the record set
	$sql = "select * from files where fileid = $id";
	
	$rsFile = getRS($con, $sql);
	if ($rowFile = getRow($rsFile)) {
		//download the file
		$type = $rowFile["filetype"];
		$filename = $rowFile["filename"];
		
		if ($type == "handsamDoc") {
			$path = $handsamdocs_fp;
		} else if ($type == "adminDoc") {
			$path = $admindocs_fp;
		} else if ($type == "userDoc") {
			$path = $userdocs_fp;
		} else if ($type == "accountLotcDoc") {
			$path = $accountLotcDocs_fp;
		} else if ($type == "accountLotcUserDoc") {
			$path = $accountLotcUserDocs_fp;
		}

		
		$file = $path.$filename;
		
		// send the right headers
		
		header("Content-Type: application/octet-stream");
		//header("Content-Length: " . filesize($path));
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		
		//echo file_get_contents($file);
		//exit;
		if ($rowFile['mid'] > 0 && $type == "handsamDoc") {
			$tjs = time() + 60000;
			$file =  $core_conf['handsam_docs_url']."?id=".$rowFile['mid']."&tjs=$tjs";
			echo file_get_contents($file);
			exit;
		} else {
			if (@file_exists($file)) echo file_get_contents($file);
			exit;
		}
	}
}