<?php

/**
 * Plan editing functionality
 *
 */
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");


$pageFiles->addModFunc('lotc_process_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_notifications');
$pageFiles->addModFunc("lotc_email_history");

$pageFiles->includePhp();

$subpageof = "index.php";

$pageVars = new loadVars($clsUS, $con);
if ($pageVars->hacking)
    headerLocation("$rootUrl/index.php");
$qs = array();
if (isset($pageVars->visitid))
    $pageFiles->addModFunc("lotc_daytrip_tickbox");

if (isset($pageVars->visitid) && $pageVars->visitid > 0 && isAllowed("planVisits")) {

    //get filter array querystring

    if (isset($_POST['filter'])) {
        $qs = __unserialize($_POST['filter']);
        $qsfrm = $_POST['filter'];
    } else if (isset($_GET['filter'])) {
        $qsfrm = $_GET['filter'];
        $qs = __unserialize(stripslashes($_GET['filter']));
    }
    //set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
    $qs_base = $qs;

    //set first crumbtrail link back to the index page with the list of visits
    $crumb_back = "<a href=\"index.php?accountid={$pageVars->accountid}&amp;rand=" . rand() . "&amp;filter=" . urlencode(serialize($qs)) . "\" title=\"EVC LOtC List\">Planner LOtC list</a>";

    //set jquery scripts
    $pageFiles->addModFunc("lotc_popup");

    //$jquery_plugin[] = "jquery.popUp.js";
    //$jquery[] = "lotc.popup.js";

    $box[] = "box.form_state.php";
    $box[] = "box.account_resource_list.php";
    $box[] = "box.handsam_resource_list.php";
    $box[] = "box.handsam_risk_assessment_list.php";


    //first get visit table data
    //query for specific stage of visit
    if ($pageVars->stage > 0) {
        //$qry = "SELECT  concat(usr.firstname,' ',usr.lastname) as leader, b.formname, b.formtext, b.formid, b.stage AS stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) as stage_cnt  from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid RIGHT JOIN a_form b ON b.planid = a_visit.planid left join usr on a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid} and (a_form.state = 'ACTIVE' or a_form.state = 'DELETED') and (a_plan.state = 'ACTIVE' or a_plan.state = 'DELETED') and CASE WHEN {$pageVars->stage} < a_visit.stage THEN  b.stage = {$pageVars->stage} ELSE b.stage = a_visit.stage END GROUP by usr.firstname, usr.lastname,b.formname, b.formtext, b.formid, b.stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, a_form.state  ";
        $qry = "SELECT  concat(usr.firstname,' ',usr.lastname) as leader, b.formname, b.formtext, b.formid, b.stage AS stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, a_visit.visit_end, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) as stage_cnt, MIN(a_form.stage) AS min_stage  from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid LEFT JOIN a_form b ON b.planid = a_visit.planid left join usr on a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid}  and CASE WHEN {$pageVars->stage} < a_visit.stage THEN  b.stage = {$pageVars->stage} ELSE b.stage = a_visit.stage END GROUP by usr.firstname, usr.lastname,b.formname, b.formtext, b.formid, b.stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, a_visit.visit_end, a_form.state  ";
    } else {
        //query to get current form stage of visit
        $qry = "SELECT  v.state, concat(usr.firstname,' ',usr.lastname) as leader, v.*, f.formname, f.formtext, f.formid, f2.stage_cnt, f2.min_stage FROM a_visit v LEFT JOIN a_form f ON v.planid = f.planid AND v.stage = f.stage LEFT JOIN (SELECT planid, COUNT(*) AS stage_cnt, min(stage) AS min_stage FROM a_form GROUP by planid) f2 ON f2.planid = v.planid  left join usr on v.leaderid = usr.userid WHERE visitid = {$pageVars->visitid}  AND f.state = 'ACTIVE'";
        //$qry = "select b.formname, b.formtext, b.formid, a_visit.*, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) as stage_cnt  from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid RIGHT JOIN a_form b ON b.stage = a_visit.stage where visitid = {$pageVars->visitid} and a_form.state = 'ACTIVE' and a_plan.state = 'ACTIVE'";
    }

    //echo $qry;

    $pageDb = new pageQry($con, $qry);
    $pageDb->rsItem();

    //get year groups
    $year_qry = "select year_group.year_text from year_group left join a_visit_year_group on year_group.year_group_id = a_visit_year_group.year_group_id where a_visit_year_group.visitid={$pageVars->visitid}";
    $yearsRs = getRS($con, $year_qry);
    $years_arr = array();
    while ($year_row = getRow($yearsRs)) {
        $years_arr[] = $year_row['year_text'];
    }
    if ($pageDb->row['state'] == 'SUPER_DELETED')
        headerLocation("$rootUrl/modules/lotc/index.php", false);

    //need to get latest form state for this form

    $qry = "select * from a_form_state where visitid = {$pageVars->visitid} and formid = {$pageDb->row['formid']} order by created desc";
   // echo $qry;
    $formStateDb = new pageQry($con, $qry);
    $formStateDb->rsList();
    if (isset($formStateDb->rows[0])) {
        $form_state = $formStateDb->rows[0]['state'];
    } else {
        $form_state = "";
    }
    if ($pageDb->row['state'] != 'DELETED' && ($form_state == "DECLINED FINANCE" || $form_state == "DECLINED PRINCIPAL" || $form_state == "DECLINED DEPUTY PRINCIPAL" || $form_state == "DECLINED GOVERNOR" || $form_state == "DECLINED EVC" || $form_state == "UNSUBMITTED" || $form_state == "") && $pageVars->my_visit) {
        //display form for this stage
        $title = "LOtC: {$pageDb->row['visitname']} :: stage {$pageDb->row['stage']} out of {$pageDb->row['stage_cnt']}";
        $frmTitle = $pageDb->row['formname'];
        $frmText = $pageDb->row['formtext'];
        $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid);
        $content = "page.display_form.php";
    } else {
        //display read only 
        $title = "LOtC: {$pageDb->row['visitname']} :: stage {$pageDb->row['stage']} out of {$pageDb->row['stage_cnt']}";
        $frmTitle = $pageDb->row['formname'];
        $frmText = $pageDb->row['formtext'];
        //set field_state to DELETED so that we get currently deleted fields that were ACTIVE when the form was submitted
        $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid, true);
        $content = "page.display_visit.php";
    }


    if (buttonClicked("save_draft") || buttonClicked("upload_file")) {
        //get db record for this form
        $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid);

        //Now for each field we can compare the db record with the $_POST
        $errors = false;
        foreach ($formDB->rows as $row) {
		//echo var_dump($row);
            if (!editFormDB($row, $con, $pageVars, false)) {
                $errors = true;
            }
        }
        if (!$errors) {
            $messages[] = SAVED;
            if (buttonClicked("upload_file")) {
                headerLocation("$rootUrl/modules/lotc/a_user_upload_doc.php?visitid={$pageVars->visitid}&formid={$pageDb->row['formid']}&fileid=0&accountid={$pageVars->accountid}&userid={$pageVars->userid}&filter=" . rawurlencode(serialize($qs)), $messages);
            } else {
                //headerLocation("$rootUrl/modules/lotc/lotc.php?filter=" . urlencode(serialize($qs)), $messages);
				headerLocation("$rootUrl/modules/lotc/index.php?filter=" . urlencode(serialize($qs)), $messages);
            }
        }
    } else if (buttonClicked("save_submit")) {
	    //get db record for this form
        $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid);
		//Now for each field we can compare the db record with the $_POST
        $errors = false;
        $approval_fields = array();
		foreach ($formDB->rows as $row) {
            if ($row['field_typeid'] == 9)
                $approval_fields[] = 'finance';
            if ($row['field_typeid'] == 10)
                $approval_fields[] = 'principal';
            if ($row['field_typeid'] == 11)
                $approval_fields[] = 'dep_principal';
            if ($row['field_typeid'] == 12)
                $approval_fields[] = 'governor';
            if (!editFormDB($row, $con, $pageVars, true)) {
                $errors = true;
            }
        }
		if (!$errors) {
		    //to form submit stuff here
            //update visit state to 'approval', and audit
            $approval_fields = array_unique($approval_fields);
            $approval = new userApproval($lotc_conf, $con, $pageVars->accountid, $approval_fields);
			//check if finance approval
            if (count($approval->finance) > 0) {
                if (updateVisitState($con, $clsUS, $pageVars->visitid, "FINANCE APPROVAL")) {
                    if (updateFormState($con, $clsUS, $pageVars->visitid, $pageDb->row['formid'], "SUBMITTED")) {
                        //notify finance
                        notifyUser($con, $pageVars, $approval->finance, 'finance');
                        $messages[] = SAVED;
                        headerLocation("$rootUrl/modules/lotc/index.php?rand=" . rand() . "&filter=" . urlencode(serialize($qs)), $messages);
                    }
                }
            }
            //check principal
            if (count($approval->principal) > 0) {
                if (updateVisitState($con, $clsUS, $pageVars->visitid, "PRINCIPAL APPROVAL")) {
                    if (updateFormState($con, $clsUS, $pageVars->visitid, $pageDb->row['formid'], "SUBMITTED")) {
                        //notify evc
                        notifyUser($con, $pageVars, $approval->principal, 'principal');
                        $messages[] = SAVED;
                        headerLocation("$rootUrl/modules/lotc/index.php?rand=" . rand() . "&filter=" . urlencode(serialize($qs)), $messages);
                    }
                }
            }
            //check dep principal
            if (count($approval->dep_principal) > 0) {
                if (updateVisitState($con, $clsUS, $pageVars->visitid, "DEPUTY PRINCIPAL APPROVAL")) {
                    if (updateFormState($con, $clsUS, $pageVars->visitid, $pageDb->row['formid'], "SUBMITTED")) {
                        //notify evc
                        notifyUser($con, $pageVars, $approval->dep_principal, 'deputy_principal');
                        $messages[] = SAVED;
                        headerLocation("$rootUrl/modules/lotc/index.php?rand=" . rand() . "&filter=" . urlencode(serialize($qs)), $messages);
                    }
                }
            }
			//check governor
            if (count($approval->governor) > 0) {
                if (updateVisitState($con, $clsUS, $pageVars->visitid, "GOVERNOR APPROVAL")) {
                    if (updateFormState($con, $clsUS, $pageVars->visitid, $pageDb->row['formid'], "SUBMITTED")) {
                        //notify evc
                        notifyUser($con, $pageVars, $approval->governor, 'governor');
                        $messages[] = SAVED;
                        headerLocation("$rootUrl/modules/lotc/index.php?rand=" . rand() . "&filter=" . urlencode(serialize($qs)), $messages);
                    }
                }
            }
			
		}
		//otherwise do EVC approval
            if (updateVisitState($con, $clsUS, $pageVars->visitid, "APPROVAL")) {
                if (updateFormState($con, $clsUS, $pageVars->visitid, $pageDb->row['formid'], "SUBMITTED")) {
                    //notify evc
                    notifyEvc($con, $pageVars);
                    $messages[] = SAVED;
                    headerLocation("$rootUrl/modules/lotc/index.php?rand=" . rand() . "&filter=" . urlencode(serialize($qs)), $messages);
                }
            }
       //echo "<script type='text/javascript'>alert('submitted successfully!')</script>";
       // headerLocation("$rootUrl/modules/lotc/mylotc.php?filter=" . urlencode(serialize($qs)), $messages);





    } else if (buttonClicked("cancel")) {
       // headerLocation("$rootUrl/modules/lotc/lotc.php?filter=" . urlencode(serialize($qs)), $messages);
	    headerLocation("$rootUrl/modules/lotc/index.php?filter=" . urlencode(serialize($qs)), $messages);
    } else if (buttonClicked("unsubmit")) {
       // if (updateVisitState($con, $clsUS, $pageVars->visitid, "APPROVAL")){
		   //change form state to unsubmitted
	//	   if (updateFormState($con, $clsUS, $pageVars->visitid, $pageVars->formid, $state = "SUBMITTED")){
		      //now change visit stage to this stage
       //       updateVisitStage($con, $clsUS, $pageVars->visitid, $pageVars->stage);
		//   }
		//}
		//change form state to unsubmitted
        updateFormState($con, $clsUS, $pageVars->visitid, $pageVars->formid, $state = "UNSUBMITTED");
        //now change visit stage to this stage
        updateVisitStage($con, $clsUS, $pageVars->visitid, $pageVars->stage);
		
        //display edit form with deleted fields removed
        $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid);
        $content = "page.display_form.php";
    }
} else {
    //trigger_error("Access Denied",E_USER_WARNING);

    headerLocation("$rootUrl/index.php", false);
}

$qry = "select * from a_form_state where visitid = {$pageVars->visitid} order by created desc";
			//and formid = {$pageDb->row['formid']} 
			//echo $qry;
			$formStateDb = new pageQry($con, $qry);
			$formStateDb->rsList(); 
			//echo var_dump($formStateDB);
			$form_state = "";
			if (isset($formStateDb->rows[0])) $form_state = $formStateDb->rows[0]['state'];
			
include("../../layout.php");
?>