<?php
$secure = true;
error_reporting(E_ALL); 
 ini_set("display_errors", 1); 
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_process_form');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$tkn = getStrFromRequest("tkn");

//get current stage for this visit for use in is_allowed_edit_coment below
	$stage_qry = "select stage from a_visit where visitid = {$pageVars->visitid}";
	$stageDb = new pageQry($con, $stage_qry);
	$stageDb->rsItem();
	
	//check if have permission to add comments to this visit
	if ($pageVars->visitid !== null && $pageVars->formid !== null  && is_allowed_add_comment($con, $pageVars, $stageDb->row['stage'])) {
	   $title = "Edit Visit";
	   $content = "page.edit_visit.php";
	} else {
	   //we have something missing or the person cannot add a comment, show an error page
	   $title = "LOtC: Error Editing Visits";
	   $content = "page.comment_error.php";
	}
	
	if (buttonClicked("save")) {
	   $qry = new dbUpdate("a_visit");
	   $qry->setParam("visitid", $pageVars->visitid);
	   $qry->setReqStringVal("visitname",$_POST['visitname'], "visitname");
	   $qry->setNumberVal("leaderid",$_POST['leaderid'], "leaderid");
	   $qry->setReqStringVal("visittext",$_POST['visittext'],"visittext");
	   $visit_date = getStrFromRequest("visit_date");
				
			
				//date time so add hrs and mins to date field
				$hrs = $_POST["visit_date_hrs"];
				$hrs = sprintf("%02d", $hrs);
				$mins = $_POST["visit_date_mins"];
				$mins = sprintf("%02d", $mins);
				$visit_date = "$visit_date {$hrs}:{$mins}:00";
				
				$visit_end = getStrFromRequest("visit_end");
			
				//date time so add hrs and mins to date field
				$hrs = $_POST["visit_end_hrs"];
				$hrs = sprintf("%02d", $hrs);
				$mins = $_POST["visit_end_mins"];
				$mins = sprintf("%02d", $mins);
				$visit_end = "$visit_end {$hrs}:{$mins}:00";
				
				
				$qry->setDateTimeVal("visit_date",$visit_date,"Visit Start time and date");
				$qry->setDateTimeVal("visit_end",$visit_end,"Visit End time and date");
	   if ($qry->execute($con)){
	   	  //echo "Saved";
		  //now do the year groups
		  if (isset($_POST['year_group_id']) && is_array($_POST['year_group_id']) && count($_POST['year_group_id']) > 0) {
		     //first delete all years that aren't in the post
             $del_ids = implode(",", $_POST['year_group_id']);
             $del_qry = "delete from a_visit_year_group where visitid = {$pageVars->visitid} and year_group_id not in($del_ids)";
             $del_exec = execSQL($con, $del_qry);
			 //now remove any already in db from post
                    $sql = "select * from a_visit_year_group where visitid = {$pageVars->visitid}";
                    $rs = getRS($con, $sql);
                    while ($row = getRow($rs)) {
                        if (in_array($row['year_group_id'], $_POST['year_group_id'])) {
                            $_POST['year_group_id'] = array_diff($_POST['year_group_id'], array($row['year_group_id']));
                        }
                    }
					$year_group_errors = false;
                    foreach ($_POST['year_group_id'] as $post) {
                        $qry2 = new dbInsert("a_visit_year_group");
                        $qry2->setReqNumberVal("visitid", $pageVars->visitid, "visitid");
                        $qry2->setReqNumberVal("year_group_id", $post, "year group id");
                        if (!$qry2->execute($con)) {
                            $year_group_errors = true;
                            $messages[] = $qry2->getError();
                        }
                    }
			//now send back to the mylotc page
			headerLocation("mylotc.php?accountid={$pageVars->accountid}&visitid={$pageVars->visitid}&rand=".rand(),$messages);
	   } else {
	      echo "error when saving";
					
		  } 
	   }
	}
	
	if (buttonClicked("cancel")){
	   //var_dump($_POST);
	   headerLocation("mylotc.php?accountid={$pageVars->accountid}&visitid={$pageVars->visitid}&rand=".rand(),$messages);
	}

include("../../layout.php");
?>


