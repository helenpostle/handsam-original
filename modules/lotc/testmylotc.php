<?php

/**
 * Plan editing functionality
 *
 */
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");


$pageFiles->addModFunc('lotc_process_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_notifications');
$pageFiles->addModFunc("lotc_email_history");

$pageFiles->includePhp();
$pageFiles->addModFunc("lotc_popup");

$subpageof = "index.php";

$pageVars = new loadVars($clsUS, $con);
if (isset($pageVars->visitid)){
} else {
  //we dpon't have a visit id is there one in post
  if (isset($_REQUEST['visitid'])) {
      $pageVars->visitid = $_POST['visitid'];
  }
}
if ($pageVars->hacking)
    headerLocation("$rootUrl/index.php");

$qs = array();
if (isset($pageVars->visitid))
  $pageFiles->addModFunc("lotc_daytrip_tickbox");

if (isAllowed("planVisits")) {
  //get filter array querystring
   //if we have come to this page from pressing a button on page.display_form.php
   //the visit id will not be in the page vars so check we have one and if not
   //get from post
   if (!isset($pageVars->visitid)){
       $pageVars->vistid = $_POST['visitid'];
	   
   }

if (isset($_POST['filter'])) {
        $qs = __unserialize($_POST['filter']);
        $qsfrm = $_POST['filter'];
    } else if (isset($_GET['filter'])) {
        $qsfrm = $_GET['filter'];
        $qs = __unserialize(stripslashes($_GET['filter']));
    }
    //set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
    $qs_base = $qs;

    //set first crumbtrail link back to the index page with the list of visits
    $crumb_back = "<a href=\"index.php?accountid={$pageVars->accountid}&amp;rand=" . rand() . "&amp;filter=" . urlencode(serialize($qs)) . "\" title=\"EVC LOtC List\">Planner LOtC list</a>";

    //set jquery scripts
    $pageFiles->addModFunc("lotc_popup");
	
	//load the form history, account resources, handsam lotc resources, handsam lotc risk assessments
	//and email to button
    $box[] = "box.form_state.php";
    $box[] = "box.account_resource_list.php";
    $box[] = "box.handsam_resource_list.php";
    $box[] = "box.handsam_risk_assessment_list.php";
	
	//first get visit table data
    //query for specific stage of visit
    if ($pageVars->stage > 0) {
        //$qry = "SELECT  concat(usr.firstname,' ',usr.lastname) as leader, b.formname, b.formtext, b.formid, b.stage AS stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) as stage_cnt  from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid RIGHT JOIN a_form b ON b.planid = a_visit.planid left join usr on a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid} and (a_form.state = 'ACTIVE' or a_form.state = 'DELETED') and (a_plan.state = 'ACTIVE' or a_plan.state = 'DELETED') and CASE WHEN {$pageVars->stage} < a_visit.stage THEN  b.stage = {$pageVars->stage} ELSE b.stage = a_visit.stage END GROUP by usr.firstname, usr.lastname,b.formname, b.formtext, b.formid, b.stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, a_form.state  ";
        $qry = "SELECT  concat(usr.firstname,' ',usr.lastname) as leader, b.formname, b.formtext, b.formid, b.stage AS stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, a_visit.visit_end, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) as stage_cnt, MIN(a_form.stage) AS min_stage  from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid LEFT JOIN a_form b ON b.planid = a_visit.planid left join usr on a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid}  and CASE WHEN {$pageVars->stage} < a_visit.stage THEN  b.stage = {$pageVars->stage} ELSE b.stage = a_visit.stage END GROUP by usr.firstname, usr.lastname,b.formname, b.formtext, b.formid, b.stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, a_visit.visit_end, a_form.state  ";
    } else {
        //query to get current form stage of visit
        $qry = "SELECT  v.state, concat(usr.firstname,' ',usr.lastname) as leader, v.*, f.formname, f.formtext, f.formid, f2.stage_cnt, f2.min_stage FROM a_visit v LEFT JOIN a_form f ON v.planid = f.planid AND v.stage = f.stage LEFT JOIN (SELECT planid, COUNT(*) AS stage_cnt, min(stage) AS min_stage FROM a_form GROUP by planid) f2 ON f2.planid = v.planid  left join usr on v.leaderid = usr.userid WHERE visitid = {$pageVars->visitid}  AND f.state = 'ACTIVE'";
        //$qry = "select b.formname, b.formtext, b.formid, a_visit.*, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) as stage_cnt  from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid RIGHT JOIN a_form b ON b.stage = a_visit.stage where visitid = {$pageVars->visitid} and a_form.state = 'ACTIVE' and a_plan.state = 'ACTIVE'";
    }
	
	$pageDb = new pageQry($con, $qry);
    $pageDb->rsItem();
	
	//get year groups
    $year_qry = "select year_group.year_text from year_group left join a_visit_year_group on year_group.year_group_id = a_visit_year_group.year_group_id where a_visit_year_group.visitid={$pageVars->visitid}";
    $yearsRs = getRS($con, $year_qry);
    $years_arr = array();
    while ($year_row = getRow($yearsRs)) {
        $years_arr[] = $year_row['year_text'];
    }
	//if the visit has been deleted, send back to index
	if ($pageDb->row['state'] == 'SUPER_DELETED')
        headerLocation("$rootUrl/modules/lotc/index.php", false);
		
	//need to get latest form state for this form
    $qry = "select * from a_form_state where visitid = {$pageVars->visitid} and formid = {$pageDb->row['formid']} order by created desc";
	$formStateDb = new pageQry($con, $qry);
    $formStateDb->rsList();
    if (isset($formStateDb->rows[0])) {
        $form_state = $formStateDb->rows[0]['state'];
    } else {
        $form_state = "";
    }
	
	//display the form
	if ($pageDb->row['state'] != 'DELETED' && ($form_state == "DECLINED FINANCE" || $form_state == "DECLINED PRINCIPAL" || $form_state == "DECLINED DEPUTY PRINCIPAL" || $form_state == "DECLINED GOVERNOR" || $form_state == "DECLINED EVC" || $form_state == "UNSUBMITTED" || $form_state == "") && $pageVars->my_visit) {
	   //display form for this stage
        $title = "LOtC: {$pageDb->row['visitname']} :: stage {$pageDb->row['stage']} out of {$pageDb->row['stage_cnt']}";
        $frmTitle = $pageDb->row['formname'];
        $frmText = $pageDb->row['formtext'];
        $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid);
        $content = "testpage.display_form.php";
	} else {
        //display read only 
        $title = "LOtC: {$pageDb->row['visitname']} :: stage {$pageDb->row['stage']} out of {$pageDb->row['stage_cnt']}";
        $frmTitle = $pageDb->row['formname'];
        $frmText = $pageDb->row['formtext'];
        //set field_state to DELETED so that we get currently deleted fields that were ACTIVE when the form was submitted
        $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid, true);
		$pageVars->commentid = 1;
        $content = "page.display_visit.php";
    }

} else {
    //the person visiting is not allowed to plan visits or the visit id is null
	//send back to index page
    //headerLocation("$rootUrl/index.php", false);
	$content = "page.error.php";

}
		
include("../../layout.php");
?>