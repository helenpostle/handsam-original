<?php

/**
* Plan editing functionality for super admin
*
*/

/**
 * Include basic stuff
 */
//make this a secure page
$secure = false;

require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_admin');
$pageFiles->addModFunc('lotc_add_links');

$pageFiles->includePhp();

$subpageof = "plan.php";

$pageVars = new loadVars($clsUS,$con);

if ($pageVars->ret != "") {
	$return = $pageVars->ret."?rand=".rand();
} else {
	$return = "handsam_guidance_links.php?rand=".rand();
}

$to_file_path = $handsamdocs_fp;

if (isAllowed("editMasterPlan")) {
	
	if ($pageVars->linkid !== null) {
		if ($pageVars->action == "add") {
			//add guidance to lotc
			//first delete any record with same fileid to avoid duplicates
			$qry = new dbDelete("lotc_links");
			$qry->setParam("fileid",$pageVars->linkid);
			if (!$qry->execute($con)) {
				$messages[] = $qry->getError();
			}
			//now add the fileid to lotc_files
			$qry = new dbInsert("lotc_links");
			$qry->setReqNumberVal("linkid",$pageVars->linkid,"linkid");
			$qry->setAudit($clsUS->userid);
			if (!$qry->execute($con)) {
				$messages[] = $qry->getError();
			} else {
				addLotcLinkToAllAccounts($pageVars->linkid,$con, $clsUS);
				headerLocation("handsam_guidance_links.php?rand=".rand(), true);
			}
		}
		
		
		if ($pageVars->action == "rm") {
			//remove guidance from lotc
			if (rmLinkFromLotc($pageVars->linkid, $con)) {
				headerLocation($return, true);
			}
		}
		
		$title = "LOtC: Handsam Guidance Edit Link";
		$content = "page.handsam_guidance_links_edit.php";
		$box[] = "box.guidance.php";

		//get file data and count if this file is used in handsam tasks and lotc
		$qry = "select f.url, f.title, f.linkid, f.linkcategory, f.description, f.created, f.edited, f.created_by, f.edited_by, (count(t.taskid) + count(lotc_links.linkid)) as cnt ";
		$qry .= " FROM links f left join task_links t on f.linkid = t.linkid left join lotc_links on f.linkid = lotc_links.linkid where f.linkid = {$pageVars->linkid} group by f.url, f.title, f.linkid, f.linkcategory, f.description, f.created, f.edited, f.created_by, f.edited_by";
				
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		
		if (buttonClicked("save")) {
			$linkid = lotcResourceLink($clsUS, $pageVars, $con, "handsamDoc");
			
			if ($linkid) {
				headerLocation("handsam_guidance_links.php?rand=".rand(),$messages);
			}

		} else if (buttonClicked("cancel")) {
			headerLocation("handsam_guidance_links.php?rand=".rand(),$messages);
		} else if (buttonClicked("delete")) {
			if ($pageVars->linkid != 0 && $pageDb->row['cnt'] == 0) {
				$linkid = lotcDeleteLink($pageVars, $con, $to_file_path);
				if ($linkid) {	
					headerLocation("handsam_guidance_links.php?rand=".rand(),$messages);	
				}
			}
			
		}		
	} else {

		//list guidance attached to lotc
		//$jquery_plugin[] = "jquery.tablesorter.js";
		//$jquery[] = "table_sorter.js";
		
		$pageFiles->addModFunc("lotc_tables");
		
		$title = "LOtC: Handsam Guidance Links List";
		$content = "page.handsam_guidance_links_list.php";
		$box[] = "box.guidance.php";
		$pageDb = getHandsamGuidanceLinksList($con);
		
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>