<?php

/**
* Plan editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");


$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_process_form');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

if (isAllowed("planVisits")) {

	if (!isset($pageVars->visitid)) {
    $filter_array = array();
    $filter_array['state'] = array("GOVERNOR APPROVAL" => "Governor approval", "DEPUTY PRINCIPAL APPROVAL" => "Deputy Principal approval", "PRINCIPAL APPROVAL" => "Principal approval", "FINANCE APPROVAL" => "Finance approval", "PLANNING" => "In planning", "APPROVAL" => "EVC approval", "COMPLETED" => "Completed", "DELETED" => "Deleted");
    $pageFiles->addModFunc("lotc_visit_filter");
  }
	if (isset($pageVars->visitid)) $pageFiles->addModFunc("lotc_daytrip_tickbox");
	$load = getStrFromRequest("load");
	$qs = array();
	$lotc_highlight = "PLANNING";


	//get filter array querystring
	
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;
	$filter_title = array();
	$filter_qry = array();
	$filter_date_field = " a_visit.created ";
	//process filters
	//state filter
  	if (is_array($qs) && array_key_exists('state', $qs)) { 
		switch ($qs['state']) {
      case '0': 
			$filter_qry['state'] = " a_visit.state != 'DELETED' ";
			$filter_title['state'] = "All";
			break;
    
      case "DEFAULT":
			$filter_qry['state'] = " (a_visit.state = 'PLANNING' or  a_visit.state = 'APPROVAL') ";
			$filter_title['state'] = "Planning &amp; Approval";
			break;
    
			case "ALL":
			$filter_qry['state'] = " a_visit.state != 'DELETED' ";
			$filter_title['state'] = "All States";
			break;
		
			case "PLANNING":
			$filter_qry['state'] = " a_visit.state = 'PLANNING' ";
			$filter_title['state'] = "Planning";
			break;
			
      case "FINANCE APPROVAL":
			$filter_qry['state'] = " a_visit.state = 'FINANCE APPROVAL' ";
			$filter_title['state'] = "Finance Approval";
			break;
    
			case "PRINCIPAL APPROVAL":
			$filter_qry['state'] = " a_visit.state = 'PRINCIPAL APPROVAL' ";
			$filter_title['state'] = "Principal Approval";
			break;
    
			case "DEPUTY PRINCIPAL APPROVAL":
			$filter_qry['state'] = " a_visit.state = 'DEPUTY PRINCIPAL APPROVAL' ";
			$filter_title['state'] = "Deputy Principal Approval";
			break;
    
			case "GOVERNOR APPROVAL":
                $filter_qry['state'] = " a_visit.state = 'GOVERNOR APPROVAL' ";
                $filter_title['state'] = "Governor Approval";
                break;
            
            case "APPROVAL":
			$filter_qry['state'] = " a_visit.state = 'APPROVAL' ";
			$filter_title['state'] = "Approval";
			break;
			
			case "COMPLETED":
			$filter_qry['state'] = " a_visit.state = 'COMPLETED' ";
			$filter_title['state'] = "Completed";
			break;

			case "DELETED":
			$filter_qry['state'] = " a_visit.state = 'DELETED' ";
			$filter_title['state'] = "Deleted";
			break;
							
				
			default: 
			$filter_qry['state'] = " (a_visit.state = 'PLANNING' or  a_visit.state = 'APPROVAL') ";
			$filter_title['state'] = "Planning &amp; Approval";
      $qs_base['state'] = 'DEFAULT';
			break;
		}
	} else {
		$filter_qry['state'] = " (a_visit.state = 'PLANNING' or  a_visit.state = 'APPROVAL'  or  a_visit.state = 'DEPUTY PRINCIPAL APPROVAL'  or  a_visit.state = 'FINANCE APPROVAL'  or  a_visit.state = 'PRINCIPAL APPROVAL') ";
		$filter_title['state'] = "Planning &amp; Approval";
    $qs_base['state'] = 'DEFAULT';
	}
	
	
	//choose if date created ot visit date filtered
	if (is_array($qs) && array_key_exists('date', $qs)) { 
		switch ($qs['date']) {
			case "visit_date":
			$filter_date_field = " a_visit.visit_date ";
			$filter_title['date'] = "Visit Date";
			break;
				
			default: 
			//$filter_date_field = " a_visit.created ";
			//$filter_title['date'] = "Date Created";
			$filter_date_field = " a_visit.visit_date ";
			$filter_title['date'] = "Visit Date";
			break;
		}
	} else {
		$filter_date_field = " a_visit.visit_date ";
		$filter_title['date'] = "Visit Date";
	}

	//month filter
	if (is_array($qs) && array_key_exists('month', $qs) && $qs['month'] >= 1) {
		settype($qs['month'], 'string');
		$filter_qry['month'] = " MONTH($filter_date_field) = '{$qs['month']}' ";
		$filter_title['month'] = date( 'F', mktime(0, 0, 0, $qs['month']) );; 
	} else {
		$filter_title['month'] = "All months"; 

	}	
	
	//year filter
	if (is_array($qs) && array_key_exists('year', $qs) && $qs['year'] >= 1) {
		settype($qs['year'], 'int');
		$filter_qry['year'] = " YEAR($filter_date_field) = '{$qs['year']}' ";
		$filter_title['year'] = $qs['year']; 
	} else {
		$filter_title['year'] = "All Years"; 

	}		
	
	//user filter
	if (is_array($qs) && array_key_exists('user', $qs) && $qs['user'] >= 1) {
		//need to get the planner name
		settype($qs['user'], 'int');
		$planner_qry = "select CONCAT(u.firstname, ' ', u.lastname) AS name from usr u where u.userid = {$qs['user']}";
		$plannerRS = getRS($con, $planner_qry);
		$plannerRow = getRow($plannerRS);
		$filter_qry['user'] = " leaderid = {$qs['user']} ";
		$filter_title['user'] = $plannerRow['name'];
	} else {
		$filter_title['user'] = "All Users"; 

	}	
		//now implode filter title
	
	$filter_title_str = ": ".implode(": ",$filter_title);
		
	//set jquery scripts
	//$jquery_plugin[] = "jquery.popUp.js";
	//$jquery[] = "lotc.popup.js";
	//$jquery[] = "lotc.visit_filter.js";
	//$jquery_plugin[] = "jquery.tablesorter.js";
	//$jquery[] = "table_sorter.js";
	
	
	
	if ($pageVars->visitid !== null) {
		//for use in determining if the visit date can be edited still. If any stage has been confirmed, then the date cannot be edited
		$visit_state = visit_state($con, $pageVars->visitid, "APPROVED EVC");
		$content = "page.visit_edit.php";
		//edit /create visit
		$title = "LOtC: New Visit";
		$qry = "select a_visit.*  from a_visit where visitid = {$pageVars->visitid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
		if (buttonClicked("save")) {
			
			$visit_date = getStrFromRequest("visit_date");
		
			//date time so add hrs and mins to date field
			$hrs = $_POST["visit_date_hrs"];
			$hrs = sprintf("%02d", $hrs);
			$mins = $_POST["visit_date_mins"];
			$mins = sprintf("%02d", $mins);
			$visit_date = "$visit_date {$hrs}:{$mins}:00";

			$visit_end = getStrFromRequest("visit_end");
		
			//date time so add hrs and mins to date field
			$hrs = $_POST["visit_end_hrs"];
			$hrs = sprintf("%02d", $hrs);
			$mins = $_POST["visit_end_mins"];
			$mins = sprintf("%02d", $mins);
			$visit_end = "$visit_end {$hrs}:{$mins}:00";
			
				
			if ($pageVars->visitid == 0) {
				$qry_stage = "select MIN( CASE WHEN a_form.state = 'ACTIVE' THEN a_form.stage END ) AS stage";
				$qry_stage .= " from a_plan left join a_form on a_plan.planid = a_form.planid";
				$qry_stage .= " where a_plan.planid = {$pageVars->planid}";
				
				$stageDb = new pageQry($con, $qry_stage);
				$stageDb->rsItem(); 
				$qry = new dbInsert("a_visit");
				$qry->setReqNumberVal("stage",$stageDb->row['stage'],"planid");
				$qry->setReqStringVal("state","PLANNING","state");
				
				//adjust planid so that 0 is null
				if ($pageVars->planid == 0) $pageVars->planid = "";
				$qry->setReqNumberVal("planid",$pageVars->planid,"planid");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
				$qry->setReqNumberVal("leaderid",$pageVars->userid,"userid");
			} else {
				$qry = new dbUpdate("a_visit");
				
				$qry->setParam("visitid",$pageVars->visitid);
				$qry->setReqNumberVal("leaderid", $_POST['leaderid'],"leaderid");
				$qry->setStringVal("state","PLANNING","state");
				$qry->setNumberVal("planid",$pageVars->planid,"planid");
			}
		
			if (isset($qry)) {
				$qry->setReqStringVal("visitname",$_POST["visitname"],"visit name");
				$qry->setStringVal("visittext",$_POST["visittext"],"visit text");
				
				//if ($visit_state->cnt == 0) {
					
					$qry->setDateTimeVal("visit_date",$visit_date,"Visit Start time and date");
					$qry->setDateTimeVal("visit_end",$visit_end,"Visit End time and date");
				//}
				
				$qry->setAudit($clsUS->userid);
				//echo $qry->getSql();
				
				if (!isset($_POST['year_group_id']) || (isset($_POST['year_group_id']) && $_POST['year_group_id'] < 1) || (isset($_POST['year_group_id']) && is_array($_POST['year_group_id']) && count($_POST['year_group_id']) == 0)) {
					$qry->failError .= "Please select at least one year group.\r\n";
				}
				
				if ($qry->execute($con)) {
					if ($pageVars->visitid == 0) {
						$pageVars->visitid = $qry->getNewID();	
					}
					
					//now record yeargroup for this visit
					if (isset($_POST['year_group_id']) && is_array($_POST['year_group_id']) && count($_POST['year_group_id']) > 0 ) {
						//first delete all years that aren't in the post
						$del_ids = implode(",", $_POST['year_group_id']);
						$del_qry = "delete from a_visit_year_group where visitid = {$pageVars->visitid} and year_group_id not in($del_ids)";
						$del_exec = execSQL($con, $del_qry);
						//now remove any already in db from post
						$sql = "select * from a_visit_year_group where visitid = {$pageVars->visitid}";
						$rs = getRS($con, $sql);
						while ($row = getRow($rs)) {
							echo "<br/>{$row['year_group_id']}<br/>";
							if (in_array($row['year_group_id'], $_POST['year_group_id'])) {
								$_POST['year_group_id'] = array_diff($_POST['year_group_id'], array($row['year_group_id']));
							}
						}
						//now insert new year groups into db	
						$year_group_errors = false;
						foreach($_POST['year_group_id'] as $post) {
							$qry2 = new dbInsert("a_visit_year_group");
							$qry2->setReqNumberVal("visitid",$pageVars->visitid,"visitid");
							$qry2->setReqNumberVal("year_group_id",$post,"year group id");
							if (!$qry2->execute($con)) {
								$year_group_errors = true;
								$messages[] = $qry2->getError();
							}
						}
					}
					if ($year_group_errors == false) {
						$messages[] = SAVED;
						headerLocation("mylotc.php?accountid=$pageVars->accountid&userid=$pageVars->userid&visitid=$pageVars->visitid", $messages);
						//list visits
						//$title = "LOtC: Visits List$filter_title_str";
						//$content = "page.visit_list.php";
						//$pageDb = getVisitList($pageVars, $con, $filter_qry); 
						//$box[] = "box.visit_list_filter.php";
					}
										
				} else {
					$messages[] = $qry->getError();	
				}
			}
		/*
		
		} else if (buttonClicked("delete")) {
				$qry = new dbUpdate("a_visit");
				$qry->setParam("visitid",$pageVars->visitid);
				$qry->setReqStringVal("state","DELETED","State");
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#change content to list
					$messages[] = DELETED;
					//list plans
					$title = "LOtC: Visit List";
					$content = "lotc/page.visit_list.php";
					$pageDb = getVisitList($pageVars, $con, $filter_qry);
					$box[] = "lotc/box.visit_list_filter.php"; 
				}
				*/
		} else if (buttonClicked("cancel")) {
			headerLocation("index.php?accountid=$pageVars->accountid&userid=$pageVars->userid", $messages);
			//list plans
			//$title = "LOtC: Visit List$filter_title_str";
			//$content = "page.visit_list.php";
			//$pageDb = getVisitList($pageVars, $con, $filter_qry); 
			//$pageVars->visitid = '';
			//$box[] = "box.visit_list_filter.php";
		}
	
				
	} else {
		
		//list plans
		$pageFiles->addModFunc("lotc_tables");
		$title = "LOtC: Visit List$filter_title_str";
		$content = "page.visit_list.php";
		$pageDb = getVisitList($pageVars, $con, $filter_qry);
		//$box[] = "box.visit_list_filter.php";
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>