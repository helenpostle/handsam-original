<?php
$ajaxModule = 'lotc';

require("../shared/startPage.php");

####make this a secure page
$secure = true;

require("../library/securePage.php");

####include other functions
include("../library/funcRequest.php");
include("../library/funcDisplay.php");
include("../library/funcDbEdit.php");
include("../library/funcDbForm.php");
include("../lotc/library/clsLoadVars.php");
include("../lotc/library/clsPageQry.php");

include("../lotc/library/funcLotcProcessForm.php");
include("../lotc/library/funcLotcDisplayForm.php");
include("../lotc/library/funcLotcDisplayVisit.php");
include("../lotc/library/funcLotcVisits.php");

$pageVars = new loadVars($clsUS,$con);
if (isAllowed("planVisits") || isAllowed("editVisits")) {
	//check if have permission to add comments to this visit
	if ($pageVars->visitid !== null && $pageVars->formid !== null && $pageVars->commentid !==null && is_allowed_add_comment($con, $pageVars)) {
		//noew need to check if editing a comment, it is their comment
		if ($pageVars->commentid > 0 && is_allowed_edit_comment($con, $pageVars, $pageVars->commentid) || $pageVars->commentid == 0) {
			$commenttext = getStrFromRequest("commenttext");
			$load = getStrFromRequest("load");
			
			
			$qry = "select a_comments.*  from a_comments where  commentid = {$pageVars->commentid}";
			$pageDb = new pageQry($con, $qry);
			$pageDb->rsItem(); 
			
			if (buttonClicked("save")) {
	
				if ($pageVars->commentid == 0) {
					$qry = new dbInsert("a_comments");
					$qry->setReqNumberVal("formid",$pageVars->formid);
					$qry->setReqNumberVal("visitid",$pageVars->visitid);				
				} else {
					$qry = new dbUpdate("a_comments");
					$qry->setParam("commentid",$pageVars->commentid);
				}
				
				$qry->setReqStringVal("commenttext",$commenttext,"comment text");
				$qry->setAudit($clsUS->userid);
				//echo $qry->getSql();
				if ($qry->execute($con)) {
				} else {
					$messages[] = $qry->getError();	
				}
	
			} else if (buttonClicked("cancel")) {
				
				echo "cancel";		
				
			} else {
				if ($load == 'yes') {
					$comments = getComments($con, $pageVars->visitid, $pageVars->formid);
					if ($comments->cnt > 0) {
						echo "<ul id=\"comment_list\">";
						foreach ($comments->rows as $row) {
						echo "<li class=\"clearfix\">";
						echo "<strong>{$row['firstname']} {$row['lastname']} : <em> ".ucwords($row['usrtype'])."</em></strong> : <em>".displayLotcDateTime($row['created'])."</em>";
						//display edit button if comment is the users own
						if (is_allowed_edit_comment($con, $pageVars, $row['commentid'])) {
							echo " <a class=\"add_comment\" href=\"?visitid={$pageVars->visitid}&amp;formid={$pageDb->row['formid']}&amp;commentid={$row['commentid']}&amp;accountid={$pageVars->accountid}&amp;userid={$pageVars->userid}\">[ edit comment ]</a>";
						}
						
						echo displayParagraphs($row['commenttext']);
						echo "</li>";
						}
						echo "</ul>";
					}						
				} else {			
					//list plans
					include("../lotc/content/page.comment_edit.php");
				}
			}
		}
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);

}
?>