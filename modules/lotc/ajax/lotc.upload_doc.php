<?php

/**
* Plan editing functionality for super admin
*
*/

/**
 * Include basic stuff
 */
$ajaxModule = 'lotc';

require("../shared/startPage.php");

####make this a secure page
$secure = true;
require("../library/securePage.php");

####include other functions
include("../library/funcRequest.php");
include("../library/funcDisplay.php");
include("../library/funcDbEdit.php");
include("../library/funcDbForm.php");
include("../lotc/library/clsLoadVars.php");
include("../lotc/library/clsPageQry.php");
include("../lotc/library/funcLotcAdmin.php");
include("../library/funcFiles.php");
include("../lotc/library/funcLotcFileUpload.php");


$pageVars = new loadVars($clsUS,$con);
$to_file_path = $accountLotcUserDocs_fp;
$load = getStrFromRequest("load");
if (isAllowed("planVisits")) {
	if ($pageVars->fileid !== null) {


		$title = "LOtC: Account Resources Edit Document";
		
		//get file data and count if this file is used in handsam tasks and lotc
		$qry = "select f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by";
		$qry .= " FROM files f where f.fileid = {$pageVars->fileid} ";
				
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		
		if (buttonClicked("save")) {
			$fileid = lotcUploadFile($clsUS, $pageVars, $con, $to_file_path, "accountLotcUserDoc");
			if ($fileid) {
				//first delete this file from lotc_files - no chance of duplicates!
				$qry = new dbDelete("a_lotc_files");
				$qry->setParam("fileid",$fileid);
				$qry->setParam("accountid",$pageVars->accountid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				}
				//now add the fileid to lotc_files
				$qry = new dbInsert("a_lotc_files");
				$qry->setReqNumberVal("fileid",$fileid,"fileid");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
				$qry->setAudit($clsUS->userid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					//echo "sql:<br/>";
					//echo $qry->getSql();
					//headerLocation("a_resource_docs.php?rand=".rand(),$messages);
				}
			} 
			//echo "qq";
		} else if (buttonClicked("cancel")) {
			
			echo "cancel";
			
		} else if (buttonClicked("delete")) {
			if ($pageVars->fileid != 0 && $pageDb->row['cnt'] == 0) {
				$fileid = lotcDeleteFile($pageVars, $con, $to_file_path);
				if ($fileid) {
					//first delete this file from lotc_files - no chance of duplicates!
					$qry = new dbDelete("a_lotc_files");
					$qry->setParam("fileid",$fileid);
					$qry->setParam("accountid",$pageVars->accountid);
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					} else {
						headerLocation("a_resource_docs.php?rand=".rand(),$messages);
					}
				}

			}
			
		} else {
			if ($load == 'yes') {
			} else {
				include("../lotc/content/page.a_resource_docs_edit.php");
			}
		}
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);
}

?>