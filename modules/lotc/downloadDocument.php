<?php
/**
* Securely Download a file from the database
*/

//MUST ADD SECURITUY AND CHECK FILE EXISTS
$content = "";
//make this a secure page
$secure = false;
//include basic stuff
include ("../../shared/startPage.php");

$pageFiles->includePhp();
//specify page content
$title = "Download Failes";		//the <title> tag and <h1> page title

//get the id 
$id = getIntFromRequest("id");
$rsCheckContent = getRS($con,"select * from content_files where fileid = $id");

if (getRow($rsCheckContent) || isAllowed("uploadHandsamDocs") || isAllowed("uploadAdminDocs") || isAllowed("uploadUserDocs")) {

	//get the record set
	$sql = "select * from files where fileid = $id";
	
	$rsFile = getRS($con, $sql);
	if ($rowFile = getRow($rsFile)) {
		//download the file
		$type = $rowFile["filetype"];
		$filename = htmlspecialchars_decode($rowFile["filename"]);
		
		
		if ($type == "handsamDoc") {
			$path = $handsamdocs_fp;
		} else if ($type == "adminDoc") {
			$path = $admindocs_fp;
		} else if ($type == "userDoc") {
			$path = $userdocs_fp;
		} else if ($type == "accountLotcDoc") {
			$path = $accountLotcDocs_fp;
		}
		
		$file = $path.$filename;
	
		// send the right headers
		
		header("Content-Type: application/octet-stream");
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		if ($rowFile['mid'] > 0 && $type == "handsamDoc") {
			$tjs = time() + 60000;
			$file =  $core_conf['handsam_docs_url']."?id=".$rowFile['mid']."&tjs=$tjs";
			echo file_get_contents($file);
			exit;
		} else {
			if (@file_exists($file)) echo file_get_contents($file);
			exit;
		}

	}
}