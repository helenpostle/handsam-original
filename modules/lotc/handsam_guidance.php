<?php

/**
* Plan editing functionality for super admin
*
*/

/**
 * Include basic stuff
 */
 
require("../../shared/startPage.php");

####make this a secure page
$secure = true;

$pageFiles->addModFunc('lotc_admin');
$pageFiles->addModFunc('lotc_file_upload');

$pageFiles->includePhp();

$subpageof = "plan.php";

$pageVars = new loadVars($clsUS,$con);

if ($pageVars->ret != "") {
	$return = $pageVars->ret."?rand=".rand();
} else {
	$return = "handsam_guidance.php?rand=".rand();
}

$to_file_path = $handsamdocs_fp;



if (isAllowed("editMasterPlan")) {
	
	if ($pageVars->fileid !== null) {
		if ($pageVars->action == "add") {
			//add guidance to lotc
			//first delete any record with same fileid to avoid duplicates
			$qry = new dbDelete("lotc_files");
			$qry->setParam("fileid",$pageVars->fileid);
			if (!$qry->execute($con)) {
				$messages[] = $qry->getError();
			}
			//now add the fileid to lotc_files
			$qry = new dbInsert("lotc_files");
			$qry->setReqNumberVal("fileid",$pageVars->fileid,"fileid");
			$qry->setAudit($clsUS->userid);
			if (!$qry->execute($con)) {
				$messages[] = $qry->getError();
			} else {
				//now add to all lotc accounts a_lotc_files
				addLotcFileToAllAccounts($pageVars->fileid,$con, $clsUS);
				
				headerLocation("handsam_guidance.php?rand=".rand(), true);
			}
		}
		
		
		if ($pageVars->action == "rm") {
			//remove guidance from lotc
			if (rmFileFromLotc($pageVars->fileid, $con)) {
				headerLocation($return, true);
			}
		}
		
		$title = "LOtC: Handsam Guidance Edit Document";
		$content = "page.handsam_guidance_edit.php";
		$box[] = "box.guidance.php";

		//get file data and count if this file is used in handsam tasks and lotc
		$qry = "select f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by, (count(t.taskid) + count(lotc_files.fileid)) as cnt ";
		$qry .= " FROM files f left join task_files t on f.fileid = t.fileid left join lotc_files on f.fileid = lotc_files.fileid where f.fileid = {$pageVars->fileid} group by f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by";
				
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		//echo $qry;
		
		if (buttonClicked("save")) {
			$fileid = lotcUploadFile($clsUS, $pageVars, $con, $to_file_path,"handsamDoc");
			if ($fileid) {
				headerLocation("handsam_guidance.php?rand=".rand(),$messages);
			}
			
		} else if (buttonClicked("cancel")) {
			headerLocation("handsam_guidance.php?rand=".rand(),$messages);
		} else if (buttonClicked("delete")) {
			if ($pageVars->fileid != 0 && $pageDb->row['cnt'] == 0) {
				$fileid = lotcDeleteFile($pageVars, $con, $to_file_path);
				if ($fileid) {
					headerLocation("handsam_guidance.php?rand=".rand(),$messages);
				}

			}
			
		}
		
	} else {

		//list guidance attached to lotc
		//$jquery_plugin[] = "jquery.tablesorter.js";
		//$jquery[] = "table_sorter.js";
		$pageFiles->addModFunc("lotc_tables");
		$title = "LOtC: Handsam Guidance Documents List";
		$content = "page.handsam_guidance_list.php";
		$box[] = "box.guidance.php";
		$pageDb = getHandsamGuidanceList($con);
		
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>