<?php

/**
* Plan editing functionality
*
*/

/**
 * Include basic stuff
 */
 
####make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_account_admin');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

if (isAllowed("editVisits")) {

	if ($pageVars->planid !== null) {
		//edit /create plan
		$title = "LOtC: Edit Plan";
		$content = "page.account_plan_copy.php";
		$qry = "select * from a_plan where planid = {$pageVars->planid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
	
		if (buttonClicked("save")) {
			#check that a plan doesn't exist with this name
			$sql = "select * from a_plan where planname = '".mysql_real_escape_string($_POST['planname'])."'  and accountid = {$pageVars->accountid}";
			$rsCheck = getRS($con,$sql);
			if (getRow($rsCheck)) {
				$messages[] = PLAN_IN_USE;	
			} else {
							
				$qry = new dbInsert("a_plan");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
		
				$qry->setReqStringVal("planname",$_POST["planname"],"plan name");
				$qry->setStringVal("plantext",$_POST["plantext"],"plan text");
				$qry->setReqStringVal("state","INACTIVE","state");
				
				$qry->setAudit($clsUS->userid);
				
				if ($qry->execute($con)) {
					$new_planid = $qry->getNewID();	
					
					//now copy all ACTIVE forms and fields in the original  plan to the new plan
					
					//now get all forms in this plan and copy them
					$qry_frm = "SELECT a_form.* FROM a_form WHERE a_form.planid = {$pageVars->planid} AND a_form.state = 'ACTIVE'";
					$rs_frm = getRS($con, $qry_frm);
					while ($row_frm = getRow($rs_frm)) {
						$qry_frm = new dbInsert("a_form");
						$qry_frm->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
						$qry_frm->setReqNumberVal("m_formid",$row_frm["m_formid"],"m_formid");
						$qry_frm->setNumberVal("planid",$new_planid,"plan ID");
						$qry_frm->setReqNumberVal("stage",$row_frm["stage"],"stage");
						$qry_frm->setReqStringVal("formname",$row_frm["formname"],"form name");
						$qry_frm->setStringVal("formtext",$row_frm["formtext"],"form text");
						$qry_frm->setReqStringVal("state","ACTIVE","state");
						$qry_frm->setAudit($clsUS->userid);
						if ($qry_frm->execute($con)) {
							//$messages[] = "Plan {$row['planname']} :: Form {$row_frm['formname']} copied";
							$formid = $qry_frm->getNewID();
							
							//now get all fields for this form
							$qry_fld = "select * from a_field where formid = {$row_frm['formid']} and state = 'ACTIVE'";
							$rs_fld = getRS($con, $qry_fld);
							while ($row_fld = getRow($rs_fld)) {
								$qry_fld = new dbInsert("a_field");
								$qry_fld->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
								$qry_fld->setReqNumberVal("m_fieldid",$row_fld["m_fieldid"],"m_fieldid");
								$qry_fld->setNumberVal("formid",$formid,"Form ID");
								$qry_fld->setReqStringVal("fieldname",$row_fld["fieldname"],"field name");
								$qry_fld->setStringVal("fieldtext",$row_fld["fieldtext"],"field text");
								$qry_fld->setStringVal("params",$row_fld["params"],"params");
								$qry_fld->setNumberVal("required",$row_fld["required"],"required");
								$qry_fld->setStringVal("default_value",$row_fld["default_value"],"default value");
								$qry_fld->setReqNumberVal("field_typeid",$row_fld["field_typeid"],"field type id");
								$qry_fld->setReqStringVal("state","ACTIVE","state");
								$qry_fld->setReqNumberVal("field_order",$row_fld["field_order"],"field_order");
								$qry_fld->setAudit($clsUS->userid);
								if ($qry_fld->execute($con)) {
									//$messages[] = "Plan {$row['planname']} :: Form {$row_frm['formname']} :: Field {$row_fld['fieldname']} copied";
									
									
								} else {
									//$messages[] = "Plan {$row['planname']} :: Form {$row_frm['formname']} :: Field {$row_fld['fieldname']} ERROR copying:".$qry_fld->getError();
									//$messages[] = $qry_fld->getSql();
								}
				
							}					
						} else {
							//$messages[] = "Plan {$row['planname']} :: Form {$row_frm['formname']} ERROR copying:".$qry_frm->getError();
						}
		
					}
					//list plans
					headerLocation("a_plan.php", $messages);					
				} else {
					$messages[] = $qry->getError();	
					headerLocation("a_plan.php", $messages);
				}
			}
		} else if (buttonClicked("cancel")) {
			headerLocation("a_plan.php", $messages);
		}
	
				
	} else {
		//list plans
		headerLocation("a_plan.php", $messages);
	}

}  else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>