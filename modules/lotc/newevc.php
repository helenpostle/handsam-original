<?php
/**
* Plan editing functionality
*
*/
error_reporting(E_ALL);
ini_set('display_errors', '1');
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");


$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_process_form');


$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
if ($pageVars->hacking) headerLocation("$rootUrl/index.php");

//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;
	$filter_title = array();
	$filter_qry = array();
	$filter_date_field = " a_visit.created ";
	//echo var_dump($qs_base);
	//set the crumbtrail first link back to the evc page with list of visits
	$crumb_back = "<a href=\"evc.php?accountid={$pageVars->accountid}&amp;rand=".rand()."&amp;filter=".urlencode(serialize($qs))."\" title=\"EVC LOtC List\">EVC LOtC list</a>";
	
	$filter_qry = array();
	$filter_date_field = " a_visit.created ";
	if ($pageVars->visitid != null){
	  //we have either come back from page of visit
	  //after clicking on a button, or we want to view a visist
		if ($pageVars->stage > 0) {
		  $qry = "SELECT concat(usr.firstname,' ',usr.lastname) as leader, b.formname, b.formtext, b.formid, b.stage AS stage, a_visit.visit_end, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) as stage_cnt  from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid RIGHT JOIN a_form b ON b.planid = a_visit.planid left join usr on a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid} and a_form.state = 'ACTIVE' and a_plan.state = 'ACTIVE' and CASE WHEN {$pageVars->stage} < a_visit.stage THEN  b.stage = {$pageVars->stage} ELSE b.stage = a_visit.stage END GROUP by usr.firstname, usr.lastname,b.formname, b.formtext, b.formid, b.stage, a_visit.visit_end, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, a_form.state ";
		} else {
		  $qry = "SELECT concat(usr.firstname,' ',usr.lastname) as leader, v.*, f.formname, f.formtext, f.formid, f2.stage_cnt FROM a_visit v LEFT JOIN a_form f ON v.planid = f.planid AND v.stage = f.stage LEFT JOIN (SELECT planid, COUNT(*) AS stage_cnt FROM a_form GROUP by planid) f2 ON f2.planid = v.planid left join usr on v.leaderid = usr.userid WHERE visitid = {$pageVars->visitid}  AND f.state = 'ACTIVE'";
		}
		
		//get year groups
		$year_qry = "select year_group.year_text from year_group left join a_visit_year_group on year_group.year_group_id = a_visit_year_group.year_group_id where a_visit_year_group.visitid={$pageVars->visitid}";
		$yearsRs = getRS($con, $year_qry); 
		$years_arr = array();
		while ($year_row = getRow($yearsRs)) {
			$years_arr[] = $year_row['year_text'];
		}
		
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
		//echo $qry;
		
		/*****************************************************************************
		******************************************************************************
		********** HAS A BUTTON BEEN CLICKED OR DO WE SHOW THE PLAN
		******************************************************************************
		*****************************************************************************/
		if (buttonClicked("approve")) {
		   /*****************************************************************************
		  ****************************************************************************
		  ********* THE APPROVE BUTTON HAS BEEN CLICKED******************************
		  ****************************************************************************
		  ****************************************************************************/
    	   echo "THE APPROVE BUTTON HAS BEEN CLICKED";
		} elseif (buttonClicked("decline")) {
		  /*****************************************************************************
		  ****************************************************************************
		  ********* THE DECLINE BUTTON HAS BEEN CLICKED******************************
		  ****************************************************************************
		  ****************************************************************************/
    	  echo "THE DECLINE BUTTON HAS BEEN CLICKED";
		} elseif (buttonClicked("cancel")) {
    	  /*****************************************************************************
		  ****************************************************************************
		  ********* THE CANCEL BUTTON HAS BEEN CLICKED******************************
		  ****************************************************************************
		  ****************************************************************************/
		  echo "The cancel button has been clicked";
		  
		}elseif (buttonClicked("delete")) {
		  /*****************************************************************************
		  ****************************************************************************
		  ********* THE DELETE BUTTON HAS BEEN CLICKED******************************
		  ****************************************************************************
		  ****************************************************************************/
    	  echo "The DELETE button has been clicked";
		}elseif (buttonClicked("unsubmit")) {
		  /*****************************************************************************
		  ****************************************************************************
		  ********* THE UNSUBMIT BUTTON HAS BEEN CLICKED******************************
		  ****************************************************************************
		  ****************************************************************************/
    	 // echo "The unsubmit button has been clicked";
		 // echo var_dump($_POST);
		 // echo "<br />about to update the form state of visit id: ".$pageVars->visitid;
		  $formid = $pageVars->formid -1;
		 // echo " form id: ".$formid." the state will be set to submitted";
		  //we need to go back one on the form because we need to go back on the stage as well
		  updateFormState($con, $clsUS, $pageVars->visitid, $pageVars->formid, $state = "SUBMITTED");
		 //we need to go back 1 stage
		 if ($pageVars->stage == 1){
		    $stage = $pageVars->stage;
		 } else {
		    $stage = $pageVars->stage -1;
		 }
		 updateVisitStage($con, $clsUS, $pageVars->visitid, $stage);
		 //get the list for the page
		 $pageDb = getVisitList($pageVars, $con, $filter_qry);
		 $content = "page.evc_visit_list.php";
		  headerLocation("$rootUrl/modules/lotc/index.php?rand=" . rand() . "&filter=" . urlencode(serialize($qs)), $messages);
		}else {
		   /*********************************************************************************
		**********************************************************************************
		***   SHOW THE VISIT PLAN
		**********************************************************************************
		**********************************************************************************/
    	  //no buttons clicked show the page with the selected visit plan
		   $qry = "select * from a_form_state where visitid = {$pageVars->visitid} and formid = {$pageDb->row['formid']} order by created desc ";
		 //  echo var_dump($pageDb);
		  // echo "the value of row is ". $pageDb->row;
		   if ($pageDb->row){
		      $formStateDb = new pageQry($con, $qry);
			  $formStateDb->rsList(); 
			  $form_state = "";
			  if (isset($formStateDb->rows[0])) $form_state = $formStateDb->rows[0]['state'];
			  $title = displayText("LOtC: {$pageDb->row['visitname']} :: stage {$pageDb->row['stage']} out of {$pageDb->row['stage_cnt']}");
			  $frmTitle = $pageDb->row['formname'];
			  $frmText = $pageDb->row['formtext'];
			  //GET THE DATA FIELDS FOR THE STAGE THIS PLAN IS CURRENTLY AT
			  $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid, FALSE);
			  $content = "newpage.evc_display_visit.php";
			  //add the form history
			  $box[] = "box.form_state.php";
			  $box[] = "box.account_resource_list.php";
			  $box[] = "box.handsam_resource_list.php";
			  $box[] = "box.handsam_risk_assessment_list.php";
		   } else {
		      //there are no results from the sql query
			  $title = "there was a problem with the sql query ".$pageDb->qry;
		   }
		
			
		}
			
	/****************************************************************************************
	*****************************************************************************************
	*********** NO VISIT ID SEND BACK TO LIST
	*****************************************************************************************
	*****************************************************************************************/	
		
	} else { 
		//echo $qry;
		//visit id is null, show list 
		$title = "LOtC: Visit List $filter_title_str";
		$content = "newpage.evc_visit_list.php";
		$pageDb = getVisitList($pageVars, $con, $filter_qry, true);
		echo var_dump($pageDb);
   }


include("../../layout.php");
?>