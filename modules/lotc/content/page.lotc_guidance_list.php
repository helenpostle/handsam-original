<?php
/***
** Shows the list of lotc plans
** 
***/

if ($pageDb->rsExists()) {
	?>
	<table  id="guidance_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Title</th>
				<th class="">Filename</th>
				<th class="">Date Attached</th>
				<th class="end">Remove</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "$rootUrl/modules/lotc/downloadDocument.php?id=".$row['fileid'];
		$action = "<a class=\"rm_link\" href=\"handsam_guidance.php?action=rm&amp;fileid={$row['fileid']}&amp;ret=lotc_guidance.php\">Remove from LOtC</a>";
		?>
		<tr>
			<td class="start"><a href="<?php echo $url;?>"><?php echo displayText($row["title"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["filename"] ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayDate($row["created"]) ?></a></td>
			<td class="end"><?php echo $action; ?></td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no LOtC guidance documents to view");
}
