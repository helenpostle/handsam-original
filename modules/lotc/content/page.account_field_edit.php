<?php 
/**
 * Shows a form for updating a lotc plan
 */
if (isAllowed("editVisits")) {
 
 	if (isset($pageVars->fieldid)) { 
		$field_order = array();
		if ($pageVars->fieldid == 0) {
			$max_order = $fldcntDb->cnt+1;
		} else {
			$max_order = $fldcntDb->cnt;
		}			
		for ($i=1; $i<=$max_order; $i++) {
			$field_order[$i] = $i;
		}
    
    $fieldids = '1,2,3,4,5,6,7,8';
    if ($lotc_conf['finance_approval_field']) $fieldids.=',9';
    if ($lotc_conf['principal_approval_field']) $fieldids.=',10';
    if ($lotc_conf['deputy_principal_approval_field']) $fieldids.=',11';
    if ($lotc_conf['governor_approval_field']) $fieldids.=',12';
    
	 	$field_type_qry = "select field_typeid as a, type_title as b from field_type where state = 'ACTIVE' and field_typeid in($fieldids)";
    
	 	if ($pageDb->row['state'] == 'NEW') {
		 	$state = array("NEW" => "NEW", "ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	 	} else { 
			$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
		}
		echo startFormTable($_SERVER["PHP_SELF"], "", "", FIELD_FORM_TITLE, FIELD_FORM_INFO);
		echo frmHiddenField($pageVars->fieldid,"fieldid");
		echo frmHiddenField($pageVars->formid,"formid");
		echo frmHiddenField($pageVars->planid,"planid");
		echo frmHiddenField($pageVars->accountid,"accountid");
		echo frmTextField($pageDb->row,"fieldname",50,"Field name",true);
		echo frmTextArea($pageDb->row,"fieldtext","5","Field description",false);
		//echo frmTextArea($pageDb->row,"default_value","3","Default Value",false);
		echo frmCheckBox($pageDb->row, "required", "Tick if required");
		echo frmSelectQuery($pageDb->row,"field_typeid",$field_type_qry,$con,"Field type",true);
		if ((isset($_POST['field_typeid']) && $_POST['field_typeid'] == 8) || $pageDb->row['field_typeid'] == 8) {
			echo frmTextArea($pageDb->row,"params","5","Enter options as a comma seperated list",true);
		}
		echo frmSelectArray($pageDb->row, "field_order" , $field_order, "Order" , true)	;
		echo frmSelectArray($pageDb->row, "state" , $state, "State" , true)	;
		
		if ($pageVars->planid != 0 && isAllowed("editVisits")) {
			echo frmShowAudit($pageDb->row,$con);	
		}
		
		echo frmButtonHelp("Save","save", "Click here to save changes");
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
		if ($pageVars->planid != 0 && isAllowed("editVisits"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this field?", "Click here to delete this field");
		
		echo endFormTable();
	} 
}

?>