<?php 
/**
 * Shows a form for updating a lotc visit
 */
 
 /*this is the page that loads when you click on edit details on 
 a lotc form */
 
if (isAllowed("planVisits")) {
 
 	if (isset($pageVars->visitid)) { 
	 	//var_dump($pageVars);
	 	$plan_qry = "select distinct a_plan.planid as a, a_plan.planname as b from a_plan left join a_form on a_plan.planid = a_form.planid left join a_field on a_form.formid = a_field.formid where a_plan.state = 'ACTIVE' and a_form.state = 'ACTIVE' and a_field.state = 'ACTIVE' and a_plan.accountid = {$pageVars->accountid}";
	 	//echo $plan_qry;
		$leader_qry = "SELECT usr.userid, concat(usr.firstname, ' ',usr.lastname) as name FROM handsam.usr_usrtype uu join usr on uu.userid = usr.userid where uu.usertype = 'visit planner' and usr.accountid = {$pageVars->accountid};";
		echo startFormTable($_SERVER["PHP_SELF"], "", "", "Edit Visit", "");
		//echo formNotes(PLAN_FORM_TITLE, PLAN_FORM_INFO);
		echo frmHiddenField($pageVars->formid,"formid");
		if (isset($qs)) {
			echo frmHiddenField(htmlspecialchars(serialize($qs)),"filter");
	
		}
		echo frmHiddenField($pageVars->formid,"formid");
		echo frmHiddenField($pageVars->visitid,"visitid");
		echo frmHiddenField($pageVars->accountid,"accountid");
		if (isset($list)) echo frmHiddenField($list,"list");
		//$pageDb->row['leader']
		echo frmTextField($pageDb->row,"visitname",50,"Visit Title",true);
		if ($pageVars->visitid > 0){
		   echo frmSelectQuery($pageDb->row,"leaderid",$leader_qry,$con,"Visit Planner",true);
		} 
		
		
		echo frmTextArea($pageDb->row,"visittext","5","Visit Summary",false);
		
		//if (isset($visit_state) && $visit_state->cnt == 0 || (isset($visit_copy) && $visit_copy)) {
						
			echo frmTimeDateField('visit_date', $pageDb->row, 'visit_date',"Visit Start Time &amp; Date",false);
			//get start date and end date. if equal then daytrip checkbosx is ticked
			//if (date('dmy',strtotime($pageDb->row['visit_date'])) == date('dmy',strtotime($pageDb->row['visit_end']))) {
				//echo frmCheckBoxNonDb(true,"daytrip","Daytrip");
			//} else {
				echo frmCheckBoxNonDb(false,"daytrip","Daytrip");
			//}
			echo frmTimeDateField('visit_end', $pageDb->row, 'visit_end',"Visit End Time &amp; Date",false);
		//}
		
		//if (dataInVisit($con, $pageVars->visitid) || (isset($visit_copy) && $visit_copy)) {
		//	echo frmSelectQuery($pageDb->row,"planid",$plan_qry,$con,"LOtC Plan",true);
		//}
		if ($pageVars->visitid == 0) {
			echo frmSelectQuery($pageDb->row,"planid",$plan_qry,$con,"LOtC Plan",true);
		}
		
		/*year group field*/
		$year_qry = "select year_group_id from a_visit_year_group where visitid = ".$pageVars->visitid;
		$year_rs = getRS($con,$year_qry);
		$year_group_aid = 0;
		//first check if this account has its own custom yeargroup
		$qry = "select year_group_id from year_group where accountid={$clsUS->accountid}";
		$rs = getRS($con, $qry);
		if (getResultRows($rs) > 0) $year_group_aid = $clsUS->accountid;
		$qry = "select year_group_id as a, year_text as b from year_group where accountid = $year_group_aid ";
		echo frmMultiSelectQuery($year_rs,"year_group_id",$qry,$con,"Year Group",true);


		if (isset($visit_copy) && $visit_copy) {
			echo frmCheckBoxNonDb(false,"copy_uploads","Copy Uploaded Files");
			echo frmCheckBoxNonDb(false,"copy_comments","Copy Comments");
		}
		
		if ($pageVars->visitid != 0 && isAllowed("planVisits")) {
			echo frmShowAudit($pageDb->row,$con);	
		}
		
		echo frmButtonHelp("Save","save", "Click here to save changes");
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
		if ($pageVars->visitid != 0 && isAllowed("planVisits"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this visit?", "Click here to delete this visit");
		
		echo endFormTable();
	} 
}
?>