<?php
/***
** Shows the list of lotc plans
** 
***/

if ($pageDb->rsExists()) {
	
	
	
	echo displayParagraphs("<a class=\"add_visit\" href=\"$rootUrl/modules/lotc/index.php?accountid={$pageVars->accountid}&amp;visitid=0\">Add a new LOtc visit</a>");
	?>
	<table  id="visit_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Visit Title</th>
				<th class="">Plan</th>
				<th class="">Visit Date</th>
				<th class="">Current Stage</th>
				<th class="lotc_state">State</th>
				<th class="end">Copy Trip</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "$rootUrl/modules/lotc/mylotc.php?accountid={$pageVars->accountid}&amp;visitid={$row['visitid']}&amp;rand=".rand()."&amp;filter=".rawurlencode(serialize($qs));
		
		if ($row["latest_visit_state"] == "DECLINED EVC" || $row["latest_visit_state"] == "DECLINED FINANCE" || $row["latest_visit_state"] == "DECLINED PRINCIPAL" || $row["latest_visit_state"] == "DECLINED DEPUTY PRINCIPAL" || $row["latest_visit_state"] == "DECLINED GOVERNOR") {
			$highlight = "red";
			$form_state = "DECLINED";
		} else if($row["state"] == "PLANNING")  {
			$highlight = "amber";
			$form_state = "PLANNING";
		} else {
			$highlight = "";
			$form_state = $row["state"];
		}

		?>
		<tr>
			<td class="start"><a href="<?php echo $url;?>"><?php echo displayText($row["visitname"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayText($row["planname"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayDate($row["visit_date"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["stage"] ?></a></td>
			<td class="lotc_state"><a class="<?php echo $highlight ?>" href="<?php echo $url;?>"><?php echo $form_state ?></a></td>
			<td class="end"><a class="copyVisit" href=<?php echo "\"$rootUrl/modules/lotc/copy_visit.php?list=true&amp;visitid={$row['visitid']}&amp;accountid={$pageVars->accountid}&amp;userid={$pageVars->userid}&amp;filter=".rawurlencode(serialize($qs))."\"";?>>Copy Visit</a></td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no LOtC visits to view");
	echo displayParagraphs("<a class=\"add_visit\" href=\"$rootUrl/modules/lotc/index.php?accountid={$pageVars->accountid}&amp;visitid=0\">Add a new LOtc visit</a>");
}
