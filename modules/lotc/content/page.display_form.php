<?php
echo visitCrumbtrail($con, $pageDb->row['stage'], $pageVars, $qs, $crumb_back);
if ($pageDb->row['visit_date'] != "") {
	$visit_date = displayLotcDateTime($pageDb->row['visit_date']);
} else {
	$visit_date = "Not Set";
}


if ($pageDb->row['visit_end'] != "") {
	$visit_end = displayLotcDateTime($pageDb->row['visit_end']);
} else {
	$visit_end = "Not Set";
}

echo "<div class=\"visit_info\">";
//echo "this is page display form";
echo "<div class=\"clearfix\"><strong>Visit Title: </strong><span>".displayText($pageDb->row['visitname'])."</span></div><div class=\"clearfix\"><strong>Visit Planner: </strong><span>{$pageDb->row['leader']}</span></div>";
echo "<div class=\"clearfix year_groups\"><strong>Year Groups: </strong><span>".implode(",",$years_arr)."</span></div>";
echo "<div class=\"clearfix\"><strong>Visit Summary: </strong><span>".displayText($pageDb->row['visittext'])."</span></div><div class=\"clearfix narrow\"><strong>Visit Start Date: </strong><span>$visit_date</span><strong>Visit End Date: </strong><span>$visit_end</span></div>";

echo "</div>";
echo "<p class=\"visit_menu clearfix\"><a class=\"copy_visit\" href=\"$rootUrl/modules/lotc/copy_visit.php?visitid={$pageVars->visitid}&amp;formid={$pageDb->row['formid']}&amp;fileid=0&amp;accountid={$pageVars->accountid}&amp;userid={$pageVars->userid}&amp;filter=".rawurlencode(serialize($qs))."\">copy visit</a>";
//only display the visit edit link if we are on the first stage
//if ($pageDb->row['stage'] == $pageDb->row['min_stage']) {
//	echo "<a class=\"edit_visit\" href=\"?visitid={$pageVars->visitid}\">edit details</a>";
//} 
//new button to load page in a new window
if ($pageDb->row['stage'] == $pageDb->row['min_stage']) {
     echo "<a class=\"new_edit_visit\" href=\"$rootUrl/modules/lotc/a_edit_visit.php?visitid={$pageVars->visitid}&amp;formid={$pageDb->row['formid']}&amp;fileid=0&amp;accountid={$pageVars->accountid}&amp;userid={$pageVars->userid}&amp;filter=".rawurlencode(serialize($qs))."\">edit visit</a>";
}

$action =$_SERVER["PHP_SELF"]."?accountid={$pageVars->accountid}&amp;visitid={$pageVars->visitid}&amp;rand=".rand()."&amp;filter=".rawurlencode(serialize($qs));

echo startFormTable($action,"clearfix", "", $frmTitle, $frmText);
//echo formNotes(PLAN_FORM_TITLE, PLAN_FORM_INFO);

foreach ($formDB->rows as $row) {
	if (function_exists("display_form_field_".$row['field_typeid'])) {
		call_user_func_array("display_form_field_".$row['field_typeid'], array($row, $row['default_value']));
	}
}
$formid = $pageDb->row['formid'];
$_POST['formid'] = $formid;

echo frmHiddenField($pageVars->visitid,"visitid" );
echo frmHiddenField($pageVars->accountid,"accountid");
echo frmHiddenField(htmlspecialchars(serialize($qs_base)),"filter");


//display uploaded documents

echo "<div class=\"uploads frmSection\">";
echo "<h3>Uploaded Files</h3>";
	
//echo "<a class=\"upload_doc\" href=\"$rootUrl/modules/lotc/a_user_upload_doc.php?visitid={$pageVars->visitid}&amp;formid={$pageDb->row['formid']}&amp;fileid=0&amp;accountid={$pageVars->accountid}&amp;userid={$pageVars->userid}&amp;filter=".rawurlencode(serialize($qs))."\">[ upload file ]</a>";

echo "<input id=\"bt_upload_file\" name=\"bt_upload_file\" type=\"submit\" value=\"[ upload file]\"/>";

$uploads = getUploads($con, $pageVars, $pageDb->row['formid']);

if ($uploads->cnt > 0) {
	echo "<ul class=\"uploads_list clearfix\">";
	foreach ($uploads->rows as $row) {
		echo "<li><a class=\"".strtolower(substr(strrchr($row['filename'],'.'),1))."\" href=\"$rootUrl/modules/lotc/downloadLotcDocument.php?id={$row['fileid']}\">{$row['title']}</a><a class=\"edit_file\" href=\"$rootUrl/modules/lotc/a_user_upload_doc.php?visitid={$pageVars->visitid}&amp;formid={$pageDb->row['formid']}&amp;fileid={$row['fileid']}&amp;accountid={$pageVars->accountid}&amp;userid={$pageVars->userid}&amp;filter=".rawurlencode(serialize($qs))."\">[ edit file ]</a></li>";
	}
	echo "<ul>";
} else {
	echo "<p>No Uploaded Files</p>";
}

echo "</div>";

//display comments
echo "<div class=\"comments frmSection clearfix\">";
echo "<h3> Comments </h3>";
//add 'add comment' link if is allowed to add comments to this visit
if (is_allowed_add_comment($con, $pageVars, $pageDb->row['stage'])) {
	//echo "<a class=\"add_comment\" href=\"\">[ add comment ]</a><input type=\"hidden\" value=\"1\" name=\"edit_comment_0\"/>";
	echo "<input id=\"bt_add_comment\" name=\"bt_add_comment\" type=\"submit\" value=\"add comment\"/>";
}
$comments = getComments($con, $pageVars->visitid, $pageDb->row['formid']);
echo "<ul id=\"comment_list\">";
if ($comments->cnt > 0) {
	foreach ($comments->rows as $row) {
		echo "<li class=\"clearfix\">";
		echo "<strong>{$row['firstname']} {$row['lastname']} : <em> ".ucwords($row['usrtype'])."</em></strong> : <em>".displayLotcDateTime($row['created'])."</em>";
		//display edit button if comment is the users own
		if (is_allowed_edit_comment($con, $pageVars, $row['commentid'], $pageDb->row['stage'])) {
			echo " <a class=\"add_comment\" href=\"\">[ edit comment ]</a><input type=\"hidden\" value=\"{$row['commentid']}\" name=\"edit_comment_{$row['commentid']}\"/>";
		}
		
		echo displayParagraphs($row['commenttext']);
		echo "</li>";
	}
} else {
	echo "<li> No Comments </li>";
}

echo "</ul>";
echo "</div>";

echo frmButtonHelp("Save as Draft","save_draft", "Click here to save changes");
echo frmButtonHelp("Save &amp; Submit","save_submit", "Click here to submit for approval");
echo frmButtonHelp("Cancel","cancel", "Click here to cancel");

echo endFormTable();


?>