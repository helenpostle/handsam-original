<?php


echo visitCrumbtrail($con, $pageDb->row['stage'], $pageVars, $qs, $crumb_back);
if ($pageDb->row['visit_date'] != "") {
	$visit_date = displayLotcDateTime($pageDb->row['visit_date']);
} else {
	$visit_date = "Not Set";
}


if ($pageDb->row['visit_end'] != "") {
	$visit_end = displayLotcDateTime($pageDb->row['visit_end']);
} else {
	$visit_end = "Not Set";
}

echo "<div class=\"visit_info\">";
echo "this is page display form";
echo "<div class=\"clearfix\"><strong>Visit Title: </strong><span>".displayText($pageDb->row['visitname'])."</span></div><div class=\"clearfix\"><strong>Visit Planner: </strong><span>{$pageDb->row['leader']}</span></div>";
echo "<div class=\"clearfix year_groups\"><strong>Year Groups: </strong><span>".implode(",",$years_arr)."</span></div>";
echo "<div class=\"clearfix\"><strong>Visit Summary: </strong><span>".displayText($pageDb->row['visittext'])."</span></div><div class=\"clearfix narrow\"><strong>Visit Start Date: </strong><span>$visit_date</span><strong>Visit End Date: </strong><span>$visit_end</span></div>";

echo "</div>";
echo "<p class=\"visit_menu clearfix\"><a class=\"copy_visit\" href=\"$rootUrl/modules/lotc/copy_visit.php?visitid={$pageVars->visitid}&amp;formid={$pageDb->row['formid']}&amp;fileid=0&amp;accountid={$pageVars->accountid}&amp;userid={$pageVars->userid}&amp;filter=".rawurlencode(serialize($qs))."\">copy visit</a>";
//only display the visit edit link if we are on the first stage
if ($pageDb->row['stage'] == $pageDb->row['min_stage']) {
	echo "<a class=\"edit_visit\" href=\"?visitid={$pageVars->visitid}\">edit details</a>";
} 

echo "</p>";
$action =$_SERVER["PHP_SELF"]."?accountid={$pageVars->accountid}&amp;visitid={$pageVars->visitid}&amp;rand=".rand()."&amp;filter=".rawurlencode(serialize($qs));

?>