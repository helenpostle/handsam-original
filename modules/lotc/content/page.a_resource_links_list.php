<?php
/***
** Shows the list of account resource links
** 
***/

if ($pageDb->rsExists()) {
	?>
	<table  id="guidance_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Title</th>
				<th class="">Url</th>
				<th class="end">Date Added</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "?linkid=".$row['linkid'];
		?>
		<tr>
			<td class="start"><a href="<?php echo $url;?>"><?php echo displayText($row["title"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo textSummary($row["url"],80) ?></a></td>
			<td class="end"><a href="<?php echo $url;?>"><?php echo displayDate($row["created"]) ?></a></td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no account resource documents to view");
}
