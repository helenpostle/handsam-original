<?php


$qry = "select a_visit.*, concat(usr.firstname,' ',usr.lastname) as leader from a_visit left join usr on  a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid}";
$pageDb = new pageQry($con, $qry);
$pageDb->rsItem();
if ($pageDb->row['visit_date'] != "") {
                    $visit_date = displayLotcDateTime($pageDb->row['visit_date']);
} else {
                    $visit_date = "Not Set";
}

                      if ($pageDb->row['visit_end'] != "") {
                          $visit_end = displayLotcDateTime($pageDb->row['visit_end']);
                      } else {
                          $visit_end = "Not Set";
                      }
                      //get year groups
                      $year_qry = "select year_group.year_text from year_group left join a_visit_year_group on year_group.year_group_id = a_visit_year_group.year_group_id where a_visit_year_group.visitid={$pageVars->visitid}";
                      $yearsRs = getRS($con, $year_qry);
                      $years_arr = array();
                      while ($year_row = getRow($yearsRs)) {
                          $years_arr[] = $year_row['year_text'];
                      }
$leader_qry = "SELECT usr.userid, concat(usr.firstname, ' ',usr.lastname) as name FROM handsam.usr_usrtype uu join usr on uu.userid = usr.userid where uu.usertype = 'visit planner' and usr.accountid = {$pageVars->accountid};";
		
//echo "<div id=\"form_body\">";
echo startFormTable($_SERVER["PHP_SELF"], "", "comment");
echo frmTextField($pageDb->row,"visitname",50,"Visit Title",true, 'inputText');
echo frmSelectQuery($pageDb->row,"leaderid",$leader_qry,$con,"Visit Planner",true);
echo frmTextArea($pageDb->row,"visittext","5","Visit Summary",false);
echo frmTimeDateField('visit_date', $pageDb->row, 'visit_date',"Visit Start Time &amp; Date",false);
echo frmCheckBoxNonDb(false,"daytrip","Daytrip");
echo frmTimeDateField('visit_end', $pageDb->row, 'visit_end',"Visit End Time &amp; Date",false);
//hidden fields
echo frmHiddenField($pageVars->formid,"formid");
echo frmHiddenField($pageVars->visitid,"visitid");
echo frmHiddenField($pageVars->accountid,"accountid");
//ECHO "tHE visit id is ".$pageVars->visitid;
		
$year_qry = "select year_group_id from a_visit_year_group where visitid = ".$pageVars->visitid;
		$year_rs = getRS($con,$year_qry);
		$year_group_aid = 0;
		//first check if this account has its own custom yeargroup
		$qry = "select year_group_id from year_group where accountid={$clsUS->accountid}";
		$rs = getRS($con, $qry);
		if (getResultRows($rs) > 0) $year_group_aid = $clsUS->accountid;
		$qry = "select year_group_id as a, year_text as b from year_group where accountid = $year_group_aid ";
		echo frmMultiSelectQuery($year_rs,"year_group_id",$qry,$con,"Year Group",true);

		
		//add the buttons
		
		echo frmButtonHelp("Save","save", "Click here to save changes");
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
		
		echo endFormTable();
//echo "</div>";   //end of form body
?>