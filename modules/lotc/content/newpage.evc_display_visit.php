<?php
//echo visitCrumbtrail($con, $pageDb->row['stage'], $pageVars, $qs, $crumb_back);
error_reporting(E_ALL);
ini_set('display_errors', '1');
echo visitCrumbtrail($con, $pageDb->row['stage'], $pageVars, $qs, $crumb_back);

if ($pageDb->row['visit_date'] != "") {
	$visit_date = displayLotcDateTime($pageDb->row['visit_date']);
} else {
	$visit_date = "Not Set";
}

if (isset($pageDb->row['visit_end']) && $pageDb->row['visit_end'] != "") {
	$visit_end = displayLotcDateTime($pageDb->row['visit_end']);
} else {
	$visit_end = "Not Set";
}
//echo var_dump($pageDb);
echo "<div class=\"visit_info\">";
//ECHO "<div class=\"clearfix\">Stage: ".displayText($pageDb->row['stage']).</div>
echo "<div class=\"clearfix\"><strong>Plan Stage: </strong><span>".displayText($pageDb->row['stage'])."</span></div>";
echo "<div class=\"clearfix\"><strong>Form ID: </strong><span>{$pageDb->row['formid']}</span></div>";
echo "<div class=\"clearfix\"><strong>Visit Title: </strong><span>".displayText($pageDb->row['visitname'])."</span></div><div class=\"clearfix\"><strong>Visit Planner: </strong><span>{$pageDb->row['leader']}</span></div>";
echo "<div class=\"clearfix year_groups\"><strong>Year Groups: </strong><span>".implode(",",$years_arr)."</span></div>";
echo "<div class=\"clearfix\"><strong>Visit Summary: </strong><span>".displayText($pageDb->row['visittext'])."</span></div><div class=\"clearfix narrow\"><strong>Visit Start Date: </strong><span>$visit_date</span><strong>Visit End Date: </strong><span>$visit_end</span></div>";
//if they are allowed to plan visits show the copy button
if (isAllowed("planVisits")) {
	echo "</div>";
	echo "<p class=\"visit_menu clearfix\"><a class=\"copy_visit\" href=\"$rootUrl/modules/lotc/copy_visit.php?visitid={$pageVars->visitid}&amp;formid={$pageDb->row['formid']}&amp;fileid=0&amp;accountid={$pageVars->accountid}&amp;userid={$pageVars->userid}&amp;filter=".rawurlencode(serialize($qs))."\">copy visit</a>";
	//as long as the state is not planning, allow editing
	if ($pageDb->row['state'] == 'PLANNING') echo "<span class=\"edit_visit\">edit details</span><span>To edit the above visit details you must first 'unsubmit' this form using the button at the foot of the page.</span></p>";
} else {
	echo "</div><p class=\"visit_menu clearfix\"></p>";
}


echo "<div id=\"form_header\">";
echo "<h2>$frmTitle</h2> this is the form name got using the form id from the sql for this visit";
echo displayParagraphs($frmText);
echo "</div>";

//now display the fields for this stage
echo "<div id=\"form_body\" class=\"clearfix\">";
//echo var_dump($formDB);
foreach ($formDB->rows as $row) {
	if (function_exists("display_visit_field_".$row['field_typeid'])) {
		call_user_func_array("display_visit_field_".$row['field_typeid'], array($row, $row['default_value']));
	}
}

//display uploaded documents

echo "<div class=\"uploads frmSection clearfix\">";
echo "<h3>Uploaded Files</h3>";
echo startSimpleForm($_SERVER["PHP_SELF"],"simple evc clearfix");
echo "<input id=\"bt_upload_file\" name=\"bt_upload_file\" type=\"submit\" value=\"[ upload file]\"/>";
echo frmHiddenField($pageVars->visitid,"visitid");
echo frmHiddenField($pageDb->row['formid'],"formid");
echo frmHiddenField(htmlspecialchars(serialize($qs_base)),"filter");
echo endFormTable2();
$uploads = getUploads($con, $pageVars, $pageDb->row['formid']);

if ($uploads->cnt > 0) {
	echo "<ul class=\"uploads_list\">";
	foreach ($uploads->rows as $row) {
		echo "<li><a class=\"".strtolower(substr(strrchr($row['filename'],'.'),1))."\" href=\"$rootUrl/modules/lotc/downloadLotcDocument.php?id={$row['fileid']}\">{$row['title']}</a><a class=\"edit_file\" href=\"$rootUrl/modules/lotc/a_evc_upload_doc.php?stage={$pageVars->stage}&visitid={$pageVars->visitid}&amp;formid={$pageDb->row['formid']}&amp;fileid={$row['fileid']}&amp;accountid={$pageVars->accountid}&amp;userid={$pageVars->userid}&amp;filter=".rawurlencode(serialize($qs))."\">[ edit file ]</a></li>";
	}
	echo "<ul>";
} else {
	echo "<p>No Uploaded Files</p>";
}

echo "</div>";

//display comments
echo "<div class=\"comments frmSection clearfix\">";
echo "<h3> Comments </h3>";
//add 'add comment' link if is allowed to add comments to this visit
if (is_allowed_add_comment($con, $pageVars, $pageDb->row['stage'])) {
	echo "<a class=\"add_comment\" href=\"\">[ add comment ]</a><input type=\"hidden\" value=\"0\" name=\"edit_comment_0\"/>";
}

$comments = getComments($con, $pageVars->visitid, $pageDb->row['formid'], $pageDb->row['stage']);
echo "<ul id=\"comment_list\">";

if ($comments->cnt > 0) {
	foreach ($comments->rows as $row) {
		echo "<li class=\"clearfix\">";
		echo "<strong>{$row['firstname']} {$row['lastname']} : <em> ".ucwords($row['usrtype'])."</em></strong> : <em>".displayLotcDateTime($row['created'])."</em>";
		//display edit button if comment is the users own
		if (is_allowed_edit_comment($con, $pageVars, $row['commentid'], $pageDb->row['stage'])) {
			echo " <a class=\"add_comment\" href=\"\">[ edit comment ]</a><input type=\"hidden\" value=\"{$row['commentid']}\" name=\"edit_comment_{$row['commentid']}\"/>";
		}
		
		echo displayParagraphs($row['commenttext']);
		echo "</li>";
	}
} else {
	echo "<li> No Comments </li>";
}
echo "</ul>";

echo "</div>";

echo "</div>";

echo "<div id=\"form_footer\">";
echo "The state of this form is ".$form_state;
echo "Testing";
if (isAllowed("editVisits")){
  echo "<br />allowed to edit visits";
}
if (isAllowed("planVisits")){
   echo "<br />allowed to plan visits";
}
if (isAllowed("editVisits") && ($form_state == "SUBMITTED" || $form_state == "APPROVED FINANCE" || $form_state == "APPROVED PRINCIPAL"  || $form_state == "APPROVED DEPUTY PRINCIPAL" || $form_state == "APPROVED GOVERNOR") && ($pageDb->row['state']!='FINANCE APPROVAL' && $pageDb->row['state']!='PRINCIPAL APPROVAL' && $pageDb->row['state']!='DEPUTY PRINCIPAL APPROVAL' && $pageDb->row['state']!='GOVERNOR APPROVAL')) {
  echo "<br />state = SUBMITTED, APPROVED FINANCE, APPROVED PRINCIPAL, APPROVED DEPUTY PRINCIPAL, APPROVED GOVERNER, FINANCE APPROVAL, PRINCIPAL APPROVAL DEPUTY PRINCIPAL APPROVAL OR GOVERNER APPROVAL";
  echo startSimpleForm($_SERVER["PHP_SELF"],"simple evc clearfix");
  echo frmHiddenField($pageVars->visitid,"visitid");
	echo frmHiddenField($pageDb->row['formid'],"formid");
	echo frmHiddenField(htmlspecialchars(serialize($qs_base)),"filter");
	echo frmButtonHelp("Email Form","email","Click here to email this visit");
	echo frmButtonConfirmHelp("Approve Form","approve", "Are you sure you want to approve this form?","Click here to approve this form");
	echo frmButtonConfirmHelp("Decline Form","decline","Are you sure you want to decline this form?", "Click here to decline this form");
	echo frmButtonConfirmHelp("Delete Visit","delete","Are you sure you want to DELETE THIS ENTIRE VISIT?", "Click here to delete this visit");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	echo endFormTable2();
} else if (isAllowed("editVisits") && ($pageDb->row['state']!='FINANCE APPROVAL' && $pageDb->row['state']!='PRINCIPAL APPROVAL' && $pageDb->row['state']!='DEPUTY PRINCIPAL APPROVAL' && $pageDb->row['state']!='GOVERNOR APPROVAL')) {
 //  ECHO "<br />state is not equal to FINANCE APPROVAL, PRINCIPAL APPROVAL, DEPUTY PRINCIPAL APPROVAL, OR GOVERNER APPROVAL";
   echo startSimpleForm($_SERVER["PHP_SELF"],"evc clearfix");
	echo frmHiddenField($pageDb->row['formid'],"formid");
	echo frmHiddenField($pageVars->visitid,"visitid");
	echo frmHiddenField(htmlspecialchars(serialize($qs_base)),"filter");
	echo frmButtonConfirmHelp("Delete Visit","delete","Are you sure you want to DELETE THIS ENTIRE VISIT?", "Click here to delete this visit");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	echo endFormTable2();
}
if (isAllowed("planVisits")  && $form_state == "APPROVED EVC") {
   ECHO "<br />state = APPROVED EVC";
   echo "<br />starting the form for unsubmitting";
	echo startSimpleForm($_SERVER["PHP_SELF"],"evc clearfix");
	echo frmHiddenField($pageVars->visitid,"visitid");
	echo frmHiddenField($pageVars->stage,"stage");
	echo frmHiddenField($pageDb->row['formid'],"formid");
	echo frmHiddenField(htmlspecialchars(serialize($qs_base)),"filter");
	if ($pageDb->row['state'] != 'COMPLETED') {
		echo frmButtonConfirmHelp("Unsubmit this stage","unsubmit", "Are you sure you want to unsubmit this stage? If you do this, then this stage must be re-submitted and approved before the planning process can continue.","Click here to unsubmit this stage");
		echo endFormTable2();
	} ELSE {
	   echo "the state is completed so not showing unsubmit button";
	}
}