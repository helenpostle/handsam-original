<?php
/***
** Shows the list of lotc plans
** 
***/
echo "<ul class=\"clearfix\" id=\"crumbtrail\">";
echo "<li><a href=\"$rootUrl/modules/lotc/plan.php?accountid={$pageVars->accountid}\">Plans</a></li>";
//echo "<li><a href=\"$rootUrl/modules/lotc/form.php?accountid={$pageVars->accountid}&amp;planid={$pageVars->planid}\">{$titleDb->row['planname']} Forms</a></li>";
echo "<li>{$titleDb->row['planname']} Forms</li>";
echo "</ul>";

if ($pageDb->rsExists()) {
	
	echo displayParagraphs("<a href=\"?accountid={$pageVars->accountid}&amp;planid={$pageVars->planid}&amp;formid=0\">Add a new LOtc form</a>");
	?>
	<table  id="form_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Form Title</th>
				<th class="">Number of Fields</th>
				<th class="">Date Created</th>
				<th class="" colspan="2">Stage</th>
				<th class="">State</th>
				<th class="end">View Fields</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "?accountid={$pageVars->accountid}&amp;planid={$pageVars->planid}&amp;formid={$row['formid']}&amp;rand=".rand();
		?>
		<tr>
			<td class="start"><a href="<?php echo $url;?>"><?php echo displayText($row["formname"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["field_cnt"] ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayDate($row["created"]) ?></a></td>
			<td class="order_arrows"><a class="order_up" href="<?php echo "$rootUrl/modules/lotc/form.php?planid={$pageVars->planid}&amp;accountid={$pageVars->accountid}&amp;uid={$row['formid']}&amp;rand=".rand();?>">up</a><a href="<?php echo "$rootUrl/modules/lotc/form.php?planid={$pageVars->planid}&amp;accountid={$pageVars->accountid}&amp;did={$row['formid']}&amp;rand=".rand();?>" class="order_down">down</a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["stage"] ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["state"] ?></a></td>
			<td class="end"><a class="uline" href="<?php echo "$rootUrl/modules/lotc/field.php?accountid={$pageVars->accountid}&amp;formid={$row["formid"]}&amp;planid={$pageVars->planid}";?>">View Fields</a></td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no forms to view in this LOtC plan.");
	echo displayParagraphs("<a href=\"?accountid={$pageVars->accountid}&amp;planid={$pageVars->planid}&amp;formid=0\">Add a new LOtc form</a>");
}
