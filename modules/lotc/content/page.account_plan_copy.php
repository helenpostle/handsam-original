<?php 
/**
 * Shows a form for updating a lotc plan
 */
if (isAllowed("editVisits")) {
 
 	if (isset($pageVars->planid)) {
	 	if ($pageDb->row['state'] == 'NEW') {
		 	$state = array("NEW" => "NEW", "ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	 	} else { 
			$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
		}
		echo startFormTable($_SERVER["PHP_SELF"]);
		echo frmLegend('Edit Plan');
		echo formNotes(PLAN_FORM_TITLE, PLAN_FORM_INFO);
		echo frmHiddenField($pageVars->planid,"planid");
		echo frmHiddenField($pageVars->accountid,"accountid");
		echo frmTextField($pageDb->row,"planname",50,"Plan name",true);
		echo frmTextArea($pageDb->row,"plantext","5","Plan description",false);
		
		if ($pageVars->planid != 0 && isAllowed("editVisits")) {
			echo frmShowAudit($pageDb->row,$con);	
		}
		
		echo frmButtonHelp("Save","save", "Click here to save changes");
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
		
		echo endFormTable();
	} 
}

?>