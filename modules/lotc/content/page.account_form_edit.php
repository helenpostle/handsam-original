<?php 
/**
 * Shows a form for updating a lotc form
 */
if (isAllowed("editVisits")) {
 
 	if (isset($pageVars->planid)) { 
	 	if ($pageDb->row['state'] == 'NEW') {
		 	$state = array("NEW" => "NEW", "ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	 	} else { 
			$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
		}
		$stage = array();
		if ($pageVars->formid == 0) {
			$max_stage = $frmcntDb->cnt+1;
		} else {
			$max_stage = $frmcntDb->cnt;
		}			
		for ($i=1; $i<=$max_stage; $i++) {
			$stage[$i] = $i;
		}
		echo startFormTable($_SERVER["PHP_SELF"], "", "", FORM_FORM_TITLE, FORM_FORM_INFO);
		echo frmHiddenField($pageVars->planid,"planid");
		echo frmHiddenField($pageVars->formid,"formid");
		echo frmHiddenField($pageVars->accountid,"accountid");
		echo frmTextField($pageDb->row,"formname",50,"Form name",true);
		echo frmTextArea($pageDb->row,"formtext","5","Form description",false);
		if (!planUsed($pageVars, $con)) {
			echo frmSelectArray($pageDb->row, "stage" , $stage, "Stage" , true)	;
			echo frmSelectArray($pageDb->row, "state" , $state, "State" , true)	;
		}
		
		
		if ($pageVars->formid != 0 && isAllowed("editVisits")) {
			echo frmShowAudit($pageDb->row,$con);	
		}
		
		echo frmButtonHelp("Save","save", "Click here to save changes");
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
		if ($pageVars->formid != 0 && isAllowed("editVisits"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this form?", "Click here to delete this form");
		
		echo endFormTable();
	} 
}

?>