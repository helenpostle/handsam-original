<?php
/* * *
 * * Shows the list of lotc plans
 * * 
 * * */

if ($pageDb->rsExists()) {
    ?>
    <table  id="visit_list" class="acc_list">
        <caption class="hidden"><?php echo $title; ?></caption>
        <thead>
            <tr>
                <th class="start ">Visit Title</th>
                <th class="">Plan</th>
                <th class="">Date Created</th>
                <th class="">Visit Date</th>
                <th class="">Current Stage</th>
                <th class="end lotc_approval_state">State</th>
            </tr>
        </thead>
        <tbody>

            <?php
            foreach ($pageDb->rows as $row) {
                $url = "$rootUrl/modules/lotc/$approval_page.php?accountid={$pageVars->accountid}&amp;visitid={$row['visitid']}&amp;rand=" . rand() . "&amp;filter=" . rawurlencode(serialize($qs));

                if ($row["latest_visit_state"] == "DECLINED EVC") {
                    $highlight = "red";
                    $form_state = "DECLINED";
                } else if ($approval_page == 'principal' && isAllowed("principalApproval") && $row["state"] == "PRINCIPAL APPROVAL") {
                    $highlight = "amber";
                    $form_state = "PRINCIPAL APPROVAL";
                } else if ($approval_page == 'finance' && isAllowed("financeApproval") && $row["state"] == "FINANCE APPROVAL") {
                    $highlight = "amber";
                    $form_state = "FINANCE APPROVAL";
                } else if ($approval_page == 'deputy_principal' && isAllowed("deputyPrincipalApproval") && $row["state"] == "DEPUTY PRINCIPAL APPROVAL") {
                    $highlight = "amber";
                    $form_state = "DEPUTY PRINCIPAL APPROVAL";
                } else if ($approval_page == 'governor' && isAllowed("governorApproval") && $row["state"] == "GOVERNOR APPROVAL") {
                    $highlight = "amber";
                    $form_state = "GOVERNOR APPROVAL";
                } else {
                    $highlight = "";
                    $form_state = $row["state"];
                }
                ?>
                <tr>
                    <td class="start"><a href="<?php echo $url; ?>"><?php echo displayText($row["visitname"]) ?></a></td>
                    <td class=""><a href="<?php echo $url; ?>"><?php echo displayText($row["planname"]) ?></a></td>
                    <td class=""><a href="<?php echo $url; ?>"><?php echo displayDate($row["created"]) ?></a></td>
                    <td class=""><a href="<?php echo $url; ?>"><?php echo displayDate($row["visit_date"]) ?></a></td>
        <?php /*
          <td class=""><a href="<?php echo $url;?>"><?php echo $row["stages"] ?></a></td>
         */ ?>
                    <td class=""><a href="<?php echo $url; ?>"><?php echo $row["stage"] ?></a></td>
                    <td class="end lotc_state "><a  class="<?php echo $highlight ?>" href="<?php echo $url; ?>"><?php echo $form_state ?></a></td>
                </tr>
                    <?php }
                ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="start"></td>
                <td ></td>
                <td ></td>
                <td ></td>
                <td ></td>
                <td class="end"></td>
            </tr>
        </tfoot>

    </table>
    <?php
} else {
    echo displayParagraphs("There are no LOtC visits to view");
}
