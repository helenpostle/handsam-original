<?php
/***
** Shows the list of lotc plans
** 
***/

if ($pageDb->rsExists()) {
?>

<table  id="visit_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Visit Title</th>
				<th class="">Plan</th>
				<th class="">Date Created</th>
				<th class="">Visit Date</th>
				<th class="">Current Stage</th>
				<th class="end lotc_state">State</th>
			</tr>
		</thead>
		<tbody>

		<?php
foreach ($pageDb->rows as $row) {
    
	$url = "$rootUrl/modules/lotc/newevc.php?accountid={$pageVars->accountid}&amp;visitid={$row['visitid']}&amp;rand=".rand()."&amp;filter=".rawurlencode(serialize($qs));
    if ($row["latest_visit_state"] == "DECLINED EVC" || $row["latest_visit_state"] == "DECLINED FINANCE" || $row["latest_visit_state"] == "DECLINED PRINCIPAL") {
			$highlight = "red";
			$form_state = "DECLINED";
		} else if($row["state"] == "APPROVAL")  {
			$highlight = "amber";
			$form_state = "APPROVAL";
		} else {
			$highlight = "";
			$form_state = $row["state"];
		}

   ?>
   <tr>
			<td class="start"><a href="<?php echo $url;?>"><?php echo displayText($row["visitname"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayText($row["planname"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayDate($row["created"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayDate($row["visit_date"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["stage"] ?></a></td>
			<td class="end lotc_state "><a  class="<?php echo $highlight ?>" href="<?php echo $url;?>"><?php echo $form_state ?></a></td>
	</tr>
   <?php

}

?>
</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
}else {
   echo displayParagraphs("There are no LOtC visits to view");
}