<?php 
/**
 * Shows a form for updating a lotc visit
 */
if (isAllowed("editVisits")) {
 
 	if (isset($pageVars->visitid)) { 
	 	
		echo startFormTable($_SERVER["PHP_SELF"], "", "","Email Visit", "");
		echo frmHiddenField($pageVars->formid,"formid");
		if (isset($qs)) {
			echo frmHiddenField(htmlspecialchars(serialize($qs)),"filter");
	
		}
		echo frmHiddenField($pageVars->formid,"formid");
		echo frmHiddenField($pageVars->visitid,"visitid");
		echo frmHiddenField($pageVars->accountid,"accountid");
		echo frmTextFieldNonDb("email_addr",250,"Email Address",true, '', 'off');
		echo frmTextFieldNonDb("email_addr_title",250,"Email Address Name (for your reference)",false);
		echo frmTextFieldNonDb("email_title",250,"Email Title",true);
		echo frmTextAreaNonDb("email_message","5","Email Message",true);
		
		echo frmCheckBoxNonDb(false,"email_uploads","Include Uploaded Files");
		echo frmCheckBoxNonDb(false,"email_comments","Include Comments");

		
		
		echo frmButtonHelp("Send Email","send", "Click here to save changes");
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
		
		echo endFormTable();
	} 
}
?>