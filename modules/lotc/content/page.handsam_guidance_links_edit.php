<?php
/**
 * Shows a  form for adding a link
 * 
 */

if (isAllowed("editMasterPlan")) {
			
echo startFileFormTable($_SERVER["PHP_SELF"], "", LINKS_FORM_TITLE, LINKS_FORM_INFO);
echo frmHiddenField($pageVars->linkid,"linkid");
echo frmHiddenField($pageVars->accountid,"accountid");
echo frmSelectArray($pageDb->row, "linkcategory" , $docCategory, "Category" , true);	

echo frmTextField($pageDb->row,"url",200,"Url",true);
echo frmTextField($pageDb->row,"title",100,"Title",true);
echo "<span class=\"goToUrl\" ><a href=\"{$pageDb->row['url']}\" target=\"_blank\">Go to URL in a new window</a></span>";
echo frmShowAudit($pageDb->row,$con);	
echo frmButtonHelp("Save","save", "Click here to save changes");
echo frmButtonHelp("Cancel","cancel", "Click here to cancel upload");
if ($pageVars->linkid != 0 && $pageDb->row['cnt'] == 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this link?", "Click here to delete this link");
	
echo endFormTable();
	
}
?>