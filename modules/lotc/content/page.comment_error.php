<?php 
/**
 * Shows a form for updating a lotc comment
 */
 
	if ($pageVars->visitid == null){
	   echo "There is no visit id set, cannot find the visit";
	}
	if ($pageVars->formid == null){
	   echo "There is no form id set, cannot process";
	}
	if (!is_allowed_add_comment($con, $pageVars, $stageDb->row['stage'])){
	   echo "You are not allowed to add comments to this stage of the visit";
	}
?>