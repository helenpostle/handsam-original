<?php 
/**
 * Shows a form for updating a lotc comment
 */
 
if (isAllowed("planVisits") || isAllowed("editVisits")|| isAllowed("principalApproval")  || isAllowed("deputyPrincipalApproval")  || isAllowed("financeApproval")) {
 
 	if (isset($pageVars->visitid)) { 
	 	
		echo startFormTable($_SERVER["PHP_SELF"], "", "comment");
		echo frmLegend('Edit Visit');
		//echo formNotes(PLAN_FORM_TITLE, PLAN_FORM_INFO);
		echo frmHiddenField($pageVars->userid,"userid");
		echo frmHiddenField($pageVars->formid,"formid");
		echo frmHiddenField($pageVars->visitid,"visitid");
		echo frmHiddenField($pageVars->accountid,"accountid");
		echo frmHiddenField($pageVars->commentid,"commentid");
		echo frmTextArea($pageDb->row,"commenttext","5","Comment",true);
				
		if ($pageVars->commentid != 0 && isAllowed("planVisits")) {
			echo frmShowAudit($pageDb->row,$con);	
		}
		
		echo frmButtonHelp("Save","save", "Click here to save changes");
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
		//if ($pageVars->visitid != 0 && isAllowed("planVisits"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this visit?", "Click here to delete this visit");
		echo frmHiddenField($tkn,"tkn");
		
		echo endFormTable();
	} 
}
?>