<?php
/***
** Shows the list of lotc plans
** 
***/

if ($pageDb->rsExists()) {
	echo displayParagraphs("<a href=\"?accountid={$pageVars->accountid}&amp;planid=0\">Add a new LOtc plan</a>");
	?>
	<table  id="plan_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Plan Title</th>
				<th class="">Number of Forms</th>
				<th class="">Date Created</th>
				<th class="">State</th>
				<th class="">View Forms</th>
				<th class="end">Copy Plan</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "?accountid={$pageVars->accountid}&amp;planid={$row['planid']}&amp;rand=".rand();
		?>
		<tr>
			<td class="start"><a href="<?php echo $url;?>"><?php echo displayText($row["planname"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["form_cnt"] ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayDate($row["created"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["state"] ?></a></td>
			<td class=""><a class="td_link" href="<?php echo "$rootUrl/modules/lotc/a_form.php?accountid={$pageVars->accountid}&amp;planid={$row["planid"]}";?>">View Forms</a></td>
			<td class="end"><a class="td_link" href="<?php echo "$rootUrl/modules/lotc/a_copy_plan.php?accountid={$pageVars->accountid}&amp;planid={$row["planid"]}";?>">Copy Plan</a></td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no LOtC plans to view");
	echo displayParagraphs("<a href=\"?accountid={$pageVars->accountid}&amp;planid=0\">Add a new LOtc plan</a>");
}
