<?php

if ($pageDb->row['visit_date'] != "") {
	$visit_date = displayLotcDateTime($pageDb->row['visit_date']);
} else {
	$visit_date = "Not Set";
}
//echo "<div class=\"visit_info\">";
//echo displayParagraphs("<strong>Visit Date: </strong>$visit_date<br/><strong>Visit Title: </strong>{$pageDb->row['visitname']}<br/><strong>Visit Planner: </strong>{$pageDb->row['leader']}<br/><strong>Visit Summary: </strong>{$pageDb->row['visittext']} ","visit_description");
//echo "</div>";

echo "<div class=\"visit_info\">";
echo "page.display_visit_email";
echo "<div class=\"clearfix\"><strong>Visit Title: </strong><span>".displayText($pageDb->row['visitname'])."</span></div><div class=\"clearfix\"><strong>Visit Planner: </strong><span>{$pageDb->row['leader']}</span></div><div class=\"clearfix\"><strong>Visit Summary: </strong><span>".displayText($pageDb->row['visittext'])."</span></div><div class=\"clearfix narrow\"><strong>Visit Start Date: </strong><span>$visit_date</span><strong>Visit End Date: </strong><span>$visit_end</span></div>";
echo "</div><p class=\"visit_menu clearfix\"></p>";


echo "<div id=\"form_header\">";
echo "<h2>$frmTitle</h2>";
echo displayParagraphs($frmText);
echo "</div>";
echo "<div id=\"form_body\" class=\"clearfix\">";
foreach ($formDB->rows as $row) {
	if (function_exists("display_visit_field_".$row['field_typeid'])) {
		call_user_func_array("display_visit_field_".$row['field_typeid'], array($row, $row['default_value']));
	}
}


//display uploaded documents
if ($email_uploads == 1) {

	echo "<div class=\"uploads frmSection clearfix\">";
	echo "<h3>Uploaded Files</h3>";
		
	$sql_uploads = "SELECT af.fileid,f.filename,  f.title, '' AS url, f.filecategory as category, 'file' AS resource FROM a_lotc_files af LEFT JOIN files f ON f.fileid = af.fileid WHERE af.accountid = {$pageVars->accountid} and filetype = 'accountLotcUserDoc' and visitid = $visitid and formid = $formid order by title asc";					
	
	//echo $sql;
	$uploads = new  pageQry($con, $sql_uploads);
	$uploads->rsList();
	
	if ($uploads->cnt > 0) {
		echo "<ul class=\"uploads_list\">";
		foreach ($uploads->rows as $row) {
			echo "<li><a class=\"".strtolower(substr(strrchr($row['filename'],'.'),1))."\" href=\"#\">{$row['title']}</a></li>";
		}
		echo "<ul>";
	} else {
		echo "<p>No Uploaded Files</p>";
	}
	
	echo "</div>";
}

if ($email_comments == 1) {
//display comments
	echo "<div class=\"comments frmSection clearfix\">";
	echo "<h3> Comments </h3>";
	//add 'add comment' link if is allowed to add comments to this visit
	
	$comments = getComments($con, $visitid, $pageDb->row['formid'], $pageDb->row['stage']);
	echo "<ul id=\"comment_list\">";
	
	if ($comments->cnt > 0) {
		foreach ($comments->rows as $row) {
			echo "<li class=\"clearfix\">";
			echo "<strong>{$row['firstname']} {$row['lastname']} : <em> ".ucwords($row['usrtype'])."</em></strong> : <em>".displayLotcDateTime($row['created'])."</em>";
			
			echo displayParagraphs($row['commenttext']);
			echo "</li>";
		}
	} else {
		echo "<li> No Comments </li>";
	}
	echo "</ul>";
	
	echo "</div>";
}

echo "</div>";

echo "<div id=\"form_footer\">";
echo "</div>";
?>