<?php
/***
** Shows the list of lotc plans
** 
***/

if ($pageDb->rsExists()) {
	?>
	<table  id="guidance_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Title</th>
				<th class="">Url</th>
				<th class="">Date Attached</th>
				<th class="end">Remove</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = $row['url'];
		$action = "<a class=\"rm_link\" href=\"handsam_guidance_links.php?action=rm&amp;linkid={$row['linkid']}&amp;ret=lotc_guidance_links.php\">Remove from LOtC</a>";
		?>
		<tr>
			<td class="start"><a href="<?php echo $url;?>"><?php echo displayText($row["title"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo textSummary($row["url"],80) ?></a></td>
			<td class="uploaded"><a href="<?php echo $url;?>"><?php echo displayDate($row["created"]) ?></a></td>
			<td class="end action"><?php echo $action;?></td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no LOtC guidance links to view");
}
