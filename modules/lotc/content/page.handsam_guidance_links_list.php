<?php
/***
** Shows the list of handsam guidance links
** 
***/

if ($pageDb->rsExists()) {
	?>
	<table  id="guidance_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Title</th>
				<th class="">Url</th>
				<th class="uploaded">Added</th>
				<th class="end action">LOtC Action</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "?linkid={$row['linkid']}&amp;rand=".rand();
		if ($row['added'] == 1) {
			$action_link = "<a class=\"rm_link \" href=\"?action=rm&amp;linkid={$row['linkid']}\">Remove</a>";
			$row_class = "highlight";
		} else {
			$action_link = "<a class=\"add_link\"href=\"?action=add&amp;linkid={$row['linkid']}\">Add</a>";
			$row_class = "";
		}
		?>
		<tr class="<?php echo $row_class;?>">
			<td class="start"><a href="<?php echo $url;?>"><?php echo displayText($row["title"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo textSummary($row["url"],80) ?></a></td>
			<td class="uploaded"><a href="<?php echo $url;?>"><?php echo displayDate($row["created"]) ?></a></td>
			<td class="end action"><?php echo $action_link;?></td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no LOtC guidance links to view");
}
