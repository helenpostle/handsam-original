<?php
/***
** Shows the list of handsam guidance documents
** 
***/

if ($pageDb->rsExists()) {
	//echo displayParagraphs("<a href=\"?accountid={$pageVars->accountid}&amp;planid=0\">Add a new LOtc plan</a>");
	?>
	<table  id="guidance_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Title</th>
				<th class="">Filename</th>
				<th class="uploaded">Uploaded</th>
				<th class="end action">LOtC Action</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "?fileid={$row['fileid']}&amp;rand=".rand();
		if ($row['added'] == 1) {
			$action_link = "<a class=\"rm_link \" href=\"?action=rm&amp;fileid={$row['fileid']}\">Remove</a>";
			$row_class = "highlight";
		} else {
			$action_link = "<a class=\"add_link\"href=\"?action=add&amp;fileid={$row['fileid']}\">Add</a>";
			$row_class = "";
		}
		?>
		<tr class="<?php echo $row_class;?>">
			<td class="start"><a href="<?php echo $url;?>"><?php echo displayText($row["title"]) ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["filename"] ?></a></td>
			<td class="uploaded"><a href="<?php echo $url;?>"><?php echo displayDate($row["created"]) ?></a></td>
			<td class="end action"><?php echo $action_link;?></td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no LOtC guidance documents to view");
}
