<?php
/***
** Shows the list of lotc plans
** 
***/
echo "<ul class=\"clearfix\" id=\"crumbtrail\">";
echo "<li><a href=\"$rootUrl/modules/lotc/plan.php?accountid={$pageVars->accountid}\">Plans</a></li>";
echo "<li><a href=\"$rootUrl/modules/lotc/form.php?accountid={$pageVars->accountid}&amp;planid={$pageVars->planid}\">{$titleDb->row['planname']} Forms</a></li>";
echo "<li>{$titleDb->row['formname']} Fields</li>";
echo "</ul>";

if ($pageDb->rsExists()) {
	echo displayParagraphs("<a href=\"?accountid={$pageVars->accountid}&amp;planid={$pageVars->planid}&amp;formid={$pageVars->formid}&amp;fieldid=0\">Add a new LOtc field to this form</a>");
	?>
	<table  id="field_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start">Field Name</th>
				<th class="">Field Type</th>
				<th class="" colspan="2">Order</th>
				<th class="">State</th>
				<th class="end">Date Created</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "?accountid={$pageVars->accountid}&amp;planid={$pageVars->planid}&amp;formid={$pageVars->formid}&amp;fieldid={$row['fieldid']}&amp;rand=".rand();
		?>
		<tr>
			<td class="start"><a href="<?php echo $url;?>"><?php echo $row["fieldname"] ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["type_title"] ?></a></td>
			<td class="order_arrows"><a class="order_up" href="<?php echo "$rootUrl/modules/lotc/field.php?formid={$pageVars->formid}&amp;planid={$pageVars->planid}&amp;accountid={$pageVars->accountid}&amp;uid={$row['fieldid']}&amp;rand=".rand();?>">up</a><a href="<?php echo "$rootUrl/modules/lotc/field.php?formid={$pageVars->formid}&amp;planid={$pageVars->planid}&amp;accountid={$pageVars->accountid}&amp;did={$row['fieldid']}&amp;rand=".rand();?>" class="order_down">down</a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["field_order"] ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo $row["state"] ?></a></td>
			<td class="end"><a href="<?php echo $url;?>"><?php echo displayDate($row["created"]) ?></a></td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no fields for this form ");
	echo displayParagraphs("<a href=\"?accountid={$pageVars->accountid}&amp;planid={$pageVars->planid}&amp;formid={$pageVars->formid}&amp;fieldid=0\">Add a new LOtc field to this form</a>");
}
