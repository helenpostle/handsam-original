<?php
/**
 * Shows a  form for uploading a file
 * 
 */

if (isAllowed("editMasterPlan")) {

	//check if this file is used in a task. if it is don't show delete button
	/*
	$qry_f = "select count() as cnt FROM files f left join task_files t on f.fileid = t.fileid  where f.fileid = $fileid group by f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by";

	$rsFiles = getRS($con,$qry_f);
	$rowFile = getRow($rsFiles);
	*/
				
	if (!isset($uploaded_filename) || $uploaded_filename == "") {
		$uploaded_filename = $pageDb->row['filename'];
	}
	
	echo startFileFormTable($_SERVER["PHP_SELF"], "",UPLOAD_FORM_TITLE, UPLOAD_FORM_INFO);
	echo frmHiddenField($pageVars->fileid,"fileid");
	echo frmHiddenField($pageVars->accountid,"accountid");
	echo frmHiddenField($uploaded_filename,"uploaded_filename");
	echo frmHiddenField(20000000,"MAX_FILE_SIZE");
	echo frmFileField("filename","Select File" ,true);
	if (isset($uploaded_filename) || $uploaded_filename != "") {
		echo "<div class=\"frm_row clearfix\"><span class=\"label\">Uploaded Filename</span><span class=\"value\"><div class=\"upload\"><a href=\"downloadDocument.php?id=".$pageVars->fileid."\" title=\"\">".$uploaded_filename."</a></div></span></div>";
	}
	
	
	echo frmHiddenField($uploaded_filename, 'uploaded_filename');
	echo frmSelectArray($pageDb->row, "filecategory" , $docCategory, "Category" , true);	
	
	echo frmTextField($pageDb->row,"title",100,"Title",true);
	
	echo frmShowAudit($pageDb->row,$con);	
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel upload");
	if ($pageVars->fileid != 0 && $pageDb->row['cnt'] == 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this file?", "Click here to delete this link");
		
	echo endFormTable();
	
}
?>