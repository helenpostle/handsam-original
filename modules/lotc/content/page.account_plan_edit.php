<?php 
/**
 * Shows a form for updating a lotc plan
 */
if (isAllowed("editVisits")) {
 
 	if (isset($pageVars->planid)) {
	 	if ($pageDb->row['state'] == 'NEW') {
		 	$state = array("NEW" => "NEW", "ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	 	} else { 
			$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
		}
		echo startFormTable($_SERVER["PHP_SELF"], "", "", PLAN_FORM_TITLE, PLAN_FORM_INFO);
		echo frmHiddenField($pageVars->accountid,"accountid");
		echo frmHiddenField($pageVars->planid,"planid");
		echo frmTextField($pageDb->row,"planname",50,"Plan name",true);
		echo frmTextArea($pageDb->row,"plantext","5","Plan description",false);
		echo frmSelectArray($pageDb->row, "state" , $state, "State" , true)	;
		
		if ($pageVars->planid != 0 && isAllowed("editVisits")) {
			echo frmShowAudit($pageDb->row,$con);	
		}
		
		echo frmButtonHelp("Save","save", "Click here to save changes");
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
		if ($pageVars->planid != 0 && isAllowed("editVisits"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this plan?", "Click here to delete this plan");
		
		echo endFormTable();
	} 
}

?>