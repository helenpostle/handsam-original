<?php

/**
* Plan editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
 
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_account_admin');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

if (isAllowed("editVisits")) {

	if ($pageVars->planid !== null) {
		//edit /create plan
		$title = "LOtC: Edit Plan";
		$content = "page.account_plan_edit.php";
		$qry = "select * from a_plan where planid = {$pageVars->planid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
	
		if (buttonClicked("save")) {
			
			#check that a plan doesn't exist with this name
			$rsCheck = getRS($con,"select * from a_plan where planname = '".mysql_real_escape_string($_POST['planname'])."' and planid != {$pageVars->planid} and accountid = {$pageVars->accountid}");

			if (getRow($rsCheck)) {
				$messages[] = PLAN_IN_USE;	
			} else {
				if ($pageVars->planid == 0) {
					$qry = new dbInsert("a_plan");
					$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
				} else {
					$qry = new dbUpdate("a_plan");
					$qry->setParam("planid",$pageVars->planid);
				}
			
				if (isset($qry)) {
					$qry->setReqStringVal("planname",$_POST["planname"],"plan name");
					$qry->setStringVal("plantext",$_POST["plantext"],"plan text");
					$qry->setReqStringVal("state",$_POST["state"],"state");
					
					$qry->setAudit($clsUS->userid);
					
					if ($qry->execute($con)) {
						if ($pageVars->planid == 0) {
							$pageVars->planid = $qry->getNewID();	
						}
						$messages[] = SAVED;
						//list plans
						$title = "LOtC: Plan List";
						$content = "page.account_plan_list.php";
						$pageDb = getPlanList($pageVars, $con); 
						$pageFiles->addModFunc("lotc_tables");
					} else {
						$messages[] = $qry->getError();	
					}
				}
			}
		} else if (buttonClicked("delete")) {
			
				$qry = new dbUpdate("a_plan");
				$qry->setParam("planid",$pageVars->planid);
				$qry->setReqStringVal("state","DELETED","State");
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#change content to list
					$messages[] = DELETED;
					//list plans
					$title = "LOtC: Plan List";
					$content = "page.account_plan_list.php";
					$pageDb = getPlanList($pageVars, $con); 
					$pageFiles->addModFunc("lotc_tables");
				}
		} else if (buttonClicked("cancel")) {
			//list plans
			$title = "LOtC: Plan List";
			$content = "page.account_plan_list.php";
			$pageDb = getPlanList($pageVars, $con); 
			$pageFiles->addModFunc("lotc_tables");
		}
	
				
	} else {
		//list plans
		$title = "LOtC: Plan List";
		$content = "page.account_plan_list.php";
		$box[] = "box.a_guidance.php";
		$pageDb = getPlanList($pageVars, $con);
		$pageFiles->addModFunc("lotc_tables");
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}

include("../../layout.php");
?>