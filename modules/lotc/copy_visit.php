<?php
/**
* Plan editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
 
$secure = true;
require("../../shared/startPage.php");
$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_form');
$pageFiles->addModFunc("lotc_daytrip_tickbox");
$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
$load = getStrFromRequest("load");
$list = getStrFromRequest("list");
$qs = array();

if (isAllowed("planVisits")) {

	//get filter array querystring
	
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;

	
	if ($pageVars->visitid !== null) {
		//for use in determining if the visit date can be edited still. If any stage has been confirmed, then the date cannot be edited
		$content = "page.visit_edit.php";
		$visit_copy = true;
		//edit /create visit
		$title = "LOtC: Copy old visit to a new visit";
		$original_visitid = $pageVars->visitid;
		$qry = "select a_visit.*  from a_visit where visitid = {$pageVars->visitid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
		if (buttonClicked("save")) {
			
			$visit_date = getStrFromRequest("visit_date");
		
			//date time so add hrs and mins to date field
			$hrs = $_POST["visit_date_hrs"];
			$hrs = sprintf("%02d", $hrs);
			$mins = $_POST["visit_date_mins"];
			$mins = sprintf("%02d", $mins);
			$visit_date = "$visit_date {$hrs}:{$mins}:00";		
				
			$qry_stage = "select MIN( CASE WHEN a_form.state = 'ACTIVE' THEN a_form.stage END ) AS stage";
			$qry_stage .= " from a_plan left join a_form on a_plan.planid = a_form.planid";
			$qry_stage .= " where a_plan.planid = {$pageDb->row['planid']}";
			$stageDb = new pageQry($con, $qry_stage);
			$stageDb->rsItem(); 
                        
			$qry = new dbInsert("a_visit");
			$qry->setReqNumberVal("stage",$stageDb->row['stage'],"planid");
			$qry->setReqStringVal("state","PLANNING","state");
			$qry->setReqNumberVal("planid",$pageDb->row['planid'],"planid");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
			$qry->setReqNumberVal("leaderid",$_POST['leaderid'],"leaderid");
			//$qry->setReqNumberVal("leaderid",$pageVars->leader,"leaderid");
		
			$qry->setReqStringVal("visitname",$_POST["visitname"],"visit name");
			$qry->setStringVal("visittext",$_POST["visittext"],"visit text");						
			$qry->setDateTimeVal("visit_date",$visit_date,"Visit tame and date");
			
			$qry->setReqNumberVal("parentid",$original_visitid,"parent id");
			
			$qry->setAudit($clsUS->userid);
			//echo $qry->getSql();
			//die();
			if ($qry->execute($con)) {
				$pageVars->visitid = $qry->getNewID();	
				$messages[] = SAVED;
				
				//now copy all data from original visit including uploaded documents if box ticked and  comments if box ticked
				
				for  ($i=1; $i<9; $i++) {
					$cp_qry = "insert into data_field_type_$i (visitid, fieldid, fieldname, field_typeid, data_$i) select (SELECT {$pageVars->visitid}) AS visitid, fieldid, fieldname, field_typeid, data_$i from data_field_type_$i where visitid = $original_visitid";
					$insert = execSQL($con, $cp_qry);
					if ($insert != "") {
						//$messages[] = $insert;
					} else {
						//$messages[] = "Copying Data Type $i";
					}
				}
				
				if (isset($_POST['copy_uploads'])) {
					$cp_qry = "insert into a_lotc_files (visitid, fileid, created, created_by, edited, edited_by, accountid, formid, state) select (SELECT {$pageVars->visitid}) AS visitid, fileid, created, created_by, edited, edited_by, accountid, formid, state from a_lotc_files where visitid = $original_visitid";
					$insert = execSQL($con, $cp_qry);
					
					//$messages[] = "Copy Uploads";
				}

				if (isset($_POST['copy_comments'])) {
					$cp_qry = "insert into a_comments (visitid, formid, created, created_by, edited, edited_by, commenttext, state) select (SELECT {$pageVars->visitid}) AS visitid, formid, created, created_by, edited, edited_by, commenttext, state from a_comments where visitid = $original_visitid";
					$insert = execSQL($con, $cp_qry);
					
					//$messages[] = "Copy Comments";
				}
				headerLocation("lotc.php?accountid=$pageVars->accountid&userid=$pageVars->userid&visitid=$pageVars->visitid", $messages);
				//list visits
			} else {
				$messages[] = $qry->getError();	
			}
		/*
		
		} else if (buttonClicked("delete")) {
				$qry = new dbUpdate("a_visit");
				$qry->setParam("visitid",$pageVars->visitid);
				$qry->setReqStringVal("state","DELETED","State");
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#change content to list
					$messages[] = DELETED;
					//list plans
					$title = "LOtC: Visit List";
					$content = "lotc/page.visit_list.php";
					$pageDb = getVisitList($pageVars, $con, $filter_qry);
					$box[] = "lotc/box.visit_list_filter.php"; 
				}
				*/
		} else if (buttonClicked("cancel")) {
			if ($list == "true") {
				headerLocation("index.php", $messages);
			} else {
				headerLocation("lotc.php?accountid=$pageVars->accountid&userid=$pageVars->userid&visitid=$pageVars->visitid", $messages);
			}
		}
	
				
	}

}  else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>