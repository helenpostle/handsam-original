<?php

/**
* Plan editing functionality for super admin
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
 
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_admin');
$pageFiles->addModFunc('lotc_add_links');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
$subpageof = "a_plan.php";

$to_file_path = $accountLotcDocs_fp;

if (isAllowed("editVisits")) {
	if ($pageVars->linkid !== null) {


		$title = "LOtC: Account Resource Edit Link";
		$content = "page.a_resource_links_edit.php";
		$box[] = "box.a_guidance.php";

		//get file data and count if this file is used in handsam tasks and lotc
		$qry = "select f.title, f.url, f.linkid, f.linkcategory, f.description, f.created, f.edited, f.created_by, f.edited_by";
		$qry .= " FROM links f where f.linkid = {$pageVars->linkid} ";
				
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		
		if (buttonClicked("save")) {
			$linkid = lotcResourceLink($clsUS, $pageVars, $con, "accountLotcDoc");
			if ($linkid) {
				//first delete any record with same fileid to avoid duplicates
				$qry = new dbDelete("a_lotc_links");
				$qry->setParam("linkid",$linkid);
				$qry->setParam("accountid",$pageVars->accountid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				}
				//now add the fileid to lotc_files
				$qry = new dbInsert("a_lotc_links");
				$qry->setReqNumberVal("linkid",$linkid,"linkid");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
				$qry->setAudit($clsUS->userid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					headerLocation("a_resource_links.php?rand=".rand(),$messages);
				}
			}
			
		} else if (buttonClicked("cancel")) {
			
			headerLocation("a_resource_links.php?rand=".rand(),$messages);
			
		} else if (buttonClicked("delete")) {
			if ($pageVars->linkid != 0 && $pageDb->row['cnt'] == 0) {
				$linkid = lotcDeleteLink($pageVars, $con, $to_file_path);
				if ($linkid) {
					//first delete any record with same fileid to avoid duplicates
					$qry = new dbDelete("a_lotc_links");
					$qry->setParam("linkid",$linkid);
					$qry->setParam("accountid",$pageVars->accountid);
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					} else {
						headerLocation("a_resource_links.php?rand=".rand(),$messages);
					}
				}

			}
			
		}
		
	} else {		
			
		//list guidance attached to lotc
		$title = "LOtC: Account Resource Links List";
		$content = "page.a_resource_links_list.php";
		$box[] = "box.a_guidance.php";
		$pageDb = getResourceLinksList($con, $pageVars->accountid);
		$pageFiles->addModFunc("lotc_tables");

	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>