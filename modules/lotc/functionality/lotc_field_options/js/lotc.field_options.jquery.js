$(document).ready(function(){
	$('#field_typeid').change(function(event){
		if ($(this).val() == 8) {
			if ($('#params').length > 0) {
				$('#params').parent().show();
			} else {
				/*add a text area after this for options*/
				addOptionsBox($(this).parent());
			}
		} else {
			if ($('#params').length > 0) {
				$('#params').parent().hide();
			}
		}
	});
		
	function addOptionsBox(elem) {
		$(elem).after("<div class='required'><label for='params'>Enter options as a comma seperated list</label><textarea class='field' rows='5' name='params' id='params'/></div>");		
	}
});