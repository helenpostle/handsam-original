<?php 
/**
 * lists lotc changes in form state 
 * 
 */
if (isAllowed("editVisits")) {
	lotcBoxTop("Email Visit to:");

	$email_qry = "select * from a_email_visit where accountid = {$pageVars->accountid}";
	$emailDb = new pageQry($con, $email_qry);
	$emailDb->rsList(); 
	if ($emailDb->cnt > 0) {
		echo "<ul id=\"emails\" class=\"lotc_box_list\">";
		foreach ($emailDb->rows as $row) {
			echo "<li><a class=\"email_addr\" href=\"#\" title=\"{$row['title']}\" >{$row['email']}</a><a title=\"delete email address\" class=\"delete_email\" href=\"$rootUrl/lotc/email_visit.php?accountid=$pageVars->accountid&amp;visitid={$pageVars->visitid}&amp;userid={$pageVars->userid}&amp;filter=".rawurlencode(serialize($qs))."&amp;emailid={$row['emailid']}\">[del]</a></li>";		
		}
		echo "<ul>";
	} else {
		echo "<p>No Email Addreses</p>";
	}
	lotcBoxBottom();
}
?>
	