<?php
$this->addCoreLibJs("jquery/plugins/jquery.rich-array.js");
$this->addModuleFuncJs("lotc.add_email.jquery.js");
$this->addModuleFuncJs("lotc.delete_email.jquery.js");
$this->addModuleLibPhp("funcLotcNotifications.php");
$this->addCoreLibPhp("funcError/funcLog.php");
$this->addCoreLibPhp("funcMail/funcMail.php");
$this->addCoreLibPhp("funcMail/clsEmail.php");
$this->addModuleBox("box.email_list.php");

?>