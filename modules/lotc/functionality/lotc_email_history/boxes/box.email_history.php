<?php 
/**
 * liste handsam resource docs for this account
 * 
 */
 
 
$qry = "SELECT * from a_email_visit_log where accountid = {$pageVars->accountid} and formid = {$pageDb->row['formid']} and visitid = {$pageVars->visitid}";


$logDb = new pageQry($con, $qry);
$logDb->rsList();

if ($logDb->cnt > 0) {

	lotcBoxTop("Visit emailed to:");
	
	echo "<ul id=\"email_log\" class=\"lotc_box_list clearfix\">";
	foreach ($logDb->rows as $row) {
		$uploads_sent = "No";
		$comments_sent = "No";
		if ($row['uploads'] == 1) $uploads_sent = "Yes";
		if ($row['comments'] == 1) $comments_sent = "Yes";
		echo "<li class=\"clearfix\"><a class=\"email_addr\" href=\"#\">{$row['email_addr']}<br/>".displayLotcDateTime($row['created'])." <span class=\"toggle\">[ more... ]</span></a><p><strong>Subject:</strong> {$row['subject']}<br/><strong>Message: </strong>{$row['message']}<br/><strong>Comments sent:</strong> $comments_sent<br/><strong>Uploads sent: </strong>$uploads_sent</p></li>";
	}
	echo "</ul>";
	lotcBoxBottom();
}

?>
	