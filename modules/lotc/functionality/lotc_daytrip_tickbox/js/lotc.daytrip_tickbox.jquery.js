$(document).ready(function(){
	//if the start date and end date are the same,. then check tick box and hide fields
	
	initDaytrip();	
	
	$('#daytrip').on('click', function() {
		if ($(this).is(':checked')) {
			$('#visit_end').fadeTo(500,0.5);
			$('#visit_end').parent().children('img').hide();
			setDaytrip();
		} else {
			$('#visit_end').fadeTo(500,1.0);
			$('#visit_end').parent().children('img').show();
		}
	});
	
	function isDaytrip() {
		if ($('#visit_end').attr('value') == $('#visit_date').attr('value')) return true;
		return false;
	}
	
	function setDaytrip() {
		var start_date = $('#visit_date').attr('value');
		$('#visit_end').attr('value', start_date)
	}
	
	function initDaytrip() {
		if (isDaytrip()) {
			$('#daytrip').attr('checked', true);
			$('#visit_end').fadeTo(500,0.5);
			$('#visit_end').parent().children('img').hide();
		}
	}
	
	$('#visit_date').live('change', function() {
        if($('#daytrip').prop('checked')) {
            setDaytrip();
        }
    });
});