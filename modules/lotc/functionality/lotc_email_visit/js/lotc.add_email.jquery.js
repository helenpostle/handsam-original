$(document).ready(function(){

	//define variable letters - letters typed in tags textarea
	var letters = "";
	
	//add the email suggestion box
	$('#email_addr').after('<ul id="email_list"></ul>');
	
	//get all item tags and put in an array
	var tags = getTags('#email_addr');
	//var tags_array = getTagsArray(tags);
	
	var tag_cloud_array = $('#emails li a.email_addr').get();
	
	/**
	 * returns string of tags from tags text box
	 */
	function getTags(id) {
		var tags = $(id).val();
		return $.trim(tags);
	}
	
	/**
	 * returns array of suggested tags from cloud_tags_array(array of of li html objects)
	 */

	function getSuggestedTags(arr,lets, tags) {
		var new_arr = [];
		jQuery.each(arr, function() {
			//need to get just the html bit from the object
			var this_tag = $(this).html();
			//make suggestions case insensitive by using tolowercase
			
			if (this_tag.substring(0,lets.length).toLowerCase() == lets.toLowerCase()) {
				//alert(this_tag.substring(0,lets.length).toLowerCase() +" == "+ lets.toLowerCase());
				new_arr.push(this_tag);
			} else {
				
			}
		});
		
		return new_arr.sort();
	}

	function formatListFromArray(arr) {
		var li = "";
		jQuery.each(arr, function() {
			li = li + "<li>" + this + "</li>";
		});
		return li;
		
	}
	
	//resets suggestion box and hides it
	function resetTagSuggestion() {
		$('#email_list').html('');
		$('#email_list').hide();

	}
		
				
	//on click a tag in tag cloud
	$('a.email_addr').click(function(event){
		var selected_tag = $(this).html();
		var selected_title = $(this).attr('title');
		$('#email_addr').val(selected_tag);
		$('#email_addr_title').val(selected_title);
		//hide suggestion box and reset letters
		resetTagSuggestion();
		letters = "";

	});
	
	//on keypress in text area get the letters typed and add to letters variable
	//letters defined at top of script
	$('#email_addr').keypress(function(e){
		//$('#email_list').fadeIn();
		var tags = getTags('#email_addr');
		//var tags_array = getTagsArray(tags);
		//if not backspace or comma
		if (e.which != 8 && e.which != 44) {
			letters = letters + String.fromCharCode(e.which);
		} else if (e.which == 8){
			//if backspace delete previous letter
			letters = letters.substring(0,letters.length-1);
		} else if (e.which == 44){
			//if a comma is entered then reset the letters and selection box
			resetTagSuggestion();
			letters = "";
		}
		var suggestedTags = [];
		if (letters.length > 0) {
			suggestedTags = getSuggestedTags(tag_cloud_array,letters,tags);
			if (suggestedTags.length != 0) {
				$('#email_list').html(formatListFromArray(suggestedTags));
				$('#email_list').show();
			} else {
				resetTagSuggestion();
			}
		} else {
			//hide suggestion box
			resetTagSuggestion();
		}
	});


	//on click a suggested tag
	$('#email_list li').on("click", function(event){
		var selected_tag = $(this).html();		
		var selected_title = $('#emails').find('a:contains(' + selected_tag + ')').attr('title');
		$('#email_addr').val(selected_tag);
		$('#email_addr_title').val(selected_title);
		//hide suggestion box and reset letters
		resetTagSuggestion();
		letters = "";
	});	
	
	//fades out suggestion box onchange in tag text area	
	$('#email_addr').change(function(){
		//$('#email_list').hide();	
	});
		
});
