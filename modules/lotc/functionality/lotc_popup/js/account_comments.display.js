$(document).ready(function(){
	var comment_limit = 0;
	var show_txt = 'hide oldest';
	$('a.show_all').on('click', function(event) {
		event.preventDefault();
		$("#comment_list").load(siteRoot + '/handsam_core/functionality/account_comments/ajax/show_all_comments.php', {accountid: item_id, limit: comment_limit}, function(){
			if (comment_limit == 0) {
				comment_limit = 8;
				var show_txt = 'hide oldest';
			} else {
				comment_limit = 0;
				var show_txt = 'show all';
			}
			
			$('a.show_all').html(show_txt);
		});
		
	});		
});