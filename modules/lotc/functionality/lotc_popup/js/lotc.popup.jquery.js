$(document).ready(function(){
	editVisit();
	editComments();
	testeditComments();
	
	function editVisit() {	
		$('a.edit_visit').click(function(event){
			event.preventDefault();
			
			var qs = {visitid: $("#visitid").val() , formid: $("#formid").val()};
			
			$.popUp({
				txt: 'text here',
				load_url: siteRoot + '/modules/lotc/functionality/lotc_popup/ajax/edit_visit_text.php',
				elem: 'div.visit_info',
				elem_action: 'replace',
				ht: '600px',
				params: qs,
				delete_url: siteRoot + '/modules/lotc/index.php'
			}, function () {

				if ($('#visit_end').attr('value') == $('#visit_date').attr('value')) {
					$('#daytrip').attr('checked', true);
					$('#visit_end').fadeTo(500,0.5);
					$('#visit_end').parent().children('img').hide();
					
				}				
				
			});	
			
		});
	}
	

	function editComments() {
	
		$('a.add_comment').click(function(event){
			
			event.preventDefault();
			var commentid = $(this).next('input').val();
			var qs = {visitid: $("#visitid").val() , formid: $("#formid").val(), commentid: commentid};
			$.popUp({
				txt: 'text here',
				load_url: siteRoot + '/modules/lotc/functionality/lotc_popup/ajax/add_comment.php',
				elem: '#comment_list',
				elem_action: 'replace',
				ht: '500px',
				params: qs
			}, '', editComments);
		});	
		
	}
	
	function testeditComments() {
	
		$('a.testadd_comment').click(function(event){
			
			alert("this is my test add comments box");
			event.preventDefault();
			var commentid = $(this).next('input').val();
			var qs = {visitid: $("#visitid").val() , formid: $("#formid").val(), commentid: commentid};
			alert(qs);
		});
		
	}


});



