<?php
$secure = true;
$ajaxModule = "handsam_core";
require("../../../../shared/startPage.php");
$pageFiles->includePhp();
$accountid = getIntFromRequest("accountid");
$limit = getIntFromRequest("limit");
$limit_qry = "";
if ($limit > 0) $limit_qry = " limit 0,$limit";
if (isAllowed("editAccount")) {
	$sql = "SELECT usr.firstname, usr.lastname, account_comments.* FROM account_comments LEFT JOIN usr ON account_comments.created_by = usr.userid WHERE account_comments.accountid = $accountid order by account_comments.created desc $limit_qry";
	$comments = getRS($con, $sql);
	if (getResultRows($comments) > 0) {
		while ($row = getRow($comments)) {
		echo "<li class=\"clearfix\">";
		echo "<strong>{$row['firstname']} {$row['lastname']}</strong> : <em>".displayDateTime($row['created'])."</em>";
		//display edit button if comment is the users own
		//if ($row['created_by'] == $clsUS->userid) {
		//	echo " <a class=\"add_comment\" href=\"?commentid={$row['commentid']}&amp;accountid=$accountid\">edit</a>";
		//}
		
		echo displayParagraphs($row['commenttext']);
		echo "</li>";
		}
	}
}
?>