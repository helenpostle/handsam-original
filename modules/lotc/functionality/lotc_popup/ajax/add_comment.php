<?php
$secure = true;
$ajaxModule = "lotc";
 error_reporting(E_ALL); 
 ini_set("display_errors", 1); 
require("../../../../../shared/startPage.php");

####make this a secure page
/*
require("../library/securePage.php");

####include other functions
include("../library/funcRequest.php");
include("../library/funcDisplay.php");
include("../library/funcDbEdit.php");
include("../library/funcDbForm.php");

*/


$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_process_form');


$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$tkn = getStrFromRequest("tkn");

//include("../lotc/library/clsLoadVars.php");
//include("../lotc/library/clsPageQry.php");

//include("../lotc/library/funcLotcDisplayForm.php");

//if (isAllowed("planVisits") || isAllowed("editVisits") || isAllowed("principalApproval") || isAllowed("deputyPrincipalApproval")  || isAllowed("financeApproval")) {
	//get current stage for this visit for use in is_allowed_edit_coment below
	$stage_qry = "select stage from a_visit where visitid = {$pageVars->visitid}";
	$stageDb = new pageQry($con, $stage_qry);
	$stageDb->rsItem();
	//check if have permission to add comments to this visit
	if ($pageVars->visitid !== null && $pageVars->formid !== null && $pageVars->commentid !==null && is_allowed_add_comment($con, $pageVars, $stageDb->row['stage'])) {

		
		//noew need to check if editing a comment, it is their comment
		if ($pageVars->commentid > 0 && is_allowed_edit_comment($con, $pageVars, $pageVars->commentid, $stageDb->row['stage']) || $pageVars->commentid == 0) {
			$commenttext = getStrFromRequest("commenttext");
			$load = getStrFromRequest("load");
			
			
			$qry = "select a_comments.*  from a_comments where  commentid = {$pageVars->commentid}";
			$pageDb = new pageQry($con, $qry);
			$pageDb->rsItem(); 
			
			if (buttonClicked("save")) {
	
				if ($pageVars->commentid == 0) {
					$qry = new dbInsert("a_comments");
					$qry->setReqNumberVal("formid",$pageVars->formid, "formid");
					$qry->setReqNumberVal("visitid",$pageVars->visitid, "visit id");				
				} else {
					$qry = new dbUpdate("a_comments");
					$qry->setParam("commentid",$pageVars->commentid);
				}
				
				$qry->setReqStringVal("commenttext",$commenttext,"comment text");
				$qry->setAudit($clsUS->userid);
				//echo $qry->getSql();
				if ($qry->execute($con)) {
				} else {
					$messages[] = $qry->getError();	
				}
	
			} else if (buttonClicked("cancel")) {
				
				echo "cancel";		
				
			} else {
				if ($load == 'yes') {
					$comments = getComments($con, $pageVars->visitid, $pageVars->formid);
					if ($comments->cnt > 0) {
						//echo "<ul id=\"comment_list\">";
						foreach ($comments->rows as $row) {
						echo "<li class=\"clearfix\">";
						echo "<strong>{$row['firstname']} {$row['lastname']} : <em> ".ucwords($row['usrtype'])."</em></strong> : <em>".displayLotcDateTime($row['created'])."</em>";
						//display edit button if comment is the users own
						if (is_allowed_edit_comment($con, $pageVars, $row['commentid'], $stageDb->row['stage'])) {
							echo " <a class=\"add_comment\" href=\"\">[ edit comment ]</a><input type=\"hidden\" value=\"{$row['commentid']}\" name=\"edit_comment_{$row['commentid']}\"/>";
						}
						
						echo displayParagraphs($row['commenttext']);
						echo "</li>";
						}
						//echo "</ul>";
					}						
				} else {			
					//list plans
					include("$rootPath/modules/lotc/content/page.comment_edit.php");
				}
			}
		}
	}
  else 
  {
    
  }
//}
?>