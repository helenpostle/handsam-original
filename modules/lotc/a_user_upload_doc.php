<?php

/**
* Plan editing functionality for super admin
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
 
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_admin');
$pageFiles->addModFunc('lotc_file_upload');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
$to_file_path = $accountLotcUserDocs_fp;

if (isAllowed("planVisits")) {
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	if ($pageVars->fileid !== null) {


		$title = "LOtC: User Upload Document";
		$content = "page.a_resource_docs_edit.php";

		//get file data and count if this file is used in handsam tasks and lotc
		$qry = "select f.accountid, f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by";
		$qry .= " FROM files f where f.fileid = {$pageVars->fileid} ";
				
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		
		if (buttonClicked("save")) {
			$fileid = lotcUploadFile($clsUS, $pageVars, $con, $to_file_path, "accountLotcUserDoc");
			if ($fileid) {
				//first delete this file from lotc_files - no chance of duplicates!
				$qry = new dbDelete("a_lotc_files");
				$qry->setParam("fileid",$fileid);
				$qry->setParam("accountid",$pageVars->accountid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				}
				//now add the fileid to lotc_files
				$qry = new dbInsert("a_lotc_files");
				$qry->setReqNumberVal("fileid",$fileid,"fileid");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"account id");
				$qry->setReqNumberVal("visitid",$pageVars->visitid,"visit id");
				$qry->setReqNumberVal("formid",$pageVars->formid,"form id");
				$qry->setAudit($clsUS->userid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					headerLocation("mylotc.php?formid={$pageVars->formid}&visitid={$pageVars->visitid}&filter=$qsfrm&accountid={$pageVars->accountid}&rand=".rand(),$messages);
				}
			}
			
		} else if (buttonClicked("cancel")) {
			
			headerLocation("mylotc.php?formid={$pageVars->formid}&visitid={$pageVars->visitid}&filter=$qsfrm&accountid={$pageVars->accountid}&rand=".rand(),$messages);
			
		} else if (buttonClicked("delete")) {
            if ($pageVars->fileid != 0 && $pageDb->row['accountid'] == $pageVars->accountid && (isAllowed("editVisits") ||$pageDb->row['created_by'] == $clsUS->userid )) {
                $qry = new dbDelete("a_lotc_files");
                $qry->setParam("fileid",$pageVars->fileid);
                $qry->setParam("accountid",$pageVars->accountid);
                if (!$qry->execute($con)) {
                    $messages[] = $qry->getError();
                } else {
                    $fileid = lotcDeleteFile($pageVars, $con, $to_file_path);
                    if ($fileid) {
                        headerLocation("mylotc.php?formid={$pageVars->formid}&visitid={$pageVars->visitid}&filter=$qsfrm&accountid={$pageVars->accountid}&rand=".rand(),$messages);
                    } else {
                        $messages[] = "Error deleting file";
                    }
                }
			} else {
                $messages[] = "Cannot delete file. File in use.";
            }
		}
		
	} else {		
			
		//list guidance attached to lotc
		$title = "LOtC: Account Resources Documents List";
		$content = "page.a_resource_docs_list.php";
		$box[] = "box.a_guidance.php";
		$pageDb = getResourceDocsList($con, $pageVars->accountid);
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>