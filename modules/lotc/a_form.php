<?php

/**
* Form editing functionality
*
*/
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
 
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_account_admin');

$pageFiles->includePhp();


$pageVars = new loadVars($clsUS,$con);

$subpageof = "a_plan.php";

if (isAllowed("editVisits")) {
	$stage = getIntFromRequest('stage');
	//get plan title
	$qry = "select planname from a_plan where planid = {$pageVars->planid} and accountid = {$pageVars->accountid}";
	$titleDb = new pageQry($con, $qry);
	$titleDb->rsItem();

	if ($pageVars->formid !== null) {
		//edit /create form
		$title = "LOtC: {$titleDb->row['planname']}: Edit Form";
		$content = "page.account_form_edit.php";
		$qry = "select * from a_form where formid = {$pageVars->formid} and accountid = {$pageVars->accountid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
		
		//also get total number of forms in this plan for use in the 'stage' field select box
		
		$qry_frmcnt = "select formid  from  a_form  where state != 'DELETED' and a_form.planid = {$pageVars->planid}";
		$frmcntDb = new pageQry($con, $qry_frmcnt);
		
	
		if (buttonClicked("save")) {
			#check that a form doesn't exist with this name in this plan
			$rsCheck = getRS($con,"select * from a_form where formname = '".mysql_real_escape_string($_POST['formname'])."'  and planid = {$pageVars->planid} and formid != {$pageVars->formid}");
			if (getRow($rsCheck)) {
				$messages[] = FORM_IN_USE;	
			} else {
							
				if ($pageVars->formid == 0  && !planUsed($pageVars, $con)) {
					$qry = new dbInsert("a_form");
					$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
				} else {
					$qry = new dbUpdate("a_form");
					$qry->setParam("formid",$pageVars->formid);
				}
			
				if (isset($qry)) {
					$qry->setNumberVal("planid",$pageVars->planid,"plan ID");
					if (!planUsed($pageVars, $con)) {
						$qry->setReqNumberVal("stage",$_POST["stage"],"stage");
					}
					$qry->setReqStringVal("formname",$_POST["formname"],"form name");
					$qry->setStringVal("formtext",$_POST["formtext"],"form text");
					if (!planUsed($pageVars, $con)) {
						$qry->setReqStringVal("state",$_POST["state"],"state");
					}
					
					$qry->setAudit($clsUS->userid);
					
					if (($pageVars->formid == 0  && !planUsed($pageVars, $con)) || $pageVars->formid > 0) {
						if ($qry->execute($con)) {
							if ($pageVars->formid == 0) {
								$pageVars->formid = $qry->getNewID();
								//now update other form stages
								$stage_qry = "Update a_form set stage = stage + 1 where formid != {$pageVars->formid} and stage >= {$stage}  and planid = {$pageVars->planid} ";
								$exec = execSQL($con, $stage_qry); 
		
							} else {
								//now update other form stages
								//different update if new stage > old stage
								if ($stage > $pageDb->row['stage']) {
									$stage_qry = "Update a_form set stage = stage - 1 where formid != {$pageVars->formid} and stage <= {$stage} and stage > {$pageDb->row['stage']}  and planid = {$pageVars->planid}";
								} else {
									$stage_qry = "Update a_form set stage = stage + 1 where formid != {$pageVars->formid} and stage >= {$stage} and stage < {$pageDb->row['stage']}  and planid = {$pageVars->planid}";
								}
	
								$exec = execSQL($con, $stage_qry); 
							}
								
							$messages[] = SAVED;
							$pageDb = getFormList($pageVars, $con);
							$title = "LOtC: {$titleDb->row['planname']}: Form List";
							$content = "page.account_form_list.php";
							$pageFiles->addModFunc("lotc_tables");
							
						} else {
							$messages[] = $qry->getError();	
						}
					} else {
						$messages[] = "This Plan has already been used so no new forms may be added";
						//list forms
						$pageDb = getFormList($pageVars, $con);			
						$title = "LOtC: {$titleDb->row['planname']}: Form List";
						$content = "page.account_form_list.php";
						$pageFiles->addModFunc("lotc_tables");
					}
				}
			}
		} else if (buttonClicked("delete")) {
			
				$qry = new dbUpdate("a_form");
				$qry->setParam("planid",$pageVars->formid);
				$qry->setReqStringVal("state","DELETED","State");
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#change content to list
					$messages[] = DELETED;
				//list forms
				$pageDb = getFormList($pageVars, $con);			
				$title = "LOtC: {$titleDb->row['planname']}: Form List";
				$content = "page.account_form_list.php";
				}
		} else if (buttonClicked("cancel")) {
		//list forms
			$pageDb = getFormList($pageVars, $con);
			$title = "LOtC: {$titleDb->row['planname']}: Form List";
			$content = "page.account_form_list.php";
			$pageFiles->addModFunc("lotc_tables");
		}
	
				
	} else {
		if (!planUsed($pageVars, $con)) {
			//change form order function
			changeFormStage($pageVars, $con);
		}

		
		//list forms
		$pageDb = getFormList($pageVars, $con);
		$title = "LOtC: {$titleDb->row['planname']}: Form List";
		$content = "page.account_form_list.php";
		$pageFiles->addModFunc("lotc_tables");

		
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>