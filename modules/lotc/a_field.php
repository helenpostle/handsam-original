<?php

/**
* Field editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
 
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_account_admin');

$pageFiles->includePhp();


$pageVars = new loadVars($clsUS,$con);

$subpageof = "a_plan.php";

if (isAllowed("editVisits")) {
	$field_order = getIntFromRequest('field_order');
		
	//get form title
	$qry = "select formname, planname from a_form left join a_plan on a_form.planid = a_plan.planid where formid = {$pageVars->formid} and a_form.accountid = {$pageVars->accountid} and a_plan.accountid = {$pageVars->accountid}";
	$titleDb = new pageQry($con, $qry);
	$titleDb->rsItem();

	if ($pageVars->fieldid !== null) {
		//$jquery[] = "field_select_options.js";
		$pageFiles->addModFunc("lotc_field_options");
		//edit /create plan
		$title = "LOtC: Edit Field";
		$content = "page.account_field_edit.php";
		$qry = "select * from a_field where fieldid = {$pageVars->fieldid} and accountid = {$pageVars->accountid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
		
		//also get total number of fields in this form for use in the 'field order'  select box
		
		$qry_fldcnt = "select fieldid  from  a_field  where state != 'DELETED' and a_field.formid = {$pageVars->formid}";
		$fldcntDb = new pageQry($con, $qry_fldcnt);
		
	
		if (buttonClicked("save")) {
			#check that a field doesn't exist with this name in this form
			$rsCheck = getRS($con,"select * from a_field where fieldname = '".mysql_real_escape_string($_POST['fieldname'])."' and formid = {$pageVars->formid} and fieldid != {$pageVars->fieldid}");
			if (getRow($rsCheck)) {
				$messages[] = FIELD_IN_USE;	
			} else {
							
				if ($pageVars->fieldid == 0) {
					$qry = new dbInsert("a_field");
					$qry->setReqNumberVal("formid",$pageVars->formid,"formid");
					$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
				} else {
					$qry = new dbUpdate("a_field");
					$qry->setParam("fieldid",$pageVars->fieldid);
				}
			
				if (isset($qry)) {
					$qry->setReqStringVal("fieldname",$_POST["fieldname"],"field name");
					$qry->setStringVal("fieldtext",$_POST["fieldtext"],"field text");
					if (isset($_POST["required"])) {
						$qry->setNumberVal("required",$_POST["required"],"required");
					} else {
						$qry->setNumberVal("required",00,"required");
					}
					//$qry->setStringVal("default_value",$_POST["default_value"],"default value");
					$qry->setReqNumberVal("field_typeid",$_POST["field_typeid"],"field type id");
					if ($_POST["field_typeid"] == 8) {
						$qry->setReqStringVal("params",$_POST["params"],"drop down list options");
					} else {
						$qry->setNullVal("params");
					}
					$qry->setReqStringVal("state",$_POST["state"],"state");
					$qry->setReqNumberVal("field_order",$_POST["field_order"],"field_order");
					$qry->setAudit($clsUS->userid);
					
					if ($qry->execute($con)) {
						if ($pageVars->fieldid == 0) {
							$pageVars->fieldid = $qry->getNewID();	
							//now update other field orders in this form
							$stage_qry = "Update a_field set field_order = field_order + 1 where fieldid != {$pageVars->fieldid} and field_order >= {$field_order}  and formid = {$pageVars->formid} ";
							$exec = execSQL($con, $stage_qry); 
						} else {
							//now update other field orders in this form
							//different update if new order > old order
							if ($field_order > $pageDb->row['field_order']) {
								$stage_qry = "Update a_field set field_order = field_order - 1 where fieldid != {$pageVars->fieldid} and field_order <= {$field_order} and field_order > {$pageDb->row['field_order']}  and formid = {$pageVars->formid}";
							} else {
								$stage_qry = "Update a_field set field_order = field_order + 1 where fieldid != {$pageVars->fieldid} and field_order >= {$field_order} and field_order < {$pageDb->row['field_order']}  and formid = {$pageVars->formid}";
							}

							$exec = execSQL($con, $stage_qry); 
							//echo $stage_qry;
						}
						$messages[] = SAVED;
						$pageDb = getFieldList($pageVars, $con);
						$title = "LOtC: {$titleDb->row['formname']}: Field List";
						$content = "page.account_field_list.php";
						$pageFiles->addModFunc("lotc_tables");
						
					} else {
						$messages[] = $qry->getError();	
					}
				}
			}
		} else if (buttonClicked("delete")) {
			
			$qry = new dbUpdate("a_field");
			$qry->setParam("fieldid",$pageVars->fieldid);
			$qry->setReqStringVal("state","DELETED","State");
			if (!$qry->execute($con)) {
				$messages[] = $qry->getError();
			} else {
				#change content to list
				$messages[] = DELETED;
				$pageDb = getFieldList($pageVars, $con);
				$title = "LOtC: {$titleDb->row['formname']}: Field List";
				$content = "page.account_field_list.php";
				$pageFiles->addModFunc("lotc_tables");
			}
		} else if (buttonClicked("cancel")) {
			$pageDb = getFieldList($pageVars, $con);
			$title = "LOtC: {$titleDb->row['formname']}: Field List";
			$content = "page.account_field_list.php";
			$pageFiles->addModFunc("lotc_tables");
		}
	
				
	} else {
		//change field order function
		changeFieldOrder($pageVars, $con);

		$pageDb = getFieldList($pageVars, $con);
		$title = "LOtC: {$titleDb->row['formname']}: Field List";
		$content = "page.account_field_list.php";
		$pageFiles->addModFunc("lotc_tables");
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>