<?php

/**
* Plan editing functionality
*
*/

/**
 * Include basic stuff
 */
 
require("../../shared/startPage.php");

####make this a secure page
$secure = true;
$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_process_form');
$pageFiles->addModFunc('lotc_form');

$pageFiles->csrf_form = '';

$pageFiles->includePhp();
//do we have $clsUS set
if(isset($_SESSION['clsUS']))
    {
        $clsUS = $_SESSION['clsUS'];
	}

$pageVars = new loadVars($clsUS,$con);
//echo "The page vars have been loaded";

$stage = getIntFromRequest('stage');
$token = getStrFromRequest('emailtoken');
$visitid = getIntFromRequest('visitid');
$userid = getIntFromRequest('userid');
$email_uploads = getIntFromRequest('uploads');
$email_comments = getIntFromRequest('comments');
if (checkEmailVisitToken($con, $token, $userid, $visitid)) {
	if ($visitid !== null) {
		//first get visit table data
		//query for specific stage of visit
		$qry = "SELECT concat(usr.firstname,' ',usr.lastname) as leader, b.formname, b.formtext, b.formid, b.stage AS stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) as stage_cnt  from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid RIGHT JOIN a_form b ON b.planid = a_visit.planid left join usr on a_visit.leaderid = usr.userid where visitid = {$visitid} and a_form.state = 'ACTIVE' and a_plan.state = 'ACTIVE' and CASE WHEN {$pageVars->stage} < a_visit.stage THEN  b.stage = {$stage} ELSE b.stage = a_visit.stage END group by usr.firstname,usr.lastname, b.formname, b.formtext, b.formid, b.stage, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date,a_form.state";

		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
		//echo $qry;
		
		//$accountid = $pageDb->row['accountid'];
		$formid = $pageDb->row['formid'];;

		//need to get latest form state for this form
		
		$qry = "select * from a_form_state where visitid = $visitid and formid = {$pageDb->row['formid']} order by created desc";
		$formStateDb = new pageQry($con, $qry);
		$formStateDb->rsList(); 
		$form_state = $formStateDb->rows[0]['state'];
		$frmTitle = $pageDb->row['formname'];
		$frmText = $pageDb->row['formtext'];
		$formDB = getLotcForm($con, $pageDb->row['stage'], $visitid);
		//$userAgent = $_SERVER['HTTP_USER_AGENT']

		
		//add all system wide css files:
		
		$pageFiles->addSystemCss("system.css");
		$pageFiles->addSystemCss("tables.content.css");
		$pageFiles->addSystemCss("tables.box.css");
				
		
		?>
		
		<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
		<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
		<head>
		<?php include("installations/$installation/layout/head.php"); ?>
		<title>LOtC Visit Details</title>
		<?php
		//include css files
		if (isset($pageFiles)) $pageFiles->includeCss();
		$userAgent = $_SERVER['HTTP_USER_AGENT'];
		$os = "Mac OS X";
		$version = "";
		$osTest = strpos($userAgent, $os);
		if ($osTest) {
		?>
		<link rel="stylesheet" href="<?php echo $rootUrl;?>/css/mac.css?v=<?php echo $css_version;?>" type="text/css" />
		<?php
		}
		?>
		</head>
		<body>
		<?php include("installations/$installation/layout/branding_nologin.php"); ?>
		<div id="wrapper" class="clearfix">
		    <div id="content_wrapper" class="clearfix">
			   <div id="content">
			     <?php echo "<br/><br/><h1>LOtC: {$pageDb->row['visitname']} :: stage {$pageDb->row['stage']} out of {$pageDb->row['stage_cnt']}</h1>";?>
				 <?php include("content/page.display_visit_email.php"); ?>
			   </div>
			</div>
		</div>
		<div id="footer" class="clearfix">
		<?php include("installations/$installation/layout/footer.php"); ?>
		</body>
		</html>	
		
		<?php
				
	}


}

?>