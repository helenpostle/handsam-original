<?php

/**
* Display list of LOtC links for this account. EVC can add or remove items from this list, so decides what account planners can see/use
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_admin');
//disable csrf form because this page has can only update this users' account anyway, so user must be logged in, and we're not submitting a form anyway.
$pageFiles->csrf_form = '';

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$subpageof = "a_plan.php";

if (isAllowed("editVisits")) {
	if ($pageVars->linkid !== null) {
		if ($pageVars->action == "add") {
			//add guidance to lotc
			//first delete any record with same fileid to avoid duplicates
			$qry = new dbDelete("a_lotc_links");
			$qry->setParam("linkid",$pageVars->linkid);
			$qry->setParam("accountid",$pageVars->accountid);
			if (!$qry->execute($con)) {
				$messages[] = $qry->getError();
			}
			//now add the fileid to lotc_files
			$qry = new dbInsert("a_lotc_links");
			$qry->setReqNumberVal("linkid",$pageVars->linkid,"linkid");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
			$qry->setAudit($clsUS->userid);
			if (!$qry->execute($con)) {
				$messages[] = $qry->getError();
			} else {
				headerLocation("a_lotc_guidance_links.php?rand=".rand(), true);
			}
		}
		
		
		if ($pageVars->action == "rm") {
			//remove guidance from lotc
			$qry = new dbDelete("a_lotc_links");
			$qry->setParam("linkid",$pageVars->linkid);
			$qry->setParam("accountid",$pageVars->accountid);
			if (!$qry->execute($con)) {
				$messages[] = $qry->getError();
			} else {
				headerLocation("a_lotc_guidance_links.php?rand=".rand(), true);
			}
		}
	}

	//list guidance attached to lotc
	$title = "LOtC: LOtC Guidance Links List";
	$content = "page.a_lotc_guidance_links_list.php";
	$box[] = "box.a_guidance.php";
	$pageDb = getAccountLotcGuidanceLinksList($con, $pageVars->accountid);
	$pageFiles->addModFunc("lotc_tables");


} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>