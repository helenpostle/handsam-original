<?php 
/**
 * lists lotc changes in form state 
 * 
 */
 
lotcBoxTop("Form History");

$history = form_state($con, $pageVars->visitid, $pageDb->row['formid']);
if ($history->cnt > 0) {
	echo "<ul class=\"lotc_box_list\">";
	foreach ($history->rows as $row) {
		echo "<li>{$row['state']} : <strong>{$row['firstname']} {$row['lastname']} : <em> ".ucwords($row['usrtype'])."</em></strong> : <em>".displayLotcDateTime($row['created'])."</em></li>";		
	}
	echo "<ul>";
} else {
	echo "<p>No History</p>";
}
lotcBoxBottom();
?>
	