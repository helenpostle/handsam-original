<?php 
/**
 * liste handsam resource RISK ASSESSMENT docs for this account
 * 
 */
 
 
$qry = "SELECT af.fileid, f.title, f.filename, '' AS url, f.filecategory as category, 'file' AS resource FROM a_lotc_files af LEFT JOIN files f ON f.fileid = af.fileid WHERE af.accountid = {$pageVars->accountid} and filetype = 'handsamDoc' and f.filecategory = 'RISK_ASSESSMENT'";
$qry .= " UNION SELECT al.linkid, l.title,'' as filename, l.url, l.linkcategory as category , 'link' AS resource FROM a_lotc_links al LEFT JOIN links l ON l.linkid = al.linkid WHERE al.accountid = {$pageVars->accountid}  and linktype = 'handsamDoc' and l.linkcategory = 'RISK_ASSESSMENT' order by title asc";

//echo $qry;

$resourceDb = new pageQry($con, $qry);
$resourceDb->rsList();


lotcBoxTop("HandSaM LOtC Risk Assessments");

if ($resourceDb->cnt > 0) {
	echo "<ul class=\"lotc_box_list\">";
	foreach ($resourceDb->rows as $row) {
		$cls = strtolower(substr(strrchr($row['filename'],'.'),1));
		if ($row['resource'] == 'file') {
			echo "<li><a target=\"_blank\" class=\"$cls\" href=\"$rootUrl/modules/lotc/downloadLotcDocument.php?id={$row['fileid']}\">{$row['title']}</a></li>";
		} else if ($row['resource'] == 'link'){
			echo "<li><a target=\"_blank\" class=\"link\" href=\"{$row['url']}\">{$row['title']}</a></li>";
		}		
	}
	echo "<ul>";
} else {
	echo "<p>No Resources</p>";
}
lotcBoxBottom();

?>
	