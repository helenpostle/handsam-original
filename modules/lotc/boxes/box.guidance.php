<?php 
/**
 * lists lotc changes in form state 
 * 
 */
 
lotcBoxTop("Handsam Guidance Options");

echo "<ul class=\"side_nav\">";
echo "<li><a href=\"$rootUrl/modules/lotc/lotc_guidance.php\">View LOtC Guidance Documents</a></li>";
echo "<li><a class=\"attach_guidance\" href=\"$rootUrl/modules/lotc/handsam_guidance.php\">Attach Handsam Guidance Documents to LOtC</a></li>";

if (isAllowed("editMasterPlan")) echo "<li><a class=\"attach_guidance\" href=\"$rootUrl/modules/lotc/handsam_guidance.php?fileid=0\">Upload Handsam Guidance Documents</a></li>";

echo "<li><a href=\"$rootUrl/modules/lotc/lotc_guidance_links.php\">View LOtC Guidance Links</a></li>";
echo "<li><a class=\"attach_guidance\" href=\"$rootUrl/modules/lotc/handsam_guidance_links.php\">Attach Handsam Guidance Links to LOtC</a></li>";

if (isAllowed("editMasterPlan")) echo "<li><a class=\"attach_guidance\" href=\"$rootUrl/modules/lotc/handsam_guidance_links.php?linkid=0\">Add Handsam Guidance Links</a></li>";

echo "</ul>";	
lotcBoxBottom();
?>
	