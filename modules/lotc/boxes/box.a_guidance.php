<?php 
/**
 * lists lotc changes in form state 
 * 
 */
 
lotcBoxTop("LOtC Guidance Options");

echo "<ul class=\"side_nav\">";
echo "<li><a href=\"$rootUrl/modules/lotc/a_lotc_guidance.php\">View LOtC Guidance Documents</a></li>";
echo "<li><a class=\"attach_guidance\" href=\"$rootUrl/modules/lotc/a_resource_docs.php\">View your own resource documents</a></li>";
echo "<li><a class=\"attach_guidance\" href=\"$rootUrl/modules/lotc/a_resource_docs.php?fileid=0\">Upload your own resource documents</a></li>";
echo "<li><a href=\"$rootUrl/modules/lotc/a_lotc_guidance_links.php\">View LOtC Guidance Links</a></li>";
echo "<li><a class=\"attach_guidance\" href=\"$rootUrl/modules/lotc/a_resource_links.php\">View your own resource links</a></li>";
echo "<li><a class=\"attach_guidance\" href=\"$rootUrl/modules/lotc/a_resource_links.php?linkid=0\">Add your own resource links</a></li>";
echo "</ul>";	
lotcBoxBottom();
?>
	