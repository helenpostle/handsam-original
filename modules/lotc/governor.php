<?php

/**
 * Plan editing functionality
 *
 */
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");


$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_process_form');


$pageFiles->includePhp();

$approval_page = 'governor';

$pageVars = new loadVars($clsUS, $con);
if ($pageVars->hacking)
    headerLocation("$rootUrl/index.php");

//include("../library/funcLotcNotifications.php");
//include("../library/funcMail.php");
if (isAllowed("governorApproval")) {

    if ($pageVars->visitid !== null && !buttonClicked("cancel") && !buttonClicked("decline") && !buttonClicked("approve") && !buttonClicked("email"))
        $pageFiles->addModFunc("lotc_email_history");
    if ($pageVars->visitid !== null)
        $pageFiles->addModFunc("lotc_email_notifications");
    $pageFiles->addModFunc("lotc_popup");
    $pageFiles->addModFunc("lotc_tables");

    if ($pageVars->visitid == null || buttonClicked("cancel") || buttonClicked("decline") || buttonClicked("approve") || buttonClicked("email")) {
        $filter_array = array();
        $filter_array['state'] = array("GOVERNOR APPROVAL" => "Governor approval", "DEPUTY PRINCIPAL APPROVAL" => "Deputy Principal approval", "PRINCIPAL APPROVAL" => "Principal approval", "FINANCE APPROVAL" => "Finance approval", "PLANNING" => "In planning", "APPROVAL" => "EVC approval", "COMPLETED" => "Completed", "DELETED" => "Deleted");
        $pageFiles->addModFunc("lotc_visit_filter");
    }

    $pageFiles->includePhp();

    $qs = array();
    $filter_title_str = "";

    //get filter array querystring

    if (isset($_POST['filter'])) {
        $qs = __unserialize($_POST['filter']);
        $qsfrm = $_POST['filter'];
    } else if (isset($_GET['filter'])) {
        $qsfrm = $_GET['filter'];
        $qs = __unserialize(stripslashes($_GET['filter']));
    }
    //set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
    $qs_base = $qs;
    $filter_title = array();
    $filter_qry = array();
    $filter_date_field = " a_visit.created ";

    //set the crumbtrail first link back to the evc page with list of visits
    $crumb_back = "<a href=\"evc.php?accountid={$pageVars->accountid}&amp;rand=" . rand() . "&amp;filter=" . urlencode(serialize($qs)) . "\" title=\"EVC LOtC List\">EVC LOtC list</a>";
    //process filters
    //state filter
    if (is_array($qs) && array_key_exists('state', $qs)) {
        switch ($qs['state']) {
            case '0':
                $filter_qry['state'] = " a_visit.state != 'DELETED' ";
                $filter_title['state'] = "All";
                break;

            case "ALL":
                $filter_qry['state'] = " a_visit.state != 'DELETED' ";
                $filter_title['state'] = "All States";
                break;

            case "PLANNING":
                $filter_qry['state'] = " a_visit.state = 'PLANNING' ";
                $filter_title['state'] = "Planning";
                break;

            case "FINANCE APPROVAL":
                $filter_qry['state'] = " a_visit.state = 'FINANCE APPROVAL' ";
                $filter_title['state'] = "Finance Approval";
                break;

            case "PRINCIPAL APPROVAL":
                $filter_qry['state'] = " a_visit.state = 'PRINCIPAL APPROVAL' ";
                $filter_title['state'] = "Principal Approval";
                break;

            case "DEPUTY PRINCIPAL APPROVAL":
                $filter_qry['state'] = " a_visit.state = 'DEPUTY PRINCIPAL APPROVAL' ";
                $filter_title['state'] = "Deputy Principal Approval";
                break;

            case "GOVERNOR APPROVAL":
                $filter_qry['state'] = " a_visit.state = 'GOVERNOR APPROVAL' ";
                $filter_title['state'] = "Governor Approval";
                break;

            case "APPROVAL":
                $filter_qry['state'] = " a_visit.state = 'APPROVAL' ";
                $filter_title['state'] = "Approval";
                break;

            case "COMPLETED":
                $filter_qry['state'] = " a_visit.state = 'COMPLETED' ";
                $filter_title['state'] = "Completed";
                break;

            case "DELETED":
                $filter_qry['state'] = " a_visit.state = 'DELETED' ";
                $filter_title['state'] = "Deleted";
                break;


            default:
                $filter_qry['state'] = " a_visit.state = 'GOVERNOR APPROVAL' ";
                $filter_title['state'] = "Governor Approval";
                $qs_base['state'] = 'GOVERNOR APPROVAL';
                break;
        }
    } else {
        $filter_qry['state'] = " a_visit.state = 'GOVERNOR APPROVAL' ";
        $filter_title['state'] = "Governor Approval";
        $qs_base['state'] = 'GOVERNOR APPROVAL';
    }

    //choose if date created ot visit date filtered
    if (is_array($qs) && array_key_exists('date', $qs)) {
        switch ($qs['date']) {
            case "visit_date":
                $filter_date_field = " a_visit.visit_date ";
                $filter_title['date'] = "Visit Date";
                break;

            default:
                //$filter_date_field = " a_visit.created ";
                //$filter_title['date'] = "Date Created";
                $filter_date_field = " a_visit.visit_date ";
                $filter_title['date'] = "Visit Date";
                break;
        }
    } else {
        $filter_date_field = " a_visit.visit_date ";
        $filter_title['date'] = "Visit Date";
    }

    //month filter
    if (is_array($qs) && array_key_exists('month', $qs) && $qs['month'] >= 1) {
        settype($qs['month'], 'string');
        $filter_qry['month'] = " MONTH($filter_date_field) = '{$qs['month']}' ";
        $filter_title['month'] = date('F', mktime(0, 0, 0, $qs['month']));
    } else {
        $filter_title['month'] = "All months";
    }

    //year filter
    if (is_array($qs) && array_key_exists('year', $qs) && $qs['year'] >= 1) {
        settype($qs['year'], 'int');
        $filter_qry['year'] = " YEAR($filter_date_field) = '{$qs['year']}' ";
        $filter_title['year'] = $qs['year'];
    } else {
        $filter_title['year'] = "All Years";
    }

    //user filter
    if (is_array($qs) && array_key_exists('user', $qs) && $qs['user'] >= 1) {
        settype($qs['user'], 'int');
        //need to get the planer name
        $planner_qry = "select CONCAT(u.firstname, ' ', u.lastname) AS name from usr u where u.userid = {$qs['user']}";
        $plannerRS = getRS($con, $planner_qry);
        $plannerRow = getRow($plannerRS);
        $filter_qry['user'] = " leaderid = {$qs['user']} ";
        $filter_title['user'] = $plannerRow['name'];
    } else {
        $filter_title['user'] = "All Users";
    }


   
    //now implode filter title

    $filter_title_str = ": " . implode(": ", $filter_title);


    if ($pageVars->visitid !== null) {
        $jquery[] = "email_logs.js";
        //first get visit table data
        //query for specific stage of visit
        if ($pageVars->stage > 0) {
            $qry = "SELECT concat(usr.firstname,' ',usr.lastname) as leader, b.formname, b.formtext, b.formid, b.stage AS stage, a_visit.visit_end, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) as stage_cnt  from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid RIGHT JOIN a_form b ON b.planid = a_visit.planid left join usr on a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid} and a_form.state = 'ACTIVE' and a_plan.state = 'ACTIVE' and CASE WHEN {$pageVars->stage} < a_visit.stage THEN  b.stage = {$pageVars->stage} ELSE b.stage = a_visit.stage END GROUP by usr.firstname, usr.lastname,b.formname, b.formtext, b.formid, b.stage, a_visit.visit_end, a_visit.visitid, a_visit.visitname, a_visit.visittext, a_visit.state, a_visit.visit_date, a_form.state ";
        } else {
            //query to get current form stage of visit
            $qry = "SELECT concat(usr.firstname,' ',usr.lastname) as leader, v.*, f.formname, f.formtext, f.formid, f2.stage_cnt FROM a_visit v LEFT JOIN a_form f ON v.planid = f.planid AND v.stage = f.stage LEFT JOIN (SELECT planid, COUNT(*) AS stage_cnt FROM a_form GROUP by planid) f2 ON f2.planid = v.planid left join usr on v.leaderid = usr.userid WHERE visitid = {$pageVars->visitid}  AND f.state = 'ACTIVE'";
        }
        $pageDb = new pageQry($con, $qry);
        $pageDb->rsItem();
        //echo $qry;
        if ($pageDb->row['state'] == 'SUPER_DELETED')
            headerLocation("$rootUrl/modules/lotc/evc.php", false);
        //get year groups
        $year_qry = "select year_group.year_text from year_group left join a_visit_year_group on year_group.year_group_id = a_visit_year_group.year_group_id where a_visit_year_group.visitid={$pageVars->visitid}";
        $yearsRs = getRS($con, $year_qry);
        $years_arr = array();
        while ($year_row = getRow($yearsRs)) {
            $years_arr[] = $year_row['year_text'];
        }
        if (buttonClicked("approve")) {
            $approval_fields = array();
            $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid, FALSE);
            /*
            foreach ($formDB->rows as $row) {
                if ($row['field_typeid'] == 11)
                    $approval_fields[] = 'dep_principal';
            }
            $approval_fields = array_unique($approval_fields);
            $approval = new userApproval($lotc_conf, $con, $pageVars->accountid, $approval_fields);


            if (count($approval->dep_principal) > 0) {
                if (updateVisitState($con, $clsUS, $pageVars->visitid, "DEPUTY PRINCIPAL APPROVAL")) {
                    if (updateFormState($con, $clsUS, $pageVars->visitid, $pageDb->row['formid'], "APPROVED PRINCIPAL")) {
                        //notify deputy
                        notifyUser($con, $pageVars, $approval->dep_principal, 'deputy_principal');
                        $messages[] = SAVED;
                        headerLocation("$rootUrl/modules/lotc/principal.php?rand=" . rand() . "&filter=" . urlencode(serialize($qs)), $messages);
                    }
                }
            }
            */
            
            //email evc for approval
            if (updateVisitState($con, $clsUS, $pageVars->visitid, "APPROVAL")) {
                if (updateFormState($con, $clsUS, $pageVars->visitid, $pageDb->row['formid'], "APPROVED GOVERNOR")) {
                    //notify evc
                    notifyEvc($con, $pageVars);
                    $messages[] = SAVED;
                    headerLocation("$rootUrl/modules/lotc/governor.php?rand=" . rand() . "&filter=" . urlencode(serialize($qs)), $messages);
                }
            }
        } else if (buttonClicked("decline")) {
            //update visit and form state
            if (updateVisitState($con, $clsUS, $pageVars->visitid, "PLANNING")) {
                if (updateFormState($con, $clsUS, $pageVars->visitid, $pageDb->row['formid'], "DECLINED GOVERNOR")) {
                    notifyPlanner($con, $pageVars, 'declined', ' Governor ');
                    $messages[] = DECLINED;
                    $title = "LOtC: Visit List";
                    $content = "page.approval_visit_list.php";
                    $pageDb = getVisitList($pageVars, $con, $filter_qry, true);
                    //$box[] = "lotc/box.visit_list_filter.php";
                }
            }
        } else if (buttonClicked("cancel")) {
            $title = "LOtC: Visit List";
            $content = "page.approval_visit_list.php";
            $pageDb = getVisitList($pageVars, $con, $filter_qry, true);
            //$box[] = "lotc/box.visit_list_filter.php";
        } else if (buttonClicked("delete")) {
            if (updateVisitState($con, $clsUS, $pageVars->visitid, "SUPER_DELETED")) {
                $title = "LOtC: Visit List";
                $content = "page.approval_visit_list.php";
                $pageDb = getVisitList($pageVars, $con, $filter_qry);
                //$box[] = "lotc/box.visit_list_filter.php";
            }
        } else if (buttonClicked("email")) {
            headerLocation("email_visit.php?accountid=$pageVars->accountid&userid=$pageVars->userid&visitid=$pageVars->visitid&filter=" . rawurlencode(serialize($qs)), $messages);
        } else {
            //need to get latest form state for this form

            $qry = "select * from a_form_state where visitid = {$pageVars->visitid} and formid = {$pageDb->row['formid']} order by created desc";

            $formStateDb = new pageQry($con, $qry);
            $formStateDb->rsList();

            $form_state = "";
            if (isset($formStateDb->rows[0]))
                $form_state = $formStateDb->rows[0]['state'];


            $title = displayText("LOtC: {$pageDb->row['visitname']} :: stage {$pageDb->row['stage']} out of {$pageDb->row['stage_cnt']}");
            $frmTitle = $pageDb->row['formname'];
            $frmText = $pageDb->row['formtext'];
            //set field_state to DELETED so that we get currently deleted fields that were ACTIVE when the form was submitted
            $formDB = getLotcForm($con, $pageDb->row['stage'], $pageVars->visitid, FALSE);
            $content = "page.approval_display_visit.php";
            $box[] = "box.form_state.php";
            $box[] = "box.account_resource_list.php";
            //$box[] = "box.a_email_log.php";
            $box[] = "box.handsam_resource_list.php";
            $box[] = "box.handsam_risk_assessment_list.php";
        }
    } else {
        //list plans
        $title = "LOtC: Visit List $filter_title_str";
        $content = "page.approval_visit_list.php";
        $pageDb = getVisitList($pageVars, $con, $filter_qry, true);
        //$box[] = "lotc/box.visit_list_filter.php";
    }
} else {
    trigger_error("Access Denied", E_USER_WARNING);
    headerLocation("$rootUrl/index.php", false);
}


include("../../layout.php");
?>