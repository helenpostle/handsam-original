<?php
$secure = true;
error_reporting(E_ALL); 
 ini_set("display_errors", 1); 
require("../../shared/startPage.php");

$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_process_form');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$tkn = getStrFromRequest("tkn");

//get current stage for this visit for use in is_allowed_edit_coment below
	$stage_qry = "select stage from a_visit where visitid = {$pageVars->visitid}";
	$stageDb = new pageQry($con, $stage_qry);
	$stageDb->rsItem();
	
	//check if have permission to add comments to this visit
	if ($pageVars->visitid !== null && $pageVars->formid !== null  && is_allowed_add_comment($con, $pageVars, $stageDb->row['stage'])) {
	   $title = "LOtC: User Add Comment";
	   $content = "page.comment_edit.php";
	} else {
	   //we have something missing or the person cannot add a comment, show an error page
	   $title = "LOtC: Error Adding Comments";
	   $content = "page.comment_error.php";
	}
	
	if (buttonClicked("save")) {
	   if ($pageVars->commentid == 0) {
			$qry = new dbInsert("a_comments");
			$qry->setReqNumberVal("formid",$pageVars->formid, "formid");
			$qry->setReqNumberVal("visitid",$pageVars->visitid, "visit id");				
		} else {
			$qry = new dbUpdate("a_comments");
			$qry->setParam("commentid",$pageVars->commentid);
		}
		$commenttext = getStrFromRequest("commenttext");
		$qry->setReqStringVal("commenttext",$commenttext,"comment text");
		$qry->setAudit($clsUS->userid);
		//echo $qry->getSql();
		if ($qry->execute($con)) {
		   headerLocation("mylotc.php?formid={$pageVars->formid}&visitid={$pageVars->visitid}&filter=$qsfrm&accountid={$pageVars->accountid}&rand=".rand(),$messages);
		} else {
			$messages[] = $qry->getError();	
		}
	}
	
	if (buttonClicked("cancel")){
	   headerLocation("mylotc.php?formid={$pageVars->formid}&visitid={$pageVars->visitid}&filter=$qsfrm&accountid={$pageVars->accountid}&rand=".rand(),$messages);
	}

include("../../layout.php");
?>