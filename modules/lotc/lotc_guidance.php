<?php

/**
* Plan editing functionality for super admin
*
*/

/**
 * Include basic stuff
 */
//make this a secure page
$secure = true;
 
require("../../shared/startPage.php");


$pageFiles->addModFunc('lotc_admin');

$pageFiles->includePhp();

$subpageof = "plan.php";

$pageVars = new loadVars($clsUS,$con);

if (isAllowed("editMasterPlan")) {

	//set jquery scripts
	//$jquery_plugin[] = "jquery.popUp.js";
	//$jquery[] = "lotc.guidance.js";
	$pageFiles->addModFunc("lotc_tables");
		//list guidance attached to lotc
		$title = "LOtC: LOtC Guidance Documents List";
		$content = "page.lotc_guidance_list.php";
		$box[] = "box.guidance.php";
		$pageDb = getLotcGuidanceList($con);

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>