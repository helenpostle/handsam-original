<?php
error_reporting(E_ALL);
 ini_set("display_errors", 1); 
/**
* Plan editing functionality
*
*/
/*MUST HAVE lOCALHOST CONFIGURED IN APACHE AS SERVER ALIAS FOR THIS SITE FOR ATTACHMENT SCRIPT TO WORK */
require("../../shared/startPage.php");

####make this a secure page
$pageFiles->addModFunc('lotc_email_visit');

####include other functions

$pageFiles->addModFunc('lotc_visits');

//include("../library/clsEmail.php");
//include("../library/funcMail.php");
//load all php files for this page
$pageFiles->includePhp();

$subpageof = "evc.php";

$pageVars = new loadVars($clsUS,$con);
$emailid = getIntFromRequest("emailid");
$email_addr = trim(getStrFromRequest("email_addr"));
$qs = array();

if (isAllowed("editVisits")) {

	//get filter array querystring
	
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;

	
	if ($pageVars->visitid !== null) {
		
		
		//$jquery_plugin[] = "jquery.rich-array.js";
		//$jquery[] = "add_email.js";
		//$jquery[] = "delete_email.js";

		$content = "page.email_visit.php";
		//$box[] = "box.a_email_visit.php";
		$title = "LOtC: Email visit details";
		
		/*
		$qry = "select a_visit.*  from a_visit where visitid = {$pageVars->visitid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
		*/
		
		if ($emailid > 0) {
			$qry = new dbDelete("a_email_visit");
			$qry->setParam("emailid", $emailid);
			$qry->setParam("accountid", $pageVars->accountid);
			if ($qry->execute($con)) {
				$messages[] = DELETED;
			}
		}
		if (buttonClicked("send")) {
			
			//work out if this email address already exists - if it does we will update its record else we wil insert a new one
			
			$e_qry = "select count(emailid) as cnt from a_email_visit where accountid = {$pageVars->accountid} and email = '".$email_addr."'";
			$eDb = new pageQry($con,$e_qry);
			$eDb->rsItem();
			//echo $e_qry;
			
			if ($eDb->row['cnt'] > 0) {
				$qry = new dbUpdate("a_email_visit");
				$qry->setStringParam("email",$_POST['email_addr']);
				$qry->setParam("accountid",$pageVars->accountid);
			} else {
				$qry = new dbInsert("a_email_visit");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
			} 
			
			$qry->setReqStringVal("email",$_POST['email_addr'],"email address");
			$qry->setStringVal("title",$_POST['email_addr_title'],"email address title");
			
		
			$qry->setAudit($clsUS->userid);
			//echo $qry->getSql();
			//die();
			if ($qry->execute($con)) {
				
				$messages[] = SAVED;
				
				//now send the email
				//get email title and message
				$text = $_POST['email_message'];
				$subject = $_POST['email_title'];
				$from = $clsUS->username;
				$to = $_POST['email_addr'];
				
				if (isset($_POST['email_uploads'])) {
					$email_uploads = getIntFromRequest('email_uploads');
				} else {
					$email_uploads = 0;
				}
				if (isset($_POST['email_comments'])) {
					$email_comments = getIntFromRequest('email_comments');
				} else {
					$email_comments = 0;
				}
				
				
				$visit_email = new clsEmail($to,$alertsMail,$subject,$text, $from, $alertsMail);
				
				//need to get all stages for this visit
				
				$qry = "select a_form.formid, a_form.stage, a_form.formname, a_form.formtext from a_form left join a_visit on a_form.planid = a_visit.planid where a_form.stage <= a_visit.stage and a_form.state = 'ACTIVE' and visitid = {$pageVars->visitid} order by stage asc";
				$stageDb = new pageQry($con, $qry);
				$stageDb->rsList();
				$cnt = 1;
				foreach ($stageDb->rows as $row) {
					$type = "text/html";
					$filename = "Stage-{$row['stage']}:{$row['formname']}.html";
					$token=getEmailVisitToken($con, $pageVars->userid, $pageVars->visitid);
					
					$file = "$protocol://localhost/modules/lotc/get_email_visit.php?emailtoken=$token&userid={$pageVars->userid}&visitid={$pageVars->visitid}&stage={$row['stage']}&uploads=$email_uploads&comments=$email_comments";

					
					$visit_email->addFileAttachment($filename,$type,$file);
					
					//now if uploads box checked, attach uploads for this form
					if ($email_uploads) {
						$upload_qry = "select files.* from a_lotc_files left join files on a_lotc_files.fileid = files.fileid where a_lotc_files.accountid = {$pageVars->accountid} and formid = {$row['formid']} and visitid = {$pageVars->visitid}";
						$uploadsDb = new pageQry($con, $upload_qry);
						$uploadsDb->rsList();
						foreach ($uploadsDb->rows as $upload_row) {
						
						
							//use path not url for uploaded files. less problematical!
							$type = $upload_row["filetype"];
							$filename = $upload_row["filename"];
							
							if ($type == "handsamDoc") {
								$path = $handsamdocs_fp;
							} else if ($type == "adminDoc") {
								$path = $admindocs_fp;
							} else if ($type == "userDoc") {
								$path = $userdocs_fp;
							} else if ($type == "accountLotcDoc") {
								$path = $accountLotcDocs_fp;
							} else if ($type == "accountLotcUserDoc") {
								$path = $accountLotcUserDocs_fp;
							}

							$file = $path.$filename;
		
						
							$filename = "Stage-{$row['stage']}:{$row['formname']}:{$upload_row['filename']}";
							
							$type = $visit_email->get_mimetype($upload_row['filename']);
							$visit_email->addBinaryFileAttachment($filename,$type,$file);
					
						}
					}
				}
				//echo $visit_email->attachments;
				//die();
				$log_qry = new dbInsert("a_email_visit_log");
				$log_qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
				$log_qry->setReqNumberVal("formid",$row['formid'],"accountid");
				$log_qry->setReqNumberVal("visitid",$pageVars->visitid,"visitid");
				$log_qry->setReqNumberVal("uploads",$email_uploads,"uploads");
				$log_qry->setReqNumberVal("comments",$email_comments,"comments");
				$log_qry->setReqStringVal("email_addr",$to,"comments");
				$log_qry->setReqStringVal("subject",$subject,"subject");
				$log_qry->setStringVal("message",$text,"message");
				$log_qry->setReqNumberVal("created_by",$pageVars->userid,"created_by");
				$log_qry->addValue("created","NOW()");
				
				//echo $visit_email->getEmailBody();
				//die();
				if (sendMailObj($visit_email)) {
					$messages[] = "Mail sent";
				} else {
					$log_qry->setReqStringVal("state","FAILED","state");
					$messages[] = "Error sending message";
					
				}
				
				if (!$log_qry->execute($con)) {
					$messages[] = "Error logging email";
				}
				
				//echo "evc.php?accountid=$pageVars->accountid&userid=$pageVars->userid&visitid=$pageVars->visitid&filter=".urlencode(serialize($qs));
				//die();
				//need to get url for each form up to current stage and attach to email
				
				headerLocation("evc.php?accountid=$pageVars->accountid&userid=$pageVars->userid&visitid=$pageVars->visitid&filter=".urlencode(serialize($qs)), $messages);				
				//list visits
			} else {
				$messages[] = $qry->getError();	
			}

		} else if (buttonClicked("cancel")) {
				//echo "<a href=\"evc.php?accountid=$pageVars->accountid&userid=$pageVars->userid&visitid=$pageVars->visitid&filter=".urlencode(serialize($qs))."\">here</a>";
				//die();
			headerLocation("evc.php?accountid=$pageVars->accountid&userid=$pageVars->userid&visitid=$pageVars->visitid&filter=".urlencode(serialize($qs)), false);
		}
	
				
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>