<?php

/**
 * Class to do database stuff on a page
 */
class userApproval {

    var $evc = array();
    var $finance = array();
    var $principal = array();
    var $dep_principal = array();
    var $governor = array();

    function userApproval($lotc_conf, $con, $accountid, $type) {
        if ($lotc_conf['finance_approval_field'] === true && in_array('finance', $type)) {
            $this->getFinance($con, $accountid);
        }
        if ($lotc_conf['principal_approval_field'] === true && in_array('principal', $type)) {
            $this->getPrincipal($con, $accountid);
        }
        if ($lotc_conf['deputy_principal_approval_field'] === true && in_array('dep_principal', $type)) {
            $this->getDepPrincipal($con, $accountid);
        }
        if ($lotc_conf['governor_approval_field'] === true && in_array('governor', $type)) {
            $this->getGovernor($con, $accountid);
        }
        $this->getEVC($con, $accountid);
        return $this;
    }

    function getFinance($con, $accountid) {
        $this->finance = $this->getUsers($con, $accountid, 'LOTC Finance');
    }

    function getPrincipal($con, $accountid) {
        $this->principal = $this->getUsers($con, $accountid, 'LOTC Principal');
    }

    function getDepPrincipal($con, $accountid) {
        $this->dep_principal = $this->getUsers($con, $accountid, 'LOTC Deputy Principal');
    }
    function getGovernor($con, $accountid) {
        $this->governor = $this->getUsers($con, $accountid, 'LOTC Governor');
    }

    function getEVC($con, $accountid) {
        $this->evc = $this->getUsers($con, $accountid, 'evc');
    }

    function getUsers($con, $accountid, $usertype) {
        $qry = "select usr.* from usr left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.accountid = $accountid and usr_usrtype.usertype = '$usertype' and usr.state = 'ACTIVE' ";
        $rs = getRS($con, $qry);
        $result = array();
        while ($row = getRow($rs)) {
            $result[] = $row;
        }
        return $result;
    }

}

