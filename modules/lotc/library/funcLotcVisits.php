<?php

/* functions for lotc planning application */

//gets list of fields for a form
function getVisitList($pageVars, $con, $filter_array, $all_visits = false) {
    //need to include secutity here. If a planner, only view own visits
    //implode filter array
    $filter_qry = implode(" and ", $filter_array);
    if ($filter_qry != "")
        $filter_qry = " and " . $filter_qry;
    $security_qry = "";
    if ($all_visits) {
        $security_qry = " and a_visit.accountid = {$pageVars->accountid} ";
    } else if (isAllowed("planVisits")) {
        $security_qry = " and a_visit.created_by = {$pageVars->userid} ";
        //$security_qry = " and a_visit.created_by = {$pageVars->userid} or (a_visit.accountid = {$pageVars->accountid} and a_visit.state = 'COMPLETED') ";
    } else if (isAllowed("editVisits")) {
        $security_qry = " and a_visit.accountid = {$pageVars->accountid} ";
    }
    $qry = "SELECT  a_visit.visit_date, a_plan.planname, a_visit.visitid, a_visit.visitname, a_visit.state, a_visit.created, c.latest_visit_state, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) AS stages,";
    $qry .= " (CASE WHEN a_visit.stage > 0 THEN a_visit.stage ELSE MIN( CASE WHEN a_form.state = 'ACTIVE' THEN a_form.stage END ) END) AS stage  ";
    $qry .= " from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid ";

    //$qry .= " LEFT JOIN latest_visit_state ON  a_visit.visitid = latest_visit_state.visitid ";

    $qry .= " left join (SELECT a.visitid, b.state AS latest_visit_state FROM (SELECT visitid, MAX(form_state_id) AS max_state_id FROM a_form_state GROUP by visitid) a, a_form_state b WHERE a.max_state_id = b.form_state_id) c ON a_visit.visitid = c.visitid ";


    $qry .= " where a_visit.state != '' $security_qry $filter_qry  and a_visit.state != 'SUPER_DELETED' GROUP by a_visit.visit_date, a_visit.visitid, a_visit.visitname, a_visit.state, a_visit.created, c.latest_visit_state";

    //echo $qry;
    //die();
    $pageDb = new pageQry($con, $qry);
    $pageDb->rsList();
    return $pageDb;
}

function getAdminVisitList($pageVars, $con, $filter_array, $all_visits = false) {
    //need to include secutity here. If a planner, only view own visits
    //implode filter array
    $filter_qry = implode(" and ", $filter_array);
    if ($filter_qry != "")
        $filter_qry = " and " . $filter_qry;
    $security_qry = "";
    
    $qry = "SELECT  a_visit.visit_date, account.accountname, a_plan.planname, a_visit.visitid, a_visit.visitname, a_visit.state, a_visit.created, c.latest_visit_state, SUM(CASE WHEN a_form.state = 'ACTIVE' then 1 else 0 end) AS stages,";
    $qry .= " (CASE WHEN a_visit.stage > 0 THEN a_visit.stage ELSE MIN( CASE WHEN a_form.state = 'ACTIVE' THEN a_form.stage END ) END) AS stage  ";
    $qry .= " from a_visit left join a_plan on a_visit.planid = a_plan.planid left join a_form on a_plan.planid = a_form.planid ";

    //$qry .= " LEFT JOIN latest_visit_state ON  a_visit.visitid = latest_visit_state.visitid ";

    $qry .= " left join (SELECT a.visitid, b.state AS latest_visit_state FROM (SELECT visitid, MAX(form_state_id) AS max_state_id FROM a_form_state GROUP by visitid) a, a_form_state b WHERE a.max_state_id = b.form_state_id) c ON a_visit.visitid = c.visitid ";
	$qry .= " left join account on a_visit.accountid = account.accountid ";
	
    $qry .= " where a_visit.state != '' $security_qry $filter_qry  and a_visit.state != 'SUPER_DELETED' GROUP by a_visit.visit_date, a_visit.visitid, a_visit.visitname, a_visit.state, a_visit.created, c.latest_visit_state";

//    echo $qry;
    //die();
    $pageDb = new pageQry($con, $qry);
    $pageDb->rsList();
    return $pageDb;
}

//function to update a_form_state table

function updateFormState($con, $clsUS, $visitid, $formid, $state = "") {
    global $messages;
    $qry = new dbInsert('a_form_state');
    $qry->setReqStringVal("state", $state, "state");
    $qry->setReqNumberVal("visitid", $visitid, "visitid");
    $qry->setReqNumberVal("formid", $formid, "formid");
    $qry->setAudit($clsUS->userid);
    //echo $qry->getSql();
    if (!$qry->execute($con)) {
        $messages[] = $qry->getError();
        return false;
    } else {
        return true;
    }
}

//function to update visit table state
function updateVisitState($con, $clsUS, $visitid, $state = "") {
    global $messages;
    $qry = new dbUpdate('a_visit');
    $qry->setParam("visitid", $visitid);
    $qry->setReqStringVal("state", $state, "state");
    $qry->setAudit($clsUS->userid);
    if (!$qry->execute($con)) {
        $messages[] = $qry->getError();
        return false;
    } else {
        return true;
    }
}

//function to update visit table stage
function updateVisitStage($con, $clsUS, $visitid, $stage) {
    global $messages;
    $qry = new dbUpdate('a_visit');
    $qry->setParam("visitid", $visitid);
    $qry->setReqStringVal("stage", $stage, "stage");
    $qry->setAudit($clsUS->userid);
    if (!$qry->execute($con)) {
        $messages[] = $qry->getError();
        return false;
    } else {
        return true;
    }
}

//moves on to next stage in plan or updates visit to complete if last stage. also checks for if any higher stages already exist. If they do then we are dealing with a unsubmiited approval and need to update visit stage too thee highest one
function nextStage($con, $stage, $visitid) {
    global $messages;
    //first check form_state table for the highest stage for this visit
    $sql = "SELECT MAX(stage) as latest_stage FROM a_form_state LEFT JOIN a_form ON a_form_state.formid = a_form.formid WHERE visitid = $visitid";
    $rsMaxStage = getRS($con, $sql);
    $rowStage = getRow($rsMaxStage);

    $stage = $rowStage['latest_stage'];

    //get next stage:
    $qry = "select a_form.stage from a_form left join a_visit on a_form.planid = a_visit.planid where a_form.stage > $stage and a_form.state = 'ACTIVE' and visitid = $visitid order by stage limit 0,1";
    $rs = getRS($con, $qry);
    $row = getRow($rs);

    if ($row['stage'] > 0) {

        $qry = new dbUpdate('a_visit');
        $qry->setParam("visitid", $visitid);
        $qry->setReqNumberVal("stage", $row['stage'], "stage");
        if (!$qry->execute($con)) {
            $messages[] = $qry->getError();
            $messages[] = $qry->getSql();
            return false;
        } else {
            //$messages[] = $qry->getSql();
            return true;
        }
    } else {
        $qry = new dbUpdate('a_visit');
        $qry->setParam("visitid", $visitid);
        $qry->setReqStringVal("state", "COMPLETED", "state");
        if (!$qry->execute($con)) {
            $messages[] = $qry->getError();
            return false;
        } else {
            return true;
        }
    }
}

function previousStage($con, $stage, $visitid){
   //if the unsubmit button pressed, returns the previous stage
   global $messages;
    //first check form_state table for the highest stage for this visit
    $sql = "SELECT MAX(stage) as latest_stage FROM a_form_state LEFT JOIN a_form ON a_form_state.formid = a_form.formid WHERE visitid = $visitid";
    $rsMaxStage = getRS($con, $sql);
    $rowStage = getRow($rsMaxStage);

    $stage = $rowStage['latest_stage'];
	if ($stage == 1){
	   //we don't need to roll back as on first stage
	} else {
	  //we need to go back a stage
	  $qry = new dbUpdate('a_visit');
        $qry->setParam("visitid", $visitid);
		$new_stage = $row['stage'] -1;
        $qry->setReqNumberVal("stage", $new_stage, "stage");
        if (!$qry->execute($con)) {
            $messages[] = $qry->getError();
            $messages[] = $qry->getSql();
            return false;
        } else {
            //$messages[] = $qry->getSql();
            return true;
        }
	}
}

//display crumbtrail of the visit
function visitCrumbtrail($con, $stage, $pageVars, $qs, $back = "") {
    $qry = "select a_form.stage, a_form.formname, a_form.formtext from a_form left join a_visit on a_form.planid = a_visit.planid where a_form.stage <= a_visit.stage and a_form.state = 'ACTIVE' and visitid = {$pageVars->visitid} order by stage asc";
    $crumbDb = new pageQry($con, $qry);
    $crumbDb->rsList();
    $str = "<ul id=\"visit_crumb\">";
    if ($back != "")
        $back = "<li class=\"stage1\">$back<li>";
    $str .= $back;
    $class = "";
    $cnt = 0;
    foreach ($crumbDb->rows as $row) {
        $class = "";
        if ($cnt == 0 && $back == "")
            $class .= " stage1 ";
        if ($row['stage'] == $stage)
            $class .= " this_stage ";
        $str .= "<li class=\"$class\"><a href=\"?accountid={$pageVars->accountid}&amp;visitid={$pageVars->visitid}&amp;stage={$row['stage']}&amp;rand=" . rand() . "&amp;filter=" . urlencode(serialize($qs)) . "\" title=\"{$row['formtext']}\">{$row['formname']}</a></li>";
        $cnt++;
    }
    $str .= "</ul>";
    return $str;
}

//checks if there is any data for this visit. 
function dataInVisit($con, $visitid) {
    if ($visitid == 0)
        return true;
    $sql = "select (";
    $sql_join = "";
    $cnt = 0;
    for ($i = 1; $i <= 8; $i++) {
        if ($cnt > 0)
            $sql .= " + ";
        $sql .= "count(df$i.data_id) ";
        $sql_join .= "left join data_field_type_$i df$i on a_field.fieldid = df$i.fieldid and df$i.visitid = $visitid ";
        $cnt++;
    }
    $sql .= ") as cnt from a_field left join a_form on a_field.formid = a_form.formid  LEFT JOIN a_plan ON a_form.planid = a_plan.planid LEFT JOIN a_visit ON a_visit.planid = a_plan.planid  $sql_join ";
    $sql .= " where  a_field.state = 'ACTIVE' and a_visit.visitid = $visitid";
    $sql .= " order by a_field.field_order";
    $result = new pageQry($con, $sql);
    $result->rsItem();

    if ($result->row['cnt'] > 0)
        return false;
    return true;
}

//get comments for a form or a whole visit. Need to supply appid to get the usertype for this app.
function getComments($con, $visitid, $formid = 0) {
    if ($formid > 0) {
        $sql = "SELECT usr.firstname, usr.lastname, a_comments.*, usrtype.usrtype FROM a_comments LEFT JOIN usr ON a_comments.created_by = usr.userid LEFT JOIN usr_usrtype ON usr.userid = usr_usrtype.userid LEFT JOIN usrtype ON usr_usrtype.usertype = usrtype.usrtype WHERE visitid = $visitid AND formid = $formid AND usrtype.appid = 2";
    } else {
        $sql = "SELECT usr.firstname, usr.lastname, a_comments.*, usrtype.usrtype FROM a_comments LEFT JOIN usr ON a_comments.created_by = usr.userid LEFT JOIN usr_usrtype ON usr.userid = usr_usrtype.userid LEFT JOIN usrtype ON usr_usrtype.usertype = usrtype.usrtype  WHERE visitid = $visitid AND usrtype.appid = 2 ";
    }
    //echo $sql;
    $result = new pageQry($con, $sql);
    $result->rsList();
    return $result;
}

//checks if a user is alowed to add comments to a visit
function is_allowed_add_comment($con, $pageVars, $stage) {
    $sql = "select a_visit.accountid, a_visit.created_by, a_visit.state from a_visit where a_visit.visitid = {$pageVars->visitid} and a_visit.stage = $stage";
    $result = new pageQry($con, $sql);
    $result->rsItem();
    //check if user is the planner of the visit and visit state = planning
    if (isAllowed("planVisits") && $result->row['created_by'] == $pageVars->userid && $result->row['state'] == 'PLANNING')
        return true;
    //check if user is the account evc and visit in approval state
    if ($result->row['accountid'] == $pageVars->accountid && isAllowed('editVisits') && $result->row['state'] == 'APPROVAL')
        return true;
    if ($result->row['accountid'] == $pageVars->accountid && isAllowed('financeApproval') && $result->row['state'] == 'FINANCE APPROVAL')
        return true;
    if ($result->row['accountid'] == $pageVars->accountid && isAllowed('principalApproval') && $result->row['state'] == 'PRINCIPAL APPROVAL')
        return true;
    if ($result->row['accountid'] == $pageVars->accountid && isAllowed('deputyPrincipalApproval') && $result->row['state'] == 'DEPUTY PRINCIPAL APPROVAL')
        return true;
    if ($result->row['accountid'] == $pageVars->accountid && isAllowed('governorApproval') && $result->row['state'] == 'GOVERNOR APPROVAL')
        return true;
    return false;
}

//check if allowed to edit comment
function is_allowed_edit_comment($con, $pageVars, $commentid, $stage) {
    $sql = "select a_comments.created_by, a_visit.state from a_comments left join a_visit on a_comments.visitid = a_visit.visitid where commentid = $commentid and a_visit.stage = $stage";
    $result = new pageQry($con, $sql);
    $result->rsItem();
    //check if user is the author of the comment
    if ($result->row['created_by'] == $pageVars->userid && $result->row['state'] == "APPROVAL" && isAllowed("editVisits"))
        return true;
    if ($result->row['created_by'] == $pageVars->userid && $result->row['state'] == "PLANNING" && isAllowed("planVisits"))
        return true;
    return false;
}

//get changes of state for a visit form so we can see when it was submitted, approved or declined
function form_state($con, $visitid, $formid) {
    $sql = "select a_form_state.*, usr.firstname, usr.lastname, usrtype.usrtype from a_form_state LEFT JOIN usr ON a_form_state.created_by = usr.userid LEFT JOIN usr_usrtype ON usr.userid = usr_usrtype.userid LEFT JOIN usrtype ON usr_usrtype.usertype = usrtype.usrtype where visitid = $visitid and formid = $formid AND usrtype.appid = 2";
    //echo $sql;
    $result = new pageQry($con, $sql);
    $result->rsList();
    return $result;
}

//returns state changes for all stages in visit
function visit_state($con, $visitid, $state = "") {
    if ($state != "")
        $state = "and state = '$state'";
    $sql = "select a_form_state.* from a_form_state where visitid = $visitid $state";
    //echo $sql;
    $result = new pageQry($con, $sql);
    $result->rsList();
    return $result;
}

//get uploads for a form or a whole visit.
function getUploads($con, $pageVars, $formid = 0) {
    if ($formid > 0) {
        $sql = "SELECT af.fileid,f.filename,  f.title, '' AS url, f.filecategory as category, 'file' AS resource FROM a_lotc_files af LEFT JOIN files f ON f.fileid = af.fileid WHERE af.accountid = {$pageVars->accountid} and filetype = 'accountLotcUserDoc' and visitid = {$pageVars->visitid} and formid = $formid order by title asc";
    } else {
        $sql = "SELECT af.fileid,f.filename, f.title, '' AS url, f.filecategory as category, 'file' AS resource FROM a_lotc_files af LEFT JOIN files f ON f.fileid = af.fileid WHERE af.accountid = {$pageVars->accountid} and filetype = 'accountLotcUserDoc' and visitid = {$pageVars->visitid} order by title asc";
    }
    //echo $sql;
    $result = new pageQry($con, $sql);
    $result->rsList();
    return $result;
}

function getEmailVisitToken($con, $userid, $visitid) {
    $token = md5(genRandomString(16));
    $qry = new dbInsert('lotc_email_trip_token');
    $qry->setReqStringVal("token", $token, "token");
    $qry->setReqNumberVal("userid", $userid, "userid");
    $qry->setReqNumberVal("visitid", $visitid, "visitid");
    if ($qry->execute($con)) {
        return $token;
    }
}

function genRandomString($length) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyz';
    $str_len = strlen($characters) - 1;
    $string = '';
    for ($p = 0; $p < $length; $p++) {
        $r = mt_rand(0, $str_len);
        $string .= $characters[$r];
    }
    return $string;
}

function checkEmailVisitToken($con, $token, $userid, $visitid) {
    $qry = "select * from lotc_email_trip_token where visitid=$visitid and userid = $userid and token = '$token'";
    $rs = getRS($con, $qry);
    if (getResultRows($rs) > 0) {
        $qry = new dbDelete('lotc_email_trip_token');
        $qry->setParam("visitid", $visitid);
        $qry->setParam("userid", $userid);
        $qry->setStringParam("token", $token);
        if ($qry->execute($con)) {
            return true;
        }
    }
}

?>