$.extend({


	popUp : function (settings, popupLoadedCalback, callbackfunc) {
		settings = jQuery.extend({
			txt: 'none'
		},settings);
		//alert (settings.delete_url);
		//CLOSING POPUP
		//Click the x event!
		$("#popupClose").on("click", function(){
			disablePopup();
		});
		
		//Click out event!
		$("#backgroundPopup").on("click", function(){
			disablePopup();
		});
		
		//Press Escape event!
		$(document).keypress(function(e){
			if(e.keyCode==27 && popupStatus==1){
				disablePopup();
			}
		});	

		
		function writePopup() {
			if ($.browser.msie && $.browser.version.substr(0,1)<7) $('#content select').hide();
			if ($("#popup").length > 0) return '';
			var txt;
			txt = "<div id='popup'>";
			txt += "<a id='popupClose'>close[x]</a>";
			txt += "<div id='inner_popup'></div>";
			txt += "</div>";
			txt += "<div id='backgroundPopup'>";
			txt += "</div>";
			return txt;
		}
			
			
		var popupStatus = 0; 

				
		function loadPopup(){
		//loads popup only if it is disabled
			if(popupStatus==0){
				$("#backgroundPopup").css({
					"opacity": "0.7"
				});
				$("#backgroundPopup").fadeIn("slow");
				$("#popup").fadeIn("slow");
				popupStatus = 1;
				
			}
			$.post(settings.load_url, settings.params, function(data){
	  			$("#inner_popup").html(data);
	  			formSubmit();
			});	

			//$('#inner_popup').load(settings.load_url + settings.params , '', formSubmit);

		}	
		
		
		//disabling popup with jQuery magic!
		function disablePopup(){
			if ($.browser.msie && $.browser.version.substr(0,1)<7) $('#content select').show();
			//disables popup only if it is enabled
			if(popupStatus==1){
				$("#backgroundPopup").fadeOut("slow");
				$("#popup").fadeOut("slow");
				popupStatus = 0;
				$('#inner_popup').html('');
				$("#popup").empty().remove();
				$("#backgroundPopup").remove();
			}
			return false;
		}
		
		//centering popup
		function centerPopup(){
			//request data for centering
			var arrPageSizes = ___getPageSize();
			var arrPageScroll = ___getPageScroll();
			
			var windowWidth = document.documentElement.clientWidth;
			var windowHeight = document.documentElement.clientHeight;
			var popupHeight = $("#popup").height() + 100;
			var popupWidth = $("#popup").width();
			//centering
			$("#popup").css({
				"position": "absolute",
				"top":	arrPageScroll[1] + (arrPageSizes[3] / 10),
				"left":	arrPageScroll[0] +(arrPageSizes[2] / 3),
				"height": 	popupHeight	
			});
			//only need force for IE6
			
			$("#backgroundPopup").css({
				"height": pageHeight
			});

		}	
		
		/**
		 * getPageSize() by quirksmode.com
		 *
		 * @return Array Return an array with page width, height and window width, height
		 */

		function ___getPageSize() {
			var xScroll, yScroll;
			if (window.innerHeight && window.scrollMaxY) {	
				xScroll = window.innerWidth + window.scrollMaxX;
				yScroll = window.innerHeight + window.scrollMaxY;
			} else if (document.body.scrollHeight > document.body.offsetHeight){ // all but Explorer Mac
				xScroll = document.body.scrollWidth;
				yScroll = document.body.scrollHeight;
			} else { // Explorer Mac...would also work in Explorer 6 Strict, Mozilla and Safari
				xScroll = document.body.offsetWidth;
				yScroll = document.body.offsetHeight;
			}
			var windowWidth, windowHeight;
			if (self.innerHeight) {	// all except Explorer
				if(document.documentElement.clientWidth){
					windowWidth = document.documentElement.clientWidth; 
				} else {
					windowWidth = self.innerWidth;
				}
				windowHeight = self.innerHeight;
			} else if (document.documentElement && document.documentElement.clientHeight) { // Explorer 6 Strict Mode
				windowWidth = document.documentElement.clientWidth;
				windowHeight = document.documentElement.clientHeight;
			} else if (document.body) { // other Explorers
				windowWidth = document.body.clientWidth;
				windowHeight = document.body.clientHeight;
			}	
			// for small pages with total height less then height of the viewport
			if(yScroll < windowHeight){
				pageHeight = windowHeight;
			} else { 
				pageHeight = yScroll;
			}
			// for small pages with total width less then width of the viewport
			if(xScroll < windowWidth){	
				pageWidth = xScroll;		
			} else {
				pageWidth = windowWidth;
			}
			arrayPageSize = new Array(pageWidth,pageHeight,windowWidth,windowHeight);
			return arrayPageSize;
		};		
		
		/**
		 * getPageScroll() by quirksmode.com
		 *
		 * @return Array Return an array with x,y page scroll values.
		 */
		function ___getPageScroll() {
			var xScroll, yScroll;
			if (self.pageYOffset) {
				yScroll = self.pageYOffset;
				xScroll = self.pageXOffset;
			} else if (document.documentElement && document.documentElement.scrollTop) {	 // Explorer 6 Strict
				yScroll = document.documentElement.scrollTop;
				xScroll = document.documentElement.scrollLeft;
			} else if (document.body) {// all other Explorers
				yScroll = document.body.scrollTop;
				xScroll = document.body.scrollLeft;	
			}
			arrayPageScroll = new Array(xScroll,yScroll);
			return arrayPageScroll;
		};

		function formSubmit() {
	
			if(typeof popupLoadedCalback == 'function'){
				popupLoadedCalback.call(this);
			}
			
			var btn;
			$('#popup form input[type=submit]').click(function(){
				btn = '&' + this.name + '=' + this.value;
				//alert("clicked");
	        });			
			$('#popup form')
	        .submit(function () { 
		        alert("submit" + btn);
		        //return false;
				var str = $('#popup form').serialize();
				var submit_str = str + btn;
				response = $.ajax({
				      url: settings.load_url,
				      async:		false,
				      global: false,
				      type: "POST",
				      data: submit_str,
				      dataType: "html",
				      success: function(msg){
				         //alert(settings.load_url + "?" + str);
				         
				      }
				   }
				).responseText;
				if (response.search("##ERROR") < 0) {
					if(response == 'cancel') {
						disablePopup();
					} else if(response == 'deleted') {
						str += "&load=yes" + response;
						txt = $.ajax({
							  url: settings.load_url,
							  async:		false,
							  global: false,
							  type: "GET",
							  data: str,
							  dataType: "html",
							  success: function(msg){
								 disablePopup();
								 //alert(settings.load_url + "?" + str);
								 
							  }
						   }
						).responseText;
						alert('Deleted');
						$(settings.elem).html(txt);
						$(this).unbind();
						
						if(typeof callbackfunc == 'function'){
							callbackfunc.call(this);
						}
						/*
						alert('Deleted');
						$(settings.elem).html(txt);
						$(this).unbind();
						
						if(typeof callbackfunc == 'function'){
							callbackfunc.call(this);
						}
						//window.location = settings.delete_url;
						*/
					} else {
						str += "&load=yes" + response;
						//alert(str);
						//alert(settings.load_url);
						txt = $.ajax({
							  url: settings.load_url,
							  async:		false,
							  global: false,
							  type: "POST",
							  data: str,
							  dataType: "html",
							  success: function(msg){
								 disablePopup();
								 //alert(settings.load_url + "?" + str);
								 
							  }
						   }
						).responseText;
						alert('Savedddddd');
						$(settings.elem).html(txt);
						$(this).unbind();
						if(typeof callbackfunc == 'function'){
							callbackfunc.call(this);
						}
					}
				
				} else {

					alert(response.replace('##ERROR:', 'Some details were missing: \n'));

				}
				//$(this).unbind();
				return false; 
			});
		}
		
		$("body").prepend(writePopup());
		//alert(settings.txt);
		centerPopup();
		//load popup
		loadPopup();						
	}
	
});	