<?php

	//gets list of fields and data for a visit stage
	function getLotcForm($con, $stage, $visitid, $deleted_fields=false) {
		$qry = dataJoinsStage($stage, $visitid, $deleted_fields);
		echo $qry;
		$formDb = new pageQry($con, $qry);
		$formDb->rsList();
		return $formDb;
		
	}
	
	
	//gets the query and title states
	function getLotcStates($state]){
	  switch ($state) {
	     case '0': 
			$state['qry'] = " a_visit.state != 'DELETED' ";
			$state['title'] = "All";
		 break;
		 case "DEFAULT":
			$state['qry'] = " (a_visit.state = 'PLANNING' or  a_visit.state = 'APPROVAL') ";
			$state['title'] = "Planning &amp; Approval";
		 break;
    
		 case "ALL":
			$state['qry'] = " a_visit.state != 'DELETED' ";
			$state['title'] = "All States";
		 break;
		
		 case "PLANNING":
			$state['qry'] = " a_visit.state = 'PLANNING' ";
			$state['title'] = "Planning";
		 break;
		 case "FINANCE APPROVAL":
			$state['qry'] = " a_visit.state = 'FINANCE APPROVAL' ";
			$state['title'] = "Finance Approval";
		 break;
		 case "PRINCIPAL APPROVAL":
			$state['qry'] = " a_visit.state = 'PRINCIPAL APPROVAL' ";
			$state['title'] = "Principal Approval";
		 break;
		 case "DEPUTY PRINCIPAL APPROVAL":
			$state['qry'] = " a_visit.state = 'DEPUTY PRINCIPAL APPROVAL' ";
			$state['title'] = "Deputy Principal Approval";
		 break;
		 case "GOVERNOR APPROVAL":
            $state['qry'] = " a_visit.state = 'GOVERNOR APPROVAL' ";
            $state['title'] = "Governor Approval";
         break;
         case "APPROVAL":
			$state['qry'] = " a_visit.state = 'APPROVAL' ";
			$state['title'] = "Approval";
		 break;
		 case "COMPLETED":
			$state['qry'] = " a_visit.state = 'COMPLETED' ";
			$state['title'] = "Completed";
		 break;
		 case "DELETED":
			$state['qry'] = " a_visit.state = 'DELETED' ";
			$state['title'] = "Deleted";
		 break;
		 default: 
			$state['qry'] = " (a_visit.state = 'PLANNING' or  a_visit.state = 'APPROVAL') ";
			$state['title'] = "Planning &amp; Approval";
			break;
	  }
	  return $state;
	}
	
	
	//gets query for joining all data tables supplied with stage
	function dataJoinsStage($stage, $visitid, $deleted_fields=false) {
		if ($deleted_fields) $deleted_fields = " or a_field.state = 'DELETED' ";
		$sql = "select distinct a_visit.visitid, a_field.fieldid, a_field.params, a_field.field_typeid, a_field.fieldname, a_field.fieldtext, a_field.required, a_field.default_value ";
		$sql_join = "";
		for ($i=1; $i<=8; $i++) {
			$sql .= ", df$i.data_$i, df$i.data_id as data_id_$i ";
			$sql_join .= "left join data_field_type_$i df$i on a_field.fieldid = df$i.fieldid and df$i.visitid = $visitid ";
		}
		$sql .= " from a_field left join a_form on a_field.formid = a_form.formid  LEFT JOIN a_plan ON a_form.planid = a_plan.planid LEFT JOIN a_visit ON a_visit.planid = a_plan.planid LEFT JOIN a_form_state ON a_form.formid = a_form_state.formid AND a_visit.visitid = a_form_state.visitid AND a_form_state.state = 'APPROVED EVC' $sql_join ";
		$sql .= " where a_form.stage = $stage and (a_field.state = 'ACTIVE' $deleted_fields) and a_visit.visitid = $visitid AND (a_field.created < a_form_state.created OR a_form_state.created IS NULL) ";
		$sql .= " order by a_field.field_order";
		return $sql;
	}
	
	
	//gets query for returning JUST data for a form 
	function dataForm($stage) {
		$sql = "select ";
		$sql_join = "";
		for ($i=1; $i<=8; $i++) {
			if ($i>1) $sql .= " , ";
			$sql .= "(case when df$i.data_$i = '' then 0 else 1 end) as cnt";
			$sql_join .= "left join data_field_type_$i df$i on a_field.fieldid = df$i.fieldid ";
		}
		$sql .= " from a_field left join a_form on a_field.formid = a_form.formid $sql_join where a_form.stage = $stage and a_field.state = 'ACTIVE'";
		return $sql;
	}


	
	//inserts data for a form field into db
	function editFormDb($row,$con,$pageVars,$submit) {
		global $messages;
    if ($row['field_typeid'] ==9 || $row['field_typeid'] ==10 || $row['field_typeid'] ==11 || $row['field_typeid'] ==12) return true;
		$dataDb = $row["data_{$row['field_typeid']}"];
		$dataTbl = "data_field_type_{$row['field_typeid']}";
		$data_id = $row["data_id_{$row['field_typeid']}"];
		$dataFrm = (isset($_POST["f_{$row['fieldid']}"])) ? $_POST["f_{$row['fieldid']}"] : '';
		if ($row['field_typeid'] == 5) {
			//date time so add hrs and mins to date field
			$hrs = $_POST["f_{$row['fieldid']}_hrs"];
			$hrs = sprintf("%02d", $hrs);
			$mins = $_POST["f_{$row['fieldid']}_mins"];
			$mins = sprintf("%02d", $mins);
			$dataFrm .= " {$hrs}:{$mins}:00";
		}
		//first check if the form $_POST is diferent from the db result
		if (checkPost($dataDb,$dataFrm)) {
			if ($data_id > 0) {
				$qry = new dbUpdate($dataTbl);
				$qry->setParam("visitid",$pageVars->visitid);
				$qry->setParam("fieldid",$row['fieldid']);
			} else {
				$qry = new dbInsert($dataTbl);
				$qry->setReqNumberVal("visitid",$pageVars->visitid,"visit id");
				$qry->setReqNumberVal("fieldid",$row['fieldid'],"field id");
			}
			
			
			$qry->setReqNumberVal("field_typeid",$row['field_typeid'],"field type id");
			$qry->setReqStringVal("fieldname",$row['fieldname'],"fieldname");
			if ($submit) {
				//required fields must be entered
				$qry = dataQry($qry, $row['field_typeid'], $dataFrm, $row['fieldname'], $row['required']);
			} else {
				//not submitting for approval so required fields can be filled in again
				$qry = dataQry($qry, $row['field_typeid'], $dataFrm, $row['fieldname'], 0);
			}
				
			
			//echo $qry->getSql()."<br/><br/>";
			if ($qry->execute($con)) {
				
				return true;	
			} else {
				$messages[] = $qry->getError();
				return false;

			}
			
		} else {
			return true;		
		}
	}	
	
	//check if database form field equals post
	function checkPost($dataFrm, $dataDb) {
		if (trim($dataDb) != trim($dataFrm)) return true;
		if (trim($dataDb) == "" && trim($dataFrm) == "") return true;
		
		return false;
	}
	
	
	
	//query for data field in daabase 
	function dataQry($qry, $id, $val, $fieldname, $required) {
		switch ($id) {
			case 1:
				if ($required) {
					$qry->setReqNumberVal("data_$id",$val,$fieldname);
				} else {
					$qry->setNumberVal("data_$id",$val,$fieldname);
				}
			break;
			
			case 2:
				if ($required) {
					$qry->setReqStringVal("data_$id",$val,$fieldname);
				} else {
					$qry->setStringVal("data_$id",$val,$fieldname);
				}
			break;
			
			case 3:
				if ($required) {
					$qry->setReqStringVal("data_$id",$val,$fieldname);
				} else {
					$qry->setStringVal("data_$id",$val,$fieldname);
				}
			break;
			
			case 4:
				if ($required) {
					$qry->setReqStringVal("data_$id",$val,$fieldname);
				} else {
					$qry->setStringVal("data_$id",$val,$fieldname);
				}
			break;
			
			//datetime
			case 5:
				if ($required) {
					$qry->setReqDateTimeVal("data_$id",$val,$fieldname);
				} else {
					$qry->setDateTimeVal("data_$id",$val,$fieldname);
				}
			break;
			
			//date
			case 6:
				if ($required) {
					$qry->setReqDateVal("data_$id",$val,$fieldname);
				} else {
					$qry->setDateVal("data_$id",$val,$fieldname);
				}
			break;
			
			//checkbox
			case 7:
				if ($required) {
					$qry->setReqNumberVal("data_$id",$val,$fieldname);
				} else {
					$qry->setNumberVal("data_$id",$val,$fieldname);
				}
			break;
			
			//select box
			case 8:
				if ($required) {
					$qry->setReqStringVal("data_$id",$val,$fieldname);
				} else {
					$qry->setStringVal("data_$id",$val,$fieldname);
				}
			break;
		}
		return $qry;
			
			
	}
?>