<?php
/**
 * Class to do database stuff on a page
 */
class pageQry {

	var $qry;
	var $con;
	var $rs;
	var $rows = array();
	var $row;
	var $cnt;

	function pageQry($con, $qry) {
		$this->con = $con;
		$this->qry = $qry;
		$this->rs = getRS($con,$qry);
		$this->cnt = getResultRows($this->rs);
		
	}
	
	
	function rsExists() {
		if ($this->cnt > 0) return true;
	}
	
	
	function rsList() {
		while ($row = getRow($this->rs)) {
			$this->rows[] = $row;
		}
	}
	
	function rsItem() {
		$this->row = getRow($this->rs);
	}
}
	
