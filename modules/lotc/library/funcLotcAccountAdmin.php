<?php
/*functions for lotc application*/

//changes order of form stage in form.php using arrows in list view
	function changeFormStage($pageVars, $con) {
		global $messages;
		if($pageVars->uid > 0) {
			//move form up one stage
			
			$stage_qry = "Update a_form set stage = stage - 1 where formid = {$pageVars->uid} and stage > 1 and planid = {$pageVars->planid}";
			$exec = execSQL($con, $stage_qry); 
			if ($exec != "") {
				$messages[] = $exec;
			} else {
				//now need to get stage we have just replaced
				$qry1 = "select stage from  a_form where formid = {$pageVars->uid}";
				$rs1 = getRS($con,$qry1);
				$row1 = getRow($rs1);
				//$stage_qry = "Update a_form set stage = stage + 1 where stage = (select stage from (select * from a_form) as x where formid = {$pageVars->uid})  and formid != {$pageVars->uid}  and planid = {$pageVars->planid}";
				$stage_qry = "Update a_form set stage = stage + 1 where stage = {$row1['stage']} and formid != {$pageVars->uid}  and planid = {$pageVars->planid}";
				$exec = execSQL($con, $stage_qry); 
			}
				
		}
		
		if($pageVars->did > 0) {
			//move form down one stage
			//get number of forms 
			$frmcnt_qry = "select formid from  a_form  where state != 'DELETED' and planid = {$pageVars->planid} ";
			$frmcntDb = new pageQry($con, $frmcnt_qry);
			$stage_qry = "Update a_form set stage = stage + 1 where formid = {$pageVars->did} and stage < {$frmcntDb->cnt} and planid = {$pageVars->planid}";
			$exec = execSQL($con, $stage_qry); 
			if ($exec != "") {
				$messages[] = $exec;
			} else {
				$qry1 = "select stage from  a_form where formid = {$pageVars->did}";
				$rs1 = getRS($con,$qry1);
				$row1 = getRow($rs1);
				//$stage_qry = "Update a_form set stage = stage - 1 where stage = (select stage from (select * from a_form) as x where formid = {$pageVars->did})  and formid != {$pageVars->did} and planid = {$pageVars->planid}";
				$stage_qry = "Update a_form set stage = stage - 1 where stage = {$row1['stage']}  and formid != {$pageVars->did} and planid = {$pageVars->planid}";
				$exec = execSQL($con, $stage_qry); 
				//echo $stage_qry;
				//die();
			}
		}	
		return $messages;
	}

	
//changes order of field_order in field.php using arrows in list view
	function changeFieldOrder($pageVars, $con) {
		global $messages;
		if($pageVars->uid > 0) {
			//move form up one stage
			
			$stage_qry = "Update a_field set field_order = field_order - 1 where fieldid = {$pageVars->uid} and field_order > 1 and formid = {$pageVars->formid}";
			$exec = execSQL($con, $stage_qry); 
			if ($exec != "") {
				$messages[] = $exec;
			} else {
				//now need to get field_order we have just replaced
				$qry1 = "select field_order from  a_field where fieldid = {$pageVars->uid}";
				$rs1 = getRS($con,$qry1);
				$row1 = getRow($rs1);
				$stage_qry = "Update a_field set field_order = field_order + 1 where field_order = {$row1['field_order']} and fieldid != {$pageVars->uid}  and formid = {$pageVars->formid}";
				$exec = execSQL($con, $stage_qry); 
			}
				
		}
		
		if($pageVars->did > 0) {
			//move form down one stage
			//get number of forms 
			$frmcnt_qry = "select fieldid from  a_field  where state != 'DELETED' and formid = {$pageVars->formid} ";
			$frmcntDb = new pageQry($con, $frmcnt_qry);
			$stage_qry = "Update a_field set field_order = field_order + 1 where fieldid = {$pageVars->did} and field_order < {$frmcntDb->cnt} and formid = {$pageVars->formid}";
			$exec = execSQL($con, $stage_qry); 
			if ($exec != "") {
				$messages[] = $exec;
			} else {
				$qry1 = "select field_order from  a_field where fieldid = {$pageVars->did}";
				$rs1 = getRS($con,$qry1);
				$row1 = getRow($rs1);
			$stage_qry = "Update a_field set field_order = field_order - 1 where field_order = {$row1['field_order']}  and fieldid != {$pageVars->did} and formid = {$pageVars->formid}";
				$exec = execSQL($con, $stage_qry); 
			}
		}	
		return $messages;
	}
	


//gets list of forms for a plan
	function getFormList($pageVars, $con) {
		$qry = "select a_form.state, a_form.formid, a_form.formname, a_form.created, a_form.stage, count(a_field.fieldid) as field_cnt from a_plan left join a_form on a_plan.planid = a_form.planid left join a_field on a_form.formid = a_field.formid AND (a_field.state != 'DELETED' OR a_field.state IS NULL) where a_plan.state != 'DELETED' and a_form.state != 'DELETED' and a_form.planid = {$pageVars->planid} and a_form.accountid = {$pageVars->accountid} group by a_form.state, a_form.formid, a_form.formname, a_form.created, a_form.stage  order by stage";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
	
//gets list of fields for a form
	function getFieldList($pageVars, $con) {
		$qry = "select a_field.*, field_type.type_title from a_field left join field_type on a_field.field_typeid = field_type.field_typeid where a_field.state != 'DELETED'  and a_field.formid = {$pageVars->formid} and a_field.accountid = {$pageVars->accountid} order by field_order";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	

//gets list of fields for a form
	function getPlanList($pageVars, $con) {
		$qry = "SELECT p.planid, p.accountid, p.m_planid, p.planname, p.plantext, p.created, p.created_by, p.edited, p.edited_by, p.state, COUNT(f.formid) AS form_cnt FROM a_plan p LEFT JOIN a_form f ON p.planid = f.planid  AND (f.state != 'DELETED') WHERE p.state != 'DELETED' AND p.accountid = {$pageVars->accountid} GROUP by p.planid, p.accountid, p.m_planid, p.planname, p.plantext, p.created, p.created_by, p.edited, p.edited_by, p.state";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		//echo $qry;
		return $pageDb;
	}
	

	
//check if an account plan has been used yet	
	function planUsed($pageVars, $con) {
		$qry = "Select count(a_visit.visitid) as cnt from a_visit where a_visit.planid = {$pageVars->planid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
		if ($pageDb->row['cnt'] > 0) {
			return true;
		} else {
			return false;
		}
		
	}
?>
