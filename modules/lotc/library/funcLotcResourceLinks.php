<?php
/*file upload function */
function lotcResourceLink($clsUS, $pageVars, $con, $linktype) {
	global $messages;
	if ($pageVars->linkid == 0) {
		$qry = new dbInsert("links");
		$qry->setReqStringVal("linktype",$linktype,"linktype");
	} else {
		$qry = new dbUpdate("links");
		$qry->setParam("linkid",$pageVars->linkid);
	}

	if (isset($qry)) {
		$qry->setReqStringVal("url",$_POST["url"],"URL");
		$qry->setReqStringVal("title",$_POST["title"],"Title");
		//$qry->setStringVal("description",$_POST["description"],"Description");
		$qry->setReqNumberVal("accountid",$pageVars->accountid,"Account id");
		$qry->setReqNumberVal("userid",$clsUS->userid,"User id");
		$qry->setStringVal("linkcategory",$_POST["linkcategory"],"Link Category");
		
		$qry->setAudit($clsUS->userid);
		
		if ($qry->execute($con)) {
			if ($pageVars->linkid == 0) {
				$pageVars->linkid = $qry->getNewID();	
			}
			$messages[] = SAVED;
			return $pageVars->linkid;
			#change content to list
			
		} else {
			$messages[] = $qry->getError();
			return false;
		}
	}
	return false;
}


function lotcDeleteLink($pageVars, $con) {
	global $messages;
	$qry = new dbDelete("links");
	if (!isAllowed("editMasterPlan")) $qry->setParam("accountid",$pageVars->accountid);
	$qry->setParam("linkid",$pageVars->linkid);
	if (!$qry->execute($con)) {
		$messages[] = $qry->getError();
		return false;
	} else {
		$messages[] = DELETED;
		return $pageVars->linkid;
	}

}


function addLotcLinkToAllAccounts($linkid,$con, $clsUS) {
	global $messages;
	//first get all accounts with lotc app
	
	$qry = "select distinct accountid from accountlicence where appid = 2";
	$rs = getRS($con, $qry);
	
	//delete this file from lotc so definitely no duplicates
	rmLotcLinkFromAllAccounts($linkid, $con);
	while ($row = getRow($rs)) {
		
		//now add the fileid to lotc_files
		$qry = new dbInsert("a_lotc_links");
		$qry->setReqNumberVal("linkid",$linkid,"linkid");
		$qry->setReqNumberVal("accountid",$row['accountid'],"accountid");
		$qry->setAudit($clsUS->userid);
		if (!$qry->execute($con)) {
			$messages[] = $qry->getError();
		} 	
	}

}



function rmLotcLinkFromAllAccounts($linkid, $con) {
	if (isAllowed("editMasterPlan")) {
		global $messages;
		$qry = new dbDelete("a_lotc_links");
		$qry->setParam("linkid",$linkid);
		if (!$qry->execute($con)) {
			$messages[] = $qry->getError();
			return false;
		}
		$messages[] = "Link removed from all LOtC accounts";
		return true;
	} else {
		return false;
	}
}

function rmLinkFromLotc($linkid, $con) {
	global $messages;
	if (rmLotcLinkFromAllAccounts($linkid, $con)) {
		$qry = new dbDelete("lotc_links");
		$qry->setParam("linkid",$linkid);
		if (!$qry->execute($con)) {
			$messages[] = $qry->getError();
			return false;
		}
		return true;
	}
	return false;
}	
?>