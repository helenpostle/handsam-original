<?php
/**
 * Class to load variables from GET, POST, user session depending on security
 */
class loadVars {

	var $userid;
	var $accountid;
	var $planid;
	var $formid;
	var $fieldid;
	var $uid;
	var $did;
	var $visitid;
	var $stage;
	var $commentid;
	var $fileid;
	var $action;
	var $ret;
	var $linkid;
	var $hacking = false;
	var $my_visit = false;
	var $my_account_visit = false;
	
	
	function loadVars($clsUS,$con) {
		if (isAllowed("editAccounts")) {
			$this->userid = getIntFromRequest("userid");
			$this->accountid = getIntFromRequest("accountid");
		} else {
			$this->userid = $clsUS->userid;
			$this->accountid = $clsUS->accountid;
		} 
		$this->planid = getIntFromRequest("planid");
		$this->formid = getIntFromRequest("formid");
		$this->fieldid = getIntFromRequest("fieldid");
		$this->uid = getIntFromRequest("uid");
		$this->did = getIntFromRequest("did");
		$this->visitid = $this->checkVisitId($clsUS,$con);
		$this->stage = getIntFromRequest("stage");
		$this->commentid = getIntFromRequest("commentid");
		$this->fileid = getIntFromRequest("fileid");
		$this->linkid = getIntFromRequest("linkid");
		$this->action = getStrFromRequest("action");
		$this->ret = getStrFromRequest("ret");
	}
	
	function checkVisitId($clsUS,$con) {
		$visitid = getIntFromRequest("visitid");
		if ($visitid === 0) return 0;
		if (isAllowed("editAccounts")) {
			return $visitid;
		} else if ($visitid > 0) {
			$qry = "select accountid, created_by from a_visit where visitid = $visitid";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if ($row['created_by'] == $clsUS->userid) $this->my_visit = true;
			if ($row['accountid'] == $clsUS->accountid) $this->my_account_visit = true;
			if ((isAllowed("governorView") || isAllowed("financeApproval") || isAllowed("deputyPrincipalApproval") || isAllowed("principalApproval") || isAllowed("editVisits") )&& $row['accountid'] == $clsUS->accountid){
				return $visitid;
			} else if (isAllowed("planVisits") && $row['created_by'] == $clsUS->userid){
				return $visitid;
			} else {
				$this->hacking = true;
			}
		}
		return null;
	}
}
	
?>