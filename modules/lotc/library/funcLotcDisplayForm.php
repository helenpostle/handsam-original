<?php
/*functions for displaying form fields*/

//display integer field - text input
function display_form_field_1($row) {
	echo lotcTextField("data_1", $row,"f_".$row['fieldid'],10,$row['fieldname'],$row['required'],$row['fieldtext']);
}

// display varchar 50 text input
function display_form_field_2($row) {
	echo lotcTextField("data_2", $row,"f_".$row['fieldid'],50,$row['fieldname'],$row['required'],$row['fieldtext']);
}

// display varchar 250 text input
function display_form_field_3($row) {
	echo lotcTextField("data_3", $row,"f_".$row['fieldid'],100,$row['fieldname'],$row['required'],$row['fieldtext']);
}

// display varchar unlimited text  text area
function display_form_field_4($row) {
	echo lotcTextArea("data_4", $row,"f_".$row['fieldid'],5,$row['fieldname'],$row['required'],$row['fieldtext']);
}


// display  datetime input
function display_form_field_5($row) {
	echo lotcTimeDateField("data_5", $row,"f_".$row['fieldid'],$row['fieldname'],$row['required'],$row['fieldtext']);
}

// display date input
function display_form_field_6($row) {
	echo lotcDateField("data_6", $row,"f_".$row['fieldid'],$row['fieldname'],$row['required'],$row['fieldtext']);
}

// display tinyint check box
function display_form_field_7($row) {
	echo lotcCheckBox("data_7", $row,"f_".$row['fieldid'],$row['fieldname'],$row['fieldtext']);
}


// display varchar 250 select box
function display_form_field_8($row) {
	echo lotcSelectBox("data_8", $row,"f_".$row['fieldid'],explode(",",$row['params'] ),$row['fieldname'],$row['required'],$row['fieldtext']);
}


// display finance user approval text
function display_form_field_9($row) {
  global $lotc_conf;
	if ($lotc_conf['finance_approval_field'])
  {
    echo "<div class='lotc_frm clearfix required'><label class=\"\" for='f_".$row['fieldid']."'>".$row['fieldname']."</label><div class='value'>".$row['fieldtext']."</div></div>";
  }
}


// display principle user approval text
function display_form_field_10($row) {
  global $lotc_conf;
	if ($lotc_conf['principal_approval_field'])
  {
    echo "<div class='lotc_frm clearfix required'><label class=\"\" for='f_".$row['fieldid']."'>".$row['fieldname']."</label><div class='value'>".$row['fieldtext']."</div></div>";
  }
}

// display dep principle user approval text
function display_form_field_11($row) {
  global $lotc_conf;
	if ($lotc_conf['deputy_principal_approval_field'])
  {
    echo "<div class='lotc_frm clearfix required'><label class=\"\" for='f_".$row['fieldid']."'>".$row['fieldname']."</label><div class='value'>".$row['fieldtext']."</div></div>";
  }
}

// display governor user approval text
function display_form_field_12($row) {
  global $lotc_conf;
	if ($lotc_conf['governor_approval_field'])
  {
    echo "<div class='lotc_frm clearfix required'><label class=\"\" for='f_".$row['fieldid']."'>".$row['fieldname']."</label><div class='value'>".$row['fieldtext']."</div></div>";
  }
}






/*special form functions to display lotc form fields - have added optional extra text field*/
function lotcRow($ctrl,$title,$field,$req,$extra_text) {
	if ($req == true) {
		$class = "required";
		$extra_text .= "<span>*required field</span>";
	} else {
		$class = "optional";
	}
	if (trim($extra_text) != "") {
		$label_class = "extra_info";
		$extra_text = "<p>$extra_text</p>";
	} else {
		$label_class = "";
	}
	$str = "<div class='lotc_frm clearfix ".$class."'><div class=\"extra_text\"><label class=\"$label_class\" for='".$field."'>".$title."</label>$extra_text</div>".$ctrl;
	if ($ctrl == "") $str .= "&nbsp;";
	$str .= "</div>\n";
	return $str;
}

/**
 * A little function which returns the posted value if set else the db value
 * Useful for redisplaying data on a form even if errors stopped it from being saved.
 * Assumes that the database field has the same name as the form field.
 */
function lotcValue($rs,$db_field, $frm_field) {
	if (isset($_POST[$frm_field])) {
		//only strip slashes from post if they were added automagically.
		if (get_magic_quotes_gpc()==1) {
			return stripslashes($_POST[$frm_field]);
		} else {
			return $_POST[$frm_field];
		}
	} else if(isset($_GET[$frm_field])) {
		//only strip slashes from post if they were added automagically.
		
		return $_GET[$frm_field];
	
		
	} else {
		//always strip slashes from database
		return stripslashes($rs[$db_field]);
	}
}

//same ass above but modified for hrs of datetime  field

function lotcTimeValue($rs,$db_field, $frm_field, $part) {
	if (isset($_POST[$frm_field.$part]) && is_numeric($_POST[$frm_field.$part])) {
		//only strip slashes from post if they were added automagically.
		if (get_magic_quotes_gpc()==1) {
			return stripslashes($_POST[$frm_field.$part]);
		} else {
			return $_POST[$frm_field.$part];
		}
	} else if(isset($_GET[$frm_field.$part]) && is_numeric($_GET[$frm_field])) {
		//only strip slashes from post if they were added automagically.
		
		return $_GET[$part.$frm_field.$part];
	
		
	} else {
		//always strip slashes from database
		if ($part == '_hrs') return strftime("%H",strtotime($rs[$db_field]));
		if ($part == '_mins') return strftime("%M",strtotime($rs[$db_field]));
	}
}


/**
 * A normal text field 
 */
function lotcTextField($db_field, $rs,$frm_field,$len,$title,$req,$extra_text) {
	$val = lotcValue($rs,$db_field, $frm_field);
	return lotcRow("<input type=\"text\" class=\"inputText\" name=\"$frm_field\" id=\"$frm_field\" value=\"$val\" maxlength=\"$len\"/>",$title,$frm_field,$req,$extra_text);
}


 /**
 * A text area field 
 */
function lotcTextArea($db_field, $rs,$field,$rows,$title,$req,$extra_text) {
	//if ($rs != "") {
		$val = lotcValue($rs,$db_field, $field);
	//} else {
	//	$val = "";
	//}
	return lotcRow("<textarea id=\"$field\" name=\"$field\" rows=\"$rows\" class=\"".frmClass($req,$field)."\">$val</textarea>",$title,$field,$req,$extra_text);
	
}


/**
 * A date field which uses a javascript calendar
 */
function lotcDateField($db_field, $rs,$field,$title,$req,$extra_text) { 
	global $rootUrl;
	$val = lotcValue($rs,$db_field, $field);
	if (strtotime($val) > 0) { 
		$date = strtotime($val);
	} else {
		$date = time();
	}
	$dcheck =  strftime("%a, %d %b %Y",$date);
	$ret = "<input name=\"$field\" id=\"$field\" readonly=\"true\" class=\"date\" value=\"$dcheck\"/>	";
	$ret .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.$field\"";
	/*
	$ret .= "<img  src=\"$rootUrl/handsam_core/functionality/form/js/cal/img.gif\" id=\"trig.$field\"";
	$ret .= "style=\"cursor: pointer; border: 0px;\"";
	$ret .= "title=\"Date selector\"/>";
		
	$ret .= "<script type=\"text/javascript\">";
	$ret .= "Calendar.setup({";
	$ret .= "inputField     :    '$field',";
	$ret .= "ifFormat       :    \"%a, %d %b %Y\",";
	$ret .= " button         :    'trig.$field',";
	$ret .= "step           :    1";
	$ret .= "});";
	$ret .= "</script>	";
	*/
	return lotcRow($ret,$title,$field,$req,$extra_text);	
}

/**
 * A date field which uses a javascript calendar
 */
 
 
function lotcTimeDateField($db_field, $rs,$field,$title,$req,$extra_text) { 
	global $rootUrl;
	$val = lotcValue($rs,$db_field, $field);
	$hrs = lotcTimeValue($rs,$db_field, $field, "_hrs");
	$mins = lotcTimeValue($rs,$db_field, $field, "_mins");
	if (strtotime($val) > 0) { 
		$date = strtotime($val);
	} else {
		$date = time();
	}
	$dcheck =  strftime("%a, %d %b %Y",$date);
	$ret = "<input name=\"$field\" id=\"$field\" readonly=\"true\" class=\"date_time\" value=\"$dcheck\"/>	";
	$ret .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.$field\"";
	$ret .= "<label class=\"hrs\" for=\"{$field}_hrs\">Hrs(24) </label><input maxlength=\"2\" name=\"{$field}_hrs\" id=\"{$field}_hrs\"  class=\"time\" value=\"$hrs\"/>	";
	$ret .= "<label class=\"mins\" for=\"{$field}_mins\">Mins </label><input maxlength=\"2\" name=\"{$field}_mins\" id=\"{$field}_mins\" class=\"time\" value=\"$mins\"/>";
	
	/*
	$ret .= "<img  src=\"$rootUrl/handsam_core/functionality/form/js/cal/img.gif\" id=\"trig.$field\"";
	$ret .= "style=\"cursor: pointer; border: 0px;\"";
	$ret .= "title=\"Date selector\"/>";
		
	$ret .= "<script type=\"text/javascript\">";
	$ret .= "Calendar.setup({";
	$ret .= "inputField     :    '$field',";
	$ret .= "ifFormat       :    \"%a, %d %b %Y\",";
	$ret .= " button         :    'trig.$field',";
	$ret .= "step           :    1";
	$ret .= "});";
	$ret .= "</script>	";
	*/
	
	return lotcRow($ret,$title,$field,$req,$extra_text);	
}



/**
 * A True/False Check box
 * Requires global variables to determine value for true and false
 */
function lotcCheckBox($db_field, $rs,$field,$title,$extra_text) {
	global $booleanTrue;
	global $booleanFalse;
	
	$val = lotcValue($rs,$db_field, $field);
	$str = "<input class=\"checkbox\" type=\"checkbox\" name=\"$field\" id=\"$field\" value=\"$booleanTrue\"";
	if ($val == $booleanTrue) $str .= " checked";
	$str .= "/>";
	return lotcRow($str,$title,$field,false,$extra_text);
}

/**
 * A select box populated by an array
 */
function lotcSelectBox($db_field, $rs,$field,$array,$title,$req,$extra_text) {
	$val = lotcValue($rs,$db_field, $field);
	$str = "<select name=\"$field\"  id=\"$field\" class=\"".frmClass($req,$field)."\">\n";
	$str .= "<option value=\"\">Pick $title</option>";
	foreach ($array as $text) {
	   	$str .= "<option value=\"".trim($text)."\"";
		if (trim($text) == trim($val)) {
			$str .= " selected";
		}
		$str .= ">".trim($text)."</option>";
      }

	$str .= "</select>";
	return lotcRow($str,$title,$field,$req,$extra_text);
}



?>