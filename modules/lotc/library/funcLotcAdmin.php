<?php
/*functions for lotc application*/

//changes order of form stage in form.php using arrows in list view
	function changeFormStage($pageVars, $con) {
		global $messages;
		if($pageVars->uid > 0) {
			//move form up one stage
			
			$stage_qry = "Update m_form set stage = stage - 1 where formid = ".$pageVars->uid." and stage > 1 and planid = ".$pageVars->planid;
			$exec = execSQL($con, $stage_qry); 
			
			if ($exec != "") {
				$messages[] = $exec;
			} else {
				//now need to get stage we have just replaced
				$qry1 = "select stage from  m_form where formid = ".$pageVars->uid;
				$rs1 = getRS($con,$qry1);
				$row1 = getRow($rs1);
				
				//$stage_qry = "Update m_form set stage = stage + 1 where stage = (select stage from (select * from m_form) as x where formid = {$pageVars->uid})  and formid != {$pageVars->uid}  and planid = {$pageVars->planid}";
				
				$stage_qry = "Update m_form set stage = stage + 1 where stage = ".$row1['stage']." and formid != ".$pageVars->uid."  and planid = ".$pageVars->planid;
				
				$exec = execSQL($con, $stage_qry); 
				//die($stage_qry);
			}
				
		}
		
		if($pageVars->did > 0) {
			//move form down one stage
			//get number of forms 
			$frmcnt_qry = "select formid from  m_form  where state != 'DELETED' and planid = ".$pageVars->planid;
			$frmcntDb = new pageQry($con, $frmcnt_qry);
			$stage_qry = "Update m_form set stage = stage + 1 where formid = ".$pageVars->did." and stage < ".$frmcntDb->cnt." and planid = ".$pageVars->planid;
			$exec = execSQL($con, $stage_qry); 
			if ($exec != "") {
				$messages[] = $exec;
			} else {
				$qry1 = "select stage from  m_form where formid = ".$pageVars->did;
				$rs1 = getRS($con,$qry1);
				$row1 = getRow($rs1);
				//$stage_qry = "Update m_form set stage = stage - 1 where stage = (select stage from (select * from m_form) as x where formid = {$pageVars->did})  and formid != {$pageVars->did} and planid = {$pageVars->planid}";
				$stage_qry = "Update m_form set stage = stage - 1 where stage = ".$row1['stage']."  and formid != ".$pageVars->did." and planid = ".$pageVars->planid;
				$exec = execSQL($con, $stage_qry); 
				//die($stage_qry);
			}
		}	
		return $messages;
	}

	
//changes order of field_order in field.php using arrows in list view
	function changeFieldOrder($pageVars, $con) {
		global $messages;
		if($pageVars->uid > 0) {
			//move form up one stage
			
			$stage_qry = "Update m_field set field_order = field_order - 1 where fieldid = ".$pageVars->uid." and field_order > 1 and formid = ".$pageVars->formid;
			$exec = execSQL($con, $stage_qry); 
			if ($exec != "") {
				$messages[] = $exec;
			} else {
				//now need to get field_order we have just replaced
				$qry1 = "select field_order from  m_field where fieldid = ".$pageVars->uid;
				$rs1 = getRS($con,$qry1);
				$row1 = getRow($rs1);

				$stage_qry = "Update m_field set field_order = field_order + 1 where field_order = ".$row1['field_order']." and fieldid != ".$pageVars->uid."  and formid = ".$pageVars->formid;
				
				
				//$stage_qry = "Update m_field set field_order = field_order + 1 where field_order = (select field_order from (select * from m_field) as x where fieldid = ".$pageVars->uid.")  and fieldid != ".$pageVars->uid." and formid = ".$pageVars->formid;
				$exec = execSQL($con, $stage_qry); 
				$messages = $exec;
			}
				
		}
		
		if($pageVars->did > 0) {
			//move form down one stage
			//get number of forms 
			$frmcnt_qry = "select fieldid from  m_field  where state != 'DELETED' and formid = ".$pageVars->formid;
			$frmcntDb = new pageQry($con, $frmcnt_qry);
			$stage_qry = "Update m_field set field_order = field_order + 1 where fieldid = ".$pageVars->did." and field_order < ".$frmcntDb->cnt." and formid = ".$pageVars->formid;
			$exec = execSQL($con, $stage_qry); 
			if ($exec != "") {
				$messages[] = $exec;
			} else {
				
				$qry1 = "select field_order from  m_field where fieldid = ".$pageVars->did;
				$rs1 = getRS($con,$qry1);
				$row1 = getRow($rs1);
				
				$stage_qry = "Update m_field set field_order = field_order - 1 where field_order = ".$row1['field_order']."  and fieldid != ".$pageVars->did." and formid = ".$pageVars->formid;
			
			
				//$stage_qry = "Update m_field set field_order = field_order - 1 where field_order = (select field_order from (select * from m_field) as x where fieldid = ".$pageVars->did.")  and fieldid != ".$pageVars->did." and formid = ".$pageVars->formid;
				$exec = execSQL($con, $stage_qry); 
				$messages = $exec;
			}
		}	
		return $messages;
	}



//gets list of forms for a plan
	function getFormList($pageVars, $con) {
		$qry = "select m_form.state, m_form.formid, m_form.formname, m_form.created, m_form.stage, count(m_field.fieldid) as field_cnt from m_plan left join m_form on m_plan.planid = m_form.planid left join m_field on m_form.formid = m_field.formid AND (m_field.state != 'DELETED' OR m_field.state IS NULL) where m_plan.state != 'DELETED' and m_form.state != 'DELETED' and m_form.planid = {$pageVars->planid} group by m_form.state, m_form.formid, m_form.formname, m_form.created, m_form.stage  order by stage";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
	
//gets list of fields for a form
	function getFieldList($pageVars, $con) {
		$qry = "select m_field.*, field_type.type_title from m_field left join field_type on m_field.field_typeid = field_type.field_typeid where m_field.state != 'DELETED'  and m_field.formid = {$pageVars->formid} order by field_order";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	

//gets list of fields for a form
	function getPlanList($pageVars, $con) {
		$qry = "select m_plan.planid, m_plan.planname, m_plan.plantext, m_plan.created, m_plan.edited, m_plan.state, m_plan.created_by, m_plan.edited_by, count(m_form.formid) as form_cnt from m_plan left join m_form on m_plan.planid = m_form.planid AND (m_form.state != 'DELETED' OR m_form.state IS NULL) where m_plan.state != 'DELETED' GROUP by m_plan.planid, m_plan.planname, m_plan.plantext, m_plan.created, m_plan.edited, m_plan.state, m_plan.created_by, m_plan.edited_by";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
//gets list of lotc guidance docs
	function getLotcGuidanceList($con) {
		$qry = "SELECT lotc_files.created, files.title, files.filename, files.fileid, files.filecategory FROM lotc_files LEFT JOIN files ON files.fileid = lotc_files.fileid WHERE files.filetype = 'handsamDoc' ORDER BY files.title,files.filename asc";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
	
//gets list of lotc guidance links
	function getLotcGuidanceLinksList($con) {
		$qry = "SELECT links.url,lotc_links.created, links.title, links.title, links.linkid, links.linkcategory FROM lotc_links LEFT JOIN links ON links.linkid = lotc_links.linkid WHERE links.linktype = 'handsamDoc' ORDER BY links.title Asc ";
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
	
//gets list of all handsam guidance docs
	function getHandsamGuidanceList($con) {
		$qry = "SELECT files.created, files.title, files.filename, files.fileid, files.filecategory, (CASE WHEN lotc_files.fileid > 0 then 1 else 0 end) AS added FROM files LEFT JOIN lotc_files ON files.fileid = lotc_files.fileid WHERE files.filetype = 'handsamDoc' ORDER BY files.title,files.filename Asc ";
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
//gets list of all handsam guidance links
	function getHandsamGuidanceLinksList($con) {
		$qry = "SELECT links.created, links.title, links.url, links.linkid, links.linkcategory, (CASE WHEN lotc_links.linkid > 0 then 1 else 0 end) AS added FROM links LEFT JOIN lotc_links ON links.linkid = lotc_links.linkid WHERE links.linktype = 'handsamDoc' ORDER BY links.title Asc ";
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
//gets list of lotc guidance docs for an account
	function getAccountLotcGuidanceList($con, $accountid) {
		$qry = "SELECT lotc_files.created, files.title, files.filename, files.fileid, files.filecategory, (CASE WHEN a_lotc_files.fileid > 0 then 1 else 0 end) AS added FROM lotc_files LEFT JOIN files ON files.fileid = lotc_files.fileid left join a_lotc_files on lotc_files.fileid = a_lotc_files.fileid and a_lotc_files.accountid = $accountid WHERE files.filetype = 'handsamDoc' ORDER BY files.title,files.filename asc";
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
//gets list of lotc guidance links for an account
	function getAccountLotcGuidanceLinksList($con, $accountid) {
		$qry = "SELECT lotc_links.created, links.title, links.url, links.linkid, links.linkcategory, (CASE WHEN a_lotc_links.linkid > 0 then 1 else 0 end) AS added FROM lotc_links LEFT JOIN links ON links.linkid = lotc_links.linkid left join a_lotc_links on lotc_links.linkid = a_lotc_links.linkid and a_lotc_links.accountid = $accountid WHERE links.linktype = 'handsamDoc' ORDER BY links.title asc";
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
	
//gets list of custom resource docs for an account
	function getResourceDocsList($con, $accountid) {
		$qry = "SELECT files.created, files.title, files.filename, files.fileid, files.filecategory  FROM files WHERE files.filetype = 'accountLotcDoc' and accountid = $accountid ORDER BY files.title,files.filename asc";
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
//gets list of custom resource docs for an account
	function getResourceLinksList($con, $accountid) {
		$qry = "SELECT links.created, links.title, links.url, links.linkid, links.linkcategory  FROM links WHERE links.linktype = 'accountLotcDoc' and accountid = $accountid ORDER BY links.title asc";
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList(); 
		return $pageDb;
	}
	
?>
