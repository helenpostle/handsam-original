<?php
/*functions for displaying form fields*/

//display integer field - text input
function display_visit_field_1($row) {
	echo lotcViewText($row,"data_1",$row['fieldname'],$row['fieldtext']);
}

// display varchar 50 text input
function display_visit_field_2($row) {
	echo lotcViewText($row,"data_2",$row['fieldname'],$row['fieldtext']);
}

// display varchar 250 text input
function display_visit_field_3($row) {
	echo lotcViewText($row,"data_3",$row['fieldname'],$row['fieldtext']);
}

// display varchar unlimited text  text area
function display_visit_field_4($row) {
	echo lotcViewText($row,"data_4",$row['fieldname'],$row['fieldtext']);
}


// display  datetime input
function display_visit_field_5($row) {
	if ($row['data_5']) {
		echo lotcViewDateTime($row, "data_5",$row['fieldname'],$row['fieldtext']);
	} else {
		echo lotcViewText($row,"data_5",$row['fieldname'],$row['fieldtext']);
	}
}

// display date input
function display_visit_field_6($row) {
	if ($row['data_6']) {
		echo lotcViewDateTime($row, "data_6",$row['fieldname'],$row['fieldtext']);
	} else {
		echo lotcViewText($row,"data_6",$row['fieldname'],$row['fieldtext']);
	}
}

// display tinyint check box
function display_visit_field_7($row) {
	echo lotcViewCheckBox($row, "data_7",$row['fieldname'],$row['fieldtext']);
}


// display varchar 250 select box
function display_visit_field_8($row) {
	echo lotcViewText($row,"data_8",$row['fieldname'],$row['fieldtext']);
}



/**
 * A div with the supplied control and label 
 */

function lotcViewData($ctrl,$title,$extra_text) {
	if (trim($extra_text) != "") {
		$label_class = "label extra_info";
		$extra_text = "<p>$extra_text</p>";
	} else {
		$label_class = "label";
	}
	$str = "<div class='lotc_frm clearfix '><div class=\"extra_text\"><span class=\"$label_class\">".$title."</span>$extra_text</div>".$ctrl;
	if ($ctrl == "") $str .= "&nbsp;";
	$str .= "</div>\n";
	return displayText($str);
}


 /**
 * A display data in a div
 */
function lotcViewText($rs,$field,$title,$extra_text) {
	$val = "<div class=\"value\">".nl2br($rs[$field])."&nbsp;</div>";
	return lotcViewData($val,$title,$extra_text);
	
}

 /**
 * A display date data in a div
 */
function lotcViewDate($rs,$field,$title,$extra_text) {
	$val = "<div class=\"value\">".displayLotcDate($rs[$field])."</div>";
	return lotcViewData($val,$title,$extra_text);
	
}

 /**
 * A display date data in a div
 */
function lotcViewDateTime($rs,$field,$title,$extra_text) {
	$val = "<div class=\"value\">".displayLotcDateTime($rs[$field])."</div>";
	return lotcViewData($val,$title,$extra_text);
	
}



/**
 * A True/False Check box
 * Requires global variables to determine value for true and false
 */
function lotcViewCheckBox($rs,$field,$title,$extra_text) {
	global $booleanTrue;
	global $booleanFalse;
	global $rootUrl;
	
	$val = $rs[$field];
	if ($val == $booleanTrue){
		$str = "<img src=\"$rootUrl/images/checkbox_yes.jpg\" alt=\"check box ticked\"/>";
	} else {
		$str = "<img src=\"$rootUrl/images/checkbox_no.jpg\" alt=\"check box unticked\"/>";
	}
	
	return lotcViewData($str,$title,$extra_text);
}

function displayLotcDate($dbdate) {
	global $lotcDateFormat;
	$date = strtotime($dbdate);
	$retDate =  strftime($lotcDateFormat,$date);
	return $retDate;
	
}

function displayLotcDateTime($dbdate) {
	global $lotcDateTimeFormat;
	$date = strtotime($dbdate);
	$dcheck =  strftime($lotcDateTimeFormat,$date);
	return $dcheck;
	
}
?>