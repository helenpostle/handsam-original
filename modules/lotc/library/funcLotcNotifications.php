<?php
/*notification functions*/

function notifyEvc($con, $pageVars) {
	global $alertsMail;
	global $messages;
	global $debug;
	global $rootUrl;
	
	//get visit details
	$qry = "select a_visit.*, usr.username, usr.firstname, usr.lastname from a_visit left join usr on a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid}";
	$visitDb = new pageQry($con, $qry);
	$visitDb->rsItem();
	
	
	//get evc user details from db
	$qry = "select usr.* from usr left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.accountid = {$pageVars->accountid} and usr_usrtype.usertype = 'evc' and usr.state = 'ACTIVE' ";
	$evcDb = new pageQry($con,$qry);
	$evcDb->rsList();
	
	foreach ($evcDb->rows as $row) {
		$to = $row['username'];
		$from = $alertsMail;
		$subject = "A LOtC plan has been submitted for approval";
    $link = $rootUrl.'/modules/lotc/evc.php?visitid='.$pageVars->visitid;
    $message = getEmailTemplate('lotc_notify_evc', $visitDb->row['visitname'], $visitDb->row['stage'], $visitDb->row['firstname'], $visitDb->row['lastname'], $visitDb->row['username'], $link);
		
		if ($debug) {
			$messages[] = "to: $to";
			$messages[] = "from: $from";
			$messages[] = "subject: $subject";
			$messages[] = "message: $message";
		}
		
		//log email sending
		$log_qry = new dbInsert("notification_log");
		$log_qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
		$log_qry->setReqNumberVal("stage",$visitDb->row['stage'],"stage");
		$log_qry->setReqNumberVal("visitid",$pageVars->visitid,"visitid");
		$log_qry->setReqStringVal("email_addr",$to,"comments");
		$log_qry->setReqStringVal("subject",$subject,"subject");
		$log_qry->setStringVal("message",$message,"message");
		$log_qry->setReqNumberVal("created_by",$pageVars->userid,"created_by");
		$log_qry->addValue("created","NOW()");		
		
		if (sendMail($to, $from, $subject, $message)) {
			$messages[] = "EVC Notification sent";
		} else {
			$messages[] = "EVC Notification FAILURE";
			$log_qry->setReqStringVal("state","FAILED","state");
		}
		if (!$log_qry->execute($con)) {
			$messages[] = "Error logging email";
		}				
	}		
}

function notifyPlanner($con, $pageVars, $state, $by='EVC') {
	global $alertsMail;
	global $messages;
	global $debug;
	global $rootUrl;
	
	//get visit details
	$qry = "select a_visit.*, usr.username, usr.firstname, usr.lastname from a_visit left join usr on a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid}";
	$visitDb = new pageQry($con, $qry);
	$visitDb->rsItem();
		
	$to = $visitDb->row['username'];
	$from = $alertsMail;
	$subject = "A LOtC plan has been $state by the $by";
  $link = $rootUrl.'/modules/lotc/lotc.php?visitid='.$pageVars->visitid;
  $message = getEmailTemplate('lotc_notify_planner', $visitDb->row['visitname'], $visitDb->row['stage'], $state, $by, $visitDb->row['firstname'].' '.$visitDb->row['lastname'], $visitDb->row['username'], $link);
	
	if ($debug) {
		$messages[] = "to: $to";
		$messages[] = "from: $from";
		$messages[] = "subject: $subject";
		$messages[] = "message: $message";
	}
	
	//log email sending
	$log_qry = new dbInsert("notification_log");
	$log_qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
	$log_qry->setReqNumberVal("stage",$visitDb->row['stage'],"stage");
	$log_qry->setReqNumberVal("visitid",$pageVars->visitid,"visitid");
	$log_qry->setReqStringVal("email_addr",$to,"comments");
	$log_qry->setReqStringVal("subject",$subject,"subject");
	$log_qry->setStringVal("message",$message,"message");
	$log_qry->setReqNumberVal("created_by",$pageVars->userid,"created_by");
	$log_qry->addValue("created","NOW()");		
	
	if (sendMail($to, $from, $subject, $message)) {
		$messages[] = "Planner Notification sent";
	} else {
		$messages[] = "Planner Notification FAILURE";
		$log_qry->setReqStringVal("state","FAILED","state");
	}
	if (!$log_qry->execute($con)) {
		$messages[] = "Error logging email";
	}				
		
}


function notifyUser($con, $pageVars, $users, $type) {
	global $alertsMail;
	global $messages;
	global $debug;
	global $rootUrl;
	
	//get visit details
	$qry = "select a_visit.*, usr.username, usr.firstname, usr.lastname from a_visit left join usr on a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid}";
	$visitDb = new pageQry($con, $qry);
	$visitDb->rsItem();
	
	foreach ($users as $row) {
		$to = $row['username'];
		$from = $alertsMail;
		$subject = "A LOtC plan has been submitted for approval";
                $link = $rootUrl."/modules/lotc/$type.php?visitid=".$pageVars->visitid;
                $message = getEmailTemplate("lotc_notify_$type", $visitDb->row['visitname'], $visitDb->row['stage'], $visitDb->row['firstname'], $visitDb->row['lastname'], $visitDb->row['username'], $link);
		
		if ($debug) {
			$messages[] = "to: $to";
			$messages[] = "from: $from";
			$messages[] = "subject: $subject ($type approval)";
			$messages[] = "message: $message";
		}
		
		//log email sending
		$log_qry = new dbInsert("notification_log");
		$log_qry->setReqNumberVal("accountid",$pageVars->accountid,"accountid");
		$log_qry->setReqNumberVal("stage",$visitDb->row['stage'],"stage");
		$log_qry->setReqNumberVal("visitid",$pageVars->visitid,"visitid");
		$log_qry->setReqStringVal("email_addr",$to,"comments");
		$log_qry->setReqStringVal("subject","$subject ($type approval)","subject");
		$log_qry->setStringVal("message",$message,"message");
		$log_qry->setReqNumberVal("created_by",$pageVars->userid,"created_by");
		$log_qry->addValue("created","NOW()");		
		
		if (sendMail($to, $from, $subject, $message)) {
			$messages[] = ucwords($type)." Notification sent";
		} else {
			$messages[] = ucwords($type)." Notification FAILURE";
			$log_qry->setReqStringVal("state","FAILED","state");
		}
		if (!$log_qry->execute($con)) {
			$messages[] = "Error logging email";
		}				
	}		
}
?>