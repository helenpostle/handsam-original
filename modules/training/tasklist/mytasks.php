<?php
/**
* My tasks Page
*/
$this_page = "mytasks.php";

//include basic stuff
$secure = true;
include ("../../shared/startPage.php");


//make this a secure page

$pageFiles->addModFunc('tasklist_licence_archive');
if ($tasklist_conf['collapse_mytask_resources'] === true) $pageFiles->addModFunc('tasklist_resources');
$pageFiles->addFunc('handsam_uploads');
$pageFiles->addModFunc('tasklist_task_edit');

$pageFiles->includePhp();


if(buttonClicked('cancelSearch'))
{
  $_POST['task_search'] = '';
}

$task_search  = getStrFromRequest("task_search");
$task_search_url = '';
if(trim($task_search) != '' )
{
  $task_search_url = urlencode($task_search);
}
//specify page content
$title = "My Tasks : ";		//the <title> tag and <h1> page title
$return = getStrFromRequest("return");
//if (!isAllowed('editTask') && isAllowed('myTasks')) {
if (isAllowed('myTasks')) {

	$userid = $clsUS->userid;
	$accountid = $clsUS->accountid;
	#check for month, year, options and set to all if none
	$month = getIntFromRequest("month");
	$year = getIntFromRequest("year");
	$option = getStrFromRequest("option");
	//if ($option == "now" && !isset($month)) {
	//	$month = 0;
	//	$year = 0;
	//}
	$category = getIntFromRequest("category");
	$accounttaskid = getIntFromRequest("accounttaskid");
	$category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') ";
	
	//get licenceid for this page

	$licenceid = setQSInt("licenceid");
	$title .= licence_archive_title($licenceid, $accountid, $con);
	if ($licenceid == 0 || !isLicence($licenceid)) {
		if ($option == "completed") {
			$licence_state = "COMPLETED";
		} else {
			$licence_state = "ACTIVE";
		}
		//get current active licenceid
		/*
		$licence_qry = "SELECT DISTINCT accountlicence.licenceid ";
		$licence_qry .= "FROM tasklist LEFT JOIN task ON tasklist.tasklistid = task.tasklistid LEFT JOIN accounttask ON task.taskid = accounttask.taskid ";
		$licence_qry .= "left join accountlicence on accounttask.licenceid = accountlicence.licenceid ";
		$licence_qry .= "WHERE accounttask.assignedto = $userid AND accounttask.state = '$licence_state' ";
		$licence_qry .= "GROUP by accountlicence.licenceid ";
		//$licence_qry = "select licenceid from accountlicence where start_date < NOW() and end_date > NOW() and accountid = $accountid";
		*/
		$licence_qry = "SELECT distinct accountlicence.licenceid FROM  accounttask LEFT JOIN accountlicence ON accounttask.licenceid = accountlicence.licenceid WHERE accounttask.assignedto = $userid AND (accounttask.state = 'ACTIVE' or accounttask.state = 'COMPLETED') AND accountlicence.end_date > now()";		
		$licence_result = getRS($con, $licence_qry);
		$licArr = array();
		while ($licence_row = getRow($licence_result)) {
			$licArr[] = $licence_row['licenceid'];
		}
		$licArr[] = 'null';
		$licence_qry = implode(",", $licArr);
		
		$licenceid = $licArr[0];
		
	} else {
		$licence_qry = $licenceid;
	}
		
	
	//display licence_archive title

	
	
	if (!isset($month)) {
		$month = 13;
	}
	if (!isset($year)) {
		$year = 0;
	}
	if (!isset($option)) {
		$option = "uncompleted";
	}
	if (!isset($category)) {
		$category = 0;
	}
	
	#get category title
	if ($category > 0) {
		$qry = "SELECT * FROM taskcategory where categoryid = ".$category;
		$result = getRS($con, $qry);
		$row = getRow($result);
		$category_name = " : ".displayText($row['categoryname']);
	} else {
		$category_name = " : All Categories";
	}		

	 #is not a super admin so get list of late tasks for this user
		
	if (isset($accounttaskid)  && $accounttaskid > 0) {
		#check that accounttaskid is in your account

		$qryCheck = "select * from accounttask where accounttaskid = $accounttaskid and accountid = $accountid";
		$resultCheck = getRS($con, $qryCheck);
		if (getRow($resultCheck)) {
			$rowCheck = getRow($resultCheck);
			$content = "myTaskedit.php"; 
	
			if (buttonClicked("save") && $rowCheck['state'] != "COMPLETED") {
								
				$qry = new dbUpdate("accounttask");
				$qry->setParam("accounttaskid",$accounttaskid);
				if (isset($qry)) {
					if(isset($_POST['state'])) {
						$state = $_POST['state'];
					} else {
						$state = "";
					}
					if ($state == "COMPLETED") {
						$completed_date = strftime("%a, %d %b %Y",time());
						$qry->setReqNumberVal("completedby",$userid,"Completed By");
						$qry->setReqDateVal("completedate",$completed_date,"Complete Date");
					} else {
						$state = "ACTIVE";
						$completed_date = "NULL";
						$qry->setNullVal('completedby');
						$qry->setNullVal('completedate');
					}
					$completed_date = strftime("%a, %d %b %Y",time());;
					$qry->setStringVal("state",$state,"state");
					$qry->setStringVal("feedback",$_POST["feedback"],"Feedback");
					$qry->setAudit($clsUS->userid);
					//echo $qry->getSQL();
					if ($qry->execute($con)) {
	
						
	#########################################################					
						#now  update any attached documents
						#get newly added file ids from form
						$fileIds = array();
						if (isset($_POST['docAddId'])) {
							$fileIds = $_POST['docAddId'];
						}
						#get unattached file ids from form
						$docDelIds = array();
						if (isset($_POST['docDelId'])) {
							$docDelIds = $_POST['docDelId'];
						}
							foreach($fileIds as $fileId) {
									#insert into database 
									$qryTaskFiles = new dbInsert("task_files");
									$qryTaskFiles->setReqNumberVal("fileid",$fileId,"File Id");
									$qryTaskFiles->setReqNumberVal("accounttaskid",$accounttaskid,"Task Id");
									if ($qryTaskFiles->execute($con)) {
										$messages[] = FILES_ATTACHED;
									} else {
										$messages[] = "Attaching file: ".$fileId." ".$qry->getError();
									}
							}
						#now look at delDocIds and unattach these docs from this task
						if (count($docDelIds) > 0) {
							foreach ($docDelIds as $deleteTaskFileId) {
									$qry = new dbDelete("task_files");
									$qry->setParam("accounttaskid",$accounttaskid);
									$qry->setParam("taskfileid",$deleteTaskFileId);
									//echo $qry->getSql();
									if ($qry->execute($con)) {
										$messages[] = FILES_UNATTACHED;
									} else {
										$messages[] = "UnAttaching task file id: ".$deleteTaskFileId." ".$qry->getError();
									}
							}
						}
						
	#############################################################			
						
						
						
						$messages[] = SAVED;
						if (isset($return)) {
							if ($return != "") {
								#return to return page
								headerLocation ("$rootUrl/$return", false);
							}
						}
						$content = "mytasklist.php"; 
						$box[] = "box.key.php";
            if ($tasklist_conf['mytask_search_box'] == true) $box[] = "box.mysearch.php";
						$box[] = "box.taskmonths.php";
						$box[] = "box.mytaskoptions.php";	
						$box[] = "box.myTaskCategories.php";
						if (!isAllowed("editAccount")) $box[] = "box.accountLicenceMyTasksList.php";				
					} else {
						$messages[] = $qry->getError();	
					}
				}
			} else 	if (buttonClicked("cancel") || buttonClicked("back")) {
				if (isset($return)) {
					if ($return != "") {
						#return to return page
						headerLocation ("$rootUrl/$return", false);
					}
				}			
				$content = "mytasklist.php"; 
				$box[] = "box.key.php";
        if ($tasklist_conf['mytask_search_box'] == true) $box[] = "box.mysearch.php";
				$box[] = "box.taskmonths.php";
				$box[] = "box.mytaskoptions.php";
				$box[] = "box.myTaskCategories.php";
				if (!isAllowed("editAccount")) $box[] = "box.accountLicenceMyTasksList.php";					
			}
		} else {
			$content = "mytasklist.php"; 
			$box[] = "box.key.php";
      if ($tasklist_conf['mytask_search_box'] == true) $box[] = "box.mysearch.php";			
			$box[] = "box.taskmonths.php";
			$box[] = "box.mytaskoptions.php";
			$box[] = "box.myTaskCategories.php";
			if (!isAllowed("editAccount")) $box[] = "box.accountLicenceMyTasksList.php";
		}
	} else {
		$content = "mytasklist.php"; 
		$box[] = "box.key.php";
    if ($tasklist_conf['mytask_search_box'] == true) $box[] = "box.mysearch.php";
		$box[] = "box.taskmonths.php";
		$box[] = "box.mytaskoptions.php";
		$box[] = "box.myTaskCategories.php";
		if (!isAllowed("editAccount")) $box[] = "box.accountLicenceMyTasksList.php";
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}



	#get options title and option specific sql query
	if ($category > 0) {
		$categorySQL = "and accounttask.categoryid=".$category;
	}else {
		$categorySQL = "";
	}
	
	if ($month > 0 && $month < 13 && $year > 0) {
		//$month_start = $year."-".$month."-01";
		//$daysinmonth = date('t', strtotime($month_start));
		//$month_end = $year."-".$month."-".$daysinmonth;
		//echo $month_start."<br/>".$month_end;
		$monthSql = "";
		//$monthSql = " and accounttask.actiondate <= '".$month_end."' and accounttask.actiondate >= '".$month_start."' ";
	} else if ($month == 13) {
		$monthSql = " ";
	//} else if ($current_licence){
	//	$monthSql = " ";
	} else {
		$monthSql = " and accounttask.actiondate <= DATE_ADD(NOW(), INTERVAL 7 DAY)";
	}
	
  
  //search
  if(trim($task_search) !='') {
      $task_search_sql = "  and accounttask.customtext like('%".$task_search."%') ";
  } else {
      $task_search_sql = "";
  }
	
	$fileJoinQry = " left join task_files tf on tf.accounttaskid = accounttask.accounttaskid left join files f on (tf.fileid = f.fileid ) "; 
	$accounttaskQry = " accounttask.accounttaskid, accounttask.key_task, accounttask.actiondate, accounttask.completedate, accounttask.state, accounttask.tasktext, accounttask.customtext, accounttask.notes, accounttask.accountid, accounttask.licenceid, accounttask.taskid, accounttask.categoryid, accounttask.assignedto, accounttask.completedby, accounttask.created, accounttask.edited, accounttask.budget, accounttask.feedback, accounttask.edited_by, accounttask.created_by, accounttask.late_task_email_alert ";
	
	switch($option) {
		case "active":
		$optionqry = "SELECT $accounttaskQry, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM accounttask left join taskcategory on accounttask.categoryid = taskcategory.categoryid ";		
		$optionqry .= " left join accountlicence on accounttask.licenceid = accountlicence.licenceid ";		
		$optionqry .= " $fileJoinQry where accountlicence.state = 'ACTIVE' and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') and accounttask.licenceid in ($licence_qry)  and accounttask.assignedto = $userid  $categorySQL $monthSql $task_search_sql group by $accounttaskQry, categoryname order by accounttask.actiondate" ;
		$category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') ";
		$option_title = "Uncompleted Tasks ";
		break;
		
		case "uncompleted":
		$optionqry = "SELECT $accounttaskQry, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM accounttask left join taskcategory on accounttask.categoryid = taskcategory.categoryid ";	
		$optionqry .= " left join accountlicence on accounttask.licenceid = accountlicence.licenceid ";	
		$optionqry .= "$fileJoinQry where accountlicence.state = 'ACTIVE' and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') and accounttask.licenceid in ($licence_qry)  and accounttask.assignedto = $userid  $categorySQL $monthSql $task_search_sql group by $accounttaskQry, categoryname order by accounttask.actiondate" ;
		$category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') ";
		$option_title = "Uncompleted Tasks ";
		break;
		
		case "completed":
		$optionqry = "SELECT $accounttaskQry, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM accounttask left join taskcategory on accounttask.categoryid = taskcategory.categoryid ";
		$optionqry .= " left join accountlicence on accounttask.licenceid = accountlicence.licenceid ";
		$optionqry .= " $fileJoinQry where accountlicence.state = 'ACTIVE' and accounttask.state = 'COMPLETED' and accounttask.licenceid in ($licence_qry) and accounttask.assignedto = $userid  $categorySQL $monthSql $task_search_sql group by $accounttaskQry, categoryname order by accounttask.actiondate" ;
		$option_title = "Completed Tasks ";
		$category_list_task_state = " and (accounttask.state = 'COMPLETED') ";
		break;
		
		case "late":
		$optionqry = "SELECT $accounttaskQry, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM accounttask left join taskcategory on accounttask.categoryid = taskcategory.categoryid ";
		$optionqry .= " left join accountlicence on accounttask.licenceid = accountlicence.licenceid ";
		$optionqry .= " $fileJoinQry where accountlicence.state = 'ACTIVE' and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') and accounttask.licenceid in ($licence_qry) and accounttask.assignedto = $userid $categorySQL and accounttask.actiondate < NOW() $monthSql  $task_search_sql group by $accounttaskQry, categoryname order by accounttask.actiondate" ;
		$category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') and accounttask.actiondate < NOW() ";
		$option_title = "Late Tasks ";
		break;
		/*
		case "now":
		$optionqry = "Select $accounttaskQry, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM accounttask left join taskcategory on accounttask.categoryid = taskcategory.categoryid $fileJoinQry where (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') and accounttask.licenceid in ($licence_qry) and accounttask.assignedto = $userid $categorySQL and accounttask.actiondate < DATE_ADD(NOW(), INTERVAL 7 DAY) group by $accounttaskQry, categoryname order by accounttask.actiondate" ;
		$category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') ";
		$option_title = "Late and next 7 days ";
		break;
		*/
		case "key_task":
		$optionqry = "SELECT $accounttaskQry, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM accounttask left join taskcategory on accounttask.categoryid = taskcategory.categoryid ";		
		$optionqry .= " left join accountlicence on accounttask.licenceid = accountlicence.licenceid ";		
		$optionqry .= " $fileJoinQry where accountlicence.state = 'ACTIVE' and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') and accounttask.licenceid in ($licence_qry) and accounttask.key_task = 1 and accounttask.assignedto = $userid  $categorySQL $monthSql $task_search_sql group by $accounttaskQry, categoryname order by accounttask.actiondate" ;
		$category_list_task_state = " and accounttask.key_task = 1 ";
		$option_title = "Key Tasks ";
		break;
	
		default:
		$optionqry = "select $accounttaskQry, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM accounttask left join taskcategory on accounttask.categoryid = taskcategory.categoryid ";
		$optionqry .= " left join accountlicence on accounttask.licenceid = accountlicence.licenceid ";		$optionqry .= " $fileJoinQry where accountlicence.state = 'ACTIVE' and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') and accounttask.licenceid in ($licence_qry) and accounttask.assignedto = $userid $categorySQL $monthSql $task_search_sql group by $accounttaskQry, categoryname order by accounttask.actiondate" ;
		$category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') ";
		$option_title = "Uncompleted Tasks ";
		break;
	    
	}
	# this record set is used in tasklist.php
	$optionresult = getRS($con, $optionqry);
	//echo $optionqry;
	//die();
	if ($month == 0) {
		$listTitle = $option_title." : Late and next 7 days ".$category_name;
	} else if ($month == 13) {
		$listTitle = $option_title." : All Months".$category_name;

	} else {
		$listTitle = $option_title." : ".$months[$month]." ".$year.$category_name;
	}	

//include layout.
include ("layout.php");

?>