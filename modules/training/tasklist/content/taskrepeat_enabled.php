<?php 
/**
 * Shows a form for editing just the day of week, day of month, or date in month of an accounttask within in a repeating task series
 * 
 * Repeating Task CRUD form with editable fields
 * included in content/taskedit.php
 * 
 */

// merge the repeating task table data into $rowTask
if(isset($RTask)) {
    $repeat_edit_type = array("All tasks", "Future Tasks");
    $rep_type = "<hr style=\"border:0px;clear:both;\" />";
    if ($accounttask->accounttaskid > 0 && $accounttask->accounttask_repeatingid > 0) $rep_type = frmExRadioNonDb("edit_type", 0, $repeat_edit_type, "Apply to:","", "edit_type")."<hr style=\"border:0px;clear:both;\" />";

    $rowRepTask = $RTask->getRow();
    echo frmHiddenField("enabled","edit_repeat");
    $repeat_checked = ""; 
    if((isset($accounttask) && $accounttask->accounttask_repeatingid > 0) || isset($_POST['is_repeatable'])) $repeat_checked = 'checked';
    if (isset($accounttask) && $accounttask->accounttaskid > 0 && $accounttask->accounttask_repeatingid > 0) {
        ?>
            <div id="repeat_task">
            <h2>Set the task to repeat <input type="checkbox" name="is_repeatable" id="is_repeatable" value="1" disabled checked /></h2>
        <?php
    } else {
            ?>
            <div>

            <label for="is_repeatable" style="width: auto;">Make this task repeatable</label><input type="checkbox" name="is_repeatable" id="is_repeatable" value="1" <?php echo $repeat_checked; ?>/>
            </div>
            <div id="repeat_task">
            <h2>Set the task to repeat</h2>
        <?php
    }
?>
        <div id="multitabs">
            <ul style="list-style-image:none;margin:0px;">
                <?php
                //only show all tabs for new repeating task. Otherwise only shoiw the tab containing the actual criteria
                if (!isset($rowRepTask['id'])  || $rowRepTask['weekly'] > 0) {
                    echo "<li><a href=\"#multitab-1\">Weekly</a></li>";
                }
                if (!isset($rowRepTask['id'])  || $rowRepTask['month_date'] > 0) {
                    echo "<li><a href=\"#multitab-2\">Monthly by Date</a></li>";
                }
                if (!isset($rowRepTask['id'])  || $rowRepTask['month_day'] > 0) {
                    echo "<li><a href=\"#multitab-3\">Monthly by Day</a></li>";
                }
                ?>    
            </ul>
            <?php
            if (!isset($rowRepTask['id'])  || $rowRepTask['weekly'] > 0) {
                ?>
                <div id="multitab-1"><!-- weekly -->
                    <div class="rpt_crt">
                        Repeat every <?php echo frmSelectArray($rowRepTask, 'weekly', day_ar(), 'a day of the week', false, 'inline') ?>
                    </div>
                    <?php echo $rep_type;?>
                    
                </div>
            <?php
            }
            if (!isset($rowRepTask['id'])  || $rowRepTask['month_date'] > 0) {
                ?>
                <div id="multitab-2"><!-- monthly -->
                    Repeat every <?php echo frmSelectArray($rowRepTask, 'month_date', ordinal_range(1,31), 'a date', false, 'inline') ?> day of the month.
                <?php echo $rep_type;?>
                </div>
            <?php
            }
            if (!isset($rowRepTask['id'])  || $rowRepTask['month_day'] > 0) {
                ?>
                <div id="multitab-3"><!-- by date -->
                    Repeat every <?php 
                    echo frmSelectArray($rowRepTask, 'position_in_month', array('First', 'Second', 'Third', 'Fourth', 'Last'), '...', false, 'inline') 
                    ?> <?php 
                    echo frmSelectArray($rowRepTask, 'month_day', day_ar(), 'a day', false, 'inline') 
                    ?> of the month.
                <?php echo $rep_type;?>
                </div>
            <?php
            }
            ?>

        </div>
            
    <div>
        <?php
        $duration_checked = '';
        if(isset($_POST['repeat_duration']) && $_POST['repeat_duration'] == 1) {
            $duration_checked = 'checked';
        } else if(isset($rowRepTask['repeat_duration']) &&  $rowRepTask['repeat_duration']==1) {
            $duration_checked = 'checked';
        } else if ((isset($_POST['repeat_start_month']) && $_POST['repeat_start_month'] == '12') || (isset($_POST['repeat_start_day']) && $_POST['repeat_start_day'] == '1') || (isset($_POST['repeat_end_month']) && $_POST['repeat_end_month'] == '12') || (isset($_POST['repeat_end_day']) && $_POST['repeat_end_day'] == '31')) {
            $duration_checked = '';
        } else if (!isset($rowRepTask['repeat_duration'])) {
            $duration_checked = 'checked';
        }
        ?>
        <label for="repeat_duration" style="width: auto;">Repeat for the duration of your licence</label><input type="checkbox" name="repeat_duration" id="repeat_duration" value="1" <?php echo $duration_checked;?>/>
        <hr style="clear:both" />
    </div>
    <div id="repeat_date">
        <p>Only repeat between specific dates of the year:</p>
        <?php
        // default dates are Jan 1st -> Dec 31st 
        if(!isset($rowRepTask['repeat_start_month']) || $rowRepTask['repeat_start_month'] == '') $rowRepTask['repeat_start_month'] = '1';
        if(!isset($rowRepTask['repeat_start_day']) || $rowRepTask['repeat_start_day'] == '') $rowRepTask['repeat_start_day'] = '1';
        if(!isset($rowRepTask['repeat_end_month']) || $rowRepTask['repeat_end_month'] == '') $rowRepTask['repeat_end_month'] = '12';
        if(!isset($rowRepTask['repeat_end_day']) || $rowRepTask['repeat_end_day'] == '') $rowRepTask['repeat_end_day'] = '31';
        ?>
        Between <?php 
            echo frmSelectArray($rowRepTask, 'repeat_start_month', month_ar(), 'a month', false, 'inline date_range');
            echo frmSelectArray($rowRepTask, 'repeat_start_day', ordinal_range(1,31), 'a date', false, 'inline date_range');
        ?> and <?php 
            echo frmSelectArray($rowRepTask, 'repeat_end_month', month_ar(), 'a month', false, 'inline date_range');
            echo frmSelectArray($rowRepTask, 'repeat_end_day', ordinal_range(1,31), 'a date', false, 'inline date_range');
        ?>                 
    </div>        
            
</div>
<hr style="border:0px;clear:both;" />
<?php
}?>