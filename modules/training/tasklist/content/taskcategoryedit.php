<?php 
/**
 * Shows a form for editing a task category
 */
if (isset($categoryid)) { 
	$rsTaskcategory = getRS($con,"select * from taskcategory where categoryid = $categoryid");
	$rowTaskcategory = getRow($rsTaskcategory); 
	?>
<?php
	echo startFormTable($_SERVER["PHP_SELF"], "", "", CATEGORY_FORM_TITLE, CATEGORY_FORM_INFO);
	//echo formNotes(CATEGORY_FORM_TITLE, CATEGORY_FORM_INFO);
	echo frmHiddenField($categoryid,"categoryid");
	echo frmHiddenField($tasklistid,"tasklistid");
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	
	//echo frmTextField($rowTaskcategory,"categoryname",100,"Task category name",true);
	if (isset($rowTaskcategory['mid']) && $rowTaskcategory['mid'] > 0) echo frmTextAreaNonEdit($rowTaskcategory,"categoryname","Task category name",true);
	echo frmTextField($rowTaskcategory,"customtext",100,"Custom task category name",true);
  
  if ($tasklist_conf['category_group_assign'] == true) {
      echo frmSelectQuery($rowTaskcategory,"group_id","select group_id as a, title as b from task_category_group ",$con,"Category Group", false);  
  }
  
	echo frmSelectArray($rowTaskcategory, "state" , $state, "State" , true);
	?>
	<?php	
	if ($categoryid != 0 && isAllowed("editTask")) {
		echo frmShowAudit($rowTaskcategory,$con);	
	}?>
	
	
<?php
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($categoryid != 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this category? All tasks within this category will also be deleted. ", "Click here to delete this category");
	
	echo endFormTable();
} ?>


