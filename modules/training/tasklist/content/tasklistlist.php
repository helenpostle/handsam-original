<?php
/***
** Shows the list of 'task lists' in the system
** 
** click on link to view tasks in the lists by month or by category
***/

$order = "tasklistname";

$qry = "SELECT * FROM tasklist WHERE state != 'DELETED' ORDER BY ".$order.";";

$result = getRS($con, $qry);
?>
<table class="acc_list ">
	<caption class="hidden"><?php echo $title;?></caption>
	<thead>
		<tr>
		    <th class="start ">Task list name</th>
			<th class="">State</th>
			<th class=""></th>
			<th class="end centre"></th>
		</tr>
	</thead>
	<tbody class="noStrip">
<?php
while ($row = getRow($result)) {
    $itemname = ($row["customtext"] != '') ? trim($row["customtext"]) : $row['tasklistname'] ;
?>
            <tr>
                <td class="start">
                    <a class="rowRollOver" title="edit task list" href="tasklistadmin.php?tasklistid=<?php echo $row["tasklistid"] ?>"><?php echo displayText($itemname); ?></a>
                </td>
                <td>
                    <a class="rowRollOver" title="edit task list" href="tasklistadmin.php?tasklistid=<?php echo $row["tasklistid"] ?>"><?php echo displayText($row["state"]) ?></a>
                </td>
                <td class="rowRollOver">
                    <a class="tableLink" title="view tasks in <?php echo displayText($itemname); ?>" href="taskadmin.php?tasklistid=<?php echo $row["tasklistid"] ?>">View tasks</a>
                </td>
                <td class="end">
                    <a class="tableLink" title="view categories in <?php echo displayText($itemname); ?>" href="taskcategory.php?tasklistid=<?php echo $row["tasklistid"] ?>">View categories</a>
                </td>
            </tr>
<?php } // endwhile 
?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td></td>
			<td></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>

</table>
<div class="add_acc" ><a href="tasklistadmin.php?tasklistid=0">Add a new task list</a></div>

