<?php 
/**
 * Shows a form for editing a task
 * 
 * Repeating Task CRUD form with non-editable fields
 * included in content/admintaskedit.php
 * 
 */
$repeat_edit_type = array("Edit all tasks", "Edit this task", "Edit this Task &amp; Future Tasks");
echo frmExRadioNonDb("edit_type", 0, $repeat_edit_type, "Edit Repeating Task","Which tasks do you want to edit in this series?");

$rowRepTask = $RTask->getRow();
echo frmHiddenField("disabled","edit_repeat");
$repeat_checked = "";
$repeating_inputs = " disabled = 'disabled' ";
if((isset($accounttask) && $accounttask->accounttask_repeatingid > 0) || isset($_POST['is_repeatable'])) $repeat_checked = 'checked';
?>
<div id="repeat_task">
    <h2>Set the task to repeat <input type="checkbox" name="is_repeatable" id="is_repeatable" value="1" <?php echo $repeat_checked.$repeating_inputs; ?>/>&nbsp;&nbsp;<input class="inputSubmit" type="submit" onclick="return confirm('Are you sure you want to edit the repeating options? You will lose any changes that you may have made to the task details.');" name="bt_edit_repeat" value="Edit repeating options"></h2>
    
    <div id="multitabs">
        <ul style="list-style-image:none;margin:0px;">
            <?php
            //only show all tabs for new repeating task. Otherwise only shoiw the tab containing the actual criteria
            if (!isset($rowRepTask['id'])  || $rowRepTask['weekly'] > 0) {
                echo "<li><a href=\"#multitab-1\">Weekly</a></li>";
            }
            if (!isset($rowRepTask['id'])  || $rowRepTask['month_date'] > 0) {
                echo "<li><a href=\"#multitab-2\">Monthly by Date</a></li>";
            }
            if (!isset($rowRepTask['id'])  || $rowRepTask['month_day'] > 0) {
                echo "<li><a href=\"#multitab-3\">Monthly by Day</a></li>";
            }
            ?>   
        </ul>

        <?php
        if (!isset($rowRepTask['id'])  || $rowRepTask['weekly'] > 0) {
            ?>
            <div id="multitab-1"><!-- weekly -->
                <div>
                    Repeat every <?php echo frmSelectArray($rowRepTask, 'weekly', day_ar(), 'a day of the week', false, 'inline disabled', true) ?>
                </div>
            </div>
        <?php
        }
        if (!isset($rowRepTask['id'])  || $rowRepTask['month_date'] > 0) {
            ?>
            <div id="multitab-2"><!-- monthly -->
                Repeat every <?php echo frmSelectArray($rowRepTask, 'month_date', ordinal_range(1,31), 'a date', false, 'inline disabled', true) ?> day of the month.
            </div>
        <?php
        }
        if (!isset($rowRepTask['id'])  || $rowRepTask['month_day'] > 0) {
            ?>
            <div id="multitab-3"><!-- by date -->
                Repeat every <?php 
                echo frmSelectArray($rowRepTask, 'position_in_month', array('First', 'Second', 'Third', 'Fourth', 'Last'), '...', false, 'inline disabled', true) 
                ?> <?php 
                echo frmSelectArray($rowRepTask, 'month_day', day_ar(), 'a day', false, 'inline disabled', true) 
                ?> of the month.
            </div>
        <?php
        }
        ?>

    </div>
    <div>
        <?php
        $duration_checked = '';
        if(isset($rowRepTask['repeat_duration']) &&  $rowRepTask['repeat_duration']==1) {
            $duration_checked = 'checked';
        } else if (!isset($rowRepTask['repeat_duration'])) {
            $duration_checked = 'checked';
        }
        ?>
        <label for="repeat_duration" style="width: auto;">Repeat for the duration of your licence</label><input type="checkbox" name="repeat_duration" id="repeat_duration" value="1" <?php echo $duration_checked.$repeating_inputs;?>/>
        <hr style="clear:both" />
    </div>
    <div id="repeat_date">
        <p>Only repeat between specific dates of the year:</p>
        <?php
        // default dates are Jan 1st -> Dec 31st 
        if(!isset($rowRepTask['repeat_start_month']) || $rowRepTask['repeat_start_month'] == '') $rowRepTask['repeat_start_month'] = '1';
        if(!isset($rowRepTask['repeat_start_day']) || $rowRepTask['repeat_start_day'] == '') $rowRepTask['repeat_start_day'] = '1';
        if(!isset($rowRepTask['repeat_end_month']) || $rowRepTask['repeat_end_month'] == '') $rowRepTask['repeat_end_month'] = '12';
        if(!isset($rowRepTask['repeat_end_day']) || $rowRepTask['repeat_end_day'] == '') $rowRepTask['repeat_end_day'] = '31';
        ?>
        Between <?php 
            echo frmSelectArray($rowRepTask, 'repeat_start_month', month_ar(), 'a month', false, 'inline disabled', true);
            echo frmSelectArray($rowRepTask, 'repeat_start_day', ordinal_range(1,31), 'a date', false, 'inline disabled', true);
        ?> and <?php 
            echo frmSelectArray($rowRepTask, 'repeat_end_month', month_ar(), 'a month', false, 'inline disabled', true);
            echo frmSelectArray($rowRepTask, 'repeat_end_day', ordinal_range(1,31), 'a date', false, 'inline disabled', true);
        ?>                 
    </div>
</div>
<hr style="border:0px;clear:both;" />
