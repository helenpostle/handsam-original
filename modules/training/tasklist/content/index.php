<?php 
/**
 * standard index page
 */
 
 ?>
<h2> The Health and Safety Management System</h2>


<ul>
	<li>HandSaM can help you manage your Health, Safety, Welfare and Security responsibilities</li>
	<li>HandSaM is comprehensive, easy to use and effective</li>
</ul>


<p>The HandSaM system is a central register of all the Health, Safety, welfare and Security legal duties of Local Education Authorities, Schools and Colleges.</p>

</p>It is likely that the duty to complete the tasks will be shared by many staff, but ultimately one person- the Head or the Head's <strong>Health and Safety Representative</strong> - will have the resposibility to ensure that all <strong>legal resposibilities</strong> have been fullfilled.</p>

<div id="healthandsafety"><img src="images/healthandsafety.png" alt="HANDSAM WILL BRING PEACE OF MIND TO ANYONE WITH THIS RESPONSIBILITY"/></div>


