<?php 
/**
 * show licence history for the selected account for TASLKS APP ONLY
 */
 
 
//display a new box for each tasklist

//first get tasklists for this account

$qry = "select distinct tasklist.tasklistid, tasklist.tasklistname from tasklist left join accountlicence on tasklist.tasklistid = accountlicence.tasklistid where accountid = $accountid and accountlicence.state in ('ACTIVE', 'INACTIVE', 'NEW')";
$tasklistRs = getRS($con, $qry);

While ($tasklistRow = getRow($tasklistRs)) {

	boxTop( $tasklistRow['tasklistname']." Licences");
	$qry = "SELECT accountlicence.* FROM accountlicence where accountid = ".$accountid." and accountlicence.state != 'DELETED' and tasklistid = ".$tasklistRow['tasklistid']." order by  end_date asc";
	$result = getRS($con, $qry);
	$thisTasklist = 0;
	$tasklistid = $tasklistRow['tasklistid'];
	
	$renewLicence = false;
	echo "<p class=\"info\">Select the <strong>CURRENT</strong> licence to view the tasks that go with that licence. ";
	if (isAllowed("editAccount")) {
		echo "Select a <strong>NEW</strong> licence to edit the licence dates and to add holiday periods to that licence.";
	}
	echo "</p>";
	?>
	<table class="boxList">
		<caption class="hidden"><?php echo $title;?> licence History</caption>
		<thead>
			<tr>
				<th class="licenceid nolink">ID</th>
				<th class="state nolink">State</th>
				<th class="licence nolink">Start</th>
				<th class="licence nolink">End</th>
			</tr>
		</thead>
		<tbody>
			<?php
			while ($row = getRow($result)) {
				#only if the licence state==new can we still edit the licence
				#'edit licence' page also has 'edit  holiday periods' link
				$edit_licence_link = "licence.php?licenceid={$row["licenceid"]}&amp;accountid={$accountid}&amp;tasklistid={$tasklistid}&amp;appid={$row["appid"]}";
				if ($row['state'] == "NEW" && isAllowed("editAccount")) {
					?>
					<tr>
						<td class="licenceid"><a class="arrow" href="<?php echo $edit_licence_link;?>"><?php echo $row["licenceid"]; ?></a></td>
						<td class="licence"><a href="<?php echo $edit_licence_link;?>"><?php echo $row['state']; ?></a></td>
						<td class="licence"><a href="<?php echo $edit_licence_link;?>"><?php echo displayDate($row['start_date']); ?></a></td>
						<td class="licence"><a href="<?php echo $edit_licence_link;?>"><?php echo displayDate($row['end_date']); ?></a></td>
					</tr>
				<?php 
				} elseif($row['state'] == "ACTIVE"){
					#show link to tasks for this licence
					//this is the old link to the actual tasklist - now used on as a link from the licence edit page instead
					$licenceLink = "$rootUrl/modules/tasklist/tasks.php?licenceid=".$row["licenceid"]."&amp;accountid=$accountid";
					if (strtotime($row['start_date']) < time() && strtotime($row['end_date']) > time()) {
						$this_state = "CURRENT";
					} else if(strtotime($row['start_date']) > time())  {
						$this_state = "ACTIVE";
					} else {
						$this_state = "ARCHIVED";
					}
					?>
					<tr>
						<td class="licenceid"><a class="arrow" href="<?php echo $edit_licence_link; ?>"><?php echo $row["licenceid"]; ?></a></td>
						<td class="licence"><a href="<?php echo $edit_licence_link; ?>"><?php echo $this_state; ?></a></td>
						<td class="licence"><a href="<?php echo $edit_licence_link; ?>"><?php echo displayDate($row['start_date']); ?></a></td>
						<td class="licence"><a href="<?php echo $edit_licence_link; ?>"><?php echo displayDate($row['end_date']); ?></a></td>
					</tr>
				<?php 
				} /*elseif($row['state'] == "NEW"){
					#show link to tasks for this licence
					$licenceLink = "tasks.php?licenceid=".$row["licenceid"];
					?>
					<tr>
						<td class="licenceid"><a class="arrow" href="<?php echo $licenceLink; ?>"><?php echo $row["licenceid"]; ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo $row['state']; ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo displayDate($row['start_date']); ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo displayDate($row['end_date']); ?></a></td>
					</tr>
				<?php 
				}*/
				#these variables store the latest licence details for use below
				$end_date = $row['end_date'];
				$currentState = $row['state'];
				$lastlicence = $row["licenceid"];
				$renewLicence = true;
			}
			
			?> 
		</tbody>
		<tfoot>
		<?php
		if (isAllowed("editAccount")) {
			if ($renewLicence) {
				if (strtotime($end_date) > strtotime(strftime("%d/%m/%Y",time()))) {
					echo "<tr><td colspan=\"4\"><a href=\"licence.php?accountid=".$accountid."&amp;licenceid=0&amp;tasklistid=".$tasklistRow['tasklistid']."&amp;appid=1\" title=\"Renew licence\">Renew licence</a></td></tr>";
				} else {
					echo "<tr><td colspan=\"4\"><a class=\"page\" href=\"licence.php?accountid=".$accountid."&amp;licenceid=0&amp;tasklistid=".$tasklistRow['tasklistid']."&amp;appid=1\" title=\"Create licence\">Create licence</a></td></tr>";
				}
			} else  {
				#show create licence button
				//echo "<tr><td colspan=\"4\"><a class=\"page\" href=\"licence.php?accountid=".$accountid."&amp;licenceid=0\" title=\"Create licence\">Create licence</a></td></tr>";
			}
		}
	
			?>
		</tfoot>
	</table>
	<?php
		boxBottom();
}
?>
	