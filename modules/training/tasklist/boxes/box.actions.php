<?php 
/**
 * lists options for viewing accoun tasks list
 * 
 */
	boxTop("Actions");
	$optionLink = "tasks.php?task_search=$task_search_url&amp;licenceid=".$licenceid."&month=".$month."&year=".$year."&category=".$category."&user=".$user."&multi=".$multi."&option=".$option."&accounttaskid=0";
?>
<div class="vertical_nav">

<ul>
	<?php 
	echo displayListLink("Add a custom task", "a", "", $optionLink);
	if (isAllowed("editOurTasks")) {
		//echo displayListLink("Request a new category", "a", "", "email.php?subject=Request%20a%20Category&return=tasks.php");
	}
	?>
</ul>
</div>
<?php
	boxBottom();
?>
	