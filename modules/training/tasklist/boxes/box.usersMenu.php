<?php 
/**
 * 
 * click on users to filter tasks by that user
 */
 if ($category == 0) {
	 $category_str = "";
 } else {
	 $category_str = "AND accounttask.categoryid = ".$category;
 }
 
 
	if ($month > 0 && $month < 13) {
		boxTop($months[$month]."'s Users");
		$qry = "SELECT usr.username, usr.userid, COUNT(usr.username) AS cnt FROM usr RIGHT JOIN accounttask ON usr.userid = accounttask.assignedto WHERE accounttask.licenceid = '$licenceid' AND MONTH(accounttask.actiondate) = '$month' AND YEAR(accounttask.actiondate) = '$year' ".$category_str.$users_menu_option." {$listTasks->monthSQL} {$listTasks->searchSQL} {$listTasks->category_list_sql} GROUP by usr.username ORDER BY usr.username, usr.userid ASC";
	} else {
		boxTop("Users");
		$qry = "SELECT usr.username, usr.userid, COUNT(usr.username) AS cnt FROM usr INNER JOIN accounttask ON usr.userid = accounttask.assignedto WHERE accounttask.licenceid = '$licenceid' ".$category_str.$users_menu_option." {$listTasks->monthSQL} {$listTasks->searchSQL} {$listTasks->category_list_sql} GROUP by usr.username  ORDER BY usr.username, usr.userid ASC";
	}
  //echo  $qry;
  $result = getRS($con, $qry);
	if(isAllowed("editAccount")) {
		$userLink = "tasks.php?task_search=$task_search_url&amp;licenceid=".$licenceid."&month=".$month."&year=".$year."&option=".$option."&multi=".$multi."&category=".$category."&group_id=".$group_id."&user=";
	} else {
		$userLink = "tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=".$month."&year=".$year."&option=".$option."&multi=".$multi."&category=".$category."&group_id=".$group_id."&user=";
	}

?>
<div class="vertical_nav">
	<ul>
		<?php 
		if ($user == 0) {
			echo "<li class=\"active\">Show all users</li>";
		} else {
			echo "<li><a href=\"".$userLink."0\" title=\"\">Show all users</a></li>";
		}
			
		while ($row = getRow($result)) {
			if ($row['cnt'] == 0) {
			} else {
				echo displayListLink(textSummary($row["username"],20)."&nbsp;(".$row['cnt'].")", $user, $row["userid"], $userLink);
			}
		}
		?>
	</ul>

</div>

<?php
	boxBottom();
?>