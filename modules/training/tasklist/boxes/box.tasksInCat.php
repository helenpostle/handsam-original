<?php 
/**
 * Standard index page box
 */
if ($view == "cat") {
	//function for cat view
	boxTop("All tasks in ".$categoryname);
	//query for category view
	$qry = "SELECT taskday, tasktext, customtext, taskid FROM  task  where categoryid = '$categoryid' and tasklistid = '$tasklistid'";
} else {
	//function for month view
	boxTop("All ".$months[$month]." tasks in ".$categoryname);
	//query for month view
	$qry = "SELECT taskday, tasktext, customtext, taskid FROM  task  where categoryid = '$categoryid' and tasklistid = '$tasklistid' and taskmonth = '$month'";
}
	$result = getRS($con, $qry);
	

?>

<table class="list">
	<caption class="hidden">Tasks in category</caption>
	<thead>
		<tr>
			<th class="description nolink">Task Description</th>
			<th class="day nolink">Day</th>
		</tr>
	</thead>
	<tbody>

	<?php
	$link24 = "taskadmin.php?taskid=0&tasklistid=".$tasklistid."&taskmonth=".$month."&categoryid=".$categoryid."&view=".$view;
	while ($row = getRow($result)) {
		$link23 = "?taskid=".$row['taskid']."&tasklistid=".$tasklistid."&taskmonth=".$month."&view=".$view."&categoryid=".$categoryid;
	?>
		<tr>
			<td class="description"><a title="View <?php echo showAbstract($row["customtext"], 70);?>" href="<?php echo $link23;?>"><?php echo showAbstract($row["customtext"], 70); ?></a></td>
			<td class="day"><?php echo $row["taskday"];?></td>
		</tr>
	<?php } ?>
	</tbody>
	<?php
	$link42 = "taskadmin.php?taskid=0&tasklistid=".$tasklistid."&taskmonth=".$month."&categoryid=".$categoryid."&view=".$view;
	if ($taskid !=0) {?>
		<tfoot>
			<tr>
				<td colspan=2><a href="<?php echo $link42;?>">Add a new task</a></td>
			</tr>
		</tfoot>
		<?php
	}?>
</table>
<?php
	boxBottom();
?>