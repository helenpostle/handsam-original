<?php 
/**
 * lists options for viewing alert tasks list
 * 
 */
	boxTop("Reporting");
	$optionLink = "mytasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=".$month."&year=".$year."&category=".$category."&option=";
?>
<div class="vertical_nav">

<ul>
	<?php 
	echo displayListLink("Show all", $option, "active", $optionLink);
	
	//echo displayListLink("Show now", $option, "now", $optionLink);
	echo displayListLink("Show uncompleted", $option, "uncompleted", $optionLink);
	echo displayListLink("Show completed", "$option", "completed", "$optionLink");
	echo "<li class=\"indent\">On time:<img class=\"inBox\" src=\"images/tick.gif\" alt=\"\"/> Late: <img class=\"inBox\" src=\"images/tick_red.gif\" alt=\"\"/>";
	echo displayListLink("Show late", $option, "late", $optionLink);
    echo displayListLink("Show key tasks", $option, "key_task", $optionLink);

	?>
</ul>
</div>
<?php
	boxBottom();
?>
	