<?php 
/**
 * Standard index page box
 */
boxTop("Tasks in category: ".$categoryname);
$qry = "SELECT assignedto, actiondate, tasktext, customtext FROM  accounttask  where categoryid = '$categoryid' and accountid='$accountid'";
$result = getRS($con, $qry);
//echo $qry;
	

?>
<p class="info">Click on any of the tasks below to select that task &quot;Action date&quot; and &quot;Assigned to user&quot; for the task being edited.</p>
<table class="boxList">
	<thead>
		<tr>
			<th class="description nolink">Task Description</th>
			<th class="day nolink">Date</th>
		</tr>
	</thead>
	<tbody>

	<?php
	while ($row = getRow($result)) {
		$linkurl = "?month=".$month."&year=".$year."&option=".$option."&category=".$category."&datefield=".strtotime($row["actiondate"])."&assignedto=".$row["assignedto"]."&accounttaskid=".$accounttaskid;
		?>
		<tr>
			<td class="description"><a class="arrow" title="View <?php echo showAbstract($row["customtext"], 70);?>" href="<?php echo $linkurl;?>"><?php echo showAbstract($row["customtext"], 70); ?></a></td>
			<td class="date"><a title="View <?php echo showAbstract($row["customtext"], 70);?>" href="<?php echo $linkurl;?>"><?php echo displayDate($row["actiondate"]);?></a></td>
		</tr>
		<?php 
	} ?>
	</tbody>
</table>
<?php
	boxBottom();
?>