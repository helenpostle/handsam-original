<?php 
/**
 * lists options for viewing accoun tasks list
 * 
 */
boxTop("Feedback Key");
?>
<ul class="key">
	<li>
		<img src="images/tick_orange.gif" alt="This task has additional notes or documents"/>
		<span>This task has additional notes or documents added by the administrator</span>
	</li>
	<li>
		<img src="images/tick_blue.gif" alt="This task has feedback"/>
		<span>This task has feedback left by the user</span>
	</li>
</ul>
<?php
boxBottom();
?>
	