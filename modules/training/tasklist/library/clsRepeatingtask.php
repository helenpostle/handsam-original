<?php

/**
 * Repeatingtask CRUD class 
 * 
 * methods: save(), getRow(), delete()
 * 
 * Create a new object with new Repeatingtask($clsUS), also can take the task_repeating table id
 *
 * @author Mei Gwilym <mei.gwilym@gmail.com>
 */
class Repeatingtask {
    
   
    /**
     * row id
     * @var int
     */
    protected $id;
    
    /**
     * DB recordset object
     * @var object
     */
    protected $row = array();
    
    /**
     * Current User Class
     * @var obj
     */
    protected $clsUS;
    
    /**
     *
     * @var int number of tasks with this repeating id 
     */
    public $num_repeats;
    
    
    /**
     *
     * @array containing all day numbers of this repeatingtaskid 
     */
    public $all_days = array();
    
    
    /*
     * 
     * array of weeks that this repeating task covers (1-52)
     */
    public $weeks_arr;
      
    /*
     * 
     * array of months that this repeating task covers (1-12)
     */
    public $months_arr;
    
    /*
     * loop array is set as weeks_arr or months_arr
     */
    public $loop_arr = array();
    
    /*
     * var type of repeat - weekly, monthly_by_date, monthly_by_day
     */
    private $repeat_type;
    
    /*
     * var the week day for a weekly repeat
     */
    private $weekly;
    
    /*
     * var the month date for a monthly by date repeat
     */
    private $monthly_by_date;
    
    /*
     * var the 'first' or 'last' part of a monthly by day repeat
     */
    private $position_in_month;
    
    /*
     * var - hte day of week for a monthly by day repeat
     */
    private $month_day;
    
    /*
     * var mid
     */
    private $mid;
    
    /*
     * array of task theoretical task dates within this repeating task series
     */
    
    public $taskDates;
    /**
     * Takes the global User object and repeatingtask id
     * 
     * @global object $con db conn
     * @param object $clsUS Current App User
     * @param int $id
     * @param int $taskid
     */
    public function __construct($con, $clsUS, $id = null)
    {
        
        
        $this->clsUS = $clsUS;
        
        if(!is_null($id) && $id > 0)
        {
            $this->id = $id;
            
            $this->get($con);
        }
    }
    
    /**
     * Saves the recordset after a form submission
     * 
     * @global array $messages
     * @return boolean
     */
    public function save($con)
    {
        global $messages;
         
        $db = ($this->id > 0) ? $this->update() : $this->insert();
                
        if(isset($_POST['weekly'])) {
            $db->setReqNumberVal('weekly', $_POST['weekly'], 'Weekly Repeating Task - Week Day');
        } else {
            $db->setNullVal('weekly');
        }
        if(isset($_POST['month_date'])) {
            $db->setReqNumberVal('month_date', $_POST['month_date'], 'Monthly by Date Repeating Task - Month Date');
        } else {
            $db->setNullVal('month_date');
        }
        if(isset($_POST['position_in_month'])) {
            $db->setReqNumberVal('position_in_month', $_POST['position_in_month'], 'Monthly by Day Repeating Task - First or Last');
        } else {
            $db->setNullVal('position_in_month');
        }
        if(isset($_POST['month_day'])) {
            $db->setReqNumberVal('month_day', $_POST['month_day'], 'Monthly by Day Repeating Task - Month Day');
        } else {
            $db->setNullVal('month_day');
        }
        
        $year = date("Y", time());
        $month = 12;
        if (isset($_POST['repeat_end_month'])) $month = $_POST['repeat_end_month'];
        $last_day_of_month = date('t', strtotime("$year-$month-01"));
        
        //must either be repeat_duration or save repeat_start_month etc
        if(isset($_POST['repeat_duration'])) {
            $db->setNumberVal('repeat_duration', $_POST['repeat_duration'], 'Repeat Duration');
            $db->setNullVal('repeat_start_month');
            $db->setNullVal('repeat_start_day');
            $db->setNullVal('repeat_end_month');
            $db->setNullVal('repeat_end_day');
            
        } else {
            $db->setNumberVal('repeat_duration', 0, 'Repeat Duration');
            if(isset($_POST['repeat_start_month'])) $db->setNumberVal('repeat_start_month', $_POST['repeat_start_month'], 'Start Month');
            if(isset($_POST['repeat_end_month'])) $db->setNumberVal('repeat_end_month', $_POST['repeat_end_month'], 'ENd Month');
            if (isset($_POST['weekly']) && $_POST['weekly'] > 0) {
                if ($_POST['repeat_end_day'] > $last_day_of_month) $_POST['repeat_end_day'] = $last_day_of_month;
                if(isset($_POST['repeat_start_day'])) $db->setNumberVal('repeat_start_day', $_POST['repeat_start_day'], 'Start Day');
                if(isset($_POST['repeat_end_day'])) $db->setNumberVal('repeat_end_day', $_POST['repeat_end_day'], 'End Day');
            } else {
                $db->setNumberVal('repeat_start_day', 1, 'Start Day');
                $db->setNumberVal('repeat_end_day', $last_day_of_month, 'End Day');
            }
        }
                
        if ($db->execute($con))
        {
            //set weeks/months arr
            $this->setDateCriteria($_POST);
            $this->get($con);
            return $this->id ? : $db->getNewId();
        }
              
        $messages[] = $db->getError();
        return false;
    }
    
    /*
     * sets the date vars
     */
    private function setDateCriteria($arr) {
        if(isset($arr['weekly']) && $arr['weekly'] > 0) {
            $this->repeat_type = "weekly";
            $this->weekly = $arr['weekly'];
            $this->setWeeks($arr);
        }
        if(isset($arr['month_date'])&& $arr['month_date'] > 0) {
            $this->repeat_type = "monthly_by_date";
            $this->setMonths($arr);
            $this->monthly_by_date = $arr['month_date'];
        }
        if(isset($arr['month_day']) && $arr['month_day'] > 0) {
            $this->repeat_type = "monthly_by_day";
            $this->setMonths($arr);
            $this->month_day = $arr['month_day'];
        }
        if (isset($arr['position_in_month'])) $this->position_in_month = $arr['position_in_month'];
       
    }
    
    /**
     * Returns the db row as an associated array
     * @return array
     */
    public function getRow()
    {
        return $this->row;
    }

    /**
     * Set the current repeating task to delete
     * 
     * @return bool true on success
     */
    public function delete($con)
    {
        global $messages;
        
        if(!$this->id) 
            return false;
        
        $db = $this->update();
        $db->setReqStringVal("state", "DELETED", "State");
        $db->setAudit($this->clsUS->userid);
        
        if($db->execute($con)) 
            return true;
        
        $messages[] = $db->getError();
        return false;
    }
    
    /**
     * Returns a DB field
     * 
     * @param string $field field name
     * @return string
     */
    public function __get($field)
    {
        // returns the array key or an empty string
        return $this->row[$field] ? :'';
    }
    
    /**
     * Sets the row id and retrieves the record
     * 
     * @param int $id
     */
    public function setId($id, $con)
    {
        $this->id = $id;
        
        $this->get($con);
    }
        
    /**
     * gets the row id
     * 
     * @param int $id
     */
    public function getId()
    {
        return $this->id;
    }
        
    /**
     * Return a DB Insert object
     * Sets `state` to ACTIVE
     * sets the audit
     * 
     * @return \dbInsert 
     */
    protected function insert()
    {
        $db = new dbInsert('task_repeating');
        $db->setReqStringVal("state", "ACTIVE", "State");
        $db->setAudit($this->clsUS->userid);
        
        return $db;
    }
    
    /**
     * Return a DB Update object,
     * Sets the row id
     * sets the audit trail
     * 
     * @return \dbUpdate
     */
    protected function update()
    {
        $db = new dbUpdate('task_repeating');
        $db->setParam('id', $this->id);
        $db->setAudit($this->clsUS->userid);
        
        return $db;
    }
    
    /**
     * Fill the object with the db row
     */
    protected function get($con)
    {
        $sql = "SELECT * FROM task_repeating WHERE id = '".mysql_real_escape_string($this->id)."';"; 
        $rs = getRS($con, $sql);
        $this->row = getRow($rs);
        $this->setDateCriteria($this->row);
        //change this when we start syncing repeatin tasks
        $this->mid = 0;  
        
        if ($this->id > 0) {
            $days = array(1=>"monday", 2=>"tuesday", 3=>"wednesday", 4=>"thursday", 5=>"friday", 6=>"saturday", 7=>"sunday");
            $sql = "select taskid, taskmonth, taskday, week_num, day_of_week, position_in_month from task where repeatingtaskid = {$this->id} and state != 'DELETED'";
            $rs = getRS($con, $sql);
            $this->num_repeats = getResultRows($rs);
            $year  = date('Y');
            while($row = getRow($rs)) {
                //calculate day number from taskmonth and taskday and current year.
                if($this->row['weekly'] > 0) {
                    $day = date("z", strtotime($year."W".str_pad($row['week_num'], 2, '0', STR_PAD_LEFT).$row['day_of_week']));
                } else if($this->row['position_in_month'] !== null && $this->row['position_in_month'] >= 0) {
                    $day = date("z",strtotime($this->row['position_in_month'].$days[$row['day_of_week']]." ".$year."-".$row['taskmonth']));
                } else {
                    $day = date("z",strtotime($year."-".str_pad($row['taskmonth'], 2, '0', STR_PAD_LEFT)."-".$row['taskday']));
                }
                $this->all_days[$row['taskid']] = $day;
            }  
        }
        return $this;
    }
    
    
    function update_num_repeats($con) {
        $sql = "select taskid from task where repeatingtaskid = {$this->id} and state != 'DELETED'";
        $rs = getRS($con, $sql);
        $this->num_repeats = getResultRows($rs);
    }
    
    /*
     * sets the array of week numbers
     */
    protected function setWeeks($arr) {
        if (isset($arr['repeat_duration']) && $arr['repeat_duration'] == 1) {
            $this->weeks_arr = range(1, 52 ,1);
        } else {
            $startdate = date('Y-m-d', strtotime($arr['repeat_start_day']."-".$arr['repeat_start_month']."-".date("Y")));
            $enddate = date('Y-m-d', strtotime($arr['repeat_end_day']."-".$arr['repeat_end_month']."-".date("Y")));
            $this->weeks_arr = getWeekNumbers($startdate, $enddate);

            $week_num = end($this->weeks_arr);
            if($week_num < 10) $week_num = "0".$week_num;
            $d=date("Y", strtotime($enddate))."W".$week_num.$arr['weekly'];
            $last_actiondate = date("Y-m-d",strtotime($d));
            
            $week_num  = reset($this->weeks_arr);
            if($week_num < 10) $week_num = "0".$week_num;
            $d=date("Y", strtotime($startdate))."W".$week_num.$arr['weekly'];
            $first_actiondate =  date("Y-m-d",strtotime($d));
            if ($last_actiondate > $enddate) array_pop($this->weeks_arr);
            if ($first_actiondate < $startdate) array_shift($this->weeks_arr);
        }    
        
    
    }

    /*
     * sets the array of month numbers
     */
    protected function setMonths($arr) {
      
        if (isset($arr['repeat_duration']) && $arr['repeat_duration'] > 0) {
            $this->months_arr = range(1, 12 ,1);
        } else {
            $last_day_of_month = date('t', strtotime(date("Y")."-".$arr['repeat_end_month']."-01"));
            $startdate = "01-".$arr['repeat_start_month']."-".date("Y");
            $enddate = "$last_day_of_month-".$arr['repeat_end_month']."-".date("Y");
            $this->months_arr = getMonthNumbers($startdate, $enddate);
        }
    }
    
    /*
     * 
     */
    
    public function setLoopArr() {
        if (count($this->weeks_arr) > 0) {
            $this->loop_arr = $this->weeks_arr;
        } else if(count($this->months_arr) > 0) {
            $this->loop_arr = $this->months_arr;
        }
    }
    
    /*
     * 
     */
    public function setPostVars($post_vals, $val) {
        switch($this->repeat_type) {
            case 'weekly':
                $post_vals['taskmonth'] = 0; 
                $post_vals['taskday'] = 0;
                $post_vals['position_in_month'] = 99; 
                $post_vals['day_of_week'] = $this->weekly;
                $post_vals['week_num'] = $val;
                break;
            case 'monthly_by_date':
                $post_vals['day_of_week'] = 0;
                $post_vals['week_num'] = 0;
                $post_vals['position_in_month'] = 99;
                $post_vals['taskday'] = $this->monthly_by_date;
                $post_vals['taskmonth'] = $val;
                break;
            case 'monthly_by_day':
                $post_vals['taskday'] = 0;
                $post_vals['week_num'] = 0;
                $post_vals['day_of_week'] = $this->month_day;
                $post_vals['position_in_month'] = $this->position_in_month;
                $post_vals['taskmonth'] = $val;
                break;
        }
        return $post_vals;
    }
    
    
    /*
     * delete all or just future/current tasks
     * 
     */
    public function deleteAllTasks($con, $min_taskid) {
        global $messages; 
        $qry = new dbUpdate("task");
        $qry->setParam("repeatingtaskid", $this->id);
        $qry->setParamGT('taskid', $min_taskid);
        $qry->setParam('taskid',$min_taskid, "or");
        $qry->setReqStringVal("state", "DELETED", "State");
        $qry->setAudit($this->clsUS->userid);
        if(!$qry->execute($con, false)) {
            $messages[] = $qry->getError();
            return false;
        } else {
            return true;
        }        
    }
    
    
       /*
     * delete all or just future/current account tasks
     * 
     */
    public function deleteAllAccountTasks($con, $min_taskid, $state) {
        global $messages;
        $qry = new dbUpdate("accounttask");
        $qry->setParam("repeatingtaskid", $this->id);
        $qry->setParamGT('taskid', $min_taskid);
        $qry->setParam('taskid',$min_taskid, "or");
        $qry->setReqStringVal("state", $state, "State");
        $qry->setAudit($this->clsUS->userid);
        //die($qry->getSql());
        if(!$qry->execute($con, false)) {
            $messages[] = $qry->getError();
            return false;
        } else {
            return true;
        }        
    }
    
    /*
     * method to get the repeating tasks in this series, and convert the calendar values to an actiondate, using the present year.
     * don't get DELETED tasks, but get inactive and active ones and we'll make them all active
     * 
     */
    public function getCurrentTaskDates($con) {
        $task_fields = " task.taskid, taskday, taskmonth, day_of_week, position_in_month, week_num ";        
        $sql = "SELECT $task_fields  FROM task WHERE state != 'DELETED' and repeatingtaskid = ".mysql_real_escape_string($this->id);
        $rs = getRS($con, $sql);
        $tasks = array();
        //$year = date('Y');
        //$days = array(1=>"monday", 2=>"tuesday", 3=>"wednesday", 4=>"thursday", 5=>"friday", 6=>"saturday", 7=>"sunday");
        switch($this->repeat_type) {
            case 'weekly':
                while($row = getRow($rs)) {
                    $tasks[$row['taskid']] = $row['week_num'];
                }
                break;
            case 'monthly_by_date':
                while($row = getRow($rs)) {
                    $tasks[$row['taskid']] = $row['taskmonth'];
                }
                break;
            case 'monthly_by_day':
                while($row = getRow($rs)) {
                    $tasks[$row['taskid']] = $row['taskmonth'];
                }
                break;            
        }
        return $tasks;
    }
    
    
   /*
     * get an array of new dates
     */
    function getTaskDates() {
        $task_dates = array(); // calculated dates
        $year = date('Y');
        $days = array(1=>"monday", 2=>"tuesday", 3=>"wednesday", 4=>"thursday", 5=>"friday", 6=>"saturday", 7=>"sunday");
        switch($this->repeat_type) {
            case 'weekly':
                foreach ($this->weeks_arr as $k => $week) {
                    $task_dates[] = $week;
                }
                break;
            case 'monthly_by_date':
                $task_dates =  $this->months_arr;
                break;
            case 'monthly_by_day':
                $task_dates =  $this->months_arr;
                break;
        }
        return $task_dates;
    }
    
    public function getDateVals($val) {
        $post_vals = array();
        switch($this->repeat_type) {
            case 'weekly':
                $post_vals['day_of_week'] = $this->weekly;
                $post_vals['week_num'] = $val;
                break;
            case 'monthly_by_date' :
                $post_vals['taskday'] = $this->monthly_by_date;
                $post_vals['taskmonth'] = $val;
                break;
            case 'monthly_by_day':
                $post_vals['day_of_week'] = $this->month_day;
                $post_vals['position_in_month'] = $this->position_in_month;
                $post_vals['taskmonth'] = $val;
                break;
        }
        return $post_vals;        
    }
    
    /*
     * when editing an accounttask that is in a repeating task series, the user can move the day part of the task.
     */
    public function updateAccounttaskDays($con, $vars, $accounttask, $end, $start) {
        $sql = "select t.*, act.accounttaskid from task t left join accounttask act on t.taskid = act.taskid where act.licenceid = {$accounttask->licenceid} and t.repeatingtaskid = {$accounttask->repeatingtaskid}";
         $rs = getRS($con, $sql);
        switch($this->repeat_type) {
        case 'weekly':
            //get min week_num of tasks were going to change
            //$week_num = getWeekNumbers($start, $end);
            if ($vars['edit_type'] == 2 || $vars['edit_type'] == 0) {
                //need to get array of future week numbers
                //if ($vars['edit_type'] == 2) $week_num = getWeekNumbers($accounttask->actiondate, $end);
                while ($row = getRow($rs)) {
                    $row['day_of_week'] = $vars['weekly'];
                    //update accounttask date
                    updateAccountTaskDates($con, $this->clsUS, $row['accounttaskid'], $row, $start, $end, false);
                }
            } else {
                //select all this task week_num
                $sql = "select t.*, act.accounttaskid from task t left join accounttask act on t.taskid = act.taskid where act.accounttaskid = {$accounttask->accounttaskid}";
                $rs = getRS($con, $sql);
                $row = getRow($rs);
                $row['day_of_week'] = $vars['weekly'];
                //update accounttask date
                updateAccountTaskDates($con, $this->clsUS, $accounttask->accounttaskid, $row, $start, $end, false);
            }
            break;
        case 'monthly_by_date' :
            if ($vars['edit_type'] == 2 || $vars['edit_type'] == 0) {
                while ($row = getRow($rs)) {
                    $row['taskday'] = $vars['month_date'];
                    //update accounttask date
                    updateAccountTaskDates($con, $this->clsUS, $row['accounttaskid'], $row, $start, $end, false);
                }
            } else {
                $sql = "select t.*, act.accounttaskid from task t left join accounttask act on t.taskid = act.taskid where act.accounttaskid = {$accounttask->accounttaskid}";
                $rs = getRS($con, $sql);
                $row = getRow($rs);
                $row['taskday'] = $vars['month_date'];
                //update accounttask date
                updateAccountTaskDates($con, $this->clsUS, $accounttask->accounttaskid, $row, $start, $end, false);
            }            
            
            break;
        case 'monthly_by_day':         
            if ($vars['edit_type'] == 2 || $vars['edit_type'] == 0) {
                while ($row = getRow($rs)) {
                    $row['day_of_week'] = $vars['month_day'];
                    $row['position_in_month'] = $vars['position_in_month'];
                    //update accounttask date
                    updateAccountTaskDates($con, $this->clsUS, $row['accounttaskid'], $row, $start, $end, false);
                }
            } else {
                $sql = "select t.*, act.accounttaskid from task t left join accounttask act on t.taskid = act.taskid where act.accounttaskid = {$accounttask->accounttaskid}";
                $rs = getRS($con, $sql);
                $row = getRow($rs);
                $row['day_of_week'] = $vars['month_day'];
                $row['position_in_month'] = $vars['position_in_month'];
                //update accounttask date
                updateAccountTaskDates($con, $this->clsUS, $accounttask->accounttaskid, $row, $start, $end, false);
            }                  
            
            
            break;
        }    
    }
} // EOC