<?php

/**
 * clsTaskFiles - saves task files from task form
 * 
 */

class accounttaskFiles{
    
    public $new_file_ids = array(); //$fileIds
    public $rm_taskfile_ids = array(); //$docDelIds by taskfileid
    public $rm_file_names = array(); //$docAddFilenames
    public $rm_file_ids = array(); //delete repeating taskfiles by fileid and taskid
    
    public function __construct() {     
        
    }
    
	
    /**
     * get newly added file ids from form
     * 
     */	
    public function newFiles() {
        if(isset($_POST['docAddId']))
        {
            $this->new_file_ids = $_POST['docAddId'];
        }            
    }
    
    /**
     * get  file ids from db - used when createing extra repeating tasks from an existing task
     * 
     */	
    public function newFilesFromDb($con, $accounttaskid) {
        $sql = "select fileid from task_files where accounttaskid = $accounttaskid";
        $rs = getRS($con, $sql);
        while ($row = getRow($rs)){
            $this->new_file_ids[] = $row['fileid'];
        }            
    }
    
        
	/**
     * get file ids to be removed from task
     * get thesed file names from form only to use to populate form again if it doesn't validate this time
     */
	public function rmFilesAdd() {
        if(isset($_POST['docDelId']))
        {
            $this->rm_taskfile_ids = $_POST['docDelId'];
        }
        
        if(isset($_POST['docAddFilename']))
        {
            $this->rm_file_names = $_POST['docAddFilename'];
        }
    }

    
    /*
     * saves fileids in array and taskid in the task_files table
     * 
     */
    public function addFiles($con, $clsUS, $accounttaskid) {
        global $messages;
        foreach($this->new_file_ids as $fileId) {
            //only let user to atach each file once to each task so check first:
            if ($this->fileCheck($con, $accounttaskid, $fileId)) {
            #insert into database 
                $qryTaskFiles = new dbInsert("task_files");
                $qryTaskFiles->setReqNumberVal("fileid", $fileId, "File Id");
                $qryTaskFiles->setReqNumberVal("accounttaskid", $accounttaskid, "Account Task Id");
                $qryTaskFiles->setAudit($clsUS->userid);
                   //echo "<br/>".$qryTaskFiles->getSql();
                if($qryTaskFiles->execute($con)) {
                    $messages[] = FILES_ATTACHED;
                } else {
                    $messages[] = "Attaching file: " . $fileId . " " . $qry->getError();
                }
            }
         }
     }
     
     /*
      * check if this file is already attached to a task
      */
    function fileCheck($con, $accounttaskid, $fileid) {
        $sql = "select fileid from task_files where accounttaskid = $accounttaskid and fileid=$fileid";
        $rs = getRS($con, $sql);
        if (getRow($rs)) return false;
        return true;
    }
     
     /*
      * rm atttached files form task_files
      * 
      */
     public function rmFiles($con, $accounttaskid) {
        global $messages;
        $fileids = $this->rm_taskfile_ids;
        if(count($this->rm_file_ids) > 0) $fileids = $this->rm_file_ids;
        //if the $this->rm_file_ids is set then use this to remove files, otherwise use $this->rm_taskfile_ids
        foreach($fileids as $rmId)
        {
            $qry = new dbDelete("task_files");
            $qry->setParam("accounttaskid", $accounttaskid);
            if(count($this->rm_file_ids) > 0) {
                $qry->setParam("fileid", $rmId);
            } else {
                $qry->setParam("taskfileid", $rmId);
            }
            //echo $qry->getSql();
            if($qry->execute($con))
            {
                $messages[] = FILES_UNATTACHED;
            }
            else
            {
                $messages[] = "UnAttaching task file id: " . $rmId . " " . $qry->getError();
            }
        }         
     }
     
     
     public function setRmFileids($con) {
        if (count($this->rm_taskfile_ids) > 0) {
            $sql = "select fileid from task_files where taskfileid in(".implode(",",$this->rm_taskfile_ids).")";
            $rs = getRS($con, $sql);
            while ($row = getRow($rs)) {
                $this->rm_file_ids[] = $row['fileid'];
            }
        }         
     }
        
}
?>