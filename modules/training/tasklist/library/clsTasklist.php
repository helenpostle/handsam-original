<?php

/**
 * clsTasklist kind of a simple crud class.  
 * 
 */

class tasklist {
    
    public $tasklistid; //tasklistid
    public $rs; //the result set row - this is used to integrate with older functionality and can be removed when that is updated.
    public $row; //same as above - can be removed later
    public $tasklistname;
    public $customtext;
    
    public function __construct($tasklistid=0) {
        $this->tasklistid = $tasklistid;
    }
    
    public function get($con) {
        $qry = "select * from tasklist where tasklistid = {$this->tasklistid}";
        $rs = getRS($con, $qry);
        $row = getRow($rs);
        $this->rs = $rs;
        $this->row = $row;
        $this->tasklistname = $row['tasklistname'];
        $this->customtext = $row['customtext'];
    }
    
   
    public function getTasklistName() {
        if ($this->customtext != '') return $this->customtext;
        return $this->tasklistname;
    }    
}
?>