<?php
/**
 * show licence menu for the selected account
 */
 
function licence_archive_title($licenceid, $accountid, $con) {
	$title = "";
	if ($licenceid > 0) {
		$qry = "SELECT * FROM accountlicence left join tasklist on accountlicence.tasklistid = tasklist.tasklistid where licenceid = ".$licenceid." and accountlicence.accountid = ".$accountid." and accountlicence.state != 'DELETED'";
		$result = getRS($con, $qry);
		$row = getRow($result);
		$title .= $row['tasklistname'];
		if (strtotime($row['start_date']) < time() && strtotime($row['end_date']) > time()) {
			$title .= " : Current Licence: ";
		} else if (strtotime($row['start_date']) > time()) {
			$title .= ": Future Licence: ";
		} else {
			$title .= ": Archived Licence: ";
		}
		
		$title .= date("Y",strtotime($row['start_date']))." - ".date("Y",strtotime($row['end_date']));
	} else {
		$title = "All Tasks Lists";
	}
	return $title;
}
?>