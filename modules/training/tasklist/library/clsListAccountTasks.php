<?php

/**
 * clsTaskList - outputs a result set which list tasks 
 * 
 */

class listAccountTasks {
    
    public $option;
    public $licenceid;
    public $categoryid;
    public $option_title; //a title that is used on the admin task list page
    public $search;
    public $categorySQL; // category filter part of query
    public $grpSQL; // category group filter part of query
    public $monthSQL; // month filter part of query
    public $userSQL; // user filter part of query
    public $searchSQL;
    
    public $category_list_sql; // part of an sql query that is used in a side box
    public $users_menu_option; // used in users side box
    public $displaydate;
    
    public function __construct($licenceid, $option, $categoryid, $userid, $month, $year, $group, $search) {
        $this->licenceid = $licenceid;
        $this->option = $option;
        $this->categoryid = $categoryid;
        $this->month = $month;
        $this->year = $year;
        $this->group = $group;
        $this->userid = $userid;
        $this->search = trim($search);
        $this->getCategorySql();
        $this->getMonthSql();
        $this->getUserSql();
        $this->getGroupSql();
        $this->getSearchSql();
    }
    
    /**
     * gets category dependent sql part of query 
     * 
     */	
    public function getCategorySql() {
        if($this->categoryid > 0) {
            $this->categorySQL = "and accounttask.categoryid=" . $this->categoryid;
        } else {
            $this->categorySQL = "";
        }
    }
	
	    /**
     * gets month dependent sql part of query 
     * 
     */	
    public function getMonthSql() {
        if ($this->month > 0 && $this->month < 13 && $this->year > 0) {
            $month_start = $this->year."-".$this->month."-01";
            $daysinmonth = date('t', strtotime($month_start));
            $month_end = $this->year."-".$this->month."-".$daysinmonth;
            $this->monthSQL = " and date(accounttask.actiondate) <= '".$month_end."' and date(accounttask.actiondate) >= '".$month_start."' ";
        } else if ($this->month == 13) {
            $this->monthSQL = " ";
        } else {
            $this->monthSQL = " and accounttask.actiondate <= DATE_ADD(NOW(), INTERVAL 28 DAY) ";
        }
    }
	
	    /**
     * gets user dependent sql part of query 
     * 
     */	
    public function getUserSql() {
        if($this->userid > 0) {
            $this->userSQL = " and accounttask.assignedto=".$this->userid;
        } else {
            $this->userSQL = "";
        }
    }
	
	    /**
     * gets category group dependent sql part of query 
     * 
     */	
    public function getSearchSql() {
        if(trim($this->search) !='') {
            $this->searchSQL = "  and accounttask.customtext like('%".$this->search."%') ";
        } else {
            $this->searchSQL = "";
        }
    }
	
     /**
     * gets category group dependent sql part of query 
     * 
     */	
    public function getGroupSql() {
        if($this->group > 0) {
            $this->grpSQL = "  and taskcategory.group_id=".$this->group;
        } else {
            $this->grpSQL = "";
        }
    }
	
	
    public function getList() {
        $task_fields = " accounttask.key_task, accounttask.accounttaskid, accounttask.actiondate, accounttask.completedate, accounttask.state, accounttask.tasktext,accounttask.customtext, accounttask.notes, accounttask.accountid, accounttask.licenceid, accounttask.taskid, accounttask.categoryid, accounttask.assignedto, accounttask.completedby, accounttask.created, accounttask.edited, accounttask.budget, accounttask.feedback, accounttask.edited_by, accounttask.created_by, accounttask.late_task_email_alert ";
        
        $fileJoinQry = " left join task_files tf on tf.accounttaskid = accounttask.accounttaskid left join files f on (tf.fileid = f.fileid ) ";
        
        switch ($this->option) {
            case "unallocated":
                $this->userSQL = " and (accounttask.assignedto is null or accounttask.assignedto = 0) "; //overrides allocated user filter
                $this->option_title = "Unallocated Tasks ";
                $this->optionqry = "SELECT $task_fields, usr.username, usr.lastname, usr.firstname, usr.userid, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid left outer join usr on accounttask.assignedto = usr.userid  $fileJoinQry   where (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') and accounttask.licenceid={$this->licenceid} {$this->categorySQL} {$this->monthSQL} {$this->userSQL} {$this->grpSQL} {$this->searchSQL} group by $task_fields, usr.username, usr.userid, taskcategory.categoryname order by accounttask.actiondate";
                $this->users_menu_option = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') {$this->userSQL} ";
                $this->displaydate = "actiondate";
                $this->category_list_sql = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED')  {$this->userSQL} ";
                break;
              
            case "active":
                $this->optionqry = "SELECT $task_fields, usr.username, usr.lastname, usr.firstname, usr.userid, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid left outer join usr on accounttask.assignedto = usr.userid  $fileJoinQry   where (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') and accounttask.licenceid={$this->licenceid} {$this->categorySQL} {$this->monthSQL} {$this->userSQL} {$this->grpSQL} {$this->searchSQL} group by $task_fields, usr.username, usr.userid, taskcategory.categoryname order by accounttask.actiondate";
                $this->option_title = "All Tasks ";
                $this->users_menu_option = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') ";
                $this->displaydate = "actiondate";
                $this->category_list_sql = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED')  ";
                break;

            case "completed":
            $this->optionqry = "SELECT $task_fields, usr.username, usr.lastname, usr.firstname, usr.userid, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid left outer join usr on accounttask.assignedto = usr.userid $fileJoinQry where accounttask.state = 'COMPLETED' and accounttask.licenceid={$this->licenceid} {$this->categorySQL} {$this->monthSQL} {$this->userSQL} {$this->grpSQL} {$this->searchSQL} group by $task_fields, usr.username, usr.userid, taskcategory.categoryname order by accounttask.actiondate" ;
            $this->users_menu_option = " and accounttask.state = 'COMPLETED' ";
            $this->option_title = "Completed Tasks ";
            $this->displaydate = "actiondate";
            $this->category_list_task_state = " and (accounttask.state = 'COMPLETED') ";
            break;

            case "uncompleted":		
            $this->optionqry = "SELECT $task_fields, usr.username, usr.lastname, usr.firstname, usr.userid, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid left outer join usr on accounttask.assignedto = usr.userid $fileJoinQry where accounttask.state != 'COMPLETED' and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW') and accounttask.licenceid={$this->licenceid} {$this->categorySQL} {$this->monthSQL} {$this->userSQL} {$this->grpSQL} {$this->searchSQL} group by $task_fields, usr.username, usr.userid, taskcategory.categoryname order by accounttask.actiondate" ;
            $this->users_menu_option = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW') ";
            $this->option_title = "Uncompleted Tasks ";
            $this->displaydate = "actiondate";
            $this->category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') ";
            break;

            case "inactive":
            $this->optionqry = "SELECT $task_fields, usr.username, usr.lastname, usr.firstname, usr.userid, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid left outer join usr on accounttask.assignedto = usr.userid $fileJoinQry where  accounttask.state = 'INACTIVE' and accounttask.licenceid={$this->licenceid} {$this->categorySQL} {$this->monthSQL} {$this->userSQL} {$this->grpSQL} {$this->searchSQL} group by $task_fields, usr.username, usr.userid, taskcategory.categoryname order by accounttask.actiondate" ;
            $this->users_menu_option = " and accounttask.state = 'INACTIVE' ";

            $this->option_title = "Inactive Tasks ";
            $this->displaydate = "actiondate";
            $this->category_list_task_state = " and (accounttask.state = 'INACTIVE') ";
            break;

            case "late":
            $this->optionqry = "SELECT $task_fields, usr.username, usr.lastname, usr.firstname, usr.userid, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid left outer join usr on accounttask.assignedto = usr.userid $fileJoinQry where accounttask.state != 'COMPLETED' and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW') and accounttask.actiondate < NOW() and accounttask.licenceid={$this->licenceid} {$this->categorySQL} {$this->monthSQL} {$this->userSQL} {$this->grpSQL} {$this->searchSQL} group by $task_fields, usr.username, usr.userid, taskcategory.categoryname order by accounttask.actiondate" ;
            $this->option_title = "Late Tasks ";
            $this->users_menu_option = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW') ";
            $this->displaydate = "actiondate";
            $this->category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') and accounttask.actiondate < NOW() ";
            break;

            case "key_task":
            $this->optionqry = "SELECT $task_fields, usr.username, usr.lastname, usr.firstname, usr.userid, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid left outer join usr on accounttask.assignedto = usr.userid  $fileJoinQry   where (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') and accounttask.key_task=1 and accounttask.licenceid={$this->licenceid} {$this->categorySQL} {$this->monthSQL} {$this->userSQL} {$this->grpSQL} {$this->searchSQL} group by $task_fields, usr.username, usr.userid, taskcategory.categoryname order by accounttask.actiondate" ;
            $this->option_title = "Key Tasks ";
            $this->users_menu_option = " and accounttask.key_task = 1 ";
            $this->displaydate = "actiondate";
            $this->category_list_task_state = " and accounttask.key_task = 1 ";
            break;

            default:
            $this->optionqry = "SELECT $task_fields, usr.username, usr.lastname, usr.firstname, usr.userid, taskcategory.customtext as categoryname, SUM(CASE WHEN f.filetype = 'adminDoc' THEN 1 ELSE 0 END) AS adminDocs, SUM(CASE WHEN f.filetype = 'userDoc' THEN 1 ELSE 0 END) AS userDocs FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid left outer join usr on accounttask.assignedto = usr.userid $fileJoinQry where (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') and accounttask.licenceid={$this->licenceid} {$this->categorySQL} {$this->monthSQL} {$this->userSQL} {$this->grpSQL} {$this->searchSQL}group by $task_fields, usr.username, usr.userid, taskcategory.categoryname order by accounttask.actiondate" ;
            $this->option_title = "All Tasks ";
            $this->users_menu_option = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') ";
            $this->displaydate = "actiondate";
            $this->category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') ";
            break;

        }
    }
    
    /*
     * get months for months nav box
     */
    function monthsNavBox($con){
        $monthsqry = "SELECT distinct MONTH(accounttask.actiondate) as month, YEAR(accounttask.actiondate) as year FROM accounttask where accounttask.state != 'INACTIVE' and accounttask.licenceid={$this->licenceid} {$this->categorySQL} {$this->searchSQL} order by accounttask.actiondate" ;
        return getRS($con, $monthsqry);
    }
    
    
    /*
     * gets title for list page
     */
    function getListTitle($months, $group_name, $category_name, $user_name, $task_search) {
        $task_search_title = '';
        if(trim($task_search) != '')
        {
          $task_search_title = " : \"$task_search\"";
        }
        if ($this->month == 0) {
            $listTitle = $this->option_title." : Now".$group_name.$category_name.$user_name.$task_search_title;
        } else if ($this->month == 13){
            $listTitle = $this->option_title." : All Months".$group_name.$category_name.$user_name.$task_search_title;
        } else {
            $listTitle = $this->option_title." : ".$months[$this->month]." ".$this->year.$group_name.$category_name.$user_name.$task_search_title;
        }
        return $listTitle;
    }
}
    ?>