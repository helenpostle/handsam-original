<?php

/**
 * clsTaskLinks - saves task links from task form 
 * 
 */

class taskLinks{
    
    public $new_link_ids = array(); //$linkIds
    public $rm_tasklink_ids = array(); //$linkDelIds
    public $rm_link_names = array(); //$linkAddFilenames
    public $rm_link_ids = array(); //delete repeating taskfiles by fileid and taskid

    
    public function __construct() {
        
    }
    
    /**
     * get newly added file ids from form
     * 
     */	
    public function newLinks() {
        if(isset($_POST['linkAddId']))
        {
            $this->new_link_ids = $_POST['linkAddId'];
        }            
    }
    
    /**
     * get  link ids from db - used when creating extra repeating tasks from an existing task
     * 
     */	
    public function newLinksFromDb($con, $taskid) {
        $sql = "select linkid from task_links where taskid = $taskid";
        $rs = getRS($con, $sql);
        while ($row = getRow($rs)){
            $this->new_link_ids[] = $row['linkid'];
        }            
    }
        
	/**
     * get file ids to be removed from task
     * get thesed file names from form only to use to populate form again if it doesn't validate this time
     */
	public function rmLinksAdd() {
        if(isset($_POST['linkDelId']))
        {
            $this->rm_tasklink_ids = $_POST['linkDelId'];
        }
        
        if(isset($_POST['linkAddFilename']))
        {
            $this->rm_link_names = $_POST['linkAddFilename'];
        }
    }
    
    /*
      * check if this file is already attached to a task
      */
    function linkCheck($con, $taskid, $linkid) {
        $sql = "select linkid from task_links where taskid = $taskid and linkid=$linkid";
        $rs = getRS($con, $sql);
        if (getRow($rs)) return false;
        return true;
    }    
    /*
     * saves linkids in  array and taskid in the task_links table
     * 
     */
    public function addLinks($con, $clsUS, $taskid) {
        global $messages;
        foreach($this->new_link_ids as $linkId) {
            if ($this->linkCheck($con, $taskid, $linkId)) {
                #insert into database 
                $qryTaskLinks = new dbInsert("task_links");
                $qryTaskLinks->setReqNumberVal("linkid", $linkId, "link Id");
                $qryTaskLinks->setReqNumberVal("taskid", $taskid, "Task Id");
                $qryTaskLinks->setAudit($clsUS->userid);
                if($qryTaskLinks->execute($con)) {
                    $messages[] = FILES_ATTACHED;
                } else {
                    $messages[] = "Attaching link: " . $linkId . " " . $qry->getError();
                }
            }
        }        
    }
    
    /*
     * remove attached links from task links table
     * 
     */
    public function rmLinks($con, $taskid) {
        global $messages;
        $linkids = $this->rm_tasklink_ids;
        if(count($this->rm_link_ids) > 0) $linkids = $this->rm_link_ids;
        //if the $this->rm_link_ids is set then use this to remove links, otherwise use $this->rm_tasklink_ids
        foreach($linkids as $rmId)
        {
            $qry = new dbDelete("task_links");
            $qry->setParam("taskid", $taskid);
            if(count($this->rm_link_ids) > 0) {
                $qry->setParam("linkid", $rmId);
            } else {
                $qry->setParam("tasklinkid", $rmId);
            }
            //echo $qry->getSql();
            if($qry->execute($con))
            {
                $messages[] = FILES_UNATTACHED;
            }
            else
            {
                $messages[] = "UnAttaching task file id: " . $rmId . " " . $qry->getError();
            }
        }         
    }    
  
    
    
     public function setRmLinkids($con) {
        if (count($this->rm_tasklink_ids) > 0) {
            $sql = "select linkid from task_links where tasklinkid in(".implode(",",$this->rm_tasklink_ids).")";
            $rs = getRS($con, $sql);
            while ($row = getRow($rs)) {
                $this->rm_link_ids[] = $row['linkid'];
            }
        }         
     }    
    
}
?>