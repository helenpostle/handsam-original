<?php
/*copy a task to a user*/
function copyTaskToUser($accounttask, $userid, $taskid, $con, $clsUS, $messages) {
	if ($userid == "" || $userid == 0) return $messages;
    $row = $accounttask->row;
    
	//also select files linked to this task which are adminDoc
	$sql = "select tf.* from task_files tf left join files f on tf.fileid = f.fileid where tf.accounttaskid = $taskid and filetype = 'adminDoc'";
	$rsFiles = getRS($con, $sql);

	//next create a custom task with this task data and assigned to this user.  Do not 
	//format the actiondate
	
	$actiondate = strtotime($row["actiondate"]);
	$row['actiondate'] = strftime("%a, %d %b %Y",$actiondate);
	$row['assignedto'] = $userid;
    $newTask = new accounttask($clsUS);
    //copies cannot be part of a repeating task series at the moment
    if (isset($row['repeatingtaskid'])) unset($row['repeatingtaskid']);
    if ($newTask->insert($con, $row)) {
		while ($rowFile = getRow($rsFiles)) {
			$qryTaskFiles = new dbInsert("task_files");
			$qryTaskFiles->setReqNumberVal("fileid",$rowFile['fileid'],"File Id");
			$qryTaskFiles->setReqNumberVal("accounttaskid",$newTask->accounttaskid,"Task Id");
			if ($qryTaskFiles->execute($con)) {
				$messages[] = FILES_ATTACHED;
			} else {
				$messages[] = "Attaching file: ".$fileId." ".$qryTaskFiles->getError();
			}			
		}	
		$messages[] = "Task copied to new user";
		$post = $_POST;
		$post['assignedto'] = $userid;
		$row['assignedto'] = 0;
		emailAssignedUsers($row, $post, $newTask->accounttaskid);
	} else {
		$messages[] = "Error copying task".$qry->getError();
    }
	return $messages;
}
?>