<?php
/**
 * task edit functions
 */
 
function check_user_task_list($accountid, $tasklistid, $userid, $con) {
	if ($userid > 0) {
		$qry = "select usr.userid as a, usr.username as b from  usr left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.accountid = $accountid and (state != 'DELETED' and state != 'INACTIVE') and usr_usrtype.tasklistid = $tasklistid and usr.userid = $userid";
		$rs = getRS($con,$qry);
		if (getResultRows($rs) > 0) {
			return true;
		}
	}
	return false;
}

//inserts an accounttask when a task is added/edited
function insertAccountTask($con, $clsUS, $accountid, $licenceid, $taskid, $vars, $start, $end, $actiondate) {
    global $core_conf;
    global $messages;
    
    //check if this is a repeating task.
    if (isset($vars["repeatingtaskid"]) && $vars["repeatingtaskid"]> 0) {
        //check for an entry in the accounttask_repeating table for this repeatingtaskid and accountid
        $sql = "select id, edit_flag from accounttask_repeating where accountid = $accountid and licenceid = $licenceid and repeatingtaskid =".$vars["repeatingtaskid"];
        //die($sql);
        $rs = getRS($con,$sql);
        
        if (getResultRows($rs)) {
            $row = getRow($rs);
            if ($row['edit_flag'] == 1) return false; //don't insert another acounttask because this account has already edited the task dates in this series .
            $accounttask_repeating_id = $row['id'];
        } else {
            //insert into accounttask_repeating
            $RTask = new RepeatingAccounttask($con, $clsUS, null, $start, $end, $accountid, $vars["repeatingtaskid"], $licenceid);
            $RTask->save($con);
            //die();
            $accounttask_repeating_id = $RTask->id;
            
        }
    } else {
        $accounttask_repeating_id = 0;
    }
    
 #for each active license without this taskid, add this new task with action date today
    $qry = new dbInsert("accounttask");
    $qry->setReqNumberVal("taskid", $taskid, "Task id");
    $qry->setReqNumberVal("accountid", $accountid, "Account id");
    $qry->setReqNumberVal("categoryid", $vars["categoryid"], "Category id");
    $qry->setReqNumberVal("licenceid", $licenceid, "licence id");
    $qry->setStringVal("customtext", $vars["customtext"], "Custom Task text");
    $qry->setReqStringVal("state", $vars["state"], "state");
    
    if (isset($vars["repeatingtaskid"])) {
        $qry->setReqNumberVal("repeatingtaskid", $vars["repeatingtaskid"], "Repeating Task Id");
    } else {
        $qry->setReqNumberVal("repeatingtaskid", 0, "Repeating Task Id");
    }
    
    
    if ($accounttask_repeating_id > 0) {
        $qry->setReqNumberVal("accounttask_repeatingid", $accounttask_repeating_id, "Repeating AccountTask Id");
    } else {
        $qry->setNullVal("accounttask_repeatingid");
    }
    
    // key task
    if($core_conf['key_task'])
    {
        if(isset($vars['key_task']) && $vars['key_task'] == 1)
            $qry->setNumberVal('key_task', 1, 'Key Task');
        else
            $qry->setNumberVal('key_task', 0, 'Key Task');
    }

     // key task date lock
    if($core_conf['key_task'] && $core_conf['key_task_lock'])
    {
        // can only be set if it's a key task
        if(isset($vars['key_task']) && $vars['key_task'] == 1) 
        {
            if(isset($vars['key_task_lock']) && $vars['key_task_lock'] == 1)
            {
                $qry->setNumberVal('key_task_lock', 1, 'Key Task Date Lockdown');
            }
            else
            {
                $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
            }
        } 
        else 
        {
            $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
        }
    }
    
    if(isset($core_conf['compliance']) && count($core_conf['compliance']) > 0) $qry->setStringVal('compliance', $vars['compliance'], 'Compliance');
   
    
    $qry->addValue("actiondate", $actiondate);
    $qry->setAudit($clsUS->userid);

    if(!$qry->execute($con)) {
        #log error for this licenceid
        $messages[] = ERROR_SAVED . " to licence id:" . $licenceid;
        return false;
    } else {
        return true;
    }
    return true;
}

/*
 * updates an accounttask when a task is edited
 */
function updateAccountTask_SA($con, $clsUS, $taskid, $vals, $repeatingtaskid=0, $start='', $end='') {
    global $core_conf;
    global $messages;
    $qry = new dbUpdate("accounttask");
    /*
    if($repeatingtaskid > 0) {
        $qry->setParam("repeatingtaskid", $repeatingtaskid);
    } else {
        $qry->setParam("taskid", $taskid);
    }
    */
    $qry->setParam("taskid", $taskid);
    
    $qry->setStringParamNotEqual("state", "COMPLETED");
    $qry->setStringParamNotEqual("state", "DELETED");
    //$qry->setStringVal("tasktext",$_POST["tasktext"],"Task description");
    $qry->setStringVal("customtext", $vals["customtext"], "Custom Task text");
    //$qry->setStringVal("tasktext",$rowTask["tasktext"],"Task text");
    $qry->setNumberVal("categoryid", $vals["categoryid"], "Category");
    $qry->setReqStringVal("flag", "EDITED", "flag");

    if (isset($vals["repeatingtaskid"])) {
        $qry->setReqNumberVal("repeatingtaskid", $vals["repeatingtaskid"], "Repeating Task Id");
    }  else if ($vals["edit_repeat"] == "enabled") {
        $qry->setReqNumberVal("repeatingtaskid", 0, "Repeating Task Id");
    }
    
    // key task
    if($core_conf['key_task']) {
        if(isset($vals['key_task']) && $vals['key_task'] == 1) { 
            $qry->setNumberVal('key_task', 1, 'Key Task');
        } else {
            $qry->setNumberVal('key_task', 0, 'Key Task');
        }
    }

    // key task date lock
    if($core_conf['key_task'] && $core_conf['key_task_lock']) {
        // can only be set if it's a key task
        if(isset($vals['key_task']) && $vals['key_task'] == 1) {
            if(isset($vals['key_task_lock']) && $vals['key_task_lock'] == 1) {
                $qry->setNumberVal('key_task_lock', 1, 'Key Task Date Lockdown');
            } else {
                $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
            }
        } 
        else {
            $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
        } 
    }    
    $qry->setReqStringVal("state", $vals["state"], "state");
    
    // HAND-940
    if(isset($core_conf['compliance']) && count($core_conf['compliance']) > 0) $qry->setStringVal('compliance', $vals['compliance'], 'Compliance');
    
    $qry->setAudit($clsUS->userid);
    //echo "<br/>".$qry->getSql();
    if($qry->execute($con, false)) {
        $messages[] = UPDATED_ACCOUNT_TASKS;
        return true;
    } else {
        $messages[] = $qry->getError();
        return false;
    }
    return true;
}

/*
 * update the accounttask date data for repeating task date criteria changes.
 */
function updateAccountTaskDates_SA($con, $clsUS, $accounttaskid, $vals, $start, $end, $ignore_assignedto = true) {
    global $core_conf;
    global $messages;
    $qry = new dbUpdate("accounttask");
    $qry->setParam("accounttaskid", $accounttaskid);
    $qry->setParam("actiondate_flag", 0); //only update accounttask which haven't had their actiondate edited
    if ($ignore_assignedto) {    
        $qry->setNullParam("assignedto"); // only update accounttasks which haven't been assigned to anybody
       $qry->setStringParamNotEqual("state", "COMPLETED");
       if (isset($vals['week_num']) && $vals['week_num'] > 0) {
           $actiondate = "'".getWeeklyDates($vals, $start, $end)."'";
           $qry->addValue("actiondate", $actiondate);
       } else if (isset($vals['position_in_month']) && $vals['position_in_month'] >= 0 && $vals['position_in_month'] <= 4 ){
           $actiondate = "'".getDayInMonth($vals, $start, $end)."'";
           $qry->addValue("actiondate", $actiondate);
       } else if (isset($vals['taskmonth']) && isset($vals['taskday'])) {
           $actiondate = "'".getMonthDate($vals, $start, $end)."'";
           //echo "<br/>".$actiondate;
           $qry->addValue("actiondate", $actiondate);
       } 
    }       
    if (isset($vals['accounttask_repeatingid']) && $vals['accounttask_repeatingid'] > 0)  $qry->setNumberVal ('accounttask_repeatingid', $vals['accounttask_repeatingid'], 'rpt id');
    //echo $qry->getSql();
    $qry->setAudit($clsUS->userid);
    if($qry->execute($con, false)) {
        return true;
    } else {
        $messages[] = $qry->getError();
        return false;
    }    
}
/*
 * delete an accounttask 
 */

function deleteAccounttask($con, $state, $taskid, $clsUS, $assignedto=false) {
    global $messages;
    $qry = new dbUpdate("accounttask");
    $qry->setParam("taskid", $taskid);
    $qry->setStringParamNotEqual("state", "COMPLETED");
    if ($assignedto == true) $qry->setNullParam("assignedto");
    $qry->setParam("actiondate_flag", 0); //only update accounttask which haven't had their actiondate edited
    $qry->setReqStringVal("state", $state, "state");
    $qry->setAudit($clsUS->userid);
    if($qry->execute($con)) {        
        return true;
    } else {
        $messages[] = $qry->getError();
        return false;
    }    
}
/*
 * insert a new task
 */

function insertTask($con, $clsUS, $vals, $taskfiles, $tasklinks, $tasklistid) {
    global $messages;
    $insertSuccess = true;
    $Task = new task($clsUS);
    if ($Task->insert($con, $vals)) {

        $taskfiles->addFiles($con, $clsUS, $Task->taskid);
        $taskfiles->rmFiles($con, $Task->taskid);

        $tasklinks->addLinks($con, $clsUS, $Task->taskid);
        $tasklinks->rmLinks($con, $Task->taskid);

        #add task
        #first get all active licenses
        $qryActiveLicences = "select accountlicence.accountid as accountid, accountlicence.licenceid as licenceid, DATE(accountlicence.start_date) as start_date, DATE(accountlicence.end_date) as end_date from accountlicence left join account on accountlicence.accountid = account.accountid where accountlicence.end_date > NOW() and accountlicence.state = 'ACTIVE' and accountlicence.tasklistid = $tasklistid";
        $resultActiveLicences = getRS($con, $qryActiveLicences);
        while ($rowActiveLicence = getRow($resultActiveLicences)) {
            #check that licence doesn't already have this taskid
            $rsCheck = getRS($con, "select taskid from accounttask where licenceid = '" . $rowActiveLicence['licenceid'] . "' and taskid = {$Task->taskid}");
            if(getResultRows($rsCheck) == 0) {
             
                $actiondate = getActiondate($vals, $rowActiveLicence['start_date'], $rowActiveLicence['end_date']);
                
                if ($actiondate) $insertSuccess = insertAccountTask($con, $clsUS, $rowActiveLicence['accountid'], $rowActiveLicence['licenceid'], $Task->taskid, $vals, $rowActiveLicence['start_date'], $rowActiveLicence['end_date'], $actiondate);
            }
        }
       
        if(!$insertSuccess)  {
            $messages[] = ERROR_SAVED;
        }
        $messages = array_unique($messages);
        return true;
    }
    $messages = array_unique($messages);
    return false;
}

/*
 * update an exisiting task
 */

function updateTask($Task, $RTask, $tasklistid, $con, $clsUS, $vals, $taskfiles, $tasklinks, $assigned = true) {
    if ($RTask->__get('id') > 0) $vals['repeatingtaskid'] = $RTask->__get('id');
    
    global $messages;
    global $core_conf;   
    if ($Task->update($con, $vals)) {

        //set repeating task row to deleted, but only if it has no more tasks. 
        if ($RTask->__get('id') > 0 && $RTask->num_repeats <=1 && !isset($vals['is_repeatable'])) $RTask->delete($con);

        $taskfiles->addFiles($con, $clsUS, $Task->taskid);
        $taskfiles->rmFiles($con, $Task->taskid);

        $tasklinks->addLinks($con, $clsUS, $Task->taskid);
        $tasklinks->rmLinks($con, $Task->taskid);

        $messages[] = SAVED;                    

        
        if (updateAccountTask_SA($con, $clsUS, $Task->taskid, $vals, 0)) {
            $messages = array_unique($messages);
            
            //now update this accounttask dates
            //$qry = "select at.edit_flag, al.accountid, al.licenceid, act.accounttaskid, date(al.start_date) as start_date, date(al.end_date) as end_date, at.id from accounttask act left join accountlicence al on act.licenceid = al.licenceid left join accounttask_repeating at on act.accounttask_repeatingid = at.id where act.taskid = ".$Task->taskid." and al.end_date > NOW() and al.state = 'ACTIVE' and (at.edit_flag=0 or at.edit_flag is null)";
            
            $qry = "SELECT AT.edit_flag, al.accountid, al.licenceid, act.accounttaskid, DATE(al.start_date) AS start_date, DATE(al.end_date) AS end_date, AT.id FROM accountlicence al LEFT JOIN  accounttask act  ON act.licenceid = al.licenceid AND act.taskid = ".$Task->taskid." LEFT JOIN accounttask_repeating AT ON act.accounttask_repeatingid = AT.id WHERE al.end_date > NOW() AND al.state = 'ACTIVE' AND al.tasklistid = $tasklistid AND (AT.edit_flag=0 OR AT.edit_flag IS NULL) ORDER BY al.licenceid";
            //echo "<br/>qry: ".$qry;
            //die();
            $rs = getRS($con, $qry);
            
            while ($row = getRow($rs)) {
                $vals['accounttask_repeatingid'] = $row['id'];
                $vals['accountid'] = $row['accountid'];
                $vals['licenceid'] = $row['licenceid'];
                if (isset($vals['repeatingtaskid']) && $vals['repeatingtaskid'] > 0) $vals['accounttask_repeatingid'] = checkForAccRep($vals, $con, $clsUS, $row['start_date'], $row['end_date'], $row['edit_flag']);
                if ($vals['accounttask_repeatingid']!=false) {
                    if($row['accounttaskid'] > 0) {
                        //echo "<br/>update licence:".$row['licenceid']." taskid: ".$row['accounttaskid'];
                        $update = updateAccountTaskDates_SA($con, $clsUS, $row['accounttaskid'], $vals, $row['start_date'], $row['end_date'], $assigned);
                    } else {
                        //insert an accointtask here
                      //echo "<br/>insert licence:".$row['licenceid']." date: ".$actiondate;
                        
                        $actiondate = getActiondate($vals, $row['start_date'], $row['end_date']);
                        $insertSuccess = insertAccountTask($con, $clsUS, $row['accountid'], $row['licenceid'], $Task->taskid, $vals, $row['start_date'], $row['end_date'], $actiondate);
                    }
                }
            }
            return true;
        } else {
            $messages = array_unique($messages);
            return false;
        }        
        /*
        //if the state is active and a licence doesn';t have this task, then insert it into accounttask.
        if ($vals['state'] == 'ACTIVE') {
            $qryActiveLicences = "select accountlicence.accountid as accountid, accountlicence.licenceid as licenceid, DATE(accountlicence.start_date) as start_date, DATE(accountlicence.end_date) as end_date from accountlicence left join account on accountlicence.accountid = account.accountid where accountlicence.end_date > NOW() and accountlicence.state = 'ACTIVE' and accountlicence.tasklistid = $tasklistid";
            $resultActiveLicences = getRS($con, $qryActiveLicences);
            while ($rowActiveLicence = getRow($resultActiveLicences)) {
                #check that licence doesn't already have this taskid
                $rsCheck = getRS($con, "select taskid from accounttask where licenceid = '" . $rowActiveLicence['licenceid'] . "' and taskid = '{$Task->taskid}'");
                if(getResultRows($rsCheck) == 0) {
                    $actiondate = getActiondate($vals, $rowActiveLicence['start_date'], $rowActiveLicence['end_date']);
                    $insertSuccess = insertAccountTask($con, $clsUS, $rowActiveLicence['accountid'], $rowActiveLicence['licenceid'], $Task->taskid, $vals, $rowActiveLicence['start_date'], $rowActiveLicence['end_date'], $actiondate);
                }
            }  
        } 
        
         */                               

    }    
}

/*
 * updates a repeating task series when the date criteria are edited only - not the actual task data
 */
function updateTaskDates($Task, $RTask, $tasklistid, $con, $clsUS, $vals, $taskfiles, $tasklinks, $active_lic) {
    global $messages;
    global $core_conf;
    if ($Task->updateTaskDates($con, $vals)) {
        
        //set repeating task row to deleted, but only if it has no more tasks. 
        if ($RTask->__get('id') > 0 && $RTask->num_repeats <=1 && !isset($vals['is_repeatable'])) $RTask->delete($con);

        $taskfiles->addFiles($con, $clsUS, $Task->taskid); //we aren't removing any other files or links froim this task -which mayb we should?
        $tasklinks->addLinks($con, $clsUS, $Task->taskid);                    
        
        $update = true;
        //get all accountlicences for this task
        $qry = "SELECT al.licenceid, al.accountid, DATE(al.start_date) AS start_date, DATE(al.end_date) AS end_date 
                FROM accountlicence al left join task t on al.tasklistid = t.tasklistid
                WHERE t.taskid = ".$Task->taskid." AND al.end_date > NOW() AND al.state = 'ACTIVE'";
        $rs = getRS($con, $qry);
        $updated_lic = array();
        while ($row = getRow($rs)) {
            //$vals['accounttask_repeatingid'] = checkForAccRep($vals, $con, $clsUS, $row['start_date'], $row['end_date']);
            $sql = "select act.accounttaskid from accounttask act left join accounttask_repeating at on act.accounttask_repeatingid = at.id where act.licenceid = {$row['licenceid']} and act.state='ACTIVE' and at.edit_flag=0 and act.taskid=".$Task->taskid;  
            $rs2 = getRS($con, $sql);
            
            while ($row2 = getRow($rs2)) {
                $updated_lic[$row['licenceid']] = $row['licenceid'];
                $actiondate = getActiondate($vals, $row['start_date'], $row['end_date']);
                if($row2['accounttaskid'] > 0) {
                    if ($actiondate) {
                        $update = updateAccountTaskDates_SA($con, $clsUS, $row2['accounttaskid'], $vals, $row['start_date'], $row['end_date'], $actiondate);                 
                    } else {
                        $acTask = new accounttask($clsUS, $row2['accounttaskid']);
                        $update = $acTask->delete($con);  
                    }
                } else {
                  //insert an accounttask
                  $insertSuccess = insertAccountTask($con, $clsUS, $row['accountid'], $row['licenceid'], $Task->taskid, $vals, $row['start_date'], $row['end_date'], $actiondate);                          
                }               
            }                   
        }
        $missing_lic = array_diff_key($active_lic, $updated_lic);
        //if the state is active and a licence doesn't have this task, then insert it into accounttask.
        if ($vals['state'] == 'ACTIVE' && count($missing_lic) > 0) {
            foreach ($missing_lic as $lic) {
                $rowActiveLicence = $lic;
                $actiondate = getActiondate($vals, $rowActiveLicence['start_date'], $rowActiveLicence['end_date']);
                $insertSuccess = insertAccountTask($con, $clsUS, $rowActiveLicence['accountid'], $rowActiveLicence['licenceid'], $Task->taskid, $vals, $rowActiveLicence['start_date'], $rowActiveLicence['end_date'], $actiondate);
            }  
        }    
        return $update;
    }    
}

/*
 * function to save accounttask_repeating
 */

function updateAccounttaskRepeating($con, $rtaskid, $clsUS, $vars, $licence_arr) {
    //need to loop through each accounttask_repeating for this repeatingtaskid
    foreach ($licence_arr as $row) {
        if ($row['edit_flag'] == 0) {
            $RTask = new RepeatingAccounttask($con, $clsUS, $row['accounttask_repeatingid'], $row['start_date'], $row['end_date'], $row['accountid'], $rtaskid, $row['licenceid']);
            $RTask->save($con, false,false,false);
        }   
    }
}


function getActiondate($vars, $start, $end) {
    
    //need to process repeating weekly and month by day tasks to get the taskday and taskmonth
    if (isset($vars['week_num']) && $vars['week_num'] > 0) {
        $actiondate = getWeeklyDates($vars, $start, $end);
    } else if (isset($vars['position_in_month']) && $vars['position_in_month'] >= 0 && $vars['position_in_month'] <= 4 ) {
        $actiondate = getDayInMonth($vars, $start, $end);
    } else if (isset($vars['taskmonth']) && isset($vars['taskday'])) {
        $actiondate = getMonthDate($vars, $start, $end);
    } else { 
        $actiondate = " NOW() ";
    }
    if  (isset($vars['repeat_end_month']) && isset($vars['repeat_start_month']) && (date('n', strtotime($actiondate)) > $vars['repeat_end_month'] || date('m', strtotime($actiondate)) < $vars['repeat_start_month'])) return false;
    if ($actiondate != " NOW() ") $actiondate = "'".$actiondate."'";
    return $actiondate;
}


function checkForAccRep($vals, $con, $clsUS, $start, $end, $flag) {
    if($flag == 1) return false;
    if(isset($vals['accounttask_repeatingid']) && $vals['accounttask_repeatingid']>0) {
        return $vals['accounttask_repeatingid'];
    }
    $sql = "select id, edit_flag from accounttask_repeating where accountid = {$vals['accountid']} and licenceid = {$vals['licenceid']} and repeatingtaskid =".$vals["repeatingtaskid"];
    $rs = getRS($con, $sql);
    $row = getRow($rs);
    if ($row['id'] > 0) return $row['id'];
    $RTask = new RepeatingAccounttask($con, $clsUS, null, $start, $end, $vals['accountid'], $vals["repeatingtaskid"], $vals['licenceid']);
    $RTask->save($con);
    return $RTask->id;
}

//get current licnece for a tasklist
function getActiveLicences($tasklistid, $con) {
    $qry = "select ar.edit_flag, ar.id as accounttask_repeatingid, ar.repeatingtaskid, accountlicence.accountid as accountid, accountlicence.licenceid as licenceid, DATE(accountlicence.start_date) as start_date, DATE(accountlicence.end_date) as end_date from accountlicence  left join accounttask_repeating ar on accountlicence.licenceid = ar.licenceid where accountlicence.end_date > NOW() and accountlicence.state = 'ACTIVE' and accountlicence.tasklistid = $tasklistid";
    $result = getRS($con, $qry);
    $arr = array();
    while ($row = getRow($result)) { 
        $arr[$row['licenceid']] = $row;
    } 
    return $arr;
}
?>