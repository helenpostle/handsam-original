<?php

/**
 * clsTaskList - outputs a result set which list tasks 
 * 
 */

class listTasks{
    
    public $option;
    public $tasklistid;
    public $categoryid;
    public $categorySQL;
    public $option_title; //a title that is used on the admin task list page
    public $category_list_sql; // part of an sql query that is used in a side box
    
    public function __construct($tasklistid, $option, $categoryid) {
        $this->tasklistid = $tasklistid;
        $this->option = $option;
        $this->categoryid = $categoryid;
    }
    
	
    /**
     * gets category dependent sql part of query 
     * 
     */	
    public function getCategorySql() {
        if($this->categoryid > 0) {
            $this->categorySQL = "and task.categoryid=" . $this->categoryid;
        } else {
            $this->categorySQL = "";
        }
        return $this->categorySQL;
    }
	
	
    public function getList() {
        
        //to convert the weekly repeating values into taskday and taskmonth for repeating tasks
        $weekly_case = ", CASE  WHEN week_num > 0 THEN ";
        $weekly_case .= " CASE WHEN dayofweek(YEAR(CURDATE())) > day_of_week THEN CONVERT(date_format(date_add( date_add( date_add(CONCAT(YEAR(CURDATE()),'-01-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-01-01'))) % 7 DAY) , INTERVAL week_num - 1 week), INTERVAL day_of_week DAY), '%e'), UNSIGNED INTEGER) ";
        $weekly_case .= " ELSE CONVERT(date_format(date_add( date_add( date_add(CONCAT(YEAR(CURDATE()),'-01-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-01-01'))) % 7 DAY) , INTERVAL week_num - 2 week), INTERVAL day_of_week DAY), '%e'), UNSIGNED INTEGER) END ";
        
        //week 1 of month
        $weekly_case .= " WHEN position_in_month = 0 THEN CONVERT(date_format(date_add(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')) + day_of_week) % 7 DAY), '%e'), UNSIGNED INTEGER) ";
        //week 2 of month
        $weekly_case .= " WHEN position_in_month = 1 THEN CONVERT(date_format(date_add(date_add(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')) + day_of_week) % 7 DAY), interval 1 week), '%e'), UNSIGNED INTEGER) ";
        //week3 of month
        $weekly_case .= " WHEN position_in_month = 2 THEN CONVERT(date_format(date_add(date_add(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')) + day_of_week) % 7 DAY), interval 2 week), '%e'), UNSIGNED INTEGER) ";
        //week 4 of month
        $weekly_case .= " WHEN position_in_month = 3 THEN CONVERT(date_format(date_add(date_add(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')) + day_of_week) % 7 DAY), interval 3 week), '%e'), UNSIGNED INTEGER) ";
        //last week of month
        $weekly_case .= " WHEN position_in_month = 4 THEN CONVERT(date_format(date_add(LAST_DAY(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')), INTERVAL (-6 - dayofweek(LAST_DAY(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'))) + day_of_week) % 7 DAY), '%e'), UNSIGNED INTEGER) ";
        $weekly_case .= " else taskday END AS taskday";
        
        $weekly_case .= " , CASE  WHEN week_num > 0 THEN ";    
        $weekly_case .= " CASE WHEN dayofweek(YEAR(CURDATE())) > day_of_week THEN CONVERT(date_format(date_add( date_add( date_add(CONCAT(YEAR(CURDATE()),'-01-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-01-01'))) % 7 DAY) , INTERVAL week_num - 1 week), INTERVAL day_of_week DAY), '%c'), UNSIGNED INTEGER) ";
        $weekly_case .= " ELSE CONVERT(date_format(date_add( date_add( date_add(CONCAT(YEAR(CURDATE()),'-01-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-01-01'))) % 7 DAY) , INTERVAL week_num - 2 week), INTERVAL day_of_week DAY), '%c'), UNSIGNED INTEGER) END else taskmonth END AS taskmonth ";
        
        $task_fields = " task.taskid, task.tasktext, task.tasklistid, task.categoryid, task.created, task.edited, task.state, task.edited_by, task.created_by, task.mid, task.customtext, task.key_task, task.key_task_lock, task.repeatingtaskid, task.day_of_week, task.position_in_month, task.week_num ";
        switch ($this->option) {
            case "active":
                $this->optionqry = "SELECT $task_fields $weekly_case  FROM task left join tasklist on task.tasklistid = tasklist.tasklistid where task.state = 'ACTIVE' " . $this->categorySQL . "  and task.tasklistid = " . $this->tasklistid . " and tasklist.state = 'ACTIVE' order by taskmonth,taskday";
                $this->option_title = "All Tasks ";
                $this->category_list_sql = " and task.state = 'ACTIVE' ";
                break;

            case "key_task":
                $this->optionqry = "SELECT $task_fields $weekly_case FROM task left join tasklist on task.tasklistid = tasklist.tasklistid where (task.state = 'ACTIVE' or task.state = 'INACTIVE') " . $this->categorySQL . "  and task.tasklistid = " . $this->tasklistid . " and tasklist.state = 'ACTIVE' and task.key_task=1  order by task.taskmonth,task.taskday";
                $this->option_title = "Key Tasks ";
                $this->category_list_sql = " and task.key_task = 1 ";
                break;

            case "inactive":
                $this->optionqry = "SELECT $task_fields $weekly_case FROM task left join tasklist on task.tasklistid = tasklist.tasklistid where  task.state = 'INACTIVE' " . $this->categorySQL . "  and task.tasklistid = " . $this->tasklistid . "  and tasklist.state = 'ACTIVE'   order by task.taskmonth,task.taskday";
                $this->option_title = "Inactive Tasks ";
                $this->category_list_sql = " and task.state = 'INACTIVE' ";
                break;
            case "deleted":
                #not set up undelete permissions yet!
                if(isAllowed("undelete")) {
                    $optionqry = "SELECT $task_fields $weekly_case FROM task left join tasklist on task.tasklistid = tasklist.tasklistid where  task.state = 'DELETED' " . $this->categorySQL . "  and task.tasklistid = " . $this->tasklistid . " and tasklist.state = 'ACTIVE'  order by task.taskmonth,task.taskday";
                    $this->option_title = "Deleted Tasks ";
                    $this->category_list_sql = " and task.state = 'DELETED' ";
                }
                break;
            default:
                $this->optionqry = "SELECT $task_fields $weekly_case FROM task left join tasklist on task.tasklistid = tasklist.tasklistid where task.state != 'INACTIVE' " . $this->categorySQL . "  and task.tasklistid = " . $this->tasklistid . " and tasklist.state = 'ACTIVE'  order by task.taskmonth,task.taskday";
                $this->option_title = "All Tasks ";
                $this->category_list_sql = " and task.state != 'INACTIVE' ";
                break;
        }
    }
    
    
}
    ?>