<?php
$this_page = "taskcategory.php";
/**
* list and edit task categories
* content: taskcategorylist.php, taskcategoryedit.php
* boxes: none
*/

/**
 * Include basic stuff
 */
$secure = true;
 
require("../../shared/startPage.php");

$pageFiles->includePhp();

#check if have requested a specific category
$categoryid = getIntFromRequest("categoryid");
$tasklistid = getIntFromRequest("tasklistid");


$title = "";

#we can do this if  we have permission to edit tasks.
if (isAllowed("editTask")) {
	$title = "Task Category List";
	
	if (buttonClicked("cancel")) {
		#change content to list
		$content = "taskcategorylist.php";		
	} else {
	
	
		if ($categoryid !== null) {
			$content = "taskcategoryedit.php";	
			$title = "Edit Task Category";
                        
			if (buttonClicked("save")) {				
				$rsCheck = getRS($con,"select * from taskcategory where customtext = '".mysql_real_escape_string($_POST["customtext"])."'  and customtext != '' and customtext is not null and tasklistid = $tasklistid and categoryid != ".$categoryid);

				if (getRow($rsCheck)) {
					$messages[] = CATEGORYNAME_IN_USE;	
				} else {
								
					if ($categoryid == 0) {
						$qry = new dbInsert("taskcategory");
						$qry->setReqNumberVal("tasklistid",$tasklistid,"task list");
						$qry->setReqStringVal("customtext",$_POST["customtext"],"Custom Task category name");
					} else {
						$qry = new dbUpdate("taskcategory");
						$qry->setParam("categoryid",$categoryid);
						if (isset($rowTaskcategory['mid']) && $rowTaskcategory['mid'] > 0) {
							$qry->setStringVal("customtext",$_POST["customtext"],"Custom Task category name");
						} else {
							//not a handsam task so need some text!
							$qry->setReqStringVal("customtext",$_POST["customtext"],"Custom Task category name");
						}
					}
				
					if (isset($qry)) {
						//$qry->setReqStringVal("categoryname",$_POST["categoryname"],"Category name");
						$qry->setReqStringVal("state",$_POST["state"],"state");
						if ($tasklist_conf['category_group_assign'] == true && isset($_POST["group_id"])) { 
                $qry->setNumberVal("group_id",$_POST["group_id"],"category group");
            }
						
						$qry->setAudit($clsUS->userid);
						
						if ($qry->execute($con)) {
							if ($categoryid == 0) {
								$categoryid = $qry->getNewID();	
							}
							//if state = INACTIVE  then change all tasks to inactive too
							//if ($_POST["state"] == 'INACTIVE') {
								$qry2 = new dbUpdate("task");
								$qry2->setParam("categoryid",$categoryid);
								//$qry2->setReqStringVal("state","INACTIVE","State");
                                $qry2->setReqStringVal("state",$_POST["state"],"State");
                                $qry2->setAudit($clsUS->userid);
								if (!$qry2->execute($con)) {
									$messages[] = $qry2->getError();
								}						
										
							//}
							
							$messages[] = SAVED;
							#change content to list
							$content = "taskcategorylist.php";
                                                        $title = "Task Category List";
							
						} else {
                            $messages[] = ERROR_SAVED;
							$messages[] = $qry->getError();	
						}
					}
				}
			} else if (buttonClicked("delete")) {
				
					$qry = new dbUpdate("taskcategory");
					$qry->setParam("categoryid",$categoryid);
					$qry->setReqStringVal("state","DELETED","State");
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					} else {
						//now delete all tasks with this category too.
						$qry2 = new dbUpdate("task");
						$qry2->setParam("categoryid",$categoryid);
						$qry2->setReqStringVal("state","DELETED","State");
                        $qry2->setAudit($clsUS->userid);
						if (!$qry2->execute($con)) {
                            $messages[] = ERROR_SAVED;
							$messages[] = $qry2->getError();
						}						
						#change content to list
						$messages[] = DELETED;
						$content = "taskcategorylist.php";
					}
			} 			
		} else {
			$content = "taskcategorylist.php";
		}
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);
	headerLocation("$rootUrl/index.php",false);
}

#Include layout page
include("layout.php");
