<?php
/**
 * Send email alerts for late tasks
 * 
 */

// set the module name
$this_mod_dir = 'tasklist';

tasklistAlerts($con);

function tasklistAlerts($con, $userid=0, $accountid=0, $accounttaskid=0) {
	global $send_email_alerts;
	global $core_conf;
	global $alertsMail;
	global $rootUrl;
        
	if ($send_email_alerts) { 
		doLog("INFO","LATE TASK EMAIL ALERTS:");
                
		#go through all users which have accounttasks which are late, uncompleted and haven't had an email alert sent, where the assigned_to user is set to receive email alerts
		$user_qry = "";
		$account_qry = "";
		$accounttask_qry = "";
		if($userid > 0) $user_qry = " and usr.userid=$userid ";
		if($accountid > 0) $account_qry = " and usr.accountid=$accountid ";
		if (is_array($accounttaskid)) {
			$accounttask_qry = " and accounttask.accounttaskid in (".implode(",",$accounttaskid).") ";
		} else if($accounttaskid > 0) {
			$accounttask_qry = " and accounttask.accounttaskid=$accounttaskid ";
		}
		$qry = "select distinct usr.*, accounttask.licenceid from accounttask left join usr on accounttask.assignedto = usr.userid LEFT JOIN accountlicence ON accounttask.licenceid = accountlicence.licenceid left join account on usr.accountid = account.accountid where accounttask.state = 'ACTIVE' and usr.state = 'ACTIVE' AND accountlicence.state = 'ACTIVE' and account.state = 'ACTIVE' and actiondate < NOW() and accounttask.late_task_email_alert = 0 and accounttask.assignedto > 0 $user_qry $account_qry ORDER BY userid";
			
		$result = getRS($con, $qry);
		
		//store admin email alerts as an array with licenceid as the key
		
		while ($row = getRow($result)) {
						
			#now go through the user and select their late tasks
			$taskQry = "select distinct usr.*, accounttask.*  from accounttask left join usr on accounttask.assignedto = usr.userid where accounttask.state = 'ACTIVE' and actiondate < NOW() and accounttask.late_task_email_alert = 0 AND accounttask.assignedto > 0  and usr.userid = {$row['userid']} ORDER BY actiondate";
			
			$taskRS = getRS($con, $taskQry);
			
			#if the task hasn't been processed and a reminder sent
			if ($row['late_task_email_alert'] == 1) {
		
				$accounttaskid_arr = array();
				$taskStr = "";
				
				
				if (!isset($admin_alert_txt[$row['licenceid']])) $admin_alert_txt[$row['licenceid']] = "";
				$admin_alert_txt[$row['licenceid']] .= ucwords($row['firstname']." ".$row['lastname'])."(".$row['username'].") late tasks: \r\n";	
				while ($taskRow = getRow($taskRS)) {
					$taskStr .= $taskRow['customtext']." - $rootUrl/modules/tasklist/mytasks.php?accounttaskid=".$taskRow['accounttaskid']."\r\n";
					
					//create the array key for this licenceid if it isn't alraedy 
					$accounttaskid_arr[] = $taskRow['accounttaskid'];
					$admin_alert_txt[$row['licenceid']] .=  $taskRow['customtext']." - $rootUrl/modules/tasklist/tasks.php?accounttaskid=".$taskRow['accounttaskid']."\r\n";		
				}	
				
        $body = getEmailTemplate('task_overdue', $taskStr);
								
				if ($core_conf['email_alert_mode'] == "TEST") {
					$alert_to = $core_conf['email_alert_test_addr'];
					//prepend real recipient to beginning of alert
					$body = "The alerts system is in TEST mode. This email should have been sent to: ".$row['username']."\r\n $body";

				} else {
					$alert_to = $row['username'];
				}
				
				if (sendMail($alert_to, $alertsMail, ALERT_LATE_TASK_TITLE, $body)) {
					#now update accounttask to mail sent for this task
					
					if ($core_conf['email_alert_mode'] == "TEST") {
						doLog("INFO","Username: ".$row['username']." :: userid: ".$row['userid']." :: accounttaskid: ".implode(',',$accounttaskid_arr)." :: TEST EMAIL SENT TO $alert_to");
					} else {
					
						$qry = new dbUpdate("accounttask");
						$qry->setParamInArray("accounttaskid",$accounttaskid_arr);
						$qry->setReqNumberVal("late_task_email_alert",1,"late task email alert");
								
						if ($qry->execute($con)) {
							doLog("INFO","Accounttaskid: ".implode(',',$accounttaskid_arr)." :: Updated: late_task_email_alert: ".$qry->getSql());
						} else  {
							doLog("INFO","Accounttaskid: ".implode(',',$accounttaskid_arr)." :: ERROR UPDATING: late_task_email_alert: ".$qry->getSql()." :: ".$qry->getError());
						}
					}		
				} else {
					doLog("INFO","Username: ".$row['username']." :: userid: ".$row['userid']." :: accounttaskid: ".implode(',',$accounttaskid_arr)." :: EMAIL FAILURE");
				}
			} else {
				
				//populate admin alert array:
				if (!isset($admin_alert_txt[$row['licenceid']])) $admin_alert_txt[$row['licenceid']] = "";
				$admin_alert_txt[$row['licenceid']] .= ucwords($row['firstname']." ".$row['lastname'])."(".$row['username'].": email alerts off for this user) late tasks: \r\n";	
										
				#update any new out of date tasks to late_task_email_alert value = 99 so that when a user's email setting activated, they don't get sent old out of date task reminders
				$accounttaskid_arr = array();
				while ($taskRow = getRow($taskRS)) {
					$accounttaskid_arr[] = $taskRow['accounttaskid'];
					//add admin alert
					$admin_alert_txt[$row['licenceid']] .=  $taskRow['customtext']." - $rootUrl/modules/tasklist/tasks.php?accounttaskid=".$taskRow['accounttaskid']."\r\n";			
				}
				if ($core_conf['email_alert_mode'] == "LIVE") {
					$qry = new dbUpdate("accounttask");
					$qry->setParamInArray("accounttaskid",$accounttaskid_arr);
					$qry->setReqNumberVal("late_task_email_alert",99,"late task email alert");
							
					if (!$qry->execute($con)) {
						doLog("INFO","Accounttaskid: ".implode(',',$accounttaskid_arr)." :: ERROR UPDATING: late_task_email_alert: ".$qry->getSql()." :: ".$qry->getError());
					} else {
						doLog("INFO","Accounttaskid: ".implode(',',$accounttaskid_arr)." :: UPDATING: late_task_email_alert to 99 : ".$qry->getSql());
					}
				}
			}
				
		}
		
		if (isset($admin_alert_txt) && is_array($admin_alert_txt) && count($admin_alert_txt) > 0) {
		//now loop through $admin_alert_txt by licenceid
			foreach ($admin_alert_txt as $licenceid => $msg) {
				//we need the accountid, tasklistid and a list of admin users for this tasklist in the account
				$qry = "select al.tasklistid, al.accountid from accountlicence al where al.licenceid=$licenceid";
				$rs = getRS($con, $qry);
				$row = getRow($rs);
				
				//get each admin for this licence
				$qry = " select u.* from usr u left join usr_usrtype uut on u.userid = uut.userid where uut.tasklistid={$row['tasklistid']} and u.accountid = {$row['accountid']} and u.state = 'ACTIVE' and u.late_task_email_alert = 1 and uut.usertype = 'Administrator' ";
				$rs = getRS($con, $qry);
				$admin_list = array();
				while ($rowAdmin = getRow($rs)) {
					$admin_list[] = $rowAdmin['username'];
				}
				$to = implode(",", $admin_list);
                                $body = getEmailTemplate('task_overdue_admin', $msg);
				
				if ($core_conf['email_alert_mode'] == "TEST") {
					$alert_to = $core_conf['email_alert_test_addr'];
					//prepend real recipient to beginning of alert
					$body = "The alerts system is in TEST mode. This email should have been sent to: $to \r\n $body";
				} else {
					$alert_to = $to;
				}
				
				if (sendMail($alert_to, $alertsMail, ALERT_LATE_TASK_TITLE."s", $body)) {
					if ($core_conf['email_alert_mode'] == "TEST") {
						doLog("INFO","Accountid: ".$row['accountid']." :: licenceid: $licenceid :: ADMIN email alert to:: $to :: TEST EMAIL SENT TO $alert_to");
					} else {
						doLog("INFO","Accountid: ".$row['accountid']." :: licenceid: $licenceid :: ADMIN email alert to:: $to :: EMAIL SENT");
					}
				} else {
					if ($core_conf['email_alert_mode'] == "TEST") {
						doLog("INFO","Accountid: ".$row['accountid']." :: licenceid: $licenceid :: ADMIN email alert to:: $to :: TEST EMAIL FAILURE to $alert_to");
					} else {
						doLog("INFO","Accountid: ".$row['accountid']." :: licenceid: $licenceid :: ADMIN email alert to:: $to :: EMAIL FAILURE");
					}
				}
				
				
			}
		}
				
		
	}
}
?>