<?php
/**
* account admin account tasks editing
* must either be an account admin(no licence id needed) or a (super admin with licence id supplied)
* content: tasklist.php, taskedit.php
* boxes: taskcategories.php, taskoptions.php
*/
/**
 * Include basic stuff
 */

$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc('tasklist_licence_archive');
$pageFiles->addModFunc('tasklist_accounttask_edit');
$pageFiles->addModFunc('tasklist_copy_task');
$pageFiles->addModFunc('assignto_multi_user');
$pageFiles->addModFunc('select_multi_tasks');
$pageFiles->addFunc('handsam_uploads');
if ($tasklist_conf['collapse_managetask_resources'] === true) $pageFiles->addModFunc('tasklist_resources');

$pageFiles->addModFunc('tasklist_list_accounttasks');
//new copy task to user function
$pageFiles->includePhp();

$accounttaskid = getIntFromRequest("accounttaskid");

$hacking = false;
$customTask = false;
$newCustomTask = false;
$return = getStrFromRequest("return");
$title = "";

$category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') ";//check if this is needed here


//need to get tasklistid, licenceid, accountid, categoryname, customtask, newcustomtask, $rowAccountTask
if ($accounttaskid > 0) {
    $accounttask = new accounttask($clsUS, $accounttaskid);
    $accounttask->get($con);
    $accountid = $accounttask->accountid;
    $rowAccountTask = $accounttask->row;
    $licenceid = $accounttask->licenceid;
    $accountlicence = new Licence($clsUS, $licenceid);
    $accountlicence->get($con);
	$tasklistid = $accountlicence->tasklistid;
    $taskcat = new taskcategory($accounttask->categoryid);
    $taskcat->get($con);
    $categoryname = $taskcat->getCatName();
    $customTask = $accounttask->customtask();
    $title = "Edit Task";
} else {
    $categoryname = "";
    //get tasklistid from accountlicence
	$licenceid = getIntFromRequest("licenceid");
	if ($licenceid > 0) {
        $accountlicence = new Licence($clsUS, $licenceid);
        $accountlicence->get($con);
		$tasklistid = $accountlicence->tasklistid;
        $accountid = $accountlicence->accountid;
	} else {
        $tl_row = getLicenceId($clsUS->userid, $con);
		$tasklistid = $tl_row['tasklistid'];
		$licenceid = $tl_row['licenceid'];
        $accountid = $tl_row['accountid'];
        $accountlicence = new Licence($clsUS, $licenceid);
        $accountlicence->get($con);
	}
    
}

if (!isAllowed("editAccount") && isAllowed("editOurTasks",1 , $tasklistid)) {
  	#is an account admin
	$accountid = $clsUS->accountid;
	if (isset($accounttaskid) && $accounttaskid > 0) {
		//$title = "Edit Task";
		#get licenceid, category name and if there's a taskid  from this $accounttaskid
		//$licenceid = $accounttask->licenceid;
	 	//$Taskcat = new taskcategory($rowAccountTask->categoryid);
        //$Taskcat->get($con);
        //$categoryname = $Taskcat->getCatName();
	 	#check if this task is for their account
	 	if ($accounttask->accountid != $accountid) {
		 	$hacking = true;
	 	}
	 	//$customTask = $accounttask->customtask();
 	} else if ($accounttaskid === 0) {
		if (!isLicence($licenceid)) {
			$hacking = true;
		}
 	} else if(!isLicence($licenceid)) {
			$hacking = true;
 	}
} else if (!isAllowed("editAccount")) {
	$hacking = true;
}

if (!$hacking) {
	//get current licence end date for use in calendar  to validate task date less than this
	$licence_end = $accountlicence->end_date;
	#check if this is the current licence. if its an old licence don't diaplay actions box
	if (strtotime($licence_end) > time()) {
		$box[] = "box.actions.php";
		$current_licence = true;
	} else {
		$actions_box = "";
		$current_licence = false;
	}
			
	#check for month, year, options and set to all if none
	$month = getIntFromRequest("month", 0);
	$year = getIntFromRequest("year", 0);
	$user = getIntFromRequest("user", 0);
	$option = getStrFromRequest("option", "active");
	
	$multi = getStrFromRequest("multi");
	$category = getIntFromRequest("category", 0);
    $group_id = getIntFromRequest("group_id", 0);
	$datefield = getIntFromRequest("datefield");
	
	#get username:
	if ($user > 0) {
		$qry = "SELECT username FROM usr where userid = ".$user;
		$result = getRS($con, $qry);
		$row = getRow($result);
		$user_name = " : ".textSummary($row['username'],22);
	} else {
		$user_name = " : All Users";
	}
	
	#get category title
	if ($category > 0) {
        $Taskcat = new taskcategory($category);
        $Taskcat->get($con);
		$category_name = " : ".displayText($Taskcat->getCatName());
	} else {
		$category_name = " : All Categories";
	}		
		
	#get group title
	if ($group_id > 0) {
		$qry = "SELECT * FROM task_category_group where group_id = ".$group_id;
		$result = getRS($con, $qry);
		$row = getRow($result);
		$group_name = " : ".displayText($row['title']);
	} else {
		$group_name = " : All Groups";
	}		
		
	#do multi edit form handling
	if ($multi == "yes") {    
		$multiSaveError = false;
		if(buttonClicked("")) {
		} else if (buttonClicked("saveMulti") && isset($_POST['selectMulti'])) {
            $multiSaveError = saveMultiAccounttasks($con, $clsUS, $licence_end, $accountid, $tasklistid);
			if ($multiSaveError == false) {
				$messages[] = SAVED;
			} else {
				$messages[] = ERROR_SAVED;
			}
			headerLocation ("tasks.php?month=$month&licenceid=$licenceid&year=$year&option=$option&category=$category&multi=yes", false);
			die();
		}	else if (buttonClicked("saveMulti") && !isset($_POST['selectMulti'])) {
			$messages[] = NONE_SELECTED;
		}
	}
	
	if (isset($accountid) && isset($accounttaskid) && isset($licenceid)){
		
		if (isset($accounttaskid)  && $accounttaskid !== null) {
			$content = "taskedit.php";
			$categoryid = $rowAccountTask['categoryid'];
			
			if (buttonClicked("save")) {			
				
				if ($_POST["state"] != 'COMPLETED' && $rowAccountTask['state'] != 'COMPLETED') {
                    
                    $taskfiles = new accounttaskFiles();
                    $taskfiles->newFiles();
                    $taskfiles->rmFilesAdd();

                    $tasklinks = new accounttaskLinks();
                    $tasklinks->newLinks();
                    $tasklinks->rmLinksAdd();
                    
                    if ($accounttaskid == 0) {
                        //insertAccounttask();
                    } else {
                        if (updateAccounttask($accounttask, $tasklistid, $licence_end, $con, $clsUS, $_POST, $taskfiles, $tasklinks)) {
                            //copy to users functionality	
                            if (isset($_POST['copyto'])) {
                                foreach ($_POST['copyto'] as $copyto) {
                                    if (!isset($messages)) $messages = array();
                                    $messages = copyTaskToUser($copyto, $accounttaskid, $con, $clsUS, $messages);
                                }
                            }
                            #return to return page
                            if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages);
                            
                            $title = "Task List";	
                            $content = "tasklist.php";
                            //$box[] = "actions.php";
                            $box[] = "box.key.php";
                            $box[] = "box.taskmonths.php";
                            $box[] = "box.taskoptions.php";
                            $box[] = "box.usersMenu.php";
                            if ($group_id >=0) $box[] = "box.taskcategories.php";
                            $box[] = "box.taskcategories.php";
                            if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";							
                        }                   
                    }
				} else if($_POST["state"] != 'COMPLETED' && $rowAccountTask['state'] == 'COMPLETED') {
					#just update state to new state - uncomplete task
                    if ($accounttask->uncomplete($con, $_POST["state"])) {
						$title = "Task List";	
						$content = "tasklist.php";
						$box[] = "box.key.php";
						$box[] = "box.taskmonths.php";
						$box[] = "box.taskoptions.php";
						$box[] = "box.usersMenu.php";
						$box[] = "box.taskcategories.php";
						if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";
					} else {
						$messages[] = $qry->getError();	
					}
				} else {
					if (isset($return) && $return != "") {
						//copy to users functionality						
                        if (isset($_POST['copyto'])) {
                            foreach ($_POST['copyto'] as $copyto) {
                                if (!isset($messages)) $messages = array();
                                $messages = copyTaskToUser($copyto, $accounttaskid, $con, $clsUS, $messages);
                            }
                        }				
                        #return to return page			
                        headerLocation ("$rootUrl/$return", false);
					}
					$title = "Task List";	
					$content = "tasklist.php";
					$box[] = "box.key.php";
					$box[] = "box.taskmonths.php";
					$box[] = "box.taskoptions.php";
					$box[] = "box.usersMenu.php";
					$box[] = "box.taskcategories.php";
					if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";
				}
			} else if (buttonClicked("delete")) {
                if ($accounttask->delete($con)) {
                    if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages);
                    $title = "Task List";	
                    $content = "tasklist.php";
                    //$box[] = "actions.php";
                    $box[] = "box.key.php";
                    $box[] = "box.taskoptions.php";
                    $box[] = "box.usersMenu.php";
                    $box[] = "box.taskcategories.php";
                    if (!isAllowed("editAccount"))   $box[] = "box.accountLicenceTasksList.php";
                }
			} else 	if (buttonClicked("cancel")) {
				if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages);
				$title = "Task List";	
				$content = "tasklist.php";
				//$box[] = "actions.php";
				$box[] = "box.key.php";
				$box[] = "box.taskoptions.php";
				$box[] = "box.usersMenu.php";
				$box[] = "box.taskcategories.php";
				if (!isAllowed("editAccount")) $box[] = "box.accountLicenceTasksList.php";
			}
		} else {
			$title = "Task List";	
			$content = "tasklist.php";
            $box[] = "box.key.php";
            $box[] = "box.taskmonths.php";
            $box[] = "box.taskoptions.php";
            $box[] = "box.usersMenu.php";
            $box[] = "box.taskcategories.php";
            if (!isAllowed("editAccount")) $box[] = "box.accountLicenceTasksList.php";
		}
	} else {	
		$title = "Task List";	
		$content = "tasklist.php";
		//$box[] = "actions.php";
		$box[] = "box.key.php";
		$box[] = "box.taskmonths.php";
		$box[] = "box.taskoptions.php";
		$box[] = "box.usersMenu.php";
        if ($group_id >=0) $box[] = "box.taskgroups.php";
		$box[] = "box.taskcategories.php";
		if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";
		#missing some of licenceid, accountid, accounttaskid
	}
	
	if (isAllowed("editAccount")) { 
		$this_page = "accounts.php";
	} else {
		$this_page = "tasks.php";
	}
    $listTasks = new listAccountTasks($licenceid, $option, $category, $user, $month, $year, $group_id);
    $listTasks->getList();
    $optionqry = $listTasks->optionqry;
    $option_title = $listTasks->option_title;
    $category_list_task_state = $listTasks->category_list_sql;
    $users_menu_option = $listTasks->users_menu_option;
    $displaydate = $listTasks->displaydate;
    
    # this record set is used in tasklist.php
    $optionresult = getRS($con, $optionqry);
    $monthresult = $listTasks->monthsNavBox($con);
	$optionresult = getRS($con, $optionqry);
    $listTitle = $listTasks->getListTitle($months, $group_name, $category_name, $user_name);
	
	if ($title == "Task List" && isAllowed("editAccount")){
		#get account name for link back to account page
		$qry = "SELECT accountname, account.accountid as aid FROM account inner join accountlicence on account.accountid = accountlicence.accountid where licenceid = ".$licenceid;
		$result = getRS($con, $qry);
		$row = getRow($result);
	
		$title_crumb = ": <a class=\"crumb\"href=\"$rootUrl/handsam_core/accounts.php?accountid=".$row['aid']."\" title=\"Return to ".$row['accountname']." account details\">".$row['accountname']."</a>";
	}

	$title = licence_archive_title($licenceid, $accountid, $con);
	
} else {
	#hacking attempt or wrong permissions so display index
	trigger_error("Access Denied",E_USER_WARNING);
    headerLocation("$rootUrl/index.php",false);
}
#Include layout page
include("layout.php");
?>