<?php
/**
 * account admin account tasks editing
 * must either be an account admin(no licence id needed) or a (super admin with licence id supplied)
 * content: admintasklist.php, adminytaskedit.php
 * boxes: admintaskcategories.php, admintaskoptions.php, admintaskmonths.php
 * 
 */

ini_set('max_execution_time', '0'); // 0 = no limit.
$this_page = "tasklistadmin.php";
$secure = true;
require("../../shared/startPage.php");
$pageFiles->addFunc('handsam_uploads');
$pageFiles->addFunc('history');
$pageFiles->addModFunc('select_multi_tasks');
$pageFiles->addModFunc('tasklist_list_tasks');
$pageFiles->addModFunc('tasklist_task_date');
$pageFiles->addModFunc('tasklist_accounttask_edit');

$accountid = $clsUS->accountid;
#get taskid for edit page
$taskid = getIntFromRequest("taskid");
$return = getStrFromRequest("return");

if ($taskid !== null) $pageFiles->addModFunc('tasklist_task_edit');

$pageFiles->includePhp();


$title = "";
$insert = false;
$insertSuccess = true;
$categoryid = getIntFromRequest("categoryid");
if (isset($_POST['categoryid']) && $_POST['categoryid'] == 0) $_POST['categoryid'] = '';

$RTask = new Repeatingtask($con, $clsUS);

//if edit, get category name for adminTasksInCategory.php box title
if(isset($taskid) && $taskid > 0) {
    // Repeating task object
    
    $Task = new task ($clsUS, $taskid);
    $Task->get($con);
    $rowTask = $Task->row;   
    if ($Task->edited_by ==  0 && $Task->edited_by !== "" && $Task->edited_by !== null) $task_hist = getHistoryItem($con, 'task', 'taskid', $taskid);

    $Taskcat = new taskcategory($Task->categoryid);
    $Taskcat->get($con);
    $categoryname = $Taskcat->getCatName();
    
    // set repeating task id
    $RTask->setId($Task->repeatingtaskid, $con);
 
} else if($taskid == 0 && isset($categoryid)) {
    // Repeating task object
    
    $rowTask = array();
    
    $Taskcat = new taskcategory($categoryid);
    $Taskcat->get($con);
    $categoryname = $Taskcat->getCatName();
}

#check for month, year, options and set to all if none
$tasklistid = getIntFromRequest("tasklistid");
$month = getIntFromRequest("month");
$option = getStrFromRequest("option");
$multi = getStrFromRequest("multi");
$category = getIntFromRequest("category");
$datefield = getIntFromRequest("datefield");

if(!isset($month)) {
    $month = 0;
}

if(!isset($option)) {
    $option = "active";
}
if(!isset($category)) {
    $category = 0;
}

$Tasklist = new tasklist($tasklistid);
$Tasklist->get($con);
$tasklistname = $Tasklist->getTasklistName();

#get category title
if($category > 0) {
    #get options title and option specific sql query
    $Taskcat = new taskcategory($category);
    $Taskcat->get($con);
    $category_name = " : " . $Taskcat->getCatName();
    $categorySQL = "and task.categoryid=" . $category;
} else {
    $category_name = " : All Categories";
    $categorySQL = "";
}

#do multi edit form handling
if($multi == "yes") {
    $multiSaveError = false;

    if(buttonClicked("saveMulti") && isset($_POST['selectMulti'])) {
        $multiSaveError = saveMultiTasks($con, $clsUS);
        if($multiSaveError == false) {
            $messages[] = SAVED;
            headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
        } else {
            $messages[] = ERROR_SAVED;
        }
    } else if(buttonClicked("saveMulti") && !isset($_POST['selectMulti'])) {
        $messages[] = NONE_SELECTED;
    }
}

if(isAllowed("editTask")) {
    if(isset($taskid) && $taskid >= 0) {
        //get task history for this task
        $title = "Edit Task";
        $content = "admintaskedit.php";
        
        if(buttonClicked("save_repeating")) {
            //get resources info
            $taskfiles = new taskFiles();
            $taskfiles->newFilesFromDb($con, $taskid);
            $tasklinks = new taskLinks();
            $tasklinks->newLinksFromDb($con, $taskid);
     
            ////we have now stopped the user from changeing the edited details between weekly, monthly by date and monthly by day, so they can only change the day/date within these instead.
            //get original task dates in an array
            $old_tasks = $RTask->getCurrentTaskDates($con);
             //update the task task_repeating table
            $rtaskid = $RTask->save($con);
            
            //get all active licences and repeatingid's in array for use later
            $activelicences = getActiveLicences($tasklistid, $con);
            //now update accounttask_repeating
            updateAccounttaskRepeating($con, $rtaskid, $clsUS, $_POST, $activelicences);
            
            $RTask->setLoopArr();
            //now get array of new dates
            $new_tasks = $RTask->getTaskDates();
            
            
            //var_dump($old_tasks);
            //var_dump($new_tasks);
            //die();
            
            //check for accounttask_repeating for each licnce
            foreach($activelicences as $al) {
                $al['repeatingtaskid'] = $rtaskid;
                checkForAccRep($al, $con, $clsUS, $al['start_date'], $al['end_date'], $al['edit_flag']);
            }
            
            //go through each new task comparing the exact date
            $update = true;
            foreach($new_tasks as $k=>$new_task) {
                if (in_array($new_task, $old_tasks)) {
                    $tid = array_search ($new_task, $old_tasks);
                    //update state to active and unset. also copy over new calendar criteria to replace the old criteria.
                    $task_update =  new task ($clsUS, $tid);                       
                    //need to process the date info for this            
                    $val_arr = $RTask->getDateVals($new_task);
                    $val_arr["state"] = 'ACTIVE';
                    $val_arr['customtext'] = $Task->row['customtext'];
                    $val_arr['tasktext'] = $Task->row['tasktext'];
                    $val_arr['tasklistid'] = $Task->row['tasklistid'];
                    $val_arr['categoryid'] = $Task->row['categoryid'];
                    $val_arr['key_task'] = $Task->row['key_task'];
                    $val_arr['key_task_lock'] = $Task->row['key_task_lock'];
                    $val_arr['repeatingtaskid'] = $Task->row['repeatingtaskid'];
                    if (isset($_POST['repeat_end_month'])) $val_arr['repeat_end_month'] = $_POST['repeat_end_month'];
                    if (isset($_POST['repeat_start_month'])) $val_arr['repeat_start_month'] = $_POST['repeat_start_month'];
                    if (!updateTaskDates($task_update, $RTask, $tasklistid, $con, $clsUS, $val_arr, $taskfiles, $tasklinks, $activelicences)) {
                        //$update = false;
                    }
                    //die();
                    $new_tasks = array_diff($new_tasks, array($new_task));
                    $old_tasks = array_diff($old_tasks, array($new_task));
                }         
            }
            if ($update) {
                $messages[] = SAVED;
                $messages[] = UPDATED_ACCOUNT_TASKS;
            } else {
                $messages[] = ERROR_SAVED;
            }
            
            //delete $old_tasks
            foreach ($old_tasks as $tid=>$old_task) {
                $task_delete =  new task ($clsUS, $tid);
                if ($task_delete->delete($con)) {
                    $RTask->update_num_repeats($con);
                    deleteAccounttask($con, "DELETED1", $tid, $clsUS, true);
                }
 
            }
            
            //insert new tasks
            $val_arr['customtext'] = $Task->row['customtext'];
            $val_arr['tasktext'] = $Task->row['tasktext'];
            $val_arr['tasklistid'] = $Task->row['tasklistid'];
            $val_arr['categoryid'] = $Task->row['categoryid'];
            $val_arr['key_task'] = $Task->row['key_task'];
            $val_arr['key_task_lock'] = $Task->row['key_task_lock'];
            $val_arr['repeatingtaskid'] = $Task->row['repeatingtaskid'];
            $val_arr['state'] = $Task->row['state'];
                      
            foreach ($new_tasks as $tid=>$new_task) {
                $val_arr = $RTask->setPostVars($val_arr, $new_task);
                insertTask($con, $clsUS, $val_arr, $taskfiles, $tasklinks, $tasklistid);
            }
            if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
            //reload page because we've just updated the list of tasks
            headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
        
        //save task details either repeating or normal 
        } else if(buttonClicked("save")) {

            $taskfiles = new taskFiles();
            $taskfiles->newFiles();
            $taskfiles->rmFilesAdd();
            
            $tasklinks = new taskLinks();
            $tasklinks->newLinks();
            $tasklinks->rmLinksAdd();
            
            $repeating_task_save = true;
           // if(isset($_POST['is_repeatable']) && $taskid == 0) {
            if(isset($_POST['is_repeatable'])) {
                //a new task
                $rtaskid = $RTask->save($con);
                if($rtaskid > 0) {
                    $_POST['repeatingtaskid'] = $rtaskid;    
                } else {
                    $repeating_task_save = false;
                }
            } 
            // save the repeating task, pass the id to Task if so
            if ($repeating_task_save) {
                if($taskid == 0) {
                    if (isset($rtaskid) && $rtaskid > 0) {
                        //insert multiple tasks
                        $RTask->setLoopArr();
                        if (count($RTask->loop_arr) > 0) {
                            $save_ok = true;
                            foreach($RTask->loop_arr as $val) {
                                $post_vars = $RTask->setPostVars($_POST, $val);
                                $save_ok = insertTask($con, $clsUS, $post_vars, $taskfiles, $tasklinks, $tasklistid);
                            }
                            if ($save_ok) {
                                $messages[] = SAVED;
                                $messages[] = SAVED_REPEATING_TASKS;
                                $messages[] = UPDATED_ACCOUNT_TASKS;
                                if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                                headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
                                
                            }
                        }
                        
                    } else {
                        //insert one task
                        if (insertTask($con, $clsUS, $_POST, $taskfiles, $tasklinks, $tasklistid)) {
                            $messages[] = SAVED;
                            $messages[] = UPDATED_ACCOUNT_TASKS;
                            if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                            headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
                            
                        }
                    }
                } else {
                    //for an existing repeating task
                    if ($Task->repeatingtaskid > 0 && isset($_POST['edit_type']) && ($_POST['edit_type'] == 0 || $_POST['edit_type'] == 2)) {
                        //update multiple tasks within this repeting taskid
                        $min_day = 0;
                        if ($_POST['edit_type'] == 2) $min_day = $Task->day_of_year;
                        
                        $save_ok = true;
                        $taskfiles->setRmFileids($con);
                        $tasklinks->setRmLinkids($con);
                        foreach ($RTask->all_days as $this_id=>$day) {
                            if ($day >= $min_day) {
                                $thisTask = new task ($clsUS, $this_id);
                                $thisTask->get($con);
                                $save_ok = updateTask($thisTask, $RTask, $tasklistid, $con, $clsUS, $_POST, $taskfiles, $tasklinks);
                            }
                        }
                        
                        if ($save_ok) {
                           if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                           headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
                           
                        }                        
                    } else if (isset($rtaskid) && $rtaskid > 0) {
                        $taskfiles->newFilesFromDb($con, $Task->taskid);
                        $tasklinks->newLinksFromDb($con, $Task->taskid);
                        $taskfiles->setRmFileids($con);
                        $tasklinks->setRmLinkids($con);
                        //var_dump($taskfiles);
                        //die();
                        //we are creating a repeating task from an existing normal task
                        $RTask->setLoopArr();
                        if (count($RTask->loop_arr) > 0) {
                            $save_ok = true;
                            //reverse $RTask->loop_arr array, so we deal with the first item last
                            $rep_arr = array_reverse($RTask->loop_arr);
                            $task_updated = false;
                            //but we need to remove the repetition nearest the current task dates, so the current task remains
                            if (count($RTask->weekly) > 0) {
                                $task_date_val = date("W",strtotime(date('Y')."-".$Task->row['taskmonth']."-".$Task->row['taskday']));
                            } else {
                                $task_date_val = $Task->row['taskmonth'];
                            }
                            foreach($rep_arr as $key => $val) {
                                $post_vars = $RTask->setPostVars($_POST, $val);
                                if ($task_date_val == $val || (count($rep_arr) == 1 && $task_updated == false)) { 
                                    // //or is the last element and there hasn't been an update yet
                                    //ammend the current task with the correct date criteria and repeatingtaskid
                                    $save_ok = updateTask($Task, $RTask, $tasklistid, $con, $clsUS, $post_vars, $taskfiles, $tasklinks, false);
                                    //die();
                                    $task_updated = true;
                                } else {
                                    $save_ok = insertTask($con, $clsUS, $post_vars, $taskfiles, $tasklinks, $tasklistid);
                                }
                                unset($rep_arr[$key]);
                                //if current task not updated, then it should be deleted as its outside the date range
                                
                            }
                            if ($save_ok) {
                                $messages[] = SAVED;
                                $messages[] = SAVED_REPEATING_TASKS;
                                $messages[] = UPDATED_ACCOUNT_TASKS;
                                if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                                headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
                                
                            }
                        }
                    } else {
                    // update a single task
                        if (updateTask($Task, $RTask, $tasklistid, $con, $clsUS, $_POST, $taskfiles, $tasklinks)) {
                            if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                            headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
                            
                        }
                    }
                }
            }
            //headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
        } else if(buttonClicked("delete")) {
            if ($Task->delete($con)) {
                $RTask->update_num_repeats($con);
                if ($RTask->num_repeats == 0) $RTask->delete($con);
                #now change state to DELETED1 in accounttask table for all state != COMPLETED tasks
                if(deleteAccounttask($con, "DELETED1", $Task->taskid, $clsUS)) {
                    $messages[] = UPDATED_ACCOUNT_TASKS;
                    
                    if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                    headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
                }
            }
            headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
        } else if(buttonClicked("delete_all_rpt") || buttonClicked("delete_future_rpt")) { 

            //delete multiple tasks
            if ($Task->repeatingtaskid > 0) {
                //update multiple tasks within this repeting taskid
                $min_taskid = 0;
                if (buttonClicked("delete_future_rpt")) $min_taskid = $Task->taskid;
                if ($RTask->deleteAllTasks($con, $min_taskid)) {
                    $RTask->update_num_repeats($con);
                    if ($RTask->num_repeats == 0) $RTask->delete($con);
                    //delete accounttasks
                    
                    if($RTask->deleteAllAccountTasks($con, $min_taskid, 'DELETED1')) {
                        if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                        headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
                        
                    }
                   
                }                     
            }            
            headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
        } else if(buttonClicked("cancel")) {
            if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
            headerLocation ("$rootUrl/modules/tasklist/taskadmin.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);
        }
    } else {
        if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
        $title = $tasklistname . "  Task List";
        $content = "admintasklist.php";
        $box[] = "box.taskmonths.php";
        $box[] = "box.admintaskoptions.php";
        $box[] = "box.admintaskcategories.php";
    }
} else {
    //trigger_error("Access Denied",E_USER_WARNING);
    headerLocation("$rootUrl/index.php", false);
}

$listTasks = new listTasks($tasklistid, $option, $category);
$listTasks->getCategorySql();
$listTasks->getList();
$optionqry = $listTasks->optionqry;
$option_title = $listTasks->option_title;
$category_list_task_state = $listTasks->category_list_sql;

# this record set is used in tasklist.php
$optionresult = getRS($con, $optionqry);

if($month == 0)
{
    $listTitle = $option_title . " : All Months" . $category_name;
}
else
{
    $listTitle = $option_title . " : " . $months[$month] . " " . $category_name;
}

#Include layout page
include("layout.php");
