function attachDoc(docid, docname, formDiv, item) {
	var cnt = 0;
	var liId = 'justAdded' + docid;
	while (window.opener.document.getElementById(liId)) {
		cnt++;
		liId += "." + cnt;
	}
		
	docDiv = window.opener.document.getElementById(formDiv);
	window.close();
	docDiv.innerHTML += "<li id=\"" + liId + "\">\n" + docname + "<input type='hidden' name='" + item +"AddId[]' value='" + docid + "'/><input type='hidden' name='" + item +"AddFilename[]' value='" + docname + "'/><img onclick=\"unattach('" + liId + "');\" src=\"images/delete.png\" alt=\"unattach document or file\"/></li>\n";
	alert("Please remember, once you have finished attaching any documents you must save the task!"); 
}

function unattach(id) {
	docLi = document.getElementById(id);
	docLi.style.display = 'none';
	docLi.innerHTML = '';
}	

function deleteFromDB(id, taskfileid, item) {
	docLi = document.getElementById(id);
	docLi.style.display = 'none';
	docLi.innerHTML = "<input type='hidden' name='" + item + "DelId[]' value='" + taskfileid + "'/>";
}

function openLink() {
	docURL = document.getElementById('url').value;
	window.open(docURL,"handsamLink","toolbar=yes,status=yes,scrollbars=yes, resize=yes, menubar=yes");
}

var xmlhttp;

function attachItemToPage(rootUrl, itemid, item_type, contentid) {
	xmlhttp=null;
	//alert(rootUrl + " : " + itemid + " : " +  item_type + " : " + contentid);
	url = rootUrl + "/modules/tasklist/attachItem.php"; // The server-side script
	// code for Mozilla, etc.
	if (window.XMLHttpRequest){
	  xmlhttp=new XMLHttpRequest()
	}
	// code for IE
	else if (window.ActiveXObject){
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")
	}
	if (xmlhttp!=null){
	  	xmlhttp.onreadystatechange=state_Change
	  	xmlhttp.open("GET", url + "?itemid=" + itemid + "&type=" + item_type + "&contentid=" + contentid, true);
	  	xmlhttp.send(null);
	}
	else {
	  alert("Your browser does not support XMLHTTP.")
	}	
	return true;
}
	
function removeItem(rootUrl, itemid, item_type) {
	xmlhttp=null;
	//alert(rootUrl + " : " + itemid + " : " +  item_type + " : " + contentid);
	url = rootUrl + "/modules/tasklist/removeItem.php"; // The server-side script
	// code for Mozilla, etc.
	if (window.XMLHttpRequest){
	  xmlhttp=new XMLHttpRequest()
	}
	// code for IE
	else if (window.ActiveXObject){
	  	xmlhttp=new ActiveXObject("Microsoft.XMLHTTP")
	}
	if (xmlhttp!=null){
	  	xmlhttp.onreadystatechange=state_Change
	  	xmlhttp.open("GET", url + "?itemid=" + itemid + "&type=" + item_type, true);
	  	xmlhttp.send(null);
	}
	else {
	  alert("Your browser does not support XMLHTTP.")
	}	
	return true;
}


function state_Change() { 
	if (xmlhttp.readyState==4){
		if (xmlhttp.status==200){
			returnText = xmlhttp.responseText;
			if (returnText !== "") {
				//alert(returnText);
				if (window.opener) {
					window.opener.location.reload(true);
				} else {
					window.location.reload(true);
				}
			}
		} else {
	  		alert("Problem retrieving XML data:" + xmlhttp.statusText);
	  	}
	} else {
	}
	
}  	

