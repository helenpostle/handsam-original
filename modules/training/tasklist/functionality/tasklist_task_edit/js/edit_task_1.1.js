/* 
 * Edit task JS
 * 
 * MG, 04-04-2013
 */

// on load
$(function(){
    
    if ($('#repeat_task').length) {
    
        // enable/disable key task date lockdown
        toggle_ktl();
        $('input#key_task').change(toggle_ktl);

        // checkbox to show/hide the repeat task UI
        toggleRepeatTask($('#is_repeatable'));    
        $('#is_repeatable').change(function(){
            toggleRepeatTask($(this));
        });

        // checkbox to enable/disable the date range
        toggleDateRange($('#repeat_duration'));

        $('#repeat_duration').change(function(){
            toggleDateRange($(this));
        });

        // UI tabs
        $("#multitabs").tabs({
            select : enableRepeatTabs,
            show : disableInputs
        });

        // disable all hidden tab input/select
        $('select, input', '.ui-tabs-hide').attr('disabled', 'disabled');

        //need to focus on a specific repeating task tab depending on what data has loaded.
        if ($('#weekly option:selected').val()) $( "#multitabs" ).tabs('select', 0);
        if ($('#month_date option:selected').val()) $( "#multitabs" ).tabs('select', 1);
        if ($('#month_day option:selected').val()) $( "#multitabs" ).tabs('select', 2);
    }
    
});


// toggles the display of the key task lockdown checkbox,
// based on the key task checkbox value
function toggle_ktl()
{
    if($('input#key_task').is(':checked'))
    {
        // show div, set checkbox to enabled
        $('#key_task_lock').show();
        $('#key_task_lock input').removeAttr('disabled');
    }
    else
    {
        // hide, disable, and uncheck
        $('#key_task_lock').hide();
        $('#key_task_lock input').attr('disabled', 'disabled').attr('checked', false);
    }
}

/**
 * Enable the current select/inputs
 */
function enableRepeatTabs(event, ui)
{
    if ($("#edit_repeat").val() == "enabled") {
        window.currentTab = ui.index;
        $('select, input', '#'+ui.panel.id).attr('disabled', false);

        toggleDateRangeDate();
    }
}

var currentTab;
/**
 * Disables all hidden tabbed select/input elements. 
 * A bit hacky, as it's called after any anims are finished.
 * It *could* be called in enableRepeatTabs(), 
 * but the event is fired before the 
 * .ui-tabs-hide class is added to the element so the select/input are not disabled
 * 
 * MG 2013-05-09
 */
function disableInputs(event, ui)
{
    $('select, input', '.ui-tabs-hide').attr('disabled', 'disabled');
}

/**
 * Show/hide the repeat task div
 */
function toggleRepeatTask(el)
{
    if(el.is(':checked'))
    {
        $('#repeat_task').show('fast');
        // disable main day/month selects
        $('#taskmonth, #taskday').attr('disabled', 'disabled').parent().hide();
    }
    else
    {
        $('#repeat_task').hide('fast');
        $('#taskmonth, #taskday').attr('disabled', false).parent().show();
    }
}

/**
 * Show/hide the date range selector divs
 */
function toggleDateRange(el)
{
    if ($("#edit_repeat").val() == "enabled") {
        if(el.is(':checked')) // disable 
        {
            $('#repeat_date').addClass('disabled');
            $('select', '#repeat_date').attr('disabled', 'disabled');
            $('#repeat_start_month').val('1');
            $('#repeat_end_month').val('12');
            $('#repeat_end_day').val('31');
            $('#repeat_start_day').val('1');
        }   
        else // enable 
        {
            $('#repeat_date').removeClass('disabled');
            $('select', '#repeat_date').attr('disabled', false);
            
        }
        toggleDateRangeDate();
    }
}

// called on each tab change
function toggleDateRangeDate()
{
    // don't bother if the box is still checked
    if($('#repeat_duration').is(':checked')) return;
    if((window.currentTab > 0 && $('#multitab-2').length > 0 && $('#multitab-3').length > 0) || (!window.currentTab && $('#multitab-1').length == 0))
    {
        $('select#repeat_start_day', '#repeat_date').attr('disabled', 'disabled');    
        $('select#repeat_end_day', '#repeat_date').attr('disabled', 'disabled');    
    }   
    else
    {
        $('select#repeat_start_day', '#repeat_date').attr('disabled', false);
        $('select#repeat_end_day', '#repeat_date').attr('disabled', false);
    }
}