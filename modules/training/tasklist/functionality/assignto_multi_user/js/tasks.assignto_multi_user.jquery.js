$(document).ready( function() {
	var cnt = 1;
	var prev_cnt = 0;
	var no_more_users = "There are no more additional users to copy";
	$('.addUser').click(function(event){
		
		//check that there are more users to copy to
		//alert($('#copyto_' + prev_cnt + ' option').length);
		if ((cnt > 1 && $('#copyto_' + prev_cnt + ' option:selected').val() > 0) || (cnt == 1 && $('#assignedto option:selected').val() > 0)) {
			if ($('#assignedto option').length > 3) {
				
				if ($('#copyto_' + prev_cnt + ' option').length == 0 || $('#copyto_' + prev_cnt + ' option').length > 3) {
					
					$('#extra_users').append("<div class='frm_row optional clearfix'><label>Assign this task to another user: </label><select id='copyto_" + cnt + "' name='copyto[]'></select></div>");
					
					if (cnt > 1) {
						$('#copyto_' + prev_cnt + ' option').clone().appendTo('#copyto_' + cnt);
						var rm = ($('#copyto_' + prev_cnt + ' option:selected').val());
						if (rm > 0) $("#copyto_" + cnt + " option[value='" + rm + "']").remove();
					} else {
						$('#assignedto option').clone().appendTo('#copyto_' + cnt);
						var rm = ($('#assignedto option:selected').val());
						if (rm > 0)	$("#copyto_" + cnt + " option[value='" + rm + "']").remove();
			
					}
					
					cnt ++;
					prev_cnt ++;
					
					//alert(el.html());
					//var el_h = el.html();
					//alert(el_h);
					//var ql = $(this).after("<div class='optional'>" + el_h + "</div>");
					//alert(ql.html());
					
				} else {
					alert(no_more_users);
				}
			} else {
				alert(no_more_users);
			}
		} else {
			alert ("Please assign the previous user first");
		}
		return false;
	});

});