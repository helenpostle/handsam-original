<?php
//Handsam task list menu items
if (isAllowed("editTasklist")) { 
	$task_menu = $clsMenu->addMenuItem("/modules/tasklist/tasklistadmin.php", "TASK ADMIN", "", "");
	$clsMenu->addMenuItem("/modules/tasklist/tasklistadmin.php", "TASK ADMIN", "", $task_menu);
	if(isAllowed("viewOurTasks")) $clsMenu->addMenuItem("/modules/tasklist/tasks.php", "MANAGE TASKS", "", $task_menu);
	if(isAllowed("myTasks")) $clsMenu->addMenuItem("/modules/tasklist/mytasks.php", "MY TASKS", "", $task_menu);
}
if (!isAllowed("editTask") && isAllowed("viewOurTasks")) { 
	$clsMenu->addMenuItem("/modules/tasklist/tasks.php", "MANAGE TASKS", "", "");
}
if (!isAllowed("editTask") && isAllowed("myTasks")) { 
	$clsMenu->addMenuItem("/modules/tasklist/mytasks.php", "MY TASKS", "", "");
}
?>