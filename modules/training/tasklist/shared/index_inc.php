<?php
//if (!isAllowed('editTask') && isAllowed('myTasks') && !isAllowed('editAccount')) {
if (isAllowed('myTasks')) {
	$noTasks = true;
	//this is a specific function if the user is logged in and is allowed to use the tasklist module. We need to check for this and then include this automatically in the future	
	include("$rootPath/modules/tasklist/library/funcLicenceArchiveTitle.php");
	$title = "";		//the <title> tag and <h1> page title
	$userid = $clsUS->userid;
	$accountid = $clsUS->accountid;
	//get licenceid for this page

	$licenceid = setQSInt("licenceid");
	$title .= licence_archive_title($licenceid, $accountid, $con);
	if ($licenceid == 0 || !isLicence($licenceid)) {
		//get current active licenceid
		$licence_qry = "select licenceid from accountlicence where start_date < NOW() and end_date > NOW() AND state = 'ACTIVE' and accountid = $accountid";
		$licence_qry_users = "select accountlicence.licenceid from accountlicence left join usr_usrtype on accountlicence.tasklistid = usr_usrtype.tasklistid where usr_usrtype.userid = ".$clsUS->userid." and usr_usrtype.usertype = 'Administrator' and start_date < NOW() and end_date > NOW() and accountid = $accountid";

		$licence_result = getRS($con, $licence_qry);
		$licence_row = getRow($licence_result);
		$licenceid = $licence_row['licenceid'];
	} else {
		$licence_qry = $licenceid;
		$licence_qry_users = "select accountlicence.licenceid from accountlicence left join usr_usrtype on accountlicence.tasklistid = usr_usrtype.tasklistid where usr_usrtype.userid = ".$clsUS->userid." and usr_usrtype.usertype = 'Administrator' and start_date < NOW() and end_date > NOW() and accountid = $accountid and accountlicence.licenceid = $licenceid";
		//$title .= "All Tasklists : ";
	}
	
	if (isAllowed("viewOurlicences") && !isAllowed("editAccount")) {
		$accountid = $clsUS->accountid;
		$pageFiles->addBox("$rootPath/modules/tasklist/boxes/accountLicenceArchiveList.php");
		$pageFiles->addBox("$rootPath/modules/tasklist/boxes/homeLicenceList.php");
	}

	if (isAllowed("editOurTasks")) {
		//$box[]="editedTaskAlert.php";
		if ($tasklist_conf['home_page_edited_task_alert']) $pageFiles->addBox("$rootPath/modules/tasklist/boxes/box.editedTaskAlert.php");
		//$box[]="userEditTaskAlert.php";
		$pageFiles->addBox("$rootPath/modules/tasklist/boxes/box.userEditTaskAlert.php");
	}
     
    
    if($core_conf['key_task'])
    {
        // show key tasks
        $tasksqry = "SELECT accounttask.*, taskcategory.customtext as categoryname 
            FROM taskcategory 
            LEFT JOIN accounttask ON taskcategory.categoryid = accounttask.categoryid 
            WHERE (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') 
                AND accounttask.accountid='$accountid'
                AND accounttask.assignedto = '$userid'
                AND accounttask.key_task = 1
                AND accounttask.licenceid IN ($licence_qry) 
            ORDER BY accounttask.actiondate;";

        $tasksresult = getRS($con, $tasksqry);
        if (getResultRows($tasksresult) > 0) 
        {
            $noTasks = false;
            $display_my_list = true;
            $caption = "Your Key Tasks";
            $alert_text = "";
            ob_start();
            include("$rootPath/modules/tasklist/content/keyTaskAlert.php");
            $index_modules_content[] = ob_get_contents();
            ob_end_clean();
        }
        
        /**
         * The above key task block 
         * will show tasks that may also 
         * appear in the below Urgent Tasks block
         * 
         * Below query will need 
         *  " AND key_task = 0 "
         * added to it
         * 
         * BUT need to check if the install is conifgured for key tasks first.
         */
    }
		
	#check for late tasks and this weeks tasks
	
	$tasksqry = "SELECT accounttask.*, CASE WHEN taskcategory.customtext != '' THEN taskcategory.customtext ELSE taskcategory.categoryname END as categoryname FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid where (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') and accounttask.accountid='$accountid'   and accounttask.assignedto = '$userid'  and accounttask.actiondate < DATE_ADD(Now(), INTERVAL 7 DAY) and accounttask.licenceid in ($licence_qry) order by accounttask.actiondate" ;
	$tasksresult = getRS($con, $tasksqry);
	if (getResultRows($tasksresult) > 0) {
		$noTasks = false;
		$display_my_list = true;
		$caption = "Your Urgent Tasks";
		$alert_text = "";
		ob_start();
		include("$rootPath/modules/tasklist/content/taskAlert.php");
		$index_modules_content[] = ob_get_contents();
		ob_end_clean();
		//late task list goes here
	} else {
		$alert_text = "<p><strong>You have no urgent tasks</strong></p>";
		$tasksqry = "SELECT accounttask.*, CASE WHEN taskcategory.customtext != '' THEN taskcategory.customtext ELSE taskcategory.categoryname END as categoryname FROM taskcategory left join accounttask on taskcategory.categoryid = accounttask.categoryid  where (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED') and accounttask.accountid='$accountid'  and accounttask.assignedto = '$userid' and accounttask.licenceid in ($licence_qry) order by accounttask.actiondate LIMIT 0,5" ;
	
		$tasksresult = getRS($con, $tasksqry);
		if (getResultRows($tasksresult) > 0) {
			$noTasks = false;
			$display_my_list = true;
			$caption = "Next 5 Tasks";
			ob_start();
			include("$rootPath/modules/tasklist/content/taskAlert.php");
			$index_modules_content[] = ob_get_contents();
			ob_end_clean();
		} else {
			$noTasks = false;
			$display_my_list = false;
			$caption = "Next 5 Tasks";
			ob_start();
			include("$rootPath/modules/tasklist/content/taskAlert.php");
			$index_modules_content[] = ob_get_contents();
			ob_end_clean();
		}
	} 
	if ($noTasks) {
		ob_start();
		include("$rootPath/modules/tasklist/content/noTasks.php");
		$index_modules_content[] = ob_get_contents();
		ob_end_clean();
	}
}


if (isAllowed('editAccount')) {
    // list all synced tasks here. ID by edited_by = 0
    // also compare with last entry for each task in taskhistory so we only list tasks which have changes to categoryid, tasktext, taskday, taskmonth.
    $sql = "select t.state, t.tasklistid, t.edited, t.taskid, t.edited, t.tasktext, t.taskday, t.taskmonth, t.categoryid from task t where t.edited_by = 0";
    $rs = getRS($con, $sql);
    $sync_list = array();
    while ($row = getRow($rs)) {
        //now get the last entry in task_history for this task
        $task_hist = getHistoryItem($con, 'task', 'taskid', $row['taskid']);
        
        //compare
        if ($row['tasktext'] != $task_hist['tasktext'] || $row['taskday'] != $task_hist['taskday'] || $row['taskmonth'] != $task_hist['taskmonth'] || $row['categoryid'] != $task_hist['categoryid'] || $row['state'] != $task_hist['state']) {
            
            $sync_list[] = array('tasklistid' => $row['tasklistid'], 'edited' => $row['edited'], 'taskid' => $row['taskid'], 'tasktext' => $row['tasktext'], 'categoryid' => $row['categoryid'], 'taskday' => $row['taskmonth'], 'taskmonth' => $row['taskmonth'], 'tasktext_h' => $task_hist['tasktext'], 'taskmonth_h' => $task_hist['taskmonth'], 'taskday_h' => $task_hist['taskday'], 'categoryid_h' => $task_hist['categoryid'], );
            
        }
    }
    ob_start();
    include("$rootPath/modules/tasklist/content/syncTasks.php");
    $index_modules_content[] = ob_get_contents();
    ob_end_clean();

}