<?php
//tasklist filter 
if($appid == 1) {
	$qry = "select tasklist.tasklistname, tasklist.tasklistid from tasklist where state = 'ACTIVE'";
	$rsTaskList = getRS($con, $qry);

	if (getResultRows($rsTaskList) > 0) {
		echo "<div class=\"tasklist_filter\">";
		echo "<label for=\"tasklist_filter\"> Task list filter: </label>";
		echo "<select id=\"tasklist_filter\">";
		echo "<option value=\"0\"";
		if ($appid == 0) echo " selected ";
		echo ">All Tasklists</option>";
		while ($rowList = getRow($rsTaskList)) {
			echo "<option value=\"".$rowList['tasklistid']."\"";
			if ($tasklist == $rowList['tasklistid']) echo " selected ";
			echo ">".$rowList['tasklistname']."</option>";
		}
		echo "</select>";
		echo "</div>";
	}
}
?>