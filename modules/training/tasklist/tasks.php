<?php
/**
* account admin account tasks editing
* must either be an account admin(no licence id needed) or a (super admin with licence id supplied)
* content: tasklist.php, taskedit.php
* boxes: taskcategories.php, taskoptions.php
*/
/**
 * Include basic stuff
 */

$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc('tasklist_licence_archive');
$pageFiles->addModFunc('tasklist_accounttask_edit');
$pageFiles->addModFunc('tasklist_copy_task');
$pageFiles->addModFunc('assignto_multi_user');
$pageFiles->addModFunc('select_multi_tasks');
$pageFiles->addFunc('handsam_uploads');
$pageFiles->addCoreLibPhp('funcError/funcLog.php');
$pageFiles->addCoreLibPhp('funcMail/funcMail.php');
$pageFiles->addModFunc('tasklist_task_date');
if ($tasklist_conf['collapse_managetask_resources'] === true) $pageFiles->addModFunc('tasklist_resources');

$pageFiles->addModFunc('tasklist_list_accounttasks');
//new copy task to user function
$pageFiles->addModFunc('tasklist_task_edit');
$pageFiles->includePhp();

$accounttaskid = getIntFromRequest("accounttaskid");

$hasLicence=true;
$hacking = false;
$customTask = false;
$newCustomTask = false;
$return = getStrFromRequest("return");

if(buttonClicked('cancelSearch'))
{
  $_POST['task_search'] = '';
}

$task_search  = getStrFromRequest("task_search");
$task_search_url = '';
if(trim($task_search) != '' )
{
  $task_search_url = urlencode($task_search);
}
$title = "";

$category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') ";//check if this is needed here

//need to get tasklistid, licenceid, accountid, categoryname, customtask, newcustomtask, $rowAccountTask
if ($accounttaskid > 0) {
    $accounttask = new accounttask($clsUS, $accounttaskid);
    $accounttask->get($con);
    $accountid = $accounttask->accountid;
    $rowAccountTask = $accounttask->row;
    $licenceid = $accounttask->licenceid;
    $accountlicence = new Licence($clsUS, $licenceid);
    $accountlicence->get($con);
    $tasklistid = $accountlicence->tasklistid;
    $taskcat = new taskcategory($accounttask->categoryid);
    $taskcat->get($con);
    $categoryname = $taskcat->getCatName();
    $customTask = $accounttask->customtask();
    $title = "Edit Task";
    
    //now get repeating task details if this has repeatingtaskid > 0
    if ($accounttask->accounttask_repeatingid > 0) {
        $RTask = new RepeatingAccounttask($con, $clsUS, $accounttask->accounttask_repeatingid, $accountlicence->start_date, $accountlicence->end_date, $accountid, 0, $licenceid);
        
    }
} else {
    if ($accounttaskid === 0) {
        $accounttask = new accounttask($clsUS, $accounttaskid);
        $accounttask->get($con);
        $rowAccountTask = $accounttask->row;
        $newCustomTask = true;
        $customTask = true;
    }
    $categoryname = "";
    //get tasklistid from accountlicence
	$licenceid = getIntFromRequest("licenceid");
	if ($licenceid > 0) {
        $accountlicence = new Licence($clsUS, $licenceid);
        $accountlicence->get($con);
        $tasklistid = $accountlicence->tasklistid;
        $accountid = $accountlicence->accountid;
	} else {
        $tl_row = getLicenceId($clsUS->userid, $con);
        if ($tl_row)
        {
          $tasklistid = $tl_row['tasklistid'];
          $licenceid = $tl_row['licenceid'];
          $accountid = $tl_row['accountid'];
          $accountlicence = new Licence($clsUS, $licenceid);
          $accountlicence->get($con);
        }
        else
        {
          $hasLicence=false;
        }
        
	}
    
}

if ($hasLicence)
{
  if (!isset($RTask)) $RTask = new RepeatingAccounttask($con, $clsUS, null, $accountlicence->start_date, $accountlicence->end_date, $accountid, 0, $licenceid);


  if (!isAllowed("editAccount") && isAllowed("editOurTasks",1 , $tasklistid)) {
      #is an account admin
    $accountid = $clsUS->accountid;
    if (isset($accounttaskid) && $accounttaskid > 0) {
      if ($accounttask->accountid != $accountid) {
        $hacking = true;
      }
    } else if ($accounttaskid === 0) {
      if (!isLicence($licenceid)) {
        $hacking = true;
      }
    } else if(!isLicence($licenceid)) {
        $hacking = true;
    }
  } else if (!isAllowed("editAccount")) {
    $hacking = true;
  }

  if (!$hacking) {
    //get current licence end date for use in calendar  to validate task date less than this
    $licence_end = $accountlicence->end_date;
    $licence_start = $accountlicence->start_date;

    #check if this is the current licence. if its an old licence don't diaplay actions box
    if (strtotime($licence_end) > time()) {
      $box[] = "box.actions.php";
      $current_licence = true;
    } else {
      $actions_box = "";
      $current_licence = false;
    }

    #check for month, year, options and set to all if none
    $month = getIntFromRequest("month", 0);
    $year = getIntFromRequest("year", 0);
    $user = getIntFromRequest("user", 0);
    $option = getStrFromRequest("option", "active");

    $multi = getStrFromRequest("multi");
    $category = getIntFromRequest("category", 0);
    $group_id = getIntFromRequest("group_id", 0);
    $datefield = getIntFromRequest("datefield");

    #get username:
    if ($user > 0) {
      $qry = "SELECT username FROM usr where userid = ".$user;
      $result = getRS($con, $qry);
      $row = getRow($result);
      $user_name = " : ".textSummary($row['username'],22);
    } else {
      $user_name = " : All Users";
    }

    #get category title
    if ($category > 0) {
          $Taskcat = new taskcategory($category);
          $Taskcat->get($con);
      $category_name = " : ".displayText($Taskcat->getCatName());
    } else {
      $category_name = " : All Categories";
    }		

    #get group title
    if ($group_id > 0) {
      $qry = "SELECT * FROM task_category_group where group_id = ".$group_id;
      $result = getRS($con, $qry);
      $row = getRow($result);
      $group_name = " : ".displayText($row['title']);
    } else {
      $group_name = " : All Groups";
    }		

    #do multi edit form handling
    if ($multi == "yes") 
          {    
              $multiSaveError = false;
              if(buttonClicked(""))
              {
                  // ve believe in nothzing, Lebowski...
              } 
              else if (buttonClicked("saveMulti") && isset($_POST['selectMulti'])) 
              {
                  $multiSaveError = saveMultiAccounttasks($con, $clsUS, $licence_end, $accountid, $tasklistid);
                  if ($multiSaveError == false) 
                  {
                      $messages[] = SAVED;
                  }
                  else 
                  {
                      $messages[] = ERROR_SAVED;
                  }
                  headerLocation ("tasks.php?month=$month&licenceid=$licenceid&year=$year&option=$option&category=$category&multi=yes", $messages);
                  die();
              }
              else if (buttonClicked("saveMulti") && !isset($_POST['selectMulti'])) 
              {
                  $messages[] = NONE_SELECTED;
              }
    }

    if (isset($accountid) && isset($accounttaskid) && isset($licenceid)){
      $multi_message = array();
      if (isset($accounttaskid)  && $accounttaskid !== null) {

        $content = "taskedit.php";
        $categoryid = $rowAccountTask['categoryid'];
        if(buttonClicked("save_repeating")) {

                  //get resources info    
                  $taskfiles = new accounttaskFiles();
                  $taskfiles->newFilesFromDb($con, $accounttaskid);
                  $tasklinks = new accounttaskLinks();
                  $tasklinks->newLinksFromDb($con, $accounttaskid);

                  ////we have now stopped the user from changeing the edited details between weekly, monthly by date and monthly by day, so they can only change the day/date within these instead.
                  //get original task dates in an array
                  $old_tasks = $RTask->getCurrentTaskDates($con); //returns $row[accounttaskid]= actiondate; - need to convert to other format for in_array comparison below.

                  //var_dump($old_tasks);
                  //die();
                   //update the task task_repeating table - edit_flag=true
                  $rtaskid = $RTask->save($con, true);

                  $RTask->setLoopArr();

                  //now get array of new dates
                  $new_tasks = $RTask->getTaskDates();

                  //go through each new task comparing the exact date
                  $update = true;

                  $ts = 0;

                  if ($_POST['edit_type'] == 1) $ts = strtotime($accounttask->actiondate);

                  foreach($new_tasks as $k=>$new_task) {
                      $post_vars = $RTask->setPostVars($_POST, $new_task);
                      $new_task_actiondate = $RTask->getActionDates($post_vars, $licence_start, $licence_end);

                      if (in_array($new_task, $old_tasks)) {

                          $tid = array_search ($new_task, $old_tasks);

                          //update state to active and unset. also copy over new calendar criteria to replace the old criteria.
                          $task_update =  new accounttask ($clsUS, $tid); 
                          $task_update->get($con);

                          if (strtotime($task_update->actiondate) >= $ts) {
                              //need to process the date info for this    
                              $val_arr = $RTask->getDateVals($new_task);
                              $val_arr["state"] = $task_update->row['state'];
                              $val_arr['customtext'] = $task_update->row['customtext'];
                              $val_arr['tasktext'] = $task_update->row['tasktext'];
                              $val_arr['categoryid'] = $task_update->row['categoryid'];
                              $val_arr['key_task'] = $task_update->row['key_task'];
                              $val_arr['key_task_lock'] = $task_update->row['key_task_lock'];
                              $val_arr['accounttask_repeatingid'] = $task_update->row['accounttask_repeatingid'];
                              $val_arr['licenceid'] = $task_update->row['licenceid'];
                              $val_arr['notes'] = $task_update->row['notes'];
                              $val_arr['accountid'] = $task_update->row['accountid'];
                              $val_arr['taskid'] = $task_update->row['taskid'];
                              $val_arr['assignedto'] = $task_update->row['assignedto'];
                              $val_arr['budget'] = $task_update->row['budget'];
                              $val_arr['task_email_user'] = $task_update->row['task_email_user'];
                              $val_arr['compliance'] = $task_update->row['compliance'];
                              $val_arr['actiondate'] = $new_task_actiondate;
                              if (!$task_update->update($con, $val_arr, $licence_end, $tasklistid)) {
                                  $update = false;
                              } else {
                                  $accounttask->get($con);
                                  $multi_message = emailAssignedUsers($accounttask->row, $val_arr, $accounttask->accounttaskid, $multi_message); 
                              }
                          }

                          $new_tasks = array_diff($new_tasks, array($new_task));
                          $old_tasks = array_diff($old_tasks, array($new_task));
                      }         
                  }

                  if ($update) {
                      $messages[] = SAVED;
                      $messages[] = UPDATED_ACCOUNT_TASKS;
                  } else {
                      $messages[] = ERROR_SAVED;
                  }

                      foreach ($old_tasks as $tid=>$old_task) {
                          $task_delete =  new accounttask ($clsUS, $tid);
                          $task_delete->get($con);
                          if ($task_delete->delete($con)) {
                              $RTask->update_num_repeats($con);
                          }

                      }


                  //insert new tasks
                      foreach ($new_tasks as $tid=>$new_task) {
                          $val_arr = $RTask->getDateVals($new_task);
                          $val_arr['actiondate'] = $RTask->getActionDates($val_arr, $licence_start, $licence_end);
                          if (strtotime($val_arr['actiondate']) >= $ts) {
                             // var_dump($val_arr);
                             // die();
                              //$val_arr = $RTask->setPostVars($val_arr, $new_task);
                              $val_arr["state"] = $accounttask->row['state'];
                              $val_arr['customtext'] = $accounttask->row['customtext'];
                              $val_arr['tasktext'] = $accounttask->row['tasktext'];
                              $val_arr['categoryid'] = $accounttask->row['categoryid'];
                              $val_arr['key_task'] = $accounttask->row['key_task'];
                              $val_arr['key_task_lock'] = $accounttask->row['key_task_lock'];
                              $val_arr['accounttask_repeatingid'] = $accounttask->row['accounttask_repeatingid'];
                              $val_arr['licenceid'] = $accounttask->row['licenceid'];
                              $val_arr['notes'] = $accounttask->row['notes'];
                              $val_arr['accountid'] = $accounttask->row['accountid'];
                              $val_arr['taskid'] = $accounttask->row['taskid'];
                              $val_arr['assignedto'] = $accounttask->row['assignedto'];
                              $val_arr['budget'] = $accounttask->row['budget'];
                              $val_arr['task_email_user'] = $accounttask->row['task_email_user'];
                              $val_arr['compliance'] = $accounttask->row['compliance'];

                              $save_ok = $accounttask->insert($con, $val_arr, true, $licenceid);
                              if ($save_ok) {
                                  $taskfiles->addFiles($con, $clsUS, $accounttask->accounttaskid);
                                  $accounttask->get($con);
                                  $multi_message = emailAssignedUsers($accounttask->row, $val_arr, $accounttask->accounttaskid, $multi_message);
                              }
                          }

                      }

                  if (is_array($multi_message) && count($multi_message) > 0) {
                      emailAssignedUserMultitask($multi_message);
                  }

                  //reload page because we've just updated the list of tasks
                  if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages); 
                  //headerLocation ("$rootUrl/modules/tasklist/tasks.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);

              } else if (buttonClicked("save")) {			

                  if ($_POST["state"] != 'COMPLETED' && $rowAccountTask['state'] != 'COMPLETED') {

                      $taskfiles = new accounttaskFiles();
                      $taskfiles->newFiles();
                      $taskfiles->rmFilesAdd();

                      $tasklinks = new accounttaskLinks();
                      $tasklinks->newLinks();
                      $tasklinks->rmLinksAdd();

                      $repeating_task_save = true;
                      // if(isset($_POST['is_repeatable']) && $taskid == 0) {
                      if(isset($_POST['is_repeatable'])) {
                          //a new task
                          $rtaskid = $RTask->save($con);
                          if($rtaskid > 0) {
                              $_POST['accounttask_repeatingid'] = $rtaskid;    
                          } else {
                              $repeating_task_save = false;
                          }
                      } 
                      if ($repeating_task_save) {

                          if ($accounttaskid == 0) {
                              $insert_vals = $_POST;
                              $insert_vals['accountid'] = $accountid;
                              $insert_vals['tasktext'] = $insert_vals['customtext'];
                              if (isset($rtaskid) && $rtaskid > 0) {
                                  //insert multiple tasks
                                  $RTask->setLoopArr();
                                  if (count($RTask->loop_arr) > 0) {
                                      $save_ok = true;

                                      $multi_message = array();
                                      foreach($RTask->loop_arr as $val) {
                                          $post_vars = $RTask->setPostVars($_POST, $val);

                                          $insert_vals['actiondate'] = $RTask->getActionDates($post_vars, $RTask->start_date, $RTask->end_date);

                                          $save_ok = $accounttask->insert($con, $insert_vals, true, $licenceid);
                                          if ($save_ok) {
                                              $taskfiles->addFiles($con, $clsUS, $accounttask->accounttaskid);
                                              $accounttask->get($con);
                                              $multi_message = emailAssignedUsers($accounttask->row, $_POST, $accounttask->accounttaskid, $multi_message);
                                          }
                                      }
                                      if ($save_ok) {
                                          if (is_array($multi_message) && count($multi_message) > 0) {
                                              emailAssignedUserMultitask($multi_message);
                                          }

                                          $messages[] = SAVED;
                                          $messages[] = SAVED_REPEATING_TASKS;

                                          #return to return page
                                          if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages);

                                          $title = "Task List";	
                                          $content = "tasklist.php";

                                          $box[] = "box.key.php";
                                          if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";
                                          $box[] = "box.taskmonths.php";
                                          $box[] = "box.taskoptions.php";
                                          $box[] = "box.usersMenu.php";
                                          if ($group_id >=0) $box[] = "box.taskgroups.php";
                                          $box[] = "box.taskcategories.php";
                                          if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";

                                      } else {
                                          //safe delete the repeatingaccounttask because we'll create a new one the next time the form is saved.
                                          $messages = array_unique($messages);
                                          $RTask->delete($con);
                                      }
                                  }                                
                              } else {
                                  if ($accounttask->insert($con, $insert_vals, true, $licenceid)) {

                                      // attach the files
                                      $taskfiles->addFiles($con, $clsUS, $accounttask->accounttaskid);
                                      //$tasklinks->addLinks($con, $clsUS, $accounttask->accounttaskid);

                                      // send an email alert to any assigned users
                                      emailAssignedUsers($rowAccountTask, $_POST, $accounttask->accounttaskid);
                                      #return to return page
                                      if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages);

                                      $title = "Task List";	
                                      $content = "tasklist.php";

                                      $box[] = "box.key.php";
                                      if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";
                                      $box[] = "box.taskmonths.php";
                                      $box[] = "box.taskoptions.php";
                                      $box[] = "box.usersMenu.php";
                                      if ($group_id >=0) $box[] = "box.taskgroups.php";
                                      $box[] = "box.taskcategories.php";
                                      if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";
                                  }                                
                              }

                          //insertAccounttask();
                          } else {

                              if ($accounttask->accounttask_repeatingid > 0 && isset($_POST['edit_type']) && ($_POST['edit_type'] == 0 || $_POST['edit_type'] == 2)) {

                                  //update multiple tasks within this repeting taskid
                                  $ts = 0;
                                  if ($_POST['edit_type'] == 2) $ts = strtotime($accounttask->actiondate);
                                  $save_ok = true;
                                  $taskfiles->setRmFileids($con);
                                  $tasklinks->setRmLinkids($con);

                                  $multi_message = array();
                                  foreach ($RTask->all_task_dates as $this_id=>$actiondate) {
                                      if (strtotime($actiondate) >= $ts) {
                                          $thisTask = new accounttask ($clsUS, $this_id);
                                          $thisTask->get($con);
                                          $this_post = $_POST;
                                          $this_post['actiondate'] = strftime("%a, %d %b %Y",strtotime($thisTask->actiondate));
                                          $save_ok = updateAccounttask($thisTask, $tasklistid, $licence_end, $con, $clsUS, $this_post, $taskfiles, $tasklinks);
                                          if ($save_ok) $multi_message = emailAssignedUsers($thisTask->row, $_POST, $this_id, $multi_message);
                                      }
                                  }
                                  if ($save_ok) {
                                     if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                                     if (is_array($multi_message) && count($multi_message) > 0) {
                                         emailAssignedUserMultitask($multi_message);
                                     }
                                     $messages = array_unique($messages);
                                     headerLocation ("$rootUrl/modules/tasklist/tasks.php?tasklistid=$tasklistid&month=$month&option=$option&category=$category", $messages);

                                  }                                 

                               } else {
                                  if(isset($_POST['accounttask_repeatingid']) && $_POST['accounttask_repeatingid'] > 0) {
                                    //repeating task
                                    $RTask->setLoopArr();
                                    //first shift the first array item
                                    $shift = array_shift($RTask->loop_arr);
                                    $post_vars = $RTask->setPostVars($_POST, $shift);
                                    $insert_vals = $_POST;
                                    $insert_vals['accountid'] = $accountid;
                                    $insert_vals['actiondate'] = $RTask->getActionDates($post_vars, $RTask->start_date, $RTask->end_date);

                                    if (updateAccounttask($accounttask, $tasklistid, $licence_end, $con, $clsUS, $insert_vals, $taskfiles, $tasklinks)) {
                                      $accounttask->get($con);
                                      $insert_vals = $accounttask->row;
                                      //insert rest of tasks
                                      if (count($RTask->loop_arr) > 0) {
                                        $save_ok = true;
                                        $multi_message = array();
                                        foreach($RTask->loop_arr as $val) {
                                            $post_vars = $RTask->setPostVars($_POST, $val);
                                            $insert_vals['actiondate'] = $RTask->getActionDates($post_vars, $RTask->start_date, $RTask->end_date);
                                            $save_ok = $accounttask->insert($con, $insert_vals, true, $licenceid);
                                            if ($save_ok) {
                                                $taskfiles->addFiles($con, $clsUS, $accounttask->accounttaskid);
                                                $accounttask->get($con);
                                                $multi_message = emailAssignedUsers($accounttask->row, $_POST, $accounttask->accounttaskid, $multi_message);
                                            }
                                        }
                                        if ($save_ok) {
                                            if (is_array($multi_message) && count($multi_message) > 0) {
                                                emailAssignedUserMultitask($multi_message);
                                            }

                                            $messages[] = SAVED;
                                            $messages[] = SAVED_REPEATING_TASKS;

                                            #return to return page
                                            if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages);

                                            $title = "Task List";	
                                            $content = "tasklist.php";

                                            $box[] = "box.key.php";
                                            if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";

                                            $box[] = "box.taskmonths.php";
                                            $box[] = "box.taskoptions.php";
                                            $box[] = "box.usersMenu.php";
                                            if ($group_id >=0) $box[] = "box.taskgroups.php";
                                            $box[] = "box.taskcategories.php";
                                            if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";

                                        } else {
                                        //safe delete the repeatingaccounttask because we'll create a new one the next time the form is saved.
                                        $messages = array_unique($messages);
                                        $RTask->delete($con);
                                        }

                                      }
                                    }

                                  } else {
                                    //normal task
                                    if (updateAccounttask($accounttask, $tasklistid, $licence_end, $con, $clsUS, $_POST, $taskfiles, $tasklinks)) {
                                         //copy to users functionality	
                                         if (isset($_POST['copyto'])) {
                                             foreach ($_POST['copyto'] as $copyto) {
                                                 if (!isset($messages)) $messages = array();
                                                 $messages = copyTaskToUser($accounttask, $copyto, $accounttaskid, $con, $clsUS, $messages);
                                             }
                                         }                            
                                         // send an email alert to any assigned users
                                         emailAssignedUsers($rowAccountTask, $_POST, $accounttask->accounttaskid);
                                         #return to return page
                                         if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages);

                                         $title = "Task List";	
                                         $content = "tasklist.php";
                                         //$box[] = "actions.php";
                                         $box[] = "box.key.php";
                                         if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";

                                         $box[] = "box.taskmonths.php";
                                         $box[] = "box.taskoptions.php";
                                         $box[] = "box.usersMenu.php";
                                         if ($group_id >=0) $box[] = "box.taskgroups.php";
                                         $box[] = "box.taskcategories.php";
                                         if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";							
                                     }                                  
                                  }




                              }
                          }
                      }
          } else if($_POST["state"] != 'COMPLETED' && $rowAccountTask['state'] == 'COMPLETED') {
            #just update state to new state - uncomplete task
                      if ($accounttask->uncomplete($con, $_POST["state"])) {
              $title = "Task List";	
              $content = "tasklist.php";
              $box[] = "box.key.php";
              if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";

              $box[] = "box.taskmonths.php";
              $box[] = "box.taskoptions.php";
              $box[] = "box.usersMenu.php";
              $box[] = "box.taskcategories.php";
              if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";
            } else {
              $messages[] = $qry->getError();	
            }
          } else {
            if (isset($return) && $return != "") {
              //copy to users functionality						
                          if (isset($_POST['copyto'])) {
                              foreach ($_POST['copyto'] as $copyto) {
                                  if (!isset($messages)) $messages = array();
                                  $messages = copyTaskToUser($accounttask, $copyto, $accounttaskid, $con, $clsUS, $messages);
                              }
                          }				
                          #return to return page			
                          headerLocation ("$rootUrl/$return", false);
            }
            $title = "Task List";	
            $content = "tasklist.php";
            $box[] = "box.key.php";
            if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";

            $box[] = "box.taskmonths.php";
            $box[] = "box.taskoptions.php";
            $box[] = "box.usersMenu.php";
            $box[] = "box.taskcategories.php";
            if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";
          }
        } else if (buttonClicked("delete")) {
                  if ($accounttask->delete($con)) {
                      if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages);
                      $title = "Task List";	
                      $content = "tasklist.php";
                      //$box[] = "actions.php";
                      $box[] = "box.key.php";
                      if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";

                      $box[] = "box.taskoptions.php";
                      $box[] = "box.usersMenu.php";
                      $box[] = "box.taskcategories.php";
                      if (!isAllowed("editAccount"))   $box[] = "box.accountLicenceTasksList.php";
                  }
        } else 	if (buttonClicked("cancel")) {
          if (isset($return) && $return != "") headerLocation ("$rootUrl/$return", $messages);
          $title = "Task List";	
          $content = "tasklist.php";
          //$box[] = "actions.php";
          $box[] = "box.key.php";
          if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";

          $box[] = "box.taskoptions.php";
          $box[] = "box.usersMenu.php";
          $box[] = "box.taskcategories.php";
          if (!isAllowed("editAccount")) $box[] = "box.accountLicenceTasksList.php";
        }
      } else {
        $title = "Task List";	
        $content = "tasklist.php";
              $box[] = "box.key.php";
              if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";

              $box[] = "box.taskmonths.php";
              $box[] = "box.taskoptions.php";
              $box[] = "box.usersMenu.php";
              $box[] = "box.taskcategories.php";
              if (!isAllowed("editAccount")) $box[] = "box.accountLicenceTasksList.php";
      }
    } else {	
      $title = "Task List";	
      $content = "tasklist.php";
      //$box[] = "actions.php";
      $box[] = "box.key.php";
      if ($tasklist_conf['task_search_box'] == true) $box[] = "box.search.php";

      $box[] = "box.taskmonths.php";
      $box[] = "box.taskoptions.php";
      $box[] = "box.usersMenu.php";
          if ($group_id >=0) $box[] = "box.taskgroups.php";
      $box[] = "box.taskcategories.php";
      if (!isAllowed("editAccount"))  $box[] = "box.accountLicenceTasksList.php";
      #missing some of licenceid, accountid, accounttaskid
    }

    if (isAllowed("editAccount")) { 
      $this_page = "accounts.php";
    } else {
      $this_page = "tasks.php";
    }
      $listTasks = new listAccountTasks($licenceid, $option, $category, $user, $month, $year, $group_id, $task_search);
      $listTasks->getList();
      $optionqry = $listTasks->optionqry;
      //echo $optionqry;
      $option_title = $listTasks->option_title;
      $category_list_task_state = $listTasks->category_list_sql;
      $users_menu_option = $listTasks->users_menu_option;
      $displaydate = $listTasks->displaydate;

      # this record set is used in tasklist.php
      $optionresult = getRS($con, $optionqry);
      $monthresult = $listTasks->monthsNavBox($con);
    $optionresult = getRS($con, $optionqry);
      $listTitle = $listTasks->getListTitle($months, $group_name, $category_name, $user_name, $task_search);

    if ($title == "Task List" && isAllowed("editAccount")){
      #get account name for link back to account page
      $qry = "SELECT accountname, account.accountid as aid FROM account inner join accountlicence on account.accountid = accountlicence.accountid where licenceid = ".$licenceid;
      $result = getRS($con, $qry);
      $row = getRow($result);

      $title_crumb = ": <a class=\"crumb\"href=\"$rootUrl/handsam_core/accounts.php?accountid=".$row['aid']."\" title=\"Return to ".$row['accountname']." account details\">".$row['accountname']."</a>";
    }

    $title = licence_archive_title($licenceid, $accountid, $con);

  } else {
    #hacking attempt or wrong permissions so display index
    trigger_error("Access Denied",E_USER_WARNING);
      headerLocation("$rootUrl/index.php",false);
  }
}
else 
{
  $messages[] = "You don't have permission to manage this tasklist.";
}
#Include layout page
include("layout.php");
?>