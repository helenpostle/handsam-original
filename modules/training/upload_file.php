<?php
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc('training_file_upload');

$pageFiles->includePhp();
$pageVars = new loadVars($clsUS,$con);
$to_file_path = $accountTrainingDocs_fp;
if (isAllowed("recordTraining")) {
	if ($pageVars->fileid !== null) {	
		
		$title = "Training Log: Upload Document";
		$content = "page.file_edit.php";
				
		//get form state
		$qry = "select state from training_log where training_id = ".$pageVars->training_id;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		if ($pageDb->row['state'] == 'IN PLANNING') {
			//get file data 
			$qry = "select f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by, t.state";
			$qry .= " FROM files f left join training_files tf on f.fileid = tf.fileid left join training_log t on tf.training_id = t.training_id where f.fileid = {$pageVars->fileid} ";
			
			$pageDb = new pageQry($con, $qry);
			$pageDb->rsItem();
		
	
			if (buttonClicked("save")) {
				$fileid = trnUploadFile($clsUS, $pageVars, $con, $to_file_path, "accountTrainingDoc");
				if ($fileid) {
					//first delete this file from lotc_files - no chance of duplicates!
					$qry = new dbDelete("training_files");
					$qry->setParam("fileid",$fileid);
					$qry->setParam("training_id",$pageVars->training_id);
					if (!$qry->execute($con)) {
						//$messages[] = $qry->getSQL();
					}
					//now add the fileid to lotc_files
					$qry = new dbInsert("training_files");
					$qry->setReqNumberVal("fileid",$fileid,"fileid");
					$qry->setReqNumberVal("training_id",$pageVars->training_id,"training id");
					$qry->setAudit($clsUS->userid);
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					} else {
						$messages[] = SAVED;
						headerLocation("training.php?training_id=".$pageVars->training_id."&rand=".rand(),$messages);
					}
				}
				
			} else if (buttonClicked("cancel")) {
				
				headerLocation("training.php?training_id=".$pageVars->training_id."&rand=".rand(),$messages);
				
			} else if (buttonClicked("delete")) {
				if ($pageVars->fileid != 0) {
					$fileid = trnDeleteFile($pageVars, $con, $to_file_path);
					if ($fileid) {
						//first delete this file from lotc_files - no chance of duplicates!
						$qry = new dbDelete("training_files");
						$qry->setParam("fileid",$fileid);
						$qry->setParam("training_id",$pageVars->training_id);
						if (!$qry->execute($con)) {
							$messages[] = $qry->getError();
						} else {
							$messages[] = DELETED;
							headerLocation("training.php?training_id=".$pageVars->training_id."&rand=".rand(),$messages);
						}
					}
	
				}
				
			} 
		} else {
			headerLocation("training.php?training_id=".$pageVars->training_id."&rand=".rand(),$messages);
		}
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}
include("../../layout.php");
?>