<?php
/**
* training log link to external web app course
*
*/
ini_set("session.gc_maxlifetime", "10"); 

####make this a secure page
$secure = true;
require("../../shared/startPage.php");

/**
 * Page functionality comes from userid and accountid being passed through the querystring
 * get the both vars and forward to a 'proper' url, i.e. the same url but with the userid and accountid appended
 */
if(getIntFromRequest('training_id') === null)
{
	$uid 		= getStrFromRequest('uid');
	$mod 		= getStrFromRequest('module');
	$pass 		= getIntFromRequest('pass');
	$score 		= getIntFromRequest('score');
	$max_score 	= getIntFromRequest('max_score');
	$certificate 		= getStrFromRequest('certificate');
  
  if ($clsUS->accountid==326 && $installation == "original")
  {
    //$pass = 1;
  }
	
	$sql = "SELECT mod_item_id AS training_id FROM ext_web_app_con WHERE unique_str = '$uid' AND module='$mod' AND state IN ('NEW', 'CHECKED', 'IN PROGRESS')  order by con_id desc limit 0,1";
	$rs = getRS($con,$sql);
	$row = getRow($rs);
	
	$training_id = $row['training_id'];
	
	headerlocation("$rootUrl/modules/training/result.php?uid=$uid&module=$mod&pass=$pass&score=$score&max_score=$max_score&training_id=$training_id&certificate=$certificate");
}


$pageFiles->addModFunc("training_edit");

//remove csrf from this page because its coming from the external web app.
$pageFiles->csrf_form = '';

$pageFiles->includePhp();

$menuPage = "index.php";

$pageVars = new loadVars($clsUS, $con);

//if ($pageVars->hacking) headerLocation("$rootUrl/index.php");

$title = "Training Course Result Page";

if (isAllowed("viewTraining"))
{
	$mod 		= getStrFromRequest('module');
	$uid 		= getStrFromRequest('uid');
	$pass 		= getIntFromRequest('pass');
	$score 		= getIntFromRequest('score');
	$max_score 	= getIntFromRequest('max_score');
	$certificate 		= getStrFromRequest('certificate');
	//now check if we have an open token/hash for this post  - only get the most recent con_id because there may be multiples if they eneterd the e-training mpdule and didn'r return to handsam correctly at the end (pressed back or closed browser etc.)
	
	$sql = "SELECT * FROM ext_web_app_con WHERE unique_str = '$uid' AND module='$mod' AND state IN ('NEW', 'CHECKED', 'IN PROGRESS') order by con_id desc limit 0,1";
	
	$rs = getRS($con,$sql);
	
	if (getResultRows($rs) > 0)
	{
		$row = getRow($rs);
		//now update with data		
		
		$state = 'IN PROGRESS';
		$result_message = "Your training is in progress.";
		if ($pass == 0)
		{
			$state = 'FAIL';
			$result_message = "You did not pass the course this time. Please try again later. ";
		}
		elseif ($pass == 1)
		{
			$state = 'PASS';
			$result_message = "Well done, you passed the course this time. We have updated your training record.";
		}
		
		// get filter array querystring
		$data = "";
		foreach($_GET as $k=>$v) {
			$data .= "$k: ".getStrFromRequest($k)."; ";
		}
		//echo $data;
		
		//insert data  into ext_web_app_con table
		$qry = new dbUpdate('ext_web_app_con');
		$qry->setParam("con_id", $row['con_id']);
		$qry->setStringVal("state", $state,"State");
		$qry->setStringVal("data", $data,"data");
		$qry->addValue("checked", "NOW()");
		
		if ($qry->execute($con))
		{
			//now add data to the training log
			$qry2 = new dbUpdate('training_log');
			$qry2->setParam("training_id", $row['mod_item_id']);
			$qry2->setNumberVal("pass", $pass, "Pass");
			$qry2->setNumberVal("score", $score, "score");
			$qry2->setNumberVal("max_score", $max_score, "max_score");
			$qry2->setStringVal("certificate", $certificate, "certificate");
			
			if ($qry2->execute($con))
			{
				$messages[] = $result_message;
				
				// write to DB if the course has been passed
				if($pass == 1)
				{
					// mark training course as complete
					$updateState = changeTrainingState($con, $pageVars, $clsUS, "COMPLETE");
					
					$messages[] = $updateState;
					
					// create the next refresher course
					$formDB = getTraining($con, $pageVars); // need to pass the course details
					$saveForm = createRefresherCourse($con, $pageVars, $clsUS, $formDB);
					
					$messages[] = $saveForm;
				}				
			}
		}
		
		//now go to training log page and display result.
		headerLocation("$rootUrl/modules/training/my_training_view.php?training_id=".$row['mod_item_id'], $messages);
	}
	else
	{
		// generic message
		$messages[] = 'We could not find a record of that course in your training log. The session may have timed out, please contact an administrator. ';
	}
}
else
{
	trigger_error("Access Denied",E_USER_WARNING);
	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");