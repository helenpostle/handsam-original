<?php
$licence_conf = getLicenceConf($rowlicence);
echo "<div id=\"module_configuration\" class=\"frmSection\">";
echo "<h3>Module Configuration Options</h3>";
echo frmExRadio($licence_conf,"mod_conf_val_0",$core_conf['yes_no_arr'],"E-Training Enabled","Do you want to allow this account to have access to the Handsam E Training Web App?");
echo "<input type=\"hidden\" value=\"e_training_enabled\" name=\"mod_conf_name[]\"/>";
echo "</div>";
?>