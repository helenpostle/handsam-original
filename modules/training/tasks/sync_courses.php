<?php
/**
 * Sync Courses
 *
 * Get the modules provided by blue22 http://handsam.stage.22blue.co.uk/api/modules/all.json and sync with `training_course`
 *
 * This page is called by ./tasks.php so no need to include the startPage.php etc.
 */
//doLog("INFO", "SYNC ONLINE COURSE FROM 22BLUE");

$http_status = is_available($training_conf['ext_app_courses']);
if ($http_status == 1)
{
  $local = getDBCourses();
  $remote = getJSON();

  $add	= array_diff_key($remote, $local);
  $remove	= array_diff_key($local, $remote);
  $check_state = $remote;

  // update the db
  if(count($add) > 0)
  {
    $rec = array();
    foreach($add as $new)
    {
      $rec[] = "('0', '".$new->name."', NOW(), '1', 'NEW', '".$new->modulename."', '".$training_conf['ext_app_courses_url']."', '1', '".$training_conf['ext_app_course_def_renewal']."', '".$training_conf['ext_app_course_def_reminder']."', 'Handsam E-Training')";
    }

    $sql = "INSERT INTO training_course (accountid, title, created, created_by, state, module, course_url, online, renewal, renewal_reminder, qual_title) VALUES ".implode(', ', $rec).";";

    doLog('INFO', 'IMPORTING AN ONLINE COURSE');
    doLog('INFO', 'SQL: '. $sql);

    $res = execSQL($con, $sql);

    if($res != "") doLog('INFO', $res);
  }

  if(count($remove) > 0)
  {
    $sql = "UPDATE training_course SET state = 'DELETED' WHERE module IN ('".implode("', '", array_keys($remove) )."');";
    execSQL($con, $sql);

    // log the sql executed
    doLog('INFO', 'DELETING A COURSE');
    doLog('INFO', 'SQL: '.$sql);
  }


  //now update all of the online courses that are on the remote server to state='NEW'- so that any that have been deleted and are added again, then the state is updated. State ='NEW' for now, can change this later
  if(count($check_state) > 0)
  {
    $sql = "UPDATE training_course SET state = 'NEW' WHERE module IN ('".implode("', '", array_keys($check_state) )."');";
    execSQL($con, $sql);

    // log the sql executed
    doLog('INFO', 'UPDATING STATE OF A COURSE');
    doLog('INFO', 'SQL: '.$sql);
  }
}
/**
 * Fetches the JSON array, returns an array
 */
function getJSON()
{
	global $training_conf;
	$json = json_decode(file_get_contents($training_conf['ext_app_courses']));
	//echo $training_conf['ext_app_courses'];
	//die();
	$ret = array();
	foreach($json as $j)
		$ret[$j->Module->modulename] = $j->Module;

	return $ret;
}

/**
 * Returns a set of online courses
 */
function getDBCourses()
{
	global $con;
	$sql = "SELECT * FROM training_course WHERE online = '1';";
	
	$res = getRS($con, $sql);
	
	$ret = array();
	while ($row = getRow($res))
	{
		$ret[$row['module']] = $row;
	}
	return $ret;
}

function is_available($url, $timeout = 30) {
	$ch = curl_init(); // get cURL handle

	// set cURL options
	$opts = array(CURLOPT_RETURNTRANSFER => true, // do not output to browser
				  CURLOPT_URL => $url,            // set URL
				  CURLOPT_NOBODY => true, 		  // do a HEAD request only
				  CURLOPT_TIMEOUT => $timeout);   // set timeout
	curl_setopt_array($ch, $opts); 

	curl_exec($ch); // do it!

	$retval = curl_getinfo($ch, CURLINFO_HTTP_CODE) == 200; // check if HTTP OK

	curl_close($ch); // close handle

	return $retval;
}