<?php
	$qry = "select distinct usr.*, ";
	$qry .= "(CASE WHEN tl.renewal_date = '' THEN '' WHEN tl.renewal_date LIKE '%month%' THEN DATE_ADD(course_date,INTERVAL tl.renewal_date MONTH) WHEN tl.renewal_date LIKE '%year%' THEN DATE_ADD(course_date,INTERVAL tl.renewal_date YEAR) WHEN tl.renewal_date LIKE '%week%' THEN DATE_ADD(course_date,INTERVAL (tl.renewal_date * 7) DAY)WHEN tl.renewal_date LIKE '%day%' THEN DATE_ADD(course_date,INTERVAL tl.renewal_date DAY) ELSE 0 END) AS renewal_date,";
	$qry .= "(CASE WHEN tl.refresher_date = '' THEN '' WHEN tl.refresher_date LIKE '%month%' THEN DATE_ADD(course_date,INTERVAL tl.refresher_date MONTH) WHEN tl.refresher_date LIKE '%year%' THEN DATE_ADD(course_date,INTERVAL tl.refresher_date YEAR) WHEN tl.refresher_date LIKE '%week%' THEN DATE_ADD(course_date,INTERVAL (tl.refresher_date * 7) DAY)WHEN tl.refresher_date LIKE '%day%' THEN DATE_ADD(course_date,INTERVAL tl.refresher_date DAY) ELSE 0 END) AS refresher_date ";
	$qry .= "from training_log tl left join usr u on tl.trainee_id = u.userid where  tl.state != 'DELETED'  ORDER BY userid";
	
	echo $qry;
	die();
?>