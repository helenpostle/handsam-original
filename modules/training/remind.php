<?php
/**
* training log editing functionality
*
*/

####make this a secure page
$secure = true;
require("../../shared/startPage.php");
$title = "Sending Reminder Email about outstanding Training Courses";
$content = "page.training_remind.php";

//echo "reminding staff of outstanding courses<br />";
//GET A LIST OF THE USERS FOR THIS ACCOUNT WITH TRAINING IN PROGRESS
$account_id = $_GET['accountid'];
$user_sql = "select distinct u.userid, u.firstname, u.lastname, u.email from training_log tl";
$user_sql .= " join usr u on u.userid = tl.trainee_id where tl.accountid = ".$account_id; 
$user_sql .= " and tl.state = 'IN PROGRESS' and u.state = 'ACTIVE' order by firstname, lastname";
//echo $user_sql;
$usr_rs = getRS($con,$user_sql);
$show_msg = '';
if ($usr_rs){
  while ($usr_row = getRow($usr_rs)){
     //loop through all the users and get all of their outstanding courses
	 $course_sql = "select title, date_format(course_date, '%d-%m-%Y') as course_date from training_log tl join training_course tc on tl.course_id = tc.course_id ";
	 $course_sql .= " where trainee_id = ".$usr_row['userid']." and tl.state = \"IN PROGRESS\"";
	 //echo "<br />".$course_sql;
	 $course_rs = getRS($con, $course_sql);
	 if ($course_rs){
	    $msg = $usr_row['firstname']." ".$usr_row['lastname']." you have the following outstanding training courses to complete:<br />";
		while ($course_row = getRow($course_rs)){
		   //make up a message with each couse in
		   $msg .= "<br />".$course_row['title']." - ".$course_row['course_date'];
		}
		$body = $msg;
		
		$rootUrl = "https://handsam.org.uk/";
		$link = $rootUrl."/modules/training/my_training.php";
		$body .= "<br><br>Please <a href='".$link."'>click here</a> to go to your training page ";
		//echo "<br />".$body;
		$to = $usr_row['email'];
		$adminMail = "admin@handsam.org.uk";
		$subject = "Training Course Reminders";
		$headers = "MIME-Version: 1.0" . "\r\n";
		$headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		$headers .= "From: ".$adminMail."\r\n";
		$mail = mail($to, $subject, $body, $headers);
		$show_msg .= "<br />Reminder email sent to ".$usr_row['firstname']." ".$usr_row['lastname'];
	 }  //end of the user has some outstanding courses
  } //end of loop through rows
} else {
   $show_msg = "There are no users with outstanding training courses";
} //end of if we have a resultset


include("../../layout.php");

?>