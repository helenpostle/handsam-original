<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'training';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$qry = "select usr.*, usr.username as email from usr where userid = ".$pageVars->trainee_id;
$formDB = new pageQry($con, $qry);
$formDB->rsItem();

if ($formDB->cnt > 0) {

		echo "<div class=\"col\">";
			echo frmTextAreaNonEdit($formDB->row,"firstname","First name");
			echo frmTextAreaNonEdit($formDB->row,"lastname","Last name");
			echo frmTextAreaNonEdit($formDB->row,"email","Email");
		echo "</div>";
		echo "<div class=\"col\">";
			echo frmTextAreaNonEdit($formDB->row,"job_title","Job title");
			echo frmTextAreaNonEdit($formDB->row,"job_desc","Job description");
		echo "</div>";
}
?>