<?php

/**
* training log provider editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'training';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$qry = "select tc.*, tc.provider_id as pid, tc2.title as related from training_course tc left join training_course tc2 on tc.related_id = tc2.course_id where tc.course_id = {$pageVars->course_id}";
$formDB = new pageQry($con, $qry);
$formDB->rsItem();

if ($formDB->cnt > 0)
{
	echo "<div class=\"col\">";
	echo frmTextAreaNonEdit($formDB->row, "title","Course title");
	echo frmTextAreaNonEditArray($formDB->row,"course_type","Course type", $trn_type_arr);
	echo frmTextAreaNonEdit($formDB->row,"qual_title","Qualification title");
	echo frmTextAreaNonEdit($formDB->row,"qual_desc","Qualification description");
	if ($formDB->row['course_type'] == 'FULL') {
		//echo frmTextAreaNonEditLink($formDB->row,"related", "Refresher course", "$rootUrl/modules/training/index.php");
	} else if ($formDB->row['course_type'] == 'REFRESHER'){
		//echo frmTextAreaNonEditLink($formDB->row,"related", "Full course", "$rootUrl/modules/training/index.php");
	}
	echo "</div>";		
	echo "<div class=\"col\">";
	echo frmHiddenField($formDB->row['pid'],"pid");	
	echo frmHiddenField($formDB->row['online'],"online");	
	echo frmTextAreaNonEdit($formDB->row,"renewal","Full renewal period ");
	//echo frmTextAreaNonEdit($formDB->row,"refresher","Refresher period");
	echo frmTextAreaNonEdit($formDB->row,"renewal_reminder", "Renewal reminder period");
	//echo frmTextAreaNonEdit($formDB->row,"refresher_reminder", "Refresher reminder period");
	echo "</div>";
}