<?php

/**
* training log get select list of providers 
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'training';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
//need to select current course for this training_id
$qry = "select provider_id from training_log where training_id = ".$pageVars->training_id;
$rs = getRS($con,$qry);
$row = getRow($rs);
$id = $row['provider_id'];
if ($id == "") $id = 0;

//$qry = "select provider_id as a, provider_name as b from training_provider where ((state != 'DELETED' and provider_id != $id) or provider_id = $id ) and (accountid = ".$pageVars->accountid." or accountid = 0) order by provider_name";
$qry = "select provider_id as a, provider_name as b from training_provider where state != 'DELETED'  and (accountid = ".$pageVars->accountid." or accountid = 0) order by provider_name";
$formDB = new pageQry($con, $qry);
$formDB->rsList();

echo "<option value=\"\">Pick a provider</option>";

foreach ($formDB->rows as $row) {
	echo "<option value=\"{$row['a']}\"";
	if ($row['a'] ==  $pageVars->provider_id) {
		echo " selected ";
	}
	echo ">{$row['b']}</option>";
}


?>