<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'training';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

	$qry = "select * from training_provider where provider_id = ".$pageVars->provider_id;
	$formDB = new pageQry($con, $qry);
	$formDB->rsItem();
	
	if ($formDB->cnt > 0) {
					
		echo "<div class=\"col\">";
			echo frmTextAreaNonEdit($formDB->row,"provider_name","Provider name");
			echo frmTextAreaNonEdit($formDB->row,"provider_address1","Address 1");
			echo frmTextAreaNonEdit($formDB->row,"provider_address2","Address 2");
			echo frmTextAreaNonEdit($formDB->row,"provider_address3","Address 3");
			echo frmTextAreaNonEdit($formDB->row,"provider_city","City");
		echo "</div>";		
		echo "<div class=\"col\">";	
			echo frmTextAreaNonEdit($formDB->row,"provider_postcode","Postcode");
			echo frmTextAreaNonEdit($formDB->row,"provider_tel1","Tel 1");
			echo frmTextAreaNonEdit($formDB->row,"provider_ref","Site ref");
			echo frmTextAreaNonEdit($formDB->row,"provider_email","Email");
			echo frmTextAreaNonEdit($formDB->row,"provider_web","Website");
		echo "</div>";
		echo frmTextAreaNonEdit($formDB->row,"provider_comments","Comments", "full");
	}
?>