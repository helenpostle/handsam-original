<style>
.unassigned{color:grey}
</style>
<?php
//training course details

echo startSimpleForm($_SERVER["PHP_SELF"],"clearfix training_course", "", "", "");
echo "<div id=\"\" class=\"criteria clearfix\">";
//new conditional to check if e-training-enabled for this account:

if (isset($clsUS->conf['training']['e_training_enabled']) && $clsUS->conf['training']['e_training_enabled'] == 1) {
	$course_qry = "select course_id as a, Concat(CASE online when 0 then '' WHEN 1 then 'Handsam Online Course: ' END, title) as b, online from training_course where state != 'DELETED' and (accountid = $accountid or accountid=0) and (renewal like '% day' or  renewal like '% week' or  renewal like '% month' or  renewal like '% year') and (renewal_reminder like '% day' or renewal_reminder like '% week' or  renewal_reminder like '% month' or  renewal_reminder like '% year') order by online desc,title";
} else {
	$course_qry = "select course_id as a, Concat(CASE online when 0 then '' WHEN 1 then 'Handsam Online Course: ' END, title) as b, online from training_course where state != 'DELETED' and (accountid = $accountid or accountid=0) and (renewal like '% day' or  renewal like '% week' or  renewal like '% month' or  renewal like '% year') and (renewal_reminder like '% day' or renewal_reminder like '% week' or  renewal_reminder like '% month' or  renewal_reminder like '% year') and  (online = 0 or (online = 1 and accountid = ".$pageVars->accountid.")) order by online desc,title";
}

$acc_qry = "select accountname as a, accountid as b from account where state='ACTIVE'";
$acc_rs = getRS($con,$acc_qry);
$acc_arr = array(0=>"Select an account: ");
while ($acc_row = getRow($acc_rs))
{
  $acc_arr[$acc_row['b']] = $acc_row['a'];
}
echo '<div class="clearfix\">';

echo "<div class=\"col clearfix\">";
echo "<h3>Assigning new course</h3>";
echo "<ul>";
if (isAllowed('superAdminTraining'))
{
  echo "<li>Select the account from the select box below.</li>";
}
echo "<li>Set the criteria for the new course using top form. Select the courses using the 5 select boxes. Then load these courses into the table below by clicking [submit selections]</li>";
echo "<li>Select the course date, cost and comments in the top form.</li>";
echo "<li>if the 'Provider' select box is available in the top form, it will only apply to non E-Training courses.</li>";
echo '</div>';
echo '<div class="col clearfix">';
echo "<ul>";
echo "<li>Select the users in the table below using the check boxes.</li>";
echo "<li>Click the 'Assign Courses' button to complete the process.</li>";
echo "<li>At the end of the process the page will display a summary of the updates.</li>";
echo "<li>Each user is only allowed to have one active instance of each course at any one time. If there are more, the old ones will be deleted.</li>";

echo "</ul>";
echo '</div>';

echo '</div>';
// clearing div
echo "<div class=\"clearfix\"></div>";

echo "<div id=\"course_details\" class=\"clearfix\">";

echo "<div class=\"col1 clearfix\">";
if (isAllowed("superAdminTraining"))
{
  echo frmExSelectArrayValue($accountid,"accountid",$acc_arr,"Select an account",true,"");
}

if ($accountid > 0) {
   $course_rs = getRS($con,$course_qry);
   $course_arr = array(0=>"Select a course:");
   while ($course_row = getRow($course_rs)) {
      //loop through courses and add to an array
	  $course_arr[$course_row['a']] = $course_row['b'];
   }
   $html = "";
   foreach($selected_courses as $n=>$title) {
      //now create dropdown box from array
	  $str = "<select name=\"course_id-".$n."\"  id=\"course_id-".$n."\" class=\"".frmClass(false,'course_id')."\">\n";
	  foreach ($course_arr as $key => $text) {
	     $str .= "<option value=\"".$key."\"";
		 if ($key == $course_id[$n]) {
            $str .= " selected";
         }
		 $str .= ">".$text."</option>";
	  }
	  $str .= "</select>";
	  $date = '';
	  if (isset($_POST["course_date_id-".$n])) $date = strtotime($_POST["course_date_id-".$n]);
	  if ($date > 0) {
         $dcheck =  strftime("%a, %d %b %Y",$date);
      } else {
         $dcheck = "";
      }
	  $str .= "<input name=\"course_date_id-".$n."\" id=\"ctl.course_date_id-".$n."\" readonly=\"true\" class=\"date\" value=\"$dcheck\"/>	";
      $str .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.course_date_id-".$n."\"/>";
	  $html.= frmExRow($str,$title,"course_id"."-".$n,false,'', 'course_id');
   }  //loop through course array
   echo $html;
   
}  //accountid > 0
echo frmButtonHelp("Reset","reset", "Click here to reset the form");
echo frmButtonHelp("Submit selections","submit_criteria", "Click here load these selections below");

echo "</div>";

echo "<div class=\"col2 clearfix\">";
if ($provider === true)
  {
    $prov_qry = "select provider_id as a, provider_name as b from training_provider where state != 'DELETED' and (accountid = ".$accountid." or accountid = 0) order by provider_name";
    $prov_rs = getRS($con,$prov_qry);
    $prov_arr = array(0=>"Select a course provider:");

    while ($prov_row = getRow($prov_rs))
    {
      $prov_arr[$prov_row['a']] = $prov_row['b'];
    }

    echo frmExSelectArrayValue(0,"provider_id",$prov_arr,"Training provider",false, "");
  }

//course cost field
  $cost = '';
  if (isset($_POST['cost'])) $cost = $_POST['cost'];
  echo frmExTextFieldNonDb("cost",100,"Cost",false, $cost, "");

  //course comments field
  $comments = '';
  if (isset($_POST['comments'])) $comments = $_POST['comments'];
  echo frmTextAreaNonDb("comments",5,"Comments",false,$comments);

echo "</div>";
echo "</div>";

?>
</div>

<div id="" class="clearfix matrix">
    <h3>User/Course Table</h3>
<div id="assigncouresetabs" class="">

<ul class="tabs">
<li><a href="#tab_1">Assign Courses</a></li>
</ul>

<div id="tab_1">
   <div class="tableformsave top">
        <?php
            $frmButton = frmButtonHelp("Assign Courses", "assign", "Save your Changes"); 
            if($course_dates_validated)
            {
              echo $frmButton;
            }
            
        ?>
        <input type="submit" value="Cancel" name="bt_reset" />
     </div>  <!--end of tableformsave top-->
	 <table  id="assign_table" class="acc_list ">
	   <thead>
	      <tr>
		    <th class="start">User</th>
			<!--if we have created a course, show header for each course created-->
			<?php
			   foreach ($selected_courses as $n=>$aCourse){
			      echo "<th class=\"\">";
				  if (isset($courses[$n])) {
				    echo $courses[$n]['title'];
					echo "<br/>".$courses[$n]['date'];
					echo "<input type='hidden' name='course_date[".$courses[$n]['course_id']."]' value='".$courses[$n]['date']."' />";
					echo "<br/>[All Users <input class='select_all' rel='".$courses[$n]['course_id']."' type='checkbox' name='select_all-".$courses[$n]['course_id']."' ".$checked_all[$courses[$n]['course_id']]." value='1' id='all_users'/>]";
				  }
				  echo " </th>";
			   }  //end of loop through courses
			   echo "<th class=\"end\"></th>";
			?>
			
		  </tr>
	   </thead>
	   
	   <tbody>
	   <?php
	   	 $checked_new = "";
		 //var_dump($dbUsers);
		 if ($dbUsers){
		    foreach($dbUsers as $user) {
		    echo '<tr>';
			echo "<td class=\"start\">{$user['fullname']}</td>";
			//now create a checkbox for each course
			foreach ($selected_courses as $n=>$aCourse) {
			   echo "<td class=\"\">";
			   if (isset($courses[$n])) {
			      if (isset($dBCourse[$user['userid']][$courses[$n]['course_id']])){
				     $old_course = $dBCourse[$user['userid']][$courses[$n]['course_id']][0];
					 echo "<span class=\"max_date\">".date('M y', strtotime($old_course['max_date']))."<input type='hidden' value='".$old_course['training_id']."' name='old_course[".$user['userid']."][".$courses[$n]['course_id']."]'/></span>";
				  } 
				  if (isset($_POST['new_course'][$user['userid']][$courses[$n]['course_id']]) ) {
				        $checked_new = " checked='checked' ";
				  } else {
				        $checked_new = "";
				  }
				  echo "<input rel='".$courses[$n]['course_id']."' class=\"new_course\" type='checkbox' name='new_course[".$user['userid']."][".$courses[$n]['course_id']."]' value='1' $checked_new />";
			   } //end if isset($courses[[$n])
			   echo "</td>";
			}  //end of loop through courses
			echo "<td class=\"end\"></td>";
			echo '</tr>';
		 } //end of loop through users
		 } else {
		    echo "<tr>";
			echo "<td class='start'>";
			echo "<td colspan='5'>There has been a problem getting the users for this account</td>";
			echo "<td class='end'></td>";
			echo "</tr>";
		 }
		 
	   ?>
	   </tbody>
	   <tfoot>
	      <tr>
                <td class="start"></td>
                <td colspan="<?php echo count($selected_courses)-1;?>"></td>
                <td></td>
                <td class="end"></td>
            </tr>
	   </tfoot>
	 </table>
	 <div class="tableformsave bottom">
	    <?php
		   if($course_dates_validated)
         {
           echo $frmButton;
         }
		?>
	 </div> <!--end of tableformsave bottom-->
</div>   <!--end of tab_1-->
</div>   <!--end of assigncouresetabs-->
</div>

<?php 
//endif;
echo endFormTable2();?>