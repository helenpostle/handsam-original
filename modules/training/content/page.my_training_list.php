<?php
/***
** Shows the list of lotc plans
** 
***/

if ($pageDb->rsExists()) {
	?>
	<table  id="trn_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Staff Name</th>
				<th class="">Course Title</th>
				<th class="">Course Date</th>
				<th class="">Renewal Date</th>
				<th class="end">Course State</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "$rootUrl/modules/training/my_training_view.php?accountid={$pageVars->accountid}&amp;training_id={$row['training_id']}&amp;rand=".rand()."&amp;filter=".rawurlencode(serialize($qs));
		$training_icon = "";
		if ($row["state"] == "IN PROGRESS") {
			if (strtotime($row['course_date']) < strtotime("today")) {
				$training_icon = "<img src=\"$rootUrl/modules/training/images/icon_error_red.gif\"/>";
			} else {
				$training_icon = "<img src=\"$rootUrl/modules/training/images/icon_error.gif\"/>";
			}
		}
		if ($row["state"] == "COMPLETE") $training_icon = "<img src=\"$rootUrl/modules/training/images/tick.gif\"/>";

		?>
		<tr>
			<td class="start"><a href="<?php echo $url;?>"><span id="userid<?php echo $row["userid"]; ?>" class="hidden"><?php echo displayText($row["usr"]); ?></span></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayText($row["title"]); ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayDate($row["course_date"]); ?></a></td>
			<td class=""><a href="<?php echo $url;?>"><?php echo displayDate($row["renewal_date"]); ?></a></td>
			<td class="end"><a href="<?php echo $url;?>"><?php echo $training_icon.displayText($trn_state[$row["state"]]); ?></a>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no Training Log Reports to view");
}
