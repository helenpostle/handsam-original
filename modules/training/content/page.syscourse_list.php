<?php

/**
 * List of system wide courses
 *
 */

$pageDb = getSystemCourseList($pageVars, $con);

echo '<p><a href="?action=create" title="">Create a new course</a>.</p>';

if ($pageDb->rsExists()):
	
	echo displayParagraphs('These courses are available system wide to all accounts.');
	
	?>
	<table id="sysco_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">Course Title</th>
				<th class="">Qualification</th>
				<th class="">Renewal Period</th>
				<th class="end">Course Type</th>
				<!--
				<th class="">State</th>
				<th class="">Url</th>
				<th class=""></th>
				<th class="end">Form State</th>
				-->
			</tr>
		</thead>
		<tbody>
	<?php
		foreach ($pageDb->rows as $row):
	?>
		<tr>
			<td class="start"><a href="list_system_courses.php?course_id=<?php echo $row['course_id']; ?>" title="<?php echo $row['title'] ?>"><?php echo $row['title'] ?></a></td>
			<td ><?php echo  $row['qual_title'] ?></td>
			<td ><?php echo $row['renewal'] ?></td>
			<td class="end" ><?php echo $row['course_type'] ?></td>
		</tr>
	<?php
		endforeach;
	?>
		</tbody>
		<tfoot>
			<tr>
				<td class="start"></td>
				<td ></td>
				<td ></td>
				<td class="end"></td>
			</tr>
		</tfoot>
	</table>
	<?php
	
else :
	echo displayParagraphs('There are no system wide courses to display.');
endif;