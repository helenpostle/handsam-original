<?php
//arrays for select boxes:


echo startFormTable($_SERVER["PHP_SELF"],"clearfix", "", "Training Log", "Please edit the details of the trainee below.  Don't forget to click the 'save' button to save your changes.");
//echo formNotes(PLAN_FORM_TITLE, PLAN_FORM_INFO);


//Personal info
echo "<div id=\"userDetails\" class=\"frmSection clearfix\">";
echo "<h3>Edit Trainee Details</h3>";
echo frmHiddenField($pageVars->trainee_id,"trainee_id");

echo "<div class=\"col\">";
	echo frmTextField($formDB->row,"firstname",100,"First name",true);
	echo frmTextField($formDB->row,"lastname",100,"Last name",true);
	echo frmTextField($formDB->row,"email",100,"Email",true);
echo "</div>";
echo "<div class=\"col\">";
	echo frmTextField($formDB->row,"job_title",100,"Job title",true);
	echo frmTextArea($formDB->row,"job_desc",5,"Job description",false);
echo "</div>";

			
	
	
echo "</div>";

echo frmButtonHelp("Save","save","Click here to Save");
echo frmButtonHelp("Cancel","cancel","Click here to Cancel");
//check usertyopes for this user. if they have other usertypes other than Training Staff, then don't delete.
$usr_qry = "select * from usr_usrtype where userid = {$pageVars->trainee_id} and usertype != 'Training Staff'";
$usr_rs = getRS($con,$usr_qry);
if (getResultRows($usr_rs) == 0 && $pageVars->trainee_id > 0) {
	echo frmButtonConfirmHelp("Delete","delete", "Are you sure you want to delete this user?","Click here to delete");
}
echo frmHiddenField($tkn,"tkn");

echo endFormTable();

?>