  <h2>Update Report</h2>
	<table  id="" class="acc_list ">
  <thead>
      <tr>
          <th class="start ">Course</th>
          <th>Successful Updates</th>
          <th>Failed Updates</th>
          <th>Extra Courses Deleted</th>
          <th>Result</th>
          
          <th class="end"></th>
          
      </tr>
  </thead>
  
        <tbody>
        <?php
        foreach($update_results as $id=>$result)
        {
          $ud_success = $result['update_success'] + $result['insert_success'];
          $ud_fail = $result['update_fail'] + $result['insert_fail'];
          $res = "SUCCESS";
          if ($ud_fail > 0)
          {
            $res = "FAILURES";
          }
          $del_suc = $result['delete_success'];
          $del_fail = $result['delete_fail'];
          $del_res = "";
          if($del_fail>0)
          {
            $del_res = "($del_fail FAILURES)";
          }
        
            echo '<tr class="'.$res.'">';
            echo "<td class=\"start\">{$result['title']}</td>";
            echo "<td class=\"\">$ud_success</td>";
            echo "<td class=\"\">$ud_fail</td>";
            echo "<td class=\"\">$del_suc deleted$del_res. </td>";
            echo "<td class=\"\">$res</td>";
            
            echo "<td class=\"end\"></td>";
            echo '</tr>';
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="start"></td>
                <td colspan="3"></td>
                <td></td>
                <td class="end"></td>
            </tr>
        </tfoot>
    </table>
  <?php
  echo startSimpleForm($_SERVER["PHP_SELF"],"clearfix training_course", "", "", "");
  echo frmButtonHelp("Assign more courses","back", "Click here to assign more courses");
  echo frmHiddenField($accountid, 'accountid');
  if (isset($_POST['provider_id']))  echo frmHiddenField($_POST['provider_id'], 'provider_id');
  if (isset($_POST['comments']))  echo frmHiddenField($_POST['comments'], 'comments');
  if (isset($_POST['cost']))  echo frmHiddenField($_POST['cost'], 'cost');
  
  foreach($selected_courses as $n=>$title)
  {
    echo frmHiddenField($_POST["course_id-".$n], "course_id-".$n);
	echo "<br />course id: ".$n;
    echo frmHiddenField($_POST["course_date_id-".$n], "course_id-".$n);
  }
  
  echo endFormTable2();
  ?>
