<?php
echo startFormTable($_SERVER["PHP_SELF"],"clearfix", "", "Training Log", "Please edit the details of the course  below.  Don't forget to click the 'save' button to save your changes.");
echo frmHiddenField($pageVars->course_id,"course_id");

if ($edit_course) {
	echo "<div id=\"courseDetails\" class=\"frmSection clearfix\">";
	echo "<h3>Edit Course Details</h3>";

		echo frmTextField($formDB->row,"title",100,"Title",true);
		echo frmSelectArray($formDB->row,"course_type",$trn_type_arr,"Course Type",true);
		echo frmTextField($formDB->row,"qual_title",100,"Qualification",false);
		echo frmTextField($formDB->row,"qual_desc",100,"Description",false);
		echo frmDateAddField($formDB->row,"renewal",$trn_date_arr,"Renewal Period",true);
		echo frmDateAddField($formDB->row,"renewal_reminder",$trn_date_arr,"Renewal Reminder Period",true);
		
		$related_qry = "select course_id as a, title as b from training_course where accountid = ".$pageVars->accountid." order by title";
		//echo frmExSelectQuery($formDB->row,"related_id",$related_qry,$con,"Select a related training course",false);
	
	echo "</div>";
	
	echo frmButtonHelp("Save","save","Click here to Save");
} else {
	echo "<div id=\"courseDetails\" class=\"frmSection clearfix\">";
	echo "<h3>Edit Course Details</h3>";
	echo displayParagraphs("This course is not editable. Please click 'cancel'."  ,'frm_info');
	echo "</div>";

}
echo frmButtonHelp("Cancel","cancel","Click here to Cancel");
if($formDB->row['accountid'] == $clsUS->accountid) echo frmButtonConfirmHelp("Delete","delete", "Are you sure you want to delete this course?","Click here to delete");
echo frmHiddenField($tkn,"tkn");

echo endFormTable();
	
?>