$(document).ready(function(){
	if ($("#trainee_id").length > 0) {
		var trainee_id = $("#trainee_id").val();
		$.post(siteRoot + "/modules/training/ajax/get_user.php", {trainee_id: trainee_id}, function(data){
  			$("#trainee_detail").html(data);
		});	
	
		$("#trainee_id").change(function(event){
			var trainee_id = $(this).val();
			$.post(siteRoot + "/modules/training/ajax/get_user.php", {trainee_id: trainee_id}, function(data){
	  			$("#trainee_detail").html(data);
			});	
	
		});
	}
	

});