$(document).ready(function(){
	var course_id = $("#course_id").val();
	var training_id = $("#training_id").val();		
	getTrnCourse(course_id, training_id)
	// get rid of empty fields
	if ($("#course_id").length > 0) $('#course_detail').html('');
	
	$('form').submit(function(e){
		$("select#provider_id").removeAttr('readonly').removeAttr('disabled');
	});
	
	/**
	 * When the course drop down is changed
	 *
	 * - get and update the course details
	 * - get and update the provider details, if one exists
	 * - get and update the course renewal dates
	 * 
	 */
	$("#course_id").change(function(event){
		var course_id = $(this).val();
		var training_id = $("#training_id").val();	
		getTrnCourse(course_id, training_id);
	});

});


/**
 * Not used on the Edit Training Log Item Page
 *
 */
function changeDate(date_field, period) {
		var course_date = new Date (date_field);
		var renewal_date = new Date;
		if (period.search("year") > 0) {
			period = parseInt(period.replace(" year", ""));
			course_date.setYear(course_date.getFullYear() + period);
		} else if (period.search("month") > 0) {
			period = parseInt(period.replace(" month", ""));
			course_date.setMonth(course_date.getMonth() + period);
				
		} else if (period.search("week") > 0) {
			period = parseInt(period.replace(" week", ""));
			course_date.setDate(course_date.getDate() + (period*7));
		} else if (period.search("day") > 0) {
			period = parseInt(period.replace(" day", ""));
			course_date.setDate(course_date.getDate() + period);
		}

		return course_date.getDate() + "/" + (course_date.getMonth()+ 1) + "/" + course_date.getFullYear();
	}
	
	
	function getTrnCourse(course_id, training_id) {
		//if (course_id > 0 && training_id > 0) {
		if (course_id > 0) {
			// unbind the click events for add/edit provider
			$("#add_provider").unbind('click', addProviderExecute);
			$("#edit_provider").unbind('click', editProviderExecute);
			
			// request the course details
			$.post(siteRoot + "/modules/training/ajax/get_course.php",
					{
						course_id: course_id,
						training_id:training_id
					},
					function(data)
					{
						$("#course_detail").html(data);
						var course_date = $('[name=course_date]').val();
						var course_id = $('#course_id').val();
						var online = $('#online').val();
						// if not an online course then allow to add/edit training provider
						if(online == 1)
						{
							
							$("select#provider_id").attr('readonly', 'readonly').attr('disabled', 'disabled');
							$("select#provider_id option[value='']").html("Handsam Online Course");
							$("select#provider_id option").removeAttr('selected');
							$("select#provider_id option[value='']").attr('selected', true);
							$("#provider_detail").html('');
							$("#add_provider").unbind('click', addProviderExecute).hide();
							$("#edit_provider").unbind('click', editProviderExecute).hide();
							
						}
						else
						{
							$("select#provider_id option[value='']").html("Pick a training provider");
							$("select#provider_id").removeAttr('readonly', 'readonly').removeAttr('disabled', 'disabled');
							$("#add_provider").bind('click', addProviderExecute).show();
							$("#edit_provider").bind('click', editProviderExecute).show();
						}
						
						// request the course renewal dates
						$.post(siteRoot + "/modules/training/functionality/training_edit/ajax/get_training_dates.php", {course_id: course_id, course_date:course_date}, 
							function(data) {
								$("#dates_update").html(data);
							}
						);
					}
			);
		}		
	}
	
	
	
