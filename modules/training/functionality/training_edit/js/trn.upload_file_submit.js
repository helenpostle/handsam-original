$(document).ready(function(){
	$('#uploads_list a.edit_file').click(function(event){
		var fileid = $(this).attr('href');
		$('#frForm').append('<input type=\'hidden\' value=\''+ fileid + '\' name=\'upload_file_id\'/>').submit();
		return false;
	});	
	
	/**
	 *	Removes an attached document or link from the document/link box
	 *
	 */
	 /*
	$('img.delete_attached').live('click', function(){
		var parent = $(this).parent();
		parent.hide();
		
		del_input = parent.children('input[type="hidden"]');
		if(del_input.attr('name') == 'docID[]') attach_type = 'doc';
		if(del_input.attr('name') == 'linkId[]') attach_type = 'link';
		
		del_input.attr('name', attach_type+'DelId[]');
	});
	*/
	
	
	$('img.delete_attached').live('click', function(){
		var parent = $(this).parent();
		parent.hide();
		
		var del_input = parent.children('input[type="hidden"]');
		if(del_input.attr('name') == 'docId[]') {
			del_input.attr('name', 'docDelId[]');
			var del_input_val = del_input.parent('li').attr('id').replace('doc', '');
			del_input.val(del_input_val);
		}
		if(del_input.attr('name') == 'docAddId[]') del_input.remove();
		if(del_input.attr('name') == 'linkId[]') {
			del_input.attr('name', 'linkDelId[]');
			var del_input_val = del_input.parent('li').attr('id').replace('doc', '');
			del_input.val(del_input_val);
		}
		if(del_input.attr('name') == 'linkAddId[]') del_input.remove();
		
	});
	
});