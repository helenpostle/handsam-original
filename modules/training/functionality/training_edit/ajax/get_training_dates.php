<?php
####make this a secure page
$secure = true;
$ajaxModule = "training";

require("../../../../../shared/startPage.php");

$pageFiles->addModFunc("training_edit");
$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
$course_date = strtotime(getStrFromRequest('course_date'));
$course_date = strftime("%Y-%m-%d",  $course_date);

if (isAllowed("recordTraining")) {
	if ($pageVars->course_id !== null) {
		$qry = "select * from training_course where course_id = {$pageVars->course_id}";
		$formDB = new pageQry($con, $qry);
		$formDB->rsItem(); 
		echo displayTrainingDateField($formDB->row, "renewal", "Renewal Date", $course_date);
		
		//echo displayTrainingDateField($formDB->row, "refresher", "Refresher Date", $course_date);
		echo displayTrainingDateField($formDB->row, "renewal_reminder", "Renewal Reminder Date", $course_date);
		//echo displayTrainingDateField($formDB->row, "refresher_reminder", "Refresher Reminder Date", $course_date);
		
		
		
	}
}


function displayTrainingDateField($row, $field, $title, $course_date) {
	$str = "";
	$val = $row[$field];
	//now calculate dates:
	if ($val == "") {
		$new_date = "";
	} else if ($field == "renewal" || $field == "refresher") {
		$new_date = strtotime("$course_date + $val");
	} else if ($field == "renewal_reminder"){
		$new_date = strtotime("$course_date + ".$row['renewal']." -".$row['renewal_reminder']);
	} else if ($field == "refresher_reminder") {
		$new_date = strtotime("$course_date + ".$row['refresher']." -".$row['refresher_reminder']);
	}
	if ($new_date != "") $new_date = strftime("%a, %d %b %Y",$new_date);	
	$str .= "<input type=\"hidden\" name=\"".$field."_date\" value=\"$new_date\"/>";

	$str .= "<div class=\"view_date clearfix\">";
	$str .= "<span class=\"label\">$title </span>";
	$str .= "<span class=\"value\" id=\"".$field."_date_a\"><span>$val</span></span>";

	$str .= "<span class=\"value\" id=\"".$field."_date_b\"><span>$new_date</span></span>";
	$str .= "</div>";
	
	return $str;
}

?>