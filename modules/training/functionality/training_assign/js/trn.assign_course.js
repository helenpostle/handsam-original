$(document).ready(function(){
    
  $('#assigncouresetabs').tabs();
  
  $('input.select_all').each(function(i,v){
    if ($(this).attr('checked')) 
    {
      $('input[rel="'+  $(this).attr('rel') +'"]').attr('checked', $(this).attr('checked'));
    }
  });
  
	//$('.course_id').change(function(event){
  //  $(this).parentsUntil('form').parent().submit();
	//});
  
  $('#accountid').change(function(event){
    $(this).parentsUntil('form').parent().submit();
	});
  
  //$('input[name="group"]').change(function(event){
  //  $(this).parentsUntil('form').parent().submit();
  //});
  
    // remove user(s) from a course
    /*
    $('input[name="bt_delete"]').click(function(event){
      
        // check that a course has been selected
        var enabled = countEnabledCourses();
        if(enabled == 0)
        {
            alert('You have not selected any courses form the User/Course Table.'+"\r\n"+'Click on a column heading to select a course by date');
            highlightTable();
            return false;
        }
      
        // check that users' boxes have been cleared
        var unchecked = countUnChecked();    
        if(unchecked == 0) 
        {
            alert('You have not cleared any user check boxes from the Course Selection');
            highlightTable();
            return false;
        }
        
        var msg = "Are you sure you want to remove "+unchecked+" user"+s(unchecked)+" from the de-selected courses?";
        
        return confirm(msg);
        
    });
  */
 
  $('#frForm').submit(function(event) {
      /*
        var cp = $('#assigncouresetabs').find("input[type=checkbox]").clone();
        cp.each(function( index ){
            $(this).attr('value', $(this).attr('checked'));
        });
        cp.attr('type','hidden').appendTo($(this));
        $(this).find('input').removeAttr('disabled');
      */
        return true;
  });
  
  $('input[name="bt_assign"]').click(function(event){
    var unchecked = countCheckedNewCourse();
    
    if(unchecked == 0)
    {
        alert('You have not selected any user check boxes');
        return false;
    }
    
    var msg = "You will assign "+unchecked+" user courses";
    
    return confirm(msg)
  });
  
  $('input[name="bt_reset"]').click(function(event){
     //var unchecked = countChecked();
     var msg = "Are you sure you want to reset the form? You will lose all of the selected values and data.";
     
     return confirm(msg);
  });  
  
  $('input.select_all').click(function(event){
    $('input[rel="'+  $(this).attr('rel') +'"]').attr('checked', $(this).attr('checked'));
  });  
  
  
  $('input.rm').change(function(){
      html = ($(this).is(':checked')) ? 'assigned' : 'not assigned';
      $(this).next('span.user-status').toggleClass('unassigned').html(html);
  });
  
});

/**
 * Returns number of checked new courses
 */
function countCheckedNewCourse() 
{
return $('input[type="checkbox"][name^="new_course"]:enabled:checked').length;
}  

function countEnabledCourses()
{
  return $('input[type="checkbox"][name^="course"]:enabled:checked').length;
}

function countUnCheckedCourse() 
{
return $('input[type="checkbox"][name^="course"]:enabled:not(:checked)').length;
}

/**
* Inflection helper
* add on to end of word to add 's' if more than 1 value
*/
function s(n)
{
  return (n>1)?'s':'';
}