$(document).ready(function() {
	$('.deleteThumb').live('click',function(){
		if (confirm('Are you sure you want to delete this file?')) {
			var qs = $(this).attr('href').replace('?','');
			txt = $.ajax({
			      url: siteRoot + '/modules/training/functionality/training_upload/php/training_upload_delete.php',
			      async:		false,
			      global: false,
			      type: "GET",
			      data: qs,
			      dataType: "html",
			      success: function(){
			         $('#thumbnail').html('');
			         $('#divFileProgressContainer').html('');
			         //$('#account_logo').html('').removeAttr('id').attr('id', 'no_logo');
				              
			      }
			   }
			).responseText;
			//alert(txt);
		}
		return false;
	});
});