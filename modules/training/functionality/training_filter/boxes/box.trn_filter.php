<?php 
/**
 * lists lotc changes in form state 
 * 
 */
 
TrainingBoxTop("Training Log Filter");

	$filter_array = array();
	$filter_array['state'] = array("ALL" => "All", "COMPLETE" => "Completed Training Log Forms", "IN PROGRESS" => "Assigned Training", "IN PLANNING" => "Draft Training Log Forms", "DELETED" => "Deleted Training Log Forms");
	$filter_array['date'] = array("course_date" => "Course Date", "renewal_date" => "Renewal Date");
	//$filter_array['date'] = array("course_date" => "Course Date", "renewal_date" => "Renewal Date", "refresher_date" => "Refresher Date");
	$filter_array['month'] = array(1 => "January", 2 => "February",3 =>  "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");
	
	

	//provider filter
	$prov_qry = "select training_provider.provider_id as a, provider_name as b from training_provider left join training_log tl on training_provider.provider_id = tl.provider_id where tl.state != 'DELETED' and (training_provider.accountid = ".$pageVars->accountid." or training_provider.accountid = 0) order by provider_name";
	$rsSelect = getRS($con,$prov_qry);
	$filter_array['provider'] = array();	
	while ($row = getRow($rsSelect)) {
		$filter_array['provider'][$row['a']] = $row['b'];
	}
    

    //course filter
	$course_qry = "select training_course.course_id as a, Concat(CASE online when 0 then '' WHEN 1 then 'Handsam Online Course: ' END, title) as b from training_course left join training_log tl on training_course.course_id = tl.course_id where tl.state != 'DELETED' and (training_course.accountid = ".$pageVars->accountid." or training_course.accountid = 0) order by online desc,title";
	$rsSelect = getRS($con,$course_qry);
	$filter_array['course'] = array();	
	while ($row = getRow($rsSelect)) {
		$filter_array['course'][$row['a']] = $row['b'];
	}


	//user filter
	$usr_qry = "select userid as a, Concat(firstname, ' ', lastname) as b from usr  left join training_log tl on usr.userid = tl.trainee_id where tl.state != 'DELETED' and usr.accountid = ".$pageVars->accountid." order by lastname";
	$rsSelect = getRS($con,$usr_qry);
	$filter_array['user'] = array();	
	while ($row = getRow($rsSelect)) {
		$filter_array['user'][$row['a']] = $row['b'];
	}

	
	//year array
	$this_year = date('Y', time());
	$start_year = 2010;
	//create year and month drop downs
	$filter_array['year'] = array();
	while ($start_year <= ($this_year + 10)) {
		$filter_array['year'][$start_year] = $start_year;
		$start_year++;
	}

	$selected = "";
	foreach ($filter_array as $filter_name => $filter_name_array) {
		$qs = $qs_base;
		echo "<div>";
		
		echo "<label for=\"$filter_name\">$filter_name</label>";
	
		echo "<select id=\"$filter_name\">";
		
		$qs[$filter_name] = 0;
		$filter_link = "?filter=".rawurlencode(serialize($qs));
		if ($filter_name != 'date' && $filter_name != 'state') {
			echo "<option value=\"$filter_link\">All {$filter_name}s</option>";
		}
		foreach ($filter_array[$filter_name] as $filter_key => $filter_value) {
			$selected = "";
			$qs[$filter_name] = $filter_key;
			
			if (isset($qs_base[$filter_name]) && strval($qs_base[$filter_name]) == strval($filter_key)) { 
				$selected = "selected";
				//echo $filter_name." : ".$qs_base[$filter_name]." : ".$filter_key;
			} else {
				$selected = "";
			}
			$filter_link = "?filter=".rawurlencode(serialize($qs));
			echo "<option $selected value=\"$filter_link\">$filter_value</option>";
		}
		echo "</select>";
		echo "</div>";
	}
	
trainingBoxBottom();
?>
	