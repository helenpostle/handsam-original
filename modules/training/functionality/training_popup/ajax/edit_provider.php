<?php
####make this a secure page
$secure = true;
$ajaxModule = "training";

require("../../../../../shared/startPage.php");

$pageFiles->addModFunc("training_edit");
$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
$tkn = getStrFromRequest("tkn");

if (isAllowed("recordTraining")) {
	if ($pageVars->provider_id !== null) {
		$qry = "select * from training_provider where provider_id = {$pageVars->provider_id}";
		$formDB = new pageQry($con, $qry);
		$formDB->rsItem(); 
		
		$load = getStrFromRequest("load");

		if (buttonClicked("save")) {
			
			if ($pageVars->provider_id == 0) {
				$qry = new dbInsert("training_provider");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
			} else {
				$qry = new dbUpdate("training_provider");
				$qry->setParam("provider_id", $pageVars->provider_id);
			}		
			//course details
			
			$qry->setReqStringVal("provider_name",$_POST["provider_name"],"Provider Name");
			$qry->setStringVal("provider_address1",$_POST["provider_address1"],"Address 1");
			$qry->setStringVal("provider_address2",$_POST["provider_address2"],"Address 2");
			$qry->setStringVal("provider_address3",$_POST["provider_address3"],"Address 3");
			$qry->setStringVal("provider_city",$_POST["provider_city"],"City");
			$qry->setStringVal("provider_postcode",$_POST["provider_postcode"],"Postcode");
			$qry->setReqStringVal("provider_tel1",$_POST["provider_tel1"],"Telephone 1");
			$qry->setStringVal("provider_ref",$_POST["provider_ref"],"Provider Ref.");
			$qry->setReqStringVal("provider_email",$_POST["provider_email"],"Email ");
			$qry->setStringVal("provider_web",$_POST["provider_web"],"Website");
			$qry->setStringVal("provider_comments",$_POST["provider_comments"],"Comments");
			$qry->setAudit($clsUS->userid);
			
			
			if ($qry->execute($con)) {
				if ($pageVars->provider_id == 0) {
					$pageVars->provider_id = $qry->getNewID();	
				}
				//echo any parameters here that will be passed back to this page when successful
				echo "&provider_id=".$pageVars->provider_id;
			} else {
				echo "##ERROR: ";
				echo $qry->getError();
			}			
			
			
		} else if (buttonClicked("cancel")) {
			
			echo "cancel";
			
		} else if (buttonClicked("delete")){
			
			//check if this course is being used
			$check_qry = "select training_id from training_log where provider_id = ".$pageVars->provider_id;
			$rs = getRS($con,$check_qry);
			$row = getRow($rs);
			if ($row['training_id']) {
				//update state to deleted because its in use
				$qry = new dbUpdate("training_provider");
				$qry->setParam("provider_id", $pageVars->provider_id);
				$qry->setStringVal("state","DELETED","state");
				$qry->setAudit($clsUS->userid);
				if ($qry->execute($con)) {

					echo "&provider_id=0";
					
				} else {
					echo "##ERROR: ";
					echo $qry->getError();
				}			
				
			} else {
				//delete properly
				$qry = new dbDelete("training_provider");
				$qry->setParam("provider_id", $pageVars->provider_id);
				$qry->setParam("accountid", $pageVars->accountid);
				if ($qry->execute($con)) {

					echo "&provider_id=0";
					
				} else {
					echo "##ERROR: ";
					echo $qry->getError();
				}			
			}

		} else {
			if ($load == 'yes') {
				$qry = "select * from training_provider where provider_id = ".$pageVars->provider_id;
				$formDB = new pageQry($con, $qry);
				$formDB->rsItem();
				if ($pageVars->provider_id > 0) {			
					echo "<div class=\"col\">";
						echo frmTextAreaNonEdit($formDB->row,"provider_name","Provider name");
						echo frmTextAreaNonEdit($formDB->row,"provider_address1","Address 1");
						echo frmTextAreaNonEdit($formDB->row,"provider_address2","Address 2");
						echo frmTextAreaNonEdit($formDB->row,"provider_address3","Address 3");
						echo frmTextAreaNonEdit($formDB->row,"provider_city","City");
					echo "</div>";		
					echo "<div class=\"col\">";	
						echo frmTextAreaNonEdit($formDB->row,"provider_postcode","Postcode");
						echo frmTextAreaNonEdit($formDB->row,"provider_tel1","Tel 1");
						echo frmTextAreaNonEdit($formDB->row,"provider_ref","Site ref");
						echo frmTextAreaNonEdit($formDB->row,"provider_email","Email");
						echo frmTextAreaNonEdit($formDB->row,"provider_web","Website");
					echo "</div>";
					echo frmTextAreaNonEdit($formDB->row,"provider_comments","Comments", "full");
					echo frmHiddenField($pageVars->provider_id,"edited_id");
				}

			} else {			
				//show form
				
				include("$rootPath/modules/training/content/page.provider_edit.php");
			}
		}
	}
}
?>