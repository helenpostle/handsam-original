


/**
 * Gives functionality to the add provider button
 * Declared before document.ready
 *
 * These are bound to their elements in \web\modules\training\functionality\training_edit\js\trn.get_course.jquery.js
 *
 */
var addProviderExecute = function()
{
	var qs = {provider_id: 0};
	
	$.popUp({
		txt: 'text here',
		load_url: siteRoot + '/modules/training/functionality/training_popup/ajax/edit_provider.php',
		elem: '#provider_detail',
		elem_action: 'replace',
		ht: '500px',
		params: qs,
		delete_url: siteRoot + '/modules/training/index.php'
	}, "", function () {	
		reloadProvider();
	});	
	
}

/**
 * Gives functionality to the edit provider button
 * Declared before document.ready
 *
 */
var editProviderExecute = function()
{				
	var qs = {provider_id: $("#provider_id").val()};
	
	$.popUp({
		txt: 'text here',
		load_url: siteRoot + '/modules/training/functionality/training_popup/ajax/edit_provider.php',
		load_params: qs,
		elem: '#provider_detail',
		elem_action: 'replace',
		ht: '500px',
		params: qs,
		delete_url: siteRoot + '/modules/training/index.php'
	}, "", function () {	
		reloadProvider();
	});
	
	
}	

	function addUser() {	
		$("#add_person").click(function(event){
			
			
			var qs = {trainee_id: 0};
			
			$.popUp({
				txt: 'text here',
				load_url: siteRoot + '/modules/training/functionality/training_popup/ajax/edit_user.php',
				elem: '#trainee_detail',
				elem_action: 'replace',
				ht: '500px',
				params: qs,
				delete_url: siteRoot + '/modules/training/index.php'
			}, "", function () {	
				reloadUsers();
			});	
			
		});
	}
	
	function editUser() {	
		$("#edit_person").live('click', function() {
			
			var qs = {trainee_id: $("#trainee_id").val()};
			
			if(qs.trainee_id.length > 0)
			{
			    $.popUp({
				    txt: 'text here',
				    load_url: siteRoot + '/modules/training/functionality/training_popup/ajax/edit_user.php',
				    load_params: qs,
				    elem: '#trainee_detail',
				    elem_action: 'replace',
				    ht: '500px',
				    params: qs,
				    delete_url: siteRoot + '/modules/training/index.php'
			    }, "", function () {	
				    reloadUsers();
			    });	
			}
			else
			{
			    alert('First, choose a user');
			}
		});
	}	
	
	function reloadUsers() {
		var trainee_id = 0;
		var training_id = 0;
		if ($('#edited_id').length > 0) trainee_id = $('#edited_id').val();
		if ($('#training_id').length > 0) training_id = $("#training_id").val();
		if ($('#ajax_message').length > 0 && $('#ajax_message').val() != "") alert($('#ajax_message').val());
		$.post(siteRoot + "/modules/training/ajax/get_user_list.php", {trainee_id:trainee_id, training_id:training_id}, function(data){
  			$("#trainee_id").html(data);
			if ($('span#userid'+trainee_id).length > 0) {
				var fullname = $("#trainee_id option[value=" + trainee_id + "]").html();
				$('span#userid'+trainee_id).html(fullname);
			}
			if ($('#ajax_message').val() == 'User deleted') $("#trainee_detail").html('');
		});	
		
		if ($("#edit_person").length == 0) {
			$("#trainee_id").after(" <img id='edit_person' src='" + siteRoot + "/modules/training/images/edit_person.png' alt='edit person' title='edit person'/>");
		}
	}

	function addCourse() {	
		$("#add_course").click(function(event){
			
			var qs = {course_id: 0};
			
			$.popUp({
				txt: 'text here',
				load_url: siteRoot + '/modules/training/functionality/training_popup/ajax/edit_course.php',
				elem: '#course_detail',
				elem_action: 'replace',
				ht: '500px',
				params: qs,
				delete_url: siteRoot + '/modules/training/index.php'
			}, "", function () {	
				reloadCourse();
			});	
			
		});
	}
	
	function editCourse() {	
		$("#edit_course").live('click', function() {
			
			
			var qs = {course_id: $("#course_id").val()};
			
			if(qs.course_id.length > 0)
			{
			    $.popUp({
				    txt: 'text here',
				    load_url: siteRoot + '/modules/training/functionality/training_popup/ajax/edit_course.php',
				    load_params: qs,
				    elem: '#course_detail',
				    elem_action: 'replace',
				    ht: '500px',
				    params: qs,
				    delete_url: siteRoot + '/modules/training/index.php'
			    }, "", function () {	
				    reloadCourse();
			    });	
			}
			else
			{
			    alert('First, choose a course');
			}
		});
	}	
	
	function reloadCourse() {
		var course_id = $('#edited_id').val();
		var training_id = $("#training_id").val();
		
		$.post(siteRoot + "/modules/training/ajax/get_course_list.php", {course_id:course_id, training_id:training_id}, function(data){
  			$("#course_id").html(data);
			var course_date = $('[name=course_date]').val();
			var course_id = $('#course_id').val();
			var training_id = $("#training_id").val();	
			getTrnCourse(course_id, training_id);
			
			if ($('#ajax_message').val() == 'deleted') $("#course_detail").html('');
			$.post(siteRoot + "/modules/training/functionality/training_edit/ajax/get_training_dates.php", {course_id: course_id, course_date:course_date}, function(data){
	  			$("#dates_update").html(data);
			});	
		});	

		if ($("#edit_course").length == 0) {
			$("#course_id").after(" <img id='edit_course' src='" + siteRoot + "/modules/training/images/edit_course.png' alt='edit course' title='edit course'/>");
		}
		//alert($('#course_acc_id').val());
		checkForOnlineCourse();
	}
	
	
	function checkForOnlineCourse() {
		var online = $('#course_acc_id').val();
		if (online == 0) alert ("online");
	}
	
	function reloadProvider() {
		var provider_id = $('#edited_id').val();
		var training_id = $("#training_id").val();
		//alert(provider_id);
		
		$.post(siteRoot + "/modules/training/ajax/get_provider_list.php", {provider_id:provider_id, training_id:training_id}, function(data){
		
  			$("#provider_id").html(data);
		});	

		if ($("#edit_provider").length == 0) {
			$("#provider_id").after(" <img id='edit_provider' src='" + siteRoot + "/modules/training/images/edit_provider.png' alt='edit provider' title='edit provider'/>");
		}
	}

$(document).ready(function(){

	$("#trainee_id").after(" <img id='add_person' src='" + siteRoot + "/modules/training/images/add_person.png' alt='add person' title='add person'/> ");
	if ($("#trainee_id option").length > 1) {
		$("#trainee_id").after(" <img id='edit_person' src='" + siteRoot + "/modules/training/images/edit_person.png' alt='edit person' title='edit person'/>");
	}
	
	$("#course_id").after(" <img id='add_course' src='" + siteRoot + "/modules/training/images/add_course.png' alt='add course' title='add course'/> ");
	if ($("#course_id option").length > 1) {
		$("#course_id").after(" <img id='edit_course' src='" + siteRoot + "/modules/training/images/edit_course.png' alt='edit course' title='edit course'/>");
	}

	$("#provider_id").after(" <img id='add_provider' src='" + siteRoot + "/modules/training/images/add_provider.png' alt='add provider' title='add provider'/> ");
	if ($("#provider_id option").length > 1) {
		$("#provider_id").after(" <img id='edit_provider' src='" + siteRoot + "/modules/training/images/edit_provider.png' alt='edit provider' title='edit provider'/>");
	}

	editUser();
	addUser();
	editCourse();
	addCourse();
	

});



