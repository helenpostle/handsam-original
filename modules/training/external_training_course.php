<?php

/**
 * A fake page to emulate the external training provider website
 * 
 * Not part of the Handsam app
 * 
 */

$uid = $_GET['uid'];
$module = $_GET['module'];
$ref = $_GET['referrer'];

?>
<!doctype html>
<html>
<head>
<title>External Training Page</title>
<style>
label{
display:inline-block;
width:75px;
}
input[type="text"]{
width:300px;
}
</style>
</head>
<body>
<h1>External Training Page</h1>

<dl>
	<dt>UID:</dt>
		<dd><?php echo $uid?></dd>
	<dt>Module:</dt>
		<dd><?php echo $module?></dd>
	<dt>Referrer:</dt>
		<dd><?php echo $ref?></dd>
</dl>

<form action="<?php echo $ref?>" method="get">
	
	<label for="uid">UID</label>
	<input type="text" name="uid" id="uid" value="<?php echo $uid ?>" />
	
	<br />
	
	<label for="module">Module</label>
	<input type="text" name="module" id="module" value="<?php echo $module?>" />	
	
	<br />
	
	<label for="pass">Pass</label>
	<select name="pass" id="pass">
		<option value="2">In Progress</option>
		<option value="1">Pass</option>
		<option value="0">FAIL</option>		
	</select>
	
	<br />
	
	<label for="score">Score</label>
	<input type="text" name="score" id="score" value="" />
	
	<br />
	
	<label for="max_score">Max Score</label>
	<input type="text" name="max_score" id="max_score" value="" />
	
	<br />
	
	<input type="submit" />
	
	
	
</form>

</body>
</html>
