<?php
/**
* training log editing functionality
*
*/

####make this a secure page
ini_set('max_execution_time', '0'); // 0 = no limit.
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc("training_assign");
//$pageFiles->addModFunc("training_edit");
//$pageFiles->addModFunc("training_popup");

$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->includePhp();

$menuPage = "assign.php";
$pageVars = new loadVars($clsUS, $con);
//if multi assign not allowed, send to index
//set in installations/default/config/mods.config.php
if ($training_conf['training_multi_assign'] === false) {
  headerLocation($rootUrl."/index.php");
}
//echo "the count of selected courses is ".count($selected_courses);
if (count($selected_courses) > 0){
   $pageVars->checkCourseIdArr($clsUS,$con, count($selected_courses));
}

//var_dump($pageVars);
//var_dump($training_conf);
$selected_courses = array('Training Course 1', 'Training Course 2', 'Training Course 3', 'Training Course 4', 'Training Course 5');
if (isAllowed("recordTraining") || isAllowed("superAdminTraining")) {
   //they are allowed to assign training
  $course_id = $pageVars->course_id_arr;
  $course_date = $pageVars->course_date_arr;
  $course_dates_validated = true;
  //get the account id
  $accountid = $pageVars->accountid;
  if($accountid == null) $accountid = 0;
  //get the users for this acount
  $dbUsers = getAccountUsers($con, $accountid);
  $title = "Assign Training";
  $contentId = "contentWide";
  $content = "page.assign.php";
  //global variable used in library/funcTrainingAssign.php
  $update_results = array();
  
  $group = getStrFromRequest("group");
  $checked_all = array();
  if ($group == false) $group = "day";
  switch ($group)
  {
     case 'year':
    include('shared/funcGroup_year.php');
 	 break;
  	 case 'month':
    include('shared/funcGroup_month.php');
     break;
  case 'week':
    include('shared/funcGroup_week.php');
    break;
  case 'day':
    include('shared/funcGroup_day.php');
    break;
  default:
    include('shared/funcGroup_day.php');  
  }
  
  $date_cols = array();
  $courses = getCourseArr($course_id, $con, $accountid, $course_date);
  //echo var_dump($courses);
  //get course renewal/reminder dates
  $course_renewal = array();
  $course_reminder = array();
   
  //check for non-online courses
  $provider = false;
  if ($courses){
     foreach($courses as $aCourse) {
        $course_renewal[$aCourse['course_id']] = $aCourse['renewal'];
     	$course_reminder[$aCourse['course_id']] = $aCourse['renewal_reminder'];
     	$course_online[$aCourse['course_id']] = $aCourse['online'];
     	$course_title[$aCourse['course_id']] = $aCourse['title'];
     	if($aCourse['online'] == 0)	{
      	   $provider = true;
     	}
  	 } //end of loop through course array
	 foreach($courses as $aCourse) {
        if (isset($_POST['select_all-'.$aCourse['course_id']])) {
      	   $checked_all[$aCourse['course_id']] = " checked=checked ";
      	} else {
      	   $checked_all[$aCourse['course_id']] = "";
     	}
    }  //end of second loop through couyrse array
  } //we have some courses in the array
  
  if (count($course_id) > 0){
    $dBCourse = getAccountCourse($con, $accountid, $course_id);    
  }
  //thats all the data loaded for the pages, now what buttons have been pressed
  
  /**************************************************************************************/
  /**************************************************************************************/
  /************   the reset button was clicked, reload the assign page  *****************/
  /**************************************************************************************/
  if(buttonClicked('reset')) {
    headerLocation($rootUrl."/modules/training/assign.php");
  }
  
  /**************************************************************************************/
  /**************************************************************************************/
  /************   the delete button was clicked, and there is a course  *****************/
  /**************************************************************************************/
  if(buttonClicked('delete') && isset($_POST['course'])) {
     //set course state to deleted for each deselected user.
	 $active_courses = array_keys($_POST['course']); // training ids are the keys, value is 1
	 // for each user
      foreach($dbUsers as $user) {
	     // if the course has that user
		 if(isset($dBCourse[$user['userid']])) {
		    foreach($dBCourse[$user['userid']] as $course_date => $userCourse) {
			   // if the training id has not been sent from the page, delete
			   if(!in_array($userCourse['training_id'], $active_courses)){
			      deleteFromCourse($con, $userCourse['training_id'], $clsUS->accountid);
			   }
			} //end of loop through courses
		 }  //end of the course set for this user
	  }  //end of loop through users
	  //now send to main page again
	  headerLocation($rootUrl."/modules/training/assign.php");
  }  //end of the delete button was clicked
  
  /**************************************************************************************/
  /**************************************************************************************/
  /************   the submit criteria button was clicked                *****************/
  /**************************************************************************************/
  if(buttonClicked('submit_criteria')) {
     //validate courses and dates
	 for ($n=0; $n<count($selected_courses); $n++){
	    if ($_POST['course_id-'.$n] > 0 && $_POST['course_date_id-'.$n] == '') {
		   $course_dates_validated = false;
		   $messages[] = "Some of the courses have no date entered.";
		}
	 }
  }
  
  /**************************************************************************************/
  /**************************************************************************************/
  /************   the assign button was clicked                         *****************/
  /**************************************************************************************/
  if(buttonClicked('assign')) {
    //make suyre a course has been selected
	if (count($course_id) == 0){
	   $messages[] = 'Please select a course.';
	}
	if ($_POST['course_date'] == false || $_POST['course_date'] == ''){
      $messages[] = 'Please select a date.';
    }
    if (count($messages) == 0){
	   //we have no errors, create course for each user
	   $cnt = 0;
       $cnt2 = 0;
	   foreach($_POST['new_course'] as $key=>$val) {
	      $vals = $_POST;
		  echo var_dump($vals);
	   }  //end of loop through selected courses
	}
	
  }  //end of assign button clicked
  
} else {
    //not allowed to assign training, send back to index page
	trigger_error("Access Denied",E_USER_WARNING);
	headerLocation("$rootUrl/index.php",false);
}
$messages = array_unique($messages);
include("../../layout.php");
?>