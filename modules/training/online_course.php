<?php
/**
* training log link to external web app course
*
*/

####make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->csrf_form = '';
$pageFiles->addModFunc("training_edit");

$pageFiles->includePhp();

$menuPage = "index.php";

$pageVars = new loadVars($clsUS,$con);
if ($_SERVER['REMOTE_ADDR'] == '212.159.112.101')
{ 
//var_dump($pageVars);
//if ($pageVars->hacking === true) die();
}
if ($pageVars->hacking) headerLocation("$rootUrl/index.php");


$qs = array();

if (isAllowed("viewTraining") && $clsUS->conf['training']['e_training_enabled'] == 1) {

	//get filter array querystring
	/*
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	$qs_base = $qs;
	*/
	$formDB = getTraining($con, $pageVars);
	
	//1. insert hash, training_id, user_id, created, expiry into  ext_web_app_con 
	
	$hash = $clsUS->getUniqueString($con);
	if ($_SERVER['REMOTE_ADDR'] == '212.159.112.101')
  { 
    //echo $formDB['course_url']."?uid=$hash&module=".$formDB['module']."&referrer=$rootUrl/modules/training/result.php&domain=$rootUrl";
    //die();
  }
	$qry = new dbInsert('ext_web_app_con');
	$qry->setNumberVal("mod_item_id",$pageVars->training_id,"Training Course ID");
	$qry->setNumberVal("userid",$clsUS->userid,"User ID");
	$qry->setStringVal("unique_str",$hash,"Hash");
	$qry->setStringVal("state","NEW","State");
	$qry->setStringVal("module", $formDB['module'], "module"); // training provider's name for the module
	$qry->addValue("created","NOW()");
	if ($qry->execute($con)) {
		//2. redirect to courseurl with added hash
		
		//echo $formDB['course_url']."?uid=$hash&module=".$formDB['module']."&referrer=$rootUrl/modules/training/result.php";
		//die();
		headerLocation($formDB['course_url']."?uid=$hash&module=".$formDB['module']."&referrer=$rootUrl/modules/training/result.php&domain=$rootUrl", false);
		die();
		
	} else {
		$messages[] = $qry->getError();
	}
	
	

	
} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>