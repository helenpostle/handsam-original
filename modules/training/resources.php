<?php
/**
 * allow users to download a file from the external training provider.
 *
 */
 #### make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->includePhp();

$hash = getStrFromRequest('trainee');
$mod = getStrFromRequest('mod');

// check that this user has the appropriate permissions

$sql = "SELECT * FROM ext_web_app_con AS e WHERE e.unique_str = '$hash' AND e.module = '$mod';";

$rs = getRS($con, $sql);

if($row = getRow($rs))
{
	$fileid = getStrFromRequest('fileid');
	
	//$sql = "SELECT * FROM files WHERE fileid = $fileid";
	
	//use this query instead, so that only files for this training course and user hash are displayed.
	
	$sql = "SELECT  f.* FROM ext_web_app_con AS e
		JOIN training_log AS tl
			ON e.userid = tl.trainee_id
		JOIN training_course tc
			on tl.course_id = tc.course_id
		JOIN systemcourse_files AS scf
			ON tc.course_id = scf.training_id
		LEFT JOIN files AS f
			ON scf.fileid = f.fileid
		WHERE e.unique_str = '$hash' AND e.module = '$mod' and f.fileid=$fileid";

	
	$rs = getRS($con, $sql);
	
	while($rowFile = getRow($rs))
	{
		$filename = $rowFile["filename"];
		$file = $handsamdocs_fp.$filename;
		header("Content-Type: application/octet-stream");
		header('Content-Disposition: attachment; filename="'.$filename.'"');
	
		echo file_get_contents($file);
		exit;		
	}
}
else
{
	trigger_error("Access Denied",E_USER_WARNING);
	headerLocation("$rootUrl/index.php",false);
}

?>