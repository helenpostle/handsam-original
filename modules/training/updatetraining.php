<?php
/**
* training log editing functionality
*
*/


 
 ini_set("display_errors", 1); 
 error_reporting(E_ALL);

 $error = error_get_last();
 if ($error) {
 var_dump($error);
}


/*
flags in the table
0 = record not updated
1 = record in training log updated
2 = user not found in user table
3 = course not found in training_course
*/

require("../../shared/startPage.php");

$pageFiles->includePhp();
$sql = "select score, uid, pass_score, shortname, firstname, lastname, a.accountid, tc.course_id"; 
$sql .= " from e_training_log tl join account a on a.accountname = tl.account ";
$sql .= "join training_course tc on tl.shortname = tc.module ";
$sql .= "where ((tl.updated = 0) || (tl.updated is null)) ";
$sql .= " order by account, firstname, lastname, shortname;";
echo $sql;
$result = mysql_query($sql ,$con);

$num_rows = mysql_num_rows($result);
echo "<br />there are ".$num_rows. " rows to process";
while ($row = mysql_fetch_array($result)){
   //first off, find the user id
   $firstname = htmlspecialchars($row['firstname']);
   $lastname = htmlspecialchars($row['lastname']);
   $user_sql = 'select userid from usr where firstname = "'.htmlspecialchars($row['firstname']).'" and lastname = "'.htmlspecialchars($row['lastname']).'" and accountid = '.$row['accountid'];
   $user_result = mysql_query($user_sql, $con);
   if (mysql_num_rows($user_result) < 1){
      //couldn't find the user
	  echo "<br />couldnt find the user id for ".htmlspecialchars($row['firstname']).'" and lastname = "'.htmlspecialchars($row['lastname']);
   } else {
      //found the user
	   $userRow = mysql_fetch_array($user_result);
	   //get the record from the training_log
	  $course_sql = "select * from training_log where trainee_id = ".$userRow['userid']." and course_id = ".$row['course_id']." and state = 'IN PROGRESS' order by course_date desc";
	  //echo "<br />".$course_sql;
	  $course_result = mysql_query($course_sql, $con);
	  if (mysql_num_rows($course_result) > 0){
	     
		$certificate = "http://handsam.etrainingcentre.org/certificate/".$row['uid']."/".$row['shortname'];
		$up_sql = "update training_log set state = 'COMPLETE', pass = 1, score = ".$row['score'].", max_score = ".$row['pass_score'];
		$up_sql .= ", certificate = '".$certificate;
		$up_sql .= "' where trainee_id = ".$userRow['userid'];
		$up_sql .= " and accountid = ".$row['accountid']." and course_id = ".$row['course_id'].";";
		//echo "<br />".$up_sql;
		executeSql($up_sql);
		echo "<br />updated the training record for ".$row['firstname']." ",$row['lastname']." for the course ".$row['shortname'];
		update_training_log(htmlspecialchars($row['firstname']), htmlspecialchars($row['lastname']), $row['shortname'], 1);
	  } else {
	     //no results so user must have completed the course
		 update_training_log(htmlspecialchars($row['firstname']), htmlspecialchars($row['lastname']), $row['shortname'], 1);
	  }
   } //end of found the user and updated their record

}  //end of loop through results



function update_training_log($firstname, $lastname, $shortname, $updated){
   $sql = 'update e_training_log set updated = ';
   $sql .= $updated.' where firstname ="'.$firstname. '" and lastname = "'.$lastname.'" and shortname = "'.$shortname.'"';
   //echo "<br />".$sql;
   executeSql($sql);
   
}
//$pageVars = new loadVars($clsUS, $con);

echo "<p></p><p></p>This is the update the training log page<br /><br />";
   

?>