<?php

error_reporting(E_ALL); 
ini_set("display_errors", 1); 
$homePath = dirname(dirname(dirname(__FILE__)));
$rootPath = "$homePath/www";

require("../../shared/startPage.php");
require("$rootPath/handsam_core/library/clsPropogate/clsReceive.php");
$logFile = "/home/handsam/logs/task.log";

//$data = "";
//foreach($_POST as $k=>$v) {
//	$data .= "$k: ".getStrFromRequest($k)."; ";
//}
$pageFiles->includePhp();
$pageVars = new loadVars($clsUS, $con);

$uid 		= $_POST['?name'];
$mod 		= $_POST['module'];
$pass 		= $_POST['pass'];
$score 		= $_POST['score'];
$max_score 	= $_POST['max_score'];
$certificate 		= "http://handsam.etrainingcentre.org/certificate/".$uid."/".$mod;

if ($uid == ''){
   $msg .= "\nvisit from clicking on button";
   var_dump($_POST);
   headerLocation("$rootUrl/modules/training/my_training.php");
} else {
   //this is the first visit to the page when we need to update the records
   $msg .= "\nvisit from code";
   //get the entry from ext_web_app_con
   $sql = "SELECT * FROM ext_web_app_con WHERE unique_str = '$uid' AND module='$mod' AND state IN ('NEW', 'CHECKED', 'IN PROGRESS')  order by con_id desc limit 0,1";
   $rs = getRS($con,$sql);
   if (getResultRows($rs) > 0){
      $msg .= "\nwe are about to update ext_web_app_con";
	  $row = getRow($rs);
	  $training_id = $row['mod_item_id'];
	  $_SESSION['training_id'] = $training_id;
	  $pageVars->training_id = $training_id;
	  //we have the training id, now we need to update the record
	  $state = 'IN PROGRESS';
	  $result_message = "Your training is in progress.";
	  if ($pass == 0) {
	     $state = 'FAIL';
		 $result_message = "You did not pass the course this time. Please try again later. ";
	  }
	  if ($pass == 1){
	     $state = 'PASS';
		 $result_message = "Well done, you passed the course this time. We have updated your training record.";
	  }
	  $update_qry = "Update ext_web_app_con set state = '".$row['state']."', ";
	  $update_qry .= " data = '".$data."', checked = now()";
   	  $update_qry .= " where con_id = ".$row['con_id'];
	  $result = mysql_query($update_qry ,$con);
	  if ($result) {
	     //we have updated the ext_web_app_con table
		 $msg .= "\nsuccessfully updated ext_web_app_con";
		 //now update the training log
		 $t_log_qry = "update training_log set pass = ".$pass.", score = ".$score.", max_score = ".$max_score;
		 if ($pass == 1){
		   $t_log_qry .= ", state = 'COMPLETE'";
		}
		$t_log_qry .= ", certificate = '".$certificate."' where training_id = ".$training_id;
		$result = mysql_query($t_log_qry ,$con);
		if ($result){
		    //we successfully updated training_log
			$msg .= "\nsuccessfully updated training_log";
			//now we need to create the next course if they passed this one			
			if ($pass == 1){
			    //get the row from the training_log to copy
				$t_log_qry = "select * from training_log where training_id = ".$training_id;
				$rs = getRS($con,$t_log_qry);
				if (getResultRows($rs) > 0){
				   $row = getRow($rs);
				   // CREATE THE REFRESHER COURSE
				   $msg .= "\nget the details so we can create the refresher course";
				   $new_sql = "insert into training_log (accountid, course_id, provider_id, course_date, renewal_date, renewal_reminder_date, comments, cost, created, created_by, trainee_id, state)";
			       $new_sql .= " values (".$row['accountid'].", ".$row['course_id'].", ".$row['provider_id'].", '".$row['renewal_date']."', ";
				   $renewal_date = "date_add('".$row['course_date']."', interval 2 year)";
				   $renewal_reminder = "date_add('".$row['course_date']."', interval 23 month)";
				   $new_sql .= $renewal_date.", ".$renewal_reminder.", '".$row['comments']."', ".$row['cost'].", now(), ".$row['created_by'].", ".$row['trainee_id'].", 'IN PROGRESS')";
				   $result = mysql_query($new_sql ,$con);
				   $msg .= "\n".$new_sql;
				   //now send to the training log page to show them their results
				   //now go to training log page and display result.
		         //  headerLocation("$rootUrl/modules/training/my_training_view.php?training_id=".$training_id, $messages);
				} else {
				   //we couldn't find the record we just updated so can't create the refresher course
				   $messages[] = "There was an error with the system. Unable to create the refresher course, please forward this message to the customer support team: ". $t_log_qry;
				}			   
			}
		} else {
		   //we couldn't update the training log
		   $msg .= "\nunable to update the training_log ".$t_log_qry;
		   $messages[] = "There was an error with the system. Unable to find the connection record, please forward this message to the customer support team: ". $t_log_qry;
		}
	  } else {
	     $msg .= "\ncould not update ext_web_app_con with the sql ".$update_qry;
		 $messages[] = "There was an error with the system. Unable to find the connection record, please forward this message to the customer support team: ". $update_qry;
	  }
   } else {
      //we were not able to update the record in the database
	  //show an error message
	  $msg .= "\nunable to run ".$sql;
	  $messages[] = "There was an error with the system. Unable to find the connection record, please forward this message to the customer support team: ". $sql;
   }
}  //end of we have some post values
$fp = fopen($logFile,"a");	//open for writing, at end of file.
fwrite($fp,$msg);	
fclose($fp);

  
?>

