<?php
/*functions for listing accidents in accident book*/

// gets list of fields for a form
function getTrainingList($pageVars, $con, $filter_array) {
	//need to include secutity here. If a planner, only view own visits
	
	//implode filter array
	$filter_qry = implode(" and ", $filter_array);
	if ($filter_qry != "") $filter_qry = " and ".$filter_qry;
	
	$qry = "SELECT tl.training_id, CONCAT(u.lastname, ', ', u.firstname) AS usr, u.userid, tl.flag, renewal_date, ";
 	$qry .= "tl.state, tl.course_date, tc.title, tp.provider_name
				FROM training_log AS tl
				LEFT JOIN usr AS u ON tl.trainee_id = u.userid
				LEFT JOIN training_course AS tc ON tl.course_id = tc.course_id
				LEFT JOIN training_provider AS tp ON tl.provider_id = tp.provider_id
				WHERE tl.accountid = ".$pageVars->accountid." $filter_qry and tl.state != 'DELETED' and u.state = 'ACTIVE' ORDER BY usr;";
	
	$pageDb = new pageQry($con, $qry);
	$pageDb->rsList();
	
	return $pageDb;
}

/**
 * Return a list of system wide training courses
 */
function getSystemCourseList($pageVars, $con, $state="")
{
	// check for permissions
	// $pageVars-> ?
	if ($state != "") {
        $state = " and state=$state ";
    } else {
        $state = " and state != 'DELETED' ";
    }
	$qry = "SELECT course_id, title, qual_title, qual_desc, renewal, renewal_reminder, course_type, state, course_url
			FROM training_course
			WHERE accountid = '0'  $state 
			ORDER BY created ASC;";
	
	$pageDb = new pageQry($con, $qry);
	$pageDb->rsList();
	
	return $pageDb;
}

/**
 * Return a single training course
 */
function getSystemCourse($course_id, $con)
{
	$sql = "SELECT tc.course_id, tc.accountid, tc.title, tc.qual_title, tc.qual_desc, tc.renewal, tc.renewal_reminder, tc.course_type, tc.state, tc.online, tc.course_url 
			FROM training_course AS tc
			WHERE tc.course_id = '$course_id'
			AND tc.accountid = '0';";
			
	$pageDb = new pageQry($con, $sql);
	$pageDb->rsItem();
	
	return $pageDb;
}

/**
 * Return a single training course
 */
function getCourse($course_id, $con, $accountid)
{
	$sql = "SELECT tc.course_id, tc.accountid, tc.title, tc.qual_title, tc.qual_desc, tc.renewal, tc.renewal_reminder, tc.course_type, tc.state, tc.online, tc.course_url 
			FROM training_course AS tc
			WHERE tc.course_id = '$course_id'
			AND (tc.accountid = 0 or tc.accountid=$accountid);";
			
	$pageDb = new pageQry($con, $sql);
	$pageDb->rsItem();
	
	return $pageDb;
}

/**
 * Return a single training course
 */
function getCourseArr($course_id, $con, $accountid, $dates)
{
  if($accountid == null) $accountid = 0;
  if (count($course_id)==0) return null;
  $rows = array();
  for ($i=0; $i<count($course_id); $i++)
  {
    if ($course_id[$i] > 0 && $dates[$i] != '')
    {
      $sql = "SELECT tc.course_id, tc.accountid, tc.title, tc.qual_title, tc.qual_desc, tc.renewal, tc.renewal_reminder, tc.course_type, tc.state, tc.online, tc.course_url 
			FROM training_course AS tc
			WHERE tc.course_id = ".$course_id[$i]." 
			AND (tc.accountid = 0 or tc.accountid=$accountid);";
      $rs = getRS($con,$sql);
      $rows[$i] = getRow($rs);
      $rows[$i]['date'] = $dates[$i];
    }
  }
  /*
  $sql = "SELECT tc.course_id, tc.accountid, tc.title, tc.qual_title, tc.qual_desc, tc.renewal, tc.renewal_reminder, tc.course_type, tc.state, tc.online, tc.course_url 
			FROM training_course AS tc
			WHERE tc.course_id in (".implode(',', $course_id).")
			AND (tc.accountid = 0 or tc.accountid=$accountid);";
	
	$pageDb = new pageQry($con, $sql);
	$pageDb->rsList();
  
  for ($i=0; $i<count($course_id); $i++)
  {
    //if (isset($dates[$i])) $pageDb->rows[$i]['date'] = $dates[$i];
  }
  
   * 
   */
	return $rows;
}

/**
 * Return all Job Roles associated with a training course
 *
 * @return object MySQL Record Set
 */
function getSystemCourseJobroles($course_id, $con)
{
	$sql = "SELECT core_jobrole_id AS role_id FROM trainingcourse_jobrole WHERE training_course_id = '$course_id';";

	return getRS($con, $sql);
}

/**
 * Return all Files associated with a training course
 * 
 * @return object MySQL Record Set
 */
function getSystemCourseFiles($course_id, $con)
{
	$sql = "SELECT sc.systemcourse_fileid AS module_fileid, f.fileid, f.filename, f.title, f.filetype, f.filecategory FROM systemcourse_files AS sc JOIN files AS f ON sc.fileid = f.fileid WHERE sc.training_id = '$course_id';";
	
	return getRS($con, $sql);
}

/**
 * Return all Links associated with a training course
 * 
 * @return object MySQL Record Set
 */
function getSystemCourseLinks($course_id, $con)
{
	$sql = "SELECT sc.systemcourse_linkid AS module_linkid, l.linkid, l.title, l.url, l.linktype, l.linkcategory FROM systemcourse_links AS sc JOIN links AS l ON sc.linkid = l.linkid WHERE sc.training_id = '$course_id';";
	
	return getRS($con, $sql);
}

/**
 * Fairly specific function to retrieve the course flag
 * used in job_role_courses.php
 *
 * @var int user id
 * @var int training course id
 * @return array MySQL row
 */
function getCourseFlag($user_id, $course_id)
{
	global $con;
	$sql = "SELECT training_id, flag FROM training_log WHERE course_id = '$course_id' AND trainee_id = '$user_id';";
	
	$res = getRS($con, $sql);
	
	return getRow($res);
}

/**
 * Sets a flag for a training_log course
 *
 * @var int training_log id
 * @var string flag
 */
function setCourseFlag($training_id, $flag)
{
	global $con;
	$sql = "UPDATE training_log SET flag = '$flag' WHERE training_id = '$training_id';";
	execSQL($con, $sql);
}