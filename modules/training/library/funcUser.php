<?php

/**
 * Training specific user functions
 *
 * Author: Mei Gwilym
 */

// get list of installation's users and their job roles
/*
 * return an array with elements like this:
 * 
 * ... = array($userid1 => array(jobroles => array(role1, role2, role3)),
 *              $userid2 => array(jobroles => array(role1, role3, role4))
 *      )
 */
function getUsersJobRoles()
{
    $sql = "SELECT u.userid, j.role_id
            FROM usr AS u 
            LEFT JOIN core_usr_jobrole AS uj
                ON u.userid = uj.usr_id
            LEFT JOIN core_jobrole AS j
                ON uj.jobrole_id = j.role_id;";
    
    $rec = getRecSet($sql);
    
    $ujr = array();
    $cur_u = 0;
    $t_roles = array();
    while($row = getRowAssoc($rec))
    {
		
        if($row['userid'] != $cur_u)
        {
            // assign the roles to the user, and store in the return array
            if($cur_u > 0)$ujr[$cur_u] = array('jobroles' => $t_roles);           
            // reset
            $t_roles = array();          
        }
		$t_roles[] = $row['role_id'];
        $cur_u = $row['userid'];
       
    }
	if($cur_u > 0) $ujr[$cur_u] = array('jobroles' => $t_roles);
    return $ujr;
}



/**
 * Returns a set of users and the courses to which they're assigned
 *
 * @var array optional: course state values, e.g. PLANNING, IN PROGRESS etc
 * @return array users with array of their training courses ids
 */
function getUsersCourses($state = array())
{
	global $con, $accountid;
	
	$sql = "SELECT u.userid, u.username,
				tl.course_id
			FROM usr AS u
			LEFT JOIN training_log AS tl
				ON u.userid = tl.trainee_id";
			/*
			WHERE u.accountid = '$accountid' 
			";
			*/
	if(count($state) > 0) $sql .= " AND tl.state IN ('".implode("','", $state)."')";
	
	$res = getRS($con, $sql);
	
	$users = array();
	$user_id = 0;
	while($row = getRow($res))
	{
		if($user_id != $row['userid'])
		{
			$user_id = $row['userid'];
			
			$users[$user_id] = array('user_id' => $row['userid'],
									 'username' => $row['username']
									 );
		}
		$users[$user_id]['training_courses'][] = $row['course_id'];		
	}
	
	return $users;
}


/**
 * gives an user a new training course
 *
 * @var int user's id
 * @var int course id
 * @var string optional: Flag text, e.g. JOB ROLE etc
 * @return void
 */
function saveUserCourse($user_id, $course_id, $flag = null)
{
	global $con;
	
	// get the user's accountid
	$sql = "SELECT accountid FROM usr WHERE userid = '$user_id';";
	$res = getRS($con, $sql);
	$accountid = getRow($res);
	$accountid = $accountid['accountid'];

	// get the training course info
	$sql = "SELECT * FROM training_course WHERE course_id = '$course_id' LIMIT 1;";
	
	$res = getRS($con, $sql);
	
	while($row = getRow($res))
	{
		$curr_date = strftime("%Y-%m-%d",  time());
		
		$renewal_date = $row['renewal'];
		$renewal_reminder_date = $row['renewal_reminder'];
		$sql = "INSERT INTO training_log
				(accountid, course_id, course_date, renewal_date, created, created_by, trainee_id, renewal_reminder_date, state, flag)
				VALUES
				('$accountid', '$course_id', NOW(), DATE_ADD(NOW(),INTERVAL $renewal_date), NOW(), '0', '$user_id', DATE_SUB(DATE_ADD(NOW(),INTERVAL $renewal_date), INTERVAL $renewal_reminder_date), 'IN PLANNING', '$flag');";
		
		execSQL($con, $sql);
	}
}

