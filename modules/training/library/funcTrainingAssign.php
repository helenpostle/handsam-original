<?php
/**
 * Functions for the Training module
 *
 */

/**
 * Gets a training course and all users assigned to it for an account
 */
function getAccountCourse($con, $accountid, $course_ids, $group="day")
{	
  if(count($course_ids)==0) return null;
  $group_qry = "";
  //get date group format
  switch ($group)
  {
    case "day":
      $group_qry = ", DATE_FORMAT(tl.course_date, '%j') as g1,  DATE_FORMAT(tl.course_date, '%Y') as year  ";
      break;
    case "week":
      $group_qry = ", DATE_FORMAT(tl.course_date, '%U') as g1,  DATE_FORMAT(tl.course_date, '%Y') as year  ";
      break;
    case "month":
      $group_qry = ", DATE_FORMAT(tl.course_date, '%m') as g1,  DATE_FORMAT(tl.course_date, '%Y') as year  ";
      break;
    case "year":
      $group_qry = ", 1 as g1, DATE_FORMAT(tl.course_date, '%Y') as year  ";
      break;
  }
	$qry = "SELECT u.userid, tc.online, tc.course_id, tc.title, tc.provider_id as pid, tc.course_type, tl.trainee_id, max(tl.course_date) as max_date, tl.training_id, tl.renewal_date, tl.renewal_reminder_date, tl.cost, tl.comments, tl.state, tl.score, tl.pass, tl.max_score, tl.certificate $group_qry ";
	$qry .= "FROM training_log tl LEFT JOIN usr u ON tl.trainee_id = u.userid ";
	$qry .= "LEFT JOIN training_course tc ON tl.course_id = tc.course_id ";
	$qry .= "LEFT JOIN training_course tc2 ON tc.related_id = tc2.course_id ";
	$qry .= "WHERE tl.accountid = $accountid and tl.course_id in( ".implode(',',array_filter($course_ids, '_remove_empty_internal'))." )";
  $qry .= " and tl.state != 'IN PLANNING' and tl.state != 'DELETED' and tl.state != 'COMPLETE'   GROUP by u.userid, tc.online, tc.course_id, tc.title, tc.provider_id, tc.course_type  ORDER BY u.userid, tl.course_date ";
	$pageDb = new pageQry($con, $qry);
	
  $arr = array();
  while ($row = getRow($pageDb->rs)) {
    $arr[$row['userid']][$row['course_id']][] = $row;
  }
	return $arr;
}

/*
 * returns array of unique dates for table col headings
 */
function getDateCols($arr)
{
  $dates = array();
  //al dates wil be in the same grouping
  //either year-day, year-week, year-month, year
  foreach($arr as $user)
  {
    foreach ($user as $course)
    {
      $dates[$course['year']][intval($course['g1'])] = $course['course_date'];
    }
  }
  //sort arrays
  ksort($dates);
  $new_dates = array();
  foreach ($dates as $key=>$val)
  {
    ksort($val);
    $new_dates[$key] = $val;
  }
  return $new_dates;
}

function getAccountUsers($con, $accountid)
{
  if ($accountid==null) $accountid=0;
  $qry = "select userid, concat(firstname , ' ' , lastname) as fullname, email, state from usr where accountid = $accountid and state = 'ACTIVE' order by lastname, firstname";
  $pageDb = new pageQry($con, $qry);
  $pageDb->rsList();
  return $pageDb->rows;
}

function deleteFromCourse($con,$training_id,$accountid)
{
  $qry = new dbUpdate('training_log');
  $qry->setParam('accountid', $accountid);
  $qry->setParam('training_id', $training_id);
  $qry->setReqStringVal('state', 'DELETED', 'deleted');
  if (!$qry->execute($con)) 
  {
    return false;
  }
  else 
  {
    return true;
  }
}
   
  
  /*
   * assign a user to a course
   */
  function assignToCourse($con,$key,$accountid, $vals, $course_id, $course_title, $course_renewal, $course_reminder)
  {
    global $update_results;
    global $messages;
    global $clsUS;
    
    if(!isset($update_results[$course_id])) 
    {
      $update_results[$course_id] = array('update_success'=>0, 'update_fail'=>0,'insert_success'=>0,'insert_fail'=>0, 'delete_success'=>0, 'delete_fail'=>0, 'title'=>$course_title);
    }
    //first check if this user is a training user
    $qry = "select * from usr_usrtype where userid = $key and (usertype = 'Training Staff' or usertype = 'Training Admin')";
    $rs = getRS($con,$qry);
    //echo $qry;
    ///echo "rows:".getResultRows($rs);
    if (getResultRows($rs) == 0)
    {
      //die('l');
      //add this usertype
      $qry = new dbInsert('usr_usrtype');
      $qry->setNumberVal('userid', $key, "userid");
      $qry->setStringVal('usertype', 'Training Staff', 'usertype');
      if (!$qry->execute($con))
      {
        $messages[] = $qry->getError();
        return false;
      }
    }
    //die($qry->getSQL());
    //insert course. 
    $qry = new dbInsert("training_log");
    $qry->setReqNumberVal("accountid",$accountid,"Accountid");   
    $qry->setReqNumberVal("trainee_id",$key,"Trainee");
    $qry->setReqNumberVal("course_id",$course_id,"Training Course");

    if (!isset($vals['provider_id'])) $vals['provider_id'] = 0;
    $qry->setNumberVal("provider_id",$vals['provider_id'],"Course Provider");			

   // $course_date = $vals['course_date'];
    $course_date = $vals['course_date'][$course_id];
    $qry->setReqDateVal("course_date",$course_date,"Course Date");
    
    if ($course_renewal[$course_id] > 0)
    {
      $renewal_date = strftime("%a, %d %b %Y", strtotime($course_date.' + '.$course_renewal[$course_id]));
      $renewal_reminder_date = strftime("%a, %d %b %Y", strtotime($renewal_date.' - '.$course_reminder[$course_id]));
    }
    else
    {
      $renewal_date = null;
      $renewal_reminder_date = null;
    }
    
    if($renewal_date == null)
    {
      $qry->setNullVal('renewal_date');
    }
    else
    {
      $qry->setReqDateVal("renewal_date",$renewal_date,"Renewal Date");
    }
    
    if($renewal_reminder_date == null)
    {
      $qry->setNullVal('renewal_reminder_date');
    }
    else
    {
      $qry->setReqDateVal("renewal_reminder_date",$renewal_reminder_date,"Renewal Reminder Date");
    }
    
    $qry->setStringVal("comments",$vals["comments"],"Comments");

    $qry->setNumberVal("cost",$vals["cost"],"Cost");

    $qry->setReqStringVal("state","IN PROGRESS","State");
    $qry->setAudit($clsUS->userid);
    if ($qry->execute($con)) {
      //echo "<br/>".$qry->getSql();
      $update_results[$course_id]['insert_success'] = $update_results[$course_id]['insert_success'] + 1;
      deleteDuplicateCourses($con,$course_id, $key, $qry->getNewID(), $course_title);
      return true;
    } else {
      $update_results[$course_id]['insert_fail'] = $update_results[$course_id]['insert_fail'] + 1;
      $messages[] =  $qry->getError();
      return false;
    }     
  }
  
  
  function emailTrainingCourse($con, $accountid, $courseid){
       $sql = "select tl.training_id, tl.accountid, training_id, date_format(course_date, '%d-%m-%Y') as course_date, email, firstname, lastname, title, course_url ";
       $sql .= "from training_log tl join usr u on tl.trainee_id = u.userid ";
       $sql .= "join training_course tc on tl.course_id = tc.course_id where tl.accountid = ".$accountid; 
       $sql .= " and date(tl.created) = date(now()) and tl.state = 'IN PROGRESS' and user_refresher_email = 0";
	   $sql_rs = getRS($con,$sql);
	   while ($sql_row = getRow($sql_rs)){
	     // return var_dump($sql_row);
	      $body = $sql_row['firstname']." ".$sql_row['lastname']." you have been booked on to the following course:\r\n";
		  $body .= $sql_row['title']." - " .$sql_row['course_date'];
		  $rootUrl = "https://handsam.org.uk/";
		  $link = $rootUrl."/modules/training/my_training_view.php?accountid=".$sql_row['accountid']."&training_id=".$sql_row['training_id'];
		  $body .= "<br><br>Please <a href='".$link."'>click here</a> to go to your training page ";
		  $to = $sql_row['email'];
		  $adminMail = "admin@handsam.org.uk";
		  $subject = "New Training Course Assigned";
		  //return "sending email from ".$adminMail;
		  $headers = "MIME-Version: 1.0" . "\r\n";
		  $headers .= "Content-type:text/html;charset=UTF-8" . "\r\n";
		  $headers .= "From: ".$adminMail."\r\n";
		  $mail = mail($to, $subject, $body, $headers);
		  //now update the training log user_refresher_email field to 1 so email not sent again
		  $update_sql = "update training_log set user_refresher_email = 1 where training_id = ".$sql_row['training_id'];
		  execSQL($con, $update_sql);
		  //if ($mail){
		  //  return "email sent";
		//  } else {
		  //  return "email not sent";
		  //}

	   }
      
  }
  
  function updateTrainingLog($con,$id, $vals, $course_id, $trainee_id, $course_title, $online, $course_renewal, $course_reminder)
  {
    global $update_results;
    global $messages;
    global $clsUS;
    
    if(!isset($update_results[$course_id])) 
    {
      $update_results[$course_id] = array('update_success'=>0, 'update_fail'=>0,'insert_success'=>0,'insert_fail'=>0, 'delete_success'=>0, 'delete_fail'=>0, 'title'=>$course_title);
    }
    $qry = new dbUpdate("training_log");
    $qry->setParam('training_id', $id);
    if ($online == 0)
    {
      if (isset($vals['provider_id'])) $qry->setNumberVal("provider_id",$vals['provider_id'],"Course Provider");
    }
    $course_date = $vals['course_date'][$course_id];
    $qry->setReqDateVal("course_date",$course_date,"Course Date");
    
    
    if ($course_renewal[$course_id] > 0)
    {
      $renewal_date = strftime("%a, %d %b %Y", strtotime($course_date.' + '.$course_renewal[$course_id]));
      $renewal_reminder_date = strftime("%a, %d %b %Y", strtotime($renewal_date.' - '.$course_reminder[$course_id]));
    }
    else
    {
      $renewal_date = null;
      $renewal_reminder_date = null;
    }
    
    if($renewal_date == null)
    {
      $qry->setNullVal('renewal_date');
    }
    else
    {
      $qry->setReqDateVal("renewal_date",$renewal_date,"Renewal Date");
    }
    
    if($renewal_reminder_date == null)
    {
      $qry->setNullVal('renewal_reminder_date');
    }
    else
    {
      $qry->setReqDateVal("renewal_reminder_date",$renewal_reminder_date,"Renewal Reminder Date");
    }
    
    if (isset($vals["comments"]) && $vals["comments"] != "") $qry->setStringVal("comments",$vals["comments"],"Comments");
    if (isset($vals["cost"]) && $vals["cost"] != "") $qry->setNumberVal("cost",$vals["cost"],"Cost");

    $qry->setReqStringVal("state","IN PROGRESS","State");
    $qry->setAudit($clsUS->userid);
    if ($qry->execute($con)) {
      //echo "<br/>".$qry->getSql().";";
      $update_results[$course_id]['update_success'] = $update_results[$course_id]['update_success'] + 1;
      deleteDuplicateCourses($con,$course_id,$trainee_id, $id, $course_title);
      return true;
    } else {
      $update_results[$course_id]['update_fail'] = $update_results[$course_id]['update_fail'] + 1;
      $messages[] =  $qry->getError();
      return false;
    }      
  }
  
  function deleteDuplicateCourses($con,$course_id, $trainee_id, $training_id, $course_title)
  {
    //state=DELETED for all other courses with this course_id, userid, and state!=COMPLETE
    global $update_results;
    global $messages;
    global $clsUS;
    
    if(!isset($update_results[$course_id])) 
    {
      $update_results[$course_id] = array('update_success'=>0, 'update_fail'=>0,'insert_success'=>0,'insert_fail'=>0, 'delete_success'=>0, 'delete_fail'=>0, 'title'=>$course_title);
    }
    $qry = new dbUpdate('training_log');
    $qry->setParam('course_id', $course_id);
    $qry->setParam('trainee_id', $trainee_id);
    $qry->setStringParamInArray('state', array('IN PROGRESS', 'IN PLANNING'));
    $qry->setParamNotEqual('training_id', $training_id);
    
    $qry->setReqStringVal('state', 'DELETED', 'deleted');
    $qry->setAudit($clsUS->userid);
    if (!$qry->execute($con)) 
    {
      $update_results[$course_id]['delete_fail'] = $update_results[$course_id]['delete_fail'] + mysql_affected_rows();
      $messages[] =  $qry->getError();
      return false;
    }
    else 
    {
      $update_results[$course_id]['delete_success'] = $update_results[$course_id]['delete_success'] + mysql_affected_rows();
      //echo "<br/>".$qry->getSql();
      return true;
    }
  }
  
  //remove empty but not 0 from array
  function _remove_empty_internal($value) {
    return !empty($value) || $value === 0;
  }
