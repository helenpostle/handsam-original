<?php
if(isAllowed("viewDocument") || isAllowed("editDocument"))
{
    $doc_menu = $clsMenu->addMenuItem("/modules/documents/index.php", DOCUMENT_MENU_LINK_ADMIN, "", "");
    if(isAllowed("editDocumentCat")) 
    {
      $clsMenu->addMenuItem("/modules/documents/category.php", DOCUMENT_MENU_LINK_CAT, "", $doc_menu);
      $clsMenu->addMenuItem("/modules/documents/index.php", DOCUMENT_MENU_LINK_ADMIN, "", $doc_menu);
    }
}
else if(isAllowed("viewMyDocument") || isAllowed("editMyDocument"))
{
    $clsMenu->addMenuItem("/modules/documents/index.php", DOCUMENT_MENU_LINK_DOCS, "", "");
}

if (!isset($doc_menu) && isAllowed("editDocumentCat")) $clsMenu->addMenuItem("/modules/documents/category.php", DOCUMENT_MENU_LINK_CAT, "", "");