<?php
//tasklist modStart page
require_once("$rootPath/modules/documents/shared/config.php");

//set in mod config
$pageFiles->addModCss($modCss);
if (file_exists("$rootPath/modules/documents/css/customise.css")) $pageFiles->addModCss("customise.css");

$pageFiles->addModFunc('doc_page');
