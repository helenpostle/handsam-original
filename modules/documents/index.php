<?php

/**
 * Document Library Module
 * 
 * Similar functionality to QGP module
 * 
 * MG 2013-02-01
 * 
 * Permissions:
 *  viewDocument
 *  viewMyDocument
 *  editDocument
 *  editMyDocument
 *  viewDocLib
 *  editDocLib
 * 
 */
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addCoreLibPhp('clsAccount/clsAccount.php');
$pageFiles->addModFunc("doc_list");
$pageFiles->addModFunc("doc_filter");

$pageFiles->includePhp();
// priority for module functionality boxes over page boxes
//$pageFiles->addBoxPriority();
$qs = array();

$pageVars = new loadVars($clsUS, $con);

if(isAllowed("viewDocument") || isAllowed("viewMyDocument") || isAllowed("editDocument") || isAllowed("editMyDocument"))
{
    $title = "Document Module";
    if(isAllowed('editDocument') || isAllowed('editMyDocument'))
        $box[] = "box.admin.php";
    
    $content = "page.doclist.php";

    //get filter array querystring

    if(isset($_POST['filter']))
    {
        $qs = __unserialize($_POST['filter']);
        $qsfrm = $_POST['filter'];
    }
    else if(isset($_GET['filter']))
    {
        $qsfrm = $_GET['filter'];
        $qs = __unserialize(stripslashes($_GET['filter']));
    }
    //set a default value for use in filter
    $qs_base = $qs;
    $filter_title = array();
    $filter_qry = array();
    $extra_select = array(); // extra values to ask for in the SELECT query
    //
    //process filters
    //category filter
    if(is_array($qs) && array_key_exists('category', $qs) && $qs['category'] > 0)
    {
        $filter_qry['category'] = " fc.catid = " . mysql_real_escape_string($qs['category']);
        //get cat name from db
        $cat_qry = "SELECT cat FROM doc_cat WHERE catid = " . mysql_real_escape_string($qs['category']);
        $cat_rs = getRS($con, $cat_qry);
        $row = getRow($cat_rs);
        $filter_title['category'] = "Category - {$row['cat']}";
    }
    else
    {
        if(isset($qs['category']) && $qs['category'] === 'ALL')
        {
            $filter_qry['category'] = "1=1";
        }
        $filter_title['category'] = "All Categories";
    }

    //tag filter
    if(is_array($qs) && array_key_exists('tag', $qs) && $qs['tag'] > 0)
    {
        $filter_qry['tag'] = " ft.tagid = " . mysql_real_escape_string($qs['tag']);
        //get tag name from db
        $tag_qry = "SELECT tag FROM doc_tags WHERE tagid = " . mysql_real_escape_string($qs['tag']);
        $tag_rs = getRS($con, $tag_qry);
        $row = getRow($tag_rs);
        $filter_title['tag'] = "Tag - {$row['tag']}";
    }
    else
    {
        //$filter_qry['tag'] = "2=2";
        $filter_title['tag'] = "All Tags";
    }

    //uploaded by fiter
    if(is_array($qs) && array_key_exists('author', $qs) && $qs['author'] > 0)
    {
        //get author name
        $qry = "select usr.userid as a, Concat(firstname, ' ', lastname) as fullname from usr  where  usr.userid = " . mysql_real_escape_string($qs['author']);
        $rs = getRS($con, $qry);
        $row = getRow($rs);
        $filter_qry['author'] = " f.created_by = " . mysql_real_escape_string($qs['author']);
        $filter_title['author'] = "Uploaded by - {$row['fullname']}";
    }
    else
    {
        $qs['author'] = 0;
        //$filter_qry['author'] = "2=2";
        //$filter_title['author'] = "";
    }

    //module filter
    if(is_array($qs) && array_key_exists('module', $qs) && $qs['module'] > 0)
    {
        //get module name
        $module_title = array(1 => 'Tasklist', 2 => 'Visits', 3 => 'Incidents', 4 => 'Training');
        $filter_title['module'] = "Used in  - " . $module_title[$qs['module']];
        $filter_join = $qs['module'];
    }
    else
    {
        $filter_join = 0;
        //$filter_title['module'] = "";
    }
    /* search box */

    if(isset($_POST['search']))
    {
        $qs['search'] = trim($_POST['search_doclib']);
        $qs_base['search'] = trim($_POST['search_doclib']);
    }

    if(is_array($qs) && array_key_exists('search', $qs) && $qs['search'] != "")
    {
        $filter_qry['search'] = " f.title LIKE '%" . mysql_real_escape_string($qs['search']) . "%' ";
        $filter_title['search'] = "Search - &quot;{$qs['search']}&quot;";
    }
    else
    {
        //$filter_qry['search'] = "1=1";
        //$filter_title['search'] = "No search term";
    }
    
    // cost codes
    // really search by accountid, but display the account's Cost Code
    if($documents_conf['task_costcode_filter'] && is_array($qs) && array_key_exists('Cost Codes', $qs) && $qs['Cost Codes'] > 0)
    {
        $cost_code_ac = new Account($qs['Cost Codes']);        
        $filter_title['cost_code'] = 'Cost Code: '.$cost_code_ac->cost_code;
        
        $filter_qry['cost_codes'] = ' a.accountid = '.$qs['Cost Codes'].' ';
    }
    
    // task categories
    if($documents_conf['task_cat_filter'] && is_array($qs) && array_key_exists('Task Category', $qs) && $qs['Task Category'] > 0)
    {
        $sql = "SELECT customtext FROM taskcategory WHERE categoryid = ".$qs['Task Category'];
        $res = getRecSet($sql);
        $row = getRowAssoc($res);
        
        $filter_title['task_cat'] = 'Task Category: '.$row['customtext'];
        $filter_qry['task_cat'] = ' tc.categoryid = '.$qs['Task Category'];
        $filter_join = 5;
        
        $extra_select[] = 'tc.customtext';
    }

    //if no search filters - then modify titles to:
    if(count($filter_qry) == 0)
    {
        $filter_title['search'] = "No search term";
        $filter_title['tag'] = "No Tags";
        $filter_title['category'] = "No Categories";
        if($documents_conf['task_costcode_filter']) $filter_title['cost_code'] = 'No Cost Codes';
        if($documents_conf['task_cat_filter']) $filter_title['task_cat'] = 'No Task Categories';
    }
    
    //now implode filter title
    $filter_title_str = implode(": ", $filter_title);
    
    $pageDb = getDocList($pageVars, $con, $filter_qry, $filter_join, '', '', $extra_select);
}
else
{
    trigger_error("Access Denied", E_USER_WARNING);
    headerLocation("$rootUrl/index.php?documents", false);
}


include("../../layout.php");
