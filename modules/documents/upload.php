<?php
/**
 * Include basic stuff
 */

// make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc("doc_list");
$pageFiles->addModFunc('doc_upload');
$pageFiles->addModFunc('doc_tags');
$pageFiles->addModFunc('doc_cats');
//$pageFiles->addModFunc('doc_attachedto');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS, $con);

// priority for module functionality boxes over page boxes
$pageFiles->addBoxPriority();

$tags_str = "";

$menuPage = "admin.php";

//get this file's categories as array this is used to populate form and in the delete function later
$file_cats = getFileCatsArr($con, $pageVars->fileid);
$to_file_path = $admindocs_fp;

if(isAllowed("editDocument") || isAllowed("editMyDocument") )
{   
    $qs = '';
    if(isset($_POST['filter']))
    {
        $qsfrm = $_POST['filter'];
        $qs = __unserialize($_POST['filter']);
    }
    else if(isset($_GET['filter']))
    {
        $qsfrm = $_GET['filter'];
        $qs = __unserialize(stripslashes($_GET['filter']));
    }
    //set a default value for use in filter
    $qs_base = $qs;

    $title = "Document Module: Upload a Document ";
    $content = "page.upload.php";

    if($pageVars->fileid !== null)
    {
        $title = "Document Module: Edit a Document ";
        
        // get file data
        $qry = "SELECT f.accountid, f.filetype, f.title, f.filename, f.fileid, f.description, f.created, f.edited, f.created_by, f.edited_by                     ,f.extra_text FROM files AS f WHERE fileid = {$pageVars->fileid}";
        $pageDb = new pageQry($con, $qry);
        $pageDb->rsItem();
        if($pageDb->row == false)
        {
            if($pageVars->fileid !== 0) $messages[] = $pageVars->fileid.': File does not exist';
        }
        if ($pageDb->row['fileid'] > 0 && isAllowedToEditDoc($pageDb->row['created_by'], $pageDb->row['accountid']) === false)
        {
          trigger_error("Access Denied", E_USER_WARNING);
          headerLocation("$rootUrl/index.php", false);
        }
        
        // file usage (http://jira.semantise.com:8080/browse/HAND-916)
        $countFileUsed = countFileUsed($pageVars->fileid, $con);
        
        if($countFileUsed['cnt'] > 0)
        {
            $fileUsage = array();
            if(!is_null($countFileUsed['lotc_fileid'])) 
            {
                $fileUsage['lotc']['title'] = 'LOtC Module';
                $fileUsage['lotc']['file'] = lotcFileUse($pageVars->fileid);
            }
            if(!is_null($countFileUsed['a_lotc_fileid'])) 
            {
                $fileUsage['alotc']['title'] = 'LOtC Module';
                $fileUsage['alotc']['file'] = alotcFileUse($pageVars->fileid);
            }
            if(!is_null($countFileUsed['taskfileid'])) 
            {
                $fileUsage['task']['title'] = 'Tasks Module';
                $fileUsage['task']['file'] = taskFileUse($pageVars->fileid);
            }
            if(!is_null($countFileUsed['acc_fileid'])) 
            {
                $fileUsage['acc']['title'] = 'Incident Module';
                $fileUsage['acc']['file'] = accFileUse($pageVars->fileid);
            }
            
            if(!is_null($countFileUsed['training_fileid'])) 
            {
                $fileUsage['train']['title'] = 'Training Module';
                $fileUsage['train']['file'] = trainFileUse($pageVars->fileid);
            }
            if(!is_null($countFileUsed['pagefileid'])) 
            {
                $fileUsage['page']['title'] = 'Page/Content Module';
                $fileUsage['page']['file'] = pageFileUse($pageVars->fileid);
            }
        }
        
        if(buttonClicked("save"))
        {
          
            $result = docUpload($clsUS, $pageVars, $con, $to_file_path, $pageDb->row);
            
            if(is_int($result))
            {
                $fileid = $result;
                $tags = new docTags($fileid);
                $tags->saveTags($_POST['tags'], $clsUS, $con);

                saveFileCats($fileid, $con);
                headerLocation("index.php?filter=" . urlencode(serialize($qs)) . "&rand=" . rand(), $messages);
            }
            else
            {
                $uploaded_filename = $result;
                $file_cats = array();
                $tags_str = "";
                if (isset($_POST['catid'])) $file_cats = $_POST['catid'];
                if (isset($_POST['tags'])) $tags_str = $_POST['tags'];
            }
        }
        else if(buttonClicked("cancel"))
        {
            headerLocation("index.php?filter=" . urlencode(serialize($qs)) . "&rand=" . rand(), $messages);
        }
        else if(buttonClicked("delete"))
        {
            $file_use = countFileUsed($pageVars->fileid, $con);
            if($file_use['cnt'] > 0)
            {
                $messages[] = 'The file is currently in use and cannot be deleted';
            }
            else
            {
                if($pageVars->fileid != 0)
                {
                    $tags = new docTags($pageVars->fileid);
                    $tags->delTags($con);
                    deleteFileCats($file_cats, $pageVars->fileid, $con);
                    $fileid = docDelete($pageVars, $con, $to_file_path);
                    if($fileid)
                    {
                        headerLocation("index.php?filter=" . urlencode(serialize($qs)) . "&rand=" . rand(), $messages);
                    }
                }
            }
        }
        else
        {
            $tags = new docTags($pageVars->fileid);
            $tags_str = $tags->getTagsStr($con);
        }
    }
}
else
{
    trigger_error("Access Denied", E_USER_WARNING);
    headerLocation("$rootUrl/index.php", false);
}
include("../../layout.php");
