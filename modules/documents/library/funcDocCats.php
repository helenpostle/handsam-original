<?php
/*
 *
 * doc category admin functions
 *
 */
 
function catFileCount($con, $catid) {
	 $qry = "select cat_fileid from doc_file_cat where catid = $catid";
	 $rs = getRs($con, $qry);
	 return getResultRows($rs);
}

function getFileCatsArr($con, $fileid) {
	$arr = array();
	$qry = "select c.catid, c.cat from doc_cat c left join doc_file_cat fc on c.catid = fc.catid where fc.fileid = $fileid";
	$rs = getRs($con, $qry);
	while ($row = getRow($rs)) {
		$arr[$row['cat']] = $row['catid'];
	}
	return $arr;
}	

function saveFileCats($fileid, $con) {
	//first get all cats for this file from db
	$file_cats_db = getFileCatsArr($con, $fileid);
	if (isset($_POST['catid']) && is_array($_POST['catid'])) {
		$catids = $_POST['catid'];
		foreach ($catids as $catid) {
			if (in_array($catid, $file_cats_db)) {
				//remove from array
				$file_cats_db = array_diff($file_cats_db, array($catid));
			} else {
				//insert file category
				$qry = new dbInsert("doc_file_cat");
				$qry->setReqNumberVal("catid",$catid,"Category id");
				$qry->setReqNumberVal("fileid",$fileid,"File id");
				if (!$qry->execute($con)) {
					//some logging here if needed
				}
			}
		}
	}
	
	//now delete all file_cats remaining in the array - $file_cats_db
	deleteFileCats($file_cats_db, $fileid, $con);
}


function deleteFileCats($file_cat_arr, $fileid, $con) {
	foreach ($file_cat_arr as $file_cat) {
		$qry = new dbDelete("doc_file_cat");
		$qry->setParam("catid",$file_cat);
		$qry->setParam("fileid",$fileid);
		if (!$qry->execute($con)) {
			//echo $qry->getError();
			//echo $qry->getSQL();
			//some logging needed here
		}
	}
}
?>