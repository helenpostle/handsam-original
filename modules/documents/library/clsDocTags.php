<?php
class docTags  {

	var $fileid;
	var $item_tags = array();
	var $messages = array();
	var $tags_str = "";
    
    
    function docTags($fileid) {
	    $this->fileid = $fileid;
    }
    
    
    function deleteFileTags($con, $userid=0) {
	    foreach ($this->item_tags as $tagid => $tag ) {
		    
		    //delete from doc_file_tags
			$qry = new dbDelete("doc_file_tags");
			$qry->setParam("fileid",$this->fileid);
			$qry->setParam("tagid",$tagid);
			if ($userid > 0) $qry->setParam("created_by",$userid);
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			}
		    		    
	    	//Now count if theres any more in doc_file_tags
	    	$rsCheck = getRS($con,"select tag_fileid from doc_file_tags where tagid = $tagid");
			//check if tag exists
			if (getResultRows($rsCheck) == 0) {
				//delete from tags table
				$del_qry = new dbDelete("doc_tags");
				$del_qry->setParam("tagid",$tagid);
				if (!$del_qry->execute($con)) {
					$this->messages[] = $del_qry->getError();
				}
			}
	    }
    }
    
    
    function insertFileTag($tagid, $con, $clsUS) {
	   	$rsCheck = getRS($con,"select tag_fileid from doc_file_tags where tagid = $tagid and fileid = $this->fileid");

		if (getResultRows($rsCheck) == 0) {
			//insert tag
			$qry = new dbInsert("doc_file_tags");
			$qry->setReqNumberVal("tagid",$tagid,"tagid");
			$qry->setReqNumberVal("fileid",$this->fileid, "fileid");
			$qry->setAudit($clsUS->userid);
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			}

		}
    }
    
    
    
    function insertTag($tag, $clsUS, $con) {
	    $tag = trim($tag);
      //check if tag already exists
        $rsCheck = getRS($con,"select tagid from doc_tags where tag = '$tag'");
      //check if tag exists
      if (getResultRows($rsCheck) == 0) {
        //insert tag
        $qry = new dbInsert("doc_tags");
        $qry->setReqStringVal("tag",$tag, "tag");
        $qry->setAudit($clsUS->userid);
        if (!$qry->execute($con)) {
          $this->messages[] = $qry->getError();
        } else {
          // return new tagid
          return $qry->getNewID();
        }
      } else {
        $row = getRow($rsCheck);
        //return existing tagid
        return $row['tagid'];
      }
    }
    
    
    function saveTags($tag_str, $clsUS, $con) {
	    
	    //first get an array with all current tags for this item
	    $this->getTags($con);
	    
		$tags = array();
		$tags = explode(",", $tag_str);
		$tags = array_filter($tags);
                
		foreach ($tags as $tag) {
			if (in_array($tag, $this->item_tags)) {
				//remove from array
				$this->item_tags = array_diff($this->item_tags, array($tag));
			} else {
				//insert tag
				$tagid = $this->insertTag($tag, $clsUS, $con);
				$this->insertFileTag($tagid, $con, $clsUS);
			}
		}
		
		//now delete tags left in the item_tags array
		$this->deleteFileTags($con, $clsUS->userid); 

    }
    
    function delTags($con) {
	    //first get an array with all current tags for this item
	    $this->getTags($con);
		//now delete tags left in the item_tags array
		$this->deleteFileTags($con);
    }
    
    
    function getTags($con) {
	    if (count($this->item_tags) == 0) {
		    $qry = "select * from doc_tags left join doc_file_tags on doc_tags.tagid = doc_file_tags.tagid where doc_file_tags.fileid = {$this->fileid} order by tag asc"; 
	  	    $rs = getRS($con,$qry);
	       	while($row = getRow($rs)) {
				$this->item_tags[$row['tagid']] = $row['tag'];
	       	}
       	}
    }
     
    function getTagsStr($con) {
	    $this->getTags($con);
	    return implode(",", $this->item_tags);
    }
    
 
    function getTagsLi($con) {
	    $this->getTags($con);
	    return "<li>".implode("</li><li>", $this->item_tags)."</li>";
    }
}