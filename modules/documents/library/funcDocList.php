<?php
/* 
 * Lib list functions
*/

function getDocList($pageVars, $con, $filter_array, $filter_join=0, $filecat="", $allowed_tags="", $select = array()) {   
    // default fields to select
    $select_array = array('f.extra_text, f.created_by', 'f.accountid', 'f.title', 'f.description', 'f.edited', 'f.created', 'CASE  WHEN f.edited > f.created THEN f.edited ELSE f.created  END  AS last_edited', 'f.fileid', 'a.accountname', 'a.cost_code');
    
    $select_array = array_merge($select_array, $select);
    
    //implode filter array
    $filter_qry = implode(" and ", $filter_array);
    
    if(trim($allowed_tags) != "")
    {
        $allowed_tags_qry = " and ft.tagid in ($allowed_tags) ";
    }
    else
    {
        $allowed_tags_qry = "";
    }
    if(trim($filecat) != "")
    {
        $file_cat_qry = " and f.filecategory = '$filecat' ";
    }
    else
    {
        $file_cat_qry = "";
    }
    if(trim($filter_qry) != "")
    {
        $filter_qry = " and " . $filter_qry;
    }
    else
    {
        $filter_qry = " \r\n-- no filter qry \r\n";
    }
  
  
  switch ($filter_join) {
    case 0:
        $join =  " ";
        break;
    case 1:
        if ($core_conf['key_task'] == true)
        {
          $join = " right join task_files tf on f.fileid = tf.fileid ";
        }
        else 
        {
          $join = " right join task_files tf on f.fileid = tf.fileid ";
        }
        break;
    case 2:
        $join = " right join lotc_files lf on f.fileid = lf.fileid ";
        break;
    case 3:
        $join = " right join acc_files af on f.fileid = af.fileid ";
        break;    
    case 4:
        $join = " right join training_files tf on f.fileid = tf.fileid ";
        break;
    case 5:
        $join = " JOIN task_files AS tf
                  ON f.fileid = tf.fileid
                JOIN accounttask AS act
                  ON tf.accounttaskid = act.accounttaskid
                JOIN taskcategory AS tc
                  ON act.categoryid = tc.categoryid ";
        break;
     
  }
  
  $view_permissions = getDocViewPermissions();	
    
	$qry = "SELECT  ".implode(', ', $select_array); 
        $qry .= " FROM files f ";
        $qry .= " left join doc_file_cat fc ";
        $qry .= " on f.fileid = fc.fileid ";
	$qry .= " left join doc_file_tags ft ";
        $qry .= " on f.fileid = ft.fileid ";
        $qry .= $join;
        $qry .= " LEFT JOIN account AS a ";
        $qry .= " ON f.accountid = a.accountid ";
        $qry .= " where f.filetype != 'handsamDoc' ";
        $qry .= " and f.mid is null ";
	$qry .= " $file_cat_qry $filter_qry $allowed_tags_qry $view_permissions ";
        $qry .= " group by f.created_by, f.accountid, f.title, f.description, f.edited, f.created, f.fileid ";
        $qry .= " order by f.title";
        
        $qry .= "; -- getDocList()"; // for the sql debug printout
	
	//echo "<br/>".$filter_qry;
	$pageDb = new pageQry($con, $qry);
	$pageDb->rsList();
	return $pageDb;
}

// @todo check these permissions!
function getDocViewPermissions() {
  global $clsUS;
  
  if(isAllowed('editDocLib'))
      return ""; // \r\nAND f.filetype = 'adminDoc' -- is allowed edit doc lib \r\n"; // AND f.filetype = 'adminDoc'"; // needed?
      
  if (isAllowed('viewDocument')) 
  {
    return " and f.accountid = {$clsUS->accountid} ";
  }
  //return " and f.accountid = {$clsUS->accountid} and (f.created_by = {$clsUS->userid} or f.filetype = 'adminDoc') ";
  return " and f.accountid = {$clsUS->accountid} and f.created_by = {$clsUS->userid} ";
}

/**
 * Permits user that can editDocLib OR (editDocument and same accountid) OR (editMyDocument and is created by user)
 * 
 * @global type $clsUS
 * @param type $created_by
 * @param type $accountid
 * @return boolean
 */
function isAllowedToEditDoc($created_by, $accountid)
{
  global $clsUS;
  if(isAllowed("editDocLib")) return true;
  if(isAllowed("editDocument") && $accountid == $clsUS->accountid) return true;
  if (isAllowed("editMyDocument") && $created_by == $clsUS->userid) return true;
  return false;
}

function isAllowedToViewDoc($created_by, $accountid, $filetype="")
{
  global $clsUS;
  if (isAllowed('editAccount')) return true;
  if(isAllowed("viewDocument") && $accountid == $clsUS->accountid) return true;
  if(isAllowed("editDocument") && $accountid == $clsUS->accountid) return true;
  if (isAllowed("viewMyDocument") && $created_by == $clsUS->userid ) return true;
  if (isAllowed("editMyDocument") && $created_by == $clsUS->userid ) return true;
  //if (isAllowed("viewMyDocument") && $accountid == $clsUS->accountid && $filetype == 'adminDoc' ) return true; 
  //if (isAllowed("editMyDocument") && $accountid == $clsUS->accountid && $filetype == 'adminDoc' ) return true; 
  return false;
}


function arrEmpty($var) {
	if (trim($var) == "") return $var;
}


function addDocCats($pageDb, $con) {
    for($i=0; $i < count($pageDb->rows); $i++) {
        $pageDb->rows[$i]['cats'] = array_flip(getFileCatsArr($con, $pageDb->rows[$i]['fileid']));
    }
    return $pageDb;
}

/*
 * checks if a file is used in a key task
 */

function isKeyTaskFile($fileid, $con)
{
  $qry = " SELECT t.accounttaskid FROM accounttask t LEFT JOIN task_files tf ON t.accounttaskid = tf.accounttaskid where tf.fileid = $fileid and t.key_task = 1 ";
  $rs = getRS($con, $qry);
  
  if (getResultRows($rs) > 0) 
  {
    return true;
  }
  return false;
}
?>