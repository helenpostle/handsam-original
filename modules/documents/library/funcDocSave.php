<?php
/* file upload function */

function docUpload($clsUS, $pageVars, $con, $to_file_path, $row)
{
    global $messages;

    if($pageVars->fileid == 0 && !isAllowed('editDocument'))
        $to_file_path = getUploadPath('userDoc');
        //echo $to_file_path;
    //where is the file?  
    if(isset($_POST['ft1']))
    {
        $current_file_path = getUploadPath($_POST['ft1']);
    }
    else if($row['filetype'])
    {
        $current_file_path = getUploadPath($row['filetype']);
    }
    else
    {
        $current_file_path = $to_file_path;
    }

    //where are we going to put the new file?
    if(isset($_POST['filetype']))
    {
        $new_file_path = getUploadPath($_POST['filetype']);
    }
    else if($row['filetype'])
    {
        $new_file_path = getUploadPath($row['filetype']);
    }
    else
    {
        $new_file_path = $to_file_path;
    }

    if($new_file_path != $current_file_path)
    {
        //get filename
        if(isset($_POST['uploaded_filename']))
        {
            $fn = $_POST['uploaded_filename'];
        }
        else
        {
            $fn = $row['filename'];
        }
        //move the file
        if($fn && file_exists($current_file_path . $fn) && copy($current_file_path . $fn, $new_file_path . $fn))
        {
            if(unlink($current_file_path . $fn))
                $messages[] = "$fn File moved";
        }
    }

    #get filename and previously uploaded filename from form
    $filename = $_FILES['filename']['name'];
    $uploaded_filename = $_POST['uploaded_filename'];
    if($filename != "")
    {
        #if filename not empty then upload file
        $file_path = $_FILES['filename']['tmp_name'];
        //die($file_path);
        $upload_file = uploadFile($filename, $file_path, $new_file_path);

        if($upload_file)
        {
            //die();
            #if upload successful check for previously uploaded filename
            if($uploaded_filename != "" && $uploaded_filename != $upload_file)
            {
                #delete this 
                $deleteFile = deleteFile($uploaded_filename, $new_file_path);
            }
            #now set previously uploaded filename as this uploaded filename - incase form doesn't validate then user doesn't have to re-select a new file
            $filename = $upload_file;
        }
        else
        {
            $messages[] = ERROR_FILE_UPLOAD;
            return false;
        }
    }
    else if($uploaded_filename != "")
    {
        $filename = $uploaded_filename;
    }
    
    if($pageVars->fileid == 0)
    {
        $qry = new dbInsert("files");
        if(!isAllowed('editDocument'))
        {
            $qry->setReqStringVal("filetype", 'userDoc', "Access");
        } 
        else 
        {
            $qry->setReqStringVal("filetype", "adminDoc", "Access");
        }
    }
    else
    {
        $qry = new dbUpdate("files");
        $qry->setParam("fileid", $pageVars->fileid);
        $qry->setParamNotEqual("filetype", "'handsamDoc'");
        $qry->setNullParam("mid");
    }

    /*
      if(isAllowed('editDocument') && isset($_POST["filetype"]) && ($row['filetype'] == 'userDoc' || $row['filetype'] == 'adminDoc' || $row['filetype'] == false))
      {
      $qry->setReqStringVal("filetype",$_POST["filetype"],"Access");
      }
     */
    if(isset($qry))
    {
        if(!isset($_POST['catid']))
        {
            $qry->failError .= "Please select at least one category.\r\n";
        }
        $qry->setReqStringVal("filename", $filename, "filename");
        $qry->setReqStringVal("title", $_POST["title"], "Title");
        $qry->setStringVal("description", $_POST["description"], "Description");
        $qry->setStringVal("extra_text", $_POST["extra_text"], "Location");
        
        $qry->setReqStringVal("filecategory", "DOCUMENT", "file category");
        $qry->setReqNumberVal("accountid", $_POST["accountid"], "Account id");
        $qry->setAudit($clsUS->userid);
        //echo $qry->getSql();
        if($qry->execute($con))
        {
            if($pageVars->fileid == 0)
            {
                $pageVars->fileid = $qry->getNewID();
            }
            $messages[] = SAVED;
            return $pageVars->fileid;
        }
        else
        {
            $messages[] = $qry->getError();
            //echo $qry->getError();
            return $filename;
        }
    }
    return false;
}

/**
 * Attempt to delete the file, then from the DB
 * 
 * @global type $messages
 * @param type $pageVars
 * @param type $con
 * @param type $to_file_path
 * @return boolean
 */
function docDelete($pageVars, $con, $to_file_path) 
{
	global $messages;
	
	//$fileid = 0;
	if(deleteFile($_POST['uploaded_filename'], $to_file_path)) {
		
		$messages[] = DELETED;
                
                $qry = new dbDelete("files");
                $qry->setParam("fileid",$pageVars->fileid);
                if (!$qry->execute($con)) {
                        $messages[] = $qry->getError();
                        return false;
                }
                
		return $pageVars->fileid;
		

	} else {
		$messages[] = ERROR_FILE_DELETE;
		return false;
	}			

}

/**
 * The number of times a file has been attached to lotc_files, task_files, content_files, training_files and acc_files
 * 
 * @param int $fileid
 * @param obj $con db
 * @return int
 */
function countFileUsed($fileid, $con)
{
    global $clsUS;
    
    $file_tables = array('lof' => 'lotc_files', 'alof' => 'a_lotc_files', 'taf' => 'task_files', 'cof' => 'content_files', 'trf' => 'training_files', 'acf' => 'acc_files');
    //construct query
    $ft_joinqry = array();
    $ft_count = array();
    foreach($file_tables as $key => $val)
    {
        $ft_joinqry[] = " left join $val $key on f.fileid = $key.fileid ";
        $ft_count[] = " count($key.fileid) ";
    }
    
    $qry = "select lof.lotc_fileid, alof.lotc_fileid AS a_lotc_fileid, taskfileid, pagefileid, training_fileid, acc_fileid, (" . implode('+', $ft_count) . ") as cnt ";
    $qry .= " FROM files f " . implode(' ', $ft_joinqry) . " where f.fileid = $fileid ";
        
    if(!isAllowed("editAccount")) $qry .= " AND f.accountid = ".$clsUS->accountid;
    
    $qry .= "; -- countFileUsed()";
    
    $rs = getRS($con, $qry);
    $row = getRow($rs);
    return $row;
}

function deleteFromObj($fileid, $con) {
    global $messages;
    $file_tables = array('lotc files'=>'lotc_files', 'task files'=>'task_files', 'content files'=>'content_files', 'training files'=>'training_files', 'incident files'=>'acc_files', 'file tags'=>'doc_file_tags', 'file categories'=>'doc_file_cat');
    foreach ($file_tables as $key => $ft)
    {
      $qry = dbDelete($ft);
      $qry->setParam("fileid",$fileid);
      if (!$qry->execute($con)) {
          $messages[] = $qry->getError();
      } else {
          $messages[] = "Deleted from $key";
      }
    }
}


/*
* function do decide where to upload file to
*/
 function getUploadPath($filetype)
 {
   global $userdocs_fp, $admindocs_fp, $accountLotcDocs_fp, $accountLotcUserDocs_fp;
   global $accountIncidentDocs_fp, $accountdocs_fp, $accountTrainingDocs_fp;

   switch ($filetype)
   {
     case 'userDoc':
       return $userdocs_fp;
       break;
     case 'adminDoc':
       return $admindocs_fp;
       break;
     case 'accountIncidentDoc':
       return $accountIncidentDocs_fp;
       break;
     case 'accountLotcUserDoc':
       return $accountLotcDocs_fp;
       break;
     case 'accountTrainingDoc':
       return $accountTrainingDocs_fp;
       break;
     default:
       return $accountdocs_fp;
   }
 }
 
// usage of files in the lotc module
// must return id, text, link, rowclass as array keys
function lotcFileUse($id)
{   
    global $clsUs, $con;
    $sql = "SELECT lf.fileid AS id, 'LOtC: Account Resources' AS text, ";
    
    if(isAllowed("editVisits")) 
        $sql .= "'/modules/lotc/a_lotc_guidance.php'";
    else
        $sql = "'' ";
        
    $sql .= "AS link 
            FROM lotc_files AS lf 
            JOIN files AS f 
                ON lf. fileid = f.fileid
            WHERE lf.fileid = ".$id;
    if(!isAllowed("editAccount")) $sql .= "AND lf.accountid = ".$clsUs->accountid;
    $sql .= "; -- lotcFileUse()"; // debugging
    $rs = getRS($con, $sql);
    
    $results = array();
    while($row = getRowAssoc($rs))
        $results[] = $row;
        
    return $results;
}


// usage of account files in the lotc module
// must return id, text, link, rowclass as array keys
function alotcFileUse($id)
{   
    global $con;
    global $clsUs;
    $sql = "SELECT lf.fileid AS id, 'LOtC: Account Resources' AS text, ";
    
    if(isAllowed("editVisits")) 
        $sql .= "'/modules/lotc/a_resource_docs.php'";
    else
        $sql = "'' ";
        
    $sql .= "AS link 
            FROM a_lotc_files AS lf 
            JOIN files AS f 
                ON lf. fileid = f.fileid
            WHERE lf.fileid = ".$id;
    if(!isAllowed("editAccount")) $sql .= "AND lf.accountid = ".$clsUs->accountid;
    $sql .= "; -- lotcFileUse()"; // debugging
    $rs = getRS($con, $sql);
    
    $results = array();
    while($row = getRowAssoc($rs))
        $results[] = $row;
        
    return $results;
}

// returns an array of Account Tasks which uses the current file
// must return id, text, link, rowclass as array keys
function taskFileUse($id)
{
    global $con, $clsUS;
    
    $sql = "SELECT act.accounttaskid AS id, act.customtext AS text, ";
    $sql .= "(CASE WHEN act.key_task = 1 THEN 'highlight' ELSE '' END) AS rowclass, ";
    
    if(isAllowed('editTask') || isAllowed('editOurTasks')) {
        $sql .= "CONCAT('/modules/tasklist/tasks.php?accounttaskid=', act.accounttaskid) ";
    } else if(isAllowed('myTasks')) {
        $sql .= " (CASE WHEN act.assignedto = ".$clsUS->userid." THEN  CONCAT('/modules/tasklist/mytasks.php?accounttaskid=', act.accounttaskid) END ) ";
    } else {
        $sql .= "'' ";
    }

    $sql .= "AS link 
        FROM accounttask AS act 
        JOIN task_files AS tf
            ON act.accounttaskid = tf.accounttaskid 
        WHERE tf.fileid = ".$id;
    if (!isAllowed("editAccount")) $sql .= " AND act.accountid = $clsUS->accountid ";
    $sql .= '; -- taskFileUse()'; // debugging
    
    $rs = getRS($con, $sql);
    $results = array();
    while($row = getRowAssoc($rs))
    {
        $results[] = $row;
    }
    return $results;
}

// use of a file in the accident/incident module
// must return id, text, link, rowclass as array keys
function accFileUse($id)
{
    global $con;
    global $clsUs;
    $sql = "SELECT a.accidentid AS id, ";
    
    if(isAllowed('adminAccidents'))
        $sql .= "CONCAT('/modules/accidents/accident.php?accountid=1&accidentid=', a.accidentid)";
    else
        $sql .= "''";
    
    $sql .= "AS link, CONCAT('Accident Report: ', a.accident_desc) AS text 
            FROM acc_files AS af
            JOIN acc_accident AS a
                ON af.accidentid = a.accidentid
            WHERE af.fileid = ".$id." ";
    if (!isAllowed("editAccount")) $sql .= "AND a.accountid = $clsUS->accountid ";
    
    $sql .= "; -- accFileUse() ";
    $rs = getRS($con, $sql);
    $results = array();
    while($row = getRowAssoc($rs))
    {
        $results[] = $row;
    }
    return $results;
}

// use of a file in the training module
// must return id, text, link as array keys
function trainFileUse($id)
{
    global $con;
    global $clsUs;
    $sql = "SELECT tf.training_id AS id, tc.title AS text,";
    
    if(isAllowed('recordTraining'))
        $sql .= "CONCAT('Training Course: ', '/modules/training/training.php?accountid=1&training_id=', tl.training_id) ";
    else
        $sql .= "'' ";
    
    $sql .= "AS link
            FROM training_files AS tf
            JOIN training_log AS tl
                ON tf.training_id = tl.training_id
            JOIN training_course AS tc
                ON tl.course_id = tc.course_id
            WHERE tf.fileid = ".$id." ";
    if (!isAllowed("editAccount")) $sql .= "AND tl.accountid = $clsUS->accountid ";
    
    $sql .= "; -- trainingFileUse()";
    $rs = getRS($con, $sql);
    $results = array();
    while($row = getRowAssoc($rs))
    {
        $results[] = $row;
    }
    return $results;
}


// use of file in the content/page module
// must return id, text, link, rowclass as array keys
function contentFileUse($id)
{
    return false;
    global $con;
    $sql = "SELECT * FROM content_files WHERE pagefileid = ".$id.";";
    $rs = getRS($con, $sql);
    $results = array();
    while($row = getRowAssoc($rs))
    {
        $results[] = $row;
    }
    return $results;
}