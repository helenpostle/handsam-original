<?php
/* * *
 * * Shows the list of task categories in the system
 * * 
 * * 
 * * */
echo displayTitle2($filter_title_str);

if($pageDb->rsExists())
{
    ?>
    <table class="acc_list">
        <thead>
            <tr>
            <?php
            if (isAllowed("editDocLib"))
            {
            ?>
                <th class="start ">Account</th>
                <?php if(isset($pageDb->rows[0]['customtext'])): ?>
                    <th class="">Task Category</th>
                <?php endif; ?>
                <th class="">Location</th>
            <?php
            }
            else
            {
              if(isset($pageDb->rows[0]['customtext'])) 
              {
                  echo "<th class=\"start\">Task Category</th>";
                  echo "<th class=\"\">Location</th>";
              } 
              else
              {
                  echo "<th class=\"start\">Location</th>";
              }
            }
            ?>
            <th class="">Document Date</th>
            <th class="end">Year</th>
            <?php // <th class="end ">Categories</th> ?>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach($pageDb->rows as $row)
            {
                $tr_class = "";
                $tr_title = '';
                if (isKeyTaskFile($row['fileid'], $con) == true) 
                {
                    $tr_class = "highlight";
                    $tr_title = "This file is attached to a Key Task";
                }
                if(isAllowed('editDocument') || isAllowedToEditDoc($row['created_by'], $row['accountid']))
                {
                    $url = "$rootUrl/modules/documents/upload.php?fileid={$row['fileid']}&amp;rand=" . rand() . "&amp;filter=" . rawurlencode(serialize($qs));
                }
                else
                {
                    $url = "download.php?id={$row['fileid']}";
                }
                ?>
                <tr class="<?php echo $tr_class;?>" title="<?=$tr_title?>">
                    <?php
                    if (isAllowed("editDocLib"))
                    {
                    ?>
                        <td class="start catname"><a href="<?php echo $url; ?>" class="downloadfile"><?php echo $row["cost_code"]; ?></a></td>
                        <?php if(isset($row['customtext']))
                        {
                        ?>
                        <td><a href="<?php echo $url; ?>" class=""><?php echo $row["customtext"]; ?></a></td>
                        <?php
                        }
                        ?>
                        <td><a href="<?php echo $url; ?>" class=""><?php echo $row["extra_text"] ?></a></td>
                    <?php
                    }
                    else
                    {
                        if(isset($row['customtext']))
                        {
                        ?>
                        <td class="start"><a href="<?php echo $url; ?>"><?php echo $row["customtext"]; ?></a></td>
                        <td><a href="<?php echo $url; ?>" class=""><?php echo $row["extra_text"] ?></a></td>
                        <?php
                        }
                        else
                        {
                            ?>
                            <td class="start"><a href="<?php echo $url; ?>"><?php echo $row["extra_text"] ?></a></td>
                            <?php
                        }
                     
                    }
					?>
                    <td><a href="<?php echo $url; ?>" class=""><?php echo $row["description"] ?></a></td>
                    <td class="end state"><a href="<?php echo $url; ?>"><?php echo $row["title"] ?></a></td>
                    <?php // <td class="end state"><a href=""><?php //echo $row['category']; </a></td> ?>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="start"></td>
                <?php if(isset($pageDb->rows[0]['customtext'])): ?>
                    <td class=""></td>
                <?php endif; ?>
                <?php
				if (isAllowed("editDocLib"))
				{
					?>
					<td class=""></td>
					<?php
				}
				?>
				<td class=""></td>
                <td class="end"></td>
            </tr>
        </tfoot>

    </table>
<?php
}
else
{
    echo '<p>No results...</p>';
}