<?php

/**
 * Shows a  form for uploading a file
 * 
 */
if(isAllowed("editDocument") || isAllowed("editMyDocument"))
{
    if(!isset($uploaded_filename) || $uploaded_filename == "")
    {
        $uploaded_filename = $pageDb->row['filename'];
    }
    echo startFileFormTable($_SERVER["PHP_SELF"], "clearfix", "Upload a Document", UPLOAD_FORM_INFO);

    echo frmHiddenField(htmlspecialchars(serialize($qs_base)), "filter");
    echo frmHiddenField($pageVars->fileid, "fileid");
    echo frmHiddenField($uploaded_filename, "uploaded_filename");
    echo frmHiddenField(20000000, "MAX_FILE_SIZE");
    
    echo frmFileField("filename", "Select File", true);
    
    // tie the document to an account.
    // if a super admin, then give a choice of accounts, 
    // otherwise, it will be the Admin/User's own school account
    // HAND-938
    if(!isAllowed('editDocLib'))
        echo frmHiddenField($pageVars->accountid, "accountid");
    else
    {
        $sql = "SELECT accountid, cost_code FROM account WHERE cost_code IS NOT NULL ORDER BY cost_code ASC;";
        $rs = getRecSet($sql);
        $recs = array();
        while($row = getRowAssoc($rs))
            $recs[$row['accountid']] = $row['cost_code'];
        //var d
        echo frmSelectArray($pageDb->row, 'accountid', $recs, 'Account', true);
    }
    
    if(isset($uploaded_filename) || $uploaded_filename != "")
    {
        echo "<div class=\"clearfix optional frm_row\"><span class=\"label\">Uploaded Filename</span><span class=\"value\"><span class=\"upload\"><a href=\"download.php?id=" . $pageVars->fileid . "\" title=\"\">" . $uploaded_filename . "</a></span></span></div>";
    }

    echo frmHiddenField($uploaded_filename, 'uploaded_filename');
    
    echo frmTextField($pageDb->row, "title", 100, "Year", true);
    echo frmTextArea($pageDb->row, "description", 5, "Document Date (Date Document Completed)", false);
    echo frmTextArea($pageDb->row, "extra_text", 5, "Location", false);
    /*
     //remove this as its confusing all docs uploaded from here will be admin docs
    if (isAllowed("editDocument") && ($pageDb->row['filetype'] == 'userDoc' || $pageDb->row['filetype'] == 'adminDoc' || $pageDb->row['filetype'] == false))
    {
      echo frmExSelectArray($pageDb->row, "", "filetype" , array("adminDoc" => "All Account Users", "userDoc" => "Account Admin Users &amp; Author Only"), "Access" , true, "Who will be able to access this file?");
      if (isset($_POST['filetype'])) echo frmHiddenField($_POST['filetype'], 'ft1');
    }
     
     */
    //category check boxes
    echo "<div id=\"doc_cats\" class=\"frmSection clearfix\">";
    echo "<h3>Document Categories</h3>";
    echo displayParagraphs("You must select at least one category below:");

    $cat_qry = "SELECT catid, cat From doc_cat ORDER BY cat;";
    $cat_rs = getRS($con, $cat_qry);
    $cat_str = "";
    while ($cat_row = getRow($cat_rs))
    {
        $cat_str .= "<div class=\"optional clearfix\"><span class=\"label\">{$cat_row['cat']}</span><input class=\"inputCheckbox\" type=\"checkbox\" name=\"catid[]\"  value=\"{$cat_row['catid']}\"";
        if(is_array($file_cats) && in_array($cat_row['catid'], $file_cats))
            $cat_str .= " checked";
        $cat_str .= "/></div>";
    }

    echo $cat_str;
    echo "</div>";
    
    echo frmTextAreaNonDb("tags", 4, "Tags", false, $tags_str);
    
    // file usage
    if($countFileUsed['cnt'] > 0):
    ?>
    <div class="frm_row clearfix">
        <h2>File Usage</h2>
        <table class="acc_list">
            <tr>
                <th>Name</th>
                <th>Module</th>
            </tr>
        <?php
            foreach($fileUsage as $file):
                foreach($file['file'] as $f):
        ?>
                <tr class="<?php if(isset($f['rowclass'])) echo $f['rowclass']; ?>">
                    <td>
                    <?php if($f['link'] != ''): ?>
                        <a href="<?php echo $rootUrl.$f['link'] ?>"><?php echo $f['text']; ?></a>
                    <?php else: ?>
                        <?php echo $f['text']; ?>
                    <?php endif; ?>
                    </td>
                    <td><?php echo $file['title']; ?></td>
                </tr>
            <?php endforeach; ?>
        <?php endforeach; ?>
        </table>
    </div>
    <?php
    endif;
    
    echo frmShowAudit($pageDb->row, $con);
    echo frmButtonHelp("Save", "save", "Click here to save changes");
    echo frmButtonHelp("Cancel", "cancel", "Click here to cancel upload");
    
    if($pageVars->fileid != 0 && $countFileUsed['cnt'] == 0)
        echo frmButtonConfirmHelp("Delete", "delete", "Are you sure you want to delete this file?", "Click here to delete this file");

    echo endFormTable();
}
