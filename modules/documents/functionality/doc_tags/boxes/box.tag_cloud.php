<?php

/**
 * box showing all links tags for link editing pages
 */
#get max number of tags for one item
$qryList = "SELECT count(*) AS cnt FROM doc_tags 
            INNER JOIN doc_file_tags 
                ON doc_tags.tagid = doc_file_tags.tagid  
            GROUP BY doc_tags.tagid ORDER BY cnt DESC LIMIT 0,1";
$resultList = getRS($con, $qryList);
$row = getRow($resultList);
$max = $row['cnt'];
$class_size = $max / 6;

$qryList = "SELECT doc_tags.tag, doc_tags.tagid, count(*) AS cnt 
            FROM doc_tags 
            INNER JOIN doc_file_tags 
                ON doc_tags.tagid = doc_file_tags.tagid 
            GROUP BY doc_tags.tag, doc_tags.tagid 
            ORDER BY tag ASC";
$resultList = getRS($con, $qryList);

if(getResultRows($resultList) > 0)
{
    accidentBoxTop("Tags");
	echo "<p>Remember, you may only remove tags from a file if you originally added them.</p>";
    echo "<div class=\"all_tags\">&gt;&gt; All Tags</div>";
    echo "<ul id=\"tag_cloud\">";
    while ($rowList = getRow($resultList))
    {
        $class = $rowList['cnt'] / $class_size;
        echo "<li class=\"size$class\">";
        echo displayText($rowList['tag']);
        echo "</li>";
    }
    echo "</ul>";

    accidentBoxBottom();
}
