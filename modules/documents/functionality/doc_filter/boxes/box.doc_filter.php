<?php

/**
 * Filter box
 * 
 */
if(isAllowed("viewMyDocument") || isAllowed("viewDocument") || isAllowed('editDocument'))
{
    accidentBoxTop("Document Filter");

    $filter_array = array();

    //get array of categories
    $cat_arr = array();
    
    //just get categories that are used in licenced for tags
    $cat_arr_qry = "SELECT c.cat, c.catid FROM doc_cat AS c 
                    LEFT JOIN doc_file_cat AS fc 
                        ON c.catid = fc.catid 
                    LEFT JOIN doc_file_tags AS ft 
                        ON ft.fileid = fc.fileid 
                    WHERE fc.catid > 0 
                    GROUP BY c.cat, c.catid;";
    
    $cat_arr_rs = getRS($con, $cat_arr_qry);

    while ($cat_arr_row = getRow($cat_arr_rs))
    {
        $cat_arr[$cat_arr_row['catid']] = $cat_arr_row['cat'];
    }

    $filter_array['category'] = $cat_arr;
    
    // this is an option,
    // and user must have editDocLib permissions 
    if($documents_conf['task_costcode_filter'] && isAllowed('editDocLib'))
    {
        /**
         * Filter by cost code. 
         * 
         * As this is an account field, it's basically filtering by account. 
         * Personally, I think that showing the account name is much better for 
         * the user. 
         */
        $sql = "SELECT a.accountid, a.cost_code 
                FROM files AS f 
                JOIN account AS a
                    ON f.accountid = a.accountid
                WHERE a.cost_code IS NOT NULL 
                AND f.filetype != 'handsamDoc' 
                AND f.mid IS NULL 
                GROUP BY a.accountid 
                ORDER BY a.cost_code ASC
            ; -- filter by account.cost_code";
        
        $res = getRecSet($sql);
        while($row = getRowAssoc($res))
            $filter_array['Cost Codes'][$row['accountid']] = $row['cost_code'];
    }
    
    if($documents_conf['task_cat_filter'])
    {
        /**
         * offer all the Tasklist Categories from the tasklist which is
            a) assigned, and 
            b) active in this Account
         */
        $permissions = (isAllowed('editDocLib')) ? '' : ' AND f.accountid = '.$clsUS->accountid;

        // get tasklist licences and get active task categories that have docs attached
        $sql = "SELECT tc.categoryid, tc.customtext
                FROM files as f
                JOIN task_files AS tf
                  ON f.fileid = tf.fileid
                JOIN accounttask AS act
                  ON tf.accounttaskid = act.accounttaskid
                JOIN taskcategory AS tc
                  ON act.categoryid = tc.categoryid
                WHERE act.state <> 'DELETED'
                AND f.filetype != 'handsamDoc' 
                AND f.mid IS NULL 
                AND tc.state = 'ACTIVE'
                $permissions
                GROUP BY tc.categoryid
        ; -- tasklist category filter ";

        $rs = getRecSet($sql);
        while($row = getRowAssoc($rs))        
            $filter_array['Task Category'][$row['categoryid']] = $row['customtext'];
   
    }
    
    //get array of users only if admin
    if($documents_conf['author_filter'] && (isAllowed("viewMyDocument") || isAllowed("viewDocument")))
    {
      $qry = "select usr.userid as a, Concat(firstname, ' ', lastname) as b from usr right join files on usr.userid = files.created_by where  usr.accountid = ".$clsUS->accountid." order by lastname";
      $rs = getRs($con, $qry);
      while ($row = getRow($rs))
      {
        $filter_array['author'][$row['a']] = $row['b'];
      }
    }

    if($documents_conf['module_filter'])
    {
    //module file is attached to filter
        if (isset($installed_modules[1])) $filter_array['module'][1] = "Tasklist";
        if (isset($installed_modules[2])) $filter_array['module'][2] = "Visits";
        if (isset($installed_modules[3])) $filter_array['module'][3] = "Incidents";
        if (isset($installed_modules[4])) $filter_array['module'][4] = "Training";
    }
    
    $selected = "";
    foreach($filter_array as $filter_name => $filter_name_array)
    {
		$label_name = $filter_name;
		if(strtolower($filter_name)=='cost codes')
		{
			$label_name = 'Account';
		}
        $qs = $qs_base;
        echo "<div>";
        
        $filter_name_nospace = str_replace(' ','', strtolower($filter_name));
        
        echo "<label for=\"$filter_name_nospace\">" . ucwords($label_name) . "</label>";

        echo '<select id="'.$filter_name_nospace.'" name="filter_'.$filter_name_nospace.'">';

        $qs[$filter_name] = 0;
        $filter_link = "?filter=" . rawurlencode(serialize($qs));
        
        if ((isset($filter_qry['search']) && isset($filter_qry['tag']) && $filter_qry['search'] == "" && $filter_qry['tag'] == "") 
                || (!isset($filter_qry['search']) && !isset($filter_qry['tag']))) 
        {
            $selected = "";
            if (isset($qs_base['category']) && $qs_base['category'] === 'ALL') $selected = "selected";
            $filter_link = "?filter=" . rawurlencode(serialize($qs));
            echo "<option $selected value=\"$filter_link\">Choose All</option>";
        }
        else
        {
            echo "<option value=\"$filter_link\">Choose All</option>";
        }
        
        foreach($filter_array[$filter_name] as $filter_key => $filter_value)
        {
            $selected = "";
            $qs[$filter_name] = $filter_key;
            if(isset($qs_base[$filter_name]) && strval($qs_base[$filter_name]) == strval($filter_key))
            {
                $selected = "selected";
                echo $filter_name . " : " . $qs_base[$filter_name] . " : " . $filter_key;
            }
            else
            {
                $selected = "";
            }
            $filter_link = "?filter=" . rawurlencode(serialize($qs));
            echo "<option $selected value=\"$filter_link\">$filter_value</option>";
        }
        echo "</select>";
        echo "</div>";
    }

    accidentBoxBottom();
}
