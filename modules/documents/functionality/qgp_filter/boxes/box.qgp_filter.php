<?php

/**
 * Filter box
 * 
 */
if(isAllowed("editDocument") || isAllowed("viewDocument"))
{
    accidentBoxTop("Document Filter");

    $filter_array = array();

    //get array of categories
    $cat_arr = array();
    if(isAllowed("editDocument"))
    {
        $cat_arr_qry = "SELECT c.cat, c.catid FROM qgp_cat AS c 
                        LEFT JOIN qgp_file_cat fc ON c.catid = fc.catid 
                        WHERE fc.catid > 0 
                        GROUP BY c.cat, c.catid;";
    }
    else if(isAllowed("viewDocument"))
    {
        //just get categories that are used in licenced for tags
        $cat_arr_qry = "SELECT c.cat, c.catid FROM qgp_cat AS c 
                        LEFT JOIN qgp_file_cat AS fc 
                            ON c.catid = fc.catid 
                        LEFT JOIN qgp_file_tags AS ft 
                            ON ft.fileid = fc.fileid 
                        WHERE fc.catid > 0 
                        GROUP BY c.cat, c.catid;";
    }

    $cat_arr_rs = getRS($con, $cat_arr_qry);

    while ($cat_arr_row = getRow($cat_arr_rs))
    {
        $cat_arr[$cat_arr_row['catid']] = $cat_arr_row['cat'];
    }

    $filter_array['category'] = $cat_arr;

    $selected = "";
    foreach($filter_array as $filter_name => $filter_name_array)
    {
        $qs = $qs_base;
        echo "<div>";

        echo "<label for=\"$filter_name\">" . ucwords($filter_name) . "</label>";

        echo "<select id=\"$filter_name\">";

        $qs[$filter_name] = 0;
        $filter_link = "?filter=" . rawurlencode(serialize($qs));
        if ((isset($filter_qry['search']) && isset($filter_qry['tag']) && $filter_qry['search'] == "" && $filter_qry['tag'] == "") || (!isset($filter_qry['search']) && !isset($filter_qry['tag']))) {
            echo "<option value=\"$filter_link\">Choose None</option>";
            $selected = "";
            if ($qs_base['category'] === 'ALL') $selected = "selected";
            $qs['category'] = "ALL";
            $filter_link = "?filter=" . rawurlencode(serialize($qs));
            echo "<option $selected value=\"$filter_link\">Choose All</option>";
        } else {
            echo "<option value=\"$filter_link\">Choose All</option>";
        }
        
        foreach($filter_array[$filter_name] as $filter_key => $filter_value)
        {
            $selected = "";
            $qs[$filter_name] = $filter_key;
            if(isset($qs_base[$filter_name]) && strval($qs_base[$filter_name]) == strval($filter_key))
            {
                $selected = "selected";
                echo $filter_name . " : " . $qs_base[$filter_name] . " : " . $filter_key;
            }
            else
            {
                $selected = "";
            }
            $filter_link = "?filter=" . rawurlencode(serialize($qs));
            echo "<option $selected value=\"$filter_link\">$filter_value</option>";
        }
        echo "</select>";
        echo "</div>";
    }

    accidentBoxBottom();
}
