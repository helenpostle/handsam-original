$(document).ready( function() {
    $('#category').change(function(event){
        window.location = $(this).val();
    });
	
    $('#tag_cloud li').click(function(event){
        var url = $(this).find('input').val();
        window.location = url;
    });
	
    $('#clear_tag').click(function(event){
        var url = $(this).find('input').val();
        window.location = url;
    });	
	
    $('#clear_search').click(function(event){
        var url = $(this).find('input').val();
        window.location = url;
    });	

    $("#qgp_search").bind("keydown", function(event) {
        // track enter key
        var keycode = (event.keyCode ? event.keyCode : (event.which ? event.which : event.charCode));
        if (keycode == 13) { // keycode for enter key
            // force the 'Enter Key' to implicitly click the Update button
            document.getElementById('search').click();
            return false;
        } else  {
            return true;
        }
    }); // end of function


});