<?php

/**
 * Securely Download a file from the database
 */
//MUST ADD SECURITUY AND CHECK FILE EXISTS
$content = "";
//make this a secure page
$secure = true;
//include basic stuff
include ("../../shared/startPage.php");
$pageFiles->addModFunc("doc_list");
$pageFiles->addModFunc("doc_upload");
$pageFiles->includePhp();
//specify page content
$title = "Download a Library File";

$pageVars = new loadVars($clsUS, $con);

//get the id 
$id = getIntFromRequest("id");
$rs = getRS($con, "SELECT * FROM files WHERE fileid = $id");
$rowFile = getRow($rs);


if($rowFile && isAllowedToViewDoc($rowFile['created_by'], $rowFile['accountid'], $rowFile['filetype'])) 
{
    //download the file
    $filename = htmlspecialchars_decode($rowFile["filename"]);
    $path = getUploadPath($rowFile['filetype']);

    $file = $path . $filename;
    
    // send the right headers
    header("Content-Type: application/octet-stream");
    header('Content-Disposition: attachment; filename="' . $filename . '"');

    echo file_get_contents($file);
    exit;

}
else
{
    // file not available, 404
    header("HTTP/1.0 404 Not Found");
    exit;
}