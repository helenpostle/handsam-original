<?php
/**
* attach item - file or link to web page
*/


//MUST ADD SECURITUY AND CHECK FILE EXISTS
$content = "";
//include basic stuff

//make this a secure page
$secure = false;

include ("../../shared/startPage.php");


$pageFiles->includePhp();

//get the id 
$contentid = getIntFromRequest("contentid");
$itemid = getIntFromRequest("itemid");
$type = getStrFromRequest("type");

if (isAllowed("editContent")) {

	if ($type == "file") {
		$qry = new dbInsert("content_files");
		$qry->setReqNumberVal("fileid",$itemid,"file Id");
		$qry->setReqNumberVal("contentid",$contentid,"Content Id");
		$qry->setAudit($clsUS->userid);
		if ($qry->execute($con)) {
			echo "File added to page";
		} else {
			echo "Error adding file: ".$qry->getSql();
		}
	
	} else if($type == "link") {
	
		$qry = new dbInsert("content_links");
		$qry->setReqNumberVal("linkid",$itemid,"link Id");
		$qry->setReqNumberVal("contentid",$contentid,"Content Id");
		$qry->setAudit($clsUS->userid);
		if ($qry->execute($con)) {
			echo "Link added to page";
		} else {
			echo "Error adding link";
		}
	
	}

	
}