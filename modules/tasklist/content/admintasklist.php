<?php
/***
**  task list for super admin
** 
** 
***/
$taskListTable = "";
if ($multi == 'yes') {
	
	$taskListTable .= startFormTable($_SERVER["PHP_SELF"], "multiSelect", "", MULTISELECT_TITLE, ''); 
	
	$startDate = time();
	$taskListTable .= frmHiddenField($option,"option");
	$taskListTable .= frmHiddenField($month,"month");
	
	$taskListTable .= frmHiddenField($category,"category");
	$taskListTable .= frmHiddenField($multi,"multi");
	$taskListTable .= frmHiddenField($tasklistid,"tasklistid");
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
        
        // UI Tabs
	$taskListTable .= '<div id="multitabs"><ul style="list-style-image:none;margin:0px;"><li><a href="#multitab-1">Task Category</a></li><li><a href="#multitab-2">Month</a></li><li><a href="#multitab-3">Day Number</a></li><li><a href="#multitab-4">State</a></li></ul>';
        
        $taskListTable .= '<div id="multitab-1">';
        $taskListTable .= "<label class=\"labelCheckbox\"  for='editCategory'>Check to Edit</label><input  class='toggleInput checkbox' type='checkbox' name='editCategory' value='categoryid'/>";
	$taskListTable .= frmSelectQueryNonDb("categoryid","select categoryid as a, customtext as b from taskcategory",$con,"Task Category",false, "", "cover slim");
        $taskListTable .= '</div>';
        
        $taskListTable .= '<div id="multitab-2">';
	$taskListTable .= "<label class=\"labelCheckbox\" for='editMonth'>Check to Edit</label><input class='toggleInput checkbox' type='checkbox' name='editMonth' value='taskmonth'/>";
	$taskListTable .= frmSelectArrayNonDb("taskmonth" , $months, "Month" , false, "", "cover slim");
        $taskListTable .= '</div>';
        
        $taskListTable .= '<div id="multitab-3">';
	$taskListTable .= "<label class=\"labelCheckbox\"  for='editDay'>Check to Edit</label><input class='toggleInput checkbox' type='checkbox' name='editDay' value='taskday'/>";
	$taskListTable .= frmTextFieldNonDb("taskday",3,"Day Number",false, "", "", "cover slim");	
        $taskListTable .= '</div>';
        
        $taskListTable .= '<div id="multitab-4">';
        $taskListTable .= "<label class=\"labelCheckbox\"  for='editState'>Check to Edit</label><input class='toggleInput checkbox' type='checkbox' name='editState' value='state'/>";
	$taskListTable .= frmSelectArrayNonDb("state" , $state, "State", false, "", "cover slim");	
	
        $taskListTable .= '</div></div>';// #multitabs
        
        // select all
        $taskListTable .= "<div class=\"select_all clearfix\"><label id=\"select_all\" for=\"select_all\"class=\"last\">".SELECT_ALL."</label><input class=\"checkbox\" id=\"selectAllBoxes\" type=\"checkbox\" name=\"select_all\"/></div>";
	
        $taskListTable .= frmButtonConfirmHelp("Save","saveMulti", "Are you sure you wnt to change all of these tasks?", "Click here to save changes");
	$taskListTable .= frmButtonHelp("Reset","cancelMulti", "Click here to cancel");
	$taskListTable .= frmEndFormNow();
}

$taskListTable .= "<table class=\"acc_list\">";
$taskListTable .= "<caption>".$listTitle."</caption>";
$taskListTable .= "<thead>";
$taskListTable .= "<tr>";
$taskListTable .= "<th class=\"day nolink start\">Day</th>";
$taskListTable .= "<th class=\"tasktext nolink\">Task Description</th>";
if ($multi == 'yes') {
	//$taskListTable .= "<th class=\"state nolink\">User</th>";
	$taskListTable .= "<th class=\"nolink end\"></th>";
} else {
	//$taskListTable .= "<th class=\"state nolink\">User</th>";
	$taskListTable .= "<th class=\"nolink end\"></th>";

}	
$taskListTable .= "</tr>";
$taskListTable .= "</thead>";
$taskListTable .= "<tbody>";

$daynum = 0;
$rowClassOdd = "";
$rowcnt = 0;
$trid = "";
if ($month == 0) {
	$monthsNav = "<li class=\"active\">All months</li>";
} else {
	$monthsNav = "<li><a href=\"taskadmin.php?month=0&option=".$option."&category=".$category."&tasklistid=".$tasklistid."&multi=".$multi."\">All months</a></li>";
	}
//stuff for hilighting  alternate days in month and category view
while ($row = getRow($optionresult)) {
    $rowClassHighlight = "";
	#sort out action date into month year and day
	$actionmonth = $row["taskmonth"];
	$actionday = $row["taskday"];
	
		#set lastmonth for first loop											
		if ($rowcnt == 0) {
			$lastmonth = 0;
			$daynum = 0;
		}
		
		//changes class of alternate day rows in table ijnboth month and category view
		if ($daynum != $actionday && $rowcnt != 0) {
			if($rowClassOdd == "odd") {
				$rowClassOdd = "";
			} else {
				$rowClassOdd = "odd";
			}
		} 
		$daynum = $actionday;
		if ($lastmonth != $actionmonth && $month == 0) { 
			$taskListTable .=  "<tr id=\"month".$actionmonth."\"class=\"headingRow\"><td class=\"centre start end\" colspan=\"3\">".$months[$actionmonth]."</td></tr>";
		}
		
		if ($lastmonth != $actionmonth) {
			$monthLink = "taskadmin.php?multi=".$multi."&tasklistid=".$tasklistid."&option=".$option."&category=".$category."&month=";
			
			$monthsNav .= displayListLink($months[$actionmonth], $month, $actionmonth, $monthLink);
		}
		$lastmonth = $actionmonth;
		$rowcnt++;
        if ($row['key_task'] == 1) $rowClassHighlight = "highlight";
		if ($month == 0 || $actionmonth == $month) {
			$link1 = "taskadmin.php?taskid=".$row["taskid"]."&month=".$month."&tasklistid=".$tasklistid."&option=".$option."&category=".$category."&multi=".$multi;
			$taskListTable .= "<tr ".$trid." class=\"".$rowClassHighlight." ".$rowClassOdd."\">";
			$taskListTable .= "<td class=\"start\"><a href=\"".$link1."\">".$actionday."</a></td>";
			$taskListTable .= "<td class=\"flag\"><a href=\"".$link1."\">".showAbstract($row["customtext"], 100)."</a></td>";
			if ($multi == 'yes') {
				$taskListTable .= "<td class=\"end\">";
                if ($row['repeatingtaskid'] == null) {
                    $taskListTable .= "<input class=\"multiSelectCheck\" id='checkBox".$rowcnt."'type='checkbox' name='selectMulti[]' value='".$row["taskid"]."' />";
                }
                $taskListTable .= "</td>";
			} else {
				$taskListTable .= "<td class=\"end\"></td>";
			}
		 	$taskListTable .= "</tr>";
	 	}					

	} 
$taskListTable .= "</tbody>";
$taskListTable .= "<tfoot>";
$taskListTable .= "<tr>";
$taskListTable .= "<td class=\"start\"></td>";
$taskListTable .= "<td ></td>";
$taskListTable .= "<td class=\"end\"></td>";
$taskListTable .= "</tr>";
$taskListTable .= "</tfoot>";
$taskListTable .= "</table>";
if ($multi == 'yes') {
	$taskListTable .= endFormTable2();
}

echo $taskListTable;
