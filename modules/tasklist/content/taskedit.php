<?php 
/**
 * Shows a form for editing an accounttask
 * 
 */

#set page variables 
$deleteDoc = "";
$edit_form = true;

#stuff that doesn't go in a new custom task
if (!$newCustomTask) {
	$box[] = "box.tasksInCategory.php";
	echo "<h2>Category: ".$categoryname."</h2>";
	if ($rowAccountTask['state'] == "COMPLETED") {
		#so user can't edit form once it is completed - they can only change the state to UNCOMPLETE the task
		$edit_form = false;
	}	
}
 
#create users array to include 'no user'
$usersArray = array("No User");
$qry = "select usr.userid as a, CONCAT(usr.firstname,' ',usr.lastname) as b from  usr left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.accountid = $accountid and (state != 'DELETED' and state != 'INACTIVE') and usr_usrtype.tasklistid = $tasklistid order by usr.username asc";
$result = getRS($con, $qry);
while ($row = getRow($result)) {
	$usersArray[$row['a']] = $row['b'];
}

$frm_class = "";
$key_task_title = "";
if ($rowAccountTask['key_task'] == 1) {
    $frm_class = "key_task";
    $key_task_title = ": <span>THIS IS A KEY TASK</span>";
}

#start form
echo startFormTable($_SERVER["PHP_SELF"], $frm_class, "", TASK_FORM_TITLE.$key_task_title, TASK_FORM_INFO);
echo frmLegend('Edit Task');
//echo formTopNotes(TASK_FORM_TITLE, TASK_FORM_INFO);
echo frmHiddenField($accounttaskid,"accounttaskid");
echo frmHiddenField($option,"option");
echo frmHiddenField($month,"month");
echo frmHiddenField($year,"year");
echo frmHiddenField($user,"user");
echo frmHiddenField($group_id,"group_id");
echo frmHiddenField($return,"return");
echo frmHiddenField($multi,"multi");
echo frmHiddenField($licenceid,"licenceid");
echo frmHiddenField($task_search,"task_search");

if(buttonClicked('edit_repeat') && isset($accounttask) && $accounttask->accounttask_repeatingid > 0) {
    include ("$rootPath/modules/tasklist/content/taskedit_disabled.php");
    $edit_task = false;
} else {
    include ("$rootPath/modules/tasklist/content/taskedit_enabled.php");
    $edit_task = true;
}

/**
 * repeating tasks
 */
if(isset($accounttask) && $accounttask->accounttask_repeatingid > 0)
{
    if(!buttonClicked('edit_repeat')) {
        include ("$rootPath/modules/tasklist/content/taskrepeat_disabled.php");
    } 
    elseif(buttonClicked('edit_repeat')){
        include ("$rootPath/modules/tasklist/content/taskrepeat_enabled.php");
    }
}
elseif($tasklist_conf['create_repeat_accounttasks'] == true) // && isAllowed('editRepeatTask'))
{
    $RTask = new Repeatingtask($con, $clsUS);
    include ("$rootPath/modules/tasklist/content/taskrepeat_enabled.php");
}


if (!$newCustomTask && isAllowed("editTask")) {
	echo frmShowAudit($rowAccountTask,$con);	
}


// check if licence end date already past. If licence expired then no save button so can't edit anything!
if (strtotime($licence_end) > time()) {

	#if deleted state, must warn users that saving state as deleted removes the task from the system
	if ($rowAccountTask['state'] == "DELETED1") { ?>
		<div class="frmbutton"><span>Click here to save changes</span><input type="submit" value="Save" name="bt_save" class="inputSubmit" onClick="checkField('state', 'DELETED', 'This Task has been deleted by the HandSaM administrator. If you save this task with State: DELETED, you will not be able to access it again. If you wish to carry on using this task please press CANCEL and save it with another State')"/></div>
        <?php
	} else {
		#otherwise normal save button.
		if ($edit_task) {
            echo frmButtonHelp("Save","save", "Click here to save changes");
        } else {
            echo frmButtonHelp("Save","save_repeating", "Save repetition changes");
        }
	}
}


echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
echo endFormTable();

