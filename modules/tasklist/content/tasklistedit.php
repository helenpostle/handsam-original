<?php 
/**
 * Shows a form for editing a task list
 */
if (isset($tasklistid)) { 
	$rsTasklist = getRS($con,"select * from tasklist where tasklistid = $tasklistid");
	$rowTasklist = getRow($rsTasklist); 
	?>
<?php
	echo startFormTable($_SERVER["PHP_SELF"], "", "", TASKLIST_FORM_TITLE, TASKLIST_FORM_INFO);
	//echo formNotes(TASKLIST_FORM_TITLE, TASKLIST_FORM_INFO);
	echo frmHiddenField($tasklistid,"tasklistid");
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	
	//enter task list name 
	//echo frmTextField($rowTasklist,"tasklistname",30,"Task list name",true);
	if (isset($rowTasklist['mid']) && $rowTasklist['mid'] > 0) echo frmTextAreaNonEdit($rowTasklist,"tasklistname","Task list name",true);
	echo frmTextField($rowTasklist,"customtext",100,"Custom task list name",true);
	
	//echo frmSelectArray($rowTasklist, "state" , $state, "State" , true)	
	
	?>
	<?php	
	if ($tasklistid != 0 && isAllowed("editTasklist")) {
		echo frmShowAudit($rowTasklist,$con);	
	}?>
	
	
<?php
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($tasklistid != 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this task list?", "Click here to delete this task list");
	
	echo endFormTable();
} ?>


