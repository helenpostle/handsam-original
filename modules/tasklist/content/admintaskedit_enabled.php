<?php 
$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
    //add deleted state if is deleted
    if($rowTask['state'] == 'DELETED') $state['DELETED'] = 'DELETED';
    
	//enter task list name 
	if ($rowTask['mid'] > 0) {
        if (isset($task_hist) &&  $task_hist['tasktext']  != $rowTask['tasktext']) {
            echo "<div class=\"sync\">";
            //display previous text
            echo frmTextAreaNonEdit($task_hist,"tasktext","Original Task description");
            //highlight changed field
            echo frmTextAreaNonEdit($rowTask,"tasktext","New Task description");
            //display validate changes checkbox
            echo frmCheckBoxNonDb(0,"sync_check_$sync_change_cnt","Changes Accepted", true, 'sync_check');
            $sync_change_cnt++;
            //display hidden field used to check for checkbox in taskadmin
            echo frmHiddenField(1,"sync_change[]");
            echo "</div>";
        } else {
            echo frmTextAreaNonEdit($rowTask,"tasktext","Task description");
        }
        
    }
    echo frmTextArea($rowTask,"customtext",5,"Custom Task description",true);

    // key task checkbox
    if($core_conf['key_task'])
    {
        $booleanTrue = 1;
        echo frmCheckBox($rowTask, 'key_task', 'Key Task');
    }

    if($core_conf['key_task'] && $core_conf['key_task_lock'])
    {
        $booleanTrue = 1;
        echo '<div id="key_task_lock">'.frmCheckBox($rowTask, 'key_task_lock', 'Key Task Date Lock').'</div>';
    }
    
    if (isset($task_hist) && $task_hist && $task_hist['categoryid']  != $rowTask['categoryid']) {
        //get category name
        $cat_sql = "select categoryname from taskcategory where categoryid = ".$task_hist["categoryid"];
        $catRs = getRS($con, $cat_sql);
        $rowCat = getRow($catRs);
        echo "<div class=\"sync\">";
        echo frmTextAreaNonEdit($rowCat,"categoryname","Original Task Category");
        echo frmSelectQuery($rowTask,"categoryid","select categoryid as a, customtext as b from taskcategory where tasklistid = $tasklistid",$con,"New Task Category",true);
        //display validate changes checkbox
        echo frmCheckBoxNonDb(0,"sync_check_$sync_change_cnt","Changes Accepted", true, 'sync_check');
        $sync_change_cnt++;
        //display hidden field used to check for checkbox in taskadmin
        echo frmHiddenField(1,"sync_change[]");
        echo "</div>";
    } else {
        echo frmSelectQuery($rowTask,"categoryid","select categoryid as a, customtext as b from taskcategory where tasklistid = $tasklistid",$con,"Task Category",true);
    }
    
    
    if (isset($task_hist) &&  $task_hist['state']  != $rowTask['state']) {
        echo "<div class=\"sync\">";
        echo frmRowNoInput("<span class=\"state\">".$task_hist['state']."</span>","Original State","state",false);
        echo frmSelectArray($rowTask, "state" , $state, "New State" , true);
        echo frmCheckBoxNonDb(0,"sync_check_$sync_change_cnt","Changes Accepted", true, 'sync_check');
        $sync_change_cnt++;
        //display hidden field used to check for checkbox in taskadmin
        echo frmHiddenField(1,"sync_change[]");
        echo "</div>";          
    } else {
        echo frmSelectArray($rowTask, "state" , $state, "State" , true);
    }
    
    if (isset($task_hist) && $task_hist && $task_hist['taskmonth']  != $rowTask['taskmonth']) {
        echo "<div class=\"sync\">";
        echo frmRowNoInput("<span class=\"taskmonth\">".$months[$task_hist['taskmonth']]."</span>","Original Month","taskmonth",false);
        echo frmSelectArray($rowTask, "taskmonth" , $months, " New Month" , true);
        //display validate changes checkbox
        echo frmCheckBoxNonDb(0,"sync_check_$sync_change_cnt","Changes Accepted", true, 'sync_check');
        $sync_change_cnt++;
        //display hidden field used to check for checkbox in taskadmin
        echo frmHiddenField(1,"sync_change[]");
        echo "</div>";          
    } else {
        echo frmSelectArray($rowTask, "taskmonth" , $months, "Month" , true);
    }
    
    if (isset($task_hist) && $task_hist && $task_hist['taskday']  != $rowTask['taskday']) {
        echo "<div class=\"sync\">";
        echo frmTextAreaNonEdit($task_hist,"taskday","Original Day Number");
    	echo frmTextField($rowTask,"taskday",3,"New Day Number",true);
        //display validate changes checkbox
        echo frmCheckBoxNonDb(0,"sync_check_$sync_change_cnt","Changes Accepted", true, 'sync_check');
        $sync_change_cnt++;
        //display hidden field used to check for checkbox in taskadmin
        echo frmHiddenField(1,"sync_change[]");
        echo "</div>";    } else {
        echo frmTextField($rowTask,"taskday",3,"Day Number",true);
    }
    
    if(isset($core_conf['compliance']) && count($core_conf['compliance']) > 0)
    {
        echo frmSelectArray($rowTask, 'compliance', $core_conf['compliance'], 'Compliance', true);
    }
    
	if (isset($_GET['taskday'])) {
		echo frmInfoRow("You have now selected a new Day and Month. To return these to their original values, please click <a href=\"taskadmin.php?taskid=".$taskid."&month=".$month."&tasklistid=".$tasklistid."&option=".$option."&category=".$category."&multi=".$multi."&categoryid=".$categoryid."\">here</a>.  To save these new details to the database you most press <strong>save</strong>.");
	}
		
	$fileDB = getTaskSuperAdminFiles($taskid, $con);
	addFileField($fileDB);
	
	$linkDB = getTaskSuperAdminLinks($taskid, $con);
	addLinkField($linkDB);
 ?>