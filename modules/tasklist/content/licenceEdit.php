<?php 
/**
 * Shows a form for editing a licence
 */
 

if (isset($licenceid)) { 
	$rslicence = getRS($con,"select * from accountlicence where licenceid = $licenceid");
	$rowlicence = getRow($rslicence);
	$licenceDuration =  $rowlicence['end_date'] - $rowlicence['start_date'];
	$state = array("ACTIVE" => "ACTIVE", "NEW" => "NEW");
	echo startFormTable($_SERVER["PHP_SELF"]);
	echo frmLegend($frmTitle);
	echo formNotes(LICENCE_FORM_TITLE, LICENCE_FORM_INFO);
	echo frmHiddenField($accountid,"accountid");
	echo frmHiddenField($licenceid,"licenceid");
	echo frmHiddenField($tasklistid,"tasklistid");
	echo frmHiddenField($appid,"appid");
	if ($tasklistid > 0) {
		echo frmHiddenField("NEW","state");
	} else if ($appid > 0) {
		echo frmSelectArray($rowlicence, "state" , $state, "State" , true);
	}
	if ($licenceid == 0) {
		echo frmDateFieldNonDb ($start_date, "start_date", "Start Date", true);
	} else {
		echo frmDateField($rowlicence,"start_date","Start date",true);
	}	
	if ($accountid != 0 && isAllowed("editAccount")) {
		echo frmShowAudit($rowlicence,$con);	
	}
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($licenceid != 0 && isAllowed("editAccount"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this licence?", "Click here to delete this licence");
	
	echo endFormTable();
}
?>