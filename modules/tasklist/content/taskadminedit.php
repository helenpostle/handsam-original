<?php 
/**
 * Shows a form for updating a task
 */
//includes side box with list of tasks in this category for this month
$box[] = "tasksInCat.php";

if (isset($taskid)) { 
	$rsTask = getRS($con,"select * from task where taskid = $taskid");
	$rowTask = getRow($rsTask);
	if (!isset($categoryid)) {
		$categoryid = $rowTask['categoryid'];
	}
	if (!isset($month)) {
		$month =  $rowTask['taskmonth'];
	}
	?>
<?php
	echo startFormTable($_SERVER["PHP_SELF"], "", "", TASKADMIN_FORM_TITLE, TASKADMIN_FORM_INFO);
	//if ($taskid == 0) {
	//	echo frmLegend('Add Task');
	//} else {
	//	echo frmLegend('Edit Task');
	//}
	//echo formNotes(TASKADMIN_FORM_TITLE, TASKADMIN_FORM_INFO);
	echo frmHiddenField($taskid,"taskid");
	echo frmHiddenField($view,"view");
	echo frmHiddenField($tasklistid,"tasklistid");
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	//enter task list name 
	echo frmTextArea($rowTask,"tasktext",5,"Task description",true);
	echo frmSelectQuery($rowTask,"categoryid","select categoryid as a, categoryname as b from taskcategory",$con,"Task Category",true);
	echo frmSelectArray($rowTask, "state" , $state, "State" , true);	
	echo frmSelectArray($rowTask, "taskmonth" , $months, "Month" , true);	
	echo frmTextField($rowTask,"taskday",3,"Day Number",true);

	
	
	?>
	<?php	
	//echo frmRow("&nbsp;","&nbsp;");
	if ($taskid != 0 && isAllowed("editTask")) {
		echo frmShowAudit($rowTask,$con);	
	}?>
	
	
<?php
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($taskid != 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this task?", "Click here to delete this category");
	
	echo endFormTable();
} ?>


