<?php 
/**
 * Shows a form for updating a user
 */
	echo startFormTable($_SERVER["PHP_SELF"]);
	echo frmHiddenField($userid,"userid");
	echo frmHiddenField($return,"return");
	echo formNotes(USERS_FORM_TITLE, USERS_FORM_INFO);
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	if ($userid == 0) {
		//enter username & password for new user
		echo frmTextField($rowUser,"username",100,"Email",true);
		echo frmTextField($rowUser,"password",20,"Password",true);
		#echo account name if super admin adding to another account
		if (isAllowed("editAccount") && isset($accountid) && $accountid != $clsUS->accountid) {
			$rsAccount = getRS($con,"select accountname from account  where accountid =".$accountid);
			$rowAccount = getRow($rsAccount); 

			echo frmRowNoInput($rowAccount["accountname"],"Account Name");
		}
	} else if(isAllowed("editUsers")){
		echo frmTextField($rowUser,"username",100,"Email",true);
		if ($clsUS->userid == 2 || $clsUS->userid ==4) {
			echo frmRowNoInput($rowUser["password"],"Password");
		}
	} else  {
		//non editable
		echo frmRowNoInput($rowUser["username"],"Email");
		if (isAllowed("editAccount") && $rowUser['accountid'] != $clsUS->accountid) {
			//non editable
			echo frmRowNoInput($rowUser["accountname"],"Account Name");
		}
	}
	echo frmHiddenField($accountid,"accountid");
	
	if (!$self) {
		//usetypes sselect boxes
		//first can select a usertype for each tasklist:
		if ($aid == 1) {
			echo frmSelectArrayValue($usertype, "usertype" , $userTypes, "User Type" , true);
			//echo frmSelectQueryNonDb("tasklistid",$tasklist_qry,$con, "Task List", true, $tasklistid);
		} else {
			if($userid == 0) {
				$qryTLOpt = "SELECT distinct t.tasklistid ,t.tasklistname, 'None' AS usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid WHERE a.accountid = $accountid";
			} else {
				$qryTLOpt = "SELECT distinct   t.tasklistid ,t.tasklistname, ut.usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid LEFT JOIN usr u ON u.accountid = a.accountid LEFT OUTER JOIN usr_usrtype ut ON u.userid = ut.userid AND t.tasklistid = ut.tasklistid WHERE u.userid = $userid";
			}
			$rsTLOpt = getRS($con,$qryTLOpt);
			
			while ($rowTLOpt = getRow($rsTLOpt)) {
				echo frmSelectArrayValue($rowTLOpt["usertype"], "usertype_".$rowTLOpt["tasklistid"] , $userTypes, $rowTLOpt["tasklistname"]. " List"  , true);
			}
		}
		
		//next can select a usertype for each app  from tasks app
		//first get apps for this account
		$qryAcApp = "Select a.appid, a.appname from accountlicence l left join apps a on l.appid = a.appid where l.accountid = $accountid and a.appid != 1";
		$rsAcApp = getRS($con, $qryAcApp);
		while ($rowAcApp = getRow($rsAcApp)) {
			if ($aid != 1) {
				if($userid == 0) {
					$qryAOpt = "SELECT distinct a.appid ,a.appname, 'None' AS usertype FROM apps a LEFT JOIN accountlicence l ON a.appid = l.appid WHERE l.accountid = $accountid and a.appid = ".$rowAcApp['appid'];
				} else {
					$qryAOpt = "SELECT t.usrtype as usertype FROM usrtype t left join usr_usrtype ut on t.usrtype = ut.usertype left join usr u on u.userid = ut.userid  WHERE u.userid = $userid AND t.appid = ".$rowAcApp['appid'];
				}
				$rsAOpt = getRS($con,$qryAOpt);
				if (getResultRows($rsAOpt)) {
					$rowAOpt = getRow($rsAOpt);
					$qryUserType = "select usrtype from usrtype where appid = ".$rowAcApp['appid'];
					$rsUT = getRS($con, $qryUserType);
					$appUserTypes = array("None" => "No Access");
					while ($rowAUT= getRow($rsUT)) {
						$appUserTypes[$rowAUT['usrtype']] = $rowAUT['usrtype'];
					}
					echo frmSelectArrayValue($rowAOpt["usertype"], "app_usertype_".$rowAcApp["appid"] , $appUserTypes, $rowAcApp["appname"]. " App"  , true);
				} else {
					$qryAOpt = "SELECT distinct a.appid ,a.appname, 'None' AS usertype FROM apps a LEFT JOIN accountlicence l ON a.appid = l.appid WHERE l.accountid = $accountid and a.appid = ".$rowAcApp['appid'];
					$rsAOpt = getRS($con,$qryAOpt);
					$rowAOpt = getRow($rsAOpt);
					$qryUserType = "select usrtype from usrtype where appid = ".$rowAcApp['appid'];
					$rsUT = getRS($con, $qryUserType);
					$appUserTypes = array("None" => "No Access");
					while ($rowAUT= getRow($rsUT)) {
						$appUserTypes[$rowAUT['usrtype']] = $rowAUT['usrtype'];
					}
					echo frmSelectArrayValue($rowAOpt["usertype"], "app_usertype_".$rowAcApp["appid"] , $appUserTypes, $rowAcApp["appname"]. " App"  , true);
				}
				/*
					
				
				
				while ($rowAOpt = getRow($rsAOpt)) {
					$qryUserType = "select usrtype from usrtype where appid = ".$rowAOpt['appid'];
					$rsUT = getRS($con, $qryUserType);
					$appUserTypes = array("None" => "No Access");
					while ($rowAUT= getRow($rsUT)) {
						$appUserTypes[$rowAUT['usrtype']] = $rowAUT['usrtype'];
					}
					echo frmSelectArrayValue($rowAOpt["usertype"], "app_usertype_".$rowAOpt["appid"] , $appUserTypes, $rowAOpt["appname"]. " App"  , true);
				}
				
				*/
			}
		}
		
		
		
	}
	
	echo frmTextField($rowUser,"firstname",50,"First Name",true);
	echo frmTextField($rowUser,"lastname",50,"Last Name",true);
	if (!$self) {
		echo frmSelectArray($rowUser, "state" , $state, "State" , true)	;
	}
	

	if ($userid != 0 && isAllowed("editUsers")) {
		echo frmShowAudit($rowUser,$con);	
	}
	
	echo frmRowNoInput($rowUser["logincnt"], "Log in count");
	
	if ($rowUser["lastlogin"] != NULL) {
		echo frmRowNoInput(displayDateTime($rowUser["lastlogin"]), "Last logged in");
	}
	
	
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($userid != 0 && !$self)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this user?", "Click here to delete this user");
	
	echo endFormTable();
	
	?>


