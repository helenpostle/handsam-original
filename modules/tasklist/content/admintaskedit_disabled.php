<?php 
$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
    //add deleted state if is deleted
    if($rowTask['state'] == 'DELETED') $state['DELETED'] = 'DELETED';
    

    echo frmTextAreaNonEdit($rowTask,"customtext","Custom Task description");

    // key task checkbox
    if($core_conf['key_task'])
    {
        $booleanTrue = 1;
        echo frmCheckBox($rowTask, 'key_task', 'Key Task', false, true);
    }

    if($core_conf['key_task'] && $core_conf['key_task_lock'])
    {
        $booleanTrue = 1;
        echo '<div id="key_task_lock">'.frmCheckBox($rowTask, 'key_task_lock', 'Key Task Date Lock', false, true).'</div>';
    }
    

    echo frmSelectQuery($rowTask,"categoryid","select categoryid as a, customtext as b from taskcategory where tasklistid = $tasklistid",$con,"Task Category", false, '', true);
    
    echo frmSelectArray($rowTask, "state" , $state, "State" , false, '', true);

 ?>