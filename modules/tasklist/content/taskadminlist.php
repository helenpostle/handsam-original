<?php
/***
** Shows the list of users in the system
** @todo make it so clicking on colunm headings reorders list? or add dropdown to view only a certain usertype? or alphabetical listing?
** i.e. make it easier for when there are loads of users
***/

/* don't do pages for now! 
if (!isset($HTTP_GET_VARS["page"])) $page = 1;
else $page = $HTTP_GET_VARS["page"];

$limit = 40;
$count = getResultRows(getRS($con, $qry));

$start = ($page - 1) * $limit;
$pages = (($count % $limit) == 0) ? $count / $limit : floor($count / $limit) + 1;
$result = getRS($con, $qry." limit $start, $limit");

echo pageNavigator("users.php", "order=".$order, $page, $pages);
		//echo "<center>".$pageNav."</center>";
*/

//only in month view 
if ($view == 'cat') {
	//query in category view
	$qry = "SELECT * FROM task where state != 'DELETED' and categoryid='$categoryid' and tasklistid='$tasklistid' order by taskmonth,taskday" ;
	$lastMonth = "";
} else {
	//query if in months view		
	$qry = "SELECT * FROM task where state != 'DELETED' and taskmonth='$month' and tasklistid='$tasklistid' order by taskday,taskmonth" ;
	//display months menu in month view
	?>
	<div id="months_navigation">
		<ul>
		<?php
		foreach($months as $mkey => $mvalue) {?>
			<li><a href="taskadmin.php?tasklistid=<?php echo $tasklistid;?>&taskmonth=<?php echo $mkey;?>"><?php echo $mvalue;?></a></li>
			<?php
		}?>
		</ul>
	</div>
	<?php
	// variable and aray to highlight selected category - only in month view
	$catids = array();
	$rowClassHighlight = "";
}	


$result = getRS($con, $qry);

?>

<table class="list">
	<caption><?php echo $listTitle;?></caption>
	<thead>
		<tr>
		<th class="day nolink">Day</th>
		<th class="description nolink">Task Description</th>
		<th class="state nolink">State</th>
		</tr>
	</thead>
	<tbody>

		<?php
		//stuff for hilighting  alternate days in month and category view
		$daynum = 0;
		$rowClassOdd = "";
		$rowcnt = 0;
		$trid = "";
		while ($row = getRow($result)) {
			$rowcnt++;
			
			//changes class of alternate day rows in table ijnboth month and category view
			if ($daynum != $row['taskday'] && $rowcnt != 1) {
				if($rowClassOdd == "odd") {
					$rowClassOdd = "";
				} else {
					$rowClassOdd = "odd";
				}
			} 
			
			// 	only happens in month view	
			if ($view != 'cat') {
				//so that first instance of class is given id=catid for anchor only in month view
				$trid = "";
				if (!in_array($row['categoryid'], $catids)) {
					$catids[] = $row['categoryid'];
					$trid = "id=\"cat".$row['categoryid']."\"";
				}
			
			
				//to give different class for  odd days and highlight category only in month view
				if ($row['categoryid'] == $catid) {
					$rowClassHighlight = "highlight"; 
				} else {
					$rowClassHighlight = "";
				}
				if ($rowClassHighlight !="") {
					$rowClassOdd = "";
				}
				
			} else {
				//add month headings
				if ($rowcnt == 1 || $row['taskmonth'] != $lastMonth) {
					echo "<tr id=\"month".$row['taskmonth']."\"class=\"headingRow\"><td class=\"centre\" colspan=\"3\">".$months[$row['taskmonth']]."</td></tr>";
				}
				$lastMonth = $row['taskmonth'];
			}
					
								
			$daynum = $row['taskday'];
			$link1 = "taskadmin.php?taskid=".$row["taskid"]."&tasklistid=".$tasklistid."&view=".$view."&taskmonth=".$row['taskmonth']."&categoryid=".$row['categoryid'];
			$link2 = "taskadmin.php?taskid=0&tasklistid=".$tasklistid."&taskmonth=".$row['taskmonth']."&categoryid=".$row['categoryid']."&view=".$view;
		?>
			<tr <?php echo $trid; ?> class="<?php echo $rowClassHighlight." ".$rowClassOdd;?>">
				<td class="day"><a href="<?php echo $link1;?>"><?php echo displayText($row["taskday"]) ?></a></td>
				<td class="description"><a href="<?php echo $link1;?>"><?php echo showAbstract($row["tasktext"], 100); ?></a></td>
				<td class="state"><a href="<?php echo $link1;?>"><?php echo displayText($row["state"]) ?></a></td>
		 	</tr>
		<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3"><a href="<?php echo $link2;?>">Add a new task</a></td>
		</tr>
	</tfoot>
</table>
