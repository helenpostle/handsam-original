<?php 
/**
 * list users tasks
 */
 
$accountid = $clsUS->accountid;
$taskListTable = "<table class=\"acc_list\">";
$taskListTable .= "<caption>".$listTitle."</caption>";
$taskListTable .= "<thead>";
$taskListTable .= "<tr>";
$taskListTable .= "<th class=\"day nolink start\">Date</th>";
$taskListTable .= "<th class=\"tasktext nolink\">Task Description</th>";
$taskListTable .= "<th class=\"state nolink feedback\">Docs</th>";
$taskListTable .= "<th class=\"nolink end\"></th>";
	
$taskListTable .= "</tr>";
$taskListTable .= "</thead>";
$taskListTable .= "<tbody>";
$daynum = 0;
$rowClassOdd = "";
$rowcnt = 0;
$trid = "";

if ($month == 13) {
	$monthsNav = "<li class=\"active\">All months</li>";
} else {
	$monthsNav = "<li><a href=\"mytasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=13&year=0&option=".$option."&category=".$category."\">All months</a></li>";
}

if ($month == 0) {
	$monthsNav .= "<li class=\"active\">Now</li>";
} else {
	$monthsNav .= "<li><a href=\"mytasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=0&year=0&option=".$option."&category=".$category."\">Now</a></li>";
}
		
$todayTask = 0;
$lateTask = 0;

//stuff for hilighting  alternate days in month and category view
while ($row = getRow($optionresult)) {
    $rowClassHighlight = "";
	#sort out action date into month year and day
	$actionyear = date("Y", strtotime($row["actiondate"]));
	$actionmonth = date("n", strtotime($row["actiondate"]));
	$actionday = date("jS", strtotime($row["actiondate"]));
	if (date("j n Y", strtotime($row["actiondate"])) == date("j n Y", time()) && $row["state"] != "COMPLETED") {
		$alertImg = "<img src=\"images/icon_error.gif\" alt=\"This task is late\"/>";
		$alertClass = "today";
		$todayTask ++;
	} else if (strtotime($row["actiondate"]) < time() && $row["state"] != "COMPLETED") {
		$alertImg = "<img src=\"images/icon_error_red.gif\" alt=\"This task is late\"/>";
		$alertClass = "late";
		$lateTask ++;
	} else if (($row['completedate'] > $row['actiondate']) && $row["state"] == "COMPLETED") {
		$alertImg = "<img src=\"images/tick_red.gif\" alt=\"This task was completed late\"/>";
		$alertClass = "completed";
	} else if($row["state"] == "COMPLETED")  {
		$alertImg = "<img src=\"images/tick.gif\" alt=\"This task was completed on time\"/>";
		$alertClass = "completed";
	} else {
		$alertImg = "";
		$alertClass = "";
	}

	##display blue tick if feedback has been left!	
	$feedbackImg = "";
	//we should also check for feedback docs and also display tick
	
	if ((trim($row["feedback"])) != "" || (trim($row["notes"])) != "" || $row['userDocs'] > 0 || $row['adminDocs'] > 0) {
		if ( $row['adminDocs'] > 0 || trim($row["notes"]) != ""){
			$feedbackImg = "<img src=\"images/tick_orange.gif\" alt=\"This task has additional notes or documents\"/>";
		} else {
			$feedbackImg = "<div class=\"tick\"></div>";
		}
		if ( $row['userDocs'] > 0 || trim($row["feedback"]) != "")	{
			$feedbackImg .= "<img src=\"images/tick_blue.gif\" alt=\"This task has feedback\"/>";
		} else {
			//$feedbackImg = "<img src=\"images/tick_blue.gif\" alt=\"This task has feedback\"/>";
		}
	} else if (strtotime($row["actiondate"]) < time() && $row["state"] != "COMPLETED") {
		$feedbackImg = "";
	}
	
		#set lastmonth for first loop											
		if ($rowcnt == 0) {
			$lastmonth = 0;
			$daynum = 0;
		}
		
		//changes class of alternate day rows in table ijnboth month and category view
		if ($daynum != $actionday && $rowcnt != 0) {
			if($rowClassOdd == "odd") {
				$rowClassOdd = "";
			} else {
				$rowClassOdd = "odd";
			}
		} 
		$daynum = $actionday;
		if ($lastmonth != $actionmonth && ($month == 0 || $month == 13)) { 
			$taskListTable .=  "<tr id=\"month".$actionmonth."\"class=\"headingRow\"><td class=\"centre start end\" colspan=\"4\">".$months[$actionmonth]." ".$actionyear."</td></tr>";
		}
		
		if ($lastmonth != $actionmonth) {
			$monthLink = "mytasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;year=".$actionyear."&option=".$option."&category=".$category."&month=";
			
			$monthsNav .= displayListLink($months[$actionmonth], $month, $actionmonth, $monthLink);

		}
		$lastmonth = $actionmonth;
		$rowcnt++;
        
        if ($row['key_task'] == 1) $rowClassHighlight = "highlight";
        
		if (($month == 13 && $year == 0) || ($month == 0 && $year == 0) || ($actionyear = $year && $actionmonth == $month)) {
			$link1 = "mytasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;accounttaskid=".$row["accounttaskid"]."&month=".$month."&year=".$year."&option=".$option."&category=".$category;
			$taskListTable .= "<tr ".$trid." class=\"".$rowClassHighlight." ".$rowClassOdd."\">";
			$taskListTable .= "<td class=\"day start\"><a href=\"".$link1."\">".$actionday."</a></td>";
			$taskListTable .= "<td class=\"tasktext flag\"><a href=\"".$link1."\">".$alertImg."<strong>".displayText($row["categoryname"]).":</strong> ".showAbstract($row["customtext"], 100)."</a></td>";
			$taskListTable .= "<td class=\"feedback state\"><a href=\"".$link1."\">$feedbackImg</a></td>";
			$taskListTable .= "<td class=\"end\"></td>";
		 	$taskListTable .= "</tr>";
	 	}					

	} 
$taskListTable .= "</tbody>";
$taskListTable .= "<tfoot>";
$taskListTable .= "<tr>";
$taskListTable .= "<td class=\"start\"></td>";
$taskListTable .= "<td class=\"\"></td>";
$taskListTable .= "<td class=\"\"></td>";
$taskListTable .= "<td class=\"end\"></td>";
$taskListTable .= "</tr>";
$taskListTable .= "</tfoot>";
$taskListTable .= "</table>";

$qry = "SELECT distinct accountlicence.licenceid, tasklist.tasklistname, tasklist.tasklistid FROM  accounttask LEFT JOIN accountlicence ON accounttask.licenceid = accountlicence.licenceid left join tasklist on accountlicence.tasklistid = tasklist.tasklistid WHERE accounttask.assignedto = $userid AND (accounttask.state = 'ACTIVE' or accounttask.state = 'COMPLETED') AND accountlicence.end_date > now() and accountlicence.state='ACTIVE' and accountlicence.accountid = ".$clsUS->accountid;

//echo $qry;
//die();
$rsTaskList = getRS($con, $qry);
$cnt = 0;
if (getResultRows($rsTaskList) > 0) {
	echo "<p>View Tasklists: <a href=\"mytasks.php\">All task lists</a> | ";
	while ($rowList = getRow($rsTaskList)) {
		if (isAllowed("myTasks", 1, $rowList['tasklistid'])) {
			if($cnt > 0) echo " | ";
			echo "<a href=\"mytasks.php?licenceid=".$rowList['licenceid']."\">".$rowList['tasklistname']."</a>";
			$cnt ++;
		}
	}
	echo "</p>";
}

echo $taskListTable;
 ?>

