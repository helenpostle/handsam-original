<?php 
/**
 * page occurs when error has been encountered
 */
 ?>
<h1>ERROR</h1>
<p>We are sorry an error has occured:</p>
<p>"<?php echo $errorFlag; ?>"</p>
<p>The administrator has been informed of this error. Please try again shortly.</p>
