<?php 
/**
 * standard about page for OMC
 */
 
 ?>
 
<h2>HandSaM features:</h2>

<ul>
	<li>A central register of all the health, safety, welfare and security topics and  tasks required by law of schools and colleges</li>
	<li>A register of health, safety, welfare and security tasks that have to be done</li>
	<li>An instant record of the tasks done and those not done</li>
	<li>Instant links to summaries of health and safety law, regulation and guidance</li>
	<li>A choice of how data is recorded to suit your needs</li>
	<li>Access by any designated user</li>
</ul>


<h2>What does HandSaM do?</h2>

<ul>
	<li>HandSaM contains a day-by-day register of tasks that fulfil your legal duties</li>
	<li>HandSaM allows you to delete and add items that suit your own programme of health and safety management</li>
	<li>HandSaM enables you to send tasks to staff responsible for completing them, and for them to record that they have done them</li>
	<li>HandSaM records which tasks have been completed, by whom and when</li>
	<li>HandSaM gives an instant daily reminder of tasks to be completed</li>
	<li>HandSaM enables you to find out more information from links to specialist documents</li>
	<li>HandSaM allows you to add links to your own school policies and procedures</li>
</ul>
<h2>What does the subscriber do?</h2>
<ul>
	<li>You decide whether to delete topics that do not concern the school</li>
	<li>You decide whether to change the dates when tasks are scheduled</li>
	<li>You ensure the tasks are completed and then recorded on Handsam</li>
	<li>You decide who should have access to Handsam</li>	
</ul>
