<?php 
/**
 * Handsam task changes
 */
 	$taskState = array("NEW" => "New Tasks", "EDITED" => "Edited Tasks", "DELETED1" => "Deleted Tasks");

 
 	$qry = "SELECT * FROM  accounttask WHERE accountid = $accountid AND (state = 'EDITED' OR state = 'DELETED1' OR state = 'NEW') ORDER BY state,edited desc";
	$result = getRS($con, $qry);
	if (getResultRows($result) > 0) {

		boxTop("Recent HandSaM changes");
		echo "<p class=\"info\">Please click on a task below to edit it. To remove it from this list, edit the task and change its state to DELETED, ACTIVE or INACTIVE.</p>";		
		echo "<div class=\"vertical_nav\">\n";
		$last_state = "";
		while ($row = getRow($result)) {
			if ($row['state'] != $last_state) {
				
				if ($last_state != "") {
					echo "</ul>\n";
				}
				echo "<h3>".$taskState[$row['state']]."</h3>\n";
				echo "<ul>\n";

			}
			$last_state = $row['state'];
			echo "<li><a href=\"$rootUrl/modules/tasklist/tasks.php?accounttaskid=".$row['accounttaskid']."&return=index.php\">".textSummary($row['customtext'], 100)."</a></li>\n";
	
		}
		echo "</ul>\n";
		echo "</div>\n";	
	
		boxBottom();
	}
?>
	