<?php 
/**
 * tasks in this category for taskadmin.php edit form
 */
boxTop("Tasks in ".$categoryname);
$weekly_case = ", CASE  WHEN week_num > 0 THEN ";
$weekly_case .= " CASE WHEN dayofweek(YEAR(CURDATE())) > day_of_week THEN CONVERT(date_format(date_add( date_add( date_add(CONCAT(YEAR(CURDATE()),'-01-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-01-01'))) % 7 DAY) , INTERVAL week_num - 1 week), INTERVAL day_of_week DAY), '%e'), UNSIGNED INTEGER) ";
$weekly_case .= " ELSE CONVERT(date_format(date_add( date_add( date_add(CONCAT(YEAR(CURDATE()),'-01-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-01-01'))) % 7 DAY) , INTERVAL week_num - 2 week), INTERVAL day_of_week DAY), '%e'), UNSIGNED INTEGER) END ";

//week 1 of month
$weekly_case .= " WHEN position_in_month = 0 THEN CONVERT(date_format(date_add(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')) + day_of_week) % 7 DAY), '%e'), UNSIGNED INTEGER) ";
//week 2 of month
$weekly_case .= " WHEN position_in_month = 1 THEN CONVERT(date_format(date_add(date_add(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')) + day_of_week) % 7 DAY), interval 1 week), '%e'), UNSIGNED INTEGER) ";
//week3 of month
$weekly_case .= " WHEN position_in_month = 2 THEN CONVERT(date_format(date_add(date_add(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')) + day_of_week) % 7 DAY), interval 2 week), '%e'), UNSIGNED INTEGER) ";
//week 4 of month
$weekly_case .= " WHEN position_in_month = 3 THEN CONVERT(date_format(date_add(date_add(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')) + day_of_week) % 7 DAY), interval 3 week), '%e'), UNSIGNED INTEGER) ";
//last week of month
$weekly_case .= " WHEN position_in_month = 4 THEN CONVERT(date_format(date_add(LAST_DAY(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01')), INTERVAL (-6 - dayofweek(LAST_DAY(CONCAT(YEAR(CURDATE()),'-',taskmonth,'-01'))) + day_of_week) % 7 DAY), '%e'), UNSIGNED INTEGER) ";
$weekly_case .= " else taskday END AS taskday";

$weekly_case .= " , CASE  WHEN week_num > 0 THEN ";    
$weekly_case .= " CASE WHEN dayofweek(YEAR(CURDATE())) > day_of_week THEN CONVERT(date_format(date_add( date_add( date_add(CONCAT(YEAR(CURDATE()),'-01-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-01-01'))) % 7 DAY) , INTERVAL week_num - 1 week), INTERVAL day_of_week DAY), '%c'), UNSIGNED INTEGER) ";
$weekly_case .= " ELSE CONVERT(date_format(date_add( date_add( date_add(CONCAT(YEAR(CURDATE()),'-01-01'), INTERVAL (8 - dayofweek(CONCAT(YEAR(CURDATE()),'-01-01'))) % 7 DAY) , INTERVAL week_num - 2 week), INTERVAL day_of_week DAY), '%c'), UNSIGNED INTEGER) END else taskmonth END AS taskmonth ";
$qry = "SELECT taskmonth, tasktext, customtext $weekly_case FROM  task  where categoryid = '$categoryid' and state != 'DELETED'";
$result = getRS($con, $qry);
	

?>
<p class="info">Click on any of the tasks below to select that task &quot;day&quot; for the task being edited.</p>
<table class="boxList">
	<thead>
		<tr>
			<th class="description nolink">Task Description</th>
			<th class="day nolink">Day</th>
		</tr>
	</thead>
	<tbody>

	<?php
	while ($row = getRow($result)) {
		$linkurl = "?month=".$month."&option=".$option."&category=".$category."&taskday=".$row["taskday"]."&taskmonth=".$row["taskmonth"]."&taskid=".$taskid."&tasklistid=".$tasklistid."&categoryid=".$categoryid;
		?>
		<tr>
			<td class="description"><a class="arrow" title="View <?php echo showAbstract($row["customtext"], 70);?>" href="<?php echo $linkurl;?>"><?php echo showAbstract($row["customtext"], 70); ?></a></td>
			<td class="day"><a title="View <?php echo showAbstract($row["customtext"], 70);?>" href="<?php echo $linkurl;?>">
            <?php
                if(isset($row["taskmonth"]) && $row["taskmonth"] > 0) echo substr($months[$row["taskmonth"]], 0, 3).": ".$row["taskday"];
            ?>
            </a>
            </td>
		</tr>
		<?php 
	} ?>
	</tbody>
</table>
<?php
	boxBottom();
?>