<?php 
/**
 * show licence history for the selected account
 */
	boxTop("Licence Archive");
	$qry = "SELECT * FROM accountlicence where accountid = ".$accountid." and state != 'DELETED'  order by end_date asc";
	$result = getRS($con, $qry);
	$renewLicence = false;
?>
<p class="info">Select a licence to view the tasks that go with that licence. </p>
<table class="boxList">
	<caption class="hidden"><?php echo $title;?> licence History</caption>
	<thead>
		<tr>
			<th class="licenceid nolink">ID</th>
			<th class="state nolink">State</th>
			<th class="licence nolink">Start</th>
			<th class="licence nolink">End</th>
		</tr>
	</thead>
	<tbody>
		<?php
		while ($row = getRow($result)) {
			#only if the licence state==new can we still edit the licence
			#'edit licence' page also has 'edit  holiday periods' link
			if ($row['state'] == "NEW" && isAllowed("editAccount")) {
				?>
				<tr>
					<td class="licenceid"><a class="arrow" href="licence.php?licenceid=<?php echo $row["licenceid"]; ?>&accountid=<?php echo $accountid;?>"><?php echo $row["licenceid"]; ?></a></td>
					<td class="licence"><a href="licence.php?licenceid=<?php echo $row["licenceid"]; ?>&accountid=<?php echo $accountid;?>"><?php echo $row['state']; ?></a></td>
					<td class="licence"><a href="licence.php?licenceid=<?php echo $row["licenceid"]; ?>&accountid=<?php echo $accountid;?>"><?php echo displayDate($row['start_date']); ?></a></td>
					<td class="licence"><a href="licence.php?licenceid=<?php echo $row["licenceid"]; ?>&accountid=<?php echo $accountid;?>"><?php echo displayDate($row['end_date']); ?></a></td>
				</tr>
			<?php 
			} elseif($row['state'] == "ACTIVE"  && isAllowed("viewOurTasks", 1,$row['tasklistid'])){
				#show link to tasks for this licence
				$licenceLink = "tasks.php?licenceid=".$row["licenceid"];
				if (strtotime($row['start_date']) < time() && strtotime($row['end_date']) > time()) {
					$this_state = "CURRENT";
				} else if(strtotime($row['start_date']) > time())  {
					$this_state = "ACTIVE";
				} else {
					$this_state = "ARCHIVED";
				}
				?>
				<tr>
					<td class="licenceid"><a class="arrow" href="<?php echo $licenceLink; ?>"><?php echo $row["licenceid"]; ?></a></td>
					<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo $this_state; ?></a></td>
					<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo displayDate($row['start_date']); ?></a></td>
					<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo displayDate($row['end_date']); ?></a></td>
				</tr>
			<?php 
			} 
                        
			// these variables store the latest licence details for use below
			$end_date = $row['end_date'];
			$currentState = $row['state'];
			$lastlicence = $row["licenceid"];
			$renewLicence = true;
		}
		
		?> 
	</tbody>
	<tfoot>
	<?php
	if (isAllowed("editAccount")) {
		if ($renewLicence) {
			if (strtotime($end_date) > strtotime(strftime("%d/%m/%Y",time()))) {
				echo "<tr><td colspan=\"4\"><a href=\"licence.php?accountid=".$accountid."&licenceid=0\" title=\"Renew licence\">Renew licence</a></td></tr>";
			} else {
				echo "<tr><td colspan=\"4\"><a class=\"page\" href=\"licence.php?accountid=".$accountid."&licenceid=0\" title=\"Create licence\">Create licence</a></td></tr>";
			}
		} else  {
			#show create licence button
			echo "<tr><td colspan=\"4\"><a class=\"page\" href=\"licence.php?accountid=".$accountid."&licenceid=0\" title=\"Create licence\">Create licence</a></td></tr>";
		}
	}

		?>
	</tfoot>
</table>
<?php
	boxBottom();
?>
	