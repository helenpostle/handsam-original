<?php 
/**
 * lists all task categories in the selected month
 * click on category to display that category
 */
	
	if ($month > 0) {
		boxTop($months[$month]."'s Categories");
		$qry = "SELECT taskcategory.categoryname, taskcategory.customtext, taskcategory.categoryid, count(taskcategory.categoryname) as cnt FROM taskcategory inner join task on taskcategory.categoryid = task.categoryid where task.tasklistid = '$tasklistid' and task.taskmonth = '$month' and task.state != 'DELETED' $category_list_task_state group by taskcategory.categoryname, taskcategory.customtext, taskcategory.categoryid order by taskcategory.customtext asc";

	} else {
		boxTop("Categories");
		$qry = "SELECT taskcategory.categoryname, taskcategory.customtext,taskcategory.categoryid, count(taskcategory.categoryname) as cnt FROM taskcategory inner join task on taskcategory.categoryid = task.categoryid where task.tasklistid = '$tasklistid' and task.state != 'DELETED' $category_list_task_state group by taskcategory.categoryname, taskcategory.customtext,taskcategory.categoryid order by  taskcategory.customtext asc";
	}
	$result = getRS($con, $qry);
	$categoryLink = "taskadmin.php?month=".$month."&tasklistid=".$tasklistid."&option=".$option."&multi=".$multi."&category=";

?>
<p class="info margin-bottom">Click on any of the categories below to select that category. Click the <strong>[+]</strong> to add atask to that category</p>

<div class="vertical_nav">

<ul>
	<?php 
	if ($category == 0) {
		echo "<li class=\"active\">Show all categories</li>";
	} else {
		echo "<li><a href=\"".$categoryLink."0\" title=\"\">Show all categories</a></li>";
	}
	while ($row = getRow($result)) {
		$html = $row["customtext"]."&nbsp;(".$row['cnt'].")<span class=''><a class='no_arrow' href='".$categoryLink.$category."&taskid=0&categoryid=".$row["categoryid"]."' title='add a task to ".$row["customtext"]."'>[+]</a></span>";
		//echo $html;	
		echo displayListLink($html, $category, $row["categoryid"], $categoryLink);
	}
	?>
</ul>
</div>

<?php
	boxBottom();
?>