<?php 
/**
 * add tasklist to the selected account
 */
boxTop("Add Task List Licence");
if (isAllowed("editAccount")) {
	$qry = "select tasklist.tasklistid as a, tasklist.tasklistname as b from tasklist  where tasklist.tasklistid <> ALL(select tasklistid from accountlicence where accountid = $accountid and tasklistid is not null and state in ('ACTIVE', 'INACTIVE', 'NEW') )and state = 'ACTIVE'";
	echo "<p class=\"info\">Add a new tasklist to this account</p>";
	echo startFormBox($_SERVER["PHP_SELF"]);
	echo frmSelectQueryNonDb("tasklistid", $qry,$con,"Task list",false);
	//echo $qry;
	echo frmButtonHelp("Add Tasklist","add_tasklist", "Click here to add tasklist");
	echo frmHiddenField($accountid,"accountid");

	echo endFormBox();

}
boxBottom();
?>
	