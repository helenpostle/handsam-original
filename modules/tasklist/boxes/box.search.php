<?php 
/**
 * lists options for viewing accoun tasks list
 * 
 */
boxTop("Search Tasks");
echo startSimpleForm($_SERVER["PHP_SELF"]);
echo frmLegend('Edit Task');
//echo formTopNotes(TASK_FORM_TITLE, TASK_FORM_INFO);
echo frmHiddenField($option,"option");
echo frmHiddenField($month,"month");
echo frmHiddenField($year,"year");
echo frmHiddenField($user,"user");
echo frmHiddenField($group_id,"group_id");
echo frmHiddenField($return,"return");
echo frmHiddenField($multi,"multi");
echo frmHiddenField($licenceid,"licenceid");
$search_btn = '<input type="submit" value="Search" name="bt_submit_search" class="inputSubmit">';
echo frmTextFieldNonDb('task_search',40,$search_btn,false, $task_search, 'off', 'task_search');
echo frmButton("Reset","cancelSearch");
echo endFormTable2();
boxBottom();
?>
	