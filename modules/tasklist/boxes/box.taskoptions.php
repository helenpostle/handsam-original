<?php 
/**
 * lists options for viewing account tasks list
 * 
 */
	boxTop("Reporting");
	if(isAllowed("editAccount")) {
		$optionLink = "tasks.php?task_search=$task_search_url&amp;licenceid=".$licenceid."&month=".$month."&year=".$year."&category=".$category."&user=".$user."&multi=".$multi."&group_id=".$group_id."&option=";
	} else {
		$optionLink = "tasks.php?task_search=$task_search_url&amp;licenceid=$licenceid&amp;month=".$month."&year=".$year."&category=".$category."&user=".$user."&multi=".$multi."&group_id=".$group_id."&option=";
	}
?>
<div class="vertical_nav">

<ul>
	<?php 
	
	echo displayListLink("Show all", $option, "active", $optionLink);
	
	echo displayListLink("Show completed", "$option", "completed", "$optionLink");
	echo "<li class=\"indent\">On time:<img class=\"inBox\" src=\"images/tick.gif\" alt=\"\"/> Late: <img class=\"inBox\" src=\"images/tick_red.gif\" alt=\"\"/>";
	
	
	echo displayListLink("Show uncompleted", $option, "uncompleted", $optionLink);
	echo displayListLink("Show late <img class=\"inBox\" src=\"images/icon_error_red.gif\" alt=\"\"/>", $option, "late", $optionLink);
	echo displayListLink("Show inactive", $option, "inactive", $optionLink);
  echo displayListLink("Show key tasks", $option, "key_task", $optionLink);
  echo displayListLink("Show unallocated", $option, "unallocated", $optionLink);
	if ($multi == 'yes') {
		echo "<li class=\"topMargin\"><a href=\"tasks.php?task_search=$task_search_url&amp;licenceid=".$licenceid."&month=$month&year=$year&option=".$option."&category=".$category."\">Hide 'Select Multiple Tasks'</a></li>";
	} else {
		echo "<li class=\"topMargin\"><a href=\"tasks.php?task_search=$task_search_url&amp;licenceid=".$licenceid."&month=$month&year=$year&option=".$option."&category=".$category."&multi=yes\">Show 'Select Multiple Tasks'</a></li>";
	}
	?>
</ul>
</div>
<?php
	boxBottom();
?>
	