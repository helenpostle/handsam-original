<?php 
/**
 * lists all task categories in the selected month
 * click on category to highlight category in content
 */
	boxTop($months[$month]."'s Categories");
	$qry = "SELECT taskcategory.customtext, taskcategory.categoryid, count(taskcategory.customtext) as cnt FROM taskcategory inner join task on taskcategory.categoryid = task.categoryid where task.tasklistid = '$tasklistid' and task.taskmonth = '$month' group by taskcategory.customtext, taskcategory.categoryid order by taskcategory.customtext asc";
	$result = getRS($con, $qry);

?>

<table class="list">
	<caption class="hidden"><?php echo $months[$month];?>'s Categories</caption>
	<thead>
		<tr>
			<th class="catname nolink">Category</th>
			<th class="addTask nolink">Add Task</th>
		</tr>
	</thead>
	<tbody class="noStrip">

	<?php
	while ($row = getRow($result)) {
	?>
		<tr>
			<td class="catname"><a title="Highlight <?php echo $row["customtext"];?>" href="?catid=<?php echo $row["categoryid"];?>&tasklistid=<?php echo $tasklistid;?>&taskmonth=<?php echo $month;?>#cat<?php echo $row["categoryid"];?>"><?php echo $row["customtext"] ?><span class="numTasks">(<?php echo $row["cnt"];?>)</span></a></td>
			<td class="centre addTask"><a href="taskadmin.php?taskid=0&tasklistid=<?php echo $tasklistid;?>&taskmonth=<?php echo $month;?>&categoryid=<?php echo $row["categoryid"];?>" title="add task to <?php echo $row["customtext"];?>">[+]</a></td>
		</tr>
	<?php } ?>
	</tbody>
		<tfoot>
			<tr>
				<td colspan="2">
				<?php
				if (isset($catid)) {?>
					<a title="Turn off Highlight" href="?tasklistid=<?php echo $tasklistid;?>&taskmonth=<?php echo $month;?>">Turn off highlight</a>
					<?php
				}?>
				</td>
			</tr>
		</tfoot>
</table>
<?php
	boxBottom();
?>