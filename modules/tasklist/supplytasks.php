<?php

/**
 * supply tasks to account licence
 * 
 */
ini_set('max_execution_time', '0'); // 0 = no limit.
$secure = true;
require("../../shared/startPage.php");
$pageFiles->addModFunc('tasklist_supply_tasks');
$pageFiles->includePhp();

#check if have requested a specific user
$accountid = getIntFromRequest("accountid");
$licenceid = getIntFromRequest("licenceid");
$submit_users = getIntFromRequest("submit_users");

if (isAllowed("editAccount")) {

    //we need to check for tasks for thus licence so that we can't get a duplicate list:
    $double_check_qry = "select accounttaskid from accounttask where licenceid = $licenceid";
    $double_check_rs = getRS($con, $double_check_qry);
    //get licence info
    $licence = new Licence($clsUS, $licenceid);
    $licence->get($con);

    if (getResultRows($double_check_rs) == 0) {

        //get array of userids for active users in this account to check against when copying assigned users over later on
        $users_array = array();
        if ($submit_users == 1)
            $users_array = getAccountUsers($accountid, $con);

        #get the holiday periods for this licence and put them into an array
        $holiday_periods = getHolidays($con, $licenceid);
        $days_off = array("Saturday", "Sunday");
        #get the starting month from $licence->start_date
        $this_month = date("n", strtotime($licence->start_date));
        #get the starting day from $licence->start_date
        $this_day = date("j", strtotime($licence->start_date));
        #get the starting Year from $licence->start_date
        $this_year = date("Y", strtotime($licence->start_date));

        //This is the bit to add custom tasks from the previous account licence to the new task list
        $previous_licenceid = getLastLicence($con, $licenceid, $licence->accountid, $licence->tasklistid);
        if ($previous_licenceid) {
            copyCustomTasks($con, $clsUS, $licenceid, $previous_licenceid, $holiday_periods, $licence->start_date, $licence->end_date, $days_off, $licence->accountid, $users_array, $submit_users);
        }

        //copy tasks over from tasks to accounttasks table
        if ($previous_licenceid && $submit_users == 1) {
            insertMonthDateLicencetasksCopyData($con, $clsUS, $licence->start_date, $licence->end_date, $holiday_periods, $days_off, $licence->tasklistid, $licence->accountid, $licenceid, $previous_licenceid);
        } else {
            insertMonthDateLicencetasksNoData($con, $clsUS, $licence->start_date, $licence->end_date, $holiday_periods, $days_off, $licence->tasklistid, $licence->accountid, $licenceid, $previous_licenceid);
        }
        insertWeeklyLicencetasks($con, $clsUS, $licence->start_date, $licence->end_date, $holiday_periods, $days_off, $licence->tasklistid, $licence->accountid, $licenceid);
        insertMonthDayLicencetasks($con, $clsUS, $licence->start_date, $licence->end_date, $holiday_periods, $days_off, $licence->tasklistid, $licence->accountid, $licenceid);

        //update licence state to active
        if ($licence->update($con, array("state" => "ACTIVE")))
            $messages[] = LICENCE_ACTIVE;

        if ($previous_licenceid) {
            copyTaskData($con, $clsUS, $licenceid, $previous_licenceid, $submit_users, $licence->accountid);
        }
        headerLocation("$rootUrl/handsam_core/accounts.php?accountid=" . $licence->accountid, $messages);
        die();
    } else {
        $messages[] = "This licence ($licenceid) already has tasks supplied. If all of the required tasks haven't been supplied, please contact the Administrator";
        headerLocation("$rootUrl/handsam_core/accounts.php?accountid=" . $licence->accountid, $messages);
        die();
    }
} else {
    trigger_error("Access Denied", E_USER_WARNING);
    headerLocation("$rootUrl/index.php", false);
}
?>