$(window).ready( function() {
    $('#trig\\.actiondate').hide();
    
    $('.cover').each(function(index) {
            // disable select boxes and inputs
            $('select', this).attr('disabled', 'disabled');
            $('input', this).attr('disabled', 'disabled');
	});
    // warning when selecting all tasks
	$('#selectAllBoxes').click(function(event){
		if ($(this).is(':checked')) {
			$('.multiSelectCheck').attr('checked', true);
			alert('You have now selected all ' + $('.multiSelectCheck').length + ' tasks on this page. Please untick this box if this isn\'t the correct action.');
		} else {
			$('.multiSelectCheck').attr('checked', false);
		}
	});
	
        // enable or disable the bulk action inputs
	$('.toggleInput').each(function(index) {
		$(this).click(function(event){
            var target_el = $(this).val();
            if($(this).is(':checked'))
                {
                    // enable
                    $('#'+target_el).removeAttr('disabled');
                    $('#'+target_el).next('img#trig\\.actiondate').show();
                }
                else
                {
                    // reset to empty value and disable
                    $('#'+target_el).attr('value', '').attr('disabled', 'disabled');
                    $('#'+target_el).next('img#trig\\.actiondate').hide();
                }
		});
		
	});
});

// UI Tabs
$(function(){
    $("#multitabs").tabs();
});