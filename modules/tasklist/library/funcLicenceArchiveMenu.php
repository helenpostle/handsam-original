<?php
/**
 * show licence menu for the selected account
 */
 
function licence_archive_menu($licenceid, $accountid, $con) {
	$menu = "";
	$class = "";
	if ($accountid > 0) {
		$qry = "SELECT * FROM accountlicence where accountid = ".$accountid." and state != 'DELETED' and state != 'NEW' order by end_date asc";
		$result = getRS($con, $qry);
		$menu = "<div id=\"licence_menu\" class=\"clearfix\"><ul>";
		
		while ($row = getRow($result)) {
			$class = "";
			if ($licenceid == $row['licenceid']) {
				$class = "current_licence";
			}
			$menu .= "<li class=\"".$class."\" ><a href=\"?licenceid=".$row['licenceid']."\">".date("Y",strtotime($row['start_date']))." - ".date("Y",strtotime($row['end_date']))."</a></li>";
		}
		$menu .= "</ul></div>";
	}
	return $menu;
}
?>