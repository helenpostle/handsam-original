<?php
/**
 * accounttask edit functions
 */
 
/*
 * get the licenecid from a userid
 */
function getLicenceId($userid, $con) {
    //get tasklistid and licence from user $clsUS if they are an administrator
    $tl_qry = "SELECT l.licenceid, l.accountid, ut.tasklistid FROM usr_usrtype ut LEFT JOIN usr u ON u.userid = ut.userid LEFT JOIN account a ON a.accountid = u.accountid LEFT JOIN accountlicence l ON a.accountid = l.accountid AND l.tasklistid = ut.tasklistid WHERE ut.userid = $userid AND ut.usertype = 'Administrator' AND l.state = 'ACTIVE' order by l.start_date desc limit 0,1 ";
    $tl_rs = getRS($con, $tl_qry);
    return getRow($tl_rs);
}
 
/*
 * update an exisiting account task
 */

function updateAccounttask($accounttask, $tasklistid, $licence_end, $con, $clsUS, $vals, $taskfiles, $tasklinks) {
    global $messages;
    global $core_conf;
    $budget = str_replace(" ", "", $vals["budget"]);
    $val_arr['budget'] = str_replace("�", "", $budget);
    if ($accounttask->update($con, $vals, $licence_end, $tasklistid)) {
        $taskfiles->addFiles($con, $clsUS, $accounttask->accounttaskid);
        $taskfiles->rmFiles($con, $accounttask->accounttaskid);
        $tasklinks->addLinks($con, $clsUS, $accounttask->accounttaskid);
        $tasklinks->rmLinks($con, $accounttask->accounttaskid);
        $messages[] = SAVED;
        return true;
    } else {
        return false;
    }  
}

/*
 * update the accounttask date data for repeating task date criteria changes.
 */
function updateAccountTaskDates($con, $clsUS, $accounttaskid, $vals, $start, $end, $ignore_assignedto = true) {
    global $core_conf;
    global $messages;
    $qry = new dbUpdate("accounttask");
    $qry->setParam("accounttaskid", $accounttaskid);
    //$qry->setParam("actiondate_flag", 0); //only update accounttask which haven't had their actiondate edited
    if ($ignore_assignedto) $qry->setNullParam("assignedto"); // only update accounttasks which haven't been assigned to anybody
    $qry->setStringParamNotEqual("state", "COMPLETED");
    
    if (isset($vals['week_num']) && $vals['week_num'] > 0) {
        $actiondate = "'".getWeeklyDates($vals, $start, $end)."'";
        $qry->addValue("actiondate", $actiondate);
    } else if (isset($vals['position_in_month']) && $vals['position_in_month'] >= 0 && $vals['position_in_month'] <= 4 ){
        $actiondate = "'".getDayInMonth($vals, $start, $end)."'";
        $qry->addValue("actiondate", $actiondate);
    } else if (isset($vals['taskmonth']) && isset($vals['taskday'])) {
        $actiondate = "'".getMonthDate($vals, $start, $end)."'";
        $qry->addValue("actiondate", $actiondate);
    }
    $qry->setNumberVal("actiondate_flag", 1, "action date flag"); //so superadmin changes won't effect this accounttask action date
    $qry->setAudit($clsUS->userid);
    if($qry->execute($con, false)) {
        //echo "<br/>$accounttaskid: ".$qry->getSQL();
        return true;
    } else {
        $messages[] = $qry->getError();
        return false;
    }    
}

/**
 * send an email to assigned and unassigned users of a task
 * 
 * @global string $rootUrl 
 * @global string $alertsMail
 * @param array $accounttaskid id of the task
 * @param array $newvals the new values, usually the $_POST. $_POST is passed as an arg to remove the dependency
 */
function emailAssignedUsers($accounttask, $newvals, $accounttaskid, $multi=false)
{
    global $rootUrl, $alertsMail, $clsUS, $con;
    $dbvals = $accounttask;
    // don't send if email option is off or set to no
    // this value will not exist in multi select form
    if(isset($newvals['task_email_user']) && $newvals['task_email_user'] == 0) return;
    
    if($newvals['assignedto'] == 0 && $dbvals['assignedto'] == 0) return; // don't send email if no one is assigned

    // if we're in the multi select, check the curent setting for sending email
    //if(!isset($newvals['task_email_user']) && $dbvals['task_email_user'] == 0) return;
    
    $link = $rootUrl;
    
	if ($newvals['assignedto'] > 0) {
		$user = new User($newvals['assignedto']);   
		// is this a new task OR
		// does this task have a new user
		if($dbvals == false || ($dbvals['accounttaskid'] > 0 && $newvals['assignedto'] != $dbvals['assignedto'])  )
		{
			// new email to current user
			$link .= '/modules/tasklist/mytasks.php?accounttaskid='.$accounttaskid;
			// new task email
			$subject = 'You have been assigned a Task';
			$message = getEmailTemplate('task_new', $link);
			
			if (is_array($multi)) {
				$multi[$newvals['assignedto']][] = "You have been assigned to complete a task. You can view the details here - $link";
				//return $multi;
			}
		}
		elseif($dbvals['accounttaskid'] > 0) // an edited task
		{
			// edited
			$link .= '/modules/tasklist/mytasks.php?accounttaskid='.$accounttaskid;
			// updated task
			$subject = 'Your task has been changed';
			$message = getEmailTemplate('task_change', $link);
			
			if (is_array($multi)) {
				$multi[$newvals['assignedto']][] = "One of your tasks has been changed - You can view the details here - $link";
				//return $multi;
			}
			
		}
		if ($multi === false) {
			sendMail($user->username, $alertsMail, $subject, $message);
		}
	}
    
	if ($dbvals['assignedto'] > 0) {
    // if the task has been reassigned
		if($dbvals != false && $dbvals['assignedto'] > 0 && $newvals['assignedto'] != $dbvals['assignedto'])
		{
			$olduser = new User($dbvals['assignedto']);
			
			$subject = 'You are no longer assigned to a Task';
			$message = getEmailTemplate('task_unassign', displayDate($dbvals['actiondate']), displayText($dbvals['customtext']));			
                        
			if (is_array($multi)) {
				$multi[$dbvals['assignedto']][] = "Your task has been reassigned to someone else - It was due on ".displayDate($dbvals['actiondate'])." - ".displayText($dbvals['customtext']);
				//return $multi;
			} else {
				sendMail($olduser->username, $alertsMail, $subject, $message);
			}
			
		}
	}
	if (is_array($multi)) {
		return $multi;
	}
	
}

/**
 * Send an email to an user who has just been assigned multiple tasks
 * 
 * @param int $userid
 * @param int $x number of new tasks
 */
function emailAssignedUserMultitask($multi_msg_arr)
{
    global $rootUrl, $alertsMail;
    
    foreach ($multi_msg_arr as $userid => $this_message) {
        $user = new User($userid);

        $link = $rootUrl."/modules/tasklist/mytasks.php";
        // new task email
        $subject = "Important Multiple Task Alerts";
        $message = getEmailTemplate('task_usermultiassign', $link, implode("\r\n",$this_message));

        sendMail($user->username, $alertsMail, $subject, $message);
    }
}