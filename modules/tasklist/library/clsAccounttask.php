<?php

/**
 * clsTask kind of a simple crud class.  
 * 
 */

class accounttask {
    
    public $rs; //the result set row - this is used to integrate with older functionality and can be removed when that is updated.
    public $row; //same as above - can be removed later
    public $categoryid;
    public $accounttaskid;
    public $taskid; 
    private $clsUS;
    public $repeatingtaskid;
    public $licenceid;
    public $accountid;
    public $actiondate;
    public $accounttask_repeatingid;
    public $customtext;
    public $tasktext;
    public $key_task;
    public $key_task_lock;
    public $notes;
    public $assignedto;
    public $budget;
    public $task_email_user;
    public $compliance;
    
    
    public function __construct($clsUS, $accounttaskid=0) {
        $this->accounttaskid = $accounttaskid;
        $this->clsUS = $clsUS;
    }
    
    public function get($con) {
        $qry = "select *, date(actiondate) as actiondate from accounttask where accounttaskid = {$this->accounttaskid}";
        $rs = getRS($con, $qry);
        $row = getRow($rs);
        $this->rs = $rs;
        $this->row = $row;
        $this->categoryid = $row['categoryid'];
        $this->taskid = $row['taskid'];
        $this->repeatingtaskid = $row['repeatingtaskid'];
        $this->accounttask_repeatingid = $row['accounttask_repeatingid'];
        $this->licenceid = $row['licenceid'];
        $this->accountid = $row['accountid'];
        $this->actiondate = $row['actiondate'];
        $this->customtext = $row['customtext'];
        $this->tasktext = $row['tasktext'];
        $this->key_task = $row['key_task'];
        $this->key_task_lock = $row['key_task_lock'];
        $this->accounttask_repeatingid = $row['accounttask_repeatingid'];
        $this->notes = $row['notes'];
        $this->assignedto = $row['assignedto'];
        $this->budget = $row['budget'];
        $this->task_email_user = $row['task_email_user'];
        //$this->compliance = $row['compliance'];       
    }
    
    public function insert($con, $val_arr, $actiondate_flag=false, $licenceid=0) {
        global $core_conf;
        global $messages;
        $qry = new dbInsert("accounttask");
        if (isset($val_arr['assignedto'])) {
            if($val_arr['assignedto'] > 0) {
                $qry->setNumberVal("assignedto",$val_arr['assignedto'],"Assigned to");
            } else {
                $qry->setNullVal("assignedto");
            } 
            
        }
        if (isset($val_arr['repeatingtaskid'])) $qry->setNumberVal("repeatingtaskid",$val_arr['repeatingtaskid'],"Repeating task id");
        if (isset($val_arr['accounttask_repeatingid'])) $qry->setNumberVal("accounttask_repeatingid",$val_arr['accounttask_repeatingid'],"Accounttask repeating id");
        if (isset($val_arr['taskid'])) $qry->setNumberVal("taskid",$val_arr['taskid'],"Task id");
        if (isset($val_arr['key_task'])) $qry->setNumberVal("key_task",$val_arr['key_task'],"key task");
        if (isset($val_arr['key_task_lock'])) $qry->setNumberVal("key_task_lock",$val_arr['key_task_lock'],"key task lock");
        $qry->setReqNumberVal("accountid",$val_arr['accountid'],"Account id");
        $qry->setReqNumberVal("categoryid",$val_arr["categoryid"],"Category id");
        if ($licenceid > 0) {
            $qry->setReqNumberVal("licenceid",$licenceid,"licence id");
        } else {
            $qry->setReqNumberVal("licenceid",$val_arr['licenceid'],"licence id");
        }
        $qry->setStringVal("tasktext",$val_arr["tasktext"],"Task text");	
        $qry->setStringVal("customtext",$val_arr["customtext"],"Custom Task text");	
        $qry->setReqStringVal("state",$val_arr["state"],"state");	
        if (isset($val_arr['actiondate'])) $qry->setReqDateVal("actiondate", $val_arr['actiondate'], "Action Date");
        if (isset($val_arr['notes'])) $qry->setStringVal("notes", $val_arr['notes'], "Notes");
        if (isset($val_arr['budget'])) $qry->setStringVal("budget", $val_arr['budget'], "budget");
        // compliance HAND-940
        if (isset($val_arr['compliance'])) $qry->setStringVal("compliance", $val_arr['compliance'], "Compliance");
        
        if (isset($val_arr["task_email_user"])) $qry->setNumberVal('task_email_user', $val_arr['task_email_user'], 'Email User');
        if ($actiondate_flag) $qry->setNumberVal("actiondate_flag", 1, "action date flag");
        $qry->setAudit($this->clsUS->userid);
        if ($qry->execute($con)) {
			$this->accounttaskid = $qry->getNewID();
            return true;
        } else {
            $messages[] = $qry->getError();
            $messages[] = ERROR_SAVED;
            return false;
        }    
                      
    }
    

    public function update($con, $val_arr, $licence_end, $tasklistid) {
        global $core_conf;
        global $messages;
        
        if ($val_arr['state'] == "DELETED1") {
            $state = "DELETED2";
        } else {
            $state = $val_arr['state'];
        }
        $qry = new dbUpdate("accounttask");
        $qry->setParam("accounttaskid",$this->accounttaskid);		
        if ($this->customtask()) {
            $qry->setReqNumberVal("categoryid",$val_arr["categoryid"],"Category");
            $qry->setReqStringVal("customtext",$val_arr["customtext"],"Task Text");
        }

        if (isset($val_arr["actiondate"]) && strtotime($val_arr["actiondate"]) <= strtotime($licence_end)) {
            $qry->setReqDateVal("actiondate", $val_arr["actiondate"], "Action Date");
        } else {
            $qry->setReqDateVal("actiondate", "DATE_LATE", "Action Date", $licence_end);
        }

        if (isset($val_arr["actiondate"]) && isset($this->actiondate) && strtotime($val_arr["actiondate"]) != strtotime($this->actiondate)) $qry->setNumberVal("actiondate_flag", 1, "action date flag");
        
        if (!check_user_task_list($this->accountid, $tasklistid, $val_arr["assignedto"], $con) || $val_arr["assignedto"] == 0 || $val_arr["assignedto"] == "" ) {
            $qry->setNullVal("assignedto");
        } else {
            $qry->setNumberVal("assignedto",$val_arr["assignedto"],"Assigned to");
        }
        $qry->setNumberVal("budget",$val_arr['budget'],"Budget");
        $qry->setReqStringVal("state",$state,"state");
        $qry->setStringVal("notes",$val_arr["notes"],"Notes");
        // compliance HAND-940
        if (isset($val_arr['compliance'])) $qry->setStringVal("compliance", $val_arr['compliance'], "Compliance");
        
        if (isset($val_arr["task_email_user"])) $qry->setNumberVal('task_email_user', $val_arr['task_email_user'], 'Email User');
        
        if (isset($val_arr['accounttask_repeatingid'])) $qry->setNumberVal("accounttask_repeatingid",$val_arr['accounttask_repeatingid'],"Accounttask repeating id");
        
        $qry->setAudit($this->clsUS->userid);
        if ($qry->execute($con)) {
           //echo $qry->getSQL();
            return true;
        } else {
            //echo $qry->getSQL();
            $messages[] = ERROR_SAVED;
            $messages[] = $qry->getError();
            return false;
        }
    }

    public function delete($con) {
        global $messages;
        $qry = new dbUpdate("accounttask");
        $qry->setParam("accounttaskid", $this->accounttaskid);
        $qry->setReqStringVal("state", "DELETED", "State");
        $qry->setAudit($this->clsUS->userid);
        if(!$qry->execute($con)) {
            $messages[] = ERROR_SAVED;
            $messages[] = $qry->getError();
            return false;
        } else {
            return true;
        }
    }
    
    /*
     * uncomplete a task
     */
    function uncomplete($con, $state) {
        $qry = new dbUpdate("accounttask");
        $qry->setParam("accounttaskid",$this->accounttaskid);
        $qry->setReqStringVal("state",$state,"state");
        $qry->setNullVal('completedate');
        if(!$qry->execute($con)) {
            $messages[] = ERROR_SAVED;
            $messages[] = $qry->getError();
            return false;
        } else {
            return true;
        }        
    }
    
    public function customtask() {
        if ($this->taskid > 0) return false;
        return true;
    }
}
?>