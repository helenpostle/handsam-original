<?php

/**
 * licence supply task  functions
 */
//get array of userids for active users in this account to check aginst when copying assigned users over later on
function getAccountUsers($accountid, $con) {
    $user_qry = "select userid from usr where accountid = $accountid and state = 'ACTIVE'";
    $user_rs = getRS($con, $user_qry);
    $users_array = array();
    while ($row = getRow($user_rs)) {
        $users_array[] = $row['userid'];
    }
    return $users_array;
}

//gets the licence holiday periods for this licence
function getHolidays($con, $licenceid) {
    $qry = "SELECT * FROM licenceholiday   where licenceid = " . $licenceid . " order by holidaystart asc";
    $result = getRS($con, $qry);
    $holiday_periods = array();
    while ($row = getRow($result)) {
        $holiday_periods = datePeriod($row['holidaystart'], $row['holidayend'], $holiday_periods);
    }
    return $holiday_periods;
}

//gets the last licenceid before the curent one for the same tasklist
function getLastLicence($con, $licenceid, $accountid, $tasklistid) {
    $qry = "SELECT licenceid FROM accountlicence where accountid = $accountid and licenceid != $licenceid and tasklistid = $tasklistid and state != 'DELETED'order by start_date desc limit 0,1";
    $result = getRS($con, $qry);
    $row = getRow($result);
    return $row['licenceid'];
}

// copy custom tasks from previous licence to the new licence
function copyCustomTasks($con, $clsUS, $licenceid, $previous_licenceid, $holiday_periods, $start_date, $end_date, $days_off, $accountid, $users_array, $submit_users) {
    global $messages;
    $qry = "SELECT * FROM accounttask where licenceid = $previous_licenceid and taskid is null and state != 'DELETED' order by actiondate asc";

    $result = getRS($con, $qry);
    while ($row = getRow($result)) {
        $time = strtotime($row['actiondate']);
        $new_actiondate_years = date("Y-n-j", $time);

        if ($time > strtotime($end_date)) {
            $new_actiondate = strftime("%a, %d %b %Y", $time);
        } else {
            //now check if this manufactured date is between the start date and end date
            while (strtotime($start_date) > strtotime($new_actiondate_years)) {
                //if it isn't it must be the following year so add 1 year
                $new_actiondate_years = date("Y-n-j", strtotime("+ 1 year", $time));
                $time = strtotime($new_actiondate_years);
            }
            //reset the new actiondate timestamp variable
            $new_actiondate = $new_actiondate_years;
            $time = strtotime($new_actiondate);
            //for all of the following adjustments, we need to check if the new date still falls within the licence period.  We'll start off by adding days on.  But if the date falls outside the licence period we'll change to taking days off
            //set the modify date variable to add days on.
            $modify_date = "+ 1 day";
            $cnt = 0;
            while ($cnt < 1) {
                $cnt++;
                //check that this date is during a vacation period
                while (in_array($new_actiondate, $holiday_periods)) {
                    $new_actiondate = date("Y-n-j", strtotime($modify_date, $time));
                    $time = strtotime($new_actiondate);
                }
                //get textual day of week for next comparison
                $new_actiondate_d_txt = date('l', $time);
                //now need to check its on a day off and if so move it on to the next day
                while (in_array($new_actiondate_d_txt, $days_off)) {
                    $new_actiondate = date("Y-n-j", strtotime($modify_date, $time));
                    $time = strtotime($new_actiondate);
                    $new_actiondate_d_txt = date('l', $time);
                }

                //check if new actiodate is still inside licence period
                if (!(strtotime($start_date) <= $time && strtotime($new_actiondate) < strtotime($end_date))) {
                    //set modify date to take days off
                    $modify_date = "-1 day";
                    //reset new_actiondate to value before modification
                    $time = strtotime($new_actiondate_years);
                    $new_actiondate = $new_actiondate_years;
                    //reset cnt to process again but taking days off instead
                    $cnt = 0;
                }
            }
            $new_actiondate = strtotime($new_actiondate);
            $new_actiondate = strftime("%a, %d %b %Y", $new_actiondate);
        }
        ///echo "<br/>custom task: {$row['accounttaskid']} actiondate: {$new_actiondate}";
        $accountTask = new accounttask($clsUS);
        $val_arr = $row;
        $val_arr['accountid'] = $accountid;
        $val_arr['licenceid'] = $licenceid;
        $val_arr['actiondate'] = $new_actiondate;

        if ($submit_users == 1 && in_array($row['assignedto'], $users_array)) {
            $val_arr['assignedto'] = $row['assignedto'];
        } else if (isset($val_arr['assignedto'])) {
            unset($val_arr['assignedto']);
        }
        if ($val_arr['state'] != 'INACTIVE')
            $val_arr['state'] = "ACTIVE";

        $val_arr['accounttask_repeatingid'] = createRepeatingTask($clsUS, $val_arr, $licenceid, $accountid, $con, $start_date, $end_date);
        if ($accountTask->insert($con, $val_arr)) {
            $messages[] = "Successfully copied custom task:" . $row["accounttaskid"];
        } else {
            $messages[] = "error copying custom task:" . $row["accounttaskid"];
        }
    }
}

//insert taskday, taskmonth tasks from tasks table into accounttask table for a new licence
//function insertMonthDateLicencetasks($con, $clsUS, $this_day, $this_month, $this_year, $tasklistid, $licenceid, $holiday_periods, $days_off, $accountid, $months) {

function insertMonthDateLicencetasksNoData($con, $clsUS, $start_date, $end_date, $holiday_periods, $days_off, $tasklistid, $accountid, $licenceid, $previous_licenceid) {
    global $messages;

    if ($previous_licenceid) {

        $year_diff = getLicenceYearDiff($accountid, $previous_licenceid, $con, $start_date);

        $sql = "SELECT  task.*, accounttask.state AS ac_state
            , CASE WHEN actiondate is null THEN null ELSE DATE_ADD(accounttask.actiondate,INTERVAL $year_diff YEAR) END as actiondate
            FROM task  LEFT JOIN accounttask ON task.taskid = accounttask.taskid AND accounttask.licenceid = $previous_licenceid 
            WHERE task.tasklistid = $tasklistid AND  task.state =  'ACTIVE' AND task.taskmonth > 0  AND task.taskday > 0 
            ORDER BY taskmonth,taskday,task.taskid ASC;";
    } else {
        $sql = "SELECT  *, null as actiondate FROM task  where tasklistid = " . $tasklistid . " and  state =  'ACTIVE' and taskmonth > 0  and taskday > 0 order by taskmonth,taskday asc";
    }
    $taskResult = getRS($con, $sql);
    $execution_error = array();
    $exec_error = false;
    #insert account tasks for this month

    while ($rowTask = getRow($taskResult)) {
        if ($rowTask['actiondate'] == null) {
            $actiondate = getMonthDate($rowTask, $start_date, $end_date);

            /*
              $date_loop = true;
              while ($date_loop) {
              $day = date("l", strtotime($actiondate));
              if (!in_array($actiondate, $holiday_periods) && !in_array($day, $days_off))  {
              break;
              } else if ($actiondate == $end_date) {
              break;
              } else {
              $actiondate = date("Y-n-j" ,strtotime("$actiondate + 1 day"));
              }
              }
              $actiondate =  strftime("%a, %d %b %Y",strtotime($actiondate));

             */
        } else {
            $actiondate = $rowTask['actiondate'];
        }
        if ($previous_licenceid > 0) {
            $actiondate = calculateNewActionDate($actiondate, $start_date, $end_date, $year_diff, $holiday_periods, $days_off);
        } else {
            $actiondate = calculateActionDateNoPrevLicence($actiondate, $holiday_periods, $days_off, $end_date);
        }

        $accountTask = new accounttask($clsUS);
        $val_arr = $rowTask;
        $val_arr['accountid'] = $accountid;
        $val_arr['licenceid'] = $licenceid;
        $val_arr['actiondate'] = $actiondate;

        if ($previous_licenceid) {
            if ($rowTask['ac_state'] == 'INACTIVE') {
                $val_arr['state'] = 'INACTIVE';
            } else {
                $val_arr['state'] = 'ACTIVE';
            }
        } else {
            $val_arr['state'] = 'ACTIVE';
        }

        $val_arr['accounttask_repeatingid'] = createRepeatingTask($clsUS, $val_arr, $licenceid, $accountid, $con, $start_date, $end_date);

        if (!$accountTask->insert($con, $val_arr)) {
            $execution_error[] = $rowTask["taskid"];
            $messages[] = ERROR_SAVED . " to task id:" . $rowTask["taskid"];
        }
    }
    if (count($execution_error) > 0) {
        $messages[] = implode(",", $execution_error) . " " . TASKS_SUPPLIED_WITH . ERRORS;
    }
}

//insert taskday, taskmonth, accounttask.notes, accounttask.assignedto, accounttask.state  tasks from tasks table into accounttask table for a new licence

function insertMonthDateLicencetasksCopyData($con, $clsUS, $start_date, $end_date, $holiday_periods, $days_off, $tasklistid, $accountid, $licenceid, $previous_licenceid) {
    global $messages;

    $year_diff = getLicenceYearDiff($accountid, $previous_licenceid, $con, $start_date);

    $sql = "SELECT  task.*, accounttask.notes, accounttask.assignedto, CASE WHEN actiondate is null THEN null ELSE DATE_ADD(accounttask.actiondate,INTERVAL $year_diff YEAR) END as actiondate, accounttask.state AS ac_state, 
            usr.state AS usr_state, accounttaskid  
            FROM task  LEFT JOIN accounttask ON task.taskid = accounttask.taskid AND accounttask.licenceid = $previous_licenceid 
            LEFT JOIN usr ON accounttask.assignedto = usr.userid
            WHERE task.tasklistid = $tasklistid AND  task.state =  'ACTIVE' AND task.taskmonth > 0  AND task.taskday > 0 
            ORDER BY taskmonth,taskday,task.taskid ASC";
    $taskResult = getRS($con, $sql);
    $execution_error = array();
    $exec_error = false;
    #insert account tasks for this month
   while ($rowTask = getRow($taskResult)) {
        $accountTask = new accounttask($clsUS);
        $val_arr = $rowTask;
        //echo "<br/>".$val_arr['customtext'];
        $val_arr['accountid'] = $accountid;
        $val_arr['licenceid'] = $licenceid;

        if ($rowTask['actiondate'] == null) {

            $actiondate = getMonthDate($rowTask, $start_date, $end_date);
            /*
              $date_loop = true;
              while ($date_loop) {
              $day = date("l", strtotime($actiondate));
              if (!in_array($actiondate, $holiday_periods) && !in_array($day, $days_off))  {
              break;
              } else if ($actiondate == $end_date) {
              break;
              } else {
              $actiondate = date("Y-n-j" ,strtotime("$actiondate + 1 day"));
              }
              }
              $actiondate =  strftime("%a, %d %b %Y",strtotime($actiondate));
             * 
             */
        } else {
            //$actiondate =  strftime("%a, %d %b %Y",strtotime($rowTask['actiondate']));
            $actiondate = $rowTask['actiondate'];
        }

        if ($previous_licenceid > 0) {
            //echo "<br/>copy  date: taskid: {$rowTask['taskid']}  date 1: $actiondate";
            $actiondate = calculateNewActionDate($actiondate, $start_date, $end_date, $year_diff, $holiday_periods, $days_off);
        } else {
            $actiondate = calculateActionDateNoPrevLicence($actiondate, $holiday_periods, $days_off, $end_date);
        }

        //echo "<br/>copy  date: taskid: {$rowTask['taskid']}  date final: $actiondate";

        $val_arr['actiondate'] = $actiondate;
        if ($rowTask['ac_state'] == 'INACTIVE') {
            $val_arr['state'] = 'INACTIVE';
        } else {
            $val_arr['state'] = 'ACTIVE';
        }
        $val_arr['notes'] = $rowTask['notes'];
        if ($rowTask['usr_state'] == 'ACTIVE') {
            $val_arr['assignedto'] = $rowTask['assignedto'];
        } else {
            $val_arr['assignedto'] = null;
        }
        $val_arr['accounttask_repeatingid'] = createRepeatingTask($clsUS, $val_arr, $licenceid, $accountid, $con, $start_date, $end_date);

        if (!$accountTask->insert($con, $val_arr)) {
            $execution_error[] = $rowTask["taskid"];
            $messages[] = ERROR_SAVED . " to task id:" . $rowTask["taskid"];
        }
    }
     
    if (count($execution_error) > 0) {
        $messages[] = implode(",", $execution_error) . " " . TASKS_SUPPLIED_WITH . ERRORS;
    }
}

//insert weekly tasks from tasks table into accounttask table for a new licence
function insertWeeklyLicencetasks($con, $clsUS, $start_date, $end_date, $holiday_periods, $days_off, $tasklistid, $accountid, $licenceid) {
    global $messages;
    $sql = "SELECT  * FROM task  where tasklistid = " . $tasklistid . " and  state =  'ACTIVE' and week_num > 0  and day_of_week > 0 order by week_num, day_of_week asc";
    $taskResult = getRS($con, $sql);
    $weekly_execution_error = array();
    $exec_error = false;
    #insert account tasks for this month

    while ($rowTask = getRow($taskResult)) {
        $actiondate = getWeeklyDates($rowTask, $start_date, $end_date);

        $date_loop = true;
        while ($date_loop) {
            $day = date("l", strtotime($actiondate));
            if (!in_array($actiondate, $holiday_periods) && !in_array($day, $days_off)) {

                break;
            } else if ($actiondate == $end_date) {

                break;
            } else {

                $actiondate = date('Y-m-d', strtotime("$actiondate + 1 day"));
            }
        }

        $actiondate = strftime("%a, %d %b %Y", strtotime($actiondate));

        //need to check for duplicates  -in case they submitted the supply tasks form twice
        $double_check_qry = "select accounttaskid from accounttask where taskid = {$rowTask["taskid"]} and licenceid = $licenceid";
        $double_check_rs = getRS($con, $double_check_qry);
        if (getResultRows($double_check_rs) == 0) {
            //echo "<br/>weekly tasks  date: taskid: {$rowTask['taskid']}  date: $actiondate";

            $accountTask = new accounttask($clsUS);
            $val_arr = $rowTask;
            $val_arr['accountid'] = $accountid;
            $val_arr['licenceid'] = $licenceid;
            $val_arr['actiondate'] = $actiondate;
            $val_arr['state'] = 'ACTIVE';
            $val_arr['accounttask_repeatingid'] = createRepeatingTask($clsUS, $val_arr, $licenceid, $accountid, $con, $start_date, $end_date);
            if (!$accountTask->insert($con, $val_arr)) {
                $weekly_execution_error[] = $rowTask["taskid"];
                $messages[] = ERROR_SAVED . " to task id:" . $rowTask["taskid"];
            }
        }
    }
    if (count($weekly_execution_error) > 0) {
        $messages[] = implode(",", $weekly_execution_error) . " " . TASKS_SUPPLIED_WITH . ERRORS;
    }
}

//insert month by day tasks from tasks table into accounttask table for a new licence
function insertMonthDayLicencetasks($con, $clsUS, $start_date, $end_date, $holiday_periods, $days_off, $tasklistid, $accountid, $licenceid) {
    global $messages;
    $sql = "SELECT  * FROM task  where tasklistid = " . $tasklistid . " and  state =  'ACTIVE' and taskmonth > 0  and day_of_week > 0 and position_in_month in (0,1,2,3,4) order by taskmonth, position_in_month, day_of_week asc";
    $taskResult = getRS($con, $sql);
    $execution_error = array();
    $exec_error = false;
    #insert account tasks for this month

    while ($rowTask = getRow($taskResult)) {
        if (isset($rowTask['position_in_month']) && $rowTask['position_in_month'] >= 0 && $rowTask['position_in_month'] <= 4) {
            $actiondate = getDayInMonth($rowTask, $start_date, $end_date);
        }
        $date_loop = true;
        while ($date_loop) {
            $day = date("l", strtotime($actiondate));
            if (!in_array($actiondate, $holiday_periods) && !in_array($day, $days_off)) {
                break;
            } else if ($actiondate == $end_date) {
                break;
            } else {
                $actiondate = date("Y-m-d", strtotime("$actiondate + 1 day"));
            }
        }

        $actiondate = strftime("%a, %d %b %Y", strtotime($actiondate));
        //need to check for duplicates  -in case they submitted the supply tasks form twice
        $double_check_qry = "select accounttaskid from accounttask where taskid = {$rowTask["taskid"]} and licenceid = $licenceid";
        $double_check_rs = getRS($con, $double_check_qry);
        if (getResultRows($double_check_rs) == 0) {
            //echo "<br/>month day tasks  date: taskid: {$rowTask['taskid']}  date: $actiondate";

            $accountTask = new accounttask($clsUS);
            $val_arr = $rowTask;
            $val_arr['accountid'] = $accountid;
            $val_arr['licenceid'] = $licenceid;
            $val_arr['actiondate'] = $actiondate;
            $val_arr['state'] = 'ACTIVE';
            $val_arr['accounttask_repeatingid'] = createRepeatingTask($clsUS, $val_arr, $licenceid, $accountid, $con, $start_date, $end_date);
            if (!$accountTask->insert($con, $val_arr)) {
                $execution_error[] = $rowTask["taskid"];
                $messages[] = ERROR_SAVED . " to task id:" . $rowTask["taskid"];
            }
        }
    }
    if (count($execution_error) > 0) {
        $messages[] = implode(",", $execution_error) . " " . TASKS_SUPPLIED_WITH . ERRORS;
    }
}

/*
 * copy  previous licence admin file data over to new licence
 */

function copyTaskData($con, $clsUS, $licenceid, $previous_licenceid, $submit_users, $accountid) {
    global $messages;
    if ($submit_users == 1) {

        //now copy administrator added resources over
        $update_qry = "INSERT INTO task_files (fileid, accounttaskid, created, created_by) 
                        SELECT distinct tf.fileid, a2.accounttaskid, NOW(), {$clsUS->userid} 
                        FROM task_files tf LEFT JOIN files f ON tf.fileid = f.fileid 
                        LEFT JOIN accounttask a1 ON tf.accounttaskid = a1.accounttaskid  
                        LEFT JOIN accounttask a2 ON a1.taskid = a2.taskid AND a2.licenceid=$licenceid  
                        WHERE a1.licenceid=$previous_licenceid  AND f.filetype = 'adminDoc' 
                        AND a1.accountid = $accountid AND a2.accountid = $accountid AND a2.licenceid=$licenceid ";

        $result = execSQL($con, $update_qry);
        if ($result == "") {
            $messages[] = "Admin resources copied over from previous licence";
        } else {
            $messages = "Error copying over admin resources from previous licence: " . $result;
        }
    }
}

function createRepeatingTask($clsUS, $vars, $licenceid, $accountid, $con, $start, $end) {
    //var_dump($vars);

    if (isset($vars["repeatingtaskid"]) && $vars["repeatingtaskid"] > 0) {
        //check for an entry in the accounttask_repeating table for this repeatingtaskid and accountid
        $sql = "select * from accounttask_repeating where accountid = $accountid and licenceid = $licenceid and repeatingtaskid =" . $vars["repeatingtaskid"];

        $rs = getRS($con, $sql);
        if (getResultRows($rs)) {
            $row = getRow($rs);
            $accounttask_repeating_id = $row['id'];
        } else {

            //insert into accounttask_repeating
            $RTask = new RepeatingAccounttask($con, $clsUS, null, $start, $end, $accountid, $vars["repeatingtaskid"], $licenceid);

            //get repeating task criteria
            $qry = "select * from task_repeating where id=" . $vars["repeatingtaskid"];
            $rs2 = getRS($con, $qry);
            $row2 = getRow($rs2);
            $arr = array();
            if ($row2['weekly'] > 0)
                $arr['weekly'] = $row2['weekly'];
            if ($row2['month_date'] > 0)
                $arr['month_date'] = $row2['month_date'];
            if ($row2['month_day'] > 0)
                $arr['month_day'] = $row2['month_day'];
            if ($row2['position_in_month'] !== null)
                $arr['position_in_month'] = $row2['position_in_month'];
            $arr['repeat_duration'] = $row2['repeat_duration'];
            $arr['repeat_end_month'] = $row2['repeat_end_month'];
            $arr['repeat_start_month'] = $row2['repeat_start_month'];
            $arr['repeat_start_day'] = $row2['repeat_start_day'];
            $arr['repeat_end_day'] = $row2['repeat_end_day'];

            $RTask->save($con, false, $arr);
            $accounttask_repeating_id = $RTask->id;
        }
    } else {
        $accounttask_repeating_id = 0;
    }
    return $accounttask_repeating_id;
}

function calculateNewActionDate($given_actiondate, $licence_start, $licence_end, $new_licence_year_diff, $holiday_periods, $days_off) {
    $time = strtotime($given_actiondate);
    $new_actiondate_years = date("Y-n-j", $time);
    //echo "<br/>year: $new_actiondate_years";
    //echo "<br/>lice: $licence_end";
    if ($time == strtotime($licence_end)) {
        $new_actiondate = strftime("%a, %d %b %Y", $time);
    } else if ($time > strtotime($licence_end)) {
        //action date after licence end, so take a year diff off
        $new_actiondate_years = date("Y-n-j", strtotime("-1 year", $time));
        //echo "<br/>new action date 2: $new_actiondate_years";
        $time = strtotime($new_actiondate_years);
        //echo "<br/>timestamp: $time";
        
        $new_actiondate = strftime("%a, %d %b %Y", $time);
        //echo "<br/>date 3: $new_actiondate";
    } else {
        //echo "lic start: $licence_start (".strtotime($licence_start).") action date yrs: $new_actiondate_years (".strtotime($new_actiondate_years).") date diff: $new_licence_year_diff";
        //echo "<br/>new adjsuted date= ".date("Y-n-j", strtotime("+" . $new_licence_year_diff . " year", $time));
        
        //now check if this manufactured date is between the start date and end date
        while (strtotime($licence_start) > strtotime($new_actiondate_years)) {
            //if it isn't it must be the following year so add 1 year
            $new_actiondate_years = date("Y-n-j", strtotime("+1 year", $time));
            //echo "<br/>new action date 3: $new_actiondate_years";
            $time = strtotime($new_actiondate_years);
            $time = strtotime($new_actiondate_years);
        }
        //reset the new actiondate timestamp variable
        $new_actiondate = $new_actiondate_years;
        $time = strtotime($new_actiondate);
        //for all of the following adjustments, we need to check if the new date still falls within the licence period.  We'll start off by adding days on.  But if the date falls outside the licence period we'll change to taking days off
        //set the modify date variable to add days on.
        $modify_date = "+ 1 day";
        $cnt = 0;
        while ($cnt < 1) {
            $cnt++;
            //check that this date is during a vacation period
            while (in_array($new_actiondate, $holiday_periods)) {
                $new_actiondate = date("Y-n-j", strtotime($modify_date, $time));
                $time = strtotime($new_actiondate);
            }
            //get textual day of week for next comparison
            $new_actiondate_d_txt = date('l', $time);
            //now need to check its on a day off and if so move it on to the next day
            while (in_array($new_actiondate_d_txt, $days_off)) {
                $new_actiondate = date("Y-n-j", strtotime($modify_date, $time));
                $time = strtotime($new_actiondate);
                $new_actiondate_d_txt = date('l', $time);
            }

            //check if new actiodate is still inside licence period
            if (!(strtotime($licence_start) <= $time && strtotime($new_actiondate) < strtotime($licence_end))) {
                //set modify date to take days off
                $modify_date = "-1 day";
                //reset new_actiondate to value before modification
                $time = strtotime($new_actiondate_years);
                $new_actiondate = $new_actiondate_years;
                //reset cnt to process again but taking days off instead
                $cnt = 0;
            }
        }
        $new_actiondate = strtotime($new_actiondate);
        $new_actiondate = strftime("%a, %d %b %Y", $new_actiondate);
        
    }
    return $new_actiondate;
}

function getLicenceYearDiff($accountid, $previous_licenceid, $con, $start_date) {
    //get year interval between previous and current licence
    $qry = "SELECT start_date FROM accountlicence where accountid = $accountid and licenceid = $previous_licenceid";
    $result = getRS($con, $qry);
    $row = getRow($result);
    $year1 = strftime("%Y", strtotime($row['start_date']));
    $year2 = strftime("%Y", strtotime($start_date));
    $year_diff = $year2 - $year1;

    if ($year_diff < 1)
        $year_diff = 1;
    return $year_diff;
}

function calculateActionDateNoPrevLicence($actiondate, $holiday_periods, $days_off, $end_date) {
    $date_loop = true;
    while ($date_loop) {
        $day = date("l", strtotime($actiondate));
        if (!in_array($actiondate, $holiday_periods) && !in_array($day, $days_off)) {
            break;
        } else if ($actiondate == $end_date) {
            break;
        } else {
            $actiondate = date("Y-n-j", strtotime("$actiondate + 1 day"));
        }
    }
    $actiondate = strftime("%a, %d %b %Y", strtotime($actiondate));
    return $actiondate;
}

?>