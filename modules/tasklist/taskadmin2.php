<?php

/**
 * account admin account tasks editing
 * must either be an account admin(no licence id needed) or a (super admin with licence id supplied)
 * content: admintasklist.php, adminytaskedit.php
 * boxes: admintaskcategories.php, admintaskoptions.php, admintaskmonths.php
 * 
 */
/**
 * Include basic stuff
 */
$this_page = "tasklistadmin.php";
$secure = true;
require("../../shared/startPage.php");
$pageFiles->addFunc('handsam_uploads');
$pageFiles->addFunc('history');
$pageFiles->addModFunc('select_multi_tasks');
$pageFiles->addModFunc('tasklist_task_edit');
$pageFiles->addModFunc('tasklist_list_tasks');

$pageFiles->includePhp();

$accountid = $clsUS->accountid;
#get taskid for edit page
$taskid = getIntFromRequest("taskid");
$return = getStrFromRequest("return");

#if an edit page, create query now so can get licenceid 

$title = "";
$insert = false;
$insertSuccess = true;

$categoryid = getIntFromRequest("categoryid");

//if edit, get category name for adminTasksInCategory.php box title
if(isset($taskid) && $taskid > 0) {
    $Task = new task ($clsUS, $taskid);
    $Task->get($con);
    $rowTask = $Task->row;
    
    if ($Task->edited_by ==  0 && $Task->edited_by !== "" && $Task->edited_by !== null) $task_hist = getHistoryItem($con, 'task', 'taskid', $taskid);

    $Taskcat = new taskcategory($Task->categoryid);
    $Taskcat->get($con);
    $categoryname = $Taskcat->getCatName();
 
} else if($taskid == 0 && isset($categoryid)) {
    $Taskcat = new taskcategory($categoryid);
    $Taskcat->get($con);
    $categoryname = $Taskcat->getCatName();
}

#check for month, year, options and set to all if none
$tasklistid = getIntFromRequest("tasklistid");
$month = getIntFromRequest("month");
$option = getStrFromRequest("option");
$multi = getStrFromRequest("multi");
$category = getIntFromRequest("category");
$datefield = getIntFromRequest("datefield");

if(!isset($month)) {
    $month = 0;
}

if(!isset($option)) {
    $option = "active";
}
if(!isset($category)) {
    $category = 0;
}

$Tasklist = new tasklist($tasklistid);
$Tasklist->get($con);
$tasklistname = $Tasklist->getTasklistName();

#get category title
if($category > 0) {
    #get options title and option specific sql query
    $Taskcat = new taskcategory($category);
    $Taskcat = get($con);
    $category_name = " : " . $Taskcat->getCatName();
    $categorySQL = "and task.categoryid=" . $category;
} else {
    $category_name = " : All Categories";
    $categorySQL = "";
}

#do multi edit form handling
if($multi == "yes") {
    $multiSaveError = false;

    if(buttonClicked("saveMulti") && isset($_POST['selectMulti'])) {
        $multiSaveError = saveMultiTasks($con, $clsUS);
        if($multiSaveError == false) {
            $messages[] = SAVED;
        } else {
            $messages[] = ERROR_SAVED;
        }
    } else if(buttonClicked("saveMulti") && !isset($_POST['selectMulti'])) {
        $messages[] = NONE_SELECTED;
    }
}

if(isAllowed("editTask")) {
    if(isset($taskid) && $taskid >= 0) {
        //get task history for this task
        $title = "Edit Task";
        $content = "admintaskedit.php";
        
        if(buttonClicked("save")) {

            $taskfiles = new taskFiles();
            $taskfiles->newFiles();
            $taskfiles->rmFilesAdd();
            
            $tasklinks = new taskLinks();
            $tasklinks->newLinks();
            $tasklinks->rmLinksAdd();

            if($taskid == 0) {
                $Task = new task($clsUS);
                if ($Task->insert($con, $_POST)) {

                    $taskfiles->addFiles($con, $clsUS, $Task->taskid);
                    $taskfiles->rmFiles($con, $Task->taskid);

                    $tasklinks->addLinks($con, $clsUS, $Task->taskid);
                    $tasklinks->rmLinks($con, $Task->taskid);

                    $messages[] = SAVED;

                    #add task
                    #first get all active licenses
                    $qryActiveLicences = "select accountlicence.accountid as accountid, accountlicence.licenceid as licenceid from accountlicence left join account on accountlicence.accountid = account.accountid where accountlicence.end_date > NOW() and accountlicence.state = 'ACTIVE' and accountlicence.tasklistid = $tasklistid";
                    $resultActiveLicences = getRS($con, $qryActiveLicences);
                    while ($rowActiveLicence = getRow($resultActiveLicences)) {
                        #check that licence doesn't already have this taskid
                        $rsCheck = getRS($con, "select taskid from accounttask where licenceid = '" . $rowActiveLicence['licenceid'] . "' and taskid = {$Task->taskid}");
                        if(getResultRows($rsCheck) == 0) {
                            $insertSuccess = insertAccountTask($con, $clsUS, $rowActiveLicence['accountid'], $rowActiveLicence['licenceid'], $Task->taskid);
                        }
                    }
                    if(!$insertSuccess)  {
                        $messages[] = ERROR_SAVED;
                    }
                    $messages[] = UPDATED_ACCOUNT_TASKS;
                    if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                    $title = $tasklistname . "  Task List";
                    $content = "admintasklist.php";
                    $box[] = "box.taskmonths.php";
                    $box[] = "box.admintaskoptions.php";
                    $box[] = "box.admintaskcategories.php";
                }
            } else {
                if ($Task->update($con, $_POST)) { 
                    $taskfiles->addFiles($con, $clsUS, $Task->taskid);
                    $taskfiles->rmFiles($con, $Task->taskid);
 
                    $tasklinks->addLinks($con, $clsUs, $Task->taskid);
                    $tasklinks->rmLinks($con, $Task->taskid);

                    $messages[] = SAVED;                    

                    //if the state is active and a licence doesn';t have this task, then insert it into accounttask.
                    if ($_POST['state'] == 'ACTIVE') {
                        $qryActiveLicences = "select accountlicence.accountid as accountid, accountlicence.licenceid as licenceid from accountlicence left join account on accountlicence.accountid = account.accountid where accountlicence.end_date > NOW() and accountlicence.state = 'ACTIVE' and accountlicence.tasklistid = $tasklistid";
                        $resultActiveLicences = getRS($con, $qryActiveLicences);
                        while ($rowActiveLicence = getRow($resultActiveLicences)) {
                            #check that licence doesn't already have this taskid
                            $rsCheck = getRS($con, "select taskid from accounttask where licenceid = '" . $rowActiveLicence['licenceid'] . "' and taskid = '{$Task->taskid}'");
                            if(getResultRows($rsCheck) == 0) {
                                $insertSuccess = insertAccountTask($con, $clsUS, $rowActiveLicence['accountid'], $rowActiveLicence['licenceid'], $Task->taskid);
                            }
                        }  
                    }                                
                    
                    #edit task, update all accounts where state != COMPLETED
                    $qry = new dbUpdate("accounttask");
                    $qry->setParam("taskid", $Task->taskid);
                    $qry->setStringParamNotEqual("state", "COMPLETED");
                    //$qry->setStringVal("tasktext",$_POST["tasktext"],"Task description");
                    $qry->setStringVal("customtext", $_POST["customtext"], "Custom Task text");
                    //$qry->setStringVal("tasktext",$rowTask["tasktext"],"Task text");
                    $qry->setNumberVal("categoryid", $_POST["categoryid"], "Category");
                    $qry->setReqStringVal("flag", "EDITED", "flag");

                    // key task
                    if($core_conf['key_task']) {
                        if(isset($_POST['key_task']) && $_POST['key_task'] == 1) { 
                            $qry->setNumberVal('key_task', 1, 'Key Task');
                        } else {
                            $qry->setNumberVal('key_task', 0, 'Key Task');
                        }
                    }

                    // key task date lock
                    if($core_conf['key_task'] && $core_conf['key_task_lock']) {
                        // can only be set if it's a key task
                        if(isset($_POST['key_task']) && $_POST['key_task'] == 1) {
                            if(isset($_POST['key_task_lock']) && $_POST['key_task_lock'] == 1) {
                                $qry->setNumberVal('key_task_lock', 1, 'Key Task Date Lockdown');
                            } else {
                                $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
                            }
                        } 
                        else {
                            $qry->setNumberVal('key_task_lock', 0, 'Key Task Date Lockdown');
                        } 
                    }

                    if($_POST["state"] == "INACTIVE") {
                        $qry->setReqStringVal("state", "DELETED1", "state");
                    }
                    $qry->setAudit($clsUS->userid);
                    if($qry->execute($con)) {
                        $messages[] = UPDATED_ACCOUNT_TASKS;
                        if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                        $title = $tasklistname . "  Task List";
                        $content = "admintasklist.php";
                        $box[] = "box.taskmonths.php";
                        $box[] = "box.admintaskoptions.php";
                        $box[] = "box.admintaskcategories.php";
                    } else {
                        $messages[] = $qry->getError();
                    }
                }         
            }             
        } else if(buttonClicked("delete")) {
            if ($Task->delete($con)) {

                #now change state to DELETED1 in accounttask table for all state != COMPLETED tasks
                $qry = new dbUpdate("accounttask");
                $qry->setParam("taskid", $Task->taskid);
                $qry->setStringParamNotEqual("state", "COMPLETED");
                $qry->setReqStringVal("state", "DELETED1", "state");
                $qry->setAudit($clsUS->userid);
                if($qry->execute($con)) {
                    
                    $messages[] = UPDATED_ACCOUNT_TASKS;
                    $title = $tasklistname . "  Task List";
                    $content = "admintasklist.php";
                    $box[] = "box.taskmonths.php";
                    $box[] = "box.admintaskoptions.php";
                    $box[] = "box.admintaskcategories.php";
                    if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
                } else {
                    $messages[] = $qry->getError();
                }
            }
        }
        else if(buttonClicked("cancel")) {
            if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
            $title = $tasklistname . "  Task List";
            $content = "admintasklist.php";
            $box[] = "box.taskmonths.php";
            $box[] = "box.admintaskoptions.php";
            $box[] = "box.admintaskcategories.php";
        }
    } else {
        if ($return == "index.php") headerLocation ("$rootUrl/index.php?homepage=1", $messages);
        $title = $tasklistname . "  Task List";
        $content = "admintasklist.php";
        $box[] = "box.taskmonths.php";
        $box[] = "box.admintaskoptions.php";
        $box[] = "box.admintaskcategories.php";
    }
} else {
    //trigger_error("Access Denied",E_USER_WARNING);
    headerLocation("$rootUrl/index.php", false);
}

$listTasks = new listTasks($tasklistid, $option, $categoryid);
$listTasks->getCategorySql();
$listTasks->getList();
$optionqry = $listTasks->optionqry;
$option_title = $listTasks->option_title;
$category_list_task_state = $listTasks->category_list_sql;

# this record set is used in tasklist.php
$optionresult = getRS($con, $optionqry);

if($month == 0)
{
    $listTitle = $option_title . " : All Months" . $category_name;
}
else
{
    $listTitle = $option_title . " : " . $months[$month] . " " . $category_name;
}

#Include layout page
include("layout.php");
