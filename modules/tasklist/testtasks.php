<?php
/**
* account admin account tasks editing
* must either be an account admin(no licence id needed) or a (super admin with licence id supplied)
* content: tasklist.php, taskedit.php
* boxes: taskcategories.php, taskoptions.php
*/
/**
 * Include basic stuff
 */

$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc('tasklist_licence_archive');
$pageFiles->addModFunc('tasklist_accounttask_edit');
$pageFiles->addModFunc('tasklist_copy_task');
$pageFiles->addModFunc('assignto_multi_user');
$pageFiles->addModFunc('select_multi_tasks');
$pageFiles->addFunc('handsam_uploads');
$pageFiles->addCoreLibPhp('funcError/funcLog.php');
$pageFiles->addCoreLibPhp('funcMail/funcMail.php');
$pageFiles->addModFunc('tasklist_task_date');
if ($tasklist_conf['collapse_managetask_resources'] === true) $pageFiles->addModFunc('tasklist_resources');

$pageFiles->addModFunc('tasklist_list_accounttasks');
//new copy task to user function
$pageFiles->addModFunc('tasklist_task_edit');
$pageFiles->includePhp();

$accounttaskid = getIntFromRequest("accounttaskid");

$hasLicence=true;
$hacking = false;
$customTask = false;
$newCustomTask = false;
$return = getStrFromRequest("return");

if(buttonClicked('cancelSearch'))
{
  $_POST['task_search'] = '';
}

$task_search  = getStrFromRequest("task_search");
$task_search_url = '';
if(trim($task_search) != '' )
{
  $task_search_url = urlencode($task_search);
}
$title = "";

$category_list_task_state = " and (accounttask.state = 'ACTIVE' or accounttask.state = 'EDITED' or accounttask.state = 'NEW' or accounttask.state = 'COMPLETED') ";//check if this is needed here

echo "TESTING";
#Include layout page
include("layout.php");
?>