<?php
$this_page = "tasklistadmin.php";

/**
* edit task lists.
* create new task list or click on link to view tasks in this list by month or by category
* content: tasklistlist.php, tasklistedit.php
*/

/**
 * Include basic stuff
 */
$secure = true;
require("../../shared/startPage.php");




$pageFiles->includePhp();

#check if have requested a specific user
$tasklistid = getIntFromRequest("tasklistid");


$title = "";
if (isAllowed("editTasklist")) {
	$title = "Edit Task List";	
	
	if (buttonClicked("cancel")) {
		$content = "tasklistlist.php";
		$title = "Task List";	
	} else {
	
	
		if ($tasklistid !== null) {
			#we can do this if  we have permission to edit accounts.
			$content = "tasklistedit.php";
			if (buttonClicked("save")) {
				#check that a task list doesn't exist with this name
				//$rsCheck = getRS($con,"select * from tasklist where tasklistname = '".mysql_real_escape_string($_POST["tasklistname"])."' and tasklistid != ".$tasklistid);
				$rsCheck = getRS($con,"select * from tasklist where customtext = '".mysql_real_escape_string($_POST["customtext"])."' and customtext != '' and customtext is not null and tasklistid != ".$tasklistid);				
				if (getRow($rsCheck)) {
					$messages[] = TASKLISTNAME_IN_USE;	
				} else {
								
					if ($tasklistid == 0) {
						$qry = new dbInsert("tasklist");
						$qry->setReqStringVal("customtext",$_POST["customtext"],"Custom Tasklist name");
					} else {
						$qry = new dbUpdate("tasklist");
						$qry->setParam("tasklistid",$tasklistid);
						if (isset($rowTasklist['mid']) && $rowTasklist['mid'] > 0) {
							$qry->setStringVal("customtext",$_POST["customtext"],"Custom Tasklist name");
						} else {
							//not a handsam task so need some text!
							$qry->setReqStringVal("customtext",$_POST["customtext"],"Custom Tasklist name");
						}
					}
				
					if (isset($qry)) {
						//$qry->setReqStringVal("tasklistname",$_POST["tasklistname"],"Task list name");
						//$qry->setReqStringVal("state",$_POST["state"],"state");
						$qry->setReqStringVal("state","ACTIVE","state");
						
						$qry->setAudit($clsUS->userid);
						if ($qry->execute($con)) {
							if ($tasklistid == 0) {
								$tasklistid = $qry->getNewID();	
							}
							$messages[] = SAVED;
							$content = "tasklistlist.php";
							
						} else {
                            $messages[] = ERROR_SAVED;
							$messages[] = $qry->getError();	
						}
					}
				}
			} else if (buttonClicked("delete")) {
				
					$qry = new dbUpdate("tasklist");
					$qry->setParam("tasklistid",$tasklistid);
					$qry->setReqStringVal("state","DELETED","State");
					if (!$qry->execute($con)) {
                        $messages[] = ERROR_SAVED;
						$messages[] = $qry->getError();
					} else {
						#change content to list
						$messages[] = DELETED;
						$content = "tasklistlist.php";
						$title = "Task List";	
					}
			} 
			
			
			
		} else {
			#no userid set - are we allowed to view the userlist?
			$content = "tasklistlist.php";
			$title = "Task List";	
		}
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);
	headerLocation("$rootUrl/index.php",false);
}

#Include layout page
include("layout.php");
?>