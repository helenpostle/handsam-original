You have either been assigned some tasks to complete, your current tasks may have been edited, or you have been removed from a task. 

You can view the details of your task list here: 

%s

More details on the exact tasks are listed below: 

%s