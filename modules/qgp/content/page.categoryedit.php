<?php 
/**
 * Shows a form for editing a task category
 */
if (isset($catid) &&  isAllowed("editQGP")) { 
	$rsCategory = getRS($con,"select * from qgp_cat where catid = $catid");
	$rowCategory = getRow($rsCategory); 
	
	echo startFormTable($_SERVER["PHP_SELF"], "", "", "Edit a QGP Category", "Use this form to edit a QGP category. The descriptio field is optional");
	echo frmHiddenField($catid,"catid");
	echo frmTextField($rowCategory,"cat",100,"Task category name",true);
	echo frmTextArea($rowCategory,"cat_desc",5,"Description",false);
	

	
	
	if ($catid != 0 && isAllowed("editQGP")) {
		echo frmShowAudit($rowCategory,$con);	
	}
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($catid != 0 && $file_cnt == 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this category?", "Click here to delete this category");
	
	echo endFormTable();

} 
?>


