<?php
/***
** Shows the list of task categories in the system
** 
** 
***/

$qry = "SELECT * FROM qgp_cat order by cat";

$result = getRS($con, $qry);

		
?>

<table class="acc_list">
	<caption class="hidden"><?php echo $title;?></caption>
	<thead>
		<tr>
			<th class="start ">Category name</th>
			<th class="end ">Description</th>
		</tr>
	</thead>
	<tbody>
<?php
while ($row = getRow($result)) {
?>
  <tr>
	<td class="start catname"><a href="qgp_cat.php?catid=<?php echo $row["catid"];?>"><?php echo $row["cat"] ?></a></td>
	<td class="end state"><a href="qgp_cat.php?catid=<?php echo $row["catid"];?>"><?php echo displayText($row["cat_desc"]) ?></a></td>
  </tr>
<?php } ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>

</table>
