<?php 
/**
 * Shows a form for editing a licence
 */

if (isset($pageVars->licenceid)) { 

	$rslicence = getRS($con,"select * from accountlicence where licenceid = {$pageVars->licenceid}");
	$rowlicence = getRow($rslicence);
	$licenceDuration =  $rowlicence['end_date'] - $rowlicence['start_date'];
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	
	echo startFormTable($_SERVER["PHP_SELF"], "clearfix", "", $frmTitle, "Use this form to edit a QGP licence.");
	
	echo frmHiddenField($accountid,"accountid");
	echo frmHiddenField($pageVars->licenceid,"licenceid");
    $all_tags = (isset($_POST['all_tags'])) ? $_POST['all_tags'] : $rowlicence['all_tags'];
    if ($all_tags == "") $all_tags=0;
    echo frmHiddenField($all_tags, 'all_tags');
	echo frmSelectArray($rowlicence, "state" , $state, "State" , true);
	
        if ($pageVars->licenceid == 0) {
		//$billing_date = 0;
		echo frmDateFieldNonDb ($start_date, "start_date", "Technical Start Date", true);
		echo frmDateFieldNonDb ($billing_date, "billing_date", "Initial/Last Billing Date", false);
	} else {
		echo frmDateField($rowlicence,"start_date","Technical Start date",true);
	//new biling date field
		if ($rowlicence['billing_date'] == "" || $rowlicence['billing_date'] == null ) {
			$billing_date = time();
		} else {
			$billing_date = strtotime($rowlicence['billing_date']);
		}
		echo frmDateFieldNonDb ($billing_date, "billing_date", "Initial/Last Billing Date", false);	
	}

	
	//new contract length field
	if ($pageVars->licenceid == 0) { 
		echo frmTextFieldNonDb("contract_length",20,"Remaining Contract Length (years)",false, $contract_len);
	} else {
		echo frmTextField($rowlicence,"contract_length",20,"Remaining Contract Length (years)",false);
	}

	echo frmTextAreaNonDb("tags",4,"Tags",false,$tags_str);	
	
	if ($accountid != 0 && isAllowed("editAccount")) {
		echo frmShowAudit($rowlicence,$con);	
	}
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($pageVars->licenceid != 0 && isAllowed("editAccount"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this licence?", "Click here to delete this licence");
	
	echo endFormTable();

}
