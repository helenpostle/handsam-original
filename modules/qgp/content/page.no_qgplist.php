<?php
/***
** Shows the list of task categories in the system
** 
** 
***/
echo displayTitle2($title);

echo displayParagraphs("Your account is not licenced to view any QGP documents at the moment. Please contact your account administrator.");
?>
