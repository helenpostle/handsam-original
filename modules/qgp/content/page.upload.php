<?php
/**
 * Shows a  form for uploading a file
 * 
 */

if (isAllowed("editQGP")) {
					
	if (!isset($uploaded_filename) || $uploaded_filename == "") {
		$uploaded_filename = $pageDb->row['filename'];
	}
	echo startFileFormTable($_SERVER["PHP_SELF"],"clearfix", "Upload a Document", UPLOAD_FORM_INFO);

	echo frmHiddenField(htmlspecialchars(serialize($qs_base)),"filter");
	echo frmHiddenField($pageVars->fileid,"fileid");
	echo frmHiddenField($pageVars->accountid,"accountid");
	echo frmHiddenField($uploaded_filename,"uploaded_filename");
	echo frmHiddenField(20000000,"MAX_FILE_SIZE");
	echo frmFileField("filename","Select File" ,true);
	if (isset($uploaded_filename) || $uploaded_filename != "") {
		echo "<div class=\"clearfix optional frm_row\"><span class=\"label\">Uploaded Filename</span><span class=\"value\"><span class=\"upload\"><a href=\"qgp_downloadDocument.php?id=".$pageVars->fileid."\" title=\"\">".$uploaded_filename."</a></span></span></div>";
	}
	
	echo frmHiddenField($uploaded_filename, 'uploaded_filename');
	echo frmHiddenField('QGP', 'filecategory');
	echo frmTextField($pageDb->row,"title",100,"Title",true);
	echo frmTextArea($pageDb->row,"description",5,"Description",false);
	
	//category check boxes
	echo "<div id=\"qgp_cats\" class=\"frmSection clearfix\">";
	echo "<h3>QGP Categories</h3>";
	echo displayParagraphs("You must select at least one category below:");

	$cat_qry = "select catid, cat from qgp_cat order by cat";
	$cat_rs = getRS($con, $cat_qry);
	$cat_str = "";
	while ($cat_row = getRow($cat_rs)) {
		$cat_str .= "<div class=\"optional clearfix\"><span class=\"label\">{$cat_row['cat']}</span><input class=\"inputCheckbox\" type=\"checkbox\" name=\"catid[]\"  value=\"{$cat_row['catid']}\"";
		if (is_array($file_cats) && in_array($cat_row['catid'], $file_cats)) $cat_str .= " checked";
		$cat_str .= "/></div>";
	}
	
	echo $cat_str;
	echo "</div>";	
	echo frmTextAreaNonDb("tags",4,"Tags",false,$tags_str);	
	
	echo frmShowAudit($pageDb->row,$con);	
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel upload");
	if ($pageVars->fileid != 0 && countFileUsed($pageVars->fileid, $con) == 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this file?", "Click here to delete this link");
		
echo endFormTable();


	
}
?>