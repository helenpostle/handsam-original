<?php 
/**
 * box showing all links tags for link editing pages
 */
 #get max number of tags for one item
if (isAllowed("editQGP") || (isset($qgpPermissions) && $qgpPermissions->inTags != "")) {
	if (isAllowed("editQGP")) {
		$qryList = "SELECT count(*) as cnt FROM qgp_tags inner join qgp_file_tags on qgp_tags.tagid = qgp_file_tags.tagid  left join files on qgp_file_tags.fileid = files.fileid where files.filecategory = 'QGP' group by qgp_tags.tagid order by cnt desc limit 0,1";
	} else if (isAllowed("viewQGP") && isset($qgpPermissions)) {
		$qryList = "SELECT count(*) as cnt FROM qgp_tags inner join qgp_file_tags on qgp_tags.tagid = qgp_file_tags.tagid and qgp_file_tags.tagid in ({$qgpPermissions->inTags}) left join files on qgp_file_tags.fileid = files.fileid where files.filecategory = 'QGP' group by qgp_tags.tagid order by cnt desc limit 0,1";

	}
	

	
	$resultList = getRS($con, $qryList);
	$row = getRow($resultList);
	$max = $row['cnt'];
	$class_size = $max / 6;
	$qs = $qs_base;


	if (isAllowed("editQGP")) {
		$qryList = "SELECT qgp_tags.tag, qgp_tags.tagid, count(*) as cnt FROM qgp_tags inner join qgp_file_tags on qgp_tags.tagid = qgp_file_tags.tagid left join files on qgp_file_tags.fileid = files.fileid where files.filecategory = 'QGP' group by qgp_tags.tag, qgp_tags.tagid order by tag asc";
	} else if (isAllowed("viewQGP") && isset($qgpPermissions)) {
		$qryList = "SELECT qgp_tags.tag, qgp_tags.tagid, count(*) as cnt FROM qgp_tags inner join qgp_file_tags on qgp_tags.tagid = qgp_file_tags.tagid and qgp_file_tags.tagid in ({$qgpPermissions->inTags}) left join files on qgp_file_tags.fileid = files.fileid where files.filecategory = 'QGP' group by qgp_tags.tag, qgp_tags.tagid order by tag asc";
	}

	$resultList = getRS($con, $qryList);

	if (getResultRows($resultList) > 0) {
		
		accidentBoxTop("Tags");
			echo "<ul id=\"tag_cloud\">";
			while ($rowList = getRow($resultList)) {
				$class = $rowList['cnt'] / $class_size;
				if (isset($qs_base['tag'])  && strval($qs_base['tag']) == strval($rowList['tagid'])) {
					$class .= " tag_selected";
				}
				//echo "<br/>page tagid: {$qs_base['tag']} tagid: {$rowList['tagid']}";
				echo "<li class=\"size$class\">";
				echo displayText($rowList['tag']);
				$qs['tag'] = $rowList['tagid'];
				$filter_link = "?filter=".rawurlencode(serialize($qs));
				echo frmHiddenField($filter_link,$rowList['tag']);
				echo "</li>";
			} 
			echo "</ul>";
			echo "<div id=\"clear_tag\">clear tag selection";
			$qs['tag'] = 0;
			$filter_link = "?filter=".rawurlencode(serialize($qs));
			echo frmHiddenField($filter_link,"clear_tag");
			echo "</div>";
		accidentBoxBottom();
	}
}
?>
	