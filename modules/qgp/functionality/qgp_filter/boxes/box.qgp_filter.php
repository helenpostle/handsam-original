<?php 
/**
 * Filter box
 * 
 */
if (isAllowed("editQGP") || (isset($qgpPermissions) && $qgpPermissions->inTags != "")) {
	accidentBoxTop("QGP Document List Filter");

		$filter_array = array();
		
		//get array of categories
		$cat_arr = array();
		if (isAllowed("editQGP")) {
			$cat_arr_qry = "select c.cat, c.catid from qgp_cat c left join qgp_file_cat fc on c.catid = fc.catid where fc.catid > 0 group by c.cat, c.catid";
		} else if (isAllowed("viewQGP") && isset($qgpPermissions)) {
			//just get categories that are used in licenced for tags
			
			$cat_arr_qry = "select c.cat, c.catid from qgp_cat c left join qgp_file_cat fc on c.catid = fc.catid LEFT JOIN qgp_file_tags ft ON ft.fileid = fc.fileid where fc.catid > 0 and ft.tagid in ({$qgpPermissions->inTags}) group by c.cat, c.catid";
		}

		//echo $cat_arr_qry;
		
		$cat_arr_rs = getRS($con, $cat_arr_qry);
		
		while ($cat_arr_row = getRow($cat_arr_rs)) {
			$cat_arr[$cat_arr_row['catid']] = $cat_arr_row['cat'];

		}
		
		$filter_array['category'] = $cat_arr;

		$selected = "";
		foreach ($filter_array as $filter_name => $filter_name_array) {
			$qs = $qs_base;
			echo "<div>";
			
			echo "<label for=\"$filter_name\">".ucwords($filter_name)."</label>";
		
			echo "<select id=\"$filter_name\">";
			
			$qs[$filter_name] = 0;
			$filter_link = "?filter=".rawurlencode(serialize($qs));
			echo "<option value=\"$filter_link\">Choose All</option>";
			foreach ($filter_array[$filter_name] as $filter_key => $filter_value) {
				$selected = "";
				$qs[$filter_name] = $filter_key;
				if (isset($qs_base[$filter_name]) && strval($qs_base[$filter_name]) == strval($filter_key)) { 
					$selected = "selected";
					echo $filter_name." : ".$qs_base[$filter_name]." : ".$filter_key;
				} else {
					$selected = "";
				}
				$filter_link = "?filter=".rawurlencode(serialize($qs));
				echo "<option $selected value=\"$filter_link\">$filter_value</option>";
			}
			echo "</select>";
			echo "</div>";
		}
		
	accidentBoxBottom();
}
?>
	