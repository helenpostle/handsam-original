<?php 
/**
 * box showing all links tags for link editing pages
 */
 #get max number of tags for one item
$qryList = "SELECT count(*) as cnt FROM qgp_tags inner join qgp_file_tags on qgp_tags.tagid = qgp_file_tags.tagid  group by qgp_tags.tagid order by cnt desc limit 0,1";
$resultList = getRS($con, $qryList);
$row = getRow($resultList);
$max = $row['cnt'];
$class_size = $max / 6;
 
$qryList = "SELECT qgp_tags.tag, qgp_tags.tagid, count(*) as cnt FROM qgp_tags inner join qgp_file_tags on qgp_tags.tagid = qgp_file_tags.tagid  group by qgp_tags.tag, qgp_tags.tagid order by tag asc";
$resultList = getRS($con, $qryList);

if (getResultRows($resultList) > 0) {
	
	accidentBoxTop("Tags");
		echo "<p>Remember, you may only remove tags from a file if you originally added them.</p>";
		echo "<div class=\"all_tags\">&gt;&gt; All Tags</div>";
		echo "<ul id=\"tag_cloud\">";
		while ($rowList = getRow($resultList)) {
			$class = $rowList['cnt'] / $class_size;
			echo "<li class=\"size$class\">";
			echo displayText($rowList['tag']);
			echo "</li>";
	 	} 
		echo "</ul>";
	
	accidentBoxBottom();
}
?>
	