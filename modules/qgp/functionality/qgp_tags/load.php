<?php
$this->addCoreLibPhp("qgp/clsQgpTags.php");
$this->addModuleLibPhp("clsQgpTagsLicence.php");
$this->addCoreLibJs("jquery/plugins/jquery.rich-array.js");
$this->addModuleFuncJs("add_tags.js");
$this->addModuleBox("box.tag_cloud.php");
$this->addModuleFuncCss("tags.css");
