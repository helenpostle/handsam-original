<?php
class qgpTagsLicence  {

	var $licenceid;
	var $item_tags = array();
	var $messages = array();
	var $tags_str = "";
    
    
    function qgpTagsLicence($licenceid) {
	    $this->licenceid = $licenceid;
    }
    
    
    function deleteLicenceTags($con) {
	    foreach ($this->item_tags as $tagid => $tag ) {
		    
		    //delete from qgp_file_tags
			$qry = new dbDelete("qgp_licence_tags");
			$qry->setParam("licenceid",$this->licenceid);
			$qry->setParam("tagid",$tagid);
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			}
	    }
    }
    
    
    function insertLicenceTag($tagid, $con) {
	    $sql = "select tag_licence_id from qgp_licence_tags where tagid = $tagid and licenceid = $this->licenceid";
	   	$rsCheck = getRS($con,$sql);
		//echo $sql;
		if (getResultRows($rsCheck) == 0) {
			//insert tag
			$qry = new dbInsert("qgp_licence_tags");
			$qry->setReqNumberVal("tagid",$tagid,"tagid");
			$qry->setReqNumberVal("licenceid",$this->licenceid, "licenceid");
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			}

		}
    }
    
  
    
    function getTagid($tag, $clsUS, $con) {
	    $tag = trim($tag);
		//check if tag already exists
	   	$rsCheck = getRS($con, "select tagid from qgp_tags where tag = '".mysql_real_escape_string($tag)."'");
		//check if tag exists
		if (getResultRows($rsCheck) > 0) {
			$row = getRow($rsCheck);
       		return $row['tagid'];
		} else {
			return 0;
		}
    }    
    
      
    
    function saveTags($tag_str, $clsUS, $con) {
	    
	    //first get an array with all current tags for this item
	    $this->getTags($con);
	    //var_dump ($this->item_tags);
	    
		$tags = array();
		$tags = explode(",", $tag_str);
		$tags = array_filter($tags);
		//var_dump ($tags);
		foreach ($tags as $tag) {
			if (in_array($tag, $this->item_tags)) {
				//remove from array
				$this->item_tags = array_diff($this->item_tags, array($tag));
			} else {
				//insert tag
				$tagid = $this->getTagid($tag, $clsUS, $con);
				if ($tagid > 0) $this->insertLicenceTag($tagid, $con);
			}
		}
		
		//now delete tags left in the item_tags array
		$this->deleteLicenceTags($con); 

    }
    
    function delTags($con) {
	    //first get an array with all current tags for this item
	    $this->getTags($con);
		//now delete tags left in the item_tags array
		$this->deleteLicenceTags($con); 
    }
    
    
    function getTags($con) {
	    if (count($this->item_tags) == 0) {
            //is this licence 'all_tags'
            $qry = "select all_tags from accountlicence where licenceid=".$this->licenceid;
            $rs = getRS($con, $qry);
            $row = getRow($rs);
            if ($row['all_tags'] == 1) {
                $qry = "select * from qgp_tags left join qgp_file_tags on qgp_tags.tagid = qgp_file_tags.tagid order by tag asc"; 
            } else {
                $qry = "select * from qgp_tags left join qgp_licence_tags on qgp_tags.tagid = qgp_licence_tags.tagid where qgp_licence_tags.licenceid = {$this->licenceid}  and qgp_tags.tagid > 0 order by tag asc"; 
            }
	  	    $rs = getRS($con,$qry);
	       	while($row = getRow($rs)) {
				$this->item_tags[$row['tagid']] = $row['tag'];
	       	}
       	}
    }
     
    function getTagsStr($con) {
	    $this->getTags($con);
	    return implode(",", $this->item_tags);
    } 

    function getTagsLi($con) {
	    $this->getTags($con);
	    return "<li>".implode("</li><li>", $this->item_tags)."</li>";
    }
  
    
}
?>