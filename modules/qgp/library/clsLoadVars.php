<?php

/**
 * Class to load variables from GET, POST, user session depending on security
 */
class loadVars {

	var $userid;
	var $accountid;
	var $fileid;
	var $licenceid;
	var $hacking = false;
	
	
	function loadVars($clsUS,$con) {
		if (isAllowed("editAccount")) {
			$this->userid = getIntFromRequest("userid");
			$this->accountid = getIntFromRequest("accountid");
		} else {
			$this->userid = $clsUS->userid;
			$this->accountid = $clsUS->accountid;
		} 
		$this->fileid = $this->checkFileId($clsUS,$con);
		$this->licenceid = $this->checkLicenceId($clsUS,$con);
	}
	

	
	function checkFileId($clsUS,$con) {
		$id = getIntFromRequest("fileid");
		if ($id === 0) return 0;
		if ($id === "") return "";
		if ($id === null) return null;
		if (isAllowed("editQgp")) {
			return $id;
		} else if ($id > 0) {
			//bit where we check for licence for tags etc goes here
			return $id;
		}
		$this->hacking = true;
		return null;
	}
		

	function checkLicenceId($clsUS,$con) {
		$id = getIntFromRequest("licenceid");
		if ($id === 0) return 0;
		if ($id === "") return "";
		if ($id === null) return null;
		if (isAllowed("editAccount")) {
			return $id;
		}
		$this->hacking = true;
		return null;
	}
		

	
}
	
?>