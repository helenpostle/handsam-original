<?php
class qgpTags  {

	var $fileid;
	var $item_tags = array();
	var $messages = array();
	var $tags_str = "";
    
    
    function qgpTags($fileid) {
	    $this->fileid = $fileid;
    }
    
    
    function deleteFileTags($con) {
	    foreach ($this->item_tags as $tagid => $tag ) {
		    
		    //delete from qgp_file_tags
			$qry = new dbDelete("qgp_file_tags");
			$qry->setParam("fileid",$this->fileid);
			$qry->setParam("tagid",$tagid);
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			}
		    		    
	    	//Now count if theres any more in qgp_file_tags
	    	$rsCheck = getRS($con,"select tag_fileid from qgp_file_tags where tagid = $tagid");
			//check if tag exists
			if (getResultRows($rsCheck) == 0) {
				//delete from tags table
				$del_qry = new dbDelete("qgp_tags");
				$del_qry->setParam("tagid",$tagid);
				if (!$del_qry->execute($con)) {
					$this->messages[] = $del_qry->getError();
				}
			}
	    }
    }
    
    
    function insertFileTag($tagid, $con) {
	   	$rsCheck = getRS($con,"select tag_fileid from qgp_file_tags where tagid = $tagid and fileid = $this->fileid");

		if (getResultRows($rsCheck) == 0) {
			//insert tag
			$qry = new dbInsert("qgp_file_tags");
			$qry->setReqNumberVal("tagid",$tagid,"tagid");
			$qry->setReqNumberVal("fileid",$this->fileid, "fileid");
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			}

		}
    }
    
    
    
    function insertTag($tag, $clsUS, $con) {
	    $tag = trim($tag);
		//check if tag already exists
	   	$rsCheck = getRS($con,"select tagid from qgp_tags where tag = '$tag'");
		//check if tag exists
		if (getResultRows($rsCheck) == 0) {
			//insert tag
			$qry = new dbInsert("qgp_tags");
			$qry->setReqStringVal("tag",$tag, "tag");
			$qry->setAudit($clsUS->userid);
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			} else {
				//return new tagid
				return $qry->getNewID();
			}
		} else {
		    $row = getRow($rsCheck);
			//return existing tagid
       		return $row['tagid'];
		}
    }
    
    
    function saveTags($tag_str, $clsUS, $con) {
	    
	    //first get an array with all current tags for this item
	    $this->getTags($con);
	    //var_dump ($this->item_tags);
	    
		$tags = array();
		$tags = explode(",", $tag_str);
		$tags = array_filter($tags);
		//var_dump ($tags);
		foreach ($tags as $tag) {
			if (in_array($tag, $this->item_tags)) {
				//remove from array
				$this->item_tags = array_diff($this->item_tags, array($tag));
			} else {
				//insert tag
				$tagid = $this->insertTag($tag, $clsUS, $con);
				$this->insertFileTag($tagid, $con);
			}
		}
		
		//now delete tags left in the item_tags array
		$this->deleteFileTags($con); 

    }
    
    function delTags($con) {
	    //first get an array with all current tags for this item
	    $this->getTags($con);
		//now delete tags left in the item_tags array
		$this->deleteFileTags($con); 
    }
    
    
    function getTags($con) {
	    if (count($this->item_tags) == 0) {
		    $qry = "select * from qgp_tags left join qgp_file_tags on qgp_tags.tagid = qgp_file_tags.tagid where qgp_file_tags.fileid = {$this->fileid} order by tag asc"; 
	  	    $rs = getRS($con,$qry);
	       	while($row = getRow($rs)) {
				$this->item_tags[$row['tagid']] = $row['tag'];
	       	}
       	}
    }
     
    function getTagsStr($con) {
	    $this->getTags($con);
	    return implode(",", $this->item_tags);
    }
    
 
    function getTagsLi($con) {
	    $this->getTags($con);
	    return "<li>".implode("<li/><li>", $this->item_tags)."</li>";
    }
  
     
}
?>