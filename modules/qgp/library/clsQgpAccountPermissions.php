<?php
class qgpAccountPermissions  {

	var $licences = array();
	var $tags = array();
	var $inTags = "";
	var $accountid = "";
  var $all_tags = false;
    
    function qgpAccountPermissions($accountid, $con) {
	    $this->accountid = $accountid;
		if ($accountid > 0) {
			$this->getActiveLicences($con);
			$this->getAllLicencedTags($con);
			$this->inTags();
		}
    }
    
    
    function getActiveLicences($con) {
	    $qry = "select licenceid, all_tags from accountlicence where accountid = {$this->accountid} and appid = 5 and (state = 'ACTIVE' ) and end_date > NOW() order by end_date asc";
	    $rs = getRS($con,$qry);
	    while ($row=getRow($rs)) {
		    $this->licences[] = $row['licenceid'];
            if ($row['all_tags'] == 1) $this->all_tags = true;
	    }
    }
    
	function getAllLicencedTags($con) {
		if (count($this->licences) > 0) {
			$licenceids = implode (',', $this->licences);
		} else {
			$licenceids=0;
		}
        if ($this->all_tags) {
            $qry = "select distinct qgp_tags.tag, qgp_tags.tagid from qgp_file_tags left join qgp_tags on qgp_file_tags.tagid = qgp_tags.tagid";
        } else {
            $qry = "select distinct qgp_tags.tag,  qgp_tags.tagid from qgp_licence_tags left join qgp_tags on qgp_licence_tags.tagid = qgp_tags.tagid where licenceid in ($licenceids)  and qgp_tags.tagid>0";
        }
	    $rs = getRS($con,$qry);
	    while ($row=getRow($rs)) {
		    if ($row['tagid'] > 0) $this->tags[$row['tag']] = $row['tagid'];
	    }
	}
	
	
	function inTags() {
		$this->inTags = implode (',', array_filter($this->tags));	
	}	  
    
}
?>