<?php 
/**
 * lists QGP admin menu options 
 * 
 */
 
accidentBoxTop("QGP Active Licences");

if (isAllowed("editAccount")) {
	$qry = "select * from accountlicence where accountid = $accountid and appid = 5 and (state = 'ACTIVE' or state = 'INACTIVE') and end_date > NOW() and licenceid != {$pageVars->licenceid} order by end_date asc";

	$rsQgp = getRS($con,$qry);
	if (getResultRows($rsQgp) > 0) {
		while ($row=getRow($rsQgp)) {
			echo "<h3> End Date: ".displayDate($row['end_date'])."</h3>";
			echo "<p>Tags: </p>";
			//now get tags for this licence
			$lic_tags = new qgpTagsLicence($row['licenceid']);
			$lic_tags_li = $lic_tags->getTagsLi($con);

			echo "<ul class=\"active_licences\">";
			echo $lic_tags_li;
			echo "</ul>";
		}

	}	
}

accidentBoxBottom();