<?php
/**
* qgp module licence editing functionality
* content: page.licenceedit.php
*
*/
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
 
require("../../shared/startPage.php");

$pageFiles->addFunc('licence');
$pageFiles->addModFunc('qgp_tags');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$menuPage = "qgp_admin.php";
$contract_len = "";

#check if have requested a specific user
$accountid = getIntFromRequest("accountid");
$title = "QGP Module licence";
if (isAllowed("editAccount")) {
	
	//get account title for page title
	$qry = "SELECT accountname FROM account where accountid = '".$accountid."';";
	$result = getRS($con, $qry);
	$row = getRow($result);
	$title = $row['accountname'];
	$billing_date = getStrFromRequest('billing_date');
	if ($billing_date) {
		$billing_date = strtotime($billing_date);
	} else {
		$billing_date = time();
	}

	$start_date = getStrFromRequest('start_date');
	if ($start_date) {
		$start_date = strtotime($start_date);
	} else {
		$start_date = time();
	}

	if ($pageVars->licenceid == 0) {
		//add new licence/renew licence
		$qry = "SELECT * FROM accountlicence  where accountlicence.accountid = ".$accountid." and accountlicence.appid = 5 and accountlicence.state != 'DELETED' order by accountlicence.licenceid desc";
		//echo $qry;
		$result = getRS($con, $qry);
		$row = getRow($result);
		//if old licence still current, show renew link
		if (strtotime($row['end_date']) > strtotime(strftime("%Y-%m-%d",time())) && $row['state'] == 'ACTIVE') {
			$frmTitle = "QGP : Renew licence";
			//start date (for date field) = current licence end date
			$start_date = (strtotime($row['end_date']));
		} else {
			//otherwise show create link
			$frmTitle = "QGP : Create licence";
			//$start_date = time();
		}
		
		//set last billing date from old licence too:
		$billing_date = strtotime($row['billing_date']);
		$contract_len = $row['contract_length'] - 1;
		if ($contract_len < 0) $contract_len = 0;

	} else {
		$qry = "SELECT * FROM accountlicence WHERE licenceid = '{$pageVars->licenceid}' and appid = 5 and state != 'DELETED'";
		//echo $qry;
		$rs_licence = getRS($con, $qry);
		$cnt_licence = getResultRows($rs_licence);
		$row_licence = getRow($rs_licence);
		$frmTitle = "QGP : Edit licence: licence Id ".$pageVars->licenceid;
	}
	$content = "page.licenceedit.php";
	$box[] = "box.qgp_active_licences.php";
	if (buttonClicked("cancel")) {
		#return to accounts page
		headerLocation ("$rootUrl/handsam_core/accounts.php?accountid=$accountid", false);
	} 
	
	
	if ($pageVars->licenceid !== null) {

		if (buttonClicked("save")) {
							
			if ($pageVars->licenceid == 0) {
				$qry = new dbInsert("accountlicence");
				$qry->setReqNumberVal("appid",5,"app id");
				$qry->setReqStringVal("state",$_POST['state'],"state");
			} else {
				$qry = new dbUpdate("accountlicence");
				$qry->setParam("licenceid",$pageVars->licenceid);
				$qry->setReqStringVal("state",$_POST["state"],"state");
			}
		
			if (isset($qry)) {
				#calculate end date from start date plus 1 year duration
				$END_YEAR = strftime("%Y",strtotime($_POST["start_date"])) + 1;
				$END_DATE = strftime("%a, %d %b %Y",(strtotime(strftime("%d %b ".$END_YEAR,strtotime($_POST["start_date"])))));
				$qry->setReqDateVal("start_date", $_POST["start_date"], "Start date");
				$qry->setDateVal("billing_date", $_POST["billing_date"], "Billing date");
				$qry->setNumberVal("contract_length",$_POST["contract_length"],"Billing period");
				$qry->setReqNumberVal("accountid",$_POST["accountid"],"Account id");
				$qry->setReqNumberVal("all_tags",$_POST["all_tags"],"All Tags");
				$qry->setReqDateVal("end_date", $END_DATE, "End date");
				
			
				$qry->setAudit($clsUS->userid);
				
				if ($qry->execute($con)) {
					if ($pageVars->licenceid == 0) {
						$pageVars->licenceid = $qry->getNewID();
						
						
					}
					
					$tags = new qgpTagsLicence($pageVars->licenceid);
					$tags->saveTags($_POST['tags'], $clsUS, $con);
					
					$messages[] = SAVED;
					
					#return to accounts page
					headerLocation ("$rootUrl/handsam_core/accounts.php?accountid=$accountid", $messages);
					
				} else {
					$messages[] = $qry->getError();	
					$tags_str = $_POST['tags'];
				}
			}
			
		} else if (buttonClicked("delete")) {
			#cannot delete self
				$qry = new dbUpdate("accountlicence");
				$qry->setParam("licenceid",$pageVars->licenceid);
				$qry->setReqStringVal("state","DELETED","State");
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#return to accounts page
					$messages[] = DELETED;
					headerLocation ("$rootUrl/handsam_core/accounts.php?accountid=$accountid", $messages);
				}
		} else {
			$tags = new qgpTagsLicence($pageVars->licenceid);
			$tags_str = $tags->getTagsStr($con);

		}
	}
	
} else {
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);
}

if (isAllowed("editAccount")) { 
	$this_page = "accounts.php";
}

#Include layout page
include("../../layout.php");
