<?php
/**
* Securely Download a file from the database
*/
//MUST ADD SECURITUY AND CHECK FILE EXISTS
$content = "";
//make this a secure page
$secure = true;
//include basic stuff
include ("../../shared/startPage.php");

$pageFiles->addModFunc("qgp_account_permissions");

$pageFiles->includePhp();
//specify page content
$title = "Download Files";		//the <title> tag and <h1> page title

$pageVars = new loadVars($clsUS,$con);
$qgpPermissions = new qgpAccountPermissions($pageVars->accountid, $con);

//get the id 
$id = getIntFromRequest("id");
$rsCheckContent = getRS($con,"select * from files where fileid = $id and filecategory = 'QGP'");

if (getRow($rsCheckContent) && (isAllowed("editQGP") || ($qgpPermissions->inTags != "" && isAllowed("viewQGP")))) {

	//get the record set
	if (isAllowed("editQGP")) {
		$sql = "select * from files where fileid = $id";
	} else if(isAllowed("viewQGP")) {
		$sql = "select files.* from files inner join qgp_file_tags on qgp_file_tags.fileid = files.fileid where files.fileid = $id and qgp_file_tags.tagid in ({$qgpPermissions->inTags})";

	}
	$rsFile = getRS($con, $sql);
	if ($rowFile = getRow($rsFile)) {
		//download the file
		$filename = htmlspecialchars_decode($rowFile["filename"]);
		$path = $handsamdocs_fp;
		
		
		$file = $path.$filename;
		
		// send the right headers

		header("Content-Type: application/octet-stream");
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		
		if ($rowFile['mid'] > 0 && $rowFile['filetype'] == "handsamDoc") {
			$tjs = time() + 60000;
			$file =  $core_conf['handsam_docs_url']."?id=".$rowFile['mid']."&tjs=$tjs";
            //die($file);
			echo file_get_contents($file);
			exit;
		} else {
			if (@file_exists($file)) echo file_get_contents($file);
			exit;
		}
	}
}