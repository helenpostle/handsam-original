<?php
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc('qgp_upload');
$pageFiles->addModFunc('qgp_tags');
$pageFiles->addModFunc('qgp_cats');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

//priority for module functionality boxes over page boxes
$pageFiles->addBoxPriority();

$tags_str = "";

$menuPage = "qgp_admin.php";

//get this file's categories as array this is used to populate form and in the delete function later
$file_cats = getFileCatsArr($con, $pageVars->fileid);

//$to_file_path = $qgpdocs_fp;
$to_file_path = $handsamdocs_fp;


if (isAllowed("editQGP")) {

	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	} else {
		$qs = "";
	}
	//set a default value for use in filter
	$qs_base = $qs;
		
	$title = "QGP Document Edit";
	$content = "page.upload.php";
	//$box[] = "box.admin.php";

	if ($pageVars->fileid !== null) {
		
		//get file data
		$qry = "select f.title, f.filename, f.fileid, f.description, f.created, f.edited, f.created_by, f.edited_by FROM files f where fileid = {$pageVars->fileid} and filecategory = 'QGP'";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		if (buttonClicked("save")) {
			$result = qgpUpload($clsUS, $pageVars, $con, $to_file_path);
			if (is_int($result)) {
				$fileid = $result;
                                
				$tags = new qgpTags($fileid);
				$tags->saveTags($_POST['tags'], $clsUS, $con);
				
				saveFileCats($fileid, $con);
				headerLocation("qgp_admin.php?filter=".urlencode(serialize($qs))."&rand=".rand(),$messages);
			} else {
				$uploaded_filename = $result;
				$file_cats = $_POST['catid'];
				$tags_str = $_POST['tags'];
				
			}
		} else if (buttonClicked("cancel")) {
			headerLocation("qgp_admin.php?filter=".urlencode(serialize($qs))."&rand=".rand(),$messages);
		} else if (buttonClicked("delete") && countFileUsed($pageVars->fileid, $con) == 0) {
			if ($pageVars->fileid != 0) {
				$tags = new qgpTags($pageVars->fileid);
				$tags->delTags($con);
				deleteFileCats($file_cats, $pageVars->fileid, $con);
				$fileid = qgpDelete($pageVars, $con, $to_file_path);
				if ($fileid) {
					headerLocation("qgp_admin.php?filter=".urlencode(serialize($qs))."&rand=".rand(),$messages);
				}
			}
		} else {
			$tags = new qgpTags($pageVars->fileid);
			$tags_str = $tags->getTagsStr($con);
		}
	} else {
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);
	headerLocation("$rootUrl/index.php",false);
}
include("../../layout.php");
?>