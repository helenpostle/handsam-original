<?php
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc("qgp_list");
$pageFiles->addModFunc("qgp_filter");

$pageFiles->includePhp();
//priority for module functionality boxes over page boxes
$pageFiles->addBoxPriority();
$qs = array();

$pageVars = new loadVars($clsUS,$con);

if (isAllowed("editQGP")) {
	
	$title = "QGP Document Module";
	$content = "page.qgplist.php";
	
	//get filter array querystring
	
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	//set a default value for use in filter
	$qs_base = $qs;
	$filter_title = array();
	$filter_qry = array();
	//process filters
	//category filter
	if (is_array($qs) && array_key_exists('category', $qs) && $qs['category'] > 0) {
		$filter_qry['category'] = " fc.catid = ".mysql_real_escape_string($qs['category']);
		//get cat name from db
		$cat_qry = "select cat from qgp_cat where catid = ".mysql_real_escape_string($qs['category']);
		$cat_rs = getRS($con, $cat_qry);
		$row = getRow($cat_rs);
		$filter_title['category'] = "Category - {$row['cat']}";
	} else {
		$filter_qry['category'] = "1=1";
		$filter_title['category'] = "All Categories";
	}
	
	//tag filter
	if (is_array($qs) && array_key_exists('tag', $qs) && $qs['tag'] > 0) {
		$filter_qry['tag'] = " ft.tagid = ".mysql_real_escape_string($qs['tag']);
		//get tag name from db
		$tag_qry = "select tag from qgp_tags where tagid = ".mysql_real_escape_string($qs['tag']);
		$tag_rs = getRS($con, $tag_qry);
		$row = getRow($tag_rs);
		$filter_title['tag'] = "Tag - {$row['tag']}";
	} else {
		$filter_qry['tag'] = "2=2";
		$filter_title['tag'] = "All Tags";
	}
	
	/*search box*/
	
	if (isset($_POST['search'])) {
		$qs['search'] = trim($_POST['qgp_search']);
		$qs_base['search'] = trim($_POST['qgp_search']);
	}
		
	if (is_array($qs) && array_key_exists('search', $qs) && $qs['search'] != "") {
		$filter_qry['search'] = " f.title like '%".mysql_real_escape_string($qs['search'])."%' ";
		$filter_title['search'] = "Search - &quot;{$qs['search']}&quot;";
	} else {
		$filter_qry['search'] = "1=1";
		$filter_title['search'] = "No search term";
	}
	
	//now implode filter title
	$filter_title_str = implode(": ",$filter_title);	
	
	$pageDb = getQgpList($pageVars, $con, $filter_qry, "QGP");

	

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>
