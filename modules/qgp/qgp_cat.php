<?php
/**
* list and edit task categories
* content: taskcategorylist.php, taskcategoryedit.php
* boxes: none
*/

/**
 * Include basic stuff
 */
$secure = true;
 
require("../../shared/startPage.php");
$pageFiles->addModFunc('qgp_cats');

$pageFiles->includePhp();

#check if have requested a specific category
$catid = getIntFromRequest("catid");

$menuPage = "qgp_admin.php";

$title = "";

#we can do this if  we have permission to edit tasks.
if (isAllowed("editQGP")) {
	$title = "Edit QGP Category";	
	
	if (buttonClicked("cancel")) {
		#change content to list
		$content = "page.categorylist.php";
		$title = "QGP Category List";
		$box[] = "box.admin.php";

		
	} else {
	
	
		if ($catid !== null) {
			$file_cnt = catFileCount($con, $catid);
			$content = "page.categoryedit.php";	
			
			if (buttonClicked("save")) {
				#check that a category doesn't exist with this name
				$rsCheck = getRS($con,"select * from qgp_cat where cat = '".mysql_real_escape_string($_POST["cat"])."' and catid != $catid");
				if (getRow($rsCheck)) {
					$messages[] = "This category name is already in use, please enter a different one";	
				} else {
								
					if ($catid == 0) {
						$qry = new dbInsert("qgp_cat");
					} else {
						$qry = new dbUpdate("qgp_cat");
						$qry->setParam("catid",$catid);
					}
				
					if (isset($qry)) {
						$qry->setReqStringVal("cat",$_POST["cat"],"Category name");
						$qry->setStringVal("cat_desc",$_POST["cat_desc"],"Category description");
						$qry->setAudit($clsUS->userid);
						
						if ($qry->execute($con)) {
							if ($catid == 0) {
								$catid = $qry->getNewID();	
							}
							$messages[] = SAVED;
							#change content to list
							$content = "page.categorylist.php";
							$box[] = "box.admin.php";
						} else {
							$messages[] = $qry->getError();	
						}
					}
				}
			} else if (buttonClicked("delete") && $file_cnt == 0) {
				
					$qry = new dbDelete("qgp_cat");
					$qry->setParam("catid",$catid);
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					} else {
						#change content to list
						$messages[] = DELETED;
						$content = "page.categorylist.php";
						$title = "QGP Category List";
						$box[] = "box.admin.php";
					}
			} 
			
			
			
		} else {
			$content = "page.categorylist.php";
			$title = "QGP Category List";
			$box[] = "box.admin.php";	
		}
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);
	headerLocation("$rootUrl/index.php",false);
}

#Include layout page
include("../../layout.php");
?>