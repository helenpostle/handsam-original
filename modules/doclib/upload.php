<?php
/**
 * Include basic stuff
 */

// make this a secure page
$secure = true;
ini_set('max_execution_time', '0');
require("../../shared/startPage.php");

$pageFiles->addModFunc('qgp_upload');
$pageFiles->addModFunc('qgp_tags');
$pageFiles->addModFunc('qgp_cats');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS, $con);

// priority for module functionality boxes over page boxes
$pageFiles->addBoxPriority();

$tags_str = "";

$menuPage = "admin.php";

//get this file's categories as array this is used to populate form and in the delete function later
$file_cats = getFileCatsArr($con, $pageVars->fileid);

//$to_file_path = $qgpdocs_fp;
$to_file_path = $handsamdocs_fp;

if(isAllowed("editDocLib"))
{
    $qs = '';
    if(isset($_POST['filter']))
    {
        $qsfrm = $_POST['filter'];
        $qs = __unserialize($_POST['filter']);
    }
    else if(isset($_GET['filter']))
    {
        $qsfrm = $_GET['filter'];
        $qs = __unserialize(stripslashes($_GET['filter']));
    }
    //set a default value for use in filter
    $qs_base = $qs;

    $title = "Library: Upload a Document ";
    $content = "page.upload.php";

    if($pageVars->fileid !== null)
    {
        $title = "Library: Edit a Document ";
        
        //get file data
        $qry = "SELECT f.title, f.file_ref, f.filename, f.fileid, f.description, f.created, f.edited, f.created_by, f.edited_by                     FROM files AS f WHERE fileid = {$pageVars->fileid} AND filecategory = 'DOCLIB'";
        $pageDb = new pageQry($con, $qry);
        $pageDb->rsItem();
        
        if(buttonClicked("save"))
        {
            $result = libUpload($clsUS, $pageVars, $con, $to_file_path);
            if(is_int($result))
            {
                $fileid = $result;
                $tags = new qgpTags($fileid);
                $tags->saveTags($_POST['tags'], $clsUS, $con);

                saveFileCats($fileid, $con);
                headerLocation("admin.php?filter=" . urlencode(serialize($qs)) . "&rand=" . rand(), $messages);
            }
            else
            {
                $uploaded_filename = $result;
                $file_cats = $_POST['catid'];
                $tags_str = $_POST['tags'];
            }
        }
        else if(buttonClicked("cancel"))
        {
            headerLocation("admin.php?filter=" . urlencode(serialize($qs)) . "&rand=" . rand(), $messages);
        }
        else if(buttonClicked("delete"))
        {
            if($pageVars->fileid != 0)
            {
                $tags = new qgpTags($pageVars->fileid);
                $tags->delTags($con);
                deleteFileCats($file_cats, $pageVars->fileid, $con);
                if (countFileUsed($pageVars->fileid, $con) > 0) deleteFromObj($fileid, $con);
                $fileid = qgpDelete($pageVars, $con, $to_file_path);
                if($fileid)
                {
                    headerLocation("admin.php?filter=" . urlencode(serialize($qs)) . "&rand=" . rand(), $messages);
                }
            }
        }
        else
        {
            $tags = new qgpTags($pageVars->fileid);
            $tags_str = $tags->getTagsStr($con);
        }
    }
}
else
{
    trigger_error("Access Denied", E_USER_WARNING);
    headerLocation("$rootUrl/index.php", false);
}
include("../../layout.php");
