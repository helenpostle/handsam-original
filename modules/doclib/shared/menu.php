<?php

if(isAllowed("editDocLib"))
{
    $clsMenu->addMenuItem("/modules/doclib/admin.php", DOCLIB_MENU_LINK_ADMIN, "", "");
}
else if(isAllowed("viewDocLib"))
{
    $clsMenu->addMenuItem("/modules/doclib/index.php", DOCLIB_MENU_LINK_DOCS, "", "");
}