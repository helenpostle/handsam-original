<?php
//tasklist modStart page
require_once("$rootPath/modules/doclib/shared/config.php");

//set in mod config
$pageFiles->addModCss($modCss);
if (file_exists("$rootPath/modules/doclib/css/customise.css")) $pageFiles->addModCss("customise.css");

$pageFiles->addModFunc('qgp_page');
