<?php

/**
 * Securely Download a file from the database
 */
//MUST ADD SECURITUY AND CHECK FILE EXISTS
$content = "";
//make this a secure page
$secure = true;
//include basic stuff
include ("../../shared/startPage.php");

$pageFiles->includePhp();
//specify page content
$title = "Download a Library File";

$pageVars = new loadVars($clsUS, $con);

//get the id 
$id = getIntFromRequest("id");
$rsCheckContent = getRS($con, "SELECT * FROM files WHERE fileid = $id AND filecategory = 'DOCLIB'");

if(getRow($rsCheckContent) && (isAllowed("editDocLib") ||  isAllowed("viewDocLib"))) {

    //get the record set
    if(isAllowed("editDocLib"))
    {
        $sql = "SELECT * FROM files WHERE fileid = $id;";
    }
    else if(isAllowed("viewDocLib"))
    {
        $sql = "SELECT files.* FROM files 
                INNER JOIN qgp_file_tags 
                    ON qgp_file_tags.fileid = files.fileid 
                WHERE files.fileid = $id;";
    }
    $rsFile = getRS($con, $sql);
    if($rowFile = getRow($rsFile))
    {
        //download the file
        $filename = $filename = htmlspecialchars_decode($rowFile["filename"]);
        $path = $handsamdocs_fp;

        $file = $path . $filename;

        // send the right headers
        header("Content-Type: application/octet-stream");
        header('Content-Disposition: attachment; filename="' . $filename . '"');

        echo file_get_contents($file);
        exit;
    }
}