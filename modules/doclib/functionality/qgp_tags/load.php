<?php
$this->addCoreLibPhp("qgp/clsQgpTags.php");
$this->addCoreLibPhp("qgp/clsQgpTagsLicence.php");
$this->addModuleFuncJs("add_tags.js");
$this->addCoreLibJs("jquery/plugins/jquery.rich-array.js");
$this->addModuleBox("box.tag_cloud.php");
$this->addModuleFuncCss("tags.css");
