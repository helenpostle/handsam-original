$(document).ready(function(){

	//define variable letters - letters typed in tags textarea
	var letters = "";
	
	//add the tag suggestion box
	$('#tags').after('<ul id="tag_list"></ul>');

	
	//moves tag cloud down page nearer tags box
	var position = ($("#tags").offset().top) - 400;
    $("#tags").click(function(){
      $("#boxes").animate({ 
        marginTop: position
      }, 100 );
    });
	
	
	
	
	//first check tag cloud for selected tags and highlight them
	//get all item tags and put in an array
	var tags = getTags('#tags');
	var tags_array = getTagsArray(tags);
	
	var tag_cloud_array = highlightTags(tags_array);
	function highlightTags(tags_arr) {
		//loop through all tags in tag cloud and if they match any in tags_array then highlight them
		var tag_cloud_arr = $('#tag_cloud li').get();
		jQuery.each(tag_cloud_arr, function() {
			var this_tag = $(this).html()
			if ($.inArray(this_tag, tags_arr) > -1) {
				$(this).addClass("tag_selected");
			} else {
				$(this).removeClass("tag_selected");
			}
		});
		return tag_cloud_arr;
	}

	/**
	 * returns string of tags from tags text box
	 */
	function getTags(id) {
		var tags = $(id).val();
		return $.trim(tags);
	}
	
	/**
	 * returns array of tags from tags string
	 */
	function getTagsArray(t) {
		var tags_array = t.split(",");
		
		
		tags_array = $.richArray.without(tags_array,'')
		tags_array = jQuery.map(tags_array, function(n){
  		return ($.trim(n));
		});					

		return tags_array;
	}

	
	/**
	 * returns array of suggested tags from cloud_tags_array(array of of li html objects)
	 */

	function getSuggestedTags(arr,lets, arr_tags) {
		lets = $.trim(lets);
		if (lets.length == 0) return "";
		var new_arr = [];
		jQuery.each(arr, function() {
			//need to get just the html bit from the object
			var this_tag = $(this).html();
			
			//make suggestions case insensitive by using tolowercase
			if (this_tag.substring(0,lets.length).toLowerCase() == lets.toLowerCase() && $.inArray(this_tag, arr_tags) == -1) {
				new_arr.push(this_tag);
			}
		});
		
		return new_arr.sort();
	}

	function formatListFromArray(arr) {
		var li = "";
		jQuery.each(arr, function() {
			li = li + "<li>" + this + "</li>";
		});
		return li;
		
	}
	
	//resets suggestion box and hides it
	function resetTagSuggestion() {
		$('#tag_list').html('');
		$('#tag_list').hide();

	}
		
				
	//on click a tag in tag cloud
	$('#tag_cloud li').click(function(event){

		var tags = getTags('#tags');
		var tags_array = getTagsArray(tags);
							

		//get clicked on tag
		var selected_tag = $(this).html();
		
		//now check if selected tag is already in the tags array
		if ($.inArray(selected_tag, tags_array) == -1) {
			tags_array.push(selected_tag);
		} else {
			//remove tag from selection
			tags_array = $.richArray.without(tags_array,selected_tag);
		}
		//add tag to selection
		$('#tags').val(tags_array.join(',') + ',');
		var tag_cloud_array = highlightTags(tags_array);

		//hide suggestion box and reset letters
		resetTagSuggestion();
		letters = "";

	});
	
	
	
	//on keypress in text area get the letters typed and add to letters variable
	//letters defined at top of script
	$('#tags').keypress(function(e){
		var tags = getTags('#tags');
		var tags_array = getTagsArray(tags);
		//if not backspace or comma
		if (e.which != 8 && e.which != 44) {
			letters = letters + String.fromCharCode(e.which);
		} else if (e.which == 8){
			//if backspace delete previous letter
			letters = letters.substring(0,letters.length-1);
		} else if (e.which == 44){
			//if a comma is entered then reset the letters and selection box
			resetTagSuggestion();
			letters = "";
		} 
		var suggestedTags = [];
		if (letters.length > 0) {
			suggestedTags = getSuggestedTags(tag_cloud_array,letters,tags_array);
			if (suggestedTags.length != 0) {
				$('#tag_list').html(formatListFromArray(suggestedTags));
				$('#tag_list').show();
			}
		} else {
			//hide suggestion box
			resetTagSuggestion()
		}
	});

	//on keyup in text highlight any  tags in tag cloud
	$('#tags').keyup(function(e){
		var tags = getTags('#tags');
		var tags_array = getTagsArray(tags);
		highlightTags(tags_array);
		if (e.which == 44){
			//if a comma is entered then reset the letters and selection box
			resetTagSuggestion();
			letters = "";
		} 
	});
	
		
		
		
	//on click a suggested tag
	$('#tag_list li').live("click", function(event){
		var tags = getTags('#tags');
		var tags_array = getTagsArray(tags);
		//remove the letters already typed from the tags array
		tags_array = $.richArray.without(tags_array,letters);
		var selected_tag = $(this).html();		
		tags_array.push(selected_tag + ',');
		//add tag to tags array
		$('#tags').val(tags_array.join(','));
		//hide suggestion box and reset letters
		resetTagSuggestion();
		letters = "";
		
		//highlight selected tag in tag_cloud
		var tags = getTags('#tags');
		var tags_array = getTagsArray(tags);

		var tag_cloud_array = highlightTags(tags_array);

	});	
	
	//fades out suggestion box onchange in tag text area	
	$('#tags').change(function(){
		//$('#tag_list').fadeOut();	
	});
		
	
	
	$('.all_tags').live("click", function(event){
		copyAllTags();
		$(this).removeClass('all_tags');
		$(this).addClass('no_tags');
		
	});
	
	$('.no_tags').live("click", function(event){
		$('#tags').html('');
		$(this).removeClass('no_tags');
		$(this).addClass('all_tags');
		$('#tag_cloud li').removeClass("tag_selected");
		
	});
	
	
	
	function copyAllTags() {
		var all_tags = $('#tag_cloud li').get();
		var cnt = 0;
		var tags_str = "";
		jQuery.each(all_tags, function() {
			var this_tag = $(this).html();
			if (cnt > 0) tags_str = tags_str + ',';
			tags_str = tags_str + this_tag;
			cnt ++;
		});
		$('#tags').html(tags_str);
		$('#tag_cloud li').removeClass("tag_selected");
		$('#tag_cloud li').addClass("tag_selected");
	}
});
