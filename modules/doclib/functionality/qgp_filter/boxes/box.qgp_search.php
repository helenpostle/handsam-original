<?php

if(isAllowed("editDocLib") || isAllowed("viewDocLib"))
{
    $search_qs_base = $qs_base;
    $search_qs_base['tag'] = 0;
    $search_qs_base['category'] = 0;

    if(isset($qs_base['search']))
    {
        $search_value = $qs_base['search'];
    }
    else
    {
        $search_value = "";
    }
    accidentBoxTop("Search");
    echo displayParagraphs("Search the Library by title. Searching will clear any category and tag selections.");
    if(isAllowed("editDocLib"))
    {
        //admin searching
        echo "<div><form action=\"admin.php\" method=\"post\" name=\"search_form\">";
    }
    else if(isAllowed("viewDocLib"))
    {
        //non admin searching
        echo "<div><form action=\"index.php\" method=\"post\" name=\"search_form\">";
    }

    echo "<label for=\"search_doclib\"></label>";
    echo "<input type=\"text\" name=\"search_doclib\" id=\"search_doclib\" maxlength=\"60\" value=\"$search_value\"/>";
    echo "<input type=\"submit\" name=\"search\" value=\"search\" id=\"search\"/>";
    echo "<input type=\"hidden\" value=\"" . htmlspecialchars(serialize($search_qs_base)) . "\" name=\"filter\"/>";
    echo "</form></div>";

    echo "<div id=\"clear_search\">clear search";
    $search_qs_base['search'] = "";
    $filter_link = "?filter=" . rawurlencode(serialize($search_qs_base));
    echo frmHiddenField($filter_link, "clear_search");
    echo "</div>";

    accidentBoxBottom();
}
