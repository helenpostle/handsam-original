<?php

/**
 * box showing all links tags for link editing pages
 */
#get max number of tags for one item
if(isAllowed("editDocLib") || isAllowed("viewDocLib"))
{
    if(isAllowed("editDocLib"))
    {
        $qryList = "SELECT count(*) AS cnt FROM qgp_tags 
                    INNER JOIN qgp_file_tags 
                        ON qgp_tags.tagid = qgp_file_tags.tagid 
					left join files on qgp_file_tags.fileid = files.fileid 
					where files.filecategory = 'DOCLIB'
                    GROUP BY qgp_tags.tagid 
                    ORDER BY cnt DESC LIMIT 0, 1";
    }
    else if(isAllowed("viewDocLib"))
    {
        $qryList = "SELECT count(*) AS cnt FROM qgp_tags 
                    INNER JOIN qgp_file_tags 
                        ON qgp_tags.tagid = qgp_file_tags.tagid                     
					left join files on qgp_file_tags.fileid = files.fileid 
					where files.filecategory = 'DOCLIB'
                    GROUP BY qgp_tags.tagid ORDER BY cnt DESC LIMIT 0, 1;";
    }

    $resultList = getRS($con, $qryList);
    $row = getRow($resultList);
    $max = $row['cnt'];
    $class_size = $max / 6;
    $qs = $qs_base;

    if(isAllowed("editDocLib"))
    {
        $qryList = "SELECT qgp_tags.tag, qgp_tags.tagid, count(*) AS cnt 
                    FROM qgp_tags 
                    INNER JOIN qgp_file_tags 
                        ON qgp_tags.tagid = qgp_file_tags.tagid 
					left join files on qgp_file_tags.fileid = files.fileid 
					where files.filecategory = 'DOCLIB'
                    GROUP BY qgp_tags.tag, qgp_tags.tagid 
                    ORDER BY tag ASC;";
    }
    else if(isAllowed("viewDocLib"))
    {
        $qryList = "SELECT qgp_tags.tag, qgp_tags.tagid, count(*) AS cnt 
                    FROM qgp_tags 
                    INNER JOIN qgp_file_tags 
                        ON qgp_tags.tagid = qgp_file_tags.tagid                         
					left join files on qgp_file_tags.fileid = files.fileid 
					where files.filecategory = 'DOCLIB'
                    GROUP BY qgp_tags.tag, qgp_tags.tagid 
                    ORDER BY tag ASC;";
    }

    $resultList = getRS($con, $qryList);

    if(getResultRows($resultList) > 0)
    {
        accidentBoxTop("Tags");
        echo "<ul id=\"tag_cloud\">";
        while ($rowList = getRow($resultList))
        {
            $class = $rowList['cnt'] / $class_size;
            if(isset($qs_base['tag']) && strval($qs_base['tag']) == strval($rowList['tagid']))
            {
                $class .= " tag_selected";
            }
            
            echo "<li class=\"size$class\">";
            echo displayText($rowList['tag']);
            $qs['tag'] = $rowList['tagid'];
            $filter_link = "?filter=" . rawurlencode(serialize($qs));
            echo frmHiddenField($filter_link, $rowList['tag']);
            echo "</li>";
        }
        echo "</ul>";
        echo "<div id=\"clear_tag\">clear tag selection";
        $qs['tag'] = 0;
        $filter_link = "?filter=" . rawurlencode(serialize($qs));
        echo frmHiddenField($filter_link, "clear_tag");
        echo "</div>";
        accidentBoxBottom();
    }
}
