<?php
/*file upload function */
function libUpload($clsUS, $pageVars, $con, $to_file_path) {
	global $messages;
	#get filename and previously uploaded filename from form
	$filename = $_FILES['filename']['name'];
	$uploaded_filename = $_POST['uploaded_filename'];
	if ($filename != "") {
		#if filename not empty then upload file
		$file_path=$_FILES['filename']['tmp_name'];
		//die($file_path);
		$upload_file = uploadFile($filename, $file_path, $to_file_path);
		
		if($upload_file) {
			//die();
			#if upload successful check for previously uploaded filename
			if ($uploaded_filename != "" && $uploaded_filename != $upload_file) {
				#delete this 
				$deleteFile = deleteFile($uploaded_filename, $to_file_path);
			}
			#now set previously uploaded filename as this uploaded filename - incase form doesn't validate then user doesn't have to re-select a new file
			$filename = $upload_file;
			
		} else {
			$messages[] = ERROR_FILE_UPLOAD;
			return false;
		}
			
		
	} else if ($uploaded_filename != "") {
		$filename = $uploaded_filename;
	}
	if ($pageVars->fileid == 0) {
		$qry = new dbInsert("files");
	} else {
		$qry = new dbUpdate("files");
		$qry->setParam("fileid",$pageVars->fileid);
	}

	if (isset($qry)) {
		if (!isset($_POST['catid'])) {
			$qry->failError .= "Please select at least one category.\r\n";
		}
		$qry->setReqStringVal("filename",$filename,"filename");
		$qry->setReqStringVal("title",$_POST["title"],"Title");
		$qry->setStringVal("description",$_POST["description"],"Description");
		if (isset($_POST["file_ref"])) $qry->setStringVal("file_ref",$_POST["file_ref"],"File Ref");
		$qry->setReqStringVal("filetype",'handsamDoc',"file type");
		$qry->setReqStringVal("filecategory", $_POST['filecategory'], "file category");
		$qry->setReqNumberVal("accountid",1,"Account id");
		$qry->setAudit($clsUS->userid);
		
		if ($qry->execute($con)) {
			if ($pageVars->fileid == 0) {
				$pageVars->fileid = $qry->getNewID();	
			}
			$messages[] = SAVED;
			return $pageVars->fileid;
			
		} else {
			$messages[] = $qry->getError();	
			//echo $qry->getError();
			return $filename;
		}
	}
	return false;
}


?>