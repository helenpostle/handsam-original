<?php
/* Lib list functions
*/

function getLibList($pageVars, $con, $filter_array, $filecat, $allowed_tags="") {
	//need to include secutity here so can only view list of files licenced to them
	
	//implode filter array
	$filter_qry = implode(" and ", $filter_array);
	if (trim($allowed_tags) != "") {
		$allowed_tags_qry = " and ft.tagid in ($allowed_tags) ";
	} else {
		$allowed_tags_qry = "";
	}
	if (trim($filter_qry) != "") {
        $filter_qry = " and ".$filter_qry;
    } else {
        $filter_qry = " and 1=100 ";
        
    }

	if (isAllowed("viewQGP")) {
	}			
    
	$qry = "SELECT f.file_ref, f.title, f.description, f.edited, f.created, CASE  WHEN   f.edited > f.created  THEN  f.edited ELSE f.created  END  AS last_edited, f.fileid FROM files f left join qgp_file_cat fc on f.fileid = fc.fileid ";
	$qry .= " left join qgp_file_tags ft on f.fileid = ft.fileid where f.filecategory = '$filecat'";
	$qry .= " $filter_qry $allowed_tags_qry group by f.title, f.description, f.edited, f.created, f.fileid order by f.title";
	
	//echo "<br/>".$qry;
	$pageDb = new pageQry($con, $qry);
	$pageDb->rsList();
	return $pageDb;
}
?>