<?php

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");

$pageFiles->addModFunc("qgp_list");
$pageFiles->addModFunc("qgp_filter");
$pageFiles->addModFunc('qgp_cats');
$pageFiles->includePhp();

//priority for module functionality boxes over page boxes
$pageFiles->addBoxPriority();
$qs = array();

$pageVars = new loadVars($clsUS, $con);

if(isAllowed("editDocLib"))
{
    $title = "Library Module: Admin";
    $box[] = "box.admin.php";
    $content = "page.doclist.php";

    //get filter array querystring	
    if(isset($_POST['filter']))
    {
        $qs = __unserialize($_POST['filter']);
        $qsfrm = $_POST['filter'];
    }
    else if(isset($_GET['filter']))
    {
        $qsfrm = $_GET['filter'];
        $qs = __unserialize(stripslashes($_GET['filter']));
    }

    //set a default value for use in filter
    $qs_base = $qs;
    $filter_title = array();
    $filter_qry = array();

    //process filters
    //category filter
    if(is_array($qs) && array_key_exists('category', $qs) && $qs['category'] > 0)
    {
        $filter_qry['category'] = " fc.catid = " . mysql_real_escape_string($qs['category']);
        //get cat name from db
        $cat_qry = "SELECT cat FROM qgp_cat WHERE catid = " . mysql_real_escape_string($qs['category']);
        $cat_rs = getRS($con, $cat_qry);
        $row = getRow($cat_rs);
        $filter_title['category'] = "Category - {$row['cat']}";
    }
    else
    {
        if (isset($qs['category']) && $qs['category'] === 'ALL') {
            $filter_qry['category'] = "1=1";
        }
        $filter_title['category'] = "All Categories";
    }

    //tag filter
    if(is_array($qs) && array_key_exists('tag', $qs) && $qs['tag'] > 0)
    {
        $filter_qry['tag'] = " ft.tagid = " . mysql_real_escape_string($qs['tag']);
        //get tag name from db
        $tag_qry = "SELECT tag FROM qgp_tags WHERE tagid = " . mysql_real_escape_string($qs['tag']);
        $tag_rs = getRS($con, $tag_qry);
        $row = getRow($tag_rs);
        $filter_title['tag'] = "Tag - {$row['tag']}";
    }
    else
    {
        //$filter_qry['tag'] = "2=2";
        $filter_title['tag'] = "All Tags";
    }

    /* search box */

    if(isset($_POST['search']))
    {
        $qs['search'] = trim($_POST['search_doclib']);
        $qs_base['search'] = trim($_POST['search_doclib']);
    }

    if(is_array($qs) && array_key_exists('search', $qs) && $qs['search'] != "")
    {
        $filter_qry['search'] = " f.title LIKE '%" . mysql_real_escape_string($qs['search']) . "%' ";
        $filter_title['search'] = "Search - &quot;{$qs['search']}&quot;";
    }
    else
    {
        //$filter_qry['search'] = "1=1";
        $filter_title['search'] = "No search term";
    }
    
    //if no search filters - then modify titles to:
    if (count($filter_qry) == 0) {
        $filter_title['search'] = "No search term";
        $filter_title['tag'] = "No Tags";
        $filter_title['category'] = "No Categories";
    }
    
    //now implode filter title
    $filter_title_str = implode(": ", $filter_title);

    $pageDb = getLibList($pageVars, $con, $filter_qry, "DOCLIB"); //gets a standard list of docs
    //we need to add a comma sperated list of qgp_cats for each file in tbe list
    $pageDb = addQgpCats($pageDb, $con);
    
    
}
else
{
    trigger_error("Access Denied", E_USER_WARNING);
    headerLocation("$rootUrl/index.php", false);
}


include("../../layout.php");
