<?php

/**
 * lists QGP admin menu options 
 * 
 */
boxTop("Library Licence");

if(isAllowed("editAccount"))
{
    $qry = "SELECT * FROM accountlicence 
            WHERE accountid = $accountid 
                AND appid = 5 
                AND (state = 'ACTIVE' OR state = 'INACTIVE') 
                AND end_date > NOW()";

    $rsQgp = getRS($con, $qry);
    if(getResultRows($rsQgp) > 0)
    {
        echo "<table class=\"boxList\">";
        echo "<thead>";
        echo "<tr>";
        echo "<th class=\"licenceid nolink\">Start date</th>";
        echo "<th class=\"licenceid nolink\">End date</th>";
        echo "</tr>";
        echo "</thead>";
        echo "<tbody>";

        while ($row = getRow($rsQgp))
        {
            echo "<tr>";
            echo "<td class=\"licenceid\"><a class=\"arrow\" href=\"$modpath/licence.php?licenceid={$row['licenceid']}&amp;accountid=$accountid\">" . displayDate($row['start_date']) . "</a></td>";
            echo "<td class=\"licenceid\"><a class=\"\" href=\"$r$modpath/licence.php?licenceid={$row['licenceid']}&amp;accountid=$accountid\">" . displayDate($row['end_date']) . "</a></td>";
            echo "</tr>";
        }
        echo "</tbody>";
        echo "<tfoot>";
        echo "<tr>";
        echo "<td colspan=\"2\">";
        echo "</tr>";
        echo "</tfoot>";
        echo "</table>";
    }

    echo "<p class=\"info\">Add a new QGP licence to this account</p>";
    echo "<p><a class=\"account_edit\" href=\"$modpath/licence.php?licenceid=0&amp;accountid=$accountid\">Create new licence</a></p>";
}

boxBottom();