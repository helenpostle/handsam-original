<?php
/* * *
 * * Shows the list of task categories in the system
 * * 
 * * 
 * * */
echo displayTitle2($filter_title_str);

if($pageDb->rsExists())
{
    ?>
    <table class="acc_list">
        <thead>
            <tr>
                <th class="start ">Title</th>
                <?php
                if ($library_conf['file_ref']) echo "<th class=\"\">Reference</th>";
                ?>
                <th class="">Categories</th>
                <th class="end ">Date Last Edited</th>
            </tr>
        </thead>
        <tbody>
            <?php
            foreach($pageDb->rows as $row)
            {
                if(isAllowed("editDocLib"))
                {
                    $url = "$rootUrl/modules/doclib/upload.php?fileid={$row['fileid']}&amp;rand=" . rand() . "&amp;filter=" . rawurlencode(serialize($qs));
                }
                else
                {
                    $url = "download.php?id={$row['fileid']}";
                }
                ?>
                <tr>
                    <td class="start catname"><a href="<?php echo $url; ?>"><?php echo $row["title"] ?></a></td>
                    <?php 
                    if ($library_conf['file_ref']) echo "<td class=\"\"><a href=\"$url\">{$row["file_ref"]}</a></td>";
                    ?>
                    <td class=""><a href="<?php echo $url; ?>"><?php echo implode('; ', $row["cats"]) ?></a></td>
                    <td class="end state"><a href="<?php echo $url; ?>"><?php echo displayDate($row["last_edited"]) ?></a></td>
                </tr>
            <?php } ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="start"></td>
                <?php
                if ($library_conf['file_ref']) echo "<td class=\"\"></td>";
                ?>
                <td class=""></td>
                <td class="end"></td>
            </tr>
        </tfoot>

    </table>
<?php
}
