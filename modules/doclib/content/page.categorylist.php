<?php
/* * *
 * * Shows the list of task categories in the system
 * * 
 * * 
 * * */

$qry = "SELECT * FROM qgp_cat ORDER BY library_state, cat;";

$result = getRS($con, $qry);
?>
<table class="acc_list">
    <caption class="hidden"><?php echo $title; ?></caption>
    <thead>
        <tr>
            <th class="start ">Category name</th>
            <th style="width: 60px;" class="">State</th>
            <th class="end address">Description</th>
        </tr>
    </thead>
    <tbody>
        <?php
        while ($row = getRow($result))
        {
            ?>
            <tr>
                <td class="start catname"><a href="category.php?catid=<?php echo $row["catid"]; ?>"><?php echo $row["cat"] ?></a></td>
                <td class=" "><a href="category.php?catid=<?php echo $row["catid"]; ?>"><?php echo $row["library_state"] ?></a></td>
                <td class="end"><a href="category.php?catid=<?php echo $row["catid"]; ?>"><?php echo displayText($row["cat_desc"]) ?></a></td>
            </tr>
<?php } ?>
    </tbody>
    <tfoot>
        <tr>
            <td class="start"></td>
            <td class=""></td>
            <td class="end"></td>
        </tr>
    </tfoot>

</table>
