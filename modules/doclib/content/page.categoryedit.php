<?php 
/**
 * Shows a form for editing a task category
 */
if (isset($catid) &&  isAllowed("editDocLib")) { 
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	$rsCategory = getRS($con,"SELECT * FROM qgp_cat WHERE catid = $catid");
	$rowCategory = getRow($rsCategory); 

	echo startFormTable($_SERVER["PHP_SELF"], "clearfix", "", "Edit a Document Category", "Use this form to edit a Document category. The description field is optional");
	echo frmHiddenField($catid,"catid");
	echo frmTextField($rowCategory,"cat",100,"Task category name",true);
	echo frmTextArea($rowCategory,"cat_desc",5,"Description",false);
    echo frmSelectArray($rowCategory, "library_state" , $state, "State" , true);
	if ($catid != 0 && isAllowed("editDocLib")) {
            echo frmShowAudit($rowCategory,$con);	
	}
	echo frmButtonHelp("Save","save", "Save all changes");
	echo frmButtonHelp("Cancel","cancel", "Cancel any changes");
        
	if ($catid != 0 && $file_cnt == 0)
            echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this category?", "Delete this category");
	
	echo endFormTable();
	
} 
