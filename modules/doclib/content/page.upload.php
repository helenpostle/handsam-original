<?php

/**
 * Shows a  form for uploading a file
 * 
 */
if(isAllowed("editDocLib"))
{
    if(!isset($uploaded_filename) || $uploaded_filename == "")
    {
        $uploaded_filename = $pageDb->row['filename'];
    }
    echo startFileFormTable($_SERVER["PHP_SELF"], "clearfix", "Upload a Document", UPLOAD_FORM_INFO);

    echo frmHiddenField(htmlspecialchars(serialize($qs_base)), "filter");
    echo frmHiddenField($pageVars->fileid, "fileid");
    echo frmHiddenField($pageVars->accountid, "accountid");
    echo frmHiddenField($uploaded_filename, "uploaded_filename");
    echo frmHiddenField(70000000, "MAX_FILE_SIZE");
    echo frmFileField("filename", "Select File", true);
    
    if(isset($uploaded_filename) || $uploaded_filename != "")
    {
        echo "<div class=\"clearfix optional frm_row\"><span class=\"label\">Uploaded Filename</span><span class=\"value\"><span class=\"upload\"><a href=\"download.php?id=" . $pageVars->fileid . "\" title=\"\">" . $uploaded_filename . "</a></span></span></div>";
    }

    echo frmHiddenField($uploaded_filename, 'uploaded_filename');
    
    echo frmHiddenField('DOCLIB', 'filecategory');

    echo frmTextField($pageDb->row, "title", 100, "Title", true);
    echo frmTextArea($pageDb->row, "description", 5, "Description", false);
    
    if ($library_conf['file_ref']) echo frmTextField($pageDb->row, "file_ref", 100, "File Ref.", false);
    
    //category check boxes
    echo "<div id=\"qgp_cats\" class=\"frmSection clearfix\">";
    echo "<h3>Document Categories</h3>";
    echo displayParagraphs("You must select at least one category below:");

    $cat_qry = "SELECT catid, cat From qgp_cat where qgp_cat.library_state = 'ACTIVE' ORDER BY cat;";
    $cat_rs = getRS($con, $cat_qry);
    $cat_str = "";
    while ($cat_row = getRow($cat_rs))
    {
        $cat_str .= "<div class=\"optional clearfix\"><span class=\"label\">{$cat_row['cat']}</span><input class=\"inputCheckbox\" type=\"checkbox\" name=\"catid[]\"  value=\"{$cat_row['catid']}\"";
        if(is_array($file_cats) && in_array($cat_row['catid'], $file_cats))
            $cat_str .= " checked";
        $cat_str .= "/></div>";
    }

    echo $cat_str;
    echo "</div>";
    
    echo frmTextAreaNonDb("tags", 4, "Tags", false, $tags_str);
    echo frmShowAudit($pageDb->row, $con);
    echo frmButtonHelp("Save", "save", "Click here to save changes");
    echo frmButtonHelp("Cancel", "cancel", "Click here to cancel upload");
    
    if($pageVars->fileid != 0) {
        if (countFileUsed($pageVars->fileid, $con) == 0) {
            echo frmButtonConfirmHelp("Delete", "delete", "Are you sure you want to delete this file?", "Click here to delete this link");
        } else {
            echo frmButtonConfirmHelp("Delete", "delete", "This file is attached to other resources. Are you sure you want to delete this file?", "Click here to delete this link");
        }
    }

    echo endFormTable();
}
