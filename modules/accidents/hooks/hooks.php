<?php
/**
 * Hooks for the accidents module
 * 
 * Mei Gwilym, Oct 2012
 */


// called when an accident book is saved 
function accident_save($args) {
    global $accidents_conf;
    global $send_email_alerts;
    
    if ($send_email_alerts == true && $accidents_conf['send_alerts'] == true) 
    {
        sendMajorAlert($args[0]);    
        sendInvestigatorAlert($args[0]);
    }
}

/** 
 * Send a Trigger Alert when a RIDDOR accident is reported. 
 * 
 * Sends an alert on RIDDOR or Death/Major Injury,
 * and a retraction if this status is changed. 
 * 
 * @param object $dbVars 
 */
function sendMajorAlert($dbVars)
{   
	global $accidents_conf;
	if ($accidents_conf['send_alerts'] == true) {
		$type = '';
		$retract = false; 
		
		// if false then this is a new incident report
		if(!$dbVars)
		{        
			if(isset($_POST['injury_Q1']) && in_array($_POST['injury_Q1'], array('fatality', 'major')))
			{
				$type = ($_POST['injury_Q1'] == 'fatality' ? 'death' : 'majinj');   
				send_incident_alert($type, $retract);
			}
			
			if(isset($_POST['riddor']) && $_POST['riddor'] == 'RIDDOR')
			{
				$type = 'riddor';
				send_incident_alert($type, $retract);
			}
		}
		else // this is an edit
		{
			// see if the death/major injury value has changed, and how.
			if(isset($dbVars['injury_Q1']) && $dbVars['injury_Q1'] != $_POST['injury_Q1'])
			{
			
				if(in_array($_POST['injury_Q1'], array('fatality', 'major')))
				{	
					$type = ($_POST['injury_Q1'] == 'fatality' ? 'death' : 'majinj');
					send_incident_alert($type, $retract);					
				}
				elseif($dbVars['injury_Q1'] == 'fatality')
				{
					$type = 'death';
					$retract = true;
					send_incident_alert($type, $retract);
				}
				elseif($dbVars['injury_Q1'] == 'major')
				{
					$type = 'majinj';
					$retract = true;
					send_incident_alert($type, $retract);
				}
				
			}
			//reset retract
			$retract = false;
			// RIDDOR reportable
			
			if(isset($dbVars['riddor']) && $dbVars['riddor'] != $_POST['riddor'])
			{
				
				if($_POST['riddor'] == 'RIDDOR')
				{
					$type = 'riddor';
				}
				else
				{
					$type = 'riddor';
					$retract = true;
				}
				send_incident_alert($type, $retract);
			}
			
		}

		
	}
}

/**
 * Send alerts to accident investigator
 * 
 * Sends to persons unassigned from an investigation, and assigned to an investigation
 * 
 * @param array of mixed values
 */
function sendInvestigatorAlert($formDB)
{      
    global $rootUrl, $alertsMail;
    // no change? then this ends here!
    // if ( new != old );
	if($_POST['int_investigator_userid'] < 1){
	    RETURN; 
	}
    if($_POST['int_investigator_userid'] == $formDB['int_investigator_userid']) return;    
    
    /* The investigator has been changed. 
     * either a fresh one added, or it has been reassigned to someone else.
     */
    
    // variables for the message templates
    $account = new Account($_POST['accountid']);
    $accountName = $account->accountname;    
    $incidentLink = $rootUrl.'/modules/accidents/accident.php?accountid='.$formDB['accountid'].'&accidentid='.$formDB['accidentid'];
    
    // array of messages to be sent
    $mailer = array();
    
    // investigator unassigned
    if($formDB['int_investigator_userid'] > 0)
    {
        $oldinvestigator = new User($formDB['int_investigator_userid']);
        $temp['to'] = $oldinvestigator->username;
        $temp['subject'] = 'Unassigned from an Accident Investigation';
        $temp['mail_body'] = getEmailTemplate('alert_int_investigator_unassign', $incidentLink);

        $mailer[] = $temp;
    }
    
    $investigator = new User($_POST['int_investigator_userid']);
    $temp['to'] = $investigator->username;
    $temp['subject'] = 'New Accident Investigation';
    $temp['mail_body'] = getEmailTemplate('alert_int_investigator', $accountName, $incidentLink);

    $mailer[] = $temp; 
    // send all messages
    foreach($mailer as $m)
        sendMail($m['to'], $alertsMail, $m['subject'], $m['mail_body']);
    
}