$(document).ready(function(){
	//define variable letters - letters typed in tags textarea
	var letters = "";
		
	//first check tag cloud for selected tags and highlight them
	//get all item tags and put in an array
	var tags = getTags('#tags');
	var tags_array = getTagsArray(tags);
	highlightTags(tags_array);
	
	
	//on click a tag in tag cloud
	//$('#tag_cloud li').click(function(event){
	$("body").delegate("#tag_cloud li", "click", function(){

		var tags = getTags('#tags');
		var tags_array = getTagsArray(tags);
							

		//get clicked on tag
		var selected_tag = $(this).html();
		
		//now check if selected tag is already in the tags array
		if ($.inArray(selected_tag, tags_array) == -1) {
			tags_array.push(selected_tag);
		} else {
			//remove tag from selection
			tags_array = $.richArray.without(tags_array,selected_tag);
		}
		//add tag to selection
		if (tags_array.length > 0) {
			$('#tags').val(tags_array.join(',') + ',');
		} else{
			$('#tags').val(tags_array.join(','));
		}
		highlightTags(tags_array);

		//hide suggestion box and reset letters
		letters = "";

	});
	
});

	function highlightTags(tags_arr) {
		//loop through all tags in tag cloud and if they match any in tags_array then highlight them
		var tag_cloud_arr = $('#tag_cloud li').get();
		jQuery.each(tag_cloud_arr, function() {
			var this_tag = $(this).html()
			if ($.inArray(this_tag, tags_arr) > -1) {
				$(this).addClass("tag_selected");
			} else {
				$(this).removeClass("tag_selected");
			}
		});
		return tag_cloud_arr;
	}

	/**
	 * returns string of tags from tags text box
	 */
	function getTags(id) {
		var tags = $(id).val();
		return $.trim(tags);
	}
	
	/**
	 * returns array of tags from tags string
	 */
	function getTagsArray(t) {
		var tags_array = t.split(",");
		
		
		tags_array = $.richArray.without(tags_array,'')
		tags_array = jQuery.map(tags_array, function(n){
  		return ($.trim(n));
		});					

		return tags_array;
	}