<?php

// php
$pageFiles->addModuleLibPhp('funcAlerts.php');

// css
$pageFiles->addModuleFuncCss('alerts.css');


// js

$pageFiles->addModuleFuncJs('alerts.user.js');
$pageFiles->addModuleFuncJs('alerts.jobrole.js');
$pageFiles->addModuleFuncJs('alerts.accountmanager.js');
$pageFiles->addModuleFuncJs('alerts.user-popup.js');
$pageFiles->addModuleFuncJs('alerts.js');