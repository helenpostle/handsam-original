/**
 * Called after an account manager is clicked and the dialog is closing 
 * 
 * user_id can be either a user object or an account id
 */ 
Alert.dialogCloseAccountmanager = function (am_id) 
{
    if (am_id != 0)
    {	
        var existing_am_match = false;

        // find all managers for this alert, check that the manager is there already
        $('#'+Alert.currentAlert).find('ul.alert_managers li input.am_id').each(function(){
            if($(this).val() == am_id) existing_am_match = true;               
        });

        // only add the manager if (s)he isn't already on the alert
        if(!existing_am_match) 
        {            
            // remove the 'No Current Managers' message
            $('#'+Alert.currentAlert+' .noaccount-manager').remove();
            
            am_ul = $('#'+Alert.currentAlert).find('ul.alert_managers');
            
            if(typeof am_id != 'object' && am_id > 0)
            {
                new_am = $().getAccountmanager(am_id);
            }
            else 
            {
                new_am = am_id;
            }

            this_alert_counter = $('#'+Alert.currentAlert).attr('id').split('-')[1];   
            manager_counter = 0;
            manager_counter = am_ul.children('li').length;

            new_am_li = $().insertAccountmanager(new_am, this_alert_counter, manager_counter); 
			//console.log(new_am_li);

            am_ul.append(new_am_li);
        }
    }
}



// return account manager list li & contents
$.fn.insertAccountmanager = function(am, alert_count, k)
{
    html = '';
    if(am.manager_title.length > 0)
    {
        var alerts_am_id = (typeof(am.alertmanager_id) == 'undefined') ? '0' : am.alertmanager_id;
		
        html = '<li class="am" data-alertamid="'+alerts_am_id+'">' 
		html += '<input type="hidden" name="alert['+alert_count+'][accountmanager]['+k+'][alert_am_id]" class="alert_am_id" value="'+am.alertmanager_id+'">';		
        html += '<h3>'+am.manager_title+'</h3>';
        html += '<input type="hidden" name="alert['+alert_count+'][accountmanager]['+k+'][acc_manager_id]" class="am_id" value="'+am.acc_manager_id+'" />';
        html += '<div>';
        if(am.manager_users && am.manager_users.length > 0)
        {
            for(i in am.manager_users)
            {
				
                html += '<span class="manageruser">'+am.manager_users[i].username+'</span>';
            }
        }
        else if(window.account_id > 0) // only show this if it's not a global alert
        {
            html += '<span class="jobroleuser">This account manager has not ben set, no email will be sent. This can be changed in Manage Accounts</span>';
        }		
		
        html+= '</div>';
        html += '<div class="alert_options"><span class="delete_am" title="Remove '+am.manager_title+' from the alert">delete</span></div>';
        html += '</li>';
    }
    return html;

}



/**
 * get info about an account manager
 * this is a synchronous call, as we need to wait for the info to come back to return the data
 */
$.fn.getAccountmanager = function (am_id, acc_id)
{
    var am_user;
    var acc_id = $('#accountid').val();
    jQuery.ajax({
         url:    siteRoot + "/modules/accidents/ajax/alerts.php",
         data : { 
             action : 'get-am-user',
             am_id : am_id,
             acc_id : acc_id
        },
         dataType : 'json',
         success: function(data)
                    {
                        am_user = data;
                    },
         async:   false
    });  
    return am_user;
}


$.fn.accountmanagerList = function(accountmanager, alert_count)
{
    var html = '';
    
    html += '<h3>Account Managers</h3>';
    html += '<ul class="alert_managers">'
	k = 0;
    if(typeof(accountmanager) != 'undefined') {
        for(j in accountmanager)
        {
            if(accountmanager[j].acc_manager_id != null && accountmanager[j].acc_manager_id != '')
            {
                html += $().insertAccountmanager(accountmanager[j], alert_count, j);
                k++;
            }
        }
    }
	if(k == 0)html += $().emptyListItem('account manager');
    html += '</ul>';
    return html;
}

$.fn.deleteAccountManager = function()
{
    $('span.delete_am').on('click', function()
    {
        if(confirm('Are you sure? Click "OK" and these Account Managers will no longer receive this alert'))
            {
                var user_rem = $(this).parent().parent();
                var am_rem_id = user_rem.attr('data-alertamid');
                var am_list = user_rem.parent('ul');
                $.post(siteRoot + "/modules/accidents/ajax/alerts.php",
                {
                    action : 'delete-am',
                    am_rem_id : am_rem_id
                },
                function(){
                    // slideup and remove
                    user_rem.slideUp('fast', function(){                        
                        $(this).remove();
                        if(am_list.children().length == 0) am_list.append($().emptyListItem('account manager'));
                    });
                    
                    
                }
            );
            }
    });
}
