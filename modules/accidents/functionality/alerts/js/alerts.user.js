/**
 * Called after an user is clicked and the dialog is closing 
 * 
 * user_id can be either a user object or an account id
 */ 
Alert.dialogCloseUser = function (user_id) 
{
    if (user_id != 0)
    {	
        var existing_user_match = false;

        // find all account users for this alert, check that the user is there already
        $('#'+Alert.currentAlert).find('ul.alert_users li input.usr_id').each(function(){
            if($(this).val() == user_id) existing_user_match = true;               
        });

        // only add the user if (s)he isn't already on the alert
        if(!existing_user_match) 
        {
            // remove the 'No Current Users' message
            $('#'+Alert.currentAlert+' .nouser').remove();
            
            var new_user;
            user_ul = $('#'+Alert.currentAlert).find('ul.alert_users');
            if(typeof user_id != 'object' && user_id > 0)
            {
                new_user = $().getAcUser(user_id);
            }
            else 
            {
                new_user = user_id;
            }
            
            this_alert_counter = $('#'+Alert.currentAlert).attr('id').split('-')[1];                

            user_counter = 0;
            user_counter = user_ul.children('li').length;

            new_user_li = $().insertUser(new_user, this_alert_counter, user_counter); 

            user_ul.append(new_user_li);
        }
    }
}




/**
 * add an user to the alert.
 * 
 * usr = user object
 * alert_counter = which alert we're managing
 * k = new index of this user
 */
$.fn.insertUser = function(usr, alert_counter, k)
{
    // check if the user has a corresponding DB record or not
    if(typeof usr.alertuser_id == 'undefined') usr.alertuser_id = 0;
    
    var html = '<li data-alertuserid="'+usr.alertuser_id+'">';
    html += '<input type="hidden" name="alert['+alert_counter+'][user]['+k+'][alertuser_id]" class="alertuser_id" value="'+usr.alertuser_id+'">';

    // if the usr_id is here then it's a registered user
    // else, it's an external user
    if(typeof usr.usr_id != 'undefined' && usr.usr_id != null ) 
    {
        html += '<input type="hidden" name="alert['+alert_counter+'][user]['+k+'][usr_id]" class="usr_id" value="'+usr.usr_id+'">';
    }
    else
    {
        html += '<input type="hidden" name="alert['+alert_counter+'][user]['+k+'][user_name]" value="'+usr.user_name+'">';
        html += '<input type="hidden" name="alert['+alert_counter+'][user]['+k+'][user_email]" value="'+usr.user_email+'">';
    }

    html += '<span class="alert_user">'+usr.user_name+'</span><span class="alert_email">'+usr.user_email+'</span>';

    html += '<div class="alert_options">';
    // edit user button eft out for now, MG 28/08 
    //html += '<span class="edit_alert_user" title="Change '+usr.user_name+'\'s details"></span>';
    html += '<span class="delete_alert_user" title="Remove '+usr.user_name+' from the alert">delete</span>';
    html += '</div>';

    html += '</li>';
    
    return html;
    // append onto ul.user_list
}
// $.fn.insertUser



/**
 * get info about an account user
 * this is a synchronous call, as we need to wait for the info to come back to return the data
 */
$.fn.getAcUser = function (user_id)
{
    var ac_user;
    
    jQuery.ajax({
         url:    siteRoot + "/modules/accidents/ajax/alerts.php",
         data : { 
             action : 'get-ac-user',
             usr_id : user_id
        },
         dataType : 'json',
         success: function(data)
                    {
                        ac_user = data;
                    },
         async:   false
    });  
    return ac_user;
}
// $.fn.getAcUser()s



// create and return a <ul> of users
$.fn.userList = function(users, alert_count)
{
    html = '<h3>People</h3>';
    html += '<ul class="alert_users">';
    var k = 0;
    for(l in users)
    {
        if(users[l].alert_user_id !== null) // no users have an object with null values
            {
                html += $().insertUser(users[l], alert_count, k);
                k++;
            }
    }
    // users
    if(k == 0) html += $().emptyListItem('user');
    html += '</ul>';
    
    return html;
}
// $.fn.userList()


// remove an user from an alert
$.fn.deleteAlertUser = function(){
    
    $('span.delete_alert_user').on('click', function()
    {
        if(confirm('Are you sure? Click "OK" and this user will no longer receive this alert'))
            {
                var user_rem = $(this).parent().parent();
                var user_rem_id = user_rem.attr('data-alertuserid');
                var user_list = user_rem.parent('ul');
                
                $.post(siteRoot + "/modules/accidents/ajax/alerts.php",
                {
                    action : 'delete-user',
                    alert_id : user_rem.parent().parent('fieldset').data('id'),
                    user_id : user_rem_id
                },
                function(){
                    // slideup and remove
                    user_rem.slideUp('fast', function(){
                        $(this).remove();
                        // show the "empty" message
                        if(user_list.children().length == 0) user_list.append( $().emptyListItem('user') );
                    });
                }
            );
            }
    });
}
// $.fn.deleteAlertUser()