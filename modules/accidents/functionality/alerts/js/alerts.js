// functionality/alerts/alerts.js

// on DOM ready
$(function(){
    
    // kick off
    Alert.init();
    
    // create a blank alert, ready for configuring
    $('a.add-alert').click(function(e){
        e.preventDefault();
		var alert_num = $('#sets fieldset').length;
        $().createFieldset('',alert_num);
    });
    
    // add a new recipient
    $(document.body).on('click', 'a.add-alert-user', function(e){
        e.preventDefault();
        user_id = 0;
        Alert.currentAlert = $(this).parent().parent('fieldset').attr('id');
        
        actions = window.userTabs; // tabs for popup. set in php
        select_users(user_id, Alert.dialogOpen, Alert.dialogClose, actions);
    });
    
});


/*******************************************************************************
 * The Alert object
 * 
 * Keep everything relevant to Alerts in this object
 * 
 */
var Alert = {};

// the ID of the alert is being 'managed'
// this is used when looping through each alert and its users
Alert.currentAlert = ''; 

// set up the page
// all alerts & users are in the window.alerts JSON model
// loop through each one and add to the DOM

// NOTE: on the global page, the alerts are in the window.alerts var, 
// the gobalAlerts is empty and not used.
Alert.init = function ()
{
   // hide the save button
    $('#alert_save').hide();
    
    if(window.alerts.length > 0)
    {
        // get rid of the default text
        $('#sets').empty();
        // show the Save button
        $('#alert_save').show();
        
        // add each alert
        for(i in window.alerts)
        {
            $().createFieldset(window.alerts[i], i);
        }
    }
    if(typeof(window.globalAlerts) != 'undefined' && window.globalAlerts.length > 0)
    {
       $().globalAlerts();
    }
    
    // set up the delete alert buttons
    // alerts are deleted via ajax
    $().deleteAlert();
    
    // set up the delete user buttons
    // users are deleted via ajax
    $().deleteAlertUser();
    
    // set up the delete jobrole buttons
    // jobroles are deleted via ajax
    $().deleteJobrole();

	// set up the delete account manager buttons
    // account managers are deleted via ajax
    $().deleteAccountManager();
}

/**
 * return an empty alert object
 * used when adding a new alert to the page.
 * 
 */
Alert.emptyAlert = function ()
{
    return {
        "alert_id" : 0, 
        "values" : "{\"amount\": 0, \"unit\": \"\"}", 
        "user" : [ {
            "alertsuser_id" : "", 
            "user_name" : "", 
            "user_email" : "", 
            "usr_id" : 0
        } ] ,
        "jobroles" : [{
                "role_id"    : 0,
                "role_title" : "",
                "role_desc"  : "",
                "role_users" : [ "" ] // would be an array of names
        }] , 
        "accountmanager" : [{
				"alertmanager_id" : 0,
                "acc_manager_id"    : 0,
                "manager_title" : "",
                "manager_users" : [ "" ] // would be an array of names
        }]
    };
}
// Alert.emptyAlert()

/**
 * when the User dialog is opened
 * 
 * @todo when creating an Other User, check the email for validity
 * 
 */
Alert.dialogOpen = function (user_id) {

    // called when an user is clicked in the user list
    $(".select_users_table").delegate("td.sel_usr", "click", function(e){
        e.preventDefault();

        var data = $(this).find("input");
        var userid = data.val();
        var aider_usertype = data.attr("class");
        //user_id_loaded = user_id;       

        Alert.dialogCloseUser(userid);
        
        Alert.dialogDestroy();			
    });
    
     // called when an role is clicked in the job roles list
    $(".select_jobroles_table").delegate("td.sel_usr", "click", function(e){
        e.preventDefault();

        var data = $(this).find("input");
        var jobroleid = data.val();
        //user_id_loaded = user_id;

        Alert.dialogCloseJobrole(jobroleid);
        
        Alert.dialogDestroy();			
    });
    
    // called when the save is clicked on the Other User form
    $('input#submit_add_user').on('click', function(e){
        e.preventDefault(); // i.e. don't submit
        
        this_user_email = $(this).siblings('#user_email').val();
        this_user_name = $(this).siblings('#user_name').val()
        
        if(this_user_name != '' && this_user_email != '')
        {
            // get the values from the two fields, 
            // and send the to the form
            extra_user = { 
                    alertuser_id : 0, 
                    user_name : this_user_name, 
                    user_email : this_user_email, 
                    usr_id : null
                };

            Alert.dialogClose(extra_user);

            Alert.dialogDestroy();
        }
        else
        {
            alert('Both the name and email must be provided.')
        }
        
        return false;
    });
	
     // called when an role is clicked in the job roles list
    $(".select_am_table").delegate("td.sel_am", "click", function(e){
        e.preventDefault();

        var data = $(this).find("input");
        var am_id = data.val();
		
        //user_id_loaded = user_id;

        Alert.dialogCloseAccountmanager(am_id);
        
        Alert.dialogDestroy();			
    });
    	
	
}
        


        

/**
 * deregister a few events when the dialog is closed. 
 * this code was being replicated, so moved to a function
 */
Alert.dialogDestroy = function()
{
    $("#select_users_dialog").dialog('close').dialog('destroy').tabs('destroy');
    $("#select_users_dialog").undelegate("td.sel_usr", "click");
}

/*******************************************************************************
 * jQuery extensions
 * 
 * All the functions below have been contructed as jQuery functions. 
 * 
 * I've found that code runs much faster like this.
 * 
 * All functions are called through the global jQuery obect: $().functionName();
 * MG, 22/08/12
 * 
 */

/**
 * Takes an Alert JSON object, and renders a fieldset for each alert
 */
$.fn.createFieldset = function (alert, alert_counter)
{
	//set alert_type from hidden field
	var alert_type = $('#alert_type').val();

    $('#alert_save').show(); // in case it's hidden
    // increase the counter so we don't use a 0 based counter
    // array.length is used later to count
    alert_counter++; 
   
    // boolean, decides if the div is animated.
    // we only want an animated div if the alert is brand new, and not created whn the page is first loaded.
    var anim = false;
    if(typeof(alert) != 'object')
    {
        alert = Alert.emptyAlert();
        anim = true;
    }
    var fieldset = $('<fieldset>').attr('id', 'alert-'+alert_counter).addClass('email-alert');
    var html = "";
    
    // if this is an Alert taken from the DB
    if(alert.id > 0)
        {
            fieldset.data('id', alert.id); // tie the id programmatically with the fieldset element
            html += '<input type="hidden" name="alert['+alert_counter+'][id]" value="'+alert.id+'" class="alert_id" />';
        }

    html += '<span class="delete_alert" title="Delete this alert">&times;</span>';
    
    
    html += '<p>Send an alert ';
    // only show the amount/unit for investigation alerts
	//console.log('at2: ' + alert_type);
    if(alert_type == 'investigation')
    {
        html += 'after ';
        // amount
        html += '<select name="alert['+alert_counter+'][values][amount]">';

        // add each option
        // alertMinDays is set in alerts.php, and eventually will be configured for the install
        for(var j=window.alertMinDays;j < (window.alertMinDays + 30);j++)
        {
            html += '<option value="'+j+'"'+ ( (alert.amount == j) ? ' selected' : '')+'>'+j+'</option>';
        }

        html += '</select> ';

        // units
        html += '<select name="alert['+alert_counter+'][values][unit]">';
        var units = ['days', 'weeks'];
        for(m in units)
        {
            html += '<option value="'+units[m]+'"'+( (units[m] == alert.unit) ? ' selected' : '')+'>'+units[m]+'</option>';
        }

        html += '</select> ';
    }
    html += 'to the following:</p>';

    // add the user list <ul>
    html += $().userList(alert.users, alert_counter);
    
    html += $().jobroleList(alert.jobroles, alert_counter);

    html += $().accountmanagerList(alert.accountmanager, alert_counter);
    
    html += '<div><a href="#" title="Add a new email address" class="add-alert-user">Add a Recipient or Job Role</a></div>';
    //html += '</html>';
    
    fieldset.html(html);
    $('#alerts form div#sets').append(fieldset);
    
    if(anim == true)
    {
        fieldset.hide();            
        fieldset.slideDown('fast');
    }
       
}
// $.fn.createFieldset()











// if there's nothing in the list, show an "this is empty" message
$.fn.emptyListItem = function(name)
{
	var classname = name.replace(" ", "-");
	if (name == 'jobrole') name = 'Job Role';
	if (name == 'account manager') name = 'Account Manager';
    return '<li class="no'+classname+'">There are no current '+name.capitalize()+'s.</li>';
}



// delete an alert
$.fn.deleteAlert = function()
{
    // delete an alert
    $('span.delete_alert').on('click', function()
    {    
        if(confirm('Are you sure? Click "OK" to delete the alert'))
        {
            var fs_rem = $(this).parent('fieldset');
            
            // delete the alert
            $.post(siteRoot + "/modules/accidents/ajax/alerts.php",
                {
                    action : 'delete-alert',
                    alert_id : fs_rem.data('id')
                },
                function(){
                    // slideup and remove
                    fs_rem.slideUp('fast', function(){
                        $(this).remove();
                    });
                }
            );
        }
    });
}

/**
 * Add global alerts to the page
 */
$.fn.globalAlerts = function()
{
    var g_alert = window.globalAlerts;

    for(i in g_alert)
        {
            var ga_div = $('<div>').addClass('g_alert');
			var html = '';
			if (g_alert[i].amount != null) {
				html += '<p>Send after '+ g_alert[i].amount +' days to:</p>';
            }
            html += '<ul>';
            // users
            for(j in g_alert[i].users)
            {
                html += '<li>';
                html += g_alert[i].users[j].user_name + '('+ g_alert[i].users[j].user_email +')';
                html += '</li>';
            }
            // jobroles
            for(j in g_alert[i].jobroles)
            {                
                html += '<li>Job Role: ';
                html += g_alert[i].jobroles[j].role_title + ' ('+g_alert[i].jobroles[j].user.length+' user(s)';
                if(g_alert[i].jobroles[j].user.length > 0) 
                    {
                        html += ': '
                        var users = new Array();
                        for(k in g_alert[i].jobroles[j].user)
                            users.push(g_alert[i].jobroles[j].user[k].user_name);
                        
                        html += users.join(', ');
                    }
                html += ')';
                html += '</li>';
            }
			
			// account managers
            for(j in g_alert[i].accountmanager)
            {                
                html += '<li>Account Manager: ';
                html += g_alert[i].accountmanager[j].manager_title;
                html += ': '
				if(g_alert[i].accountmanager[j].manager_users.length > 0) 
				{
					var users = new Array();
					for(k in g_alert[i].accountmanager[j].manager_users) {
						if(g_alert[i].accountmanager[j].manager_users[k].username != null) {
							html += g_alert[i].accountmanager[j].manager_users[k].username;
						} else {
							html += "No user set";
						}
					}
				}
					
				html += '</li>';
            }
			
            html += '</ul>';
            
            ga_div.html(html);
            $('#global_alerts').append(ga_div);
        }
    
}
// $.fn.globalAlerts 

String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}