/**
 * Called after an user is clicked and the dialog is closing 
 * 
 * user_id can be either a user object or an account id
 */ 
Alert.dialogCloseJobrole = function (jr_id) 
{
    if (jr_id != 0)
    {
        var existing_jr_match = false;

        // find all roles for this alert, check that the role is there already
        $('#'+Alert.currentAlert).find('ul.alert_roles li input.jr_id').each(function(){
            if($(this).val() == jr_id) existing_jr_match = true;               
        });

        // only add the user if (s)he isn't already on the alert
        if(!existing_jr_match) 
        {            
            // remove the 'No Current Job Roles' message
            $('#'+Alert.currentAlert+' .nojobrole').remove();
            
            jr_ul = $('#'+Alert.currentAlert).find('ul.alert_roles');
            
            if(typeof jr_id != 'object' && jr_id > 0)
            {
                new_jr = $().getJobrole(jr_id);
            }
            else 
            {
                new_jr = jr_id;
            }

            this_alert_counter = $('#'+Alert.currentAlert).attr('id').split('-')[1];                

            role_counter = 0;
            role_counter = jr_ul.children('li').length;

            new_jr_li = $().insertJobrole(new_jr, this_alert_counter, role_counter); 

            jr_ul.append(new_jr_li);
        }
    }
}


// return job role list li & contents
$.fn.insertJobrole = function(jr, alert_count, k)
{
    html = '';
    if(jr.role_title.length > 0)
    {
        var alerts_jr_id = (typeof(jr.alerts_jobrole_id) == 'undefined') ? '0' : jr.alerts_jobrole_id;

        html = '<li class="jobrole" data-alertuserid="'+alerts_jr_id+'">'        
        html += '<h3>'+jr.role_title+'</h3>';
        html += '<input type="hidden" name="alert['+alert_count+'][jobrole]['+k+'][alerts_jobrole_id]" value="'+alerts_jr_id+'" />';
        html += '<input type="hidden" name="alert['+alert_count+'][jobrole]['+k+'][role_id]" class="jr_id" value="'+jr.role_id+'" />';
        html += '<div>';

        if(jr.user && jr.user.length > 0)
        {
            for(i in jr.user)
            {
                html += '<span class="jobroleuser">'+jr.user[i].user_name+'</span>';
            }
        }
        else if(window.account_id > 0) // only show this if it's not a global alert
        {
            html += '<span class="jobroleuser">There are no users in this role, no email will be sent. This can be changed in Manage Accounts > Job Roles</span>';
        }

        html+= '</div>';
        html += '<div class="alert_options"><span class="delete_jobrole" title="Remove '+jr.role_title+' from the alert">delete</span></div>';
        html += '</li>';
    }
    return html;

}


/**
 * gets job role data via ajax for the job role list.
 * 
 */
$.fn.getJobrole = function (jobroleid)
{
    var jr;
    var acc_id = $('#accountid').val();
    
    jQuery.ajax({
        url:    siteRoot + "/modules/accidents/ajax/alerts.php",
        data : { 
            action : 'get-jobrole',
            jr_id : jobroleid,
            acc_id: acc_id
        },
        dataType : 'json',
        success: function(data)
        {
            jr = data;
        },
        async:   false
    });  
    
    return jr;
}




$.fn.jobroleList = function(jobrole, alert_count)
{
    var html = '';
    
    html += '<h3>Job Roles</h3>';
    html += '<ul class="alert_roles">'
	k = 0;
    if(typeof(jobrole) != 'undefined') {
        for(j in jobrole)
        {
            if(jobrole[j].role_title != null && jobrole[j].role_title != '')
            {
                html += $().insertJobrole(jobrole[j], alert_count, j);
                k++;
            }
        }
    }
	if(k == 0)html += $().emptyListItem('jobrole');

    html += '</ul>';
    return html;
}





$.fn.deleteJobrole = function()
{
    $('span.delete_jobrole').on('click', function()
    {
        if(confirm('Are you sure? Click "OK" and users with this jobrole will no longer receive this alert'))
            {
                var user_rem = $(this).parent().parent();
                var user_rem_id = user_rem.attr('data-alertuserid');
                var job_list = user_rem.parent('ul');
                
                $.post(siteRoot + "/modules/accidents/ajax/alerts.php",
                {
                    action : 'delete-jobrole',
                    jr_id : user_rem_id
                },
                function(){
                    // slideup and remove
                    user_rem.slideUp('fast', function(){                        
                        $(this).remove();
                        if(job_list.children().length == 0) job_list.append($().emptyListItem('jobrole'));
                    });
                    
                    
                }
            );
            }
    });
}


