<?php
$this->addModuleLibPhp("funcAccidentEditGet.php");
$this->addModuleLibPhp("funcAccidentSaveDraft.php");
$this->addModuleLibPhp("funcAccidentSaveComplete.php");

$this->addModuleLibPhp("funcAccidentInjury.php");
$this->addModuleLibPhp("funcAccidentFirstAider.php");
$this->addModuleLibPhp("funcAccidentFrm.php");

$this->addModuleFuncJs("acc.investigation_required.js");


$this->addModuleFuncJs("acc.del_injury.jquery.js");
$this->addModuleFuncJs("acc.del_aider.jquery.js");
$this->addModuleFuncJs("acc.add_injury.jquery.js");
$this->addModuleFuncJs("acc.add_aider.jquery.js");

$this->addModuleFuncJs("acc.riddor_box.jquery.js");
$this->addModuleFuncJs("acc.first_aid_given_box_1.1.jquery.js");

$this->addModuleFuncJs("acc.get_injured_party_1.1.jquery.js");
$this->addModuleFuncJs("acc.get_site.jquery.js");
$this->addModuleFuncJs("acc.dependent_fields.jquery.js");

$this->addModuleFuncJs("acc.get_ambulance_caller_1.0.jquery.js");
$this->addModuleFuncJs("acc.get_senthome_staff_1.0.js");
$this->addModuleFuncJs("acc.get_restraint_staff_1.0.js");

$this->addModuleFuncJs("acc.accident_type.js");

$this->addModuleFuncJs("acc.upload_file_submit.js");



$this->csrf = 'ajax';

