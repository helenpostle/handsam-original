$(document).ready(function(){
	loadDepField("about_accident", "9", "fall_height", siteRoot);
	loadDepField("status", "OTHER", "status_other", siteRoot);
	loadDepField("status", "STUDENT", "year_group", siteRoot);
	
	function loadDepField(trig_fld, trig_val, fld, siteRoot) {
		var trig_fld = "#" + trig_fld;
		var fld_el = "#" + fld;
		$(trig_fld).change(function(event){
			var this_el = $(this);
			var fld_url = siteRoot + "/modules/accidents/fields/field.php";
			if ($(trig_fld).val() == trig_val) {
				
				if($(fld_el).length > 0) {
					$(fld_el).parent().show();
	 			} else {
					$.post(fld_url, {field: fld},function(data) {
	     				this_el.after(data)
	     			});	
	 			}		
				
			}  else {
	
				if ($(fld_el).size() > 0) {
					$(fld_el).parent().hide();
				}
			}
		});	
		
		
		$(trig_fld).closest('form').submit(function(event) {
			$(fld_el + ":hidden").remove();		
		});			
	}
	
	
	
	
});