$(document).ready(function(){
	$('#firstAidDetails .addItem').click(function(event){
		var item_num = ($("input[name='first_aider_id[]']").length) + 1;
		var cp = $(this).parent().next().html();
		$(this).parent().parent().append("<div class=\"clearfix first_aider\" id='new_item'>" + cp + "</div>");
		$('#new_item h4').html('First Aider ' + item_num);
		$('#new_item input').val('');
		$('#new_item textarea').html('');
		$("#new_item input[name='aider_num[]']").val(item_num);
		$("#new_item input[name='first_aider_id[]']").val(0);
		$("#new_item span.text").html('Select First Aider');
		$("#new_item .col").remove();
		$("#new_item option").remove();		
		$('#new_item a.deleteItem').html('Delete First Aider ' + item_num);
		$('html, body').animate({
			scrollTop: $("#new_item").offset().top
		}, 2000);		
		$("#new_item").removeAttr('id');
		
		return false;	

	});	
	

});