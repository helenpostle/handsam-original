/* 
 * User/AssocUser popup
 * 
 * used for selecting a staff member who restrained a child
 */

$(document).ready(function()
{
    // get the userid values
    var id = 0;
    var userid = $('input[name="restraint_userid"]').val()
    var associd = $('input[name="restraint_associd"]').val();
    var usertype = 0;
    
    if(userid > 0)
    {
        id = userid;
        usertype = 1;
    }
    else if(associd > 0)
    {
        id = associd;
        usertype = 2;
    }
    
    getUser(id, usertype);
	
    // open the ambulance caller user select
    $("#restraint_id").click(function(event){
        function fn() {
            $("#select_users_dialog").delegate("td.sel_usr", "click", function(){
                var data = $(this).find("input");
                var userid = data.val();
                var usertype = data.attr("class");
                getUser(userid, usertype);
                reloadUserList(userid, usertype);
                $("#select_users_dialog").dialog('close').dialog('destroy').tabs('destroy');		
                $("#select_users_dialog").undelegate("td.sel_usr", "click");
            });
        }
		
        function close_fn() {
            var userid = $('input[name="restraint_userid"]').val()
            var associd = $('input[name="restraint_associd"]').val();
            var usertype = 0;

            if(userid > 0)
            {
                id = userid;
                usertype = 1;
            }
            else if(associd > 0)
            {
                id = associd;
                usertype = 2;
            }
            getUser(id, usertype);
            reloadUserList(userid, usertype);
            $("#select_users_dialog").undelegate("td.sel_usr", "click");
        }
        select_users("", fn, close_fn);		
    });
	
    /**
     * get the user details (if set)
     * display the form fields with these details
     */
    function getUser(userid, usertype) 
    {
        if(userid == 0) return;
        
        $.post(siteRoot + "/modules/accidents/ajax/get_user.php", 
            {
                id: userid, 
                usertype: usertype
            }, 
            function(data) {
                $("#restraint_user").html(data);
            });
    }
	
    /**
     * put the user details in the fake dropdown
     */
    function reloadUserList(userid, usertype) 
    {
        if(userid == 0) return;
        $.post(siteRoot + "/modules/accidents/ajax/get_user_list.php", 
        {
            id: userid, 
            usertype: usertype
        }, 
        function(data) {
            if(data.usertype == 1)
                {
                    $('#restraint_id input[name="restraint_userid"]').val(data.id);
                    $('#restraint_id input[name="restraint_associd"]').val('0');
                }
            else if(data.usertype == 2)
                {
                    $('#restraint_id input[name="restraint_userid"]').val('0');
                    $('#restraint_id input[name="restraint_associd"]').val(data.id);
                }
            $("#restraint_id .text").html(data.name);
        },
        'json'
        );
    }


});

