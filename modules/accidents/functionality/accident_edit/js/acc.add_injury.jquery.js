$(document).ready(function(){
	$('#injuryDetails .addItem').click(function(event){
		var item_num = ($("input[name='injury_id[]']").length + 1);
		var cp = $(this).parent().parent().next().html();
		$(this).parent().parent().parent().append("<div class=\"clearfix\" id='new_item'>" + cp + "</div>");
		$('#new_item h4').html('Add Injury ' + item_num);
		$('#new_item option').removeAttr('selected');
		$('#new_item textarea').html('');
		$("#new_item [name='injury_num[]']").val(item_num);
		$("#new_item [name='injury_id[]']").val('');
		$('html, body').animate({
			scrollTop: $("#new_item").offset().top
		}, 2000);
		$("#new_item").removeAttr('id');

		return false;

	});	
	

});