$(document).ready(function(){
	$("#site_area").autocomplete(ac_work_site_area, {
		matchContains: true,
		minChars: 0
	});	
	
	$("#site_name").autocomplete(ac_work_site_name, {
		matchContains: true,
		minChars: 0
	});
	
	$("#site_address1").autocomplete(siteRoot + "/modules/accidents/ajax/autofill_site.php", {
		width: 260,
		matchContains: true,
		//selectFirst: false,
		extraParams: {
			site_name: function() { 
				//alert($("#site_name").val());
				return $("#site_name").val();
			},
			field: 'site_address1'
		}
	});

	$("#site_address2").autocomplete(siteRoot + "/modules/accidents/ajax/autofill_site.php", {
		width: 260,
		matchContains: true,
		//selectFirst: false,
		extraParams: {
			site_name: function() { 
				//alert($("#site_name").val());
				return $("#site_name").val();
			},
			field: 'site_address2'
		}
	});	
	
	$("#site_address3").autocomplete(siteRoot + "/modules/accidents/ajax/autofill_site.php", {
		width: 260,
		matchContains: true,
		//selectFirst: false,
		extraParams: {
			site_name: function() { 
				//alert($("#site_name").val());
				return $("#site_name").val();
			},
			field: 'site_address3'
		}
	});	
	
	$("#site_city").autocomplete(siteRoot + "/modules/accidents/ajax/autofill_site.php", {
		width: 260,
		matchContains: true,
		//selectFirst: false,
		extraParams: {
			site_name: function() { 
				//alert($("#site_name").val());
				return $("#site_name").val();
			},
			field: 'site_city'
		}
	});
	
	$("#site_postcode").autocomplete(siteRoot + "/modules/accidents/ajax/autofill_site.php", {
		width: 260,
		matchContains: true,
		//selectFirst: false,
		extraParams: {
			site_name: function() { 
				//alert($("#site_name").val());
				return $("#site_name").val();
			},
			field: 'site_postcode'
		}
	});	
	
	$("#site_tel1").autocomplete(siteRoot + "/modules/accidents/ajax/autofill_site.php", {
		width: 260,
		matchContains: true,
		//selectFirst: false,
		extraParams: {
			site_name: function() { 
				//alert($("#site_name").val());
				return $("#site_name").val();
			},
			field: 'site_tel1'
		}
	});
	
	$("#site_tel2").autocomplete(siteRoot + "/modules/accidents/ajax/autofill_site.php", {
		width: 260,
		matchContains: true,
		//selectFirst: false,
		extraParams: {
			site_name: function() { 
				//alert($("#site_name").val());
				return $("#site_name").val();
			},
			field: 'site_tel2'
		}
	});	
});