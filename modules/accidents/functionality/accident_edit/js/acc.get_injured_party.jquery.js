$(document).ready(function(){
	var injuredid = $("#injuredid").val();
	var injured_usertype = $("#injured_usertype").val();
	getInjuredParty(injuredid,injured_usertype);
	
/*	
	
	
	$("#injuredid").change(function(event){
		var injuredid = $(this).val();
		$.post(siteRoot + "/modules/accidents/ajax/get_injured_party.php", {injuredid: injuredid}, function(data) {
			$("#injured_party").html(data);
		});

	});
	
*/
	
	$("#injuredid").click(function(event){
		function fn() {
			$("#select_users_dialog").delegate("td.sel_usr", "click", function(){
				alert("edit injured");
				var data = $(this).find("input");
				var userid = data.val();
				var injured_usertype = data.attr("class");
				getInjuredParty(userid, injured_usertype);
				reloadInjuredParties(userid,injured_usertype);
				$("#select_users_dialog").dialog('close').dialog('destroy').tabs('destroy');
				$("#select_users_dialog").undelegate("td.sel_usr", "click");			
			});
		}
		
		function close_fn() {
			alert("closed injured");
			var injuredid = $("#injuredid").val();
			var injured_usertype = $("#injured_usertype").val();
			getInjuredParty(injuredid,injured_usertype);
			reloadInjuredParties(injuredid,injured_usertype);
			$("#select_users_dialog").undelegate("td.sel_usr", "click");
			
		}
		select_users("", fn, close_fn);
		
		
		
	});
	
	
	function getInjuredParty(injuredid, injured_usertype) {
		$.post(siteRoot + "/modules/accidents/ajax/get_injured_party.php", {injuredid: injuredid, injured_usertype: injured_usertype}, function(data) {
			$("#injured_party").html(data);
			
		});
	}

	function reloadInjuredParties(injuredid,injured_usertype) {
		
		
		$.post(siteRoot + "/modules/accidents/ajax/get_injured_party_list.php", {injuredid:injuredid, injured_usertype:injured_usertype}, function(data) {
			$("#injuredid").html(data);
		});
	}


});