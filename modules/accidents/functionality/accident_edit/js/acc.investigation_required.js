$(document).ready(function(){
    investigationToggle($('input[name="investigation_required"]:checked'));
	$('input[name="investigation_required"]').change(function(event){
		investigationToggle(this)
	});	
    
});

function investigationToggle(el) {
    if ($(el).val() == 0) {
        $('#int_investigator_userid').parent().hide();
        $('input[name="bt_investigation"]').parent().hide();
    } else {
        $('#int_investigator_userid').parent().show();
        $('input[name="bt_investigation"]').parent().show();
    }
}