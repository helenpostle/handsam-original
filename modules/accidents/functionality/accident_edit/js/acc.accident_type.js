/**
 * Behaviour when selecting different accident types.
 * 
 * MG 10-07-2013
 */

var IncidentDetailsTitle = '';
var InjuredPartyTitle = '';
var InjuredPartyField = '';
var InjuredPartyDropDown = '';

$(function(){
    
    // load defaults
    IncidentDetailsTitle = getIncidentDetailsTitle()
    InjuredPartyTitle = getInjuredPartyTitle();
    InjuredPartyField = getInjuredPartyField();
    InjuredPartyDropDown = getInjuredPartyDropDown();
    
    // check on page load
    toggleAccidentType();
    toggleNearMiss();
    
    // check on change
    $('input[name="accident_type"]').change(function()
    {
        setLabelDefaults(); // must be the first in this list
        toggleAccidentType();
        toggleNearMiss();
    });
});

/**
 * Reverts all changeable labels to their original values.
 * to be run before updating the values to new
 */
function setLabelDefaults()
{
    setIncidentDetailsTitle();
    setInjuredPartyTitle();
    setInjuredPartyField();
    setInjuredPartyDropDown();
	setInjuredPartyDropDown('Select the Injured Party');
}


function toggleAccidentType()
{
    var is_accident = ($('input[name="accident_type"]:checked').val() == 'ACCIDENT');
    $('.accident').toggle(is_accident);
}

function toggleNearMiss()
{
    var is_nearmiss = ($('input[name="accident_type"]:checked').val() == 'NEAR MISS');
    
    if(is_nearmiss)
    {
        
    }
}


function setIncidentDetailsTitle(val)
{
    val = val || IncidentDetailsTitle;
    $('#personalDetails h3').html(val);
}

function getIncidentDetailsTitle()
{
    return $('#personalDetails h3').html();
}


function setInjuredPartyTitle(val)
{
    val = val || InjuredPartyTitle;
    $('#accidentDetails h3').html(val);
}

function getInjuredPartyTitle()
{
    return $('#accidentDetails h3').html();
}


function setInjuredPartyField(val)
{
    val = val || InjuredPartyField;
	if (getBrowser()) $('label[for=injuredid]').html(val);
}

function getInjuredPartyField()
{
    return $('label[for=injuredid]').html();
}


function setInjuredPartyDropDown(val)
{
	val = ($('input[name="injuredid"]').val() > 0) ? getInjuredPartyDropDown() : val;
    if (getBrowser())$('#injuredid span.text').html(val);
}

function getInjuredPartyDropDown()
{
    return $('#injuredid span.text').html();
}

function getBrowser()
{
	var browser = navigator.userAgent.toLowerCase();
	if (browser.indexOf("windows nt 5.1") >= 0 && browser.indexOf("msie 8.0") >= 0) return false
	return true;
}