$(document).ready(function(){
	var siteid = $("#siteid").val();
	$.post(siteRoot + "/modules/accidents/ajax/get_site.php", {siteid: siteid}, function(data) {
		$("#accident_site").html(data);
	});
	
	$("#siteid").change(function(event){
		var siteid = $(this).val();
		$.post(siteRoot + "/modules/accidents/ajax/get_site.php", {siteid: siteid}, function(data) {
			$("#accident_site").html(data);
		});
	});
	

});