/* 
 * User/AssocUser popup
 */

$(document).ready(function()
{
    // get the userid values
    var id = 0;
    var userid = $('input[name="sent_home_userid"]').val()
    var associd = $('input[name="sent_home_associd"]').val();
    var usertype = 0;
    
    if(userid > 0)
    {
        id = userid;
        usertype = 1;
    }
    else if(associd > 0)
    {
        id = associd;
        usertype = 2;
    }
    
    getUser(id, usertype);
	
    // open the ambulance caller user select
    $("#sent_home_id").click(function(event){
        function fn() {
            $("#select_users_dialog").delegate("td.sel_usr", "click", function(){
                var data = $(this).find("input");
                var userid = data.val();
                var usertype = data.attr("class");
                getUser(userid, usertype);
                reloadUserList(userid, usertype);
                $("#select_users_dialog").dialog('close').dialog('destroy').tabs('destroy');		
                $("#select_users_dialog").undelegate("td.sel_usr", "click");
            });
        }
		
        function close_fn() {
            var userid = $('input[name="sent_home_userid"]').val()
            var associd = $('input[name="sent_home_associd"]').val();
            var usertype = 0;

            if(userid > 0)
            {
                id = userid;
                usertype = 1;
            }
            else if(associd > 0)
            {
                id = associd;
                usertype = 2;
            }
            getUser(id, usertype);
            reloadUserList(userid, usertype);
            $("#select_users_dialog").undelegate("td.sel_usr", "click");
        }
        select_users("", fn, close_fn);		
    });
	
    /**
     * get the user details (if set)
     * display the form fields with these details
     */
    function getUser(userid, usertype) 
    {
        if(userid == 0) return;
        
        $.post(siteRoot + "/modules/accidents/ajax/get_user.php", 
            {
                id: userid, 
                usertype: usertype
            }, 
            function(data) {
                $("#sent_home_user").html(data);
            });
    }
	
    /**
     * put the user details in the fake dropdown
     */
    function reloadUserList(userid, usertype) 
    {
        if(userid == 0) return;
        $.post(siteRoot + "/modules/accidents/ajax/get_user_list.php", 
        {
            id: userid, 
            usertype: usertype
        }, 
        function(data) {
            if(data.usertype == 1)
                {
                    $('#sent_home_id input[name="sent_home_userid"]').val(data.id);
                    $('#sent_home_id input[name="sent_home_associd"]').val('0');
                }
            else if(data.usertype == 2)
                {
                    $('#sent_home_id input[name="sent_home_userid"]').val('0');
                    $('#sent_home_id input[name="sent_home_associd"]').val(data.id);
                }
            $("#sent_home_id .text").html(data.name);
        },
        'json'
        );
    }


});

