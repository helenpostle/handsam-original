$(document).ready(function(){
	$('#firstAidDetails .deleteItem').on('click', function(e) {
		e.preventDefault();
		
		var num = $(this).next().val();
		var id = $(this).prev().val();
		
		$(this).parent().after("<input type='hidden' value='" + id + "' name='del_aider_id[]'/>");
		
		if (num == 1) {
		    // uncheck the radio button
		    // $('input:radio[name=first_aid]').filter('[value=0]').attr('checked', true)
		    // $('#firstAidGiven').toggle();
		    $(this).parent().hide();
		} else {
		    $(this).parent().remove();
		}

	});	
	
	$('#firstAidDetails .unDeleteItem').on('click', function() {
		$(this).parent().prev().show();
		$(this).parent().remove();
		return false;

	});
	
	
	
		
});