$(document).ready(function(){
	$('#injuryDetails .deleteItem').on('click', function() {
		var num = $(this).next().val();
		var id = $(this).prev().val();
		$(this).parent().after("<div class=\"clearfix\"><h4>Injury " + num + " Deleted</h4><a href=''class='unDeleteItem'>Un-Delete Injury</a><input type='hidden' value='" + id + "' name='del_injury_id[]'/></div>");
		$(this).parent().hide();
		return false;

	});	
	
	$('#injuryDetails .unDeleteItem').on('click', function() {
		$(this).parent().prev().show();
		$(this).parent().remove();
		return false;

	});		
});