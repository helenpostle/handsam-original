$(document).ready(function(){
	loadDepField("#about_accident", "9", "fall_height", siteRoot);
	loadDepField("#status", "OTHER", "status_other", siteRoot);
	loadDepField("#status", "STUDENT", "year_group", siteRoot);
	//loadDepField("input[name='riddor']", "1", "incident_type", siteRoot);
});	

	function loadDepField(trig_fld, trig_val, fld, siteRoot) {
		//var trig_fld = "#" + trig_fld;
		var fld_el = "[name='" + fld + "']";
		$(trig_fld).change(function(event){
			var this_el = $(this);
			var fld_url = siteRoot + "/modules/accidents/fields/field.php";
			if (this_el.val() == trig_val) {
				if($(fld_el).length > 0) {
					$(fld_el).parents('.fld_bound').show();
	 			} else {
					$.post(fld_url, {field: fld},function(data) {
	     				this_el.parent().after(data)
              if(trig_fld=='#status' && trig_val=='STUDENT')
              {
                $('#ctl.guardian_notified_date').live('click', function() {
                  alert('clicked');
                  /*
                  $(this).datepicker({
                    dateFormat: 'D, dd M yy',
                    showOn:'focus'
                  }).focus();
                  */
                });
                
              }
	     			});	
	 			}		
				
			}  else {
	
				if ($(fld_el).length > 0) {
					$(fld_el).parents('.fld_bound').hide();
				}
			}
		});	
		
		
		$(trig_fld).closest('form').submit(function(event) {
			$(fld_el + ":hidden").remove();		
		});			
	}
