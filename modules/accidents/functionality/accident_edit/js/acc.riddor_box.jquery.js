/**
 * Accident Form behaviours
 *
 * 
 * UPDATED: MG 11-07-2013
 */

$(document).ready(function(){

    toggleAccident();
    toggleRiddor();

    if ($("input[name='party_injured']:checked").val() == 0) hide_partyInjured(); 
    if ($("input[name='ambulance']:checked").val() == 0) hide_ambulance_details(); 
        
    var restoreAlert = false;
    var restoreAmbAlert = false	

    $("input[name='party_injured']").change(function(event){
        if ($("input[name='party_injured']:checked").val() == 0) {
            hide_partyInjured();
            alert("All Injury details will be deleted when the form is saved");
        } else 	if ($("input[name='party_injured']:checked").val() == 1 & restoreAlert) {
            show_partyInjured();
            alert("All Injury details restored");
        } else if($("input[name='party_injured']:checked").val() == 1) {
            show_partyInjured();
        }
        restoreAlert = true;
    });

    // toggle the ambulance div on radio button change
    $("input[name='ambulance']").change(function(event){
        if ($("input[name='ambulance']:checked").val() == 0) {
            hide_ambulance_details();
            alert("All ambulance details will be deleted when the form is saved");
        } else 	if ($("input[name='ambulance']:checked").val() == 1 & restoreAmbAlert) {
            show_ambulance_details();
            alert("All ambulance details restored");
        } else if($("input[name='ambulance']:checked").val() == 1) {
            show_ambulance_details();
        }
        restoreAmbAlert = true;
    });
	
    // event listener for riddor changes
    $("input[name=riddor]").change(function(event){
        toggleRiddor();
    });
	
    // event listener for accident_type changes
    $("input[name=accident_type]").change(function(event){
        toggleAccident();
        toggleInvestigator();
    });
	

    // now on submit - if the fields are hidden we must delete all fields before submitting.
	
    $('form.acc_form').submit(function(event) {		
        $(".acc_form div:hidden").remove();
        if ($("input[name='party_injured']:checked").val() == 0) {
            $('#partyInjured option').removeAttr('selected');
            $("input[name='injury_id[]']").each(function () {
                var value = $(this).val();
                $(this).after("<input type='hidden' value='" + value + "' name='del_injury_id[]'/>");
            });
        }
    });

	
});

// array of values corresponding to accident_type 
// values in this array will hide the accident investigator box when selected
var hide_investigator = [];

function toggleIncidentForms() 
{
    var acc_type = $('input[name=accident_type]:checked').val();
    
    if (acc_type == "NEAR MISS") {
        $('.acc_only').hide();
        hide_partyInjured();
    }
    if (acc_type == "ACCIDENT") {
        $('.acc_only').show();
        show_partyInjured();
    }
}

function hide_partyInjured() {
    $('#partyInjured').hide();
    $("input[name='party_injured']:eq(0)").attr('checked', 'checked');
}

function show_partyInjured() {
    $('#partyInjured').show();
    $("input[name='party_injured']:eq(1)").attr('checked', 'checked');
}

function hide_ambulance_details() {
    $('#ambulance_details').hide();
    $("input[name='ambulance']:eq(0)").attr('checked', 'checked');
}

function show_ambulance_details() {
    $('#ambulance_details').show();
    $("input[name='ambulance']:eq(1)").attr('checked', 'checked');
}

/**
 * Show/hide all accident related fields.
 * 
 */
function toggleAccident()
{
    $('.acc_only').toggle( $('input[name=accident_type]:checked').val() == 'ACCIDENT' );    
}

/**
 * Show/hide the riddor ref number and info boxes
 */
function toggleRiddor()
{
    $('.riddor_fld').toggle( ($('input[name=riddor]:checked').val() == 'RIDDOR') );
}

/**
 * Show/hide the investigator fields, dependant on what values are in the hide_investigator array
 */
function toggleInvestigator()
{
    var acc_type = $('input[name=accident_type]:checked').val();
    
    $('.investigator').toggle( ($.inArray(acc_type, hide_investigator) == -1) );
}

/**
 * Sets riddor radios to NON-RIDDOR
 * only if no value has already been set
 * also triggers the change event
 */
function setNonRiddor()
{
    if(typeof($('input[name="riddor"]:checked').val()) == 'undefined')
        $('#riddor_NON-RIDDOR').attr('checked', 'checked').trigger('change');
}