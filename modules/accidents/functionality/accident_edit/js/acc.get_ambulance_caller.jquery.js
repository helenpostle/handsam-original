$(document).ready(function(){
	var ambulance_caller_id = $("#ambulance_caller_id").val();
	var ambulance_caller_usertype = $("#ambulance_caller_usertype").val();
	getAmbulanceCaller(ambulance_caller_id,ambulance_caller_usertype);
	
	$("#ambulance_caller_id").click(function(event){
		function fn() {
			$("#select_users_dialog").delegate("td.sel_usr", "click", function(){
				var data = $(this).find("input");
				var ambulance_caller_id = data.val();
				var ambulance_caller_usertype = data.attr("class");
				getAmbulanceCaller(ambulance_caller_id,ambulance_caller_usertype);
				reloadAmbulanceCallers(ambulance_caller_id,ambulance_caller_usertype);
				$("#select_users_dialog").dialog('close').dialog('destroy').tabs('destroy');		
				$("#select_users_dialog").undelegate("td.sel_usr", "click");
			});
		}
		
		function close_fn() {
			var ambulance_caller_id = $("#ambulance_caller_id").val();
			var ambulance_caller_usertype = $("#ambulance_caller_usertype").val();
			getAmbulanceCaller(ambulance_caller_id,ambulance_caller_usertype);
			reloadAmbulanceCallers(ambulance_caller_id,ambulance_caller_usertype);
			$("#select_users_dialog").undelegate("td.sel_usr", "click");
		}
		select_users("", fn, close_fn);
		
		
		
	});
	
	
	function getAmbulanceCaller(ambulance_caller_id,ambulance_caller_usertype) {
		$.post(siteRoot + "/modules/accidents/ajax/get_ambulance_caller.php", {ambulance_caller_id: ambulance_caller_id, ambulance_caller_usertype: ambulance_caller_usertype}, function(data) {
			$("#ambulance_caller").html(data);
			
		});
	}
	
	function reloadAmbulanceCallers(ambulance_caller_id,ambulance_caller_usertype) {
		
		
		$.post(siteRoot + "/modules/accidents/ajax/get_ambulance_caller_list.php", {ambulance_caller_id: ambulance_caller_id, ambulance_caller_usertype: ambulance_caller_usertype}, function(data) {
			$("#ambulance_caller_id").html(data);
		});
	}


});