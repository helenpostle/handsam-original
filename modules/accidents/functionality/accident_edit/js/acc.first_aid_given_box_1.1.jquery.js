$(document).ready(function(){
    if ($("input[name='first_aid']:checked").val() == 0) $('#firstAidGiven').hide(); 
    var restoreAlert = false;
	
    $("input[name='first_aid']").change(function(event){
        $('#firstAidGiven').toggle();
        if ($("input[name='first_aid']:checked").val() == 0) alert("All First Aider details will be deleted when the form is saved");
        if ($("input[name='first_aid']:checked").val() == 1 & restoreAlert) alert("All First Aider details restored");
        restoreAlert = true;
    });

    //now on submit - if the fields are hidden we must delete all fields before submitting.
	
    $('form.acc_form').submit(function(event) {

        if ($("input[name='first_aid']:checked").val() == 0) {
            $("input[name='first_aider_id[]']").each(function () {
                var value = $(this).val();
                $(this).after("<input type='hidden' value='" + value + "' name='del_aider_id[]'/>");
            });
            $('#firstAidGiven textarea').html('');
        }
    });

	

    var sel_usr_flag = 0;
	
    $("#firstAidDetails").delegate("div.fakedd", "click", function(){ //
        sel_usr_flag = 0;
        var parent_el = $(this).parent().parent();
        var aider_num = parent_el.find("input[name='aider_num[]']").val();
        var fAcomments = parent_el.find("textarea").val();
        var first_aider_id = parent_el.find("input[name='first_aider_id[]']").val();
                
        function fn(aider_num) {
            $("#select_users_dialog").delegate("td.sel_usr", "click", function(){
                var data = $(this).find("input");
                var userid = data.val();
                var aider_usertype = data.attr("class");
                aider_num_loaded = aider_num;
                getFirstAider(userid, aider_usertype, parent_el, aider_num, fAcomments, first_aider_id);
				
                //reloadInjuredParties(userid,injured_usertype);
                close_fn(aider_num);
                $("#select_users_dialog").dialog('close').dialog('destroy').tabs('destroy');
                $("#select_users_dialog").undelegate("td.sel_usr", "click");			
            });
        }
        function close_fn(aider_num) {
            $('.first_aider').each(function() {
                var this_aider_num = $(this).find("input[name='aider_num[]']").val();
                var fAcomments = $(this).find("textarea").val();
                var first_aider_id = $(this).find("input[name='first_aider_id[]']").val();
                var userid = $(this).find(".aider_userid").val();
                var aider_usertype = $(this).find("input[name='aider_usertype[]']").val();
                var parent_el = $(this);
			
                if (!(aider_num == this_aider_num && sel_usr_flag == 1)) {
                    if(userid > 0)
                        getFirstAider(userid, aider_usertype, parent_el, this_aider_num, fAcomments, first_aider_id);
                }
            });
            $("#select_users_dialog").undelegate("td.sel_usr", "click");
        }
        select_users(aider_num, fn, close_fn);		
		
    });
	
    function getFirstAider(userid, aider_usertype, parent_el, aider_num, fAcomments, first_aider_id) {
        sel_usr_flag = 1;
        $.post(siteRoot + "/modules/accidents/ajax/get_first_aider.php", 
        {
            aider_userid: userid, 
            aider_usertype: aider_usertype, 
            aider_num:aider_num, 
            first_aider_id:first_aider_id
        }, 
        function(data) {
            parent_el.html(data);
            parent_el.find("textarea").html(fAcomments);
			
        });
    }
	
});