$(document).ready(function(){
	$("#siteid").after(" <img id='add_site' src='" + siteRoot + "/modules/accidents/images/add_site.png' alt='add site' title='add site'/> ");
	if ($("#siteid option").length > 1) {
		$("#siteid").after(" <img id='edit_site' src='" + siteRoot + "/modules/accidents/images/edit_site.png' alt='edit site' title='edit site'/>");
	}
  editSite();
	addSite();
});
	
	function addSite() {	
		$("#add_site").click(function(event){
			
			
			var qs = {siteid: 0};
			
			$.popUp({
				txt: 'text here',
				load_url: siteRoot + '/modules/accidents/functionality/accidents_popup/ajax/edit_site.php',
				elem: '#accident_site',
				elem_action: 'replace',
				ht: '500px',
				params: qs,
				delete_url: siteRoot + '/modules/accidents/index.php'
			}, "", function () {	
				reloadSites();
			});	
			
		});
	}

	function editSite() {	
		$("#edit_site").on('click', function() {
			
			
			var qs = {siteid: $("#siteid").val()};
			
			$.popUp({
				txt: 'text here',
				load_url: siteRoot + '/modules/accidents/functionality/accidents_popup/ajax/edit_site.php',
				load_params: qs,
				elem: '#accident_site',
				elem_action: 'replace',
				ht: '500px',
				params: qs,
				delete_url: siteRoot + '/modules/accidents/index.php'
			}, "", function () {	
				reloadSites();
			});	
			
		});
	}	
	
	function reloadSites() {
		var siteid = $('#edited_siteid').val();
		$.post(siteRoot + "/modules/accidents/ajax/get_site_list.php", {siteid:siteid}, function(data) {
			$("#siteid").html(data);
		});

		if ($("#edit_site").length == 0) {
			$("#siteid").after(" <img id='edit_site' src='" + siteRoot + "/modules/accidents/images/edit_site.png' alt='edit site' title='edit site'/>");
		}
	}	



