<?php
####make this a secure page
$secure = true;
$ajaxModule = "accidents";

require("../../../../../shared/startPage.php");

$pageFiles->addModFunc("accident_edit");
$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
/*
$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_process_form');
*/


$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
$tkn = getStrFromRequest("tkn");

if (isAllowed("recordAccidents")) {
	if ($pageVars->injuredid !== null) {
		if ($pageVars->injured_usertype == 2) {
			$qry = "select usr_assoc.* from usr_assoc where assoc_userid = {$pageVars->injuredid}";
		} else {
			$qry = "select usr.* from usr where userid = {$pageVars->injuredid}";
		}
		//$qry = "select acc_injured_party.* from acc_injured_party where injuredid = {$pageVars->injuredid}";
		$formDB = new pageQry($con, $qry);
		$formDB->rsItem(); 
		
		$load = getStrFromRequest("load");

		if (buttonClicked("save")) {
			
			if ($pageVars->injured_usertype == 2) {
				$qry = new dbInsert("usr_assoc");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
			} else {
				
				if ($pageVars->injured_usertype == 2) {
					$qry = new dbUpdate("usr_assoc");
					$qry->setParam("assoc_userid", $pageVars->injuredid);				
				} else {
					$qry = new dbUpdate("usr");
					$qry->setParam("userid", $pageVars->injuredid);				
				}
			}		
			//personal details
			$qry->setReqStringVal("firstname",$_POST["firstname"],"First Name");
			$qry->setReqStringVal("lastname",$_POST["lastname"],"Last Name");
			$qry->setReqStringVal("address1",$_POST["address1"],"Address 1");
			$qry->setStringVal("address2",$_POST["address2"],"Address 2");
			$qry->setStringVal("address3",$_POST["address3"],"Address 3");
			$qry->setStringVal("city",$_POST["city"],"City");
			$qry->setReqStringVal("postcode",$_POST["postcode"],"Postcode");
			$qry->setReqStringVal("tel1",$_POST["tel1"],"Telephone 1");
			$qry->setStringVal("tel2",$_POST["tel2"],"Telephone 2");
			$qry->setReqNumberVal("age",$_POST["age"],"Age");		
			$qry->setReqStringVal("sex",$_POST["sex"],"Sex");

			$qry->setAudit($clsUS->userid);
			if ($qry->execute($con)) {
				if ($pageVars->injuredid == 0) {
					$pageVars->injuredid = $qry->getNewID();	
				}
				//echo any parameters here that will be passed back to this page when successful
				echo "&injuredid=".$pageVars->injuredid."&injured_usertype=".$pageVars->injured_usertype;
			} else {
				echo "##ERROR: ";
				echo $qry->getError();
			}			
			
			
		} else if (buttonClicked("cancel")) {
			
			echo "cancel";
			
		} else {
			if ($load == 'yes') {
				if ($pageVars->injured_usertype == 2) {
					$qry = "select usr_assoc.* from usr_assoc where assoc_userid = {$pageVars->injuredid}";
				} else {
					$qry = "select usr.* from usr where userid = {$pageVars->injuredid}";
				}
				//$qry = "select * from injured_party where injuredid = ".$pageVars->injuredid;
				$formDB = new pageQry($con, $qry);
				$formDB->rsItem();
				echo $qry;
				echo "<div class=\"col\">";
					echo frmTextAreaNonEdit($formDB->row,"firstname","First Name");
					echo frmTextAreaNonEdit($formDB->row,"lastname","Last Name");
					echo frmTextAreaNonEdit($formDB->row,"address1","Address 1");
					echo frmTextAreaNonEdit($formDB->row,"address2","Address 2");
					echo frmTextAreaNonEdit($formDB->row,"address3","Address 3");
					echo frmTextAreaNonEdit($formDB->row,"city","City");
				echo "</div>";
				echo "<div class=\"col\">";
					echo frmTextAreaNonEdit($formDB->row,"postcode","Postcode");
					echo frmTextAreaNonEdit($formDB->row,"tel1","Telephone 1");
					echo frmTextAreaNonEdit($formDB->row,"tel2","Telephone 2");
					echo frmTextAreaNonEdit($formDB->row,"age","Age");
					echo frmTextAreaNonEdit($formDB->row,"sex","Sex");
					echo frmHiddenField($pageVars->injuredid,"edited_id");
					echo frmHiddenField($pageVars->injured_usertype,"injured_usertype");

				echo "</div>";
				
			} else {			
				//show form
				
				include("$rootPath/modules/accidents/content/page.injured_edit.php");
			}
		}
	}
}
?>