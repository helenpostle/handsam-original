<?php
####make this a secure page
$secure = true;
$ajaxModule = "accidents";

require("../../../../../shared/startPage.php");


$pageFiles->addModFunc("accident_edit");
if ($accidents_conf['site_tags']) $pageFiles->addModFunc('accident_tags');
$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
$tkn = getStrFromRequest("tkn");

if ($accidents_conf['site_tags']) {
	$tags_str = "";

	/**
	 * tags stuff
	 */
	 #get max number of tags for one item
	$qryTagList = "SELECT count(*) as cnt FROM acc_tags inner join acc_site_tags on acc_tags.tagid = acc_site_tags.tagid  group by acc_tags.tagid order by cnt desc limit 0,1";
	$resultTagList = getRS($con, $qryTagList);
	$tagRow = getRow($resultTagList);
	$max = $tagRow['cnt'];
	
	$class_size = $max / 6;
	if ($class_size == 0) $class_size = 1;
	
	$qryTagList = "SELECT acc_tags.tag, acc_tags.tagid, COUNT(acc_site_tags.tagid) AS cnt, CASE WHEN acc_site_tags.siteid = {$pageVars->siteid} THEN 1 ELSE 0 END AS site_cnt FROM acc_tags LEFT JOIN acc_site_tags ON acc_tags.tagid = acc_site_tags.tagid GROUP by acc_tags.tag, acc_tags.tagid ORDER BY tagid ASC";
	$resultTagList = getRS($con, $qryTagList);
}

if (isAllowed("recordAccidents")) {
	if ($pageVars->siteid !== null) {
		$qry = "select * from acc_accident_site where siteid = {$pageVars->siteid}";
		$formDB = new pageQry($con, $qry);
		$formDB->rsItem(); 
		
		
		if ($pageVars->siteid == 0 && !isset($_POST["site_name"])) {
			//get accountname  
			$_POST["site_name"] = $clsUS->accountname;
		}
		
		$load = getStrFromRequest("load");

		if (buttonClicked("save")) {
			if ($pageVars->siteid == 0) {
				$qry = new dbInsert("acc_accident_site");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
			} else {
				$qry = new dbUpdate("acc_accident_site");
				$qry->setParam("siteid", $pageVars->siteid);
			}		
			$qry->setReqStringVal("site_name",$_POST["site_name"],"Site Name");
			$qry->setStringVal("site_address1",$_POST["site_address1"],"Site Address 1");
			$qry->setStringVal("site_address2",$_POST["site_address2"],"Site Address 2");
			$qry->setStringVal("site_address3",$_POST["site_address3"],"Site Address 3");
			$qry->setStringVal("site_city",$_POST["site_city"],"City");
			$qry->setStringVal("site_postcode",$_POST["site_postcode"],"Site Postcode");
			$qry->setStringVal("site_tel1",$_POST["site_tel1"],"Site Telephone 1");
			//$qry->setStringVal("site_ref",$_POST["site_ref"],"Site Ref");
			$qry->setReqStringVal("site_area",$_POST["site_area"],"Site Area");
			if ($accidents_conf['site_tags'] && strlen($_POST['tags']) < 3 && $accidents_conf['site_tags_required']) $qry->failError .= "Please add at least one tag to this site \r\n";
			$qry->setAudit($clsUS->userid);
			if ($qry->execute($con)) {
				if ($pageVars->siteid == 0) {
					$pageVars->siteid = $qry->getNewID();	
				}
				if ($accidents_conf['site_tags']) {
					$tags = new accTags($pageVars->siteid, false, false);
					$tags->saveTags($_POST['tags'], $clsUS, $con);
				}
				
				//echo any parameters here that will be passed back to this page when successful
				echo "&siteid=".$pageVars->siteid;
			} else {
				echo "##ERROR: \r\n";
				echo $qry->getError();
			}			
			
			
		} else if (buttonClicked("cancel")) {
			
			echo "cancel";
			
		} else {
      if (trim($load) != '') {
				$qry = "select * from acc_accident_site where siteid = ".$pageVars->siteid;
				$formDB = new pageQry($con, $qry);
				$formDB->rsItem();

				echo "<div class=\"col\">";
					echo frmTextAreaNonEdit($formDB->row, "site_area","Site Area");
					echo frmTextAreaNonEdit($formDB->row,"site_name","Site Name");
					echo frmTextAreaNonEdit($formDB->row,"site_address1","Address 1");
					echo frmTextAreaNonEdit($formDB->row,"site_address2","Address 2");
					echo frmTextAreaNonEdit($formDB->row,"site_address3","Address 3");
				echo "</div>";		
				echo "<div class=\"col\">";	
					echo frmTextAreaNonEdit($formDB->row,"site_city","City");
					echo frmTextAreaNonEdit($formDB->row,"site_postcode","Postcode");
					echo frmTextAreaNonEdit($formDB->row,"site_tel1","Tel 1");
					if ($accidents_conf['site_tags']) echo frmRowNoInput("<span  class=\"\">".$_POST['tags']."</span>","Site Tags");
				echo "</div>";

				echo frmHiddenField($pageVars->siteid,"edited_siteid");
				
			} else {			
				if ($accidents_conf['site_tags']) {
					$tags = new accTags($pageVars->siteid, false, false);
					$tags_str = $tags->getTagsStr($con);
				}
				//show form
				
				include("$rootPath/modules/accidents/content/page.site_edit.php");
			}
		}
	}
}
?>