/**
 * Illness accident type
 * 
 * 
 */

$(function(){
    
    // first check on page load
    toggleIllness();
    toggleIllnessExtra();
    
    // check every time their value changes
    $('input[name=accident_type]').change(function(){ 
        toggleIllness();   
        toggleIllnessExtra();        
    });
    
    $('input[name=sent_home]').change(function(){
        toggleIllnessExtra();
    })    
});

// hide the investigator box when ILLNESS is selected
//	Removed	07/10/2014	(dcl)
//	Some Illness incidents require an Investigate.
// hide_investigator.push('ILLNESS');

/**
 * Show/hide all illness related fields, dependant on value of accident_type radios
 * 
 * All fields have a class of 'illness' in the HTML
 */
function toggleIllness()
{
    var is_illness = ($('input[name="accident_type"]:checked').val() == 'ILLNESS');
    $('.illness').toggle(is_illness);
    
    // if is_illness, then hide the investigator
    // $('.investigator').toggle(!is_illness);
    
    //new check on investigation_required input
    if ($('input[name="investigation_required"]').length>0) {
        var inv_req = $('input[name="investigation_required"]:checked');
        if(inv_req.val() == 0) {
            $('input[name="bt_investigation"]').parent().hide();
        }
    }
    
    if(is_illness) 
    {
        setNonRiddor();
        
        setIncidentDetailsTitle('Patient\'s Personal Details');
        setInjuredPartyField('Select Patient<span>* Required Field</span>');
        setInjuredPartyDropDown('Select Patient');
    }
}

/**
 * Show/hide the two extra Illness fields: sent home contact and sent home authorised by
 * only show if child was sent home, i.e. if sent home radio is "yes".
 */
function toggleIllnessExtra()
{
    $('.sent_home_extra').toggle( ($('input[name=sent_home]:checked').val() == 1) );
}