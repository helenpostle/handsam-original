<?php 
/**
 * lists lotc changes in form state 
 * 
 */
 
accidentBoxTop("Incident Log Filter");

	$filter_array = array();
	
	/*
	//get reporter list	array
		$filter_array['reporter'] = array();
		$qry = "SELECT  distinct u.userid AS a, u.username, CONCAT(u.firstname, ' ', u.lastname) AS b FROM usr u LEFT JOIN accident ac ON u.userid = ac.created_by  WHERE ac.accountid = {$pageVars->accountid}  ORDER BY u.lastname ASC";
		$result = getRS($con, $qry);
		while ($row = getRow($result)) {
			$filter_array['reporter'][$row['a']] = $row['b'];
		}	
	*/
	
	$filter_array['reporter'] = array(1 => "My reports", 2 => "Other reporters");
	$filter_array['riddor'] = array("RIDDOR" => "Riddor reportable",  "NON-RIDDOR" => "Non-Riddor reportable");
	
	$filter_array['state_inc'] = array("IN PROGRESS" => "In Progress Reports",  "COMPLETED" => "Completed Reports");
	$filter_array['state_inv'] = array("IN PROGRESS" => "In Progress Reports",  "COMPLETED" => "Completed Reports");
	$filter_array['date'] = array("created" => "Report Date", "accident_date" => "Accident Date");
	$filter_array['month'] = array(1 => "January", 2 => "February",3 =>  "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");
	
	//year array
	$this_year = date('Y', time());
	$start_year = 2010;
	//create year and month drop downs
	$filter_array['year'] = array();
	while ($start_year <= $this_year) {
		$filter_array['year'][$start_year] = $start_year;
		$start_year++;
	}

	$selected = "";
	foreach ($filter_array as $filter_name => $filter_name_array) {
		$qs = $qs_base;
		echo "<div class=\"box_row clearfix\">";
		
		
		switch ($filter_name) {
			case "state_inc":
			$disp_name =  "Incident report state";
			$all_name = "All reports";
			break;
			
			case "riddor":
			$disp_name =  $filter_name;
			$all_name = "Riddor and Non-Riddor";
			break;
			
			case "state_inv":
			$disp_name =  "Investigation report state";
			$all_name = "All reports";
			break;
			
			default:
			$disp_name = $filter_name;
			$all_name = "All ".$filter_name."s";
			break;
		}
		echo "<label for=\"$filter_name\">";
		echo $disp_name;
		echo "</label>";
	
		echo "<select id=\"$filter_name\">";
		
		$qs[$filter_name] = 0;
		$filter_link = "?filter=".rawurlencode(serialize($qs));
		if ($filter_name != 'date') {
			echo "<option value=\"$filter_link\">{$all_name}</option>";
		}
		foreach ($filter_array[$filter_name] as $filter_key => $filter_value) {
			$selected = "";
			$qs[$filter_name] = $filter_key;
			if (isset($qs_base[$filter_name]) && strval($qs_base[$filter_name]) == strval($filter_key)) { 
				$selected = "selected";
				echo $filter_name." : ".$qs_base[$filter_name]." : ".$filter_key;
			} else {
				$selected = "";
			}
			$filter_link = "?filter=".rawurlencode(serialize($qs));
			echo "<option $selected value=\"$filter_link\">$filter_value</option>";
		}
		echo "</select>";
		echo "</div>";
	}
	
accidentBoxBottom();
?>
	