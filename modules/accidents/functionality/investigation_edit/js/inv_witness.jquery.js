$(document).ready(function(){
	
	/*
	$('#witnessDetails select').each(function() {
		var parent_el = $(this).parent().parent();
		var this_w_num = parent_el.find("input[name='witness_num[]']").val();
		var w_statement = parent_el.find("textarea").val();
		var witness_id = parent_el.find("input[name='witness_id[]']").val();
		var userid = parent_el.find(".witness_userid").val();
		var witness_usertype = parent_el.find("input[name='witness_usertype[]']").val();
		var parent_el = $(this).parent().parent();
		//alert(userid);
		getWitness(userid, witness_usertype, parent_el, this_w_num, w_statement, witness_id);
	});
	*/
	//add witness
	$('#witnessDetails .addItem').click(function(event){
		var item_num = ($("input[name='witness_id[]']").length) + 1;
		var cp = $(this).parent().next().html();
		$(this).parent().parent().append("<div class=\"clearfix witness\" id='new_item'>" + cp + "</div>");
		$('#new_item h4').html('Witness ' + item_num);
		$('#new_item input').val('');
		$('#new_item textarea').html('');
		$("#new_item input[name='witness_num[]']").val(item_num);
		$("#new_item input[name='witness_id[]']").val(0);
		$("#new_item .col").remove();
		$("#new_item option").remove();
		$("#new_item select").append("<option value=''>Pick Witness</option>");
		
		$("#new_item").removeAttr('id');

		return false;

	});	

	//del witness
	$('#witnessDetails .deleteItem').on('click', function() {
		var num = $(this).next().val();
		var id = $(this).prev().val();
		$(this).parent().after("<input type='hidden' value='" + id + "' name='del_witness_id[]'/></div>");
		//if (num == 1) {
		//	$(this).parent().hide();
		//} else {
			$(this).parent().remove();
		//}
		return false;

	});	
	
	
	var sel_usr_flag = 0;
	
	$("#witnessDetails").delegate("select", "click", function(){
		sel_usr_flag = 0;
		var parent_el = $(this).parent().parent();
		var w_num = parent_el.find("input[name='witness_num[]']").val();
		var w_statement = parent_el.find("textarea").val();
		var witness_id = parent_el.find("input[name='witness_id[]']").val();
		function fn(w_num) {
			$("#select_users_dialog").delegate("td.sel_usr", "click", function(){
				var data = $(this).find("input");
				var userid = data.val();
				var witness_usertype = data.attr("class");
				w_num_loaded = w_num;
				getWitness(userid, witness_usertype, parent_el, w_num, w_statement, witness_id);
				//alert(w_num);
				close_fn(w_num);
				$("#select_users_dialog").dialog('close').dialog('destroy').tabs('destroy');
				$("#select_users_dialog").undelegate("td.sel_usr", "click");			
			});
		}
		function close_fn(w_num) {
			$('.witness').each(function() {
				var this_w_num = $(this).find("input[name='witness_num[]']").val();
				var w_statement = $(this).find("textarea").val();
				var witness_id = $(this).find("input[name='witness_id[]']").val();
				var userid = $(this).find(".witness_userid").val();
				var witness_usertype = $(this).find("input[name='witness_usertype[]']").val();
				var parent_el = $(this);
			
				if (!(w_num == this_w_num && sel_usr_flag == 1)) {
					getWitness(userid, witness_usertype, parent_el, this_w_num, w_statement, witness_id);
				}
			});
			$("#select_users_dialog").undelegate("td.sel_usr", "click");
		}
		select_users(w_num, fn, close_fn);
		
		
		
	});
	
	function getWitness(userid, witness_usertype, parent_el, w_num, w_statement, witness_id) {
		sel_usr_flag = 1;
		$.post(siteRoot + "/modules/accidents/ajax/get_witness.php", {witness_userid: userid, witness_usertype: witness_usertype, witness_num:w_num, witness_id:witness_id}, function(data) {
			parent_el.html(data);
			parent_el.find("textarea").html(w_statement);
			
		});
	}
	


});