var enableSaveButtons = function() {
	$('input[name="bt_save_submit"]').removeAttr("disabled").removeClass("disabled");
	// Add here other "save" buttons
}

var disableSaveButtons = function() {
	$('input[name="bt_save_submit"]').attr("disabled", "disabled").addClass("disabled");
	// Add here other "save" buttons
}


$(document).ready(function(){
	
	$('#confirm_RA_upload').bind("click",
		function(eventObject) {
			if( $(this).is(":checked") ) {
				enableSaveButtons();
			}
			else {
				disableSaveButtons();
			}
		}
	);
	
	disableSaveButtons();
});