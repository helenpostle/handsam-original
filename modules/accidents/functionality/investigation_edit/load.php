<?php
$this->addModuleLibPhp("funcInvestigationGet.php");
$this->addModuleLibPhp("funcInvestigationSave.php");
$this->addModuleLibPhp("funcWitness.php");
$this->addModuleFuncJs("inv_witness_1.1.jquery.js");
$this->addModuleFuncJs("inv.confirmRA.js");
$this->addModuleFuncJs("acc.upload_file_submit.js");

$this->csrf = 'ajax';

?>