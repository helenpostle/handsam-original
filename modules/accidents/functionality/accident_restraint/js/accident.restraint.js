/**
 * Restraint accident type
 * 
 * MG 10-07-2013
 */


$(function(){
    toggleRestraint();
    // check on every accident_type click
    $('input[name="accident_type"]').change(function(){
        toggleRestraint();
    });
    
});

/**
 * Show or hide restraint related fields, dependant on accident_type radio values
 * 
 * all fields have a class of 'restraint' in the HTML
 */
function toggleRestraint()
{
	var is_res = ($('input[name="accident_type"]:checked').val() == 'RESTRAINT');
    $('.restraint').toggle(is_res);
    
    // set to non-riddor
    if(is_res) 
    {
        setNonRiddor();
        
        setIncidentDetailsTitle('Restrainee\'s Personal Details');
        setInjuredPartyTitle('Restraint Incident Details');
        setInjuredPartyField('Select Restrainee<span>* Required Field</span>');
        setInjuredPartyDropDown('Select Restrainee');
    }
}
