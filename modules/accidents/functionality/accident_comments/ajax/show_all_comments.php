<?php
$secure = true;
$ajaxModule = "handsam_core";
require("../../../../../shared/startPage.php");
$pageFiles->includePhp();
$accidentid = getIntFromRequest("accidentid");
$limit = getIntFromRequest("limit");
$limit_qry = "";
if ($limit > 0) $limit_qry = " limit 0,$limit";
if (isAllowed("editAccount")) {
	$sql = "SELECT usr.firstname, usr.lastname, acc_comments.* FROM acc_comments LEFT JOIN usr ON acc_comments.created_by = usr.userid WHERE acc_comments.accidentid = $accidentid order by acc_comments.created desc $limit_qry";
	$comments = getRS($con, $sql);
	if (getResultRows($comments) > 0) {
		while ($row = getRow($comments)) {
		echo "<li class=\"clearfix\">";
		echo "<strong>{$row['firstname']} {$row['lastname']}</strong> : <em>".displayDateTime($row['created'])."</em>";
		//display edit button if comment is the users own
		//if ($row['created_by'] == $clsUS->userid) {
		//	echo " <a class=\"add_comment\" href=\"?commentid={$row['commentid']}&amp;accountid=$accountid\">edit</a>";
		//}
		
		echo displayParagraphs($row['commenttext']);
		echo "</li>";
		}
	}
}
?>