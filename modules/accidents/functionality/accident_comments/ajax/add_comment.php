<?php
$secure = true;

$ajaxModule = "accidents";

require("../../../../../shared/startPage.php");
$pageFiles->includePhp();
$commentid = getIntFromRequest("commentid");
$commenttext = getStrFromRequest("commenttext");
$comments = getStrFromRequest("comments"); //for a new acccident, this is the currently entered comments on the page
$load = getStrFromRequest("load");
$tkn = getStrFromRequest("tkn");

$pageVars = new loadVars($clsUS,$con);

if (isAllowed("recordAccidents")) {
		//now need to check if editing a comment, it is their comment
		if ($commentid !== null) {
			
			$qry = "select * from acc_comments where commentid = $commentid and created_by = {$clsUS->userid}";
			$rsCheck = getRS($con, $qry);
			if ($commentid == 0 || getResultRows($rsCheck) > 0) {
				$row = getRow($rsCheck);
				
				
				if (buttonClicked("save")) {
                    
                    if ($pageVars->accidentid == 0) {
                        //echo $_POST['commenttext'];                       
                    } else {
                        if ($commentid == 0) {
                            $qry = new dbInsert("acc_comments");
                            $qry->setReqNumberVal("accidentid",$pageVars->accidentid, "accidentid");
                        } else {
                            $qry = new dbUpdate("acc_comments");
                            $qry->setParam("commentid",$commentid);
                        }

                        $qry->setReqStringVal("commenttext",$_POST['commenttext'],"comment text");
                        $qry->setAudit($clsUS->userid);
                        //echo $qry->getSql();
                        if ($qry->execute($con)) {
                        } else {
                            echo "##ERROR: \r\n";
                            echo $qry->getError();	
                        }
                    }

				} else if (buttonClicked("cancel")) {
					
					echo "cancel";	
					
				} else if (buttonClicked("delete")) {
					
					if ($commentid > 0) {
						$qry = new dbDelete("acc_comments");
						$qry->setParam("commentid",$commentid);
						$qry->setParam("accidentid",$pageVars->accidentid);
						if ($qry->execute($con)) {
						} else {
							$messages[] = $qry->getError();	
						}
					}
					
				//} else if ($load != 'yes') {
				} else {
					if (trim($load) == 'yes') {
                        if ($pageVars->accidentid > 0) {
                            echo "<h3> Comments </h3>";
                            echo "<a class=\"add_comment\" href=\"?accidentid={$pageVars->accidentid}&amp;commentid=0\">add comment</a>";
                            $sql = "SELECT usr.firstname, usr.lastname, acc_comments.* FROM acc_comments LEFT JOIN usr ON acc_comments.created_by = usr.userid WHERE acc_comments.accidentid = {$pageVars->accidentid} order by acc_comments.created desc limit 0,8";
                            $comments = getRS($con, $sql);
                            echo "<ul id=\"comment_list\">";
                            if (getResultRows($comments) > 0) {
                                while ($row = getRow($comments)) {
                                    echo "<li class=\"clearfix\">";
                                    echo "<strong>{$row['firstname']} {$row['lastname']}</strong> : <em>".displayDateTime($row['created'])."</em>";
                                    //display edit button if comment is the users own
                                    //if ($row['created_by'] == $clsUS->userid) {
                                        //echo " <a class=\"add_comment\" href=\"?commentid={$row['commentid']}&amp;accountid=$accountid\">edit</a>";
                                    //}

                                    echo displayParagraphs($row['commenttext']);
                                    echo "</li>";
                                }
                                echo "</ul>";
                                //count comments - idf more than 8 show all
                                $comment_qry = "select commentid from acc_comments where accidentid = {$pageVars->accidentid}";
                                $comment_rs = getRS($con, $comment_qry);
                                if (getResultRows($comment_rs) > 8) {
                                    echo "<a class=\"show_all\" href=\"#\">show all</a>";
                                }
                            }
                        } else {
                            echo "<h3> Comments </h3>";
                            
                            echo "<ul id=\"comment_list\">";
                            echo "<li class=\"clearfix\">";
                            echo "<strong>{$clsUS->firstname} {$clsUS->lastname}</strong> : <em>".strftime($datetimeFormat,time())."</em>";
                            echo frmHiddenField($commenttext, "new_acc_comm");
                            echo displayParagraphs($commenttext);
                            echo "</li>";
                            echo "</ul>";
                        }
					} else {
					
						include("$rootPath/modules/accidents/content/page.comment_edit.php");
					}
						
				}
			}
				
		}
}
?>