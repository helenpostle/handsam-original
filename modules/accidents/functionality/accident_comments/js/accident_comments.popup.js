$(document).ready(function(){
	editComments();
	$('form.acc_form').submit(function(event) {
        if ($("#new_acc_comm").length) {
            var acc_comment = $("#new_acc_comm").val();
            $("#acc_comment").val(acc_comment);
        }
    });

});

function editComments() {

    $('a.add_comment').on('click', function(event) {
        event.preventDefault();
        var accidentid = $("#accidentid").val(); 
        var qs = {accidentid: accidentid, commentid:$(this).attr('href')};
        $.popUp({
            txt: 'text here',
            load_url: siteRoot + '/modules/accidents/functionality/accident_comments/ajax/add_comment.php',
            load_params: qs,
            elem: 'div.comments',
            elem_action: 'replace',
            ht: '500px',
            params: qs
        }, '', editComments);
    });		
}  