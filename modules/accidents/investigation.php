<?php
//error_reporting(E_ALL); 
//ini_set("display_errors", 1); 
/**
* accident report editing functionality
*
*/
####make this a secure page
$secure = true;
require("../../shared/startPage.php");
$pageFiles->addFunc("select_users");

$pageFiles->addModFunc("investigation_edit");
$pageFiles->addModFunc('file_upload');
$pageFiles->addModFunc("accident_edit");
$pageFiles->addModFunc('accident_comments');
$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->includePhp();

$menuPage = "index.php";

$pageVars = new loadVars($clsUS,$con);
if ($pageVars->hacking) headerLocation("$rootUrl/index.php");
$qs = array();

//some accident comments stuff
$a_comment = getStrFromRequest("comment");
if ($pageVars->accidentid > 0) {
	$pageFiles->addJsVar('item_id', $pageVars->accidentid);
}

if (isAllowed("recordAccidents")) {
	$box[] = "box.hse_info.php";
	//get filter array querystring
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;
	
	//set first crumbtrail link back to the index page with the list of visits
	//$crumb_back = "<a href=\"index.php?accountid={$pageVars->accountid}&amp;rand=".rand()."&amp;filter=".urlencode(serialize($qs))."\" title=\"\">Return to Incident Log Reports List</a>";
        
	$formDB = getInvestigation($con,  $pageVars);
	$witDB = getWitness($con, $pageVars);
	
	$intInv = getIntInvestigator($con, $pageVars);
        
    $inj_party = '';
    $accident = getAccident($con, $pageVars);
    if(strlen($accident['firstname']) > 0) $inj_party = $accident['firstname'].' '.$accident['lastname'];
        
	$title = "Incident Investigation Report - ".$inj_party;	
	
	if ($formDB['report_state'] == "COMPLETE" || (isAllowed("editAccount") && $formDB['acc_accountid'] !== $clsUS->accountid)) 
	{
		$title = "Incident Investigation Report - ".$inj_party;
		$content = "page.investigation_view.php";
		$box[] = "box.file_view.php";
        if ($clsUS->userid == $intInv['userid']) {
            $box[] = "box.edit_comments.php";
        } else {
            $box[] = "box.view_comments.php";
        }		/**
		 * Return 
		 */
		if (buttonClicked("return")) {
			headerLocation("$rootUrl/modules/accidents/accident.php?accidentid={$pageVars->accidentid}&filter=".urlencode(serialize($qs)),$messages);
		}
		if (buttonClicked("return_to_list")) {
			headerLocation("$rootUrl/modules/accidents/index.php?filter=".urlencode(serialize($qs)),$messages);
		}
	} 
	elseif(
		($formDB['report_state'] == "IN PROGRESS" || $formDB['report_state'] == "NEW" || $formDB['report_state'] == "") 
		&& ($intInv['userid'] ==  $clsUS->userid)
		)  
	{

		$content = "page.investigation_edit.php";
		$box[] = "box.file_upload.php";
        $box[] = "box.edit_comments.php";
		
		/** 
		 * Save draft
		 */
		if (buttonClicked("save_draft")  || isset($_POST['upload_file'])) 
		{
			$saveForm = saveDraftInvestigation($con, $pageVars, $clsUS);
			
			if ($saveForm == "") {
				$saveWitness = saveWitness($con, $pageVars, $clsUS);
				$delWitness = delWitness($con, $pageVars, $clsUS);
			} else {
				$delWitness = array();
				$saveWitness = array();
			}
			if ($saveForm == ""  && count($saveWitness) == 0  && count($delWitness) == 0) 
			{
				$messages[] = SAVED;
				if (isset($_POST['upload_file'])) 
				{
					headerLocation("$rootUrl/modules/accidents/upload_file.php?fileid={$_POST['upload_file']}&accidentid=".$pageVars->accidentid."&filter=".urlencode(serialize($qs)), $messages);
				} 
				else
				{
					//headerLocation("$rootUrl/modules/accidents/accident.php?accidentid={$pageVars->accidentid}&filter=".urlencode(serialize($qs)),$messages);
					//now just return to the incident list, not the incident form
					headerLocation("$rootUrl/modules/accidents/index.php?filter=".urlencode(serialize($qs)),$messages);

				}
			} 
			else 
			{
				$messages[] = $saveForm;
				$messages = array_merge($messages, $delWitness, $saveWitness);
			}	
		} 
		/**
		 * Save Submit 
		 */
		else if (buttonClicked("save_submit")) {
			$saveForm = saveCompleteInvestigation($con, $pageVars, $clsUS);
			if ($saveForm == "") {
				$saveWitness = saveWitness($con, $pageVars, $clsUS);
				$delWitness = delWitness($con, $pageVars, $clsUS);
			}  else {
				$delWitness = array();
				$saveWitness = array();
			}
			if ($saveForm == ""  && count($saveWitness) == 0  && count($delWitness) == 0) {
				$messages[] = SAVED_COMPLETE;
				//headerLocation("$rootUrl/modules/accidents/accident.php?accidentid={$pageVars->accidentid}&filter=".urlencode(serialize($qs)),$messages);
				//now just return to the incident list, not the incident form
				headerLocation("$rootUrl/modules/accidents/index.php?filter=".urlencode(serialize($qs)),$messages);
			} else {
				$messages[] = $saveForm;
				if (isset($delWitness) && isset($saveWitness) ) $messages = array_merge($messages, $delWitness, $saveWitness);
			}		

		} 
		/** 
		 * Cancel 
		 */
		else if (buttonClicked("cancel")) {
			headerLocation("$rootUrl/modules/accidents/accident.php?accidentid={$pageVars->accidentid}&filter=".urlencode(serialize($qs)),$messages);
		}
	} else if( isAllowed("recordAccidents") && $formDB['acc_accountid'] == $clsUS->accountid) {
		if (buttonClicked("return")) {
			headerLocation("$rootUrl/modules/accidents/accident.php?accidentid={$pageVars->accidentid}&filter=".urlencode(serialize($qs)),$messages);
			die();
		}
		if (buttonClicked("return_to_list")) {
			headerLocation("$rootUrl/modules/accidents/index.php?filter=".urlencode(serialize($qs)),$messages);
			die();
		}

		$title = "Incident Investigation Report - ".$inj_party;
		$content = "page.investigation_view.php";
        if ($clsUS->userid == $intInv['userid']) {
            $box[] = "box.edit_comments.php";
        } else {
            $box[] = "box.view_comments.php";
        }	}	
} else {

	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
