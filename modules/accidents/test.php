<?php

/**
* List accidents
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../../shared/startPage.php");

$pageFiles->addModFunc("accident_list");

$pageFiles->includePhp();
//priority for module functionality boxes over page boxes
$pageFiles->addBoxPriority();
$pageVars = new loadVars($clsUS,$con);

if (isAllowed("viewAccidents") || isAllowed("recordAccidents")) {

	if (!isset($pageVars->accidentid)) $pageFiles->addModFunc("accident_filter");
	if (!isset($pageVars->accidentid)) $pageFiles->addModFunc("accident_list");
	//$load = getStrFromRequest("load");
	$qs = array();
	//$lotc_highlight = "PLANNING";


	//get filter array querystring
	
	if (isset($_POST['filter'])) {
		$qs = __unserialize($_POST['filter']);
		$qsfrm = $_POST['filter'];
	} else if(isset($_GET['filter'])) {
		$qsfrm = $_GET['filter'];
		$qs = __unserialize(stripslashes($_GET['filter']));
	}
	//set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
	$qs_base = $qs;
	$filter_title = array();
	$filter_qry = array();
	$filter_date_field = " acc_accident.created ";
	//process filters
	//incident report state filter
	if (is_array($qs) && array_key_exists('state_inc', $qs)) { 
		switch ($qs['state_inc']) {
			case "ALL":
			$filter_qry['state_inc'] = " acc_accident.state != 'DELETED' ";
			$filter_title['state_inc'] = "All Incident Reports";
			break;
		
			case "IN PROGRESS":
			$filter_qry['state_inc'] = " acc_accident.state = 'IN PROGRESS' ";
			$filter_title['state_inc'] = "New incident ";
			break;
						
			case "COMPLETED":
			$filter_qry['state_inc'] = " acc_accident.state = 'COMPLETE' ";
			$filter_title['state_inc'] = "Completed Incident reports";
			break;

			case "DELETED":
			$filter_qry['state_inc'] = " acc_accident.state = 'DELETED' ";
			$filter_title['state_inc'] = "Deleted Incident Reports";
			break;
							
			default: 
			$filter_qry['state_inc'] = "  acc_accident.state != 'DELETED' ";
			$filter_title['state_inc'] = "All Incident Reports";
			break;
		}
	} else {
		$filter_qry['state_inc'] = "  acc_accident.state != 'DELETED' ";
		$filter_title['state_inc'] = "All Incident Reports";
	}
	



	//investigation report state filter
	if (is_array($qs) && array_key_exists('state_inv', $qs)) { 
		switch ($qs['state_inv']) {
			case "ALL":
			$filter_qry['state_inv'] = " 1=1 ";
			$filter_title['state_inv'] = "All Investigation Reports";
			break;
		
			case "IN PROGRESS":
			$filter_qry['state_inv'] = " acc_investigation.state = 'IN PROGRESS' ";
			$filter_title['state_inv'] = "New Investigation Reports ";
			break;
						
			case "COMPLETED":
			$filter_qry['state_inv'] = " acc_investigation.state = 'COMPLETE' ";
			$filter_title['state_inv'] = "Completed Investigation reports";
			break;

			case "DELETED":
			$filter_qry['state_inv'] = " acc_investigation.state = 'DELETED' ";
			$filter_title['state_inv'] = "Deleted Investigation Reports";
			break;
							
			default: 
			$filter_qry['state_inv'] = "  1=1 ";
			$filter_title['state_inv'] = "All Investigation Reports";
			break;
		}
	} else {
		$filter_qry['state_inv'] = "  1=1 ";
		$filter_title['state_inv'] = "All Investigation Reports";
	}
	

	//riddor value filter
	if (is_array($qs) && array_key_exists('riddor', $qs)) { 
		switch ($qs['riddor']) {
		
			case "ALL": 
			$filter_qry['riddor'] = " 1=1 ";
			//$filter_title['riddor'] = "";
			break;

			case "RIDDOR":
			$filter_qry['riddor'] = " acc_accident.riddor = 'RIDDOR' ";
			$filter_title['riddor'] = "Riddor Reportable ";
			break;
						
			case "NON-RIDDOR":
			$filter_qry['riddor'] = " acc_accident.riddor = 'NON-RIDDOR' ";
			$filter_title['riddor'] = "Non-Riddor Reportable ";
			break;

			default: 
			$filter_qry['riddor'] = " 1=1 ";
			//$filter_title['riddor'] = "";
			break;
		}
	} else {
		$filter_qry['riddor'] = " 1=1 ";
		//$filter_title['riddor'] = "";
	}
	


	//choose all reporters or just logged in reporter
	if (is_array($qs) && array_key_exists('reporter', $qs)) { 
		if ($qs['reporter'] == 1) {
			$filter_qry['reporter'] = "  acc_accident.created_by = {$clsUS->userid} ";
			$filter_title['reporter'] = "My reports";
		} else if ($qs['reporter'] == 2) {
			$filter_qry['reporter'] =  "  acc_accident.created_by != {$clsUS->userid} ";
			$filter_title['reporter'] = "Other reporters";
		} else {
			$filter_title['reporter'] = "All reporters";
		}
			
	}	
	
	//choose if date created or visit date filtered
	if (is_array($qs) && array_key_exists('date', $qs)) { 
		switch ($qs['date']) {
			case "accident_date":
			$filter_date_field = " acc_accident.accident_time ";
			$filter_title['date'] = "Incident Date";
			break;
				
			default: 
			$filter_date_field = " acc_accident.created ";
			$filter_title['date'] = "Report Date";
			break;
		}
	}	

	//month filter
	if (is_array($qs) && array_key_exists('month', $qs) && $qs['month'] >= 1) {
		$filter_qry['month'] = " MONTH($filter_date_field) = '{$qs['month']}' ";
		$filter_title['month'] = date( 'F', mktime(0, 0, 0, $qs['month']) );; 
	} else {
		$filter_title['month'] = "All months"; 

	}	
	
	//year filter
	if (is_array($qs) && array_key_exists('year', $qs) && $qs['year'] >= 1) {
		$filter_qry['year'] = " YEAR($filter_date_field) = '{$qs['year']}' ";
		$filter_title['year'] = $qs['year']; 
	} else {
		$filter_title['year'] = "All Years"; 

	}		
	
	//now implode filter title
	$filter_title_str = " ".implode(": ",$filter_title);
			
	if ($pageVars->accidentid !== null) {
	} else {
		
		//list accidents
		$title = "Incident Log: $filter_title_str";
		$content = "page.accident_list.php";
		$box[] = "acc.report_list.php";
		$pageDb = getAccidentList($pageVars, $con, $filter_qry);
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
?>