<?php 
/**
* report menu
*/
?>

<h3 class="reportList1">Incident Reports</h3>
<ul class="reportList">
	<li><a href="reporting.php?report=accident">Incident Report Detail by Date</a></li>
	<li><a href="reporting.php?report=accidentsummary">Incident Report Summary by Date</a></li>
	<li><a href="reporting.php?report=injuredparty">Incident Report by Injured Party</a></li>
	<li><a href="reporting.php?report=sitearea">Incident Report By Site Area</a></li>
	<li><a href="reporting.php?report=accidenttype">Incident Report By Incident Type</a></li>
</ul>
<h3 class="reportList1">Injury Reports</h3>
<ul class="reportList">
	<li><a href="reporting.php?report=injury_part">Injury Part Report Summary</a></li>
	<li><a href="reporting.php?report=injury_type">Injury Type Report Summary</a></li>
</ul>
