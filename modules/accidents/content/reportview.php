<?php
/**
 * when given a rs, outputs a nice table
 * first field is special field for link so do not output normally
 */

$fields = mysql_num_fields($rsReport);
if (getResultRows($rsReport) > 0) {
	
	//show what we chose
	echo "<table  class='report'>";
	
	$paramNames = array("startdate" => "Start Date","enddate" => "End Date","type" => "Type");
	
	foreach ($arrayParams as $key=>$value) {
		if (@$paramNames[$key] != "") {
			echo "<tr><td><b>".$paramNames[$key]."</b></td><td>$value</td></tr>";
		}
	}
	
	echo "</table><br/>";
	
	//show table of results
	
	echo "<table id='reportTable'>";
	//headings

	echo "<thead><tr>";
	for ($i = 2; $i < $fields; $i++) {
		//if ($sortCols[$i] != ""){
		//	echo "<th><a href=\"\">".mysql_field_name($rsReport,$i)."</a></th>";
		//} else {
	    	echo "<th>".mysql_field_name($rsReport,$i)."&nbsp;&nbsp;&nbsp;&nbsp;</th>";
    	//}
	}	
	echo "</tr></thead><tbody>";	
	while($rowReport = mysql_fetch_row($rsReport)){
	echo "<tr>";
	for ($i = 2; $i < $fields; $i++) {
		if ($i == 2 && $link != "") {
			echo "<td><a href='$link".$rowReport[0]."'>".displayText($rowReport[$i])."</a></td>";
		} else if ($i == 3 && $link2 != ""){
	    	echo "<td><a href='$link2".$rowReport[1]."'>".displayText($rowReport[$i])	."</a></td>";
		} else {
	    	echo "<td>".displayText($rowReport[$i])	."</td>";
    	}
	}	
	echo "</tr>";	
		
		
	} 
	echo "</tbody></table>";
} else {
	include "content/reportparameters.php";
}
 
?>