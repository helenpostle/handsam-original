<?php

if($pageVars->accidentid > 0)
{
    $disp_incidentid = "[ID: " . $pageVars->accidentid . "]";
}
else
{
    $disp_incidentid = "";
}

echo "<h2 style=\"margin-bottom: 0px;\">";
if(!( $formDB['firstname'] == null && $formDB['lastname'] == null ))
{
    $injured_name = $formDB['firstname'] . " " . $formDB['lastname'] . ", ";
    echo $injured_name;
}
if(!($formDB['accident_time'] == null ))
{
    //echo $formDB['accident_time'];
    $acc_date = date_parse_from_format("Y-m-d G:i:s", $formDB['accident_time']);
    $disp_acc_date = sprintf("%02d", $acc_date["day"]) . "/" . sprintf("%02d", $acc_date["month"]) . "/" . $acc_date["year"] . " - " . sprintf("%02d", $acc_date["hour"]) . ":" . sprintf("%02d", $acc_date["minute"]);
    echo $disp_acc_date;
}
echo "</h2><div class=\"clearfix\"></div>";

echo startFormTable($_SERVER["PHP_SELF"], "clearfix acc_form", "", "Incident Report $disp_incidentid", "Please enter the details of the incident below. If more than one person was injured, then a seperate report should be filled in for that person.  To make this easier, this report may be copied, so that all of the information, apart from the personal details, are already filled in.");

echo frmHiddenField($pageVars->accidentid, "accidentid");
if($pageVars->accidentid == 0)
    echo frmHiddenField($acc_comment, "acc_comment");
echo frmHiddenField($pageVars->accountid, "accountid");
echo frmHiddenField(htmlspecialchars(serialize($qs_base)), "filter");

echo "<div id=\"incidentType\" class=\"frmSection clearfix\">";
echo "<h3>Incident Type</h3>";

// accident type radios
echo frmExRadio($formDB, "accident_type", $acc_type_arr, "Incident Type", "");

// copy formDB just for this field
// as next line will cause formDB to show undefined index notices MG 03-07-2013
$riddorDB = $formDB;
if($accidents_conf['show_riddor_box'] == false && is_null($riddorDB["riddor"]))
    $riddorDB["riddor"] = '';

echo frmExRadio($riddorDB, "riddor", $riddor_arr, "Riddor Reportable", "Is the Incident Riddor Reportable?", '');
echo frmExTextField($formDB, "", "riddor_ref", 10, "HSE incident report number", true, "", 'riddor_fld riddor riddor_only');

echo "<div id=\"riddor_text\" class=\"riddor_only riddor_fld\">";
echo "<h4>RIDDOR INFORMATION</h4>";
echo "<p>RIDDOR stands for Reporting of Injuries, Diseases and Dangerous Occurrences Regulations 2013 and places a legal duty on employers, self-employed people, and people in control of premises to report certain types of major incidents.</p><p>Under the regulations you must report:";
echo "<ul class=\"\"><li><a target=\"_blank\" href=\"http://www.hse.gov.uk/riddor/guidance.htm#reportable\">deaths</a></li>";
echo "<li><a target=\"_blank\" href=\"http://www.hse.gov.uk/riddor/guidance.htm#reportable\">major injuries</a></li>";
echo "<li><a target=\"_blank\" href=\"http://www.hse.gov.uk/riddor/what-must-i-report.htm\">over-7-day injuries </a> - where an employee or self-employed person is away from work or unable to perform their normal work duties for more than 7 consecutive days</li>";
echo "<li>injuries to members of the public or people not at work where they are taken from the scene of an accident to hospital, (no link for this)</li>";
echo "<li><a target=\"_blank\" href=\"http://www.hse.gov.uk/riddor/guidance.htm#disease\">some work-related diseases</a> - <a target=\"_blank\" href=\"http://www.legislation.gov.uk/uksi/1995/3163/schedule/3/made\">Click here for a list of diseases</a></li>";
echo "<li><a target=\"_blank\" href=\"http://www.hse.gov.uk/riddor/guidance.htm#dangerous\">dangerous occurrences</a> - where something happens that does not result in an injury, but could have done</li>";
echo "<li><a target=\"_blank\" href=\"http://www.hse.gov.uk/riddor/guidance.htm#gas\">Gas Safe registered gas fitters </a> must also report dangerous gas fittings they find, and gas conveyors/suppliers must report some flammable gas incidents</li></ul>";
echo "RIDDOR applies to all work activities but not all incidents are reportable. If someone has had an accident in a work situation where you are in charge, and you are unsure whether to report it just call the Incident Contact Centre (ICC) on 0845 300 99 23. </p>";
echo "<p class=\"small\">Information supplied to HSE in a RIDDOR report is not passed on to your insurance company. If you think your insurer needs to know about a work related accident, injury, or case of ill health please remember to contact them separately - insurers have told us that reporting injuries and illnesses at work to them quickly could save you time and money.</p>";
echo "</div>";
echo "</div>";

//Personal info
echo "<div id=\"personalDetails\" class=\"frmSection clearfix\">";
echo "<h3>Incident Victim Personal Details</h3>";
//display select box of victims

if((isset($_POST['injured_usertype']) && $_POST['injured_usertype'] == 2) || ((!isset($_POST['injured_usertype']) && $formDB['injured_usertype'] == 2)))
{
    $injured_fld = 'assoc_userid';
    $injured_table = 'usr_assoc';
}
else
{
    $injured_fld = 'userid';
    $injured_table = 'usr';
}

if(isset($_POST['injured_usertype']))
{
    $injured_val = $pageVars->injuredid;
}
else
{
    $injured_val = $formDB[$injured_fld];
}

if($injured_val == "")
    $injured_val = 0;
$ip_qry = "select $injured_fld as a, Concat(firstname, ' ', lastname) as b from $injured_table where accountid = " . $pageVars->accountid . " and $injured_fld = $injured_val order by lastname";

//we can't use a form function here because of we could have a userid or assoc_userid
$rsInjured = getRS($con, $ip_qry);
$rowInjured = getRow($rsInjured);

// if the query was successul, else supply defaults
list($a, $b) = ($rowInjured) ? $rowInjured : array('', 'Select the Person Concerned');

$str = '<div id="injuredid" class="fakedd">';
$str .= '<input type="hidden" name="injuredid" value="' . $a . '" >';
$str .= '<span class="text">' . $b . '</span>';
$str .= '<span class="downarrow">&#x25BE;</span></div>';

echo frmRow($str, 'Select Person Concerned', 'injuredid', true);

echo "<div class=\"clearfix\">";

// victim type not in injured_party table but in accident table because it may change between accidents but it is important to record this as it was for each accident.
echo frmSelectArray($formDB, "status", $status_arr, "Status", true, "other");
//extra field if 'other' selected
if(showField("status", "status_other", "OTHER", $formDB))
{
    require("$rootPath/modules/accidents/fields/status_other.php");
}
//extra field if 'student' selected
if(showField("status", "year_group", "STUDENT", $formDB))
{
    require("$rootPath/modules/accidents/fields/year_group.php");
}
echo "</div>";
echo "<div id=\"injured_party\">";
echo frmHiddenFieldDb($formDB, "injured_usertype");
//echo "<input type=\"hidden\" value=\"{$formDB['injured_usertype']}\" name=\"injured_usertype\" id=\"injured_usertype\"/>";

echo "</div>";
echo "</div>";

//accident site details
echo "<div id=\"accidentSiteDetails\" class=\"frmSection clearfix\">";
echo "<h3>Incident Site Details</h3>";
$site_qry = "select siteid as a, Concat(site_name, ' - ', site_area) as b from acc_accident_site where accountid = " . $pageVars->accountid . " order by site_name, site_area";
echo frmExSelectQuery($formDB, "siteid", $site_qry, $con, "Select a site area", true);
echo "<div id=\"accident_site\" class=\"clearfix\">";
echo "<div class=\"col clearfix\">";
echo frmTextAreaNonEdit($formDB, "site_area", "Site Area");
echo frmTextAreaNonEdit($formDB, "site_name", "Site Name");
echo frmTextAreaNonEdit($formDB, "site_address1", "Address 1");
echo frmTextAreaNonEdit($formDB, "site_address2", "Address 2");
echo frmTextAreaNonEdit($formDB, "site_address3", "Address 3");
echo "</div>";
echo "<div class=\"col clearfix\">";
echo frmTextAreaNonEdit($formDB, "site_city", "City");
echo frmTextAreaNonEdit($formDB, "site_postcode", "Postcode");
echo frmTextAreaNonEdit($formDB, "site_tel1", "Tel 1");
//echo frmTextAreaNonEdit($formDB,"site_ref","Site Ref");
echo "</div>";
echo "</div>";
echo "</div>";


// date & time
echo "<div id=\"accidentDatetime\" class=\"frmSection clearfix\">";
echo '<h3>Date and Time</h3>';
echo frmExTimeDateField($formDB, "accident_time", "Incident Time &amp Date", true, '');
echo '</div>';


//accident details
echo "<div id=\"accidentDetails\" class=\"frmSection clearfix\">";
echo "<h3>Incident Details</h3>";
echo frmExSelectArray($formDB, "", "about_accident", $about_accident_arr, "About the Incident", true, "Please select which item best describes what happened.", "acc_only");

//extra field if 'fall from height' selected
if(showField("about_accident", "fall_height", "9", $formDB))
{
    require("$rootPath/modules/accidents/fields/fall_height.php");
}

echo frmExTextArea($formDB, "", "accident_desc", 10, "Incident Description", true, "What was happening beforehand?<br />What was the incident?<br />How were any injuries caused?", '');

if($accidents_conf['restraint_type'] == true)
{
    echo '<div class="restraint">';

    // echo frmTextArea($formDB, 'restraint_desc', 3, 'Restraint Incident Description', true, '');

    // get the user; either registered or associated
    // user or assoc userid
    if($formDB['restraint_userid'] > 0)
    {
        $acc_field = 'restraint_userid';
        $usr_field = 'userid';
        $usr_table = 'usr';
    }
    elseif($formDB['restraint_associd'] > 0)
    {
        $acc_field = 'restraint_associd';
        $usr_field = 'assoc_userid';
        $usr_table = 'usr_assoc';
    }

    $rowSH = false;
    if(isset($acc_field) && isset($usr_field) && isset($usr_table))
    {
        $sql = "SELECT $usr_field as a, Concat(firstname, ' ', lastname) as b FROM " . $usr_table . " WHERE accountid = " . $pageVars->accountid . " and " . $usr_field . " = '" . $formDB[$acc_field] . "';";

        $rsSH = getRS($con, $sql);
        $rowSH = getRow($rsSH);
    }

    // if the query was successul, else supply defaults
    list($a, $b) = ($rowSH) ? $rowSH : array('0', 'Member of staff performing restraint');

    $userid = '0';
    $associd = '0';
    if($formDB['restraint_userid'] > 0)
    {
        $userid = $a;
    }
    elseif($formDB['restraint_associd'] > 0)
    {
        $associd = $a;
    }

    $str = '<div id="restraint_id" class="fakedd">';
    $str .= '<input type="hidden" name="restraint_userid" value="' . $userid . '" >';
    $str .= '<input type="hidden" name="restraint_associd" value="' . $associd . '" >';
    $str .= '<span class="text">' . $b . '</span>';
    $str .= '<span class="downarrow">&#x25BE;</span></div>';

    echo frmRow($str, "Who performed the restraint?", "restraint_id", true, '');

    echo '<div id="restraint_user"></div>';
    echo '</div>';
}

if($formDB['ambulance'] == 0 || $formDB['ambulance'] == 1 || isset($_POST['ambulance']))
{
    echo frmExRadio($formDB, "ambulance", $yes_no_arr, "Ambulance Called", "", "acc_only");
}
else
{
    echo frmExRadioNonDb("ambulance", 0, $yes_no_arr, "Ambulance Called", "", "acc_only");
}

echo "<div id=\"ambulance_details\">";

//ambulance fields
echo frmExTextField($formDB, "", "ambulance_time", 6, "Ambulance response time", true, "(In minutes)", 'acc_only');
if(
        (isset($_POST['ambulance_caller_usertype'])
        && $_POST['ambulance_caller_usertype'] == 2)
        || ((!isset($_POST['ambulance_caller_usertype']) && $formDB['ambulance_caller_usertype'] == 2)))
{
    $amb_fld = 'ambulance_assoc_userid';
    $usr_fld = 'assoc_userid';
    $amb_table = 'usr_assoc';
}
else
{
    $amb_fld = 'ambulance_userid';
    $usr_fld = 'userid';
    $amb_table = 'usr';
}

if(isset($_POST['ambulance_caller_usertype']))
{
    $amb_val = $pageVars->ambulance_caller_id;
}
else
{
    $amb_val = $formDB[$amb_fld];
}

if($amb_val == "")
    $amb_val = 0;
$amb_qry = "select $usr_fld as a, Concat(firstname, ' ', lastname) as b from $amb_table where accountid = " . $pageVars->accountid . " and $usr_fld = $amb_val order by lastname";
//we can't use a form function here because we could have a userid or assoc_userid

$rsAmb = getRS($con, $amb_qry);
$rowAmb = getRow($rsAmb);

// if the query was successul, else supply defaults
list($a, $b) = ($rowAmb) ? $rowAmb : array('', 'Select the ambulance caller');

$str = '<div id="ambulance_caller_id" class="fakedd">';
$str .= '<input type="hidden" name="ambulance_caller_id" value="' . $a . '" >';
$str .= '<span class="text">' . $b . '</span>';
$str .= '<span class="downarrow">&#x25BE;</span></div>';

echo frmRow($str, "Select the ambulance caller", "ambulance_caller_id", true, "acc_only");
echo "<div id=\"ambulance_caller\">";

echo frmHiddenFieldDb($formDB, "ambulance_caller_usertype");
//echo "<input type=\"hidden\" value=\"{$formDB['ambulance_caller_usertype']}\" name=\"ambulance_caller_usertype\" id=\"ambulance_caller_usertype\"/>";
echo "</div>";
echo "</div>";
echo "</div>";


//injury details
echo "<div id=\"injuryDetails\" class=\"frmSection clearfix acc_only\">";
echo "<h3>Injury Details</h3>";
echo frmExRadio($formDB, "party_injured", $yes_no_arr, "Was the person injured", false, "acc_only");
echo "<div id=\"partyInjured\">";
echo frmSelectArray($formDB, "injury_Q1", $injury_Q1_arr, "What was the injury?", true, 'acc_only');
echo frmSelectArray($formDB, "injury_Q2", $injury_Q2_arr, "What happened to the injured person?", true, 'acc_only');
echo displayParagraphs("Please describe the details of the injury sustained ONLY by this person. <br/><strong><a class=\"addItem\" href=\"\">Click here to add another injury for this person</a>.</strong>", 'frm_info acc_only');

// get injuries
$frmInjuries = getFormInjuries($injuryDB->rows);
$injury_cnt = count($frmInjuries);
if($injury_cnt == 0)
    $injury_cnt = 1;
for($i = 0; $i < $injury_cnt; $i++)
{
    if(!isset($injuryDB->rows[$i]['injury_id']))
    {
        $injTitle = "Add";
        $inj_id = 0;
    }
    else
    {
        $injTitle = "Edit";
        $inj_id = $frmInjuries[$i]['injury_id'];
    }
    echo "<div class=\"clearfix\">";
    echo "<h4>$injTitle Injury " . ($i + 1) . "</h4>";
    echo frmExSelectArray($frmInjuries, $i, "injury_part[]", $injury_part_arr, "What was injured?", true, "", "acc_only", "");
    echo frmExSelectArray($frmInjuries, $i, "injury_type[]", $injury_type_arr, "Type of Injury", true, "", "acc_only", "");
    echo frmExTextArea($frmInjuries, $i, "injury_desc[]", 5, "Injury Description", true, "", "acc_only");
    echo frmHiddenField($inj_id, "injury_id[]");
    echo "<a href=\"\" class=\"deleteItem\">Delete This Injury</a>";
    echo frmHiddenField(($i + 1), "injury_num[]");
    echo "</div>";
}
echo "</div>";
echo "</div>";

//first aid details
echo "<div id=\"firstAidDetails\" class=\"frmSection clearfix clearfix acc_only\">";
echo "<h3>First Aid Details</h3>";
echo frmExRadio($formDB, "first_aid", $yes_no_arr, "First Aid Given", false, "acc_only");
echo "<div id=\"firstAidGiven\">";
echo frmExTextArea($formDB, "", "first_aid_desc", 5, "First Aid Description", true, "", "acc_only");
echo displayParagraphs("Please add the details of the first aider(s) who attended this person. <a class=\"addItem\" href=\"\">Click here to add another first aider</a>.", "frm_info acc_only");
$frmFirstAiders = getFormFirstAiders($fAiderDB->rows, $pageVars, $con);
$faCnt = count($frmFirstAiders);
if($faCnt == 0)
    $faCnt = 1;
for($i = 0; $i < $faCnt; $i++)
{
    if(!isset($fAiderDB->rows[$i]['first_aider_id']))
    {
        $fATitle = "Add";
        $fA_id = 0;
    }
    else
    {
        $fATitle = "Edit";
        $fA_id = $fAiderDB->rows[$i]['first_aider_id'];
    }
    echo "<div class=\"first_aider clearfix\" id=\"\">";

    echo "<h4>First Aider " . ($i + 1) . "</h4>";

	if(isset($_POST['fA_usertype'][$i]))
    {
        $fa_val = $_POST['aider_userid'][$i];
    }
    else
    {
        if(isset($frmFirstAiders[$i]))
        {
            $fa_val = $frmFirstAiders[$i]['aider_userid'];
        }
        else
        {
            $fa_val = 0;
        }
    }
	
    if((isset($_POST['fA_usertype'][$i]) && $_POST['fA_usertype'][$i] == 2) || ((!isset($_POST['fA_usertype'][$i]) && isset($frmFirstAiders[$i]) && $frmFirstAiders[$i]['fA_usertype'] == 2)))
    {
		$fa_qry = "select assoc_userid as a, Concat(firstname, ' ', lastname) as b from usr_assoc where accountid = " . $pageVars->accountid . " and assoc_userid = $fa_val order by lastname";
    }
    else if((isset($_POST['fA_usertype'][$i]) && $_POST['fA_usertype'][$i] == 1) || ((!isset($_POST['fA_usertype'][$i]) && isset($frmFirstAiders[$i]) && $frmFirstAiders[$i]['fA_usertype'] == 1)))
    {
		$fa_qry = "select userid as a, Concat(firstname, ' ', lastname) as b from usr where accountid = " . $pageVars->accountid . " and userid = $fa_val order by lastname";
    }
	else
	{
		$fa_qry = "select userid as a, Concat(firstname, ' ', lastname) as b from usr where accountid = " . $pageVars->accountid . " and userid = 0";
	}


    //we can't use a form function here because of we could have a userid or assoc_userid
    $rsFa = getRS($con, $fa_qry);
    $rowFa = getRow($rsFa);

    //*
    $fA_str = '<div id="aider_userid_' . ($i + 1) . '" class="fakedd">';
    if($injured_val > 0)
    {
        $fA_str .= '<input type="hidden" name="aider_userid[]" value="' . $rowFa['a'] . '" >';
        $fA_str .= '<span class="text">' . $rowFa['b'] . '</span>';
    }
    else
    {
        //var_dump($frmFirstAiders[$i]);
        $form_user_id = (isset($frmFirstAiders[$i])) ? $frmFirstAiders[$i]['aider_userid'] : '';
        $fA_str .= '<input type="hidden" name="aider_userid[]" value="' . $form_user_id . '" >';
        $form_user_name = (isset($frmFirstAiders[$i])) ? $frmFirstAiders[$i]['firstname'] . ' ' . $frmFirstAiders[$i]['lastname'] : 'Select the First Aider';
        $fA_str .= '<span class="text">' . $form_user_name . '</span>';
    }
    $fA_str .= '<span class="downarrow">&#x25BE;</span></div>';

    echo frmRow($fA_str, "Select a first aider", "aider_userid_" . ($i + 1), true, "full");

    if(count($frmFirstAiders) > 0)
    {
        echo "<input type=\"hidden\" value=\"{$frmFirstAiders[$i]['fA_usertype']}\" name=\"aider_usertype[]\" id=\"\"/>";
        echo "<div class=\"col clearfix\">";
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "firstname", "First Name");
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "lastname", "Last Name");
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "address1", "Address 1");
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "address2", "Address 2");
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "address3", "Address 3");
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "city", "City");
        echo "</div>";
        echo "<div class=\"col clearfix\">";
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "postcode", "Postcode");
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "tel1", "Telephone 1");
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "tel2", "Telephone 2");
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "age", "Age");
        echo frmTextAreaNonEdit($frmFirstAiders[$i], "sex", "Sex");
        echo "</div>";
    }

    echo frmExTextArea($frmFirstAiders, $i, "fAcomments[]", 5, "Comments", false, "Please enter report from this first aider.", "full fAcomments");
    echo frmHiddenField($fA_id, "first_aider_id[]");
    echo "<a href=\"\" title=\"$fA_id\" class=\"deleteItem\">Delete First Aider " . ($i + 1) . "</a>";
    echo frmHiddenField(($i + 1), "aider_num[]");
    echo "</div>";
}
echo "</div>";
echo "</div>";


if($accidents_conf['illness_type'] == true)
{
    echo '<div id="illnessDetails" class="frmSection clearfix illness">';
    echo '<h3>Illness Details</h3>';

    echo frmExRadio($formDB, 'paracetamol', $yes_no_arr, 'Was paracetemol administered?', '', '');
    echo frmExRadio($formDB, 'sent_home', $yes_no_arr, 'Was the child sent home?', '', '');
    echo frmExTextField($formDB, '', 'sent_home_contact', 100, 'Who was contacted?', true, '', 'sent_home_extra');

    // user or assoc userid
    if($formDB['sent_home_userid'] > 0)
    {
        $acc_field = 'sent_home_userid';
        $usr_field = 'userid';
        $usr_table = 'usr';
    }
    elseif($formDB['sent_home_associd'] > 0)
    {
        $acc_field = 'sent_home_associd';
        $usr_field = 'assoc_userid';
        $usr_table = 'usr_assoc';
    }

    $rowSH = false;
    if(isset($acc_field) && isset($usr_field) && isset($usr_table))
    {
        $sql = "SELECT $usr_field as a, Concat(firstname, ' ', lastname) as b FROM " . $usr_table . " WHERE accountid = " . $pageVars->accountid . " and " . $usr_field . " = '" . $formDB[$acc_field] . "';";

        $rsSH = getRS($con, $sql);
        $rowSH = getRow($rsSH);
    }

    // if the query was successul, else supply defaults
    list($a, $b) = ($rowSH) ? $rowSH : array('0', 'Who authorised the sending home?');

    $userid = '0';
    $associd = '0';
    if($formDB['sent_home_userid'] > 0)
    {
        $userid = $a;
    }
    elseif($formDB['sent_home_associd'] > 0)
    {
        $associd = $a;
    }

    $str = '<div id="sent_home_id" class="fakedd">';
    $str .= '<input type="hidden" name="sent_home_userid" value="' . $userid . '" >';
    $str .= '<input type="hidden" name="sent_home_associd" value="' . $associd . '" >';
    $str .= '<span class="text">' . $b . '</span>';
    $str .= '<span class="downarrow">&#x25BE;</span></div>';

    echo frmRow($str, "Who authorised the sending home?", "sent_home_id", true, 'sent_home_extra');

    echo '<div id="sent_home_user"></div>';

    echo '</div>';
} // if illness option


//Assign Investigators info
echo "<div id=\"investigators\" class=\"frmSection clearfix investigator \">";
echo "<h3>Internal Incident Investigation</h3>";

//is an investigation required?
if ($accidents_conf['investigation_optional'] == true) {
    echo frmExRadio($formDB, "investigation_required", array('No', 'Yes'), "Investigation Required", "Is an investigation required?", '');
}


//display select box of account users  for internal investigator
$int_qry = "select userid as a, Concat(lastname, ', ', firstname) as b from usr where accountid = " . $pageVars->accountid . " and state='ACTIVE' order by lastname";
if($formDB['inv_state'] == 'IN PROGRESS' || $formDB['inv_state'] == null)
{
    echo frmExSelectQuery($formDB, "int_investigator_userid", $int_qry, $con, "Select an internal investigator", false, '');
}
else
{
    $int_qry = "select userid as a, Concat(lastname, ', ', firstname) as b from usr where userid='" . $formDB['int_investigator_userid'] . "';";
    $int_rs = getRS($con, $int_qry);
    $int_row = getRow($int_rs);
    echo frmTextAreaNonEdit($int_row, "b", "Internal investigator");
}
echo "</div>";


//echo frmButtonHelp("Save as Draft", "save_draft", "Click here to save changes");
echo frmButtonHelp("Save ", "save_draft", "Save your progress so far");
//echo frmButtonConfirmHelp("Save as Complete", "save_submit", "Are you sure you want to complete this report? Once it is completed you may not edit it again.", "Click here to save as complete");
echo frmButtonConfirmHelp("Complete", "save_submit", "Are you sure you want to complete this report? Once it is completed you may not edit it again.", "Complete your report of this incident");
echo frmButtonConfirmHelp("Save as Draft &amp; Copy", "copy", "Are you sure you want to copy this incident?", "Click here to copy this incident");
echo frmButtonHelp("To Investigation Form", "investigation", "Click here to proceed to the <strong>investigation</strong>", 'investigator');
echo frmButtonHelp("Cancel", "cancel", "Click here to cancel");

echo endFormTable();
