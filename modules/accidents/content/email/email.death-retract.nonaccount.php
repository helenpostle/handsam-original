Dear %1$s,

An incident that happened at %2$s Academy which was previously reported as a Death, has now been downgraded. 

The incident occurred on %3$s. 

It was reported by %4$s.

Yours,

Handsam Alert Manager

* You're receiving this message as your details are listed to receive alerts for this level and type of incident. *