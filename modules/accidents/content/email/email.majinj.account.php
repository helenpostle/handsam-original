Dear %1$s,

An incident has happened at %2$s Academy which has been reported as a Major Injury. 

The incident occurred on %3$s. 

It was reported by %4$s.

You can view the details by logging into the Handsam system with the link below:

%5$s

Yours,

Handsam Alert Manager

* You're receiving this message as your details are listed to receive alerts for this level and type of incident. *