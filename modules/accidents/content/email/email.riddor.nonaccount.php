Dear %1$s,

A new incident report has been added/updated to RIDDOR status on the %2$s account on the Handsam system and you are set to receive alerts for this type and level of incident. 

The report is for an incident which occurred on %3$s. 

It was reported by %$4s.

Yours,

Handsam Alert Manager