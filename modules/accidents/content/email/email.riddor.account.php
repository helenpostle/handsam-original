Dear %1$s,

A new incident report has been added/updated to the %2$s account to RIDDOR status on the Handsam system and you are set to receive alerts for this type and level of incident. 

The report is for an incident which occurred on %3$s to %4$s. 

Please click the link below and login to the Handsam system to view the incident report. 

%5$s

Yours,

Handsam Alert Manager