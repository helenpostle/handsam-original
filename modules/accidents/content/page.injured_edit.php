<?php
//arrays for select boxes:


echo startFormTable($_SERVER["PHP_SELF"],"clearfix", "", "Incident Report", "Please edit the details of the injured party below.  Don't forget to click the 'save' button to save your changes.");
//echo formNotes(PLAN_FORM_TITLE, PLAN_FORM_INFO);


//Personal info
echo "<div id=\"personalDetails\" class=\"frmSection clearfix\">";
echo "<h3>Edit Incident Victim Details</h3>";
echo frmHiddenField($pageVars->injuredid,"injuredid");
echo frmHiddenField($pageVars->injured_usertype,"injured_usertype");

echo "<div class=\"col\">";
	echo frmTextField($formDB->row,"firstname",100,"First Name",true);
	echo frmTextField($formDB->row,"lastname",100,"Last Name",true);
	echo frmTextField($formDB->row,"address1",100,"Address 1",true);
	echo frmTextField($formDB->row,"address2",100,"Address 2",false);
	echo frmTextField($formDB->row,"address3",100,"Address 3",false);
	echo frmTextField($formDB->row,"city",100,"City",false);
echo "</div>";
echo "<div class=\"col\">";
	echo frmTextField($formDB->row,"postcode",100,"Postcode",true);
	echo frmTextField($formDB->row,"tel1",100,"Telephone 1",true);
	echo frmTextField($formDB->row,"tel2",100,"Telephone 2",false);
	echo frmTextField($formDB->row,"age",100,"Age",true);
	echo frmRadioArray($formDB->row,"sex",$sex_arr,"Sex");
echo "</div>";

			
	
	
echo "</div>";

echo frmButton("Save","save");
echo frmButtonHelp("Cancel","cancel");
echo frmHiddenField($tkn,"tkn");

echo endFormTable();

?>