<?php
if ($pageVars->accidentid > 0) {
	$disp_incidentid = "[ID: ".$pageVars->accidentid."]";
} else {
	$disp_incidentid = "";
}

$incident_title = "";
if ($inj_party != "") {
    $incident_title .= "<h2>$inj_party, ";

    if( ! ($formDB['accident_time'] == null ) )
    {
        //echo $formDB['accident_time'];
        $acc_date = date_parse_from_format("Y-m-d G:i:s", $formDB['accident_time']);
        $disp_acc_date = sprintf("%02d",$acc_date["day"])."/".sprintf("%02d",$acc_date["month"])."/".$acc_date["year"]." - ".sprintf("%02d",$acc_date["hour"]).":".sprintf("%02d",$acc_date["minute"]);
        $incident_title .= $disp_acc_date;
    }
    $incident_title .= "</h2>";
}

$created_by = getRecorderDetails($con, $formDB['created_by']);
$edited_by = getRecorderDetails($con, $formDB['edited_by']);
$audit =  "<div id=\"incident_audit\">";
$audit .= $incident_title;
$audit .= "<h3>Investigation entered by: <span>".$created_by['fullname']." [".displayDateTime($formDB['created'])."]</span></h3>";
if (isset($edited_by['fullname']) && $edited_by['fullname'] != "" )$audit .= "<h3>Investigation last edited by: <span>".$edited_by['fullname']." [".displayDateTime($formDB['edited'])."]</span></h3>";
$audit .= "</div>";

echo $audit; 


echo startFormTable($_SERVER["PHP_SELF"],"clearfix acc_form", "", "Incident Investigation Report $disp_incidentid", "Please enter the details of the incident investigation below. " );


echo frmHiddenField($pageVars->accidentid,"accidentid");
echo frmHiddenField($formDB['investigation_id'],"investigation_id");
echo frmHiddenField($pageVars->accountid,"accountid");

//Assign Investigators info
echo "<div id=\"investigators\" class=\"frmSection clearfix\">";
echo "<h3>External Incident Investigator</h3>";
	
echo frmTextAreaNonEdit($formDB,"ext_investigator_name","External investigator");

echo "</div>";



//witnesses
echo "<div id=\"witnessDetails\" class=\"frmSection clearfix clearfix\">";
	echo "<h3>Witness Details</h3>";
        
        if($formDB['no_witness'] == '1')
        {
            echo '<h4>There were no witnesses.</h4>';
        }
        else
        {
		$frmWitness = getWitnessArr($witDB->rows, $pageVars, $con);
		$wCnt = count($frmWitness);
		if ($wCnt == 0) $wCnt = 1;
		for ($i = 0; $i < $wCnt; $i++) {
			echo "<div class=\"witness clearfix\" id=\"\">";

				echo "<h4>Witness ".($i + 1)."</h4>";
				
				
			
				if (count($frmWitness) > 0) {
					echo "<div class=\"col clearfix\">";
						echo frmTextAreaNonEdit($frmWitness[$i],"firstname","First Name");
						echo frmTextAreaNonEdit($frmWitness[$i],"lastname","Last Name");
						echo frmTextAreaNonEdit($frmWitness[$i],"address1","Address 1");
						echo frmTextAreaNonEdit($frmWitness[$i],"address2","Address 2");
						echo frmTextAreaNonEdit($frmWitness[$i],"address3","Address 3");
						echo frmTextAreaNonEdit($frmWitness[$i],"city","City");
					echo "</div>";
					echo "<div class=\"col clearfix\">";
						echo frmTextAreaNonEdit($frmWitness[$i],"postcode","Postcode");
						echo frmTextAreaNonEdit($frmWitness[$i],"tel1","Telephone 1");
						echo frmTextAreaNonEdit($frmWitness[$i],"tel2","Telephone 2");
						echo frmTextAreaNonEdit($frmWitness[$i],"age","Age");
						echo frmTextAreaNonEdit($frmWitness[$i],"sex","Sex");
					echo "</div>";
					echo frmTextAreaNonEdit($frmWitness[$i], "witness_statement","Witness statement", "full");
				}
			echo "</div>";
		}
        }
	//echo "</div>";
echo "</div>";


//investigation causes
echo "<div id=\"investigationRootCauses\" class=\"frmSection clearfix\">";
	echo "<h3>Investigation</h3>";
	echo frmTextAreaNonEdit($formDB, "causes","Causes");
echo "</div>";




//post accident details
echo "<div id=\"postAccidentDetails\" class=\"frmSection clearfix\">";
	echo "<h3>Post Incident Details</h3>";
	echo frmTextAreaNonEdit($formDB,"issues_desc","Issues caused by Incident.");
	echo frmTextAreaNonEdit($formDB, "precautions","Actions Taken.");
echo "</div>";


//post accident details
echo "<div id=\"postAccidentDetails\" class=\"frmSection clearfix\">";
	echo "<h3>Follow-up Details</h3>";
	echo frmTextAreaNonEdit($formDB, "follow_up","Follow up details");
echo "</div>";


//post accident details
echo "<div id=\"impactAssessmentDetails\" class=\"frmSection clearfix\">";
	echo "<h3>Impact Assessment</h3>";
	echo frmTextAreaNonEdit($formDB,"days_off","Days off");
	echo frmTextAreaNonEdit($formDB,"cost","Cost (GBP)");
echo "</div>";

echo "<div class=\"frm_row clearfix \"><a class=\"frm_link\"  href=\"$rootUrl/modules/accidents/dashboard_accident.php?report_id=$report_id&amp;accountid=".$pageVars->accountid."&amp;accidentid=".$pageVars->accidentid."&amp;params=".urlencode($params)."\">View Incident Report</a></div>";
echo "<div class=\"frm_row clearfix \"><a class=\"frm_link\"  href=\"$rootUrl/handsam_core/dashboard_incident_detail.php?report_id=$report_id&amp;accountid=".$pageVars->accountid."&amp;params=".urlencode($params)."\">Return to Dashboard Report</a></div>";

echo endFormTable();

?>