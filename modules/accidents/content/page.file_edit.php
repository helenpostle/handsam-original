<?php
/**
 * Shows a  form for uploading a file
 * 
 */
if (isAllowed("recordAccidents")) {
			
	if (!isset($uploaded_filename) || $uploaded_filename == "") {
		$uploaded_filename = $pageDb->row['filename'];
	}
	
	echo startFileFormTable($_SERVER["PHP_SELF"], "", UPLOAD_FORM_TITLE, UPLOAD_FORM_INFO);
	echo frmHiddenField($pageVars->fileid,"fileid");
	echo frmHiddenField($pageVars->accountid,"accountid");
	if (isset($pageVars->accidentid)) {
		echo frmHiddenField($pageVars->accidentid,"accidentid");
	}
	echo frmHiddenField($uploaded_filename,"uploaded_filename");
	echo frmHiddenField(20000000,"MAX_FILE_SIZE");
	echo frmFileField("filename","Select File" ,true);
	if (isset($uploaded_filename) || $uploaded_filename != "") {
		echo "<div class=\"frm_row clearfix \"><span class=\"label\">Uploaded Filename</span><span class=\"value\"><div class=\"upload\"><a href=\"$rootUrl/modules/accidents/downloadAccDocument.php?id=".$pageVars->fileid."\" title=\"\">".$uploaded_filename."</a></div></span></div>";
	}
	
	
	echo frmHiddenField($uploaded_filename, 'uploaded_filename');
	
	echo frmTextField($pageDb->row,"title",100,"Title",true);
	
	echo frmShowAudit($pageDb->row,$con);	
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel upload");
	//if ($pageVars->fileid != 0 && $pageDb->row['created_by'] == $pageVars->userid)	
	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this file?", "Click here to delete this link");
		
	echo endFormTable();
	
}
?>