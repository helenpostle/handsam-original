<?php
 /**
  * this is the report parameter form
  */
?>

<form method="GET" action="reporting.php">
<input type="hidden" name="run" value="true">
<input type="hidden" name="report" value="<?php echo $report; ?>">



<?php if (isset($arrayParams["startdate"]) && @$arrayParams["startdate"] != "") { 
	echo frmDateFieldNonDb(strtotime($arrayParams["startdate"]),"startdate","Start Date",true);
		
 }

 if (isset($arrayParams["enddate"]) && @$arrayParams["enddate"] != "") { 
	echo frmDateFieldNonDb(strtotime($arrayParams["enddate"]),"enddate","End Date",true);
		
 }

 if (isset($arrayParams["reporterid"]) && @$arrayParams["reporterid"] != "") { 
	$qry = "select distinct acc_accident.created_by as a, Concat(usr.firstname, ' ', usr.lastname) as b from acc_accident left join usr on acc_accident.created_by = usr.userid where acc_accident.accountid = ".$accountid." and acc_accident.state = 'COMPLETE' order by lastname";
	$rsSelect = getRS($con,$qry);
	$str = "<select name=\"reporterid\"  id=\"reporterid\">\n";
	$str .= "<option value=\"0\"";
	if ($arrayParams["reporterid"] == 0) $str .= " selected ";
	$str .= ">All Reporters</option>";
	while ($row = getRow($rsSelect)) {
		$str .= "<option value=\"".$row[0]."\"";
		if ($row[0] == $arrayParams["reporterid"]) $str .= " selected ";
		$str .= ">".$row[1]."</option>";
	}
	$str .= "</select>";
	echo frmRow($str,"Select a reporter","reporterid",true);
		
 }
 
 
 if (isset($arrayParams["siteid"]) && @$arrayParams["siteid"] != "" ) { 
	$qry = "select siteid as a, CONCAT(site_name, ' - ', site_area) as b from acc_accident_site where accountid = ".$accountid." order by site_name,site_area";
	$rsSelect = getRS($con,$qry);
	$str = "<select name=\"siteid\"  id=\"siteid\">\n";
	while ($row = getRow($rsSelect)) {
		$str .= "<option value=\"".$row[0]."\"";
		if ($row[0] == $arrayParams["siteid"]) $str .= " selected ";
		$str .= ">".$row[1]."</option>";
	}
	$str .= "</select>";
	echo frmRow($str,"Select a site area","siteid",true);
		
 } 
 
 if (isset($arrayParams["injuredid"]) && @$arrayParams["injuredid"] != "" ) { 
	$qry = "select distinct ";
	
	$qry .= " CASE WHEN acc_accident.injured_usertype=2 THEN CONCAT('a_',ass.assoc_userid) ELSE CONCAT('u_',u.userid) END AS a, ";
	$qry .= " CASE WHEN acc_accident.injured_usertype=2 THEN CONCAT(ass.lastname, ', ', ass.firstname) ELSE CONCAT(u.lastname, ', ', u.firstname) END AS b, "; 
	$qry .= " CASE WHEN acc_accident.injured_usertype=2 THEN ass.lastname ELSE u.lastname END AS lastname FROM acc_accident  ";
	$qry .= " LEFT JOIN usr u ON acc_accident.userid = u.userid  ";
	$qry .= " LEFT JOIN usr_assoc ass ON ass.assoc_userid = acc_accident.assoc_userid  ";
	$qry .= " where acc_accident.accountid = ".$accountid." order by lastname";
	$rsSelect = getRS($con,$qry);
	$str = "<select name=\"injuredid\"  id=\"injuredid\">\n";
	while ($row = getRow($rsSelect)) {
		$str .= "<option value=\"".$row[0]."\"";
		if ($row[0] == $arrayParams["injuredid"]) $str .= " selected ";
		$str .= ">".$row[1]."</option>";
	}
	$str .= "</select>";
	echo frmRow($str,"Select an injured party","injuredid",true);
		
 } 
 
 if (isset($arrayParams["accident_type"]) && @$arrayParams["accident_type"] != "" ) { 
	$str = "";
	foreach ($acc_type_arr as $radio_key => $radio_val) {
		$str .= "$radio_val <input class=\"inputRadio\" type=\"radio\" name=\"accident_type\" value=\"$radio_key\"";
		if ($radio_key == $arrayParams["accident_type"]) $str .= " checked ";
		$str .= "/>";
	}
	
	echo frmRow($str,"Choose an accident type: ","accident_type",true);
	
 } 
 
 
 
 /*
 ?>


 
<?php if (@$arrayParams["enddate"] != "") {  
	 echo frmDateJ2($arrayParams["enddate"],"enddate","End Date",true); 

 } ?>

<?php if (@$arrayParams["level"] != "") { ?>
	<tr><td>Level</td><td>
		<select name="level" id="level">
	        <option value="1" <?php echo ($arrayParams["level"] == "1" ? "SELECTED" : "") ?>>Level 1</option>
	        <option value="2" <?php echo ($arrayParams["level"] == "2" ? "SELECTED" : "") ?>>Level 2</option>
	        <option value="3" <?php echo ($arrayParams["level"] == "3" ? "SELECTED" : "") ?>>Level 3</option>
	        <option value="All" <?php echo ($arrayParams["level"] == "All" ? "SELECTED" : "") ?>>All Levels</option>
        </select> 
	</td></tr>		
<?php } ?>

<?php if (@$arrayParams["delevel"] != "") { ?>
	<tr><td>Level</td><td>
		<select name="delevel" id="delevel">
	        <option value="2" <?php echo ($arrayParams["delevel"] == "2" ? "SELECTED" : "") ?>>Level 2</option>
	        <option value="3" <?php echo ($arrayParams["delevel"] == "3" ? "SELECTED" : "") ?>>Level 3</option>
	        <option value="All" <?php echo ($arrayParams["delevel"] == "All" ? "SELECTED" : "") ?>>All Levels</option>
        </select> 
	</td></tr>		
<?php } ?>


<?php if (@$arrayParams["type"] != "") { ?>
	<tr><td>Type</td><td>
		<select name="type" id="type">
        <option value="REGISTER" <?php echo ($arrayParams["type"] == "REGISTER" ? "SELECTED" : "") ?>>New Registrations</option>
        <option value="REREGISTER" <?php echo ($arrayParams["type"] == "REREGISTER" ? "SELECTED" : "") ?>>Re-Registrations</option>
        <option value="UPGRADE" <?php echo ($arrayParams["type"] == "UPGRADE" ? "SELECTED" : "") ?>>Upgrades</option>
        <option value="Direct Entry" <?php echo ($arrayParams["type"] == "Direct Entry" ? "SELECTED" : "") ?>>Direct Entries</option>
        <option value="All" <?php echo ($arrayParams["type"] == "All" ? "SELECTED" : "") ?>>All Registrations</option>
      </select>
	</td></tr>		
<?php } ?>
<?php if (@$arrayParams["tmc"] != "") { ?>
	<tr><td>Training Company</td><td>
		<select name="tmc" id="tmc">
		<option value="All" <?php echo ($arrayParams["tmc"] == "All" ? "SELECTED" : "") ?>>All Companies</option><?php 
			$rsTMC = getRS($con,"select companyid, companyname from ios_company where tmc = 1 and state <> 'obsolete' order by companyname");
			while ($rowTMC = getRow($rsTMC)) { ?>
				<option value="<?php echo $rowTMC["companyid"] ?>" <?php echo ($arrayParams["tmc"] == $rowTMC["companyid"] ? "SELECTED" : "") ?>><?php echo $rowTMC["companyname"] ?></option>	
			
			<?php } ?>
	        
        </select> 
	</td></tr>		
<?php } ?>

<?php if (@$arrayParams["assessorid"] != "") { ?>
	<tr><td>Assessor</td><td>
		<select name="assessorid" id="assessorid">
		<?php 
			$rsAssessor = getRS($con,"SELECT techid as assessorid, concat(surname,', ',firstname,' - ',irataid) as assessorname from ios_tech where assessor_until > now() ORDER BY surname ASC");
			while ($rowAssessor = getRow($rsAssessor)) { ?>
				<option value="<?php echo $rowAssessor["assessorid"] ?>" <?php echo ($arrayParams["assessorid"] == $rowAssessor["assessorid"] ? "SELECTED" : "") ?>><?php echo $rowAssessor["assessorname"] ?></option>	
			
			<?php } ?>
        </select> 
	</td></tr>		
<?php } ?>
<?php if (@$arrayParams["trainerid"] != "") { ?>
	<tr><td>Trainer</td><td>
		<select name="trainerid" id="trainerid">
		<?php 
			$rsTrainer = getRS($con,"SELECT techid as trainerid, concat(surname,', ',firstname,' - ',irataid) as trainername from ios_tech where assessor_until > now() ORDER BY surname ASC");
			while ($rowTrainer = getRow($rsTrainer)) { ?>
				<option value="<?php echo $rowTrainer["trainerid"] ?>" <?php echo ($arrayParams["trainerid"] == $rowTrainer["trainerid"] ? "SELECTED" : "") ?>><?php echo $rowTrainer["trainername"] ?></option>	
			
			<?php } ?>
        </select> 
	</td></tr>		
<?php } */?>





<input type="submit" name="runreport" value="Run Report"/>
</form> 
