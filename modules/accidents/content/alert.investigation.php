<?php

/**
 * Email alert form for late investigations
 *  
 */
?>
<div id="alerts">
    
    <p>Create and configure overdue Accident Investigation alerts for this account.</p>

    <form action="" method="post">
        
        <div id="sets">
            <hr>
            <p>There are <em>no</em> current alerts configured for this account. To create your first alert, click below. </p>
            <?php 
                // the page is rendered with JS via a JSON model.
                // see web/modules/accidents/functionality/alerts/js/alerts.js 
            ?>
        </div>
		<input type="hidden" value="<?php echo $account_id;?>" name="accountid"  id="accountid"/>
		<input type="hidden" value="investigation" name="alert_type"  id="alert_type"/>
        <input type="submit" value="Save" name="bt_Save" id="alert_save" />

    </form>

</div><!-- #alerts -->

<p><a href="#" class="add-alert" title="Create and configure a new alert">Create a new alert</a></p>

<h3>Global Alerts</h3>

<p>These alerts apply to this account and are common to all.</p>

<div id="global_alerts"></div>