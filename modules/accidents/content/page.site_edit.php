<?php

//arrays for select boxes:

echo startFormTable($_SERVER["PHP_SELF"], "clearfix", "", "Incident Report", "Please edit the details of the incident site below.  Don't forget to click the 'save' button to save your changes.");
//echo formNotes(PLAN_FORM_TITLE, PLAN_FORM_INFO);

echo "<div id=\"accidentSiteDetails\" class=\"frmSection clearfix\">";
echo "<h3>Part 2: Incident Site Details</h3>";
echo displayParagraphs("Please describe the details of the site where the incident occurred.", "frm_info");

echo frmHiddenField($pageVars->siteid, "siteid");

echo "<div class=\"col\">";
echo frmTextField($formDB->row, "site_name", 100, "Site Name", true);
echo frmTextField($formDB->row, "site_address1", 100, "Address 1", false);
echo frmTextField($formDB->row, "site_city", 100, "City", false);
echo frmTextField($formDB->row, "site_tel1", 100, "Tel 1", false);
echo "</div>";
echo "<div class=\"col\">";
echo frmTextField($formDB->row, "site_area", 200, "Site Area", true);
echo frmTextField($formDB->row, "site_address2", 100, "Address 2", false);
echo frmTextField($formDB->row, "site_address3", 100, "Address 3", false);
echo frmTextField($formDB->row, "site_postcode", 100, "Postcode", false);
//echo frmTextField($formDB->row,"site_ref",100,"Site Ref",false);
echo "</div>";

//tags area
if ($accidents_conf['site_tags']) {
  echo "<div class=\"full\">";
  echo frmTextAreaNonDb("tags", 4, "Tags", true, $tags_str, true);
  echo "<div class=\"frm_row\"><span class=\"label\">Select tags from:</span>";
  if (getResultRows($resultTagList) > 0) {

    echo "<ul id=\"tag_cloud\">";
    while ($rowTagList = getRow($resultTagList)) {
      $tag_selected = "";
      // remove from the list if the tag is already selected
      if (array_key_exists($rowTagList['tagid'], $tags->item_tags))
        continue;

      echo "<li class=\"$tag_selected\">";
      echo displayText($rowTagList['tag']);
      echo "</li>";
    }
    echo "</ul>";
  }
  echo "</div>";
  echo "</div>";
}



echo "</div>";

echo frmButton("Save", "save");
echo frmButton("Cancel", "cancel");
echo frmHiddenField($tkn, "tkn");

echo endFormTable();
?>