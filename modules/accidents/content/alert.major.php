<?php
/**
 * Global email alert form for riddor alerts
 *  
 */
if ($account_id == 0) {?>
	<div id="alerts">
		
		<p>Create and configure email alerts for incidents that are marked as a Death or a Major Injury.</p>

		<form action="" method="post">        
			<div id="sets">
				<?php 
					// the page is rendered with JS via a JSON model.
					// see web/modules/accidents/functionality/alerts/js/alerts.js 
				?>
			</div>
			<input type="hidden" value="0" name="accountid"  id="accountid"/>
			<input type="hidden" value="major" name="alert_type"  id="alert_type"/>
			<input type="submit" value="Save" name="bt_Save" id="alert_save" />

		</form>

	</div><!-- #alerts -->
	<?php
} else {
	?>
	<div id="global_alerts"></div>
	<?php
}
?>

