<?php
if ($pageVars->accidentid > 0) {
	$disp_incidentid = "[ID: ".$pageVars->accidentid."]";
} else {
	$disp_incidentid = "";
}

echo startFormTable($_SERVER["PHP_SELF"],"clearfix acc_form", "", "Incident Investigation Report $disp_incidentid", "Please enter the details of the incident investigation below. " );
//echo formNotes(PLAN_FORM_TITLE, PLAN_FORM_INFO);


echo frmHiddenField($pageVars->accidentid,"accidentid");
echo frmHiddenField($formDB['investigation_id'],"investigation_id");
echo frmHiddenField($pageVars->accountid,"accountid");
echo frmHiddenField(htmlspecialchars(serialize($qs_base)),"filter");

//Assign Investigators info
echo "<div id=\"investigators\" class=\"frmSection clearfix\">";
echo "<h3>External Incident Investigator</h3>";

echo frmTextField($formDB, 'ext_investigator_name', 100, 'External Investigator', false);

echo "</div>";

        

//witnesses
echo "<div id=\"witnessDetails\" class=\"frmSection clearfix clearfix\">";
	echo "<h3>Witness Details</h3>";
	
        echo displayParagraphs("Please add the details of the person(s) who witnessed this incident. To add another witness <a class=\"addItem\" href=\"\">click here</a>.", "frm_info");
        
        // no witness checkbox
        echo '<p style="margin-left:20px;">If there were no witnesses, please tick this box.</p>';
        echo frmCheckBox($formDB, 'no_witness', 'No Witnesses');
        
		$frmWitness = getWitnessArr($witDB->rows, $pageVars, $con);
		$wCnt = count($frmWitness);
		if ($wCnt == 0) $wCnt = 1;
		
		for ($i = 0; $i < $wCnt; $i++) {
			if (!isset($witDB->rows[$i]['witness_id'])) {
				$wTitle = "Add";
				$w_id = 0;
			} else {
				$wTitle = "Edit";
				$w_id = $witDB->rows[$i]['witness_id'];
			}
			
			echo "<div class=\"witness clearfix\" id=\"\">";

			echo "<h4>Witness ".($i + 1)."</h4>";
			
			if ((isset($_POST['witness_usertype'][$i]) && $_POST['witness_usertype'][$i] == 2) 
				|| (((isset($_POST['witness_usertype']) && !isset($_POST['witness_usertype'][$i])) && isset($frmWitness[$i]) && $frmWitness[$i]['witness_usertype'] == 2))) 
			{
			    $w_fld = 'assoc_userid';
			    $w_table = 'usr_assoc';
			} else {
				$w_fld = 'userid';
				$w_table = 'usr';
			}
			
			// the user OR user_assoc id
			$w_val = 0;
			if (isset($_POST['witness_usertype'][$i])) {
			    $w_val = $_POST['witness_userid'][$i];
			} else {
			    if (isset($frmWitness[$i]) && isset($frmWitness[$i][$w_fld])){
					$w_val = $frmWitness[$i][$w_fld];
			    }
			}
				
			$w_qry = "select $w_fld as a, Concat(firstname, ' ', lastname) as b from $w_table where accountid = '".$pageVars->accountid."' and $w_fld = '$w_val' order by lastname";
			
			// we can't use a form function here because if we could have a userid or assoc_userid
			$rsFa = getRS($con, $w_qry);
			$rowFa = getRow($rsFa);

			// if the query was successul, else supply defaults
			list($a, $b) = ($rowFa) ? $rowFa : array('', 'Pick Witness');
			$str = '<div id="witness_userid_'.$i.'" class="fakedd witness_userid\">';
			$str .= '<input type="hidden" name="witness_userid[]" value="'.$a.'" >';
			$str .= '<span class="text">'.$b.'</span>';
			$str .= '<span class="downarrow">&#x25BE;</span></div>';

			echo frmRow($str, 'Select a Witness', 'witness_userid_'.$i, true);

			if (count($frmWitness) > 0) 
			{
				if (isset($frmWitness[$i]['witness_usertype'])) {
					echo "<input type=\"hidden\" value=\"{$frmWitness[$i]['witness_usertype']}\" name=\"witness_usertype[]\" id=\"\"/>";
				} else {
					echo "<input type=\"hidden\" value=\"\" name=\"witness_usertype[]\" id=\"\"/>";
				}
				echo "<div class=\"col clearfix\">";
				echo frmTextAreaNonEdit($frmWitness[$i],"firstname","First Name");
				echo frmTextAreaNonEdit($frmWitness[$i],"lastname","Last Name");
				echo frmTextAreaNonEdit($frmWitness[$i],"address1","Address 1");
				echo frmTextAreaNonEdit($frmWitness[$i],"address2","Address 2");
				echo frmTextAreaNonEdit($frmWitness[$i],"address3","Address 3");
				echo frmTextAreaNonEdit($frmWitness[$i],"city","City");
				echo "</div>";
				echo "<div class=\"col clearfix\">";
				echo frmTextAreaNonEdit($frmWitness[$i],"postcode","Postcode");
				echo frmTextAreaNonEdit($frmWitness[$i],"tel1","Telephone 1");
				echo frmTextAreaNonEdit($frmWitness[$i],"tel2","Telephone 2");
				echo frmTextAreaNonEdit($frmWitness[$i],"age","Age");
				echo frmTextAreaNonEdit($frmWitness[$i],"sex","Sex");
				echo "</div>";
			}
			echo frmExTextArea($frmWitness, $i, "witness_statement[]",5,"Statement",false,"Please enter report from this witness.", "full witness_statement");
			echo frmHiddenField($w_id,"witness_id[]");	
			
			//if($w_id > 0)
			    echo "<a href=\"\" title=\"$w_id\" class=\"deleteItem\">Delete Witness</a>";
			
			echo frmHiddenField(($i + 1),"witness_num[]");
			echo "</div>";
		}
echo "</div>";


//Investigation outcome
echo "<div id=\"investigationRootCauses\" class=\"frmSection clearfix\">";
echo "<h3>Investigation</h3>";
echo frmExTextArea($formDB, "", "causes",5,"Causes",true,"What did the investigation find was the root caus(es) of the Incident?");
echo "</div>";


//post accident details
echo "<div id=\"postAccidentDetails\" class=\"frmSection clearfix\">";
	echo "<h3>Post Incident Details</h3>";
	if ($accidents_conf['investigation_issues_fld']) echo frmExTextArea($formDB, "", "issues_desc",5,"Issues",true,"Describe the issues caused by Incident.");
	echo frmExTextArea($formDB, "", "precautions",5,"Actions Taken",true,"Describe the precautions put in place since the incident to prevent a re-occurrence.");
echo "</div>";


//post accident details
echo "<div id=\"postAccidentDetails\" class=\"frmSection clearfix\">";
	echo "<h3>Follow-up Details</h3>";
	echo frmExTextArea($formDB, "", "follow_up",5,"Follow up details",true,"Record when and how you followed up contact with the victim/injured party, insurers and/or legal representatives");
echo "</div>";


//impact assessment details
echo "<div id=\"impactAssessmentDetails\" class=\"frmSection clearfix\">";
	echo "<h3>Impact Assessment</h3>";
	echo frmTextField($formDB,"days_off",100,"Days off",false);
	echo frmExTextField($formDB,"","cost",100,"Cost",false, "(GBP)");
echo "</div>";



//impact assessment details
echo "<div id=\"confirm_sign_off\" class=\"frmSection clearfix\">";
	echo "<h3>Confirm and Save</h3>";
	echo '<div style="float: none; display: block; margin: 0px 100px 15px 100px;"><label for="confirm_RA_upload" style="float:none; display:inline;">I confirm that I have revisited and updated where necessary the relevant Risk Assessments</label><input type="checkbox" id="confirm_RA_upload" /></div>';
	echo frmButtonHelp("Save as Draft","save_draft", "Click here to save changes");
	echo frmButtonConfirmHelp("Save as Signed Off","save_submit", "Are you sure you want to sign off this report? Once it is signed off you may not edit it again.","Click here to sign off");
echo "</div>";










echo frmButtonHelp("Cancel - Back to Incident Form","cancel", "Click here to cancel");

echo endFormTable();

?>