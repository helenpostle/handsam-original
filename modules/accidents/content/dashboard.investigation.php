<?php
/**
 * For accident investigators
 * 
 * Show all open investigations
 * 
 */

include("$rootPath/modules/accidents/library/funcAccidentList.php");

$filter_array[] = '';

// get user's investigations

$sql = "SELECT acc_investigation.state AS inv_state, acc_accident_site.site_name, acc_accident.*, acc_accident.state AS report_state, 
        CASE WHEN acc_accident.injured_usertype=2 THEN '' ELSE u.username END AS username, 
        CASE WHEN acc_accident.injured_usertype=2 THEN a.firstname ELSE u.firstname END AS firstname, 
        CASE WHEN acc_accident.injured_usertype=2 THEN a.lastname ELSE u.lastname END AS lastname from acc_accident
        LEFT JOIN usr_assoc AS a ON acc_accident.assoc_userid = a.assoc_userid 
        LEFT JOIN usr AS u ON acc_accident.userid = u.userid 
        LEFT JOIN acc_accident_site ON acc_accident.siteid = acc_accident_site.siteid 
        LEFT JOIN acc_investigation ON acc_accident.accidentid = acc_investigation.accidentid 
        WHERE acc_accident.accountid = {$clsUS->accountid} 
        AND int_investigator_userid = '{$clsUS->userid}' 
        ORDER BY accident_time ASC
        ;";

//$sql = "SELECT * FROM acc_accident WHERE accountid = '{$clsUS->accountid}' AND int_investigator_userid = '{$clsUS->userid}' AND state = 'IN PROGRESS';";

$rs = getRecSet($sql);
if(getResultRows($rs))
{
?>
    <h2>Current Investigations</h2>
    <p>You have been assigned to investigate the following incidents: </p>
    <table  id="acc_list" class="acc_list">
        <caption class="hidden"><?php echo $title; ?></caption>
        <thead>
            <tr>
                <th class="start ">ID</th>
                <th class="">Injured Person's Name</th>
                <th class="">Incident Date</th>
                <th class="">Incident Type</th>
                <th class="">Incident Report State</th>
                <th class="end">Investigation Report State</th>
            </tr>
        </thead>
        <tbody>
        <?php
        while($row = getRowAssoc($rs))
        {
            $url = "$rootUrl/modules/accidents/accident.php?accountid={$clsUS->accountid}&amp;accidentid={$row['accidentid']}&amp;rand=".rand();
		$url_inv = "$rootUrl/modules/accidents/investigation.php?accountid={$clsUS->accountid}&amp;accidentid={$row['accidentid']}&amp;rand=".rand();
		
		$investigation_state = "NOT STARTED";
		if ($row["inv_state"] != "") $investigation_state = $row["inv_state"];
		?>
		<tr>
			<td class="start"><?php echo $row["accidentid"]; ?></td>
			<td class=""><?php echo displayText($row["lastname"].", ".$row["firstname"]); ?></td>
			<td class=""><?php echo displayDate($row["accident_time"]); ?></td>
			<?php
			if ($row["riddor"] == "RIDDOR") {
				$riddor_state = "Riddor";
			} elseif($row["accident_type"] == 'ILLNESS') {
				$riddor_state = "Illness";
			} elseif($row["accident_type"] == 'RESTRAINT') {
				$riddor_state = "Restraint";
			} elseif ($row["riddor"] == "NON-RIDDOR") {
				$riddor_state = "Non-Riddor";
			} else {
				$riddor_state = "";
			}
			?>
			<td class=""><?php echo displayText($riddor_state); ?></td>
			<td class=""><a class="h_ul" href="<?php echo $url;?>"><?php echo ucwords(strtolower($row["report_state"])); ?></a></td>
			<td class="end"><a class="h_ul" href="<?php echo $url_inv;?>"><?php echo  ucwords(strtolower($investigation_state));?></a></td>
		</tr>
            <?php
        }
        ?>
        </tbody>
        <tfoot>
            <tr>
                <td class="start"></td>
                <td ></td>
                <td ></td>
                <td ></td>
                <td ></td>
                <td class="end"></td>
            </tr>
        </tfoot>
    </table>    
    <?php 
}
// display