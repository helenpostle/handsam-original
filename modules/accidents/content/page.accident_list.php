<?php
/***
** Shows the list of lotc plans
** 
***/

if ($pageDb->rsExists()) {
	
	echo displayParagraphs("<a class=\"add_visit\" href=\"$rootUrl/modules/accidents/accident.php?accountid={$pageVars->accountid}&amp;accidentid=0\">Add a new Incident Report</a>");
	?>
	<table  id="acc_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start ">ID</th>
				<th class="">Injured Person's Name</th>
				<th class="">Incident Date</th>
				<th class="">Incident Type</th>
				<th class="">Incident Report State</th>
				<th class="end">Investigation Report State</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	
	foreach ($pageDb->rows as $row) {
		$url = "$rootUrl/modules/accidents/accident.php?accountid={$pageVars->accountid}&amp;accidentid={$row['accidentid']}&amp;rand=".rand()."&amp;filter=".rawurlencode(serialize($qs));
		$url_inv = "$rootUrl/modules/accidents/investigation.php?accountid={$pageVars->accountid}&amp;accidentid={$row['accidentid']}&amp;rand=".rand()."&amp;filter=".rawurlencode(serialize($qs));
		
		$investigation_state = "NOT STARTED";
		if ($row["inv_state"] != "") $investigation_state = $row["inv_state"];
		?>
		<tr>
			<td class="start"><?php echo $row["accidentid"]; ?></td>
			<td class=""><?php echo displayText($row["lastname"].", ".$row["firstname"]); ?></td>
			<td class=""><?php echo displayDate($row["accident_time"]); ?></td>
			<?php
			if ($row["riddor"] == "RIDDOR") {
				$riddor_state = "Riddor";
			} elseif($row["accident_type"] == 'ILLNESS') {
				$riddor_state = "Illness";
			} elseif($row["accident_type"] == 'RESTRAINT') {
				$riddor_state = "Restraint";
			} elseif ($row["riddor"] == "NON-RIDDOR") {
				$riddor_state = "Non-Riddor";
			} else {
				$riddor_state = "";
			}
			?>
			<td class=""><?php echo displayText($riddor_state); ?></td>
			<td class=""><a class="h_ul" href="<?php echo $url;?>"><?php echo ucwords(strtolower($row["report_state"])); ?></a></td>
			<td class="end">
                <?php 
                if($row["investigation_required"] == 1 || !isset($accidents_conf['investigation_optional']) ||  (isset($accidents_conf['investigation_optional']) && $accidents_conf['investigation_optional'] == false)) {
                    ?> 
                    <a class="h_ul" href="<?php echo $url_inv;?>"><?php echo  ucwords(strtolower($investigation_state));?></a>
                    <?php
                } else {
                    echo "N/A";
                }
                ?>
            </td>
		</tr>
		<?php 
	} ?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td ></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>
	
	</table>
	<?php
} else {
	echo displayParagraphs("There are no Accident Reports to view");
	echo displayParagraphs("<a class=\"add_visit\" href=\"$rootUrl/modules/accidents/accident.php?accountid={$pageVars->accountid}&amp;accidentid=0\">Add a new Accident Report</a>");
}
