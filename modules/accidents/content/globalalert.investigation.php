<?php
/**
 * Global email alert form for late investigations
 *  
 */
?>
<div id="alerts">
    
    <p>You can create and configure global email alerts for overdue Accident Investigations. </p>
    <p>Global alerts check for late Incident Investigations across all schools.</p>

    <form action="" method="post">
        
        <div id="sets">
            <hr />
            <p>There are no current global email alerts configured for this install. To create your first alert, click below. </p>
            <?php 
                // the page is rendered with JS via a JSON model.
                // see web/modules/accidents/functionality/alerts/js/alerts.js 
            ?>
        </div>
		<input type="hidden" value="0" name="accountid"  id="accountid"/>
 		<input type="hidden" value="investigation" name="alert_type"  id="alert_type"/>
       <input type="submit" value="Save" name="bt_Save" id="alert_save" />

    </form>

</div><!-- #alerts -->

<p><a href="#" class="add-alert" title="Create and configure a new alert">Create a new alert</a></p>
