<?php
echo startFormTable($_SERVER['PHP_SELF'],
    'clearfix acc_form',
    '',
    'Bump Report - '.$reportDate->format('jS F Y'),
    'Please enter the details of any minor injuries occurring on this date.');

echo frmHiddenField($formDB['BumpID'], 'bumpID');
echo frmHiddenField($accountID, 'accountID');
echo frmHiddenField($reportDate->format('Y-m-d'), 'reportdate');

// Names of people injured
echo '<div id="postDetails" class="frmSection clearfix">';
echo '<h3>Names of Injured Parties</h3>';
echo frmExTextArea($formDB,
    '',
    'Details',
    10,
    'Names of People Injured',
    true,
    'List all the people who suffered a minor injury.');
echo '</div>';

// Notes
echo '<div id="postNotes" class="frmSection clearfix">';
echo '<h3>Injury Notes</h3>';
echo frmExTextArea($formDB,
    '',
    'Note',
    10,
    'Minor Injury Notes',
    false,
    '');
echo '</div>';

// Form action buttons
echo '<div id="Bump Report Actions" class="frmSection clearfix">';
echo '<h3>Form Action</h3>';
echo frmButtonHelp('Save',
    'save',
    'Click here to Save Changes');
echo frmButtonHelp('Cancel',
    'cancel',
    'Click here to cancel');
echo '</div>';

echo endFormTable();
