<?php
// Configure Calendar
require_once ($rootPath.'/vendor/calendar/calendar.php');
$month = $month;
$year = $year;
$calendar = Calendar::factory($month, $year);
$calendar->standard('today');

// Set-up Calendar events
$today = new DateTime('now');
$endPeriod = $endOfMonth->add(new DateInterval('P1D'));
$interval = DateInterval::createFromDateString('1 day');
$period = new DatePeriod($reportDate, $interval, $endPeriod);
$i = 0;
$bumpDate = getNextBumpDate($bumpDates, $i);
foreach ($period as $calDate) {

    $image = null;
    if (!is_null($bumpDate) && $calDate == $bumpDate) {
        $image = 'edit.png';
        $i = $i + 1;
        $bumpDate = getNextBumpDate($bumpDates, $i);
    }
    else {
        // Do not display link to add new Bump Report if Calendar date is greater than Today's date
        if ($calDate <= $today) {
            $image = 'new.png';
        }
    }

    if (!is_null($image)) {
        $event = $calendar->event()
            ->condition('timestamp', strtotime($calDate->Format('Y-m-d')))
            ->output('<a href="'.$rootUrl.'/modules/accidents/bumpreport.php?reportdate='.$calDate->format('Y-m-d').'"><img src="'.$rootUrl.'/images/'.$image.'" alt="Bump Report" height="23" width="23" /></a>');
        $calendar->attach($event);
    }
}

function getNextBumpDate($bumpDates, $i) {
    $bumpDate = null;
    if ($i <= count($bumpDates) - 1)
        $bumpDate = new DateTime($bumpDates[$i]['ReportDate']);
    return $bumpDate;
}

?>

<table class="calendar">
    <thead>
        <tr class="navigation">
            <th class="prev-month"><a href="<?php echo htmlspecialchars($calendar->prev_month_url()) ?>"><?php echo $calendar->prev_month() ?></a></th>
<th colspan="5" class="current-month"><?php echo $calendar->month() ?> <?php echo $calendar->year ?></th>
<th class="next-month"><a href="<?php echo htmlspecialchars($calendar->next_month_url()) ?>"><?php echo $calendar->next_month() ?></a></th>
</tr>
<tr class="weekdays">
    <?php foreach ($calendar->days() as $day): ?>
        <th><?php echo $day ?></th>
    <?php endforeach ?>
</tr>
</thead>
<tbody>
<?php foreach ($calendar->weeks() as $week): ?>
    <tr>
        <?php foreach ($week as $day): ?>
            <?php
            list($number, $current, $data) = $day;

            $classes = array();
            $output  = '';

            if (is_array($data))
            {
                $classes = $data['classes'];
                $title   = $data['title'];
                $output  = empty($data['output']) ? '' : '<ul class="output"><li>'.implode('</li><li>', $data['output']).'</li></ul>';
            }
            ?>
            <td class="day <?php echo implode(' ', $classes) ?>">
                <span class="date" title="<?php echo implode(' / ', $title) ?>"><?php echo $number ?></span>
                <div class="day-content">
                    <?php echo $output ?>
                </div>
            </td>
        <?php endforeach ?>
    </tr>
<?php endforeach ?>
</tbody>
</table>
