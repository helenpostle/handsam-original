<?php
if ($pageVars->accidentid > 0) {
	$disp_incidentid = "[ID: ".$pageVars->accidentid."]";
} else {
	$disp_incidentid = "";
}

$incident_title = "<h2>";
if( ! ( $formDB['firstname'] == null && $formDB['lastname'] == null )  )
{
	$injured_name = $formDB['firstname'] . " " . $formDB['lastname'] . ", ";
	$incident_title .= $injured_name;
}
if( ! ($formDB['accident_time'] == null ) )
{
	//echo $formDB['accident_time'];
	$acc_date = date_parse_from_format("Y-m-d G:i:s", $formDB['accident_time']);
	$disp_acc_date = sprintf("%02d",$acc_date["day"])."/".sprintf("%02d",$acc_date["month"])."/".$acc_date["year"]." - ".sprintf("%02d",$acc_date["hour"]).":".sprintf("%02d",$acc_date["minute"]);
	$incident_title .= $disp_acc_date;
}
$incident_title .= "</h2>";

$created_by = getRecorderDetails($con, $formDB['created_by']);
$edited_by = getRecorderDetails($con, $formDB['edited_by']);
$audit =  "<div id=\"incident_audit\">";
$audit .= $incident_title;
$audit .= "<h3>Incident entered by: <span>".$created_by['fullname']." [".displayDateTime($formDB['created'])."]</span></h3>";
if ($edited_by['fullname'] != "" )$audit .= "<h3>Incident last edited by: <span>".$edited_by['fullname']." [".displayDateTime($formDB['edited'])."]</span></h3>";
$audit .= "</div>";

echo $audit; 
//echo "<div class=\"clearfix\"></div>";

echo startFormTable($_SERVER["PHP_SELF"],"clearfix" ,"" , "Incident Report $disp_incidentid", "");

echo frmHiddenField($pageVars->accidentid,"accidentid");
echo frmHiddenField($pageVars->accountid,"accountid");
//incident type
echo "<div id=\"personalDetails\" class=\"frmSection clearfix\">";
echo "<h3>Incident Type</h3>";
	echo frmTextAreaNonEditArray($formDB,"riddor","Riddor Reportable", $riddor_arr);
    if($formDB['riddor'] == 'RIDDOR') echo frmTextAreaNonEdit($formDB,"riddor_ref", "HSE incident report number");
	echo frmTextAreaNonEditArray($formDB,"accident_type","Incident Type", $acc_type_arr);
echo "</div>";

//Personal info
echo "<div id=\"personalDetails\" class=\"frmSection clearfix\">";
echo "<h3>Incident Victim Personal Details</h3>";

	echo "<div class=\"col\">";
		echo frmTextAreaNonEdit($formDB,"firstname", "First Name");
		echo frmTextAreaNonEdit($formDB,"lastname","Last Name");
		echo frmTextAreaNonEdit($formDB,"address1","Address 1");
		echo frmTextAreaNonEdit($formDB,"address2","Address 2");
		echo frmTextAreaNonEdit($formDB,"address3","Address 3");
		echo frmTextAreaNonEdit($formDB,"city","City");
	echo "</div>";
	echo "<div class=\"col\">";
		echo frmTextAreaNonEdit($formDB,"postcode","Postcode");
		echo frmTextAreaNonEdit($formDB,"tel1","Telephone 1");
		echo frmTextAreaNonEdit($formDB,"tel2","Telephone 2");
		echo frmTextAreaNonEdit($formDB,"age","Age");
		echo frmTextAreaNonEdit($formDB,"sex","Sex");
		echo frmTextAreaNonEdit($formDB,"status","Status","other");
		if ($formDB['status'] == "OTHER" ) {
			echo frmTextAreaNonEdit($formDB,"status_other","Enter Other");
		}
		if ($formDB['status'] == "STUDENT" ) {
			echo frmTextAreaNonEdit($formDB,"year_text","Year Group");
		}
	echo "</div>";
echo "</div>";

//accident site details
echo "<div id=\"accidentSiteDetails\" class=\"frmSection clearfix\">";
echo "<h3>Incident Site Details</h3>";
echo displayParagraphs("Please describe the details of the site where the incident occurred.", "frm_info");
	echo "<div class=\"col\">";
		echo frmTextAreaNonEdit($formDB,"site_area","Site Area");
		echo frmTextAreaNonEdit($formDB,"site_name","Site Name");
		echo frmTextAreaNonEdit($formDB,"site_address1","Address 1");
		echo frmTextAreaNonEdit($formDB,"site_address2","Address 2");
		echo frmTextAreaNonEdit($formDB,"site_address3","Address 3");
	echo "</div>";		
	echo "<div class=\"col\">";	
		echo frmTextAreaNonEdit($formDB,"site_city","City");
		echo frmTextAreaNonEdit($formDB,"site_postcode","Postcode");
		echo frmTextAreaNonEdit($formDB,"site_tel1","Telephone 1");
		echo frmTextAreaNonEdit($formDB,"site_ref","Site Ref");
	echo "</div>";
echo "</div>";

//accident details
echo "<div id=\"accidentDetails\" class=\"clearfix frmSection\">";
	echo "<h3>Incident Details</h3>";
	echo frmDateTimeNonEdit($formDB,"accident_time","Incident Time &amp Date");
	//echo frmTextAreaNonEdit($formDB,"accident_type","Accident Type");
	if ($formDB['accident_type'] == "ACCIDENT") {
		echo frmTextAreaNonEditArray($formDB,"about_accident","About the Incident", $about_accident_arr);
		if ($formDB['about_accident'] == "fall") {
			echo frmTextAreaNonEdit($formDB,"fall_height","Fall Distance (m)");
		}
	}
	//echo frmTextAreaNonEdit($formDB, "before_accident","Before the Incident", "full_width");
	echo frmTextAreaNonEdit($formDB, "accident_desc","Incident Description", "full_width");
	//echo "<div id=\"riddor_text\">";
		//echo "<h4>RIDDOR INFORMATION</h4><p>Extra RIDOR info here</p>";
	//echo "</div>";
	
	//ambulance details
	if ($formDB['accident_type'] == "ACCIDENT") {
		echo frmTextAreaNonEditArray($formDB,"ambulance","Ambulance Called",$yes_no_arr);
		
		if($formDB['ambulance'] == 1) {
			echo frmTextAreaNonEdit($formDB,"ambulance_time","Ambulance response time (minutes)");
			if ($formDB['ambulance_caller_usertype'] == 2) {
				$amb_fld = 'ambulance_assoc_userid';
				$usr_fld = 'assoc_userid';
				$amb_table = 'usr_assoc';
			} else {
				$amb_fld = 'ambulance_userid';
				$usr_fld = 'userid';
				$amb_table = 'usr';
			}
			$amb_usr_id = 0;
			if ($formDB[$amb_fld] > 0) $amb_usr_id = $formDB[$amb_fld];	
			$amb_qry = "select * from $amb_table where accountid = ".$pageVars->accountid." and $usr_fld = $amb_usr_id order by lastname";
			
			//we can't use a form function here because of we could have a userid or assoc_userid
			$rsAmb = getRS($con,$amb_qry);
			$rowAmb = getRow($rsAmb);
		
			echo "<h4>Ambulance Caller</h4>";
				
				echo "<div class=\"col\">";
					echo frmTextAreaNonEdit($rowAmb,"firstname", "First Name");
					echo frmTextAreaNonEdit($rowAmb,"lastname","Last Name");
					echo frmTextAreaNonEdit($rowAmb,"address1","Address 1");
					echo frmTextAreaNonEdit($rowAmb,"address2","Address 2");
					echo frmTextAreaNonEdit($rowAmb,"address3","Address 3");
					echo frmTextAreaNonEdit($rowAmb,"city","City");
				echo "</div>";
				echo "<div class=\"col\">";
					echo frmTextAreaNonEdit($rowAmb,"postcode","Postcode");
					echo frmTextAreaNonEdit($rowAmb,"tel1","Telephone 1");
					echo frmTextAreaNonEdit($rowAmb,"tel2","Telephone 2");
					echo frmTextAreaNonEdit($rowAmb,"age","Age");
					echo frmTextAreaNonEdit($rowAmb,"sex","Sex");
				echo "</div>";
			
			
				
		} 
	}
echo "</div>";

if ($formDB['accident_type'] == "ACCIDENT") {
	//injury details
	echo "<div id=\"injuryDetails\" class=\"frmSection\">";
		echo "<h3>Injury Details</h3>";
		echo frmTextAreaNonEditArray($formDB,"party_injured","Was the person injured",$yes_no_arr);
		if ($formDB['party_injured'] == 1) {
			echo frmTextAreaNonEditArray($formDB,"injury_Q1","Was the injury?", $injury_Q1_arr);
			echo frmTextAreaNonEditArray($formDB,"injury_Q2","Did the injured person?", $injury_Q2_arr);
		}
		echo "<div class=\"clearfix\" id=\"partyInjured\">";		
			echo displayParagraphs("Please describe the details of the injury sustained ONLY by this person. .",'frm_info');
			for ($i = 0; $i < (count($injuryDB->rows)); $i++) {
				echo "<div class=\"clearfix\">";
				echo "<h4>Injury ".($i + 1)."</h4>";
					echo frmTextAreaNonEdit($injuryDB->rows[$i], "injury_part","What was injured?");
					echo frmTextAreaNonEdit($injuryDB->rows[$i], "injury_type","Type of Injury");
					echo frmTextAreaNonEdit($injuryDB->rows[$i], "injury_desc","Injury Description", "full_width");
				echo "</div>";
			}
		echo "</div>";
	echo "</div>";
}

if ($formDB['accident_type'] == "ACCIDENT") {
	//first aid details
	echo "<div id=\"firstAidDetails\" class=\"frmSection clearfix\">";
		echo "<h3>First Aid Details</h3>";
		echo frmTextAreaNonEditArray($formDB,"first_aid","First Aid Given",$yes_no_arr);
		
		echo "<div id=\"firstAidGiven\">";
			if (count($fAiderDB->rows) > 0 ) echo frmTextAreaNonEdit($formDB, "first_aid_desc","First Aid Description", "full_width");
			if (count($fAiderDB->rows) > 0)	echo displayParagraphs("Please add the details of the first aider(s) who attended this person. .", "frm_info");
	
			for ($i = 0; $i < (count($fAiderDB->rows)); $i++) {
				echo "<div class=\"clearfix\">";
				echo "<h4>First Aider ".($i + 1)."</h4>";
					echo "<div class=\"col\">";
						echo frmTextAreaNonEdit($fAiderDB->rows[$i],  "firstname","First Name");
						echo frmTextAreaNonEdit($fAiderDB->rows[$i],  "lastname","Last Name");
						echo frmTextAreaNonEdit($fAiderDB->rows[$i],  "address1","Address 1");
						echo frmTextAreaNonEdit($fAiderDB->rows[$i],  "address2","Address 2");
						echo frmTextAreaNonEdit($fAiderDB->rows[$i],  "address3","Address 3");
					echo "</div>";
					echo "<div class=\"col\">";
						echo frmTextAreaNonEdit($fAiderDB->rows[$i],  "city","City");
						echo frmTextAreaNonEdit($fAiderDB->rows[$i],  "postcode","Postcode");
						echo frmTextAreaNonEdit($fAiderDB->rows[$i],  "tel1","Telephone 1");
						echo frmTextAreaNonEdit($fAiderDB->rows[$i],  "tel2","Telephone 2");
					echo "</div>";
					echo  frmTextAreaNonEdit($fAiderDB->rows[$i],  "fAcomments","Comments", "full");
						
				echo "</div>";
			}
		echo "</div>";
	
	
	echo "</div>";
}

echo "<div id=\"investigators\" class=\"frmSection clearfix\">";
echo "<h3>Internal Incident Investigator</h3>";	$int_invid = $formDB['int_investigator_userid'];
	if ($int_invid=="") $int_invid=0;
	
	$int_qry = "select userid as a, Concat(lastname, ', ', firstname) as b from usr where userid=$int_invid";
	$int_rs = getRS($con, $int_qry);
	$int_row = getRow($int_rs);
	echo frmTextAreaNonEdit($int_row,"b","Internal investigator");
echo "</div>";

echo "<div class=\"frm_row clearfix \"><a class=\"frm_link\" href=\"$rootUrl/modules/accidents/dashboard_investigation.php?report_id=$report_id&amp;accountid=".$pageVars->accountid."&amp;accidentid=".$pageVars->accidentid."&amp;params=".urlencode($params)."\">View Investigation Report</a></div>";
echo "<div class=\"frm_row clearfix\"><a class=\"frm_link\" href=\"$rootUrl/handsam_core/dashboard_incident_detail.php?report_id=$report_id&amp;accountid=".$pageVars->accountid."&amp;params=".urlencode($params)."\">Return to Dashboard Report</a></div>";

echo endFormTable();

?>