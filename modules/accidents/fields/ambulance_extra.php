<?php
if (isset($formDB)) {
	echo frmExTextField($formDB,"","ambulance_time",6,"Ambulance response time",true, "(In minutes)");
	if ((isset($_POST['ambulance_caller_usertype']) && $_POST['ambulance_caller_usertype'] == 2) || ((!isset($_POST['ambulance_caller_usertype']) && $formDB['ambulance_caller_usertype'] == 2))) {
		$amb_fld = 'ambulance_assoc_userid';
		$usr_fld = 'assoc_userid';
		$amb_table = 'usr_assoc';
	} else {
		$amb_fld = 'ambulance_userid';
		$usr_fld = 'userid';
		$amb_table = 'usr';
	}
	
	if (isset($_POST['ambulance_caller_usertype'])) {
		$amb_val = $pageVars->ambulance_caller_id;
	} else {
		$amb_val = $formDB[$amb_fld];
	}
	
	if ($amb_val == "") $amb_val = 0;
	$amb_qry = "select $usr_fld as a, Concat(firstname, ' ', lastname) as b from $amb_table where accountid = ".$pageVars->accountid." and $usr_fld = $amb_val order by lastname";
	//we can't use a form function here because of we could have a userid or assoc_userid
	$rsAmb = getRS($con,$amb_qry);
	$rowAmb = getRow($rsAmb);
	//$val = frmValue($formDB,$injuryfield);
	$amb_str = "<select name=\"ambulance_caller_id\"  id=\"ambulance_caller_id\">\n";
	if ($amb_val > 0) {
		$amb_str .= "<option selected=\"selected\" value=\"{$rowAmb['a']}\">{$rowAmb['b']}</option>";
	} else {
		$amb_str .= "<option value=\"\">Pick Injured Party</option>";
	}
	$amb_str .= "</select>";
	echo frmRow($amb_str,"Select the ambulance caller","ambulance_caller_id",true, "");
	echo "<div id=\"ambulance_caller\">";

	echo "<input type=\"hidden\" value=\"{$formDB['ambulance_caller_usertype']}\" name=\"ambulance_caller_usertype\" id=\"ambulance_caller_usertype\"/>";
	echo "</div>";
	
} else {
	echo frmExTextFieldNonDb("ambulance_time",6,"Ambulance response time",true, "", "(In minutes)");
}
?>