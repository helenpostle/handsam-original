<?php
/*year group field*/
$year_group_aid = 0;
$year_group_val = "";
$guardian_notified = 0;
$guardian_notified_date = "";

if (isset($formDB)) {
	$year_group_val = $formDB['year_group'];
	$guardian_notified = $formDB['guardian_notified'];
	$guardian_notified_date = strtotime($formDB['guardian_notified_date']);
}
if (isset($_POST['year_group'])) {
	$year_group_val = $_POST['year_group'];
	if (isset($_POST['guardian_notified'])) $guardian_notified = $_POST['guardian_notified'];
	$guardian_notified_date = strtotime($_POST['guardian_notified_date']);
}
//first check if this account has its own custom yeargroup
$qry = "select year_group_id from year_group where accountid={$clsUS->accountid}";
$rs = getRS($con, $qry);
if (getResultRows($rs) > 0) $year_group_aid = $clsUS->accountid;
$qry = "select year_group_id as a, year_text as b from year_group where accountid = $year_group_aid ";

echo "<div class=\"fld_bound\">";
echo frmSelectQueryNonDb("year_group",$qry,$con,"Year Group",true, $year_group_val);

echo frmCheckBoxNonDb($guardian_notified,"guardian_notified","Parent / Carer Notified", false);

echo frmDateFieldNonDb($guardian_notified_date, "guardian_notified_date", "Date Parent / Carer Notified" ,false);
echo "</div>";
?>