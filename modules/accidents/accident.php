<?php

/**
 * accident report editing functionality
 *
 */
####make this a secure page
$secure = true;
require("../../shared/startPage.php");
$pageFiles->addFunc("select_users");

if($accidents_conf['illness_type']) $pageFiles->addModFunc('accident_illness');
if($accidents_conf['restraint_type']) $pageFiles->addModFunc('accident_restraint');
    
$pageFiles->addModFunc("accident_edit");
$pageFiles->addModFunc("accidents_popup");
$pageFiles->addModFunc('file_upload');
$pageFiles->addModFunc('accident_comments');
if ($accidents_conf['site_tags'])
    $pageFiles->addModFunc('accident_tags');

$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->addModFunc('accident_hooks');


$pageFiles->includePhp();
$menuPage = "index.php";

$pageVars = new loadVars($clsUS, $con);
if ($pageVars->hacking)
    headerLocation("$rootUrl/index.php", "");
$qs = array();

$acc_comment = getStrFromRequest("acc_comment");
$acc_id = $pageVars->accidentid; //used to get initial accidentid. - so we can test if it was 0 after an insert (for acc_comments)

if ($pageVars->accidentid > 0) {
	$pageFiles->addJsVar('item_id', $pageVars->accidentid);
}
/* get arrays forauto complete - if these get too big then we need to use ajax instead */

if (isAllowed("recordAccidents")) {
    $box[] = "box.hse_info.php";
    //get filter array querystring
    if (isset($_POST['filter'])) {
        $qs = __unserialize($_POST['filter']);
        $qsfrm = $_POST['filter'];
    } else if (isset($_GET['filter'])) {
        $qsfrm = $_GET['filter'];
        $qs = __unserialize(stripslashes($_GET['filter']));
    }
    //set a default value for use in lotc/box.visit_list_filter.php and page.visit_list.php
    $qs_base = $qs;

    //set first crumbtrail link back to the index page with the list of visits
    $crumb_back = "<a href=\"index.php?accountid={$pageVars->accountid}&amp;rand=" . rand() . "&amp;filter=" . urlencode(serialize($qs)) . "\" title=\"\">Return to Incident Log Reports List</a>";
    
    $title = "Edit Incident Report";
    if ($pageVars->copyid > 0) 
    {
        // make a copy of the form, with personal data removed
        $formDB = getCopy($con, $pageVars);
        $injuryDB = getInjuries($con, $pageVars);
        $fAiderDB = getFirstAiders($con, $pageVars);
    } 
    elseif($pageVars->accidentid !== null)
    {
        $formDB = getAccident($con, $pageVars);
        //get injuries
        $injuryDB = getInjuries($con, $pageVars);
        $fAiderDB = getFirstAiders($con, $pageVars);
    }    
    
    // redirect to accident index if $formDB is not set
    if(!isset($formDB))
    {
        $messages[] = 'There was an error. [accountid not set]';
        headerLocation("$rootUrl/modules/accidents/index.php?filter=" . urlencode(serialize($qs)), $messages);
    }
    //
    if ($formDB['report_state'] == "COMPLETE"
            || ($formDB['report_state'] == "IN PROGRESS" && ($formDB['report_created_by'] != $clsUS->userid))) {
        
        $box[] = "box.file_view.php";
        $content = "testpage.accident_view.php";
        if ($clsUS->userid == $formDB['report_created_by']) {
            $box[] = "box.edit_comments.php";
        } else {
            $box[] = "box.view_comments.php";
        }
        $title = "View Incident Report";

        if (buttonClicked("return")) {
            headerLocation("$rootUrl/modules/accidents/index.php?filter=" . urlencode(serialize($qs)), $messages);
        } else if (buttonClicked("investigation")) {
            headerLocation("$rootUrl/modules/accidents/investigation.php?accidentid=" . $pageVars->accidentid . "&filter=" . urlencode(serialize($qs)), $messages);
        } else if (buttonClicked("return_to_list")) {
            headerLocation("$rootUrl/modules/accidents/index.php?filter=" . urlencode(serialize($qs)), $messages);
            die();
        } else if (buttonClicked("copy")) {
            headerLocation("$rootUrl/modules/accidents/accident.php?accidentid=0&copyid=" . $pageVars->accidentid . "&filter=" . urlencode(serialize($qs)), $messages);
        }
        
    } elseif (
            ($formDB['report_state'] == "IN PROGRESS" || $formDB['report_state'] == "NEW" || $formDB['report_state'] == "")
            && ($formDB['report_created_by'] == $clsUS->userid
            || $formDB['report_created_by'] == "")
    ) {

        $content = "page.accident_edit.php";
        $box[] = "box.file_upload.php";
        $box[] = "box.edit_comments.php";
        
        // save drafts
        if (buttonClicked("save_draft") || isset($_POST['upload_file']) || buttonClicked("investigation")) {
             
            $saveForm = saveDraftForm($con, $pageVars, $clsUS, $formDB);
            $saveInj = saveInjury($con, $pageVars, $clsUS);
            $saveAider = saveFirstAiders($con, $pageVars, $clsUS);
			
            if (isset($acc_comment) && trim($acc_comment) != "" && $acc_id == 0 && $pageVars->accidentid > 0) {
                //save a comment
                $qry = new dbInsert("acc_comments");
                $qry->setReqNumberVal("accidentid",$pageVars->accidentid, "accidentid");
                $qry->setReqStringVal("commenttext",$acc_comment,"comment text");
                $qry->setAudit($clsUS->userid);
                //echo $qry->getSql();
                if ($qry->execute($con)) {
                     $messages[] = "Comment Saved";
                }
            }
            
            $delInj = delInjury($con, $pageVars, $clsUS);
            $delAider = delAider($con, $pageVars, $clsUS);
            if ($saveForm == "" && count($saveInj) == 0 && count($saveAider) == 0 && count($delInj) == 0 && count($delAider) == 0) {
                // hook in the alerts
                
                if($formDB['accidentid'] == null) $formDB['accidentid'] = $pageVars->accidentid;
                if($formDB['accountid'] == null) $formDB['accountid'] = $pageVars->accountid;
                call_hook('accidents', 'accident_save', $formDB);
                
                $messages[] = SAVED;
                if (isset($_POST['upload_file'])) {
                    headerLocation("$rootUrl/modules/accidents/upload_file.php?fileid={$_POST['upload_file']}&accidentid=" . $pageVars->accidentid . "&filter=" . urlencode(serialize($qs)), $messages);
                    die();
                } else if (buttonClicked("investigation")) {
                    headerLocation("$rootUrl/modules/accidents/investigation.php?accidentid=" . $pageVars->accidentid . "&filter=" . urlencode(serialize($qs)), $messages);
                    die();
                } else {
                    headerLocation("$rootUrl/modules/accidents/index.php?filter=" . urlencode(serialize($qs)), $messages);
                }
            } else {
                $messages[] = $saveForm;
                $messages = array_merge($messages, $saveInj, $delInj, $delAider, $saveAider);
            }
        } else if (buttonClicked("save_submit")) { // save as complete		
            
            $saveForm = saveCompleteForm($con, $pageVars, $clsUS, $formDB);
            $saveInj = saveInjury($con, $pageVars, $clsUS);
            $saveAider = saveFirstAiders($con, $pageVars, $clsUS);
            
            if (isset($acc_comment) && trim($acc_comment) != "" && $acc_id == 0 && $pageVars->accidentid > 0) {
                //save a comment
                $qry = new dbInsert("acc_comments");
                $qry->setReqNumberVal("accidentid",$pageVars->accidentid, "accidentid");
                $qry->setReqStringVal("commenttext",$acc_comment,"comment text");
                $qry->setAudit($clsUS->userid);
                //echo $qry->getSql();
                if ($qry->execute($con)) {
                     $messages[] = "Comment Saved";
                }
            }            
            
            $delInj = delInjury($con, $pageVars, $clsUS);
            $delAider = delAider($con, $pageVars, $clsUS);
            
            if ($saveForm == "" && count($saveInj) == 0 && count($saveAider) == 0 && count($delInj) == 0 && count($delAider) == 0) 
            {
            if($formDB['accidentid'] == null) $formDB['accidentid'] = $pageVars->accidentid;
            if($formDB['accountid'] == null) $formDB['accountid'] = $pageVars->accountid;// hook in the alerts
            call_hook('accidents', 'accident_save', $formDB);
			
                $messages[] = SAVED_COMPLETE;
                
                headerLocation("$rootUrl/modules/accidents/index.php?filter=" . urlencode(serialize($qs)), $messages);
            } 
            else 
            {
                $messages[] = $saveForm;
                $messages = array_merge($messages, $saveInj, $delInj, $delAider, $saveAider);
            }
        }else if (buttonClicked("copy")) {
            headerLocation("$rootUrl/modules/accidents/accident.php?accidentid=0&copyid=" . $pageVars->accidentid . "&filter=" . urlencode(serialize($qs)), $messages);
        } else if (buttonClicked("cancel")) {
            headerLocation("$rootUrl/modules/accidents/index.php?filter=" . urlencode(serialize($qs)), $messages);
        }
    }
} else {
    trigger_error("Access Denied", E_USER_WARNING);
    
    headerLocation("$rootUrl/index.php", false);
}


include("../../layout.php");
