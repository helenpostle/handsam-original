<?php
//error_reporting(E_ALL); 
//ini_set("display_errors", 1); 
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
require("../../shared/startPage.php");

$pageFiles->addModFunc('file_upload');

$pageFiles->includePhp();
$pageVars = new loadVars($clsUS,$con);
$to_file_path = $accountIncidentDocs_fp;
if (isAllowed("recordAccidents")) {
	if ($pageVars->fileid !== null) {	
		
		$title = "Incident Book: Upload Document";
		$content = "page.file_edit.php";
				
		//get form state
		$qry = "select a.state as acc_state, i.state as inv_state from acc_accident a left join acc_investigation i on a.accidentid = i.accidentid where a.accidentid = ".$pageVars->accidentid;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		if ($pageDb->row['acc_state'] == 'IN PROGRESS' || $pageDb->row['inv_state'] == 'IN PROGRESS' ) {
			if ($pageDb->row['acc_state'] == 'IN PROGRESS') {
				$page_return = "accident.php?accidentid=".$pageVars->accidentid."&rand=".rand();
			} else if ($pageDb->row['inv_state'] == 'IN PROGRESS') {
				$page_return = "investigation.php?accidentid=".$pageVars->accidentid."&rand=".rand();
			} else {
				$page_return = "index.php";
			}
			//get file data 
			$qry = "select f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by, a.state as acc_state, i.state as inv_state ";
			$qry .= " FROM files f left join acc_files af on f.fileid = af.fileid left join acc_accident a on af.accidentid = a.accidentid left join acc_investigation i on a.accidentid = i.accidentid where f.fileid = {$pageVars->fileid} ";
			
			$pageDb = new pageQry($con, $qry);
			$pageDb->rsItem();
		
	
			if (buttonClicked("save")) {
				$fileid = accUploadFile($clsUS, $pageVars, $con, $to_file_path, "accountIncidentDoc");
				if ($fileid) {
					//first delete this file from lotc_files - no chance of duplicates!
					$qry = new dbDelete("acc_files");
					$qry->setParam("fileid",$fileid);
					$qry->setParam("accidentid",$pageVars->accidentid);
					if (!$qry->execute($con)) {
						//$messages[] = $qry->getSQL();
					}
					//now add the fileid to lotc_files
					$qry = new dbInsert("acc_files");
					$qry->setReqNumberVal("fileid",$fileid,"fileid");
					$qry->setReqNumberVal("accidentid",$pageVars->accidentid,"accident id");
					$qry->setAudit($clsUS->userid);
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					} else {
						$messages[] = SAVED;
						headerLocation($page_return,$messages);
					}
				}
				
			} else if (buttonClicked("cancel")) {
				
				headerLocation($page_return,$messages);
				
			} else if (buttonClicked("delete")) {
				if ($pageVars->fileid != 0) {
					$fileid = accDeleteFile($pageVars, $con, $to_file_path);
					if ($fileid) {
						//first delete this file from lotc_files - no chance of duplicates!
						$qry = new dbDelete("acc_files");
						$qry->setParam("fileid",$fileid);
						$qry->setParam("accidentid",$pageVars->accidentid);
						if (!$qry->execute($con)) {
							$messages[] = $qry->getError();
						} else {
							$messages[] = DELETED;
							headerLocation($page_return,$messages);
						}
					}
	
				}
				
			} 
		} else {
			headerLocation("index.php",$messages);
		}
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}
include("../../layout.php");
?>