<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'accidents';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);



$qry1 = "";
$qry2 = "";
//get aider details
if ($pageVars->witness_usertype == 1) {
	$qry1 = "select * from usr where userid = '".$pageVars->witness_userid. "' and accountid = '".$pageVars->accountid."'";
	$qry2 = "select userid as a , Concat(firstname, ' ', lastname) as b from usr where accountid = '".$pageVars->accountid."' and userid = '".$pageVars->witness_userid."'";
} else if ($pageVars->witness_usertype == 2) {
	$qry1 = "select * from usr_assoc where assoc_userid  = '".$pageVars->witness_userid."'";
	$qry2 = "select assoc_userid as a , Concat(firstname, ' ', lastname) as b from usr_assoc where accountid = '".$pageVars->accountid."' and assoc_userid = '".$pageVars->witness_userid."'";
}

if ($qry1 != "" && $qry2 != "") {
    //get witness select box with witness id selected:
    echo "<h4>Witness ".$pageVars->witness_num."</h4>";

	$formDB2 = new pageQry($con, $qry2);
	$formDB2->rsList();
	
	foreach ($formDB2->rows as $row2)
	{
	    $fakedd = '<div id="witness_userid_'.$pageVars->witness_num.'" class="fakedd witness_userid">';
	    $fakedd .= '<input type="hidden" name="witness_userid[]" value="'.$row2['a'].'" >';
	    $fakedd .= '<span class="text">'.$row2['b'].'</span>';
	    $fakedd .= '<span class="downarrow">&#x25BE;</span>';
	    $fakedd .= '</div>';
	    echo frmRow($fakedd, 'Select a Witness', 'witness_userid_'.$pageVars->witness_num, true);
	    break;
	}
	
	
	$formDB = new pageQry($con, $qry1);
	$formDB->rsItem();
	if ($formDB->cnt > 0) {
		echo "<input type=\"hidden\" value=\"{$pageVars->witness_usertype}\" name=\"witness_usertype[]\" id=\"\"/>";
		echo "<div class=\"col clearfix\">";
			echo frmTextAreaNonEdit($formDB->row,"firstname","First Name");
			echo frmTextAreaNonEdit($formDB->row,"lastname","Last Name");
			echo frmTextAreaNonEdit($formDB->row,"address1","Address 1");
			echo frmTextAreaNonEdit($formDB->row,"address2","Address 2");
			echo frmTextAreaNonEdit($formDB->row,"address3","Address 3");
			echo frmTextAreaNonEdit($formDB->row,"city","City");
		echo "</div>";
		echo "<div class=\"col clearfix\">";
			echo frmTextAreaNonEdit($formDB->row,"postcode","Postcode");
			echo frmTextAreaNonEdit($formDB->row,"tel1","Telephone 1");
			echo frmTextAreaNonEdit($formDB->row,"tel2","Telephone 2");
			echo frmTextAreaNonEdit($formDB->row,"age","Age");
			echo frmTextAreaNonEdit($formDB->row,"sex","Sex");
		echo "</div>";
		echo "<div class=\"frm_row full clearfix optional\"><div class=\"extra_text\"><label class=\"extra_info\" for=\"witness_statement_{$pageVars->witness_num}\">Witness statement</label><p>Please enter report from this witness.</p></div><textarea id=\"witness_statement_{$pageVars->witness_num}\" class=\"\" name=\"witness_statement[]\"></textarea></div>";
		
		//these next 3 elements must be kept in this order!
		echo frmHiddenField($pageVars->witness_id, "witness_id[]");
		//if($pageVars->witness_num > 1)
		echo "<a href=\"#\" style=\"clear:both\" title=\"{$pageVars->witness_userid}\" class=\"deleteItem\">Delete Witness {$pageVars->witness_num}</a>";
		echo frmHiddenField($pageVars->witness_num, "witness_num[]");	
		
	}
}
