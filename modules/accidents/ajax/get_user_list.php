<?php

/**
 * accident report editing functionality
 *
 */
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'accidents';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS, $con);

if($_POST['usertype'] == 1)
{
    $qry = "SELECT userid AS a, CONCAT(firstname, ' ', lastname) AS b FROM usr WHERE accountid = " . $pageVars->accountid . " AND userid = " . mysql_real_escape_string($_POST['id']) . " ORDER BY lastname";
}
else if($_POST['usertype'] == 2)
{
    $qry = "SELECT assoc_userid AS a, Concat(firstname, ' ', lastname) AS b FROM usr_assoc WHERE accountid = " . $pageVars->accountid . " AND assoc_userid = " . mysql_real_escape_string($_POST['id']) . " ORDER BY lastname;";
}

if(isset($qry))
{
    $formDB = new pageQry($con, $qry);
    $formDB->rsList();

    foreach($formDB->rows as $row)
    {
        echo json_encode(array('usertype' => $_POST['usertype'], 'id' => $row['a'], 'name' => $row['b']));
    }
}
