<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'accidents';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);
$qry = "";
if ($pageVars->injured_usertype == 1) {
	$qry = "select userid as a, Concat(firstname, ' ', lastname) as b from usr where accountid = ".$pageVars->accountid." and userid = ".$pageVars->injuredid." order by lastname";
} else if ($pageVars->injured_usertype == 2) {
	$qry = "select assoc_userid as a, Concat(firstname, ' ', lastname) as b from usr_assoc where accountid = ".$pageVars->accountid." and assoc_userid = ".$pageVars->injuredid." order by lastname";
}

//$qry = "select injuredid as a, Concat(firstname, ' ', lastname) as b from acc_injured_party where accountid = ".$pageVars->accountid." and order by lastname";
if($qry != "")
{
    $formDB = new pageQry($con, $qry);
    $formDB->rsList();

    foreach ($formDB->rows as $row) {
        echo json_encode(array('id' => $row['a'], 'name' => $row['b']));
    }
}
