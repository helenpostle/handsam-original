<?php

/**
 * Receive Ajax requests from the alert config page. 
 */

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'accidents';
require("../../../shared/startPage.php");

$pageFiles->addModFunc('alerts');
$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

// some ajax requests will be made for users created within the page and not yet added to the db.
// if so, then no user or alert id will be passed to this page.
// db deletion are only exectued if this parameter is passed.

if(isAjax())
{
    $action = getStrFromRequest('action');
    
    switch($action)
    {
        case "delete-alert":
            $alert_id = getIntFromRequest('alert_id');
            if($alert_id) deleteAlert($alert_id);
            
            break;
        case "delete-user":
            $user_id = getIntFromRequest('user_id');
            if($user_id) deleteAlertUser($user_id);
            
            break;
        case "get-ac-user":
            $usr_id = getIntFromRequest('usr_id');
            echo getAccountUser($usr_id);
            break;
        case "get-jobrole":
            $role_id = getIntFromRequest("jr_id");
            $acc_id = getIntFromRequest("acc_id");
            echo getJobrole($role_id, $acc_id);
            break;
        case "delete-jobrole":
            $alertjr_id = getIntFromRequest("jr_id");
            if($alertjr_id) deleteJobrole($alertjr_id);
            break;
        case "get-am-user":
            $am_id = getIntFromRequest('am_id');
			$acc_id = getIntFromRequest('acc_id');
            echo getAccountManager($am_id, $acc_id);
            break;
        case "delete-am":
            $alert_am_id = getIntFromRequest("am_rem_id");
            if($alert_am_id) deleteAlertAccountManager($alert_am_id);
            break;
    }
}

/**
 * Detect an Ajax request
 * @return bool true if AJAX
 */
function isAjax()
{
    return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest');
}