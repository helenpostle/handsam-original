<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'accidents';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

//get aider select box with aiderid selected:
echo "<h4>First Aider ".$pageVars->aider_num."</h4>";

$qry1 = "";
$qry2 = "";

//get aider details
if ($pageVars->aider_usertype == 1) {
	$qry1 = "select * from usr where userid = ".$pageVars->aider_userid. " and accountid = ".$pageVars->accountid;
	$qry2 = "select userid as a , Concat(firstname, ' ', lastname) as b from usr where accountid = ".$pageVars->accountid." and userid = ".$pageVars->aider_userid;
} else if ($pageVars->aider_usertype == 2) {
	$qry1 = "select * from usr_assoc where assoc_userid  = ".$pageVars->aider_userid;
	$qry2 = "select assoc_userid as a , CONCAT(firstname, ' ', lastname) as b from usr_assoc where accountid = ".$pageVars->accountid." and assoc_userid = ".$pageVars->aider_userid;
}

if ($qry1 != "" && $qry2 != "") {
	$formDB2 = new pageQry($con, $qry2);
	$formDB2->rsList();
	//echo "<div class=\"frm_row clearfix \"><label for=\"aider_userid_{$pageVars->aider_num}\">Select a first aider</label><select class=\"aider_userid\" name=\"aider_userid[]\" id=\"aider_userid_{$pageVars->aider_num}\">";
	foreach ($formDB2->rows as $row2) {
	    //$fakedd = '<label for="aider_userid_">Select a First Aider<label>';
	    $fakedd = '<div id="aider_userid_'.$pageVars->aider_num.'" class="fakedd">';
	    $fakedd .= '<input type="hidden" name="aider_userid[]" value="'.$row2['a'].'" >';
	    $fakedd .= '<span class="text">'.$row2['b'].'</span>';
	    $fakedd .= '<span class="downarrow">&#x25BE;</span>';
	    $fakedd .= '</div>';
	    echo frmRow($fakedd, "Select a first aider", "aider_userid_".$pageVars->aider_num, true, "full");
	    break;
	    
		echo "<option value=\"{$row2['a']}\"";
		if ($row2['a'] ==  $pageVars->aider_userid) {
			echo " selected ";
		}
		echo ">{$row2['b']}</option>";
	}
	//echo "</select></div>";
	
	
	$formDB = new pageQry($con, $qry1);
	$formDB->rsItem();
	if ($formDB->cnt > 0) {
		echo "<input type=\"hidden\" value=\"{$pageVars->aider_usertype}\" name=\"aider_usertype[]\" id=\"\"/>";
		echo "<div class=\"col clearfix\">";
			echo frmTextAreaNonEdit($formDB->row,"firstname","First Name");
			echo frmTextAreaNonEdit($formDB->row,"lastname","Last Name");
			echo frmTextAreaNonEdit($formDB->row,"address1","Address 1");
			echo frmTextAreaNonEdit($formDB->row,"address2","Address 2");
			echo frmTextAreaNonEdit($formDB->row,"address3","Address 3");
			echo frmTextAreaNonEdit($formDB->row,"city","City");
		echo "</div>";
		echo "<div class=\"col clearfix\">";
			echo frmTextAreaNonEdit($formDB->row,"postcode","Postcode");
			echo frmTextAreaNonEdit($formDB->row,"tel1","Telephone 1");
			echo frmTextAreaNonEdit($formDB->row,"tel2","Telephone 2");
			echo frmTextAreaNonEdit($formDB->row,"age","Age");
			echo frmTextAreaNonEdit($formDB->row,"sex","Sex");
		echo "</div>";
		echo "<div class=\"frm_row full clearfix optional\"><div class=\"extra_text\"><label class=\"extra_info\" for=\"fAcomments_{$pageVars->aider_num}\">Comments</label><p>Please enter report from this first aider.</p></div><textarea id=\"fAcomments_{$pageVars->aider_num}\" class=\"\" name=\"fAcomments[]\"></textarea></div>";
		//these next 3 elements must be kept in this order!
		echo frmHiddenField($pageVars->first_aider_id,"first_aider_id[]");
		echo "<a href=\"\" title=\"{$pageVars->first_aider_id}\" class=\"deleteItem\">Delete First Aider $pageVars->aider_num</a>";
		echo frmHiddenField($pageVars->aider_num,"aider_num[]");	
		
	}
}
?>