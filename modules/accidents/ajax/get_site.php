<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'accidents';
require("../../../shared/startPage.php");
if ($accidents_conf['site_tags']) $pageFiles->addModFunc('accident_tags');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$qry = "select * from acc_accident_site where siteid = ".$pageVars->siteid;
$formDB = new pageQry($con, $qry);
$formDB->rsItem();

if ($accidents_conf['site_tags']) {
	$tags = new accTags($pageVars->siteid, false, false);
	$tags->getTags($con);
	$site_tags = implode(',', $tags->item_tags);
}

if ($formDB->cnt > 0) {

	echo "<div class=\"col clearfix\">";
		echo frmTextAreaNonEdit($formDB->row, "site_area","Site Area");
		echo frmTextAreaNonEdit($formDB->row,"site_name","Site Name");
		echo frmTextAreaNonEdit($formDB->row,"site_address1","Address 1");
		echo frmTextAreaNonEdit($formDB->row,"site_address2","Address 2");
		echo frmTextAreaNonEdit($formDB->row,"site_address3","Address 3");
	echo "</div>";		
	echo "<div class=\"col clearfix\">";	
		echo frmTextAreaNonEdit($formDB->row,"site_city","City");
		echo frmTextAreaNonEdit($formDB->row,"site_postcode","Postcode");
		echo frmTextAreaNonEdit($formDB->row,"site_tel1","Tel 1");
		if ($accidents_conf['site_tags']) echo frmRowNoInput("<span  class=\"\">".$site_tags."</span>","Site Tags");
	echo "</div>";
}
?>