<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'accidents';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

$qry = "select siteid as a, Concat(site_name, ' - ', site_area) as b from acc_accident_site where accountid = ".$pageVars->accountid." order by site_name, site_area";
$formDB = new pageQry($con, $qry);
$formDB->rsList();

foreach ($formDB->rows as $row) {
	echo "<option value=\"{$row['a']}\"";
	if ($row['a'] ==  $pageVars->siteid) {
		echo " selected ";
	}
	echo ">{$row['b']}</option>";
}
?>