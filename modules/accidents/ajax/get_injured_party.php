<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'accidents';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

if ($pageVars->injured_usertype == 1) {
	$qry = "select * from usr where userid = ".$pageVars->injuredid;
} else if ($pageVars->injured_usertype == 2) {
	$qry = "select * from usr_assoc where assoc_userid  = ".$pageVars->injuredid;
}

if (isset($qry)) {
	$formDB = new pageQry($con, $qry);
	$formDB->rsItem();
	if ($formDB->cnt > 0) {
		echo "<input type=\"hidden\" value=\"{$pageVars->injured_usertype}\" name=\"injured_usertype\" id=\"injured_usertype\"/>";
		echo "<div class=\"col clearfix\">";
			echo frmTextAreaNonEdit($formDB->row,"firstname","First Name");
			echo frmTextAreaNonEdit($formDB->row,"lastname","Last Name");
			echo frmTextAreaNonEdit($formDB->row,"address1","Address 1");
			echo frmTextAreaNonEdit($formDB->row,"address2","Address 2");
			echo frmTextAreaNonEdit($formDB->row,"address3","Address 3");
			echo frmTextAreaNonEdit($formDB->row,"city","City");
		echo "</div>";
		echo "<div class=\"col clearfix\">";
			echo frmTextAreaNonEdit($formDB->row,"postcode","Postcode");
			echo frmTextAreaNonEdit($formDB->row,"tel1","Telephone 1");
			echo frmTextAreaNonEdit($formDB->row,"tel2","Telephone 2");
			echo frmTextAreaNonEdit($formDB->row,"age","Age");
			echo frmTextAreaNonEdit($formDB->row,"sex","Sex");
		echo "</div>";
	}
}
?>