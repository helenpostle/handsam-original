<?php

/**
* accident report editing functionality
*
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = 'accidents';
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

if ($pageVars->ambulance_caller_usertype == 1) {
	$qry = "select userid as a, Concat(firstname, ' ', lastname) as b from usr where accountid = ".$pageVars->accountid." and userid = ".$pageVars->ambulance_caller_id." order by lastname";
} else if ($pageVars->ambulance_caller_usertype == 2) {
	$qry = "select assoc_userid as a, Concat(firstname, ' ', lastname) as b from usr_assoc where accountid = ".$pageVars->accountid." and assoc_userid = ".$pageVars->ambulance_caller_id." order by lastname";
}

if (isset($qry)) {
	$formDB = new pageQry($con, $qry);
	$formDB->rsList();
	
	foreach ($formDB->rows as $row) {
	    echo json_encode(array('id' => $row['a'], 'name' => $row['b']));
	    continue;
		echo "<option value=\"{$row['a']}\"";
		if ($row['a'] ==  $pageVars->ambulance_caller_id) {
			echo " selected ";
		}
		echo ">{$row['b']}</option>";
		
	}
}
?>