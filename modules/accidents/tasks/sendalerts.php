<?php

/**
 * Send email alerts
 * 
 * For each Accident Investigation that is open, 
 * send an alert to each designated receiver
 * 
 * Mei Gwilym, August 2012
 */
 
error_reporting(E_ALL);
ini_set("display_errors", 1); 
if (($send_email_alerts && $accidents_conf['send_alerts'] == true) || (isset($admin_tools) && $admin_tools)) {
//if ($accidents_conf['send_alerts'] == true && $send_email_alerts) {
	
	include_once("$rootPath/handsam_core/library/clsUser/clsUser.php");
	include_once("$rootPath/handsam_core/library/clsAlerts/clsUserAlert.php");
	include_once("$rootPath/handsam_core/library/clsAlerts/clsAlerts.php");
	include_once("$rootPath/modules/accidents/library/funcSendAlerts.php");
	include_once("$rootPath/modules/accidents/library/funcAlerts.php");
	
	//this is the master array of users. key = Userid 
	$Users = array();
	
	$active_users = array();
	//create active users array - only send alerts to users in this array
	$active_qry = "select userid from usr u left join account a on u.accountid = a.accountid where u.state='ACTIVE' and a.state='ACTIVE'";
	$active_rs = getRS($con, $active_qry);
	While ($active_row = getRow($active_rs)) {
		$active_users[] = $active_row['userid'];
	}
	
	//1. loop through all investigation alerts
	$all_alerts = getAllAlerts("investigation");
	
	// go through each and compare
	foreach($all_alerts as $a) {
		//more user arrays - key = userid
		$global_users = array();
		$account_users = array();
		$jr_users = array();
		$am_users = array();
		
		//invsetigations array - key = accountid
		$investigations = array();
		
		$global_investigations = array();
		
		//get investigations that fullfill this criteria
		$alert_items = getOverdueInvs($a['amount'], $a['unit'], $a['account_id']);
		//var_dump($alert_items);
		$alert_text = "The following Incident Investigations are ".$a['amount']." ".$a['unit']."(s) late: \n";
		
		//for global alert users will be the same for every investigation 
		if ($a['account_id'] ==  0) $global_users = getAlertUsers($a['id']);
		
		//loop through each investigation
		foreach ($alert_items as $itemid => $item) {
			if ($a['account_id'] > 0) {
				if (!array_key_exists($item['accountid'], $account_users)) {
					$account_users[$item['accountid']] = getAlertUsers($a['id'], $item['accountid']);
				}
			}
			if (!array_key_exists($item['accountid'], $jr_users)) {
				$jr_users[$item['accountid']] = getAlertJobRoleUsers($a['id'], $item['accountid']);
			}

			if (!array_key_exists($item['accountid'], $am_users)) {
				$am_users[$item['accountid']] = getAlertAMUsers($a['id'], $item['accountid']);
			}
			
			//if (!array_key_exists($a['account_id'], $investigations)) {
			$investigations[$item['accountid']][$itemid] = $item;
			if ($a['account_id'] ==  0) $global_investigations[$itemid] = $item;
			//}
		}
		
		//now loop through each of the recipients arrays that we have just populated
		
		foreach ($global_users as $k => $v) {
			if (array_key_exists('usr_id', $v)) {
				if (!array_key_exists($v['usr_id'], $Users)) {
					//a new user
					$Users[$v['usr_id']] = new UserAlert($v['usr_id']);
				}
				$Users[$v['usr_id']]->newAlert($a['id'],$alert_text, $global_investigations, "acc_investigation");
			}
		}

		
		foreach ($account_users as $k => $v) {
			//loop through each account
			foreach ($v as $us) {
				if (array_key_exists('usr_id', $us)) {
					
					if (!array_key_exists($us['usr_id'], $Users)) {
						//a new user
						$Users[$us['usr_id']] = new UserAlert($us['usr_id']);
					}
					$Users[$us['usr_id']]->newAlert($a['id'],$alert_text, $investigations[$k], "acc_investigation");
				}
			}
		}
		
		foreach ($jr_users as $k => $v) {
			//loop through each account
			foreach ($v as $us) {
				if (array_key_exists('usr_id', $us)) {
					
					if (!array_key_exists($us['usr_id'], $Users)) {
						//a new user
						$Users[$us['usr_id']] = new UserAlert($us['usr_id']);
					}
					$Users[$us['usr_id']]->newAlert($a['id'],$alert_text, $investigations[$k], "acc_investigation");
				}
			}
		}
		
		foreach ($am_users as $k => $v) {
			//loop through each account
			foreach ($v as $us) {
				if (array_key_exists('usr_id', $us) && $us['usr_id'] > 0) {
					if (!array_key_exists($us['usr_id'], $Users)) {
						//a new user
						$Users[$us['usr_id']] = new UserAlert($us['usr_id']);
					}
					$Users[$us['usr_id']]->newAlert($a['id'],$alert_text, $investigations[$k], "acc_investigation");
				}
			}
		}
		
	}	
	
	//now loop through the Users array and send alerts and log
	$alert_cnt = 0;
	foreach ($Users as $a_user) {
		if (in_array($a_user->userid, $active_users)) {
			$txt = $a_user->getText();
			if ($txt != "") {
                                $txt = getEmailTemplate('alert_log', $txt);
				$alert_cnt ++;
				//there is an alert to send to this user
				if (isset($admin_tools) && $admin_tools && $send_this_alert == false) {
					//make an array of the alerts
					$at_result[] = array('userid' => $a_user->userid, 'email' => $a_user->email, 'from' => $alertsMail, 'message' => $txt); 
				} elseif ($accidents_conf['send_alerts']) {
					//send alert
					if (sendMail($a_user->email, $alertsMail, "Handsam Late Incident Investigation Reports", $txt)) {
						//update alerts_log with this alertid, itemids and userid and success=1;
						$user_alert_arr = $a_user->getAlert();
						foreach ($user_alert_arr as $ak => $av) {		
							$incidentids = implode(',',array_keys($av->items));
							
							foreach (array_keys($av->items) as $itemid) {
								echo logAlert($a_user->userid, $itemid, $ak, "acc_investigation", $con);
							}
						}
						//log email sending
						doLog("INFO","Username: ".$a_user->email." :: userid: ".$a_user->userid." :: alertid: ".$ak." :: incidentids: ".$incidentids);
						
						if (isset($admin_tools) && $admin_tools && $send_this_alert) {
							$at_result[] = array('Status' => 'SENT', 'userid' => $a_user->userid, 'email' => $a_user->email, 'from' => $alertsMail, 'message' => nl2br($txt));
							$messages[] = "All of these alerts have been sent";
						} else {
							echo "<br/>alert sent: alertid - ".$ak." :: userid -  ".$a_user->userid." :: email - ".$a_user->email;
						}
						
					} else {
						foreach ($a_user->getAlert() as $ak => $av) {
							$incidentids = implode(',',array_keys($av->items));
						}
						doLog("INFO","Username: ".$a_user->email." :: userid: ".$a_user->userid." :: alertid: ".$ak." :: incidentids: ".$incidentids." :: EMAIL FAILURE");
						if (isset($admin_tools) && $admin_tools && $send_this_alert) {
							$at_result[] = array('Status' => 'FAILED', 'userid' => $a_user->userid, 'email' => $a_user->email, 'from' => $alertsMail, 'message' => nl2br($txt));
							
						} else {
							echo "<br/><br/>alert email failed: userid -  ".$a_user->userid." :: email - ".$a_user->email." :: message - $txt";
						}
					}
				} else {
					echo "alerts turned off in module config";
				}
			}
		}
		
	}
	//remove any messages from here if there's loads.
	if (count($messages) >=  $alert_cnt && $messages>1) $messages = array();

} else {
	echo "NO send alerts";
}




?>