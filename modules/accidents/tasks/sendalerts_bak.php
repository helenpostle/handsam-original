<?php

/**
 * Send email alerts
 * 
 * For each Accident Investigation that is open, 
 * send an alert to each designated receiver
 * 
 * Mei Gwilym, August 2012
 */
if ($accidents_conf['send_alerts'] == true) {
	
	echo "send alerts";
	error_reporting(E_ALL);
	ini_set("display_errors", 1); 
	
	// should only be run on weekdays?

	// make this a secure page
	$ac_alert_pageFiles = new clsLoadPageFiles($rootPath, $rootUrl, $jQuery, "accidents");

	// user select popup
	$ac_alert_pageFiles->addFunc("select_users");
	// add the extra files for the Alerts
	$ac_alert_pageFiles->addModFunc('alerts');

	$ac_alert_pageFiles->includePhp();

	$minimum_alert_time = 10; // 10 days. should probably be in the site's config.

	// @todo check that this is executed by the server, and not an user.

	$alertType = 'investigation';

	// get all overdue investigations (overdue for at least 10 days)
	$overdueInvs = getOverdueInvs(1);
			echo "<br/><br/>:all inv";
			var_dump ($overdueInvs);
			echo "<br/><br/>";
	// get all alerts configured for this account/install
	$alerts = getAllAlerts($alertType);
			echo "<br/><br/>:all alerts";
			var_dump ($alerts);
			echo "<br/><br/>";
	 
	// go through each and compare
	foreach($alerts as $a)
	{
		foreach($overdueInvs as $inv)
		{
			// check if it's late
			if( isInvLate($inv, $a) )
			{
				sendAlert($a, $inv);
			}
		}
	} 

	// get alert settings 
	// find out how overdue
	// send the alerts
} else {
	echo "NO send alerts";
}