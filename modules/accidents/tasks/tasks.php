<?php

/**
 * Nightly tasks for the accidents module
 * 
 * Called from $rootPath/handsam_core/tasks/tasks.php
 */
// set the module name
$this_mod_dir = 'accidents';

// late investigation email alerts
include("$rootPath/modules/accidents/tasks/sendalerts.php");