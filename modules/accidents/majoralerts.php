<?php

/**
 * Config for Death or Major Injury reports.
 * 
 * These are trigger alerts, and sent when an accident form is saved, where Death 
 * or Major Injury has been selected. 
 */

// make this a secure page
$secure = true;
require("../../shared/startPage.php");

// user select popup
$pageFiles->addFunc("select_alert_users");
// add the extra files for the Alerts
$pageFiles->addModFunc('alerts');

$pageFiles->addCoreLibPhp('clsAccount/clsAccount.php');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS, $con);
if ($pageVars->hacking) headerLocation("$rootUrl/index.php", "");

// alerts config
$alertType = 'major';

if (isAllowed("editAlerts"))
{    
    // get the chosen account id to manage
    $account_id = getIntFromRequest('account_id');
    if(is_null($account_id)) $account_id = 0;

    // save any changes
    if(buttonClicked('Save') && isset($_POST['alert']))
    {
        $save = false;
        
        foreach($_POST['alert'] as $alert)
        {
            // if there are no users/jobroles added to the alert
            // don't save the alert
            if(
                    (isset($alert['user']) && count($alert['user']) > 0) ||
                    (isset($alert['jobrole']) && count($alert['jobrole']) > 0) ||
                    (isset($alert['accountmanager']) && count($alert['accountmanager']) > 0)
            )
            {
                if($alertid = saveAlert($alertType, $account_id, $alert))
                {
                    // save the users (and jobroles)
                    saveAlertUsers($alertid, $alert);
                    $save = true;
                }
            }
        }
        
        if($save) headerLocation('?account_id='.$account_id, array('The alert was saved successfully.'));
    }
    
    // display the riddor alert
    $globalAlerts = getGlobalAlerts($alertType);


    foreach($globalAlerts as &$a)
    {
        $a['users'] = getAlertUsers($a['id'], '0');
        $a['jobroles'] = getAlertJobroles($a['id'], $account_id);
		$a['accountmanager'] = getAlertAccManagers($a['id'], $account_id);
    }	
	
    if($account_id > 0)
    {
        // @todo error check
        $account = new Account($account_id);
        
        $alerts = array();
        $pageFiles->addJsVar('alerts', json_encode($alerts));

        $pageFiles->addJsVar('globalAlerts', json_encode($globalAlerts));
        
        $title = "Death or Major Injury Alerts for ".$account->accountname;
        $content = 'alert.major.php';
        
        $box[] = 'box.major_alert_help.php';
    } else {
	
    //  add on the users and jobroles, by &reference
		if(is_array($globalAlerts) && count($globalAlerts) == 0)
		{
			// first visit to this page
			// create a blank alert and do it again.
			createBlankAlert($alertType);
			$globalAlerts = getGlobalAlerts($alertType);
		}
    
	   
		$pageFiles->addJsVar('alerts', json_encode($globalAlerts));
		
		// tabs for user popup
		$userTabs = array('superadmin', 'jobroles', 'accountmanager'); // 'otheruser' removed
		$pageFiles->addJsVar('userTabs', json_encode($userTabs));
		
		$title = 'Global Death or Major Injury Alert Configuration';
		$content = 'alert.major.php';	
		$accounts = getAccountsAlerts($alertType);
		$box[] = 'box.account_major_alertlist.php';
	}




}
else
{
    trigger_error("Access Denied", E_USER_WARNING);  
    headerLocation("$rootUrl/index.php",false);
}

include("../../layout.php");