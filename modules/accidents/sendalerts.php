<?php

/**
 * Send email alerts
 * 
 * For each Accident Investigation that is open, 
 * send an alert to each designated receiver
 * 
 * Mei Gwilym, August 2012
 */

 error_reporting(E_ALL);
 ini_set("display_errors", 1); 
// should only be run on weekdays?

// make this a secure page
$secure = true;
require("../../shared/startPage.php");

// user select popup
$pageFiles->addFunc("select_users");
// add the extra files for the Alerts
$pageFiles->addModFunc('alerts');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS, $con);
if ($pageVars->hacking) headerLocation("$rootUrl/index.php", "");

$minimum_alert_time = 10; // 10 days. should probably be in the site's config.

// @todo check that this is executed by the server, and not an user.

$alertType = 'investigation';

// get all overdue investigations (overdue for at least 10 days)
$overdueInvs = getOverdueInvs();
// get all alerts configured for this account/install
$alerts = getAlerts($alertType);
		echo "<br/><br/>:all alerts";
        var_dump ($alerts);
		echo "<br/><br/>";
 
// go through each and compare
foreach($alerts as $a)
{
    foreach($overdueInvs as $inv)
    {
        // check if it's late
        if( isInvLate($inv, $a) )
        {
            sendAlert($a, $inv);
        }
    }
} 

// get alert settings 
// find out how overdue
// send the alerts