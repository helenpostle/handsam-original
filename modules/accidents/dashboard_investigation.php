<?php
//error_reporting(E_ALL); 
//ini_set("display_errors", 1); 
/**
* accident report editing functionality
*
*/
####make this a secure page
$secure = true;
require("../../shared/startPage.php");
$pageFiles->addFunc("select_users");

$pageFiles->addModFunc("investigation_edit");
$pageFiles->addModFunc('file_upload');
$pageFiles->addModFunc("accident_edit");
$pageFiles->addModFunc('accident_comments');
$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->includePhp();
$report_id = getStrFromRequest("report_id");

if (isset($_POST['params'])) {
	$params = $_POST['params'];
} else if(isset($_GET['params'])) {
	$params = $_GET['params'];
}

$menuPage = "";

$pageVars = new loadVars($clsUS,$con);
if ($pageVars->hacking) headerLocation("$rootUrl/index.php");
$qs = array();

//some accident comments stuff
$a_comment = getStrFromRequest("comment");
if ($pageVars->accidentid > 0) {
	$pageFiles->addJsVar('item_id', $pageVars->accidentid);
}

if (isAllowed("recordAccidents")) {
	//$box[] = "box.hse_info.php";
	//get filter array querystring

	
	//set first crumbtrail link back to the index page with the list of visits
	//$crumb_back = "<a href=\"index.php?accountid={$pageVars->accountid}&amp;rand=".rand()."&amp;filter=".urlencode(serialize($qs))."\" title=\"\">Return to Incident Log Reports List</a>";
        
	$formDB = getInvestigation($con,  $pageVars);
	$witDB = getWitness($con, $pageVars);
	
	$intInv = getIntInvestigator($con, $pageVars);
        
    $inj_party = '';
    $accident = getAccident($con, $pageVars);
    if(strlen($accident['firstname']) > 0) $inj_party = $accident['firstname'].' '.$accident['lastname'];
        
	$title = "Incident Investigation Report - ".$inj_party;	
	

    $title = "Incident Investigation Report - ".$inj_party;
    $content = "page.dashboard_investigation_view.php";
    $box[] = "box.file_view.php";
    $box[] = "box.view_comments.php";
 

} else {

	trigger_error("Access Denied",E_USER_WARNING);

	headerLocation("$rootUrl/index.php",false);
}


include("../../layout.php");
