<?php

/**
 * Overview of alerts for each school for this install
 * 
 * Lists name of school with number of alerts. 
 */

// make this a secure page
$secure = true;
require("../../shared/startPage.php");

// user select popup
$pageFiles->addFunc("select_alert_users");
// add the extra files for the Alerts
$pageFiles->addModFunc('alerts');

$pageFiles->addCoreLibPhp('clsAccount/clsAccount.php');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS, $con);
if ($pageVars->hacking) headerLocation("$rootUrl/index.php", "");

// alerts config
$alertType = 'investigation';
$alertMinDays = $accidents_conf['investigation_late_min']; // the minimum time after which an alert can be sent

if (isAllowed("editAlerts"))
{    
    // get the chosen account id to manage
    $account_id = getIntFromRequest('account_id');
    if(is_null($account_id)) $account_id = 0;
    $alertusers = array();
        
    // save the form
    if(buttonClicked('Save') && isset($_POST['alert']))
    {
        $save = false;
        // save the form details
        // loop through each row and save as a single record (and user details)
        foreach($_POST['alert'] as $alert)
        {
            // if there are no users/jobroles added to the alert
            // don't save the alert
            if(
                    (isset($alert['user']) && count($alert['user']) > 0) ||
                    (isset($alert['jobrole']) && count($alert['jobrole']) > 0) ||
                    (isset($alert['accountmanager']) && count($alert['accountmanager']) > 0)
            )
            {
                if($alertid = saveAlert($alertType, $account_id, $alert))
                {
                    // save the users (and jobroles)
                    saveAlertUsers($alertid, $alert);
                    $save = true;
                }
            }
        }
        if($save) headerLocation('?account_id='.$account_id, array('The alert was saved successfully.'));
    }
    
    // global alerts
    $globalAlerts = getGlobalAlerts($alertType);    
    //  add on the users and jobroles, by &reference
    foreach($globalAlerts as &$a)
    {
        $a['users'] = getAlertUsers($a['id'], '0');
        $a['jobroles'] = getAlertJobroles($a['id'], $account_id);
		$a['accountmanager'] = getAlertAccManagers($a['id'], $account_id);
    }
    
    // show for a particular account
    if($account_id != 0)
    {
        // @todo error check
        $account = new Account($account_id);
        
        // tabs for user popup
        //$userTabs = array('users', 'jobroles', 'otheruser');
        $userTabs = array('superadmin', 'users', 'jobroles', 'accountmanager');
        // get all alerts for this account.
        $alerts = getAlerts($account_id, $alertType);        
        //  add on the users and jobroles, by &reference
        foreach($alerts as &$a)
        {
            $a['users'] = getAlertUsers($a['id'], $account_id);
            $a['jobroles'] = getAlertJobroles($a['id'], $account_id);
			$a['accountmanager'] = getAlertAccManagers($a['id'], $account_id);
        }
        
        $pageFiles->addJsVar('alerts', json_encode($alerts));
        $pageFiles->addJsVar('globalAlerts', json_encode($globalAlerts));
        
        $title = "Investigation Email Alerts for ".$account->accountname;
        $content = 'alert.investigation.php';
        
        $box[] = 'box.investigation_alert_help.php';
    }
    else // show the global alert config
    {
        $title = 'Global Investigation Email Alert Configuration';
        
        // get all schools for this account
        // show in a table
        // clickable links for each school    
        $accounts = getAccountsAlerts($alertType);
        $box[] = 'box.accountalertlist.php';
        
        $content = 'globalalert.investigation.php';        
        
        $pageFiles->addJsVar('alerts', json_encode($globalAlerts));
        $pageFiles->addJsVar('globalAlerts', json_encode(array())); // empty array to stop JS errors
        
        // tabs for user popup
        $userTabs = array('superadmin', 'jobroles', 'accountmanager'); // 'otheruser' removed
    }
    
    // tabs config
    $pageFiles->addJsVar('userTabs', json_encode($userTabs));
    // provide the account id for the JS
    $pageFiles->addJsVar('account_id', $account_id);
    
    $pageFiles->addJsVar('alertMinDays', $alertMinDays);
}
else
{
    trigger_error("Access Denied", E_USER_WARNING);  
    headerLocation("$rootUrl/index.php",false);
}

include("../../layout.php");