<?php 
 /**
  * This is the basis of the reporting system.
  * Related files are:
  *			/content/reportparameters.php
  *			/content/reportlist.php
  *			/content/reportview.php
  * To add a new report, add to the 'case' statement below, 
  * and add a link in content/reportlist.php 
  */
$secure = true;

require("../../shared/startPage.php");
$pageFiles->addFunc('reporting');
$pageFiles->includePhp();
$menuPage = "index.php";
$accountid = $clsUS->accountid;
if (isAllowed("createReport")) {
	$title = "Reporting Tool";
	$report = "";
	if (isset($_GET["report"])) $report = @$reports_arr[@$_GET["report"]];
	$content = "reportlist.php";
	$sql = "";
	$csv = isset($_GET["csv"]);
	$run = isset($_GET["run"]);
	$print = isset($_GET["print"]);
	$link = "";
	$link2 = "";
	$arrayParams = array();
	
	function getParam($name,$default) {
		if (getDateFromRequest($name) != null) {
			return getDateFromRequest($name);
		} else if (isset($_GET[$name])) {
			return getStrFromRequest($name);
		} else {
			return $default;
		}	
	}
	
	
	/**
	 * Settings for each report.
	 * The SQL fieldnames are used as the report headings
	 * The output is created automatically as html or CSV
	 * Parameters must be added to reportparameters.php if they are not already there
	 * The first field is ignored, unless $url is set, 
	 * in which case it is appended to $url and set as a link on the first column.
	 */
	 if ($report != "") {
	 	@include ("reports/".$report.".php");
 	}
	
	//if we are running a report, get the rs and set the content.
	if ($sql != "") {
		if ($run) {
			$rsReport = getRS($con,$sql);
			if (!$print) $messages[] = getResultRows($rsReport)." records found.";
			//$js[] = "js/sortReportTable.php";
			$content = "reportview.php";
			
		} else {
			$messages[] = $advice;
			$content = "reportparameters.php";	
		}	
	}
	
	if ($report != "" & !$print) $box[]="report_options.php"; 
} else {
	#trying to do something we shouldn't! throw error so it is logged.	
	trigger_error("Access Denied",E_USER_WARNING);
}	
//do a csv or a page
if ($csv) {
	$filename = str_replace (" ", "", strip_tags($title));
	downloadAsCSV($rsReport,$filename);
} else if ($print) {
	include("printLayout.php");
} else {
 	include("layout.php");
}
?>