<?php
	function getInvestigation($con, $pageVars) {
		
        global $accidents_conf;
		
		$qry = "select acc_investigation.*,  acc_investigation.state as report_state, acc_accident.created_by as report_created_by, acc_accident.accountid as acc_accountid, acc_accident.accident_time, "; 
		$qry .= "CASE WHEN acc_investigation.ext_investigator_usertype=2 THEN '' ELSE u.username END AS username, ";
		$qry .= "CASE WHEN acc_investigation.ext_investigator_usertype=2 THEN a.firstname ELSE u.firstname END AS firstname, ";
		$qry .= "CASE WHEN acc_investigation.ext_investigator_usertype=2 THEN a.lastname ELSE u.lastname END AS lastname, ";
		$qry .= "CASE WHEN acc_investigation.ext_investigator_usertype=2 THEN a.email ELSE u.email END AS email ";
		$qry .= " from acc_accident left join acc_investigation on acc_investigation.accidentid = acc_accident.accidentid";
		$qry .= " left join usr u on acc_investigation.ext_investigator_userid = u.userid ";
		$qry .= " left join usr_assoc a on acc_investigation.ext_investigator_assoc_userid = a.assoc_userid ";
		
		$qry .= " where acc_accident.accidentid = ".$pageVars->accidentid; 
		if ($accidents_conf['investigation_optional'] == true) {
            $qry .= " and acc_accident.investigation_required = 1 "; 
        }
		if (!isAllowed("editAccount")) $qry .= " and acc_accident.accountid = ".$pageVars->accountid;
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		return $pageDb->row;
	}

	function getIntInvestigator($con, $pageVars) {
		$qry = "select u.* from usr u left join acc_accident a on u.userid = a.int_investigator_userid where a.accidentid = ".$pageVars->accidentid;
		$Db = new pageQry($con, $qry);
		$Db->rsItem();
		return $Db->row;
	}
?>