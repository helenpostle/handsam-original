<?php

/* functions for saving  accidents in accident book */

//function to save a draft form with few required fields
function saveDraftForm($con, $pageVars, $clsUS, $formDB) {
    global $pageVars, $accidents_conf;
    if ($pageVars->accidentid == 0) {
        $qry = new dbInsert("acc_accident");
        $qry->setReqNumberVal("accountid", $pageVars->accountid, "Accountid");
    } else {
        $qry = new dbUpdate("acc_accident");
        $qry->setParam("accidentid", $pageVars->accidentid);
    }

    if ($pageVars->injuredid == 0)
        $pageVars->injuredid = "";
    if ($pageVars->injured_usertype == 1) {
        $qry->setReqNumberVal("userid", $pageVars->injuredid, "Injured Party", "Please enter the Injured Party");
        $qry->setNullVal("assoc_userid");
    } else if ($pageVars->injured_usertype == 2) {
        $qry->setReqNumberVal("assoc_userid", $pageVars->injuredid, "Injured Party", "Please enter the Injured Party");
        $qry->setNullVal("userid");
    } else {
        $qry->setReqNumberVal("userid", $pageVars->injuredid, "Injured Party", "Please enter the Injured Party");
    }
    $qry->setReqNumberVal("injured_usertype", $pageVars->injured_usertype, "Injured user type");


    $qry->setStringVal("status", $_POST["status"], "Status");
    if (isset($_POST["status_other"])) {
        $qry->setReqStringVal("status_other", $_POST["status_other"], "Status - Other");
    } else {
        $qry->setNullVal("status_other");
    }

    //year group, guardian notification if status == student
    if (isset($_POST["year_group"])) {
        $qry->setNumberVal("year_group", $_POST["year_group"], "Year Group");
    } else {
        $qry->setNullVal("year_group");
    }

    //year group, guardian notification if status == student
    if (isset($_POST["guardian_notified"])) {
        $qry->setReqBooleanVal("guardian_notified", $_POST["guardian_notified"], "Parent/carer notified");
    } else {
        $qry->setNullVal("guardian_notified");
    }

    //year group, guardian notification if status == student
    if (isset($_POST["guardian_notified_date"])) {
        $qry->setDateVal("guardian_notified_date", $_POST["guardian_notified_date"], "Parent/carer date notified");
    } else {
        $qry->setNullVal("guardian_notified_date");
    }


    //site details
    if ($pageVars->siteid == 0) {
        $qry->setNullVal("siteid");
    } else {
        $qry->setNumberVal("siteid", $pageVars->siteid, "Site Details");
    }

    //accident details
    $acc_date = $_POST['accident_time'];
    $hrs = $_POST["accident_time_hrs"];
    $hrs = sprintf("%02d", $hrs);
    $mins = $_POST["accident_time_mins"];
    $mins = sprintf("%02d", $mins);
    $acc_date .= " {$hrs}:{$mins}:00";

    $qry->setReqDateTimeVal("accident_time", $acc_date, "Accident Time and Date");

    if (isset($_POST["accident_desc"])) {
        $qry->setStringVal("accident_desc", $_POST["accident_desc"], "Accident Description");
    } else {
        $qry->setNullVal("accident_desc");
    }

    if (isset($_POST["riddor"])) {
        $qry->setStringVal("riddor", $_POST["riddor"], "Riddor");
    } else {
        $qry->setNullVal("riddor");
    }

    if (isset($_POST["riddor_ref"])) {
        $qry->setStringVal("riddor_ref", $_POST["riddor_ref"], "Riddor Ref");
    } else {
        $qry->setNullVal("riddor_ref");
    }


    //accident type specific 
    if (isset($_POST["accident_type"])) {
        switch ($_POST["accident_type"]) {
            case "ACCIDENT":
                $qry->setReqStringVal("accident_type", $_POST["accident_type"], "Accident Type");
                $qry = save_draft_accident($qry, $pageVars);
                break;
            case "NEAR MISS":
                $qry->setReqStringVal("accident_type", $_POST["accident_type"], "Accident Type");
                $qry = save_draft_nearmiss($qry);
                break;
            case "ILLNESS":
                $qry->setReqStringVal("accident_type", $_POST["accident_type"], "Accident Type");
                $qry = save_draft_illness($qry);
                break;
            case "RESTRAINT":
                $qry->setReqStringVal("accident_type", $_POST["accident_type"], "Accident Type");
                $qry = save_draft_restraint($qry);
                break;
            default:
                $qry->setStringVal("accident_type", "", "Accident Type");
        }
    } else {
        $qry->setNullVal("accident_type");
    }

    //only save internal investigator if the investigation report is still in progress
    $inv_qry = "select state from acc_investigation where accidentid = " . $pageVars->accidentid;
    $inv_rs = getRS($con, $inv_qry);
    $inv_row = getRow($inv_rs);
    if ($inv_row['state'] != 'COMPLETE') {
        if ($accidents_conf['investigation_optional'] == true) {
            $qry->setNumberVal("investigation_required", $_POST['investigation_required'], "Investigation required");
        }
        if ($accidents_conf['investigation_optional'] == true && isset($_POST['investigation_required']) && $_POST['investigation_required'] == 0 && $formDB['inv_state'] == null) {
            $qry->setNullVal("int_investigator_userid");
        } else if (isset($_POST["int_investigator_userid"]) && $_POST["int_investigator_userid"] > 0) {
            $qry->setNumberVal("int_investigator_userid", $_POST['int_investigator_userid'], "Internal Investigator");
        } else if($formDB['inv_state'] == null) {
            $qry->setNullVal("int_investigator_userid");
        }
    }

    $qry->setReqStringVal("state", "IN PROGRESS", "State");
    $qry->setAudit($clsUS->userid);
    if ($qry->execute($con)) {
        if ($pageVars->accidentid == 0) {
            $pageVars->accidentid = $qry->getNewID();
        }
        return "";
    } else {
        return $qry->getError();
    }
}

//complete near miss
function save_draft_nearmiss($qry) {
    $qry->setNullVal("first_aid");
    $qry->setNullVal("injury_Q1");
    $qry->setNullVal("injury_Q2");
    $qry->setNullVal("first_aid_desc");
    $qry->setNullVal("ambulance");
    $qry->setNullVal("ambulance_userid");
    $qry->setNullVal("ambulance_caller_usertype");
    $qry->setNullVal("ambulance_time");
    $qry->setNullVal("ambulance_assoc_userid");
    $qry->setNullVal("party_injured");
    $qry->setNullVal("about_accident");
    $qry->setNullVal("fall_height");
    return $qry;
}

//complete accident
function save_draft_accident($qry, $pageVars) {
    if (isset($_POST["about_accident"])) {
        $qry->setStringVal("about_accident", $_POST["about_accident"], "About the accident");
    }

    if (isset($_POST["fall_height"])) {
        $qry->setNumberVal("fall_height", $_POST["fall_height"], "Fall Height");
    } else {
        $qry->setNullVal("fall_height");
    }

    //ambulance related fields
    if (isset($_POST["ambulance"])) {
        $qry->setNumberVal("ambulance", $_POST["ambulance"], "Ambulance");
    } else {
        $qry->setNullVal("ambulance");
    }

    if ($pageVars->ambulance_caller_usertype == 1) {
        $qry->setReqNumberVal("ambulance_userid", $pageVars->ambulance_caller_id, "Ambulance caller", "Please enter the ambulance caller");
        $qry->setNullVal("ambulance_assoc_userid");
    } else if ($pageVars->ambulance_caller_usertype == 2) {
        $qry->setReqNumberVal("ambulance_assoc_userid", $pageVars->ambulance_caller_id, "Ambulance caller", "Please enter the ambulance caller");
        $qry->setNullVal("ambulance_userid");
    } else {
        $qry->setNumberVal("ambulance_userid", $pageVars->ambulance_caller_id, "Ambulance caller", "Please enter the ambulance caller");
    }

    if (isset($_POST["ambulance_caller_usertype"])) {
        $qry->setNumberVal("ambulance_caller_usertype", $_POST["ambulance_caller_usertype"], "ambulance caller usertype");
    } else {
        $qry->setNullVal("ambulance_caller_usertype");
    }

    if (isset($_POST["ambulance_time"])) {
        $qry->setNumberVal("ambulance_time", $_POST["ambulance_time"], "Ambulance time", "Please enter the number value in minutes");
    } else {
        $qry->setNullVal("ambulance_time");
    }

    //injury
    if (isset($_POST["party_injured"])) {
        $qry->setReqNumberVal("party_injured", $_POST["party_injured"], "Person Injured");
    } else {
        $qry->setNullVal("party_injured");
    }
    if (isset($_POST["injury_Q1"])) {
        $qry->setStringVal("injury_Q1", $_POST["injury_Q1"], "Was the injury");
    } else {
        $qry->setNullVal("injury_Q1");
    }
    if (isset($_POST["injury_Q2"])) {
        $qry->setStringVal("injury_Q2", $_POST["injury_Q2"], "Did the injured person");
    } else {
        $qry->setNullVal("injury_Q2");
    }

    //First Aid Details		
    if (isset($_POST["first_aid"])) {
        $qry->setStringVal("first_aid", $_POST["first_aid"], "First Aid Given");
    } else {
        $qry->setNullVal("first_aid");
    }
    if (isset($_POST["first_aid_desc"])) {
        $qry->setStringVal("first_aid_desc", $_POST["first_aid_desc"], "First Aid Description");
    } else {
        $qry->setNullVal("first_aid_desc");
    }
    return $qry;
}

//complete illness
function save_draft_illness($qry) {
    if (isset($_POST['paracetamol']))
        $qry->setBooleanVal('paracetamol', $_POST['paracetamol'], 'Paracetamol Administered');
    else
        $qry->setNullVal('paracetamol');

    if (isset($_POST['sent_home']))
        $qry->setBooleanVal('sent_home', $_POST['sent_home'], 'Sent Home');
    else
        $qry->setNullVal('sent_home');

    if (isset($_POST['sent_home_contact']))
        $qry->setStringVal('sent_home_contact', $_POST['sent_home_contact'], 'Sent Home Contact');
    else
        $qry->setNullVal('sent_home_contact');

    if (isset($_POST['sent_home_userid']))
        $qry->setNumberVal('sent_home_userid', $_POST['sent_home_userid'], 'Sent Home User');
    else
        $qry->setNullVal('sent_home_userid');

    if (isset($_POST['sent_home_associd']))
        $qry->setNumberVal('sent_home_associd', $_POST['sent_home_associd'], 'Sent Home Associated User');
    else
        $qry->setNullVal('sent_home_associd');

    $qry->setNullVal("first_aid");
    $qry->setNullVal("injury_Q1");
    $qry->setNullVal("injury_Q2");
    $qry->setNullVal("first_aid_desc");
    $qry->setNullVal("ambulance");
    $qry->setNullVal("ambulance_userid");
    $qry->setNullVal("ambulance_caller_usertype");
    $qry->setNullVal("ambulance_time");
    $qry->setNullVal("ambulance_assoc_userid");
    $qry->setNullVal("party_injured");
    $qry->setNullVal("about_accident");
    $qry->setNullVal("fall_height");
    return $qry;
}

function save_draft_restraint($qry) {
    if (isset($_POST['restraint_userid']))
        $qry->setStringVal('restraint_userid', $_POST['restraint_userid'], 'Restraint Registered User');
    else
        $qry->setNullVal('restraint_userid');

    if (isset($_POST['restraint_associd']))
        $qry->setStringVal('restraint_associd', $_POST['restraint_associd'], 'Restraint Associated User');
    else
        $qry->setNullVal('restraint_associd');
    // null values
    $qry->setNullVal("first_aid");
    $qry->setNullVal("injury_Q1");
    $qry->setNullVal("injury_Q2");
    $qry->setNullVal("first_aid_desc");
    $qry->setNullVal("ambulance");
    $qry->setNullVal("ambulance_userid");
    $qry->setNullVal("ambulance_caller_usertype");
    $qry->setNullVal("ambulance_time");
    $qry->setNullVal("ambulance_assoc_userid");
    $qry->setNullVal("party_injured");
    $qry->setNullVal("about_accident");
    $qry->setNullVal("fall_height");
    return $qry;
}
?>		
