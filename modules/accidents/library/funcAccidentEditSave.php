<?php
/*functions for saving  accidents in accident book*/


	
	//function to save a draft form with few required fields
	function saveDraftForm($con, $pageVars, $clsUS) 
        {
		GLOBAL $pageVars;
		if ($pageVars->accidentid == 0) {
			$qry = new dbInsert("acc_accident");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
		} else {
			$qry = new dbUpdate("acc_accident");
			$qry->setParam("accidentid", $pageVars->accidentid);
		}		

		if ($pageVars->injuredid == 0) $pageVars->injuredid = "";
		if ($pageVars->injured_usertype == 1) {
			$qry->setReqNumberVal("userid",$pageVars->injuredid,"Injured Party", "Please enter the Injured Party");
			$qry->setNullVal("assoc_userid");
		} else if($pageVars->injured_usertype == 2) {
			$qry->setReqNumberVal("assoc_userid",$pageVars->injuredid,"Injured Party", "Please enter the Injured Party");
			$qry->setNullVal("userid");
		} else {
			$qry->setReqNumberVal("userid",$pageVars->injuredid,"Injured Party", "Please enter the Injured Party");
		}
		$qry->setReqNumberVal("injured_usertype",$pageVars->injured_usertype,"Injured user type");
		
		
		$qry->setStringVal("status",$_POST["status"],"Status");
		if (isset($_POST["status_other"])) {
			$qry->setReqStringVal("status_other",$_POST["status_other"],"Status - Other");
		} else {
			$qry->setNullVal("status_other");
		}
		
		//year group, guardian notification if status == student
		if (isset($_POST["year_group"])) {
			$qry->setNumberVal("year_group",$_POST["year_group"],"Year Group");
		} else {
			$qry->setNullVal("year_group");
		}

		//year group, guardian notification if status == student
		if (isset($_POST["guardian_notified"])) {
			$qry->setReqBooleanVal("guardian_notified",$_POST["guardian_notified"],"Parent/carer notified");
			} else {
			$qry->setNullVal("guardian_notified");
		}

		//year group, guardian notification if status == student
		if (isset($_POST["guardian_notified_date"])) {
			$qry->setDateVal("guardian_notified_date",$_POST["guardian_notified_date"],"Parent/carer date notified");
		} else {
			$qry->setNullVal("guardian_notified_date");
		}

		
		//site details
		if ($pageVars->siteid == 0) {
			$qry->setNullVal("siteid");
		} else {
			$qry->setNumberVal("siteid",$pageVars->siteid,"Site Details");
		}

		//accident details
		$acc_date = $_POST['accident_time'];
		$hrs = $_POST["accident_time_hrs"];
		$hrs = sprintf("%02d", $hrs);
		$mins = $_POST["accident_time_mins"];
		$mins = sprintf("%02d", $mins);
		$acc_date .= " {$hrs}:{$mins}:00";

		$qry->setReqDateTimeVal("accident_time",$acc_date,"Accident Time and Date");
		
		if (isset($_POST["accident_type"])) {
			$qry->setStringVal("accident_type",$_POST["accident_type"],"Accident Type");
		} else {
			$qry->setNullVal("accident_type");
		}
		
		if (isset($_POST["about_accident"])) {
			$qry->setStringVal("about_accident",$_POST["about_accident"],"About the accident");
		}
		
		if (isset($_POST["fall_height"])) {
			$qry->setNumberVal("fall_height",$_POST["fall_height"],"Fall Height");
		} else {
			$qry->setNullVal("fall_height");
		}
		

		if (isset($_POST["accident_desc"]))
			$qry->setStringVal("accident_desc", $_POST["accident_desc"],"Accident Description");
		else
			$qry->setNullVal("accident_desc");
		
		if (isset($_POST["riddor"])) {
			$qry->setStringVal("riddor",$_POST["riddor"],"Riddor");
		} else {
			$qry->setNullVal("riddor");
		}
		
		if (isset($_POST["riddor_ref"])) {
			$qry->setStringVal("riddor_ref",$_POST["riddor_ref"],"Riddor Ref");
		} else {
			$qry->setNullVal("riddor_ref");
		}
		
		//ambulance related fields
		if (isset($_POST["ambulance"])) {
			$qry->setNumberVal("ambulance",$_POST["ambulance"],"Ambulance");
		} else {
			$qry->setNullVal("ambulance");
		}
		
		if ($pageVars->ambulance_caller_usertype == 1) {
			$qry->setReqNumberVal("ambulance_userid",$pageVars->ambulance_caller_id,"Ambulance caller", "Please enter the ambulance caller");
			$qry->setNullVal("ambulance_assoc_userid");
		} else if($pageVars->ambulance_caller_usertype == 2) {
			$qry->setReqNumberVal("ambulance_assoc_userid",$pageVars->ambulance_caller_id,"Ambulance caller", "Please enter the ambulance caller");
			$qry->setNullVal("ambulance_userid");
		} else {
			$qry->setNumberVal("ambulance_userid",$pageVars->ambulance_caller_id,"Ambulance caller", "Please enter the ambulance caller");
		}
				
		if (isset($_POST["ambulance_caller_usertype"])) {
			$qry->setNumberVal("ambulance_caller_usertype",$_POST["ambulance_caller_usertype"],"ambulance caller usertype");
		} else {
			$qry->setNullVal("ambulance_caller_usertype");
		}
		
				
		if (isset($_POST["ambulance_time"])) {
			$qry->setNumberVal("ambulance_time",$_POST["ambulance_time"],"Ambulance time", "Please enter the number value in minutes");
		} else {
			$qry->setNullVal("ambulance_time");
		}
				
		//injury
		if (isset($_POST["party_injured"])) {
			$qry->setReqNumberVal("party_injured",$_POST["party_injured"],"Person Injured");
		} else {
			$qry->setNullVal("party_injured");
		}
		if (isset($_POST["injury_Q1"])) {
			$qry->setStringVal("injury_Q1",$_POST["injury_Q1"],"Was the injury");
		} else {
			$qry->setNullVal("injury_Q1");
		}
		if (isset($_POST["injury_Q2"])) {
			$qry->setStringVal("injury_Q2",$_POST["injury_Q2"],"Did the injured person");
		} else {
			$qry->setNullVal("injury_Q2");
		}
		
		//First Aid Details		
		if (isset($_POST["first_aid"])) {
			$qry->setStringVal("first_aid",$_POST["first_aid"],"First Aid Given");
		} else {
			$qry->setNullVal("first_aid");
		}
		if (isset($_POST["first_aid_desc"])) {
			$qry->setStringVal("first_aid_desc",$_POST["first_aid_desc"],"First Aid Description");
		} else {
			$qry->setNullVal("first_aid_desc");
		}
                

                // illness
                if (isset($_POST["accident_type"]) && $_POST["accident_type"] == 'ILLNESS') 
                {                    
                    if(isset($_POST['paracetamol']))
                        $qry->setBooleanVal('paracetamol', $_POST['paracetamol'], 'Paracetamol Administered');
                    else
                        $qry->setNullVal('paracetamol');
                    
                    if(isset($_POST['sent_home']))
                        $qry->setBooleanVal('sent_home', $_POST['sent_home'], 'Sent Home');
                    else
                        $qry->setNullVal('sent_home');
                    
                    if(isset($_POST['sent_home_contact']))
                        $qry->setStringVal('sent_home_contact', $_POST['sent_home_contact'], 'Sent Home Contact');
                    else
                        $qry->setNullVal('sent_home_contact');
                    
                    if(isset($_POST['sent_home_userid']))
                        $qry->setNumberVal('sent_home_userid', $_POST['sent_home_userid'], 'Sent Home User');
                    else
                        $qry->setNullVal('sent_home_userid');
                    
                    if(isset($_POST['sent_home_associd']))
                        $qry->setNumberVal('sent_home_associd', $_POST['sent_home_associd'], 'Sent Home Associated User');
                    else
                        $qry->setNullVal('sent_home_associd');                    
                }
                
                // Restraint
                if (isset($_POST["accident_type"]) && $_POST["accident_type"] == 'RESTRAINT') 
                {  
                    /*
                     // removed as is a duplicate of the Incident Description field
                    if(isset($_POST['restraint_desc']))
                        $qry->setStringVal('restraint_desc', $_POST['restraint_desc'], 'Restraint Description');
                    else
                        $qry->setNullVal('restraint_desc');
                    // */
                    
                    if(isset($_POST['restraint_userid']))
                        $qry->setStringVal('restraint_userid', $_POST['restraint_userid'], 'Restraint Registered User');
                    else
                        $qry->setNullVal('restraint_userid');
                    
                    if(isset($_POST['restraint_associd']))
                        $qry->setStringVal('restraint_associd', $_POST['restraint_associd'], 'Restraint Associated User');
                    else
                        $qry->setNullVal('restraint_associd');
                }
                
		//only save internal investigator if the investigation report is still in progress
		$inv_qry = "select state from acc_investigation where accidentid = ".$pageVars->accidentid;
		$inv_rs = getRS($con, $inv_qry);
		$inv_row = getRow($inv_rs);
		if ($inv_row['state'] != 'COMPLETE') {
                    if (isset($_POST["int_investigator_userid"]) && $_POST["int_investigator_userid"] > 0) {
                        $qry->setNumberVal("int_investigator_userid", $_POST['int_investigator_userid'], "Internal Investigator");
                    }else{
                        $qry->setNullVal("int_investigator_userid");
                    }
		}
		
		
		$qry->setReqStringVal("state","IN PROGRESS","State");
		$qry->setAudit($clsUS->userid);
		if ($qry->execute($con)) {
			if ($pageVars->accidentid == 0) {
				$pageVars->accidentid = $qry->getNewID();	
			}
			return "";
		} else {
			return $qry->getError();
		}
	}
		
		
		
	// function to save a draft form with few required fields
	function saveCompleteForm($con, $pageVars, $clsUS) {
		GLOBAL $pageVars;
		if ($pageVars->accidentid == 0) {
			$qry = new dbInsert("acc_accident");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
		} else {
			$qry = new dbUpdate("acc_accident");
			$qry->setParam("accidentid", $pageVars->accidentid);
		}
		
		//personal details
		if ($pageVars->injuredid === 0) $pageVars->injuredid = "";
		if ($pageVars->injured_usertype == 1) {
			$qry->setReqNumberVal("userid",$pageVars->injuredid,"Injured Party", "Please enter the Injured Party");
			$qry->setNullVal("assoc_userid");
		} else if($pageVars->injured_usertype == 2) {
			$qry->setReqNumberVal("assoc_userid",$pageVars->injuredid,"Injured Party", "Please enter the Injured Party");
			$qry->setNullVal("userid");
		} else {
			$qry->setReqNumberVal("userid",$pageVars->injuredid,"Injured Party", "Please enter the Injured Party");
		}
		
		$qry->setReqNumberVal("injured_usertype",$pageVars->injured_usertype,"Injured user type");

		$qry->setReqStringVal("status",$_POST["status"],"Status");
		if (isset($_POST["status_other"])) {
			$qry->setReqStringVal("status_other",$_POST["status_other"],"Status - Other");
		} else {
			$qry->setNullVal("status_other");
		}
		
		//year group if status == student
		if (isset($_POST["year_group"])) {
			$guardian_notified = 0;
			if (isset($_POST['guardian_notified'])) $guardian_notified = $_POST['guardian_notified'];
			
			$qry->setBooleanVal("guardian_notified",$guardian_notified,"Parent/carer notified");
			$qry->setReqNumberVal("year_group",$_POST["year_group"],"Year Group");
			$qry->setDateVal("guardian_notified_date",$_POST["guardian_notified_date"],"Parent/carer date notified");
		} else {
			$qry->setNullVal("year_group");
			$qry->setNullVal("guardian_notified");
			$qry->setNullVal("guardian_notified_date");
		}
		
		//accident details
		$acc_date = $_POST['accident_time'];
		$hrs = $_POST["accident_time_hrs"];
		$hrs = sprintf("%02d", $hrs);
		$mins = $_POST["accident_time_mins"];
		$mins = sprintf("%02d", $mins);
		$acc_date .= " {$hrs}:{$mins}:00";

		$qry->setReqDateTimeVal("accident_time",$acc_date,"Accident Time and Date");
		if (isset($_POST["accident_type"])) {
			$qry->setReqStringVal("accident_type",$_POST["accident_type"],"Accident Type");
		} else {
			$qry->setReqStringVal("accident_type","","Accident Type");
		}
		
		//now if an accident:
		if (isset($_POST["accident_type"]) && $_POST["accident_type"] == "ACCIDENT") {
		
		if (isset($_POST["riddor"])) {
			$qry->setReqStringVal("riddor",$_POST["riddor"],"Riddor");
		} else {
			$qry->setReqStringVal("riddor","","Riddor");
		}

		if ($_POST["riddor"] == 'RIDDOR') {
			if (isset($_POST["riddor_ref"])) {
				$qry->setReqStringVal("riddor_ref",$_POST["riddor_ref"],"Riddor ref");
			} else {
				$qry->setReqStringVal("riddor_ref","","Riddor_ref");
			}
		}
		
		$qry->setReqStringVal("accident_desc",$_POST["accident_desc"],"Incident Description");
	
		//site details
		if ($pageVars->siteid === 0) $pageVars->siteid = "";
		$qry->setReqNumberVal("siteid",$pageVars->siteid,"Site Details");
                
                    
			//ambulance related fields
			$qry->setReqNumberVal("ambulance",$_POST["ambulance"],"Ambulance");
			
			if ($_POST["ambulance"] == 1) {
			
				if ($pageVars->ambulance_caller_usertype == 1) {
					$qry->setReqNumberVal("ambulance_userid",$pageVars->ambulance_caller_id,"Ambulance caller", "Please enter the ambulance caller");
					$qry->setNullVal("ambulance_assoc_userid");
				} else if($pageVars->ambulance_caller_usertype == 2) {
					$qry->setReqNumberVal("ambulance_assoc_userid",$pageVars->ambulance_caller_id,"Ambulance caller", "Please enter the ambulance caller");
					$qry->setNullVal("ambulance_userid");
				} else {
					$qry->setReqNumberVal("ambulance_userid",$pageVars->ambulance_caller_id,"Ambulance caller", "Please enter the ambulance caller");
					$qry->setReqNumberVal("ambulance_caller_usertype","","ambulance caller usertype");
				}
				
				$qry->setReqNumberVal("ambulance_time",$_POST["ambulance_time"],"Ambulance time", "Please enter the ambulance reponse time");
				
			} else {
				$qry->setNullVal("ambulance_userid");
				$qry->setNullVal("ambulance_caller_usertype");
				$qry->setNullVal("ambulance_time");
				$qry->setNullVal("ambulance_assoc_userid");
			}
			//injury
			$qry->setReqNumberVal("party_injured",$_POST["party_injured"],"Person Injured");
			
			if ($_POST["party_injured"] == 1) {
				$qry->setReqStringVal("injury_Q1",$_POST["injury_Q1"],"Was the injury");
				$qry->setReqStringVal("injury_Q2",$_POST["injury_Q2"],"Did the injured person");
				
				for($i=0; $i<count($_POST['injury_id']); $i++) {
					$injury_entered = false;
					//echo "<br/>inj type".$_POST["injury_type"][$i];
					//echo "<br/>part".$_POST["injury_part"][$i];
					//echo "<br/>desc:".$_POST["injury_desc"][$i];
					if ($_POST["injury_type"][$i] != "" && $_POST["injury_part"][$i] != "" && $_POST["injury_desc"][$i] != "") {
						$injury_entered = true;
					} 
				}
				if ($injury_entered == false) $qry->failError .= "Please enter the injury details.\r\n";
			} else {
				$qry->setNullVal("injury_Q1");
				$qry->setNullVal("injury_Q2");
			}
			
				
			//First Aid Details		
			$qry->setReqNumberVal("first_aid",$_POST["first_aid"],"First Aid Given");
			
			if ($_POST["first_aid"] == 1) {
				$qry->setReqStringVal("first_aid_desc",$_POST["first_aid_desc"],"First Aid Description");
			} else {
				$qry->setNullVal("first_aid_desc");
			}
			
			$qry->setReqStringVal("about_accident",$_POST["about_accident"],"About the accident");
	
			
			if ($_POST["about_accident"] == "fall") {
				$qry->setReqNumberVal("fall_height",$_POST["fall_height"],"Fall Height");
			} else {
				$qry->setNullVal("fall_height");
			}	
			
			//$qry->setReqNumberVal("days_off",$_POST["days_off"],"Days Off");
			//$qry->setReqNumberVal("cost",$_POST["cost"],"Cost");
				
		} 
		elseif (isset($_POST["accident_type"]) && $_POST["accident_type"] == "NEAR MISS")
		{
			$qry->setNullVal("first_aid");
			$qry->setNullVal("injury_Q1");
			$qry->setNullVal("injury_Q2");
			$qry->setNullVal("first_aid_desc");
			$qry->setNullVal("ambulance");
			$qry->setNullVal("ambulance_userid");
			$qry->setNullVal("ambulance_caller_usertype");
			$qry->setNullVal("ambulance_time");
			$qry->setNullVal("ambulance_assoc_userid");
			$qry->setNullVal("party_injured");
			$qry->setNullVal("about_accident");
			$qry->setNullVal("fall_height");			
		}
		elseif(isset($_POST["accident_type"]) && $_POST["accident_type"] == "ILLNESS")
		{                    
			$qry->setReqStringVal('paracetamol', $_POST['paracetamol'], 'Paracetamol Administered');
			$qry->setReqStringVal('sent_home', $_POST['sent_home'], 'Sent Home');
			
			if($_POST['sent_home'] == '1')
			{
				$qry->setReqStringVal('sent_home_contact', $_POST['sent_home_contact'], 'Sent Home Contact');
				
				// one or the other must be set
				if(!isset($_POST['sent_home_associd']))
					$qry->setReqNumberVal('sent_home_userid', $_POST['sent_home_userid'], 'Sent Home Registered User', 'A person who authorised the sending home must be selected');
				
				if(!isset($_POST['sent_home_userid']))
				$qry->setReqNumberVal('sent_home_associd', $_POST['sent_home_associd'], 'Sent Home Associated User', 'A person who authorised the sending home must be selected');
			}
			
			// null values
			$qry->setNullVal("first_aid");
			$qry->setNullVal("injury_Q1");
			$qry->setNullVal("injury_Q2");
			$qry->setNullVal("first_aid_desc");
			$qry->setNullVal("ambulance");
			$qry->setNullVal("ambulance_userid");
			$qry->setNullVal("ambulance_caller_usertype");
			$qry->setNullVal("ambulance_time");
			$qry->setNullVal("ambulance_assoc_userid");
			$qry->setNullVal("party_injured");
			$qry->setNullVal("about_accident");
			$qry->setNullVal("fall_height");			
		}
		elseif(isset($_POST["accident_type"]) && $_POST["accident_type"] == "RESTRAINT")
		{      
			// removed as is a duplicate of the incident description field
			// $qry->setReqStringVal('restraint_desc', $_POST['restraint_desc'], 'Restraint Description');
			
			// one or the other must be set
			if(!isset($_POST['restraint_associd']))
			{
				$qry->setReqNumberVal('restraint_userid', $_POST['restraint_userid'], 'Restraint Registered User', 'A person must be selected as a Restrainer');
			}
			
			if(!isset($_POST['restraint_userid']))
			{
				$qry->setReqNumberVal('restraint_associd', $_POST['restraint_associd'], 'Restraint Associated User', 'A person must be selected as a Restrainer');
			}
			
			// null values
			$qry->setNullVal("first_aid");
			$qry->setNullVal("injury_Q1");
			$qry->setNullVal("injury_Q2");
			$qry->setNullVal("first_aid_desc");
			$qry->setNullVal("ambulance");
			$qry->setNullVal("ambulance_userid");
			$qry->setNullVal("ambulance_caller_usertype");
			$qry->setNullVal("ambulance_time");
			$qry->setNullVal("ambulance_assoc_userid");
			$qry->setNullVal("party_injured");
			$qry->setNullVal("about_accident");
			$qry->setNullVal("fall_height");
		}
				
		$qry->setReqStringVal("state","COMPLETE","State");

		//only save internal investigator if the investigation report is still in progress
		$inv_qry = "select state from acc_investigation where accidentid = ".$pageVars->accidentid;
		$inv_rs = getRS($con, $inv_qry);
		$inv_row = getRow($inv_rs);
		if ($inv_row['state'] != 'COMPLETE') {
			if (isset($_POST["int_investigator_userid"]) && $_POST["int_investigator_userid"] > 0) {
				$qry->setReqNumberVal("int_investigator_userid", $_POST['int_investigator_userid'], "Internal Investigator");
			}
		}        
                
		$qry->setAudit($clsUS->userid);
		$qry->getSql();
		if ($qry->execute($con)) {
			if ($pageVars->accidentid == 0) {
				$pageVars->accidentid = $qry->getNewID();	
			}
			return "";
		} else {
			return $qry->getError();
		}
	}		
