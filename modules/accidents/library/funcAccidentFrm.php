<?php
/*functions for displaying the accident forms*/

function showField($trig_field, $show_field, $val, $formDB) {
	if (($formDB[$trig_field] == $val && !isset($_POST[$trig_field])) || (isset($_POST[$trig_field]) &&  isset($_POST[$show_field]) && $_POST[$trig_field] == $val)) return true;
	return false;
}
?>