<?php
##now split can be deleted##

/*functions for editing accidents in accident book*/

	//gets an accident
	function getAccident($con, $pageVars) {
		
		
		$qry = "select *, accident.state as report_state, accident.created_by as report_created_by from accident left join injured_party on accident.injuredid = injured_party.injuredid left join accident_site on accident.siteid = accident_site.siteid where accident.accidentid = ".$pageVars->accidentid." and accident.accountid = ".$pageVars->accountid;
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		return $pageDb->row;
	}

	//gets an accident without personal info of victim
	function getCopy($con, $pageVars) {
		$qry = "select 'NEW' as state, ''as injuredid, '' as status,'' as first_aid,'' as first_aidDesc,'' as status_other,'' as party_injured,'' as injury_Q1,'' as injury_Q2,'' as fall_height,'' as days_off,'' as cost,'' as accident_type, siteid, accident_time, accident_desc, about_accident, riddor, ambulance, precautions  from accident where accidentid = ".$pageVars->copyid." and accountid = ".$pageVars->accountid;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		return $pageDb->row;
	}
	
	function getFirstAiders($con, $pageVars) {
		$qry = "select first_aider.* from first_aider left join accident on first_aider.accidentid = accident.accidentid where first_aider.accidentid = ".$pageVars->accidentid." and accident.accountid = ".$pageVars->accountid;	
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList();
		return $pageDb;
	}


	function getInjuries($con, $pageVars) {
		$qry = "select injury_details.* from injury_details left join accident on injury_details.accidentid = accident.accidentid where injury_details.accidentid = ".$pageVars->accidentid." and accident.accountid = ".$pageVars->accountid;	
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList();
		return $pageDb;
	}

	function getInjuredParties($con, $pageVars) {
		$qry = "select * from injured_party where accountid = ".$pageVars->accountid;	
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList();
		return $pageDb;
	}
	

	
	
	
	//function to save a draft form with few required fields
	function saveDraftForm($con, $pageVars, $clsUS) {
		GLOBAL $pageVars;
		if ($pageVars->accidentid == 0) {
			$qry = new dbInsert("accident");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
		} else {
			$qry = new dbUpdate("accident");
			$qry->setParam("accidentid", $pageVars->accidentid);
		}
		

		//echo "injuredid: ".$pageVars->injuredid;
		
		if ($pageVars->injuredid === 0) $pageVars->injuredid = "";
		$qry->setReqNumberVal("injuredid",$pageVars->injuredid,"Injured Party");
		
		
		$qry->setStringVal("status",$_POST["status"],"Status");
		if (isset($_POST["status_other"])) {
			$qry->setReqStringVal("status_other",$_POST["status_other"],"Status - Other");
		} else {
			$qry->setNullVal("status_other");
		}
		
		//site details
		if ($pageVars->siteid == 0) {
			$qry->setNullVal("siteid");
		} else {
			$qry->setNumberVal("siteid",$pageVars->siteid,"Site Details");
		}

		//accident details
		$acc_date = $_POST['accident_time'];
		$hrs = $_POST["accident_time_hrs"];
		$hrs = sprintf("%02d", $hrs);
		$mins = $_POST["accident_time_mins"];
		$mins = sprintf("%02d", $mins);
		$acc_date .= " {$hrs}:{$mins}:00";

		$qry->setReqDateTimeVal("accident_time",$acc_date,"Accident Time and Date");
		
		if (isset($_POST["accident_type"])) {
			$qry->setStringVal("accident_type",$_POST["accident_type"],"Accident Type");
		} else {
			$qry->setNullVal("accident_type");
		}
		$qry->setStringVal("about_accident",$_POST["about_accident"],"About the accident");
		if (isset($_POST["fall_height"])) {
			$qry->setNumberVal("fall_height",$_POST["fall_height"],"Fall Height");
		} else {
			$qry->setNullVal("fall_height");
		}
		$qry->setStringVal("accident_desc",$_POST["accident_desc"],"Accident Description");
		$qry->setStringVal("riddor",$_POST["riddor"],"Riddor");
		$qry->setStringVal("ambulance",$_POST["ambulance"],"Ambulance");
		
		//injury
		$qry->setReqNumberVal("party_injured",$_POST["party_injured"],"Person Injured");
		$qry->setStringVal("injury_Q1",$_POST["injury_Q1"],"Was the injury");
		$qry->setStringVal("injury_Q2",$_POST["injury_Q2"],"Did the injured person");
		
		//First Aid Details		
		$qry->setStringVal("first_aid",$_POST["first_aid"],"First Aid Given");
		$qry->setStringVal("first_aid_desc",$_POST["first_aid_desc"],"First Aid Description");
		
		//Post Acident deatils
		
		if (trim($_POST["days_off"]) == "") {
			$qry->setNullVal("days_off");
		} else {
			$qry->setNumberVal("days_off",$_POST["days_off"],"Days Off");
		}			
		
		if (trim($_POST['cost']) == "") {
			$qry->setNullVal("cost");
		} else {
			$qry->setNumberVal("cost",$_POST["cost"],"Cost");
		}
		$qry->setStringVal("precautions",$_POST["precautions"],"Precautions");
		$qry->setReqStringVal("state","IN PROGRESS","State");
		$qry->setAudit($clsUS->userid);
		if ($qry->execute($con)) {
			if ($pageVars->accidentid == 0) {
				$pageVars->accidentid = $qry->getNewID();	
			}
			return "";
		} else {
			return $qry->getError();
		}
	}	
		
		
		
	//function to save a draft form with few required fields
	function saveCompleteForm($con, $pageVars, $clsUS) {
		GLOBAL $pageVars;
		if ($pageVars->accidentid == 0) {
			$qry = new dbInsert("accident");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
		} else {
			$qry = new dbUpdate("accident");
			$qry->setParam("accidentid", $pageVars->accidentid);
		}
		
		//personal details
		if ($pageVars->injuredid === 0) $pageVars->injuredid = "";
		$qry->setReqNumberVal("injuredid",$pageVars->injuredid,"Injured Party");
		$qry->setReqStringVal("status",$_POST["status"],"Status");
		if (isset($_POST["status_other"])) {
			$qry->setReqStringVal("status_other",$_POST["status_other"],"Status - Other");
		} else {
			$qry->setNullVal("status_other");
		}
		
		//site details
		if ($pageVars->siteid === 0) $pageVars->siteid = "";
		$qry->setReqNumberVal("siteid",$pageVars->siteid,"Site Details");

		//accident details
		$acc_date = $_POST['accident_time'];
		$hrs = $_POST["accident_time_hrs"];
		$hrs = sprintf("%02d", $hrs);
		$mins = $_POST["accident_time_mins"];
		$mins = sprintf("%02d", $mins);
		$acc_date .= " {$hrs}:{$mins}:00";

		$qry->setReqDateTimeVal("accident_time",$acc_date,"Accident Time and Date");
		$qry->setReqStringVal("accident_type",$_POST["accident_type"],"Accident Type");
		
		$qry->setStringVal("about_accident",$_POST["about_accident"],"About the accident");

		if ($_POST["about_accident"] == "fall") {
			$qry->setReqNumberVal("fall_height",$_POST["fall_height"],"Fall Height");
		} else {
			$qry->setNullVal("fall_height");
		}		
				
		
		$qry->setReqStringVal("accident_desc",$_POST["accident_desc"],"Accident Description");
		$qry->setReqStringVal("riddor",$_POST["riddor"],"Riddor");
		$qry->setReqStringVal("ambulance",$_POST["ambulance"],"Ambulance");
		
		//injury
		$qry->setReqNumberVal("party_injured",$_POST["party_injured"],"Person Injured");
		
		if ($_POST["party_injured"] == 1) {
			$qry->setReqStringVal("injury_Q1",$_POST["injury_Q1"],"Was the injury");
			$qry->setReqStringVal("injury_Q2",$_POST["injury_Q2"],"Did the injured person");
		} else {
			$qry->setNullVal("injury_Q1");
			$qry->setNullVal("injury_Q2");
		}
			
		//First Aid Details		
		$qry->setReqNumberVal("first_aid",$_POST["first_aid"],"First Aid Given");
		
		if ($_POST["first_aid"] == 1) {
			$qry->setReqStringVal("first_aid_desc",$_POST["first_aid_desc"],"First Aid Description");
		} else {
			$qry->setNullVal("first_aid_desc");
		}
		
		//Post Acident deatils
		
		$qry->setReqNumberVal("days_off",$_POST["days_off"],"Days Off");
		$qry->setReqNumberVal("cost",$_POST["cost"],"Cost");
		$qry->setReqStringVal("precautions",$_POST["precautions"],"Precautions");
		
		$qry->setReqStringVal("state","COMPLETE","State");
		$qry->setAudit($clsUS->userid);
		if ($qry->execute($con)) {
			if ($pageVars->accidentid == 0) {
				$pageVars->accidentid = $qry->getNewID();	
			}
			return "";
		} else {
			return $qry->getError();
		}
	}		

	//save injury details
	
	function saveInjury($con, $pageVars, $clsUS) {
		$return = array();
		//echo "injury func AID={$pageVars->accidentid}<br/>";
		if ($pageVars->accidentid > 0) {
			//echo "accidentid > 0<br/>";
			for($i=0; $i<count($_POST['injury_id']); $i++) {
				//echo "injury $i<br/>";
				if (trim($_POST["injury_type"][$i]) != "" || trim($_POST["injury_part"][$i]) != "" || trim($_POST["injury_desc"][$i]) != "") {
					if ($_POST['injury_id'][$i] == 0) {
						$qry = new dbInsert("injury_details");
						$qry->setReqNumberVal("accidentid",$pageVars->accidentid,"Accidentid");
					} else {
						$qry = new dbUpdate("injury_details");
						$qry->setParam("injury_id", $_POST['injury_id'][$i]);
					}
					$injury_num = $i + 1;
					$qry->setReqStringVal("injury_type",$_POST["injury_type"][$i],"Injury $injury_num Injury Type");
					$qry->setReqStringVal("injury_part",$_POST["injury_part"][$i],"Injury $injury_num Injury Part");
					$qry->setReqStringVal("injury_desc",$_POST["injury_desc"][$i],"Injury $injury_num Injury Description");
					$qry->setAudit($clsUS->userid);
					if ($qry->execute($con)) {

					//$return[] = "";
					} else {
						$return[] = $qry->getError();
					}
				}
		
			}
		}
		//die();
		return $return;

	}

	
	//save first Aider details
	
	function saveFirstAiders($con, $pageVars, $clsUS) {
		$return = array();
		if ($pageVars->accidentid > 0) {
			for($i=0; $i<count($_POST['first_aider_id']); $i++) {
				if (trim($_POST["fAfirstname"][$i]) != "" || trim($_POST["fAlastname"][$i]) != "" || trim($_POST["fAaddress1"][$i]) != "" || trim($_POST["fAaddress2"][$i]) || trim($_POST["fAaddress3"][$i]) != "" || trim($_POST["fAcity"][$i]) || trim($_POST["fApostcode"][$i]) || trim($_POST["fAtel1"][$i]) || trim($_POST["fAtel2"][$i]) || trim($_POST["fAcomments"][$i])) {
					if ($_POST['first_aider_id'][$i] == 0) {
						$qry = new dbInsert("first_aider");
						$qry->setReqNumberVal("accidentid",$pageVars->accidentid,"Accidentid");
					} else {
						$qry = new dbUpdate("first_aider");
						$qry->setParam("first_aider_id", $_POST['first_aider_id'][$i]);
					}
					$aider_num = $i + 1;
					$qry->setReqStringVal("fAfirstname",$_POST["fAfirstname"][$i],"First Aider $aider_num First Name");
					$qry->setReqStringVal("fAlastname",$_POST["fAlastname"][$i],"First Aider $aider_num Last Name");
					$qry->setReqStringVal("fAaddress1",$_POST["fAaddress1"][$i],"First Aider $aider_num Address 1");
					$qry->setStringVal("fAaddress2",$_POST["fAaddress2"][$i],"First Aider $aider_num Address 2");
					$qry->setStringVal("fAaddress3",$_POST["fAaddress3"][$i],"First Aider $aider_num Address 3");
					$qry->setStringVal("fAcity",$_POST["fAcity"][$i],"First Aider $aider_num City");
					$qry->setReqStringVal("fApostcode",$_POST["fApostcode"][$i],"First Aider $aider_num Postcode");
					$qry->setReqStringVal("fAtel1",$_POST["fAtel1"][$i],"First Aider $aider_num Telephone 1");
					$qry->setStringVal("fAtel2",$_POST["fAtel2"][$i],"First Aider $aider_num Telephone 2");
					$qry->setStringVal("fAcomments",$_POST["fAcomments"][$i],"First Aider $aider_num Comments");
					$qry->setAudit($clsUS->userid);
					if ($qry->execute($con)) {
						//$return[] = "";
					} else {
						$return[] = $qry->getError();
					}
				}
		
			}
		}
		return $return;

	}
	
	//delete injuries that have the hidden delete field posted
	
	function delInjury($con, $pageVars, $clsUS) {
		$return = array();
		if (isset($_POST['del_injury_id'])) {
			for($i=0; $i<count($_POST['del_injury_id']); $i++) {

					if ($_POST['del_injury_id'][$i] != "") {
					$qry = new dbDelete("injury_details");
					$qry->setParam("injury_id", $_POST['del_injury_id'][$i]);
					$injury_num = $i + 1;
					if ($qry->execute($con)) {
						//$return[] = "";
					} else {
						$return[] = "Error deleting Injury $injury_num ".$qry->getError();
					}
				}
		
			}
		}
		return $return;

	}
	
	//delete first aiders that have the hidden delete field posted
	
	function delAider($con, $pageVars, $clsUS) {
		$return = array();
		if (isset($_POST['del_aider_id'])) {
			for($i=0; $i<count($_POST['del_aider_id']); $i++) {

					if ($_POST['del_aider_id'][$i] != "") {
					$qry = new dbDelete("first_aider");
					$qry->setParam("first_aider_id", $_POST['del_aider_id'][$i]);
					$injury_num = $i + 1;
					if ($qry->execute($con)) {
						//$return[] = "";
					} else {
						$return[] = "Error deleting First Aider $injury_num ".$qry->getError();
					}
				}
		
			}
		}
		return $return;

	}	
	
	
	



		
?>