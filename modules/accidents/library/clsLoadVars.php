<?php
/**
 * Class to load variables from GET, POST, user session depending on security
 */
class loadVars {

	var $userid;
	var $injured_usertype;
	var $accountid;
	var $accidentid;
	var $copyid;
	var $injuredid;
	var $siteid;
	var $aider_userid;
	var $aider_usertype;
	var $first_aider_id;
	var $aider_num;
	var $ambulance_caller_id;
	var $ambulance_caller_usertype;
	var $investigation_id;

	var $witness_userid;
	var $witness_usertype;
	var $witness_id;
	var $witness_num;
	
	var $fileid;

	var $hacking = false;
	var $hacking_arr = array();
	
	
	
	function loadVars($clsUS,$con) {
		if (isAllowed("editAccount")) {
			$this->userid = getIntFromRequest("userid");
			$this->accountid = getIntFromRequest("accountid", $clsUS->accountid);
			$this->accidentid = getIntFromRequest("accidentid");
		} else {
			$this->userid = $clsUS->userid;
			$this->accountid = $clsUS->accountid;
			$this->accidentid = $this->checkAccidentId($clsUS,$con);
		} 
		$this->injured_usertype = getIntFromRequest("injured_usertype");
		$this->ambulance_caller_usertype = getIntFromRequest("ambulance_caller_usertype");
		
		if (isset($_POST['aider_userid']) && is_array($_POST['aider_userid'])) {
			
			for ($i=0; $i<count($_POST['aider_userid']); $i++) {
				if ($_POST['aider_userid'][$i] > 0) {
					$this->aider_usertype[$i] = intval($_POST['aider_usertype'][$i]);
					$this->aider_num[$i] = intval($_POST['aider_num'][$i]);
					$this->first_aider_id[$i] = intval($_POST['first_aider_id'][$i]);
					$this->aider_userid[$i] = $this->checkUserId($clsUS,$con, "aider_userid", $this->aider_usertype[$i], $i);
				}
			}
			
		} else {
			
			$this->aider_usertype = getIntFromRequest("aider_usertype");
			$this->aider_num = getIntFromRequest("aider_num");
			$this->first_aider_id = getIntFromRequest("first_aider_id");
			$this->aider_userid = $this->checkUserId($clsUS,$con, "aider_userid", $this->aider_usertype);
		}
		
		$this->copyid = $this->checkCopyId($clsUS,$con);
		$this->injuredid = $this->checkUserId($clsUS,$con, "injuredid", $this->injured_usertype);
		$this->ambulance_caller_id = $this->checkUserId($clsUS,$con, "ambulance_caller_id", $this->ambulance_caller_usertype);
		$this->siteid = $this->checkSiteId($clsUS,$con);
		
		$this->investigation_id = $this->checkInvestigationId($clsUS,$con);
		
		
		if (isset($_POST['witness_userid']) && is_array($_POST['witness_userid'])) {
			
			for ($i=0; $i<count($_POST['witness_userid']); $i++) {
				if ($_POST['witness_userid'][$i] > 0) {
					$this->witness_usertype[$i] = intval($_POST['witness_usertype'][$i]);
					$this->witness_num[$i] = intval($_POST['witness_num'][$i]);
					$this->witness_id[$i] = intval($_POST['witness_id'][$i]);
					$this->witness_userid[$i] = $this->checkUserId($clsUS,$con, "witness_userid", $this->witness_usertype[$i], $i);
				}
			}
			
		} else {
			
			$this->witness_usertype = getIntFromRequest("witness_usertype");
			$this->witness_num = getIntFromRequest("witness_num");
			$this->witness_id = getIntFromRequest("witness_id");
			$this->witness_userid = $this->checkUserId($clsUS,$con, "witness_userid", $this->witness_usertype);
		}
		
		$this->fileid = $this->checkFileId($clsUS,$con);
		if ($this->hacking == true) {
			debugLog($con, $clsUS, $this, "", $this->hacking_arr, "hacking", "");
		}
		
	}
	
	function checkAccidentId($clsUS,$con) {
		$accidentid = getIntFromRequest("accidentid");
		if ($accidentid === 0) return 0;
		if ($accidentid === null) return null;
		if ($accidentid === "") return "";
		if (isAllowed("adminAccidents")) {
			return $accidentid;
		} else if ($accidentid > 0) {
			$qry = "select accountid, created_by from acc_accident where accidentid = $accidentid";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordAccidents") && $row['accountid'] == $clsUS->accountid){
				return $accidentid;
			} else if (isAllowed("viewAccidents") && $row['accountid'] == $clsUS->accountid){
				return $accidentid;
			}
		}
		$this->hacking = true;
		$this->hacking_arr[] = "accidentid=$accidentid";
		return null;
	}
	
	//$this->checkUserId($clsUS,$con, "aider_userid", $this->aider_usertype[$i], $i)
	function checkUserId($clsUS,$con, $param_id, $param_type, $i="") {
		if ($i === "") {
			$id = getIntFromRequest($param_id);
		} else {
			if (settype($_POST[$param_id][$i],"integer")) {
				$id = $_POST[$param_id][$i];
			} else {
				return 0;
			}
		}
		if ($id === 0) return 0;
		if ($id === null) return "";
		if ($id === "") return "";
		if (isAllowed("adminAccidents")) {
			return $id;
		} else if ($id > 0) {
			//echo $param_type;
			if ($param_type == 1) $qry = "select accountid, created_by from usr where userid = $id";
			if ($param_type == 2) $qry = "select accountid, created_by from usr_assoc where assoc_userid = $id";
			//die($qry);
			if (isset($qry)) {
				$rs = getRS($con,$qry);
				$row = getRow($rs);
				if (isAllowed("recordAccidents") && $row['accountid'] == $clsUS->accountid){
					return $id;
				} else if (isAllowed("viewAccidents") && $row['accountid'] == $clsUS->accountid){
					return $id;
				}
			}
		}
		
		$this->hacking = true;
		$this->hacking_arr[] = "$param_id:$param_type=$id";
		return null;
	}	
	
	function checkSiteId($clsUS,$con) {
		$siteid = getIntFromRequest("siteid");
		if ($siteid == 0) return 0;
		if (isAllowed("adminAccidents")) {
			return $siteid;
		} else if ($siteid > 0) {
			$qry = "select accountid, created_by from acc_accident_site where siteid = $siteid";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordAccidents") && $row['accountid'] == $clsUS->accountid){
				return $siteid;
			} else if (isAllowed("viewAccidents") && $row['accountid'] == $clsUS->accountid){
				return $siteid;
			}
		}
		$this->hacking = true;
		$this->hacking_arr[] = "siteid=$siteid";
		return null;
	}	
	
	function checkCopyId($clsUS,$con) {
		$accidentid = getIntFromRequest("copyid");
		if ($accidentid === 0) return 0;
		if ($accidentid == "") return "";
		if (isAllowed("adminAccidents")) {
			return $accidentid;
		} else if ($accidentid > 0) {
			$qry = "select accountid, created_by from acc_accident where accidentid = $accidentid";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordAccidents") && $row['accountid'] == $clsUS->accountid){
				return $accidentid;
			} else if (isAllowed("viewAccidents") && $row['accountid'] == $clsUS->accountid){
				return $accidentid;
			}
		}
		$this->hacking = true;
		$this->hacking_arr[] = "copyid=$accidentid";
		return null;
	}

	function checkInvestigationId($clsUS,$con) {
		$investigation_id = getIntFromRequest("investigation_id");
		if ($investigation_id === 0) return 0;
		if ($investigation_id === null) return 0;
		if (isAllowed("adminAccidents")) {
			return $investigation_id;
		} else if ($investigation_id > 0) {
			$qry = "select accountid, created_by from acc_investigation where investigation_id = $investigation_id";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordAccidents") && $row['accountid'] == $clsUS->accountid){
				return $investigation_id;
			} else if (isAllowed("viewAccidents") && $row['accountid'] == $clsUS->accountid){
				return $investigation_id;
			}
		}
		$this->hacking = true;
		$this->hacking_arr[] = "investigationid=$investigation_id";
		return null;
	}	

	
	function checkFileId($clsUS,$con) {
		$id = getIntFromRequest("fileid");
		if ($id === 0) return 0;
		if ($id === "") return "";
		if ($id === null) return null;
		if (isAllowed("editAccount")) {
			return $id;
		} else if ($id > 0) {
			$qry = "select created_by, accountid from files where fileid = $id";
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("recordAccidents") && $row['accountid'] == $clsUS->accountid && $row['created_by'] == $clsUS->userid){
				return $id;
			}
		}
		$this->hacking = true;
		$this->hacking_arr[] = "fileid=$ids";
		return null;
	}	
	
	
	
}
	
?>