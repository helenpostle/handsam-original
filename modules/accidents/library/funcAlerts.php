<?php
/**
 * funcAlerts.php
 * 
 * Custom functions for the Accident Alerts
 * 
 * MG, August 2012
 */

/**
 * Returns Alerts of a type
 * 
 * @param int account id
 * @param string the module or function which specifies the type of alert
 */
function getAlerts($account_id, $type)
{    
    $alert = array();
    $sql = "SELECT id, alert_type, the_values, amount, unit
            FROM alerts
            WHERE account_id = '".$account_id."' 
                AND alert_type = '".$type."' 
                AND state = 'OK'                 
            ORDER BY amount ASC;";
    
    $res = getRecSet($sql);
    
    while($row = getRowAssoc($res))
        $alert[] = $row;
    
    return $alert;
}

/**
 * Get Global alerts  
 */
function getGlobalAlerts($type)
{
    return getAlerts('0', $type);
}


/**
 * Returns All Alerts of a type
 * 
 * 
 * @param string the module or function which specifies the type of alert
 */
function getAllAlerts($type)
{    
    $alert = array();
    $sql = "SELECT id, alert_type, the_values, amount, unit, account_id 
            FROM alerts
            WHERE alert_type = '".$type."' 
                AND state = 'OK'                 
            ORDER BY amount ASC;";
    
    $res = getRecSet($sql);
    
    while($row = getRowAssoc($res))
        $alert[] = $row;
    
    return $alert;
}

/**
 * Retrieves all the users for an alert
 * @param int the alert id
 * @return array
 */
function getAlertUsers($alert_id, $account_id=0)
{
    $users = array();
	$account_qry = "";
	if ($account_id > 0) $account_qry = "AND (u.accountid = '".$account_id."' OR u.accountid = 1)";
    $sql = "SELECT au.id AS alertuser_id, 
            IF(au.usr_id IS NULL, au.user_name, CONCAT(u.firstname, ' ', u.lastname)) AS user_name,
            IF(au.usr_id IS NULL, au.user_email, u.username) AS user_email,
            u.accountid AS accountid,
			u.userid as usr_id 
            FROM alerts_users AS au
            LEFT JOIN usr AS u 
              ON au.usr_id = u.userid
            WHERE au.alerts_id = '".$alert_id."'
                $account_qry;";
    
    $res = getRecSet($sql);
    
    while($row = getRowAssoc($res))
        $users[] = $row;
    
    return $users;
}


/**
 * Retrieves all job roles associated with an alert
 * @param int $alert_id
 * @return array
 */
 
function getAlertJobroles($alert_id, $account_id)
{
    $roles = array();
    $sql = "SELECT aj.id AS alerts_jobrole_id, aj.jobrole_id, 
            j.role_title, j.role_desc, 
            u.userid AS usr_id, u.accountid, CONCAT(u.firstname, ' ', u.lastname) AS username, u.username AS useremail
            FROM alerts_jobrole AS aj 
            LEFT JOIN core_jobrole AS j
                ON aj.jobrole_id = j.role_id
            LEFT JOIN core_usr_jobrole AS uj
                ON j.role_id = uj.jobrole_id
            LEFT JOIN usr AS u
                ON uj.usr_id = u.userid AND u.accountid = '".$account_id."'
            WHERE aj.alert_id = '".$alert_id."'
            ;";
    $res = getRecSet($sql);
    
    return formatAlertJobroles($res);
}

/** 
 * used for processing alerts and gertting an array of job role users for an alert
 *
 */
function getAlertJobRoleUsers($alert_id, $a_id) {
	$users = array();
    /*
	$sql = "SELECT aj.id AS alerts_jobrole_id, aj.jobrole_id, 
            j.role_title, j.role_desc, 
            u.userid AS usr_id, u.accountid, CONCAT(u.firstname, ' ', u.lastname) AS username, u.username AS useremail
            FROM alerts_jobrole AS aj 
            LEFT JOIN core_jobrole AS j
                ON aj.jobrole_id = j.role_id
            LEFT JOIN core_usr_jobrole AS uj
                ON j.role_id = uj.jobrole_id
            LEFT JOIN usr AS u
                ON uj.usr_id = u.userid AND u.accountid = '".$a_id."'
            WHERE aj.alert_id = '".$alert_id."'
            ;";
	*/
	$sql = "SELECT u.userid AS usr_id, u.accountid, CONCAT(u.firstname, ' ', u.lastname) AS user_name, u.username AS user_email
            FROM alerts_jobrole AS aj 
            right JOIN core_usr_jobrole AS uj
                ON aj.jobrole_id = uj.jobrole_id
            right JOIN usr AS u
                ON uj.usr_id = u.userid AND u.accountid = '".$a_id."'
            WHERE aj.alert_id = '".$alert_id."'
            ;";
			
	$res = getRecSet($sql);
    while($row = getRowAssoc($res))
        $users[] = $row;   
    return $users;	
}


/**
 * Nicely formats the result array for JSON
 * @param type $rs myql recordset
 * @param type $account_id account id. to filter out non account users
 * @return type 
 */
function formatAlertJobroles($rs)
{
    $jobrole = array();
        
    $i = -1; // this will increment in the first iteration to 0
    $the_id = 0; // role_id tracker
    while($row = getRowAssoc($rs))
    {        
        // this should be in the original SQL query
        // only increments $i when the role has changed
        if($the_id != $row['jobrole_id'])
        {
            $the_id = $row['jobrole_id'];
            $i++;
            
            $jobrole[$i]['user'] = array(); // set a default empty array
        }
        
        $jobrole[$i]['role_id'] = $row['jobrole_id'];
        $jobrole[$i]['role_title'] = $row['role_title'];
        $jobrole[$i]['alerts_jobrole_id'] = $row['alerts_jobrole_id'];
        
        if(!is_null($row['usr_id']))
        {
            $jobrole[$i]['user'][] = array('usr_id' => $row['usr_id'],
                                            'user_name' => $row['username'],
                                            'user_email' => $row['useremail']
            );
        }

    } // while
    return $jobrole;
}

/**
 * Retrieves all the users for an alert
 * @param int the alert id
 * @return array
 */
function getAlertAccManagers($alert_id, $account_id)
{
    $acc_managers = array();
	if ($account_id == 0) {
		$sql = "SELECT am.id AS alertmanager_id, 
			am.acc_manager_id,
			CASE acc_manager_id WHEN 2 THEN '".ACCOUNTS_ACCOUNT_MANAGER_2."' ELSE '".ACCOUNTS_ACCOUNT_MANAGER."' END AS manager_title
            FROM alerts_acc_manager am 
            WHERE am.alert_id = '".$alert_id."';";
			
		$res = getRecSet($sql);  
		while($row = getRowAssoc($res))
        $acc_managers[] = $row;
		return $acc_managers;

	} else {
		$sql = "SELECT am.id AS alertmanager_id, 
			am.acc_manager_id,
			CASE acc_manager_id WHEN 2 THEN '".ACCOUNTS_ACCOUNT_MANAGER_2."' ELSE '".ACCOUNTS_ACCOUNT_MANAGER."' END AS manager_title ,
			CASE acc_manager_id WHEN 2 THEN CONCAT(u2.firstname, ' ', u2.lastname)  ELSE CONCAT(u1.firstname, ' ', u1.lastname) END AS username ,
			CASE acc_manager_id WHEN 2 THEN u2.username  ELSE u1.username END AS email,
			CASE acc_manager_id WHEN 2 THEN u2.userid  ELSE u1.userid END AS usr_id 
            FROM alerts_acc_manager am 
            LEFT JOIN alerts a ON am.alert_id = a.id 
            LEFT JOIN account ac ON $account_id = ac.accountid
            LEFT JOIN usr u1 ON ac.account_manager = u1.userid
            LEFT JOIN usr u2 ON ac.account_manager_2 = u2.userid
            WHERE am.alert_id = '".$alert_id."';";
		$res = getRecSet($sql);    
		return formatAlertAccManagers($res);
	}
}

/**
 * Nicely formats the result array for JSON
 * @param type $rs myql recordset
 * @param type $account_id account id. to filter out non account users
 * @return type 
 */
function formatAlertAccManagers($rs)
{
    $acc_manager = array();
        
    $i = -1; // this will increment in the first iteration to 0
    $the_id = 0; // role_id tracker
    while($row = getRowAssoc($rs))
    {        
        // this should be in the original SQL query
        // only increments $i when the role has changed
        if($the_id != $row['alertmanager_id'])
        {
            $the_id = $row['alertmanager_id'];
            $i++;
            
            $acc_manager[$i]['manager_users'] = array(); // set a default empty array
        }
        
        $acc_manager[$i]['alertmanager_id'] = $row['alertmanager_id'];
        $acc_manager[$i]['manager_title'] = $row['manager_title'];
        $acc_manager[$i]['acc_manager_id'] = $row['acc_manager_id'];
        
        if(!is_null($row['alertmanager_id']))
        {
            $acc_manager[$i]['manager_users'][] = array('usr_id' => $row['usr_id'],
                                            'username' => $row['username'],
                                            'email' => $row['email']
            );
			
        }

    } // while
    return $acc_manager;
}

/** 
 * used for processing alerts and gertting an array of account manager users for an alert
 *
 */
function getAlertAMUsers($alert_id, $a_id) {
	$users = array();
	$sql = "SELECT am.id AS alertmanager_id, 
			am.acc_manager_id,
			CASE acc_manager_id WHEN 2 THEN '".ACCOUNTS_ACCOUNT_MANAGER_2."' ELSE '".ACCOUNTS_ACCOUNT_MANAGER."' END AS manager_title ,
			CASE acc_manager_id WHEN 2 THEN CONCAT(u2.firstname, ' ', u2.lastname)  ELSE CONCAT(u1.firstname, ' ', u1.lastname) END AS user_name ,
			CASE acc_manager_id WHEN 2 THEN u2.username  ELSE u1.username END AS user_email,
			CASE acc_manager_id WHEN 2 THEN u2.userid  ELSE u1.userid END AS usr_id 
            FROM alerts_acc_manager am 
            LEFT JOIN alerts a ON am.alert_id = a.id 
            LEFT JOIN account ac ON $a_id = ac.accountid
            LEFT JOIN usr u1 ON ac.account_manager = u1.userid
            LEFT JOIN usr u2 ON ac.account_manager_2 = u2.userid
            WHERE am.alert_id = '".$alert_id."';";
	$res = getRecSet($sql);
    while($row = getRowAssoc($res))
        $users[] = $row;   
    return $users;	
}

/**
 * Saves a single alert: either an edit or new record. 
 * 
 * @param string type of alert
 * @param array Alert array
 * @return int the id of the record
 */
function saveAlert($type, $account_id, $alert)
{
    global $con;
    $values = serialize($alert['values']);
	$amount = $alert['values']['amount'];
	if ($amount == '') {
		$amount = 'null';
	} else {
		$amount = "'".escapeString($alert['values']['amount'])."'";
	}
   if(isset($alert['id']))
    {
        // edit
        $sql = "UPDATE alerts 
                SET the_values = '".escapeString($values)."', 
                    amount = $amount, 
                    unit = '".  escapeString($alert['values']['unit'])."', 
                    account_id = '".  escapeString($account_id)."' 
                WHERE id = '".escapeString($alert['id'])."';";
        
        execSQL($con, $sql);
        $id = $alert['id'];
    }
    else
    {
        // new
        $sql = "INSERT INTO alerts (alert_type, the_values, amount, unit, account_id) 
            VALUES ('".$type."', '".escapeString($values)."', $amount, '".escapeString($alert['values']['unit'])."', '".  escapeString($account_id)."');";
        execSQL($con, $sql);
        $id = get_last_insert_id();
    }

    return $id;
}

/**
 *
 * @param string alert type
 * @param int account id, default is 0
 * @return int id of the new record
 */
function createBlankAlert($type, $account_id = '0')
{
    $alert = array('values' => array('amount' => '', 'unit' => ''));
    
    return saveAlert($type, $account_id, $alert);
}

/**
 * Saves the user data to the database, 
 * if they're a new or existing user.
 * 
 * @param int alert id
 * @param object user 
 */
function saveAlertUsers($alertid, $alert)
{
    if(isset($alert['user']))
    {
        $user = $alert['user'];
        foreach($user as $u)
        {
            if($u['alertuser_id'] > 0)
            {
                // edit
                editAlertUser($u);
            }
            else
            {
                // new
                newAlertUser($alertid, $u);
            }
        }
    }
    
    if(isset($alert['jobrole']))
    {
        $jr = $alert['jobrole'];
        foreach($jr as $role)
        {
			
            if($role['alerts_jobrole_id'] == 0)
            {
                newJobrole($alertid, $role);
            }
        }
    }
    if(isset($alert['accountmanager']))
    {
        $am = $alert['accountmanager'];
        foreach($am as $manager)
        {
			
            if($manager['alert_am_id'] == 0)
            {
                newAccountManager($alertid, $manager);
            }
        }
    }
}

/** 
 * Updates user data in the DB
 * used in saveAlertUser()
 * 
 * @global object MySQL db con
 * @param type $u
 * @return type 
 */
function editAlertUser($u)
{
    global $con;
    
    $sql = "UPDATE alerts_users ";
    if(isset($u['usr_id']))
    {
        // account user
        $sql .= "SET usr_id = '".mysql_real_escape_string($u['usr_id'])."' ";
    }
    else
    {
        // other user
        $sql .= "SET user_name = '".mysql_real_escape_string($u['user_name'])."', 
                     user_email = '".mysql_real_escape_string($u['user_email'])."' ";
    }
    $sql .= "WHERE id = '".$u['alertuser_id']."' ;";
    
    return execSQL($con, $sql);
}

// used in saveAlertUser()
function newAlertUser($alertid, $u)
{
    global $con;
    $alertid = mysql_real_escape_string($alertid);
	if(isset($u['usr_id'])) $userid = mysql_real_escape_string($u['usr_id']);
	if(isset($u['user_name'])) $user_name = mysql_real_escape_string($u['user_name']);
	if(isset($u['user_email'])) $user_name = mysql_real_escape_string($u['user_email']);
	
	//first do server side check for no duplicates
	
	$dbCheck = "select * from alerts_users where alert_id = $alertid and usr_id = $userid";
	$dbRS = getRS($con,$dbCheck);
	if (getResultRows($dbRS) == 0) {
		//proceed with insert
	
		$sql = "INSERT INTO alerts_users (alerts_id, ";
		
		if(isset($u['usr_id']))
		{
			// account user
			$sql .= "usr_id) VALUES ('$alertid', '$userid') ";
		}
		else
		{
			// other user
			$sql .= "user_name, user_email) VALUES ('$alertid', '$user_name', '$user_email') ";
		}
		
		return execSQL($con, $sql);
	}
}

/**
 * Mark an alert as Deleted
 * @global type DB connection
 * @param type alert id
 * @return MySQL reource
 */
function deleteAlert($alert_id)
{
    global $con;
    
    $sql = "UPDATE alerts SET state = 'DELETED' WHERE id = '$alert_id'";
    return execSQL($con, $sql);
}


function deleteAlertUser($user_id)
{
    $sql = "DELETE FROM alerts_users WHERE id = '$user_id'";
    return executeSql($sql);
}

/**
 * Returns a JSON object of account manager data"
 * @param type $usr_id 
 */
function getAccountManager($am_id, $accountid=0)
{
	if ($accountid>0) {
		
		if ($am_id == 2) {
			$sql = "SELECT '".ACCOUNTS_ACCOUNT_MANAGER_2."' AS manager_title ,
				CONCAT(u2.firstname, ' ', u2.lastname) as username,
				u2.username  AS email,
				u2.userid  AS usr_id 
				FROM account ac 
				LEFT JOIN usr u2 ON ac.account_manager_2 = u2.userid
				WHERE ac.accountid = '$accountid';";
		} else {
			$sql = "SELECT '".ACCOUNTS_ACCOUNT_MANAGER."' AS manager_title ,
				CONCAT(u2.firstname, ' ', u2.lastname) as username,
				u2.username  AS email,
				u2.userid  AS usr_id 
				FROM account ac 
				LEFT JOIN usr u2 ON ac.account_manager = u2.userid
				WHERE ac.accountid = '$accountid';";
		}		
		$rs = getRecSet($sql);
		$row = getRow($rs);
		$manager_users = array('usr_id' => $row['usr_id'], 'username' => $row['username'], 'email' => $row['email']);
		$res = array("acc_manager_id" => $am_id, "manager_title" => $row['manager_title'], 
			"manager_users" => array($manager_users));
		return json_encode($res);
	} else {
		if ($am_id == 2) {
			$title = ACCOUNTS_ACCOUNT_MANAGER_2;
		} else {
			$title = ACCOUNTS_ACCOUNT_MANAGER;
		}
			
		$res = array("acc_manager_id" => $am_id, "manager_title" => $title);
		return json_encode($res);
	}
}


/**
 * adds  a new row into the alerts_acc_manager table
 *  
 */ 
function newAccountManager($alertid, $manager)
{
	global $con;
	$alertid = mysql_real_escape_string($alertid);
	$am_id  = mysql_real_escape_string($manager['acc_manager_id']);
	
	//first check db for this am/alert combination
	$dbCheck = "select * from alerts_acc_manager where alert_id = $alertid and acc_manager_id = $am_id";
	$dbRS = getRS($con, $dbCheck);
	if (getResultRows($dbRS) == 0) {
		//proceed with insert
		$sql = "INSERT INTO alerts_acc_manager (alert_id, acc_manager_id) VALUES ('$alertid', '$am_id');";
		return executeSQL($sql);
	}
}


/**
 * deletes a row from the alerts_acc_manager table
 * @param type $alerts_acc_manager_id 
 */ 
function deleteAlertAccountManager($alerts_acc_manager_id)
{
    $sql = "DELETE FROM alerts_acc_manager WHERE id = '$alerts_acc_manager_id'";
    return executeSql($sql);
}
/**
 * Returns a JSON object of user data"
 * @param type $usr_id 
 */
function getAccountUser($usr_id)
{
    $sql = "SELECT userid AS usr_id, CONCAT(firstname, ' ', lastname) AS user_name, username AS user_email FROM usr WHERE userid = '$usr_id';";
    
    $res = getRecSet($sql);
    return json_encode(getRowAssoc($res));
}


function getAccountsAlerts($type)
{
    global $con;
    
    $qry = "SELECT a.school_dfe, a.accountid, a.accountname, a.address1, a.town, a.postcode, a.state, max(end_date) AS licence_end, (SELECT COUNT(*) FROM alerts AS at WHERE at.state = 'OK' AND at.alert_type = '".$type."'  AND at.account_id = a.accountid) AS alert_count 
    FROM account AS a
    LEFT JOIN accountlicence AS al ON a.accountid = al.accountid 
    WHERE a.state != 'DELETED'
    GROUP BY a.school_dfe, a.accountid, a.accountname, a.address1, a.town, a.postcode, a.state 
    ORDER BY a.accountname;";
    
    return getRS($con, $qry);
}

/**
 * return a jobrole JSON object
 * @param int $role_id
 * @return string JSON  
 */
function getJobrole($role_id, $acc_id = 0)
{	
    $users = array();
    if ($acc_id == 0) 
    {
        $sql = "SELECT r.role_id, r.role_title, r.role_desc FROM core_jobrole AS r WHERE r.role_id = '" . $role_id . "';";

        $rec = getRecSet($sql);
        while ($row = getRowAssoc($rec)) 
        {
            $roles['role_id'] = $row['role_id'];
            $roles['role_title'] = $row['role_title'];
            $roles['role_desc'] = $row['role_desc'];
        }
    }
    else
    {
				
        $sql = "SELECT r.role_id, r.role_title, r.role_desc, CONCAT(u.firstname, ' ', u.lastname) AS user 
        FROM core_jobrole AS r 
        LEFT JOIN core_usr_jobrole AS uj
            ON uj.jobrole_id = r.role_id
        LEFT JOIN usr AS u
            ON u.userid = uj.usr_id  
        WHERE r.role_id = '" . $role_id . "' and u.accountid = $acc_id;";
        
        $rec = getRecSet($sql);
        
        $users = array();
        
		if (getResultRows($rec) > 0) {
		
			$row = getRowAssoc($rec);
			
			$roles['role_id'] = $row['role_id'];
			$roles['role_title'] = $row['role_title'];
			$roles['role_desc'] = $row['role_desc'];
			if ($row['user'] != '')
				$users[] = array('user_name' => $row['user']);
			

			$roles['user'] = $users;
		}  else {
			//there are no users with job role and accountid, so get job role details
			$sql = "SELECT r.role_id, r.role_title, r.role_desc FROM core_jobrole AS r WHERE r.role_id = '" . $role_id . "';";
			$rec = getRecSet($sql);
			$row = getRowAssoc($rec);
			$roles['role_id'] = $row['role_id'];
			$roles['role_title'] = $row['role_title'];
			$roles['role_desc'] = $row['role_desc'];    
		
		}		
    }
		
    return json_encode($roles);
}

//
function editJobrole($role)
{
    $sql = "UPDATE alerts_users SET jobrole_id = '".  mysql_real_escape_string($role['role_id'])."'WHERE id = '".$role['alertuser_id']."' ;";
    
    return executeSQL($sql);
}

// add a new jobrole to the table
function newJobrole($alertid, $role)
{
	global $con;
	$alertid = mysql_real_escape_string($alertid);
	$role_id  = mysql_real_escape_string($role['role_id']);
	
	//first check db for this jobrole/alert combination
	$dbCheck = "select * from alerts_jobrole where alert_id = $alert_id and jobrole_id = $role_id";
	$dbRS = getRS($con, $dbCheck);
	if (getResultRows($dbRS) == 0) {
		//proceed with insert
		$sql = "INSERT INTO alerts_jobrole (alert_id, jobrole_id) VALUES ('$alertid', '$role_id');";
		return executeSQL($sql);
	}
}

function deleteJobrole($alertuser_id)
{
    $sql = "DELETE FROM alerts_jobrole WHERE id = '".  mysql_real_escape_string($alertuser_id)."';";
    
    return executeSQL($sql);
}

