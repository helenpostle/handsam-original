<?php
	function getWitness($con, $pageVars) {
		$qry = "select acc_witness.witness_statement, acc_witness.witness_id, acc_witness.userid, acc_witness.assoc_userid, acc_witness.witness_usertype, ";
		
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.firstname ELSE u.firstname END AS firstname, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.lastname ELSE u.lastname END AS lastname, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.address1 ELSE u.address1 END AS address1, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.address2 ELSE u.address2 END AS address2, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.address3 ELSE u.address3 END AS address3, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.city ELSE u.city END AS city, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.postcode ELSE u.postcode END AS postcode, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.tel1 ELSE u.tel1 END AS tel1, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.tel2 ELSE u.tel2 END AS tel2, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.email ELSE u.email END AS email, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.sex ELSE u.sex END AS sex, ";
		$qry .= "CASE WHEN acc_witness.witness_usertype=2 THEN a.age ELSE u.age END AS age ";
		
		
		
		$qry .= " from acc_witness left join acc_accident on acc_witness.accidentid = acc_accident.accidentid ";
		
		$qry .= " left join usr u on acc_witness.userid = u.userid ";
		$qry .= " left join usr_assoc a on acc_witness.assoc_userid = a.assoc_userid ";

		
		
		$qry .= " where acc_witness.accidentid = ".$pageVars->accidentid;
		if (!isAllowed("editAccount")) $qry .= " and acc_accident.accountid = ".$pageVars->accountid;	
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList();
		return $pageDb;
	}


	
	
/*functions for saving  first aider details in accident book*/

	//save first Aider details
	
	function saveWitness($con, $pageVars, $clsUS) 
	{
		$return = array();
		if ($pageVars->accidentid > 0) 
		{
                    if(isset($_POST['witness_id']) && count($_POST['witness_id'] > 0) 
                            && (!isset($_POST['no_witness']) || $_POST['no_witness'] != 1 )) // if no_witness is ticked
			{
				for($i=0; $i<count($_POST['witness_id']); $i++) 
				{
					if (isset($_POST["witness_userid"][$i]) && isset($_POST["witness_usertype"][$i]) && $_POST["witness_userid"][$i] > 0 && $_POST["witness_usertype"][$i] > 0) 
					{
					    	if ($_POST['witness_id'][$i] == 0) {
							$qry = new dbInsert("acc_witness");
							$qry->setReqNumberVal("accidentid",$pageVars->accidentid,"Accidentid");
						} else {
							$qry = new dbUpdate("acc_witness");
							$qry->setParam("witness_id", $_POST['witness_id'][$i]);
						}
						
						$witness_num = $i + 1;
						if ($_POST["witness_usertype"][$i] == 1) {
							$qry->setReqNumberVal("userid",$_POST["witness_userid"][$i],"Witness $witness_num userid");
							$qry->setNullVal("assoc_userid");
						} else if ($_POST["witness_usertype"][$i] == 2) {
							$qry->setReqNumberVal("assoc_userid",$_POST["witness_userid"][$i],"Witness $witness_num userid");
							$qry->setNullVal("userid");
						}
						$qry->setReqNumberVal("witness_usertype",$_POST["witness_usertype"][$i],"witness $witness_num usertype");
						$qry->setStringVal("witness_statement",$_POST["witness_statement"][$i],"witness $witness_num Comments");
						$qry->setAudit($clsUS->userid);
						if ($qry->execute($con)) {
							//$return[] = "";
						} else {
							$return[] = $qry->getError();
						}
					}
		
				}
			} else {
				$err = delAllWitness($con, $pageVars, $clsUS);
				if ($err != "") $return[] = $err;
			}
		}
		return $return;

	}
	
	
	//delete first aiders that have the hidden delete field posted
	
	function delWitness($con, $pageVars, $clsUS) {
		$return = array();
		if (isset($_POST['del_witness_id'])) {
			for($i=0; $i<count($_POST['del_witness_id']); $i++) {
				
				if ($_POST['del_witness_id'][$i] != "") {
					$del_id = $_POST['del_witness_id'][$i];
					settype($del_id,"integer");
					$qry = new dbDelete("acc_witness");
					$qry->setParam("witness_id", $del_id);
					//now check that this is a first aider only linked to this accident
					$qry->setParam("accidentid", $pageVars->accidentid);
					$injury_num = $i + 1;
					if ($qry->execute($con)) {
						//$return[] = "";
					} else {
						$return[] = "Error deleting Witness $injury_num ".$qry->getError();
					}
				}
		
			}
		}
		return $return;

	}	

	
	function delAllWitness($con, $pageVars, $clsUS) {
		$return = "";
		$qry = new dbDelete("acc_witness");
		$qry->setParam("accidentid", $pageVars->accidentid);
		if ($qry->execute($con)) {
			//$return[] = "";
		} else {
			$return = "Error deleting witnesses ".$qry->getError();
		}
		return $return;
	}	
	
	

	#creates an array of witnesses, either form the db query for this accident, or from the post for this form if it exists
	function getWitnessArr($witDB, $pageVars, $con) {
		
		if (isset($_POST['witness_id'])) {
			$wit = array();
			for ($i = 0; $i < (count($_POST['witness_id'])); $i++) {
				$wit[$i]['witness_id'] = $_POST['witness_id'][$i];
				$wit[$i]['witness_userid'] = $_POST['witness_userid'][$i];
				$wit[$i]['witness_num'] = $_POST['witness_num'][$i];
				if (isset($_POST['witness_usertype'][$i])) $wit[$i]['witness_usertype'] = $_POST['witness_usertype'][$i];
				$wit[$i]['witness_statement'] = $_POST['witness_statement'][$i];
				//now get usr details foir thius witness
				if (isset($_POST['witness_usertype'][$i]) && $_POST['witness_usertype'][$i] == 1) {
					$qry  = " select * from usr where userid = {$_POST['witness_userid'][$i]} and accountid = {$pageVars->accountid}";
					$rs = getRS($con,$qry);
					$row = getRow($rs);				
				} else if ($_POST['witness_userid'][$i] > 0) {
					$qry  = " select * from usr_assoc where assoc_userid = {$_POST['witness_userid'][$i]} and accountid = {$pageVars->accountid}";
					$rs = getRS($con,$qry);
					$row = getRow($rs);				
				}
				
				if (isset($row)) {
					$wit[$i]['firstname'] = $row['firstname'];
					$wit[$i]['lastname'] = $row['lastname'];
					$wit[$i]['address1'] = $row['address1'];
					$wit[$i]['address2'] = $row['address2'];
					$wit[$i]['address3'] = $row['address3'];
					$wit[$i]['city'] = $row['city'];
					$wit[$i]['postcode'] = $row['postcode'];
					$wit[$i]['tel1'] = $row['tel1'];
					$wit[$i]['tel2'] = $row['tel2'];
					
					$wit[$i]['age'] = $row['age'];
					$wit[$i]['sex'] = $row['sex'];
				}
			}
		} else {
			$wit = $witDB;
		}
		return $wit;
	}	
        
function getNoWitness($id)
{
    global $con;
    
    $sql = "SELECT no_witness FROM acc_investigation WHERE id = $id;";
    
}