<?php
class accTags  {

	var $siteid;
	var $item_tags = array();
	var $messages = array();
	var $tags_str = "";
	var $delete_tags;
	var $add_tags;
    
    
    function accTags($siteid, $delete_tags=true, $add_tags=true) {
	    $this->siteid = $siteid;
	    $this->delete_tags = $delete_tags;
	    $this->add_tags = $add_tags;
	    
	    
    }
    
    
    function deleteSiteTags($con) {
	    foreach ($this->item_tags as $tagid => $tag ) {
		    
		    //delete from acc_site_tags
			$qry = new dbDelete("acc_site_tags");
			$qry->setParam("siteid",$this->siteid);
			$qry->setParam("tagid",$tagid);
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			}
		    		    
	    	//Now count if theres any more in qgp_site_tags
	    	$rsCheck = getRS($con,"select tag_siteid from acc_site_tags where tagid = $tagid");
			//check if tag exists
			if ($this->delete_tags && getResultRows($rsCheck) == 0) {
				//delete from tags table
				$del_qry = new dbDelete("acc_tags");
				$del_qry->setParam("tagid",$tagid);
				if (!$del_qry->execute($con)) {
					$this->messages[] = $del_qry->getError();
				}
			}
	    }
    }
    
    
    function insertSiteTag($tagid, $con) {
	   	$rsCheck = getRS($con,"select tag_siteid from acc_site_tags where tagid = $tagid and siteid = $this->siteid");

		if (getResultRows($rsCheck) == 0) {
			//insert tag
			$qry = new dbInsert("acc_site_tags");
			$qry->setReqNumberVal("tagid",$tagid,"tagid");
			$qry->setReqNumberVal("siteid",$this->siteid, "siteid");
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			}

		}
    }
    
    
    
    function insertTag($tag, $clsUS, $con) {
	    $tag = trim($tag);
		//check if tag already exists
	   	$rsCheck = getRS($con,"select tagid from acc_tags where tag = '$tag'");
		//check if tag exists
		if ($this->add_tags && getResultRows($rsCheck) == 0) {
			//insert tag
			$qry = new dbInsert("acc_tags");
			$qry->setReqStringVal("tag",$tag, "tag");
			$qry->setAudit($clsUS->userid);
			if (!$qry->execute($con)) {
				$this->messages[] = $qry->getError();
			} else {
				//return new tagid
				return $qry->getNewID();
			}
		} else if(getResultRows($rsCheck) > 0) {
		    $row = getRow($rsCheck);
			//return existing tagid
       		return $row['tagid'];
		} else {
			return 0;
		}
    }
    
    
    function saveTags($tag_str, $clsUS, $con) {
	    $this->tags_str = $tag_str;
	    //first get an array with all current tags for this item
	    $this->getTags($con);
	    //var_dump ($this->item_tags);
	    
		$tags = array();
		$tags = explode(",", $tag_str);
		$tags = array_filter($tags);
		//var_dump ($tags);
		foreach ($tags as $tag) {
			if (in_array($tag, $this->item_tags)) {
				//remove from array
				$this->item_tags = array_diff($this->item_tags, array($tag));
			} else {
				//insert tag
				$tagid = $this->insertTag($tag, $clsUS, $con);
				if ($tagid > 0) $this->insertSiteTag($tagid, $con);
			}
		}
		
		//now delete tags left in the item_tags array
		$this->deleteSiteTags($con); 

    }
    
    function delTags($con) {
	    //first get an array with all current tags for this item
	    $this->getTags($con);
		//now delete tags left in the item_tags array
		$this->deleteSiteTags($con); 
    }
    
    
    function getTags($con) {
	    if (count($this->item_tags) == 0) {
		    $qry = "select * from acc_tags left join acc_site_tags on acc_tags.tagid = acc_site_tags.tagid where acc_site_tags.siteid = {$this->siteid} order by acc_tags.tagid asc"; 
	  	    $rs = getRS($con,$qry);
	       	while($row = getRow($rs)) {
				$this->item_tags[$row['tagid']] = $row['tag'];
	       	}
       	}
    }
     
    function getTagsStr($con) {
	    $this->getTags($con);
	    return implode(",", $this->item_tags);
    }
    
 
    function getTagsLi($con) {
	    $this->getTags($con);
	    return "<li>".implode("<li/><li>", $this->item_tags)."</li>";
    }
  
     
}
?>