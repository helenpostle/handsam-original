<?php
/**
 * Send an alert.
 * 
 * $type is a lowercase string, either 'riddor', 'death', 'majinj'.
 * 
 * @global obj $pageVars
 * @global string $alertsMail
 * @global string $rootPath
 * @global string $rootUrl
 * @param string $type 
 */
function send_incident_alert($type, $retract = false)
{
    global $clsUS, $pageVars, $alertsMail, $rootPath, $rootUrl, $messages, $con;

    // make sure it's lowercase and what we want
    $type = strtolower($type);
    if(!in_array($type, array('riddor', 'death', 'majinj'))) return;
	
	switch ($type) {
		case 'death':
		$sbjct = 'Incident Alert: Death';
		// list of recipients	
		$to = getTriggerAlertRecipients('major', $pageVars->accountid);
		break;
		case 'majinj':
		$sbjct = 'Incident Alert: Major Injury';
		// list of recipients	
		$to = getTriggerAlertRecipients('major', $pageVars->accountid);
		break;
		case 'riddor':
		$sbjct = 'Incident Alert: Riddor';
		// list of recipients	
		$to = getTriggerAlertRecipients($type, $pageVars->accountid);
		break;
		default:
		$sbjct = 'Incident Alert: $type';
		// list of recipients	
		$to = getTriggerAlertRecipients($type, $pageVars->accountid);

	}
	

    
    // amend the $type value to send the retract email
    if($retract == true) $type = $type .'-retract';
    $filename = $rootPath."/modules/accidents/content/email/email.".$type.".account.php";
    $email_body = getEmailContent($filename);
 
    if($pageVars->injured_usertype == 1)
        $victim = new User($pageVars->injuredid); // account user
    if($pageVars->injured_usertype == 2)
        $victim = new Assocuser($pageVars->injuredid); // assoc user
    
    $inc_victim = $victim->firstname.' '.$victim->lastname;
    
    $account = new Account($pageVars->accountid);
    
    $link = $rootUrl.'/modules/accidents/accident.php?accountid='.$pageVars->accountid.'&accidentid='.$pageVars->accidentid;
    $link = "<a href=\"$link\">$link</a>";
    // the person who filed the report
    $reporting_user = new User($clsUS->userid);
    $reporter = $reporting_user->firstname.' '.$reporting_user->lastname;
    
    foreach($to['account'] as $rec)
    {
        $email_body_inst = sprintf($email_body, $rec['name'], $account->accountname, $_POST['accident_time'], $inc_victim, $link);
		if (sendHtmlMail($rec['email'], $alertsMail, $sbjct, nl2br($email_body_inst), true)) {
			logAlert($rec['userid'], $_POST['accidentid'], $rec['alert_id'], "acc_".$type, $con);
			doLog("INFO","Username: ".$rec['email']." :: userid: ".$rec['userid']." :: alertid: ".$rec['alert_id']." :: incidentid: ".$_POST['accidentid']);
			if (isAllowed("handsamOnly")  && $clsUS->userid == 1) {
				//display this for testing purposes for handsam admin
				$messages[] = "<ul><li>$sbjct</li><li>To: ".$rec['email']." (".$rec['userid'].")</li><li>From: $alertsMail</li><li>$email_body_inst</li></ul>";
			}
		} else {
			doLog("INFO","Username: ".$rec['email']." :: userid: ".$rec['userid']." :: alertid: ".$rec['alert_id']." :: incidentid: ".$_POST['accidentid']." :: $type ALERT EMAIL FAILED");
			if (isAllowed("handsamOnly")  && $clsUS->userid == 1) {
				//display this for testing purposes for handsam admin
				$messages[] = "<ul><li>ALERT EMAIL FAILED</li><li>$sbjct</li><li>To: ".$rec['email']." (".$rec['userid'].")</li><li>From: $alertsMail</li><li>$email_body_inst</li></ul>";
			}
		}
    }
    
    $filename = $rootPath."/modules/accidents/content/email/email.".$type.".nonaccount.php";
    $email_body = getEmailContent($filename);
    
    foreach($to['other'] as $rec)
    {
        $email_body_inst = sprintf($email_body, $rec['name'], $account->accountname, $_POST['accident_time'], $reporter);
		if (sendHtmlMail($rec['email'], $alertsMail, $sbjct, nl2br($email_body_inst), true)) {
			logAlert($rec['userid'], $_POST['accidentid'], $rec['alert_id'], "acc_".$type, $con);
			doLog("INFO","Username: ".$rec['email']." :: userid: ".$rec['userid']." :: alertid: ".$rec['alert_id']." :: incidentid: ".$_POST['accidentid']);
			if (isAllowed("handsamOnly")  && $clsUS->userid == 1) {
				//display this for testing purposes for handsam admin
				$messages[] = "<ul><li>$sbjct</li><li>To: ".$rec['email']." (".$rec['userid'].")</li><li>$email_body_inst</li></ul>";
			}
		} else {
			doLog("INFO","Username: ".$rec['email']." :: userid: ".$rec['userid']." :: alertid: ".$rec['alert_id']." :: incidentid: ".$_POST['accidentid']." :: $type ALERT EMAIL FAILED");
			if (isAllowed("handsamOnly")  && $clsUS->userid == 1) {
				//display this for testing purposes for handsam admin
				$messages[] = "<ul><li>ALERT EMAIL FAILED</li><li>$sbjct</li><li>To: ".$rec['email']." (".$rec['userid'].")</li><li>$email_body_inst</li></ul>";
			}
		}
    }
}

/**
 * Return an array of investigations, overdue by at least $a $i with accountid = $aid
 *  
 */
function getOverdueInvs($a, $u, $aid=0)
{
	switch ($u) {
		case  "days":
		$units = "DAY";
		break;
		
		case  "weeks":
		$units = "WEEK";
		break;
		
		default:
		$units = "DAY";
		break;
	}
	$a_qry = "";
	if ($aid > 0) $a_qry = " and acc_investigation.accountid = $aid ";
    $ret = array();
    $sql = "SELECT concat('Overdue Investigation  - account: ',account.accountname, ' - Internal investigator: ', usr.firstname, ' ', usr.lastname, ' - Incident Id: ', acc_accident.accidentid) as title, acc_investigation.*, account.accountname, usr.firstname, usr.lastname, usr.username FROM acc_investigation left join acc_accident on acc_investigation.accidentid = acc_accident.accidentid right join usr on acc_accident.int_investigator_userid = usr.userid left join account on acc_accident.accountid = account.accountid WHERE now() > DATE_ADD(acc_investigation.created, INTERVAL $a $units) and acc_investigation.state != 'COMPLETE' $a_qry;";
	$res = getRecSet($sql);
	while($row = getRowAssoc($res))
	{
       $ret[$row['accidentid']] = $row;
	}
   
    return $ret;
}

/**
 * check that the time since the investigation was opened
 * is more than the alert time and less than alert + 1.
 * As this is checked once a day, each alert will only be sent out once.
 * e.g. alert is 10 days, so check that time passed is more than 10d and less than 11d. 
 * 
 * @param array investigation 
 * @param array alert 
 */
 /*
function isInvLate($inv, $alert)
{
    // our base date
    $startDate = new DateTime($inv['created']);
    
    // now
    $nowDate = new DateTime;
    
    // get the min and max values
    $minDate = $startDate->add(new DateInterval('P'.$alert['amount'].'D'));
    $maxDate = clone $minDate;
    $maxDate->add(new DateInterval('P1D'));
    
    // return true if NOW is between the min and max
    return ($nowDate > $minDate && $nowDate < $maxDate);
}
*/
/**
 * Sends an alert to each user 
 */
function sendAlert($alert, $incident)
{
    $link = ''; // @todo create email link;
    // for each user, send an alert
    // @todo can be extended to contain user-conigured alert
    // @todo check for account vs global alerts
    
    foreach($alert as $a)
    {
        $email['subject'] = "Incident Investigation overdue";
        
        // @todo get improvemnet on message content.
        // @todo get msg frm config, different for non a/c users
        $email['msg'] = "Dear ".$name;
        $email['msg'] .= "\r\nThe investigation into Incident XX is now ".$a['amount']." days overdue. ";
        $email['msg'] .= "\r\nYou can view the investigation page here: ".$link;
        $email['msg'] .= "\r\nThis is an automated message, please do not reply.";
        
        // send to each user
        foreach($a['users'] as $u)
        {
            if(is_null($u['usr_id']))
            {
			// get user details from db
            }
            else
            {
                // we already have name and email in $u
            }
            var_dump ($a);
			echo "<br/><br/>";
            // @todo send the message
            
            logAlertEmail();
        }        
    }
}

/**
 * Logs that an alert has been sent to the user 
 * @param type $alert
 * @param type $incident 
 */
function logAlertEmail($alert, $incident)
{
    // @todo log the message to the DB
}

/**
 * Gets all schools for this account, and the number of alerts for each school
 * @return mysql object/recordset
 */
 
 
 /**
 * Gets all recipients for a Trigger alert type. 
 * 
 * Returns an array of name/email pairs
 * 
 * @param string $alert_type
 * @return array
 */
function getTriggerAlertRecipients($alert_type, $accountid)
{
    // account holders' emails and other users' emails
    $emails = array('account' => array(), 'other' => array());
	
	//first need to get alertid fro this alert type:
	$sql = "select id from alerts where alert_type = '".$alert_type."' and state = 'OK'";
	
	$res = getRecSet($sql);
	$row = getRowAssoc($res);
	$alert_id = $row['id'];
	
    // get all from alerts where type = $alert_type
    // return comma seperated list of email addresses
    $sql = "SELECT u.userid, a.id, au.user_name AS other_name, au.user_email AS other_email, CONCAT(u.firstname, ' ', u.lastname) AS account_name, u.username AS account_email
            FROM alerts AS a
            JOIN alerts_users AS au
                ON a.id = au.alerts_id
            LEFT JOIN usr AS u
                ON au.usr_id = u.userid
			LEFT JOIN account as ac
				on u.accountid = ac.accountid 
            WHERE a.alert_type = '".$alert_type."'  and a.state = 'OK' and u.state='ACTIVE' and ac.state='ACTIVE'
        ;";
    $res = getRecSet($sql);
    
    while($row = getRowAssoc($res))
    {
        if(is_null($row['other_email'])) $emails['account'][$row['userid']] = array('name' => $row['account_name'], 'email' => $row['account_email']);
        elseif(is_null($row['account_email'])) $emails['other'][] = array('name' => '', 'email' => $row['other_email']);
    }
    
    // jobrole users
	$sql = "SELECT CONCAT(u.firstname, ' ', u.lastname) AS fullname, u.username AS email, u.userid 
            FROM alerts_jobrole AS aj 
            LEFT JOIN core_jobrole AS j
                ON aj.jobrole_id = j.role_id
            LEFT JOIN core_usr_jobrole AS uj
                ON j.role_id = uj.jobrole_id
            LEFT JOIN usr AS u
                ON uj.usr_id = u.userid
            WHERE aj.alert_id = $alert_id AND u.accountid = $accountid";
   $res = getRecSet($sql);
    
    while($row = getRowAssoc($res))
        $emails['account'][$row['userid']] = array('name' => $row['fullname'], 'email' => $row['email'], 'userid' => $row['userid'], 'alert_id' => $alert_id);
    
    // return a comma seperated string of emails
	
	//account managers
	$sql = "SELECT 
		CASE acc_manager_id WHEN 2 THEN CONCAT(u2.firstname, ' ', u2.lastname)  ELSE CONCAT(u1.firstname, ' ', u1.lastname) END AS fullname ,
		CASE acc_manager_id WHEN 2 THEN u2.username  ELSE u1.username END AS email,
		CASE acc_manager_id WHEN 2 THEN u2.userid  ELSE u1.userid END AS userid 
		FROM alerts_acc_manager am 
		LEFT JOIN alerts a ON am.alert_id = a.id 
		LEFT JOIN account ac ON $accountid = ac.accountid
		LEFT JOIN usr u1 ON ac.account_manager = u1.userid
		LEFT JOIN usr u2 ON ac.account_manager_2 = u2.userid
		WHERE am.alert_id = $alert_id";
	$res = getRecSet($sql);    	
    while($row = getRowAssoc($res))
        $emails['account'][$row['userid']] = array('name' => $row['fullname'], 'email' => $row['email'], 'userid' => $row['userid'], 'alert_id' => $alert_id);	
    return $emails;
}




function getEmailContent($filename)
{
    $handle = fopen($filename, "r");
    $email_body = fread($handle, filesize($filename));
    fclose($handle);
    
    return $email_body;
}
/**
 * Create a trigger alert
 * @param string alert type 
 */
function createTriggerAlert($alert_type)
{
    $sql = "INSERT INTO alerts (alert_type) VALUES ('".$alert_type."');";
    
    executeSql($sql);
}



function logAlert($userid, $itemid, $alertid, $type, $con) {
	$qry = new dbInsert("alert_log");
	$qry->setReqNumberVal("alertid",$alertid,"alertid");
	$qry->setReqNumberVal("userid",$userid,"userid");
	$qry->setReqNumberVal("itemid",$itemid,"itemid");
	$qry->setReqNumberVal("success",1,"success");
	$qry->setReqStringVal("alert_type",$type,"type");
	$qry->addValue("created","NOW()");
	if (!$qry->execute($con)) {
		return $qry->getError()." :: ".$qry->getSql();
	}
	return "";
}

 ?>