<?php

/*
 *save complate accident form functions
 *
 */
	// function to save a draft form with few required fields
	function saveCompleteForm($con, $pageVars, $clsUS, $formDB) {
		global $pageVars, $accidents_conf;
		if ($pageVars->accidentid == 0) {
			$qry = new dbInsert("acc_accident");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
		} else {
			$qry = new dbUpdate("acc_accident");
			$qry->setParam("accidentid", $pageVars->accidentid);
		}
		
		//Common to all accident types
		//personal details
		if ($pageVars->injuredid === 0) $pageVars->injuredid = "";
		if ($pageVars->injured_usertype == 1) {
			$qry->setReqNumberVal("userid",$pageVars->injuredid,"Injured Party", "Please enter the Injured Party");
			$qry->setNullVal("assoc_userid");
		} else if($pageVars->injured_usertype == 2) {
			$qry->setReqNumberVal("assoc_userid",$pageVars->injuredid,"Injured Party", "Please enter the Injured Party");
			$qry->setNullVal("userid");
		} else {
			$qry->setReqNumberVal("userid",$pageVars->injuredid,"Injured Party", "Please enter the Injured Party");
		}
		
		$qry->setReqNumberVal("injured_usertype",$pageVars->injured_usertype,"Injured user type");

		$qry->setReqStringVal("status",$_POST["status"],"Status");
		if (isset($_POST["status_other"])) {
			$qry->setReqStringVal("status_other",$_POST["status_other"],"Status - Other");
		} else {
			$qry->setNullVal("status_other");
		}
		
		//year group if status == student
		if (isset($_POST["year_group"])) {
			$guardian_notified = 0;
			if (isset($_POST['guardian_notified'])) $guardian_notified = $_POST['guardian_notified'];
			
			$qry->setBooleanVal("guardian_notified",$guardian_notified,"Parent/carer notified");
			$qry->setReqNumberVal("year_group",$_POST["year_group"],"Year Group");
			$qry->setDateVal("guardian_notified_date",$_POST["guardian_notified_date"],"Parent/carer date notified");
		} else {
			$qry->setNullVal("year_group");
			$qry->setNullVal("guardian_notified");
			$qry->setNullVal("guardian_notified_date");
		}
		
		//accident time details
		$acc_date = $_POST['accident_time'];
		$hrs = $_POST["accident_time_hrs"];
		$hrs = sprintf("%02d", $hrs);
		$mins = $_POST["accident_time_mins"];
		$mins = sprintf("%02d", $mins);
		$acc_date .= " {$hrs}:{$mins}:00";
		$qry->setReqDateTimeVal("accident_time",$acc_date,"Accident Time and Date");
		
		//accident site details
		if ($pageVars->siteid === 0) $pageVars->siteid = "";
		$qry->setReqNumberVal("siteid",$pageVars->siteid,"Site Details");

		//only save internal investigator if the investigation report is still in progress
		$inv_qry = "select state from acc_investigation where accidentid = ".$pageVars->accidentid;
		$inv_rs = getRS($con, $inv_qry);
		$inv_row = getRow($inv_rs);
		
		if ($inv_row['state'] != 'COMPLETE') {
            if ($accidents_conf['investigation_optional'] == true) {
                $qry->setNumberVal("investigation_required", $_POST['investigation_required'], "Investigation required");
            }
            if ($accidents_conf['investigation_optional'] == true && isset($_POST['investigation_required']) && $_POST['investigation_required'] == 0 && $formDB['inv_state'] == null) {
            $qry->setNullVal("int_investigator_userid");
        } else if (!isset($accidents_conf['investigation_optional']) || (isset($accidents_conf['investigation_optional']) && $accidents_conf['investigation_optional'] == false) || (isset($_POST['investigation_required']) && $_POST['investigation_required'] == 1)) {
            $qry->setReqNumberVal("int_investigator_userid", $_POST['int_investigator_userid'], "Internal Investigator");
        } else if($formDB['inv_state'] == null) {
            $qry->setNullVal("int_investigator_userid");
        }
		}        
		
		//riddor
		if (isset($_POST["riddor"])) {
			$qry->setReqStringVal("riddor",$_POST["riddor"],"Riddor");
			if ($_POST["riddor"] == 'RIDDOR') {
				if (isset($_POST["riddor_ref"])) {
					$qry->setReqStringVal("riddor_ref",$_POST["riddor_ref"],"Riddor ref");
				} else {
					$qry->setReqStringVal("riddor_ref","","Riddor_ref");
				}
			}
		} else {
			$qry->setReqStringVal("riddor","","Riddor");
		}
		
		//accident description
		$qry->setReqStringVal("accident_desc",$_POST["accident_desc"],"Incident Description");

		//accident type specific 
		if (isset($_POST["accident_type"])) {
			switch ($_POST["accident_type"]) {
				case "ACCIDENT":
					$qry->setReqStringVal("accident_type",$_POST["accident_type"],"Accident Type");
					$qry = save_complete_accident($qry, $pageVars);
					break;
				case "NEAR MISS":
					$qry->setReqStringVal("accident_type",$_POST["accident_type"],"Accident Type");
					$qry = save_complete_nearmiss($qry);
					break;
				case "ILLNESS":
					$qry->setReqStringVal("accident_type",$_POST["accident_type"],"Accident Type");
					$qry = save_complete_illness($qry);
					break;
				case "RESTRAINT":
					$qry->setReqStringVal("accident_type",$_POST["accident_type"],"Accident Type");
					$qry = save_complete_restraint($qry);
					break;
				default:
					$qry->setReqStringVal("accident_type","","Accident Type");
			}		
		} else {
			$qry->setReqStringVal("accident_type","","Accident Type");
		}
	
		$qry->setReqStringVal("state","COMPLETE","State");               
		$qry->setAudit($clsUS->userid);
		$qry->getSql();
		if ($qry->execute($con)) {
			if ($pageVars->accidentid == 0) {
				$pageVars->accidentid = $qry->getNewID();	
			}
			return "";
		} else {
			return $qry->getError();
		}
	}
	
	//complete near miss
	function save_complete_nearmiss($qry) {
		$qry->setNullVal("first_aid");
		$qry->setNullVal("injury_Q1");
		$qry->setNullVal("injury_Q2");
		$qry->setNullVal("first_aid_desc");
		$qry->setNullVal("ambulance");
		$qry->setNullVal("ambulance_userid");
		$qry->setNullVal("ambulance_caller_usertype");
		$qry->setNullVal("ambulance_time");
		$qry->setNullVal("ambulance_assoc_userid");
		$qry->setNullVal("party_injured");
		$qry->setNullVal("about_accident");
		$qry->setNullVal("fall_height");		
		return $qry;
	}

	//complete accident
	function save_complete_accident($qry, $pageVars) {
		//ambulance related fields
		$qry->setReqNumberVal("ambulance",$_POST["ambulance"],"Ambulance");
		if ($_POST["ambulance"] == 1) {
			if ($pageVars->ambulance_caller_usertype == 1) {
				$qry->setReqNumberVal("ambulance_userid",$pageVars->ambulance_caller_id,"Ambulance caller", "Please enter the ambulance caller");
				$qry->setNullVal("ambulance_assoc_userid");
			} else if($pageVars->ambulance_caller_usertype == 2) {
				$qry->setReqNumberVal("ambulance_assoc_userid",$pageVars->ambulance_caller_id,"Ambulance caller", "Please enter the ambulance caller");
				$qry->setNullVal("ambulance_userid");
			} else {
				$qry->setReqNumberVal("ambulance_userid",$pageVars->ambulance_caller_id,"Ambulance caller", "Please enter the ambulance caller");
				$qry->setReqNumberVal("ambulance_caller_usertype","","ambulance caller usertype");
			}
			$qry->setReqNumberVal("ambulance_time",$_POST["ambulance_time"],"Ambulance time", "Please enter the ambulance reponse time");
		} else {
			$qry->setNullVal("ambulance_userid");
			$qry->setNullVal("ambulance_caller_usertype");
			$qry->setNullVal("ambulance_time");
			$qry->setNullVal("ambulance_assoc_userid");
		}
		
		//injury
		$qry->setReqNumberVal("party_injured",$_POST["party_injured"],"Person Injured");
		if ($_POST["party_injured"] == 1) {
			$qry->setReqStringVal("injury_Q1",$_POST["injury_Q1"],"Was the injury");
			$qry->setReqStringVal("injury_Q2",$_POST["injury_Q2"],"Did the injured person");
			for($i=0; $i<count($_POST['injury_id']); $i++) {
				$injury_entered = false;
				if ($_POST["injury_type"][$i] != "" && $_POST["injury_part"][$i] != "" && $_POST["injury_desc"][$i] != "") {
					$injury_entered = true;
				} 
			}
			if ($injury_entered == false) $qry->failError .= "Please enter the injury details.\r\n";
		} else {
			$qry->setNullVal("injury_Q1");
			$qry->setNullVal("injury_Q2");
		}
				
		//First Aid Details		
		$qry->setReqNumberVal("first_aid",$_POST["first_aid"],"First Aid Given");
		if ($_POST["first_aid"] == 1) {
			$qry->setReqStringVal("first_aid_desc",$_POST["first_aid_desc"],"First Aid Description");
		} else {
			$qry->setNullVal("first_aid_desc");
		}
		$qry->setReqStringVal("about_accident",$_POST["about_accident"],"About the accident");
		
		//fall
		if ($_POST["about_accident"] == "fall") {
			$qry->setReqNumberVal("fall_height",$_POST["fall_height"],"Fall Height");
		} else {
			$qry->setNullVal("fall_height");
		}	
		return $qry;
	}

	
	//complete illness
	function save_complete_illness($qry) {
		$qry->setReqStringVal('paracetamol', $_POST['paracetamol'], 'Paracetamol Administered');
		$qry->setReqStringVal('sent_home', $_POST['sent_home'], 'Sent Home');
		if($_POST['sent_home'] == '1')
		{
			$qry->setReqStringVal('sent_home_contact', $_POST['sent_home_contact'], 'Sent Home Contact');
			
			// one or the other must be set
			if(isset($_POST['sent_home_associd']) && $_POST['sent_home_associd'] > 0) {
				$qry->setReqNumberVal('sent_home_associd', $_POST['sent_home_associd'], 'Sent Home Associated User', 'A person who authorised the sending home must be selected');
			} else if(isset($_POST['sent_home_userid']) && $_POST['sent_home_userid'] > 0) {
				$qry->setReqNumberVal('sent_home_userid', $_POST['sent_home_userid'], 'Sent Home Registered User', 'A person who authorised the sending home must be selected');
			} else {
				$qry->setReqNumberVal('sent_home_userid', '', 'Sent Home User', 'A person who authorised the sending home must be selected');
			}
		}
		
		// null values
		$qry->setNullVal("first_aid");
		$qry->setNullVal("injury_Q1");
		$qry->setNullVal("injury_Q2");
		$qry->setNullVal("first_aid_desc");
		$qry->setNullVal("ambulance");
		$qry->setNullVal("ambulance_userid");
		$qry->setNullVal("ambulance_caller_usertype");
		$qry->setNullVal("ambulance_time");
		$qry->setNullVal("ambulance_assoc_userid");
		$qry->setNullVal("party_injured");
		$qry->setNullVal("about_accident");
		$qry->setNullVal("fall_height");	
		return $qry;
	}

	function save_complete_restraint($qry) {
		// one or the other must be set
		if(isset($_POST['restraint_associd']) && $_POST['restraint_associd'] > 0)
		{
			$qry->setReqNumberVal('restraint_associd', $_POST['restraint_associd'], 'Restraint Associated User', 'A person must be selected as a Restrainer');
		} else if(isset($_POST['restraint_userid']) && $_POST['restraint_userid'] > 0) {
			if ($_POST['restraint_userid'] == 0) $_POST['restraint_userid'] = '';
			$qry->setReqNumberVal('restraint_userid', $_POST['restraint_userid'], 'Restraint Registered User', 'A person must be selected as a Restrainer');
		} else {
			$qry->setReqNumberVal('restraint_userid', '', 'Restraint 
			User', 'A person must be selected as a Restrainer');
		}
		
		// null values
		$qry->setNullVal("first_aid");
		$qry->setNullVal("injury_Q1");
		$qry->setNullVal("injury_Q2");
		$qry->setNullVal("first_aid_desc");
		$qry->setNullVal("ambulance");
		$qry->setNullVal("ambulance_userid");
		$qry->setNullVal("ambulance_caller_usertype");
		$qry->setNullVal("ambulance_time");
		$qry->setNullVal("ambulance_assoc_userid");
		$qry->setNullVal("party_injured");
		$qry->setNullVal("about_accident");
		$qry->setNullVal("fall_height");
		return $qry;
	}
	?>