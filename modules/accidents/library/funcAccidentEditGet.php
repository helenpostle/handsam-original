<?php
/*functions for retreiving data for editing accidents in accident book*/

	//gets an accident
	function getAccident($con, $pageVars) 
        {	
		$qry = "select acc_investigation.state as inv_state, acc_accident_site.*, acc_accident.*, year_group.year_text, acc_accident.state as report_state, acc_accident.created_by as report_created_by, "; 
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN '' ELSE u.username END AS username, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.firstname ELSE u.firstname END AS firstname, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.lastname ELSE u.lastname END AS lastname, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.address1 ELSE u.address1 END AS address1, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.address2 ELSE u.address2 END AS address2, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.address3 ELSE u.address3 END AS address3, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.city ELSE u.city END AS city, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.postcode ELSE u.postcode END AS postcode, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.tel1 ELSE u.tel1 END AS tel1, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.tel2 ELSE u.tel2 END AS tel2, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.email ELSE u.email END AS email, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.sex ELSE u.sex END AS sex, ";
		$qry .= "CASE WHEN acc_accident.injured_usertype=2 THEN a.age ELSE u.age END AS age ";
		$qry .= " from acc_accident left join usr u on acc_accident.userid = u.userid ";
		$qry .= " left join usr_assoc a on acc_accident.assoc_userid = a.assoc_userid ";
		$qry .= " left join acc_investigation  on acc_accident.accidentid = acc_investigation.accidentid ";
		$qry .= "left join acc_accident_site on acc_accident.siteid = acc_accident_site.siteid left join year_group on acc_accident.year_group = year_group.year_group_id where acc_accident.accidentid = ".$pageVars->accidentid;
		if (!isAllowed("editAccount")) $qry .= " and acc_accident.accountid = ".$pageVars->accountid;
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		return $pageDb->row;
	}

	//gets an accident without personal info of victim
	function getCopy($con, $pageVars) {
		$qry = "select 'NEW' as state, '' as injuredid, '' as userid, '' AS injured_usertype, '' AS firstname, '' AS lastname, '' as assoc_userid,  '' as status,'' as first_aid,'' as first_aidDesc,'' as status_other,'' as party_injured,'' as injury_Q1,'' as injury_Q2,'' as fall_height,'' as days_off,'' as cost,'' as accident_type, siteid, accident_time, accident_desc, 'NEW' AS report_state, '' AS report_created_by, about_accident, riddor, ambulance, precautions, investigation_required  from acc_accident where accidentid = ".$pageVars->copyid." and accountid = ".$pageVars->accountid;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem();
		return $pageDb->row;
	}
	
	function getFirstAiders($con, $pageVars) {
		$qry = "select acc_first_aider.fAcomments, acc_first_aider.first_aider_id, acc_first_aider.userid, acc_first_aider.assoc_userid, acc_first_aider.fA_usertype, ";
		
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.assoc_userid ELSE u.userid END AS aider_userid, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.firstname ELSE u.firstname END AS firstname, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.lastname ELSE u.lastname END AS lastname, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.address1 ELSE u.address1 END AS address1, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.address2 ELSE u.address2 END AS address2, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.address3 ELSE u.address3 END AS address3, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.city ELSE u.city END AS city, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.postcode ELSE u.postcode END AS postcode, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.tel1 ELSE u.tel1 END AS tel1, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.tel2 ELSE u.tel2 END AS tel2, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.email ELSE u.email END AS email, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.sex ELSE u.sex END AS sex, ";
		$qry .= "CASE WHEN acc_first_aider.fA_usertype=2 THEN a.age ELSE u.age END AS age ";
		
		
		
		$qry .= " from acc_first_aider left join acc_accident on acc_first_aider.accidentid = acc_accident.accidentid ";
		
		$qry .= " left join usr u on acc_first_aider.userid = u.userid ";
		$qry .= " left join usr_assoc a on acc_first_aider.assoc_userid = a.assoc_userid ";

		
		
		$qry .= " where acc_first_aider.accidentid = ".$pageVars->accidentid;	
		if (!isAllowed("editAccount")) $qry .= " and acc_accident.accountid = ".$pageVars->accountid;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList();
		return $pageDb;
	}


	function getInjuries($con, $pageVars) {
		$qry = "select acc_injury_details.* from acc_injury_details left join acc_accident on acc_injury_details.accidentid = acc_accident.accidentid where acc_injury_details.accidentid = ".$pageVars->accidentid;
		if (!isAllowed("editAccount")) $qry .= " and acc_accident.accountid = ".$pageVars->accountid;
		$qry .= "  order by injury_id"; 
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList();
		return $pageDb;
	}
    
    
    function getRecorderDetails($con, $userid) {
        if ($userid>0) {
            $qry = "select *, concat(firstname, ' ', lastname) as fullname from usr where userid = $userid";
            $rs = getRS($con, $qry);
            $row = getRow($rs);
            return $row;
        } else {
            return false;
        }
    }
	

?>