<?php
/*functions for listing accidents in accident book*/

//gets list of fields for a form
	function getAccidentList($pageVars, $con, $filter_array) {
		//need to include secutity here. If a planner, only view own visits
		
		//implode filter array
		$filter_qry = implode(" and ", $filter_array);
		if ($filter_qry != "") $filter_qry = " and ".$filter_qry;

		if (isAllowed("viewAccidents")) {
			$security_qry = " and accident.accountid = {$pageVars->accountid}";
		}			
		
		$qry = "SELECT acc_investigation.state as inv_state, acc_accident_site.site_name, acc_accident.*, acc_accident.state AS report_state, CASE WHEN acc_accident.injured_usertype=2 THEN '' ELSE  u.username END AS username, CASE WHEN acc_accident.injured_usertype=2 THEN a.firstname ELSE u.firstname END  AS firstname, CASE WHEN acc_accident.injured_usertype=2 THEN a.lastname ELSE u.lastname END  AS lastname from acc_accident  ";
		
		$qry .= " left join usr_assoc a on acc_accident.assoc_userid = a.assoc_userid ";
		$qry .= " left join usr u on acc_accident.userid = u.userid ";
		
		$qry .= "left join acc_accident_site on acc_accident.siteid = acc_accident_site.siteid ";
		$qry .= "left join acc_investigation  on acc_accident.accidentid = acc_investigation.accidentid ";
		
		//now can view accidents in all states that have been creared by all account users. we'll only show others in 'read only' view if they are another users' and are still IN PROGRESS
		$qry .= " where acc_accident.accountid = ".$pageVars->accountid."  $filter_qry order by accident_time";
		
		//echo $qry;
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsList();
		return $pageDb;
	}
?>