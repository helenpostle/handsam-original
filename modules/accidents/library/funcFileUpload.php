<?php
/*file upload function */
function accUploadFile($clsUS, $pageVars, $con, $to_file_path, $filetype) {
	global $messages;
	#get filename and previously uploaded filename from form
	$filename = $_FILES['filename']['name'];
	$uploaded_filename = $_POST['uploaded_filename'];
	if ($filename != "") {
		#if filename not empty then upload file
		$file_path=$_FILES['filename']['tmp_name'];
		//die($file_path);
		$upload_file = uploadFile($filename, $file_path, $to_file_path);
		
		if($upload_file) {
			//die();
			#if upload successful check for previously uploaded filename
			if ($uploaded_filename != "") {
				#delete this 
				$deleteFile = deleteFile($uploaded_filename, $to_file_path);
			}
			#now set previously uploaded filename as this uploaded filename - incase form doesn't validate then user doesn't have to re-select a new file
			$filename = $upload_file;
		} else {
			$messages[] = ERROR_FILE_UPLOAD;
			return false;
		}
			
		
	} else if ($uploaded_filename != "") {
		$filename = $uploaded_filename;
	}
	if ($pageVars->fileid == 0) {
		$qry = new dbInsert("files");
		$qry->setReqStringVal("filetype",$filetype,"Filetype");
	} else {
		$qry = new dbUpdate("files");
		$qry->setParam("fileid",$pageVars->fileid);
	}

	if (isset($qry)) {
		$qry->setReqStringVal("filename",$filename,"filename");
		$qry->setReqStringVal("title",$_POST["title"],"Title");
		//$qry->setStringVal("description",$_POST["description"],"Description");
		$qry->setReqNumberVal("accountid",$pageVars->accountid,"Account id");
		$qry->setReqNumberVal("userid",$clsUS->userid,"User id");
		//$qry->setStringVal("filecategory",$_POST["filecategory"],"File Category");
		
		$qry->setAudit($clsUS->userid);
		
		if ($qry->execute($con)) {
			if ($pageVars->fileid == 0) {
				$pageVars->fileid = $qry->getNewID();	
			}
			$messages[] = SAVED;
			return $pageVars->fileid;
			
		} else {
			$messages[] = $qry->getError();	
			echo $qry->getError();
			return false;
		}
	}
	return false;
}


function accDeleteFile($pageVars, $con, $to_file_path) {
	global $messages;
	$qry = new dbDelete("files");
	$qry->setParam("fileid",$pageVars->fileid);
	$qry->setParam("accountid", $pageVars->accountid);
	if (!$qry->execute($con)) {
		$messages[] = $qry->getError();
	}
	//$fileid = 0;
	if(deleteFile($_POST['uploaded_filename'], $to_file_path)) {
		$messages[] = DELETED;
		return $pageVars->fileid;
		

	} else {
		$messages[] = ERROR_FILE_DELETE;
		return false;
	}			
}

	//get uploads for a form .
function getUploads($con, $pageVars) {
	$sql = "SELECT af.fileid, f.filename,  f.title, '' AS url, f.filecategory as category, 'file' AS resource FROM acc_files af LEFT JOIN files f ON f.fileid = af.fileid WHERE f.accountid = {$pageVars->accountid} and filetype = 'accountIncidentDoc' and accidentid = {$pageVars->accidentid} order by title asc";					
	//echo $sql;
	$result = new  pageQry($con, $sql);
	$result->rsList();
	return $result;
}

?>