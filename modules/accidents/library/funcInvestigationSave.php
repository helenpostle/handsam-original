<?php
	//function to save a draft form with few required fields
	function saveDraftInvestigation($con, $pageVars, $clsUS) {
		GLOBAL $pageVars;
		Global $accidents_conf;
		if ($pageVars->investigation_id == 0) {
			$qry = new dbInsert("acc_investigation");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
			$qry->setReqNumberVal("accidentid",$pageVars->accidentid,"Accountid");
		} else {
			$qry = new dbUpdate("acc_investigation");
			$qry->setParam("accidentid", $pageVars->accidentid);
			$qry->setParam("investigation_id", $pageVars->investigation_id);
		}
		
                 // external investigator name
                if(isset($_POST['ext_investigator_name']) && strlen($_POST['ext_investigator_name']) > 0)
                {
                    $qry->setHtmlVal('ext_investigator_name', $_POST['ext_investigator_name']);
                }
                
		/*
		if (isset($_POST["int_investigator_userid"]) && $_POST["int_investigator_userid"] > 0) {
			$qry->setNumberVal("int_investigator_userid", $_POST['int_investigator_userid'], "Internal Investigator");
		} else {
			$qry->setNullVal("int_investigator_userid");
		}		
		
		
		if (isset($_POST["ext_investigator_userid"]) && $_POST["ext_investigator_userid"] > 0) {
			$qry->setNumberVal("ext_investigator_userid",$_POST['ext_investigator_userid'],"External investigator ");
		} else {
			$qry->setNullVal("ext_investigator_userid");
		}
		$qry->setNumberVal("ext_investigator_usertype",1,"External investigator user type");
		*/

		if (trim($_POST["days_off"]) == "") {
			$qry->setNullVal("days_off");
		} else {
			$qry->setNumberVal("days_off",$_POST["days_off"],"Days Off");
		}			
		
		if (trim($_POST['cost']) == "") {
			$qry->setNullVal("cost");
		} else {
			$qry->setNumberVal("cost",$_POST["cost"],"Cost");
		}
                
                // no witness
                $no_witness = 0;
                if(isset($_POST['no_witness']) && $_POST['no_witness'] == '1') $no_witness = 1;
                $qry->setNumberVal('no_witness', $no_witness, 'No Witness');
		
		$qry->setStringVal("causes",$_POST["causes"],"Root Causes");
		
		if ($accidents_conf['investigation_issues_fld']) $qry->setStringVal("issues_desc",$_POST["issues_desc"],"Issues");
		$qry->setStringVal("precautions",$_POST["precautions"],"Precautions");
		$qry->setStringVal("follow_up",$_POST["follow_up"],"Precautions");
		
		$qry->setReqStringVal("state","IN PROGRESS","State");
		$qry->setAudit($clsUS->userid);
		if ($qry->execute($con)) {
			if ($pageVars->investigation_id == 0) {
				$pageVars->investigation_id = $qry->getNewID();	
			}
			return "";
		} else {
			return $qry->getError();
		}
	}	
	
	
	function saveCompleteInvestigation($con, $pageVars, $clsUS) {
		GLOBAL $pageVars;
		global $accidents_conf;
		if ($pageVars->investigation_id == 0) {
			$qry = new dbInsert("acc_investigation");
			$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
			$qry->setReqNumberVal("accidentid",$pageVars->accidentid,"Accountid");
		} else {
			$qry = new dbUpdate("acc_investigation");
			$qry->setParam("accidentid", $pageVars->accidentid);
			$qry->setParam("investigation_id", $pageVars->investigation_id);
		}
				
                // external investigator name
                if(isset($_POST['ext_investigator_name']) && strlen($_POST['ext_investigator_name']) > 0)
                {
                    $qry->setHtmlVal('ext_investigator_name', $_POST['ext_investigator_name']);
                }
                /*
		if (isset($_POST["ext_investigator_userid"]) && $_POST["ext_investigator_userid"] > 0) {
			$qry->setNumberVal("ext_investigator_userid",$_POST['ext_investigator_userid'],"External investigator ");
		} else {
			$qry->setNullVal("ext_investigator_userid");
		}
		$qry->setNumberVal("ext_investigator_usertype",1,"External investigator user type");
		*/

		if (trim($_POST["days_off"]) == "") {
			$qry->setNullVal("days_off");
		} else {
			$qry->setNumberVal("days_off",$_POST["days_off"],"Days Off");
		}			
		
		if (trim($_POST['cost']) == "") {
			$qry->setNullVal("cost");
		} else {
			$qry->setNumberVal("cost",$_POST["cost"],"Cost");
		}
                
                // no witness
                $no_witness = 0;
                if(isset($_POST['no_witness']) && $_POST['no_witness'] == '1') $no_witness = 1;
                $qry->setNumberVal('no_witness', $no_witness, 'No Witness');
		
		$qry->setReqStringVal("causes",$_POST["causes"],"Root Causes");
		
		if ($accidents_conf['investigation_issues_fld']) $qry->setReqStringVal("issues_desc",$_POST["issues_desc"],"Issues");
		$qry->setReqStringVal("precautions",$_POST["precautions"],"Action's taken");
		$qry->setReqStringVal("follow_up",$_POST["follow_up"],"Follow up details");
		
		$qry->setReqStringVal("state","COMPLETE","State");
		//die($qry->getSQL());
		$qry->setAudit($clsUS->userid);
		if ($qry->execute($con)) {
			if ($pageVars->investigation_id == 0) {
				$pageVars->investigation_id = $qry->getNewID();	
			}
			return "";
		} else {
			return $qry->getError();
		}
	}		
?>