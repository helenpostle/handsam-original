<?php

/*functions for saving  injury details in accident book*/

	//save injury details
	
	function saveInjury($con, $pageVars, $clsUS) {
		$return = array();
		//echo "injury func AID={$pageVars->accidentid}<br/>";
		if ($pageVars->accidentid > 0) {
			//echo "accidentid > 0<br/>";
			if (isset($_POST['injury_id']) && count($_POST['injury_id']) > 0) {
				for($i=0; $i<count($_POST['injury_id']); $i++) {
					//echo "injury $i<br/>";
					if (trim($_POST["injury_type"][$i]) != "" || trim($_POST["injury_part"][$i]) != "" || trim($_POST["injury_desc"][$i]) != "") {
						if ($_POST['injury_id'][$i] == 0) {
							$qry = new dbInsert("acc_injury_details");
							$qry->setReqNumberVal("accidentid",$pageVars->accidentid,"Accidentid");
						} else {
							$qry = new dbUpdate("acc_injury_details");
							$qry->setParam("injury_id", $_POST['injury_id'][$i]);
						}
						$injury_num = $i + 1;
						$qry->setReqStringVal("injury_type",$_POST["injury_type"][$i],"Injury $injury_num Injury Type");
						$qry->setReqStringVal("injury_part",$_POST["injury_part"][$i],"Injury $injury_num Injury Part");
						$qry->setReqStringVal("injury_desc",$_POST["injury_desc"][$i],"Injury $injury_num Injury Description");
						$qry->setAudit($clsUS->userid);
						if ($qry->execute($con)) {
	
						//$return[] = "";
						} else {
							$return[] = $qry->getError();
						}
					}
			
				}
			} else {
				$err = delAllInjuries($con, $pageVars, $clsUS);
				if ($err != "") $return[] = $err;
			}
		}
		return $return;

	}
	
	
	
	//delete injuries that have the hidden delete field posted
	
	function delInjury($con, $pageVars, $clsUS) {
		$return = array();
		if (isset($_POST['del_injury_id'])) {
			for($i=0; $i<count($_POST['del_injury_id']); $i++) {

				if ($_POST['del_injury_id'][$i] != "") {
					$del_id = $_POST['del_injury_id'][$i];
					settype($del_id,"integer");
					$qry = new dbDelete("acc_injury_details");
					$qry->setParam("injury_id", $del_id);
					$qry->setParam("accidentid", $pageVars->accidentid);
					$injury_num = $i + 1;
					if ($qry->execute($con)) {
						//$return[] = "";
					} else {
						$return[] = "Error deleting Injury $injury_num ".$qry->getError();
					}
				}
		
			}
		}
		return $return;

	}
	
	function delAllInjuries($con, $pageVars, $clsUS) {
		$return = "";
		$qry = new dbDelete("acc_injury_details");
		$qry->setParam("accidentid", $pageVars->accidentid);
		if ($qry->execute($con)) {
			//$return[] = "";
		} else {
			$return = "Error deleting all Injuries for accident  $injury_num{$pageVars->accidentid} ".$qry->getError();
		}
		return $return;
	}
	
	#creates an array of injuries, either form the db query for this accident, or from the post for this form if it exists
	function getFormInjuries($injuryDB_rows) {
		$injuries = array();
		if (isset($_POST['injury_id'])) {
			for ($i = 0; $i < (count($_POST['injury_id'])); $i++) {
				$injuries[$i]['injury_id'] = $_POST['injury_id'][$i];
				$injuries[$i]['injury_num'] = $_POST['injury_num'][$i];
				$injuries[$i]['injury_desc'] = $_POST['injury_desc'][$i];
				$injuries[$i]['injury_part'] = $_POST['injury_part'][$i];
				$injuries[$i]['injury_type'] = $_POST['injury_type'][$i];
				
			}
		} else {
			$injuries = $injuryDB_rows;
		}
		return $injuries;
	}
	
	
?>