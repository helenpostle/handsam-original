<?php
/* Returns a record set of all the Bumps and Lumps incidents that occurred in the month
 * specified by $reportDate.
 * @param Object $pageVar       Page class
 * @param Object $con           MySQL connection object
 * @param Date $reportDate      Bump Report Date. Defaults to today's date if not specified
 * @return Array                Array of dates
 */
function getBumpList($pageVars, $con, $reportDate) {

    // Get Start and End date for our SQL Query
    $dateStart = null;
    $dateEnd = null;
    If (!isset($reportDate))
        $reportDate = date('Y-m-d');
    $dateStart = date('Y-m-01', strtotime($reportDate));
    $dateEnd = date('Y-m-t', strtotime($reportDate));

    // Build SQL Query
    $sql = 'SELECT ReportDate FROM acc_bump WHERE AccountID = '.$pageVars->accountid.
        ' AND ReportDate BETWEEN \''.$dateStart.'\' AND \''.$dateEnd.'\' ORDER BY ReportDate';

    // Run Query
    $pageDb = new pageQry($con, $sql);
    $pageDb->rsList();
    return $pageDb->rows;
}

/*
 * Return the bump report details for the specified account and report date
 * @param Integer $accountID    Account ID
 * @param DateTime $reportDate  Report Date
 * @param Object $con           MySQL connection object
 * @return RecordSet            Bump report record
 *
 */
function getBumpReport($accountID, $reportDate, $con) {

    $sql = 'SELECT * FROM acc_bump WHERE AccountID = '.$accountID.' AND '.
                'ReportDate = \''.$reportDate.'\'';
    $pageDb = new pageQry($con, $sql);
    $pageDb->rsItem();
    return $pageDb->row;
}

/*
 * Attempts to save the Bump Report
 * @param Object $con           MySQL connection object
 * @param Array $postVars       POST values
 * @param Object $pageVars      Page class
 * @param Object $clsUS         User Session Class
 * @return Array                Array of error messages
 */
function saveBumpReport($con, $postVars, $pageVars, $clsUS) {
    if ($postVars['bumpID'] == 0) {
        $qry = new dbInsert('acc_bump');
        $qry->setStringVal('ReportDate', date('Y-m-d H:i:s', strtotime($postVars['reportdate'])), 'Report Date');
        $qry->setReqNumberVal('accountid', $pageVars->accountid, 'Account ID');
        $qry->setReqNumberVal('CreatedUserID', $pageVars->userid, 'Created User ID');
        $qry->addValue('CreatedTime', 'NOW()');
    }
    else {
        $qry = new dbUpdate('acc_bump');
        $qry->setParam("BumpID", $postVars['bumpID']);
        $qry->setReqNumberVal('ModifiedUserID', $pageVars->userid, 'Modified User ID');
        $qry->addValue('ModifiedTime', 'NOW()');
    }

    $qry->setReqStringVal('Details', $postVars['Details'], 'Injured Parties');
    $qry->setStringVal('Note', $postVars['Note'], 'Notes');

    if ($qry->execute($con)) {
        // die($qry->getSQL());
        return '';
    }
    else {
        return $qry->getError();
    }
}
