<?php

/*functions for saving  first aider details in accident book*/

	//save first Aider details
	
	function saveFirstAiders($con, $pageVars, $clsUS) {
		$return = array();
		if ($pageVars->accidentid > 0) {
			if( isset($_POST['first_aider_id']) && count($_POST['first_aider_id'] > 0)) {
				for($i=0; $i<count($_POST['first_aider_id']); $i++) {
					if (isset($_POST["aider_userid"][$i]) && isset($_POST["aider_usertype"][$i]) && $_POST["aider_userid"][$i] > 0 && $_POST["aider_usertype"][$i] > 0) {
						if ($_POST['first_aider_id'][$i] == 0) {
							$qry = new dbInsert("acc_first_aider");
							$qry->setReqNumberVal("accidentid",$pageVars->accidentid,"Accidentid");
						} else {
							$qry = new dbUpdate("acc_first_aider");
							$qry->setParam("first_aider_id", $_POST['first_aider_id'][$i]);
						}
						$aider_num = $i + 1;
						if ($_POST["aider_usertype"][$i] == 1) {
							$qry->setReqNumberVal("userid",$_POST["aider_userid"][$i],"First Aider $aider_num uerid");
							$qry->setNullVal("assoc_userid");
						} else if ($_POST["aider_usertype"][$i] == 2) {
							$qry->setReqNumberVal("assoc_userid",$_POST["aider_userid"][$i],"First Aider $aider_num userid");
							$qry->setNullVal("userid");
						}
						$qry->setReqNumberVal("fA_usertype",$_POST["aider_usertype"][$i],"First Aider $aider_num usertype");
						$qry->setStringVal("fAcomments",$_POST["fAcomments"][$i],"First Aider $aider_num Comments");
						$qry->setAudit($clsUS->userid);
						if ($qry->execute($con)) {
							//$return[] = "";
						} else {
							$return[] = $qry->getError();
						}
					}
		
				}
			} else {
				$err = delAllFirstAiders($con, $pageVars, $clsUS);
				if ($err != "") $return[] = $err;
			}
		}
		return $return;

	}
	
	
	//delete first aiders that have the hidden delete field posted
	
	function delAider($con, $pageVars, $clsUS) {
		$return = array();
		if (isset($_POST['del_aider_id'])) {
			for($i=0; $i<count($_POST['del_aider_id']); $i++) {
				
				if ($_POST['del_aider_id'][$i] != "") {
					$del_id = $_POST['del_aider_id'][$i];
					settype($del_id,"integer");
					$qry = new dbDelete("acc_first_aider");
					$qry->setParam("first_aider_id", $del_id);
					//now check that this is a first aider only linked to this accident
					$qry->setParam("accidentid", $pageVars->accidentid);
					$injury_num = $i + 1;
					if ($qry->execute($con)) {
						//$return[] = "";
					} else {
						$return[] = "Error deleting First Aider $injury_num ".$qry->getError();
					}
				}
		
			}
		}
		return $return;

	}	

	
	function delAllFirstAiders($con, $pageVars, $clsUS) {
		$return = "";
		$qry = new dbDelete("acc_first_aider");
		$qry->setParam("accidentid", $pageVars->accidentid);
		if ($qry->execute($con)) {
			//$return[] = "";
		} else {
			$return = "Error deleting First Aider $injury_num ".$qry->getError();
		}
		return $return;
	}	
		
	#creates an array of first aiders, either form the db query for this accident, or from the post for this form if it exists
	function getFormFirstAiders($fAiderDB_rows, $pageVars, $con) {
		$first_aiders = array();
		if (isset($_POST['first_aider_id'])) {
			for ($i = 0; $i < (count($_POST['first_aider_id'])); $i++) {
				$first_aiders[$i]['first_aider_id'] = $_POST['first_aider_id'][$i];
				$first_aiders[$i]['aider_userid'] = $_POST['aider_userid'][$i];
				$first_aiders[$i]['aider_num'] = $_POST['aider_num'][$i];
				$first_aiders[$i]['fAcomments'] = $_POST['fAcomments'][$i];
				$first_aiders[$i]['fA_usertype'] = "";
				if (isset($_POST['aider_usertype'][$i])) $first_aiders[$i]['fA_usertype'] = $_POST['aider_usertype'][$i];
				if (isset($_POST['aider_usertype'][$i]) && $_POST['aider_usertype'][$i] == 1) {
					$qry  = " select * from usr where userid = {$_POST['aider_userid'][$i]} and accountid = {$pageVars->accountid}";
				} else if(isset($_POST['aider_usertype'][$i]) && $_POST['aider_usertype'][$i] == 2){
					$qry  = " select * from usr_assoc where assoc_userid = {$_POST['aider_userid'][$i]} and accountid = {$pageVars->accountid}";
				} else {
					$qry  = " select * from usr_assoc where assoc_userid = 0 and accountid = {$pageVars->accountid}";
				}
				
				$rs = getRS($con,$qry);
				$row = getRow($rs);
				$first_aiders[$i]['firstname'] = $row['firstname'];
				$first_aiders[$i]['lastname'] = $row['lastname'];
				$first_aiders[$i]['address1'] = $row['address1'];
				$first_aiders[$i]['address2'] = $row['address2'];
				$first_aiders[$i]['address3'] = $row['address3'];
				$first_aiders[$i]['city'] = $row['city'];
				$first_aiders[$i]['postcode'] = $row['postcode'];
				$first_aiders[$i]['tel1'] = $row['tel1'];
				$first_aiders[$i]['tel2'] = $row['tel2'];
			}
		} else {
			$first_aiders = $fAiderDB_rows;
		}
		return $first_aiders;
	}	
	
?>