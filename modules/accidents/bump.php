<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
/**
 * Script........: bump.php
 * Date..........: 13th April 2015
 * Author........: David Loosley
 * Description...: Bumps & Lumps Report
 *
 */

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require('../../shared/startPage.php');
$pageFiles->addModFunc('bump_list');
// Instead of fixing a possible bug in clsLoadPageFiles->addCss() and maybe causing a problem elsewhere
// it was decided to just load the calendar CSS file manually
$pageFiles->cssUrl[] = $rootUrl.'/vendor/calendar/style.css';
$pageFiles->cssFile[] = $rootPath.'/vendor/calendar/style.css';
$pageFiles->includePhp();

//priority for module functionality boxes over page boxes
// $pageFiles->addBoxPriority();
$pageVars = new loadVars($clsUS, $con);

if (isAllowed('viewAccidents') || isAllowed('recordAccidents')) {

    // Get month and year to be displayed
    $month = date('n');
    $year = date('Y');
    $reportDate = date('Y-m-d');
    if (isset($_GET['m']))
        $month = $_GET['m'];
    if(isset($_GET['y']))
        $year = $_GET['y'];

    // Prepare content and retrieve Bump Reports for selected month
    $title = 'Bump Report';
    $content = 'page.bump_list.php';
    // $box[] = '';
    $reportDate = new DateTime($year.'-'.$month.'-01');
    $endOfMonth = new DateTime($reportDate->format('Y-m-d'));
    $endOfMonth->modify('last day of this month');
    $bumpDates = getBumpList($pageVars, $con, $reportDate->format('Y-m-d'));

}
else {
	trigger_error('Access Denied',E_USER_WARNING);
	headerLocation('$rootUrl/index.php',false);
}

include('../../layout.php');
