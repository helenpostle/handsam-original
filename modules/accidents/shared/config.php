<?php
$modCss = "acc_2.0.css";
$status_arr = $accidents_conf['user_status'];
$sex_arr = $accidents_conf['user_sex'];
$yes_no_arr = $accidents_conf['yes_no_arr'];
$no_yes_arr = $accidents_conf['no_yes_arr'];
$riddor_arr = $accidents_conf['riddor'];
$acc_type_arr = $accidents_conf['acc_type'];

// add the Illness if option if configured
if($accidents_conf['illness_type']) $acc_type_arr = array_merge($acc_type_arr, array('ILLNESS'=>'Illness'));
// add restraint option
if($accidents_conf['restraint_type']) $acc_type_arr = array_merge($acc_type_arr, array('RESTRAINT' => 'Restraint'));

$about_accident_arr = $accidents_conf['about_accident_arr'];

$injury_Q1_arr = $accidents_conf['injury_Q1'];

$injury_Q2_arr = $accidents_conf['injury_Q2'];

$injury_part_arr = $accidents_conf['injury_part'];

$injury_type_arr = $accidents_conf['injury_type'];

Define("SAVED_COMPLETE","Saved as complete");

$reports_arr = array("accident" => "accident", "accidentsummary" => "accidentsummary", "accidenttype" => "accidenttype", "injuredparty" => "injuredparty", "injury_part" => "injury_part", "injury_type" => "injury_type", "sitearea" => "sitearea");
?>