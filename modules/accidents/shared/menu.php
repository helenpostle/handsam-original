<?php

// accidents menu 
//if (isAllowed("recordAccidents" && !isAllowed("editAccount"))) {
if (isset($clsUS)) {
    $inc_lic_qry = "select * from accountlicence where accountid = {$clsUS->accountid} and end_date > NOW() and state = 'ACTIVE' ";
    $inc_rs = getRS($con, $inc_lic_qry);

    if (isAllowed("recordAccidents") && getResultRows($inc_rs) > 0) {
        $incident_menu = $clsMenu->addMenuItem("/modules/accidents/index.php", "INCIDENT LOG", "", "");
        // also to the submenu
        $clsMenu->addMenuItem("/modules/accidents/index.php", "INCIDENT LOG", "", $incident_menu);
        $clsMenu->addMenuItem("/modules/accidents/bump.php", "BUMPS REPORT", "", $incident_menu);
    } else if (isAllowed('editAlerts')) {
        $incident_menu = $clsMenu->addMenuItem("/modules/accidents/alerts.php", "INCIDENT LOG", "", "");
    }

    // access to the manage alerts screen.
    if (isAllowed('editAlerts')) {
		$clsMenu->addMenuItem("/modules/accidents/riddoralerts.php", "Manage RIDDOR Alerts", "", $incident_menu);
        $clsMenu->addMenuItem("/modules/accidents/majoralerts.php", "Manage Death/Major Injury Alerts", "", $incident_menu);
        $clsMenu->addMenuItem("/modules/accidents/alerts.php", "Manage Investigation Alerts", "", $incident_menu);
    }
}
