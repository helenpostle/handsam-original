<?php

if(isAllowed("recordAccidents"))
{
    //$title .= "Accident Module";    
    ob_start();
    include("$rootPath/modules/accidents/content/page.index.php");
    $index_modules_content[] = ob_get_contents();
    ob_end_clean();

    // show open investigations
    ob_start();
    include("$rootPath/modules/accidents/content/dashboard.investigation.php");
    $index_modules_content[] = ob_get_contents();
    ob_end_clean();
}
