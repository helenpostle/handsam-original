<?php
/**
 * HSE info
 */
 

accidentBoxTop('Accident Comments', 'acc_comments');
    //comments area
 echo "<div class=\"comments\">";   
    echo "<h3> Comments </h3>";

    if ($pageVars->accidentid > 0) {

        $sql = "SELECT usr.firstname, usr.lastname, acc_comments.* FROM acc_comments LEFT JOIN usr ON acc_comments.created_by = usr.userid WHERE acc_comments.accidentid = {$pageVars->accidentid} order by acc_comments.created desc limit 0,8";
        $comments = getRS($con, $sql);
        echo "<ul id=\"comment_list\">";

        if (getResultRows($comments) > 0) {
            while ($rowComment = getRow($comments)) {
                echo "<li class=\"clearfix\">";
                echo "<strong>{$rowComment['firstname']} {$rowComment['lastname']}</strong> : <em>".displayDateTime($rowComment['created'])."</em>";

                echo displayParagraphs($rowComment['commenttext']);
                echo "</li>";
            }
        } else {
            echo "<li> No Comments </li>";
        }
        echo "</ul>";
        //count comments - idf more than 8 show all
        $comment_qry = "select commentid from acc_comments where accidentid = {$pageVars->accidentid}";
        $comment_rs = getRS($con, $comment_qry);
        if (getResultRows($comment_rs) > 8) {
            echo "<a class=\"show_all\" href=\"#\">show all</a>";
        }
    }
    echo "</div>";

accidentBoxBottom();
?>