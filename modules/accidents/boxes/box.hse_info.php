<?php
/**
 * HSE info
 */
 

accidentBoxTop('HSE Information');
echo '<h3>RIDDOR - Report an incident</h3>';
echo displayParagraphs('RIDDOR is the Reporting of Injuries, Diseases and Dangerous Occurrences Regulations 2013. Employers, the self-employed and those in control of premises must report specified workplace incidents.  More can found out about RIDDOR <a target="_blank" href=\'http://www.hse.gov.uk/riddor/index.htm\'> on the HSE website</a>');
 
echo "<ul class=\"boxList\">\n"; 
echo "<li><a target=\"_blank\" href='http://www.hse.gov.uk/riddor/what-must-i-report.htm'>Report online</a></li>\n";
echo "<li><a target=\"_blank\"  href='http://www.hse.gov.uk/riddor/report.htm#phone'>Report by phone - 0845 300 99 23</a> - <em>only for fatal and major injuries</em></li>\n";
echo "<li><a target=\"_blank\"  href='http://www.hse.gov.uk/riddor/report.htm#email'>Report by e-mail</a></li>\n";
echo "<li><a target=\"_blank\"  href='http://www.hse.gov.uk/contact/outofhours.htm'>Reporting out of hours</a></li>\n";
echo "</ul>\n";
accidentBoxBottom();

?>