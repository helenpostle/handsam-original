<?php
/**
 * options to print or download a report
 */
 
    //$url = $_SERVER["REQUEST_URI"];
    $qs = "";
    $i = 0;
    foreach ($_GET as $key=>$val) {
	    if ($key != "run" && $key != "runreport") {
		    if ($i == 0) {
		 		$qs .= "?".cleanKey( $key )."=".getStrFromRequest($key); 
	 		} else {
		 		$qs .= "&".cleanKey( $key )."=".getStrFromRequest($key);
	 		}
	 		$i ++;
 		}
    }
    $url = "reporting.php".$qs;
	accidentBoxTop("REPORT OPTIONS");
	
	if ($run) {
		echo "<ul>\n";
		echo "<li class=\"reportList1\"><a href=\"$url&run=true&csv=true\">Download as CSV</a></li>";
		//echo "<li><a href=\"$url&run=true&print=true\" target=\"_new\">Printable Version</a></li>";
		
		echo "<li class=\"reportList1\"><a href=\"$url\">Change Parameters</a></li>";
	}
	echo "<li class=\"reportList1\"><a href=\"reporting.php\">Back to Report List</a></li>";
	echo "</ul>";
	
	accidentBoxBottom();?>