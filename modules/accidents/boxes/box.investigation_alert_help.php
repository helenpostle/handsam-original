<?php

// quick help box for alerts.

accidentBoxTop('Alerts Help');
?>
<p>You can create and control your own email alerts here. These will be sent for overdue investigations. </p>

<p>First choose a duration, e.g. 10 days. </p>

<p>Then add recipients to receive the alert. If you add a job role, and alert will be sent to each user associated with that role.</p>

<p>Click 'Create an Alert' to add extra alerts. </p>
<?php 
accidentBoxBottom();
?>