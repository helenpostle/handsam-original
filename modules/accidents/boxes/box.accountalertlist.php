<?php

/**
 * Box to display all accounts (schools) and the number of alerts
 */
accidentBoxTop('Per Account');
echo '<p>Configure alerts on an individual school basis.</p>';

echo '<ul>';

while($r = getRowAssoc($accounts))
{
    echo '<li style="border-bottom:none">';
    echo '<a href="alerts.php?account_id='.$r['accountid'].'">'.$r['accountname'].'</a>';
    // if the school has alerts configure, show how many
    if($r['alert_count'] > 0) echo ' (',$r['alert_count'],' alert'.(($r['alert_count'] > 1) ? 's':'').')';
    echo '</li>';
}
echo '</ul>';

accidentBoxBottom();
