<?php
/**
 * HSE info
 */
 

accidentBoxTop('File Uploads');
echo displayParagraphs('Here is a list of files that have been uploaded to this incident. Click the button below to upload another file, or click on a file to download it. ');

	$uploads = getUploads($con, $pageVars);
	echo "<div id=\"uploads_list\">";
		if ($uploads->cnt > 0) {
			echo "<ul class=\"clearfix\">";
			foreach ($uploads->rows as $row) {
				echo "<li  class=\"clearfix\"><a class=\"".strtolower(substr(strrchr($row['filename'],'.'),1))."\" href=\"$rootUrl/modules/accidents/downloadAccDocument.php?id={$row['fileid']}\">{$row['title']}</a></li>";
			}
			echo "</ul>";
		} else {
			echo "<p>No Uploaded Files</p>";
		}
	echo "</div>";
accidentBoxBottom();

?>