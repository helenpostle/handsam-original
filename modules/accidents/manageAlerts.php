<?php

/**
 * Email alerts Configuration page
 * 
 * Mei Gwilym, August 2012
 */

/*
 * NOT USED
 * 
 * I've left this in for now, instead of deleting.
 * There were some codez that I didn't want to delete, but this should go at some point,
 * and should certainly not be part of the push to live.
 * MG 16/10
 */


// make this a secure page
$secure = true;
require("../../shared/startPage.php");

// user select popup
$pageFiles->addFunc("select_alert_users");
// add the extra files for the Alerts
$pageFiles->addModFunc('alerts');

$pageFiles->addCoreLibPhp('clsAccount/clsAccount.php');

$pageFiles->includePhp();

$pageVars = new loadVars($clsUS, $con);
if ($pageVars->hacking) headerLocation("$rootUrl/index.php", "");

// alerts config
$alertType = 'investigation';
$alertMinDays = 10; // the minimum time after which an alert can be sent

if (isAllowed("editAlerts"))
{    
    // get the chosen account id to manage
    $account_id = getIntFromRequest('account_id');
    // @todo error check
    $account = new Account($account_id);
    
    // save the form
    if(buttonClicked('Save') && isset($_POST['alert']))
    {
        // save the form details
        // loop through each row and save as a single record (and user details)
        foreach($_POST['alert'] as $alert)
        {
            // if there are no users added to the alert
            // don't save the alert
            if(
                    (isset($alert['user']) && count($alert['user']) > 0) ||
                    (isset($alert['jobrole']) && count($alert['jobrole']) > 0)
            )
            {
                if($alertid = saveAlert('investigation', $account_id, $alert))
                {
                    // save the users
                    saveAlertUsers($alertid, $alert);
                }
            }
        }
    }
    
    // load up everything for the page
    //$alerts = getAlerts($account_id, $alertType);
    $alertusers = getAlertUsers($account_id, $alertType);
    
    foreach($alertusers as &$u) // passed as a reference, we're manipulating $alertusers in the loop
    {
        $u['jobrole'] = getAlertJobroles($account_id, $u['jobrole']);
    }
    
    $globalAlerts = getAlertUsers('0', $alertType);
    foreach($globalAlerts as &$g) // passed as a reference, we're manipulating $globalAlerts in the loop
    {
        $g['jobrole'] = getAlertJobroles('0', $u['jobrole']);
    }  
    
    // the alerts page is created from a JSON model by a script
    $pageFiles->addJsVar('alerts', json_encode(array_values($alertusers)));
    $pageFiles->addJsVar('globalAlerts', json_encode(array_values($globalAlerts)));
    $pageFiles->addJsVar('userTabs', json_encode(array('users', 'jobroles')));
    
    $pageFiles->addJsVar('account_id', $account_id);
    
    $pageFiles->addJsVar('alertMinDays', $alertMinDays);
    
    $title = "Email Alerts for ".$account->accountname;
    $content = 'alert.investigation.php';
    $box[] = 'box.alert-help.php';
}
else
{
    trigger_error("Access Denied", E_USER_WARNING);  
    headerLocation("$rootUrl/index.php",false);
}

include("../../layout.php");