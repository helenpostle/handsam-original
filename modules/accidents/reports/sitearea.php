<?php
$link = "$rootUrl/modules/accidents/reporting.php?report=injuredparty&run=true&injuredid=";

$advice = "Select a site area to get a list of incidents for that area.";
	    	
$arrayParams["siteid"] = getParam("siteid",1);

$arrayParams["reporterid"] = getParam("reporterid",1);

$nameQry = "select CONCAT(site_name, ' - ', site_area) as site from acc_accident_site where siteid = ".$arrayParams["siteid"]." and accountid = ".$accountid;
$nameRS = getRS($con,$nameQry);
$nameRow = getRow($nameRS);
$title = "Incident list report for site area: ".displayText($nameRow['site']);

if ($arrayParams["reporterid"] == 0) {
	$title .= ": All Reporters"; 
	$reporterQry = "";
} else {
	if (isset($_GET["reporterid"])) {
		$usrQry = "select Concat(usr.firstname, ' ', usr.lastname) as fullname from usr where userid = {$arrayParams["reporterid"]} ";
		$usrRs = getRS($con, $usrQry);
		$usrRow = getRow($usrRs);
		$title .= ", Reporter: {$usrRow['fullname']}"; 
		$reporterQry = " and acc_accident.created_by = {$arrayParams["reporterid"]} ";
	}
	
}



$sql = "SELECT CASE WHEN acc_accident.injured_usertype=2 THEN CONCAT('a_',acc_accident.assoc_userid) ELSE CONCAT('u_', acc_accident.userid) END AS link, '' as link2, ";

$sql .= "CASE WHEN acc_accident.injured_usertype=2 THEN CONCAT(a.lastname, ', ', a.firstname) ELSE CONCAT(u.lastname, ', ', u.firstname) END AS \"Injured Party\", ";

$sql .= "DATE_FORMAT(accident_time,'%d/%m/%y') as \"Accident Date\", status as \"Status\", accident_type as \"Type\", (CASE when riddor = 'NON-RIDDOR'  THEN 'No' ELSE 'Yes' END) as \"Riddor\", (CASE when ambulance = 0  THEN 'No' ELSE 'Yes' END) as \"Ambulance\", (CASE when party_injured = 0  THEN 'No' ELSE 'Yes' END) as \"Injured\", (CASE when first_aid = 0  THEN 'No' ELSE 'Yes' END) as \"First Aid\"";
//$sql .= "DATE_FORMAT(accident_time,'%d/%m/%y') as \"Accident Date\", status as \"Status\", accident_type as \"Type\", (CASE when riddor = 'NON-RIDDOR'  THEN 'No' ELSE 'Yes' END) as \"Riddor\", (CASE when ambulance = 0  THEN 'No' ELSE 'Yes' END) as \"Ambulance\", (CASE when party_injured = 0  THEN 'No' ELSE 'Yes' END) as \"Injured\", (CASE when first_aid = 0  THEN 'No' ELSE 'Yes' END) as \"First Aid\", days_off as \"Days Off\", cost as \"Cost\"";

$sql .= " from acc_accident left join usr u on acc_accident.userid = u.userid ";
$sql .= " left join usr_assoc a on acc_accident.assoc_userid = a.assoc_userid ";

$sql .= " left join acc_accident_site on acc_accident.siteid = acc_accident_site.siteid ";
$sql .= "WHERE acc_accident.state = 'COMPLETE' AND acc_accident.siteid = ".$arrayParams["siteid"] ." and acc_accident.accountid = $accountid ";

$sql .= $reporterQry;

$sql .= " ORDER BY accident_time";
	    	
?>