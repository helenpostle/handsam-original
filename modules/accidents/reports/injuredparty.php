<?php
$link = "$rootUrl/modules/accidents/reporting.php?report=sitearea&run=true&siteid=";

$advice = "Select an injured party to get a list of incidents that they were involved in.";
	    	
$arrayParams["injuredid"] = getParam("injuredid",1);
$arrayParams["reporterid"] = getParam("reporterid",1);


$arrayParams["accountid"] = $clsUS->accountid;

$injuredidQry = "";
$reporterQry = "";

if (isset($_GET["injuredid"])) {
	
	//test for u_ for usr table
	$injuredid_arr = explode ("_", $arrayParams["injuredid"]);
	if ($injuredid_arr[0] == "u") {
		$nameQry = "select CONCAT(firstname, ' ', lastname) as injured_party from usr where userid = ".$injuredid_arr[1]." and accountid = ".$arrayParams["accountid"];
		$injuredidQry = "AND acc_accident.userid = ".$injuredid_arr[1];
	} else if ($injuredid_arr[0] == "a") {
		$nameQry = "select CONCAT(firstname, ' ', lastname) as injured_party from usr_assoc where assoc_userid = ".$injuredid_arr[1]." and accountid = ".$arrayParams["accountid"];
		$injuredidQry = "AND acc_accident.assoc_userid = ".$injuredid_arr[1];
	}
	
	$nameRS = getRS($con,$nameQry);
	$nameRow = getRow($nameRS);
	$title = "Incident list report for injured party: ".displayText($nameRow['injured_party']);
}
if ($arrayParams["reporterid"] == 0) {
	$title .= ": All Reporters"; 
} else {
	if (isset($_GET["reporterid"])) {

		$usrQry = "select Concat(usr.firstname, ' ', usr.lastname) as fullname from usr where userid = {$arrayParams["reporterid"]} ";
		$usrRs = getRS($con, $usrQry);
		$usrRow = getRow($usrRs);
		$title .= ", Reporter: {$usrRow['fullname']}"; 
		$reporterQry = " and acc_accident.created_by = {$arrayParams["reporterid"]} ";
	}
	
}

//$sql = "SELECT acc_accident.siteid AS link, '' as link2, acc_accident_site.site_name as \"Site Name\", accident_type as \"Type\", DATE_FORMAT(accident_time,'%d/%m/%y') as \"Accident Date\", status as \"Status\", (CASE when riddor = 'NON-RIDDOR'  THEN 'No' ELSE 'Yes' END) as \"Riddor\", (CASE when ambulance = 0  THEN 'No' ELSE 'Yes' END) as \"Ambulance\", (CASE when party_injured = 0  THEN 'No' ELSE 'Yes' END) as \"Injured\", (CASE when first_aid = 0  THEN 'No' ELSE 'Yes' END) as \"First Aid\", days_off as \"Days Off\", cost as \"Cost\"";
$sql = "SELECT acc_accident.siteid AS link, '' as link2, acc_accident_site.site_name as \"Site Name\", accident_type as \"Type\", DATE_FORMAT(accident_time,'%d/%m/%y') as \"Accident Date\", status as \"Status\", (CASE when riddor = 'NON-RIDDOR'  THEN 'No' ELSE 'Yes' END) as \"Riddor\", (CASE when ambulance = 0  THEN 'No' ELSE 'Yes' END) as \"Ambulance\", (CASE when party_injured = 0  THEN 'No' ELSE 'Yes' END) as \"Injured\", (CASE when first_aid = 0  THEN 'No' ELSE 'Yes' END) as \"First Aid\"";
$sql .= "from acc_accident  left join acc_accident_site on acc_accident.siteid = acc_accident_site.siteid ";
$sql .= "WHERE acc_accident.state = 'COMPLETE' and acc_accident.accountid = $accountid ";

$sql .= "$injuredidQry ";

$sql .= $reporterQry;

$sql .= " ORDER BY accident_time";
//echo $sql;
	    	
?>