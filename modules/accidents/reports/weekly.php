<?php

$advice = "Select a Start Date and an End Date to get a list of reports Between these two dates. Default period is for last month.";
	    	
$arrayParams["startdate"] = getParam("startdate",getStartLastMonth());
$arrayParams["enddate"] = getParam("enddate",getEndLastMonth());

$title = "Accident list detailed report: ".$arrayParams["startdate"]." to ".$arrayParams["enddate"];


$rsAccName = getRS($con,"SELECT  accountname from account where accountid = ".$accountid);
$rowAccName = getRow($rsAccName);
$arrayParams["accountname"] = $rowAccName["accountname"];
$title = "Accident list detaled report: <br/>".$arrayParams["startdate"]." to ".$arrayParams["enddate"];

$sql = "SELECT 1 AS link, CONCAT(accident.lastname, ', ', accident.firstname) AS \"Injured Party\", status as \"Status\", site_name as \"Site Name\", accident_type as \"Type\", (CASE when riddor = 0  THEN 'No' ELSE 'Yes' END) as \"Riddor\", (CASE when ambulance = 0  THEN 'No' ELSE 'Yes' END) as \"Ambulance\", (CASE when party_injured = 0  THEN 'No' ELSE 'Yes' END) as \"Injured\", (CASE when first_aid = 0  THEN 'No' ELSE 'Yes' END) as \"First Aid\", days_off as \"Days Off\", cost as \"Cost\" ";
$sql .= "FROM acc_accident ";
$sql .= "WHERE state = 'COMPLETE' AND CAST(accident_time AS DATE) >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND CAST(accident_time AS DATE) <= '".formatDatabaseInputDate($arrayParams["enddate"])."' and acc_accident.accountid = $accountid ";


$sql .= " ORDER BY \"Injured Party\"";
//echo $sql;
	    	
?>