<?php
$advice = "Select a Start Date and an End Date to get a  summary  of reports Between these two dates. Default period is for last month.";
	    	
$arrayParams["startdate"] = getParam("startdate",getStartLastMonth());
$arrayParams["enddate"] = getParam("enddate",getEndLastMonth());
$arrayParams["reporterid"] = getParam("reporterid",1);
		    
$title = "Incident Report Summary : <br/>".$arrayParams["startdate"]." to ".$arrayParams["enddate"];

if ($arrayParams["reporterid"] == 0) {
	$title .= ", All Reporters"; 
	$reporterQry = "";
} else {
	if (isset($_GET["reporterid"])) {
		$usrQry = "select Concat(usr.firstname, ' ', usr.lastname) as fullname from usr where userid = {$arrayParams["reporterid"]} ";
		$usrRs = getRS($con, $usrQry);
		$usrRow = getRow($usrRs);
		$title .= ", Reporter: {$usrRow['fullname']}"; 
		$reporterQry = " and acc_accident.created_by = {$arrayParams["reporterid"]} ";
	}
	
}

$sql = "SELECT '' AS link, '' as link2, count(accidentid) as \"Total Incidents\", sum(CASE WHEN accident_type = 'NEAR MISS' THEN 1 ELSE 0 END) as \"Near Misses\", sum(CASE WHEN accident_type = 'ACCIDENT' THEN 1 ELSE 0 END) as \"Accidents\", sum(CASE WHEN riddor = 'RIDDOR' THEN 1 ELSE 0 END) as \"RIDDOR Reportable\", sum(CASE WHEN ambulance = 1 THEN 1 ELSE 0 END) as \"Ambulance attended\", sum(CASE WHEN party_injured = 1 THEN 1 ELSE 0 END) as \"Party Injured\", sum(CASE WHEN first_aid = 1 THEN 1 ELSE 0 END) as \"First Aid Given\" ";
//$sql = "SELECT '' AS link, '' as link2, count(accidentid) as \"Total Incidents\", sum(CASE WHEN accident_type = 'NEAR MISS' THEN 1 ELSE 0 END) as \"Near Misses\", sum(CASE WHEN accident_type = 'ACCIDENT' THEN 1 ELSE 0 END) as \"Accidents\", sum(CASE WHEN riddor = 'RIDDOR' THEN 1 ELSE 0 END) as \"RIDDOR Reportable\", sum(CASE WHEN ambulance = 1 THEN 1 ELSE 0 END) as \"Ambulance attended\", sum(CASE WHEN party_injured = 1 THEN 1 ELSE 0 END) as \"Party Injured\", sum(CASE WHEN first_aid = 1 THEN 1 ELSE 0 END) as \"First Aid Given\", sum(days_off) as \"Days Off\", sum(cost) as \"Cost\" ";
$sql .= " FROM acc_accident ";
$sql .= "WHERE state = 'COMPLETE' AND CAST(accident_time AS DATE) >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND CAST(accident_time AS DATE) <= '".formatDatabaseInputDate($arrayParams["enddate"])."' and acc_accident.accountid = $accountid ";

$sql .= $reporterQry;


//$sql .= " GROUP BY \"Near Misses\", \"Accidents\", \"RIDDOR Reportable\", \"Ambulance attended\", \"Party Injured\",\"First Aid Given\",\"Days Off\",\"Cost\"";
$sql .= " GROUP BY \"Near Misses\", \"Accidents\", \"RIDDOR Reportable\", \"Ambulance attended\", \"Party Injured\",\"First Aid Given\"";

?>