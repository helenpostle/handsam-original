<?php

$advice = "Select a Start Date and an End Date to get a list of reports Between these two dates. Default period is for last month.";
	    	
$arrayParams["startdate"] = getParam("startdate",getStartLastMonth());
$arrayParams["enddate"] = getParam("enddate",getEndLastMonth());


$arrayParams["reporterid"] = getParam("reporterid",1);

$title = "Injury part report: ".$arrayParams["startdate"]." to ".$arrayParams["enddate"];

$rsAccName = getRS($con,"SELECT  accountname from account where accountid = ".$accountid);
$rowAccName = getRow($rsAccName);
$arrayParams["accountname"] = $rowAccName["accountname"];
$title = "Injury part report: <br/>".$arrayParams["startdate"]." to ".$arrayParams["enddate"];


if ($arrayParams["reporterid"] == 0) {
	$title .= ": All Reporters"; 
	$reporterQry = "";
} else {
	if (isset($_GET["reporterid"])) {
		$usrQry = "select Concat(usr.firstname, ' ', usr.lastname) as fullname from usr where userid = {$arrayParams["reporterid"]} ";
		$usrRs = getRS($con, $usrQry);
		$usrRow = getRow($usrRs);
		$title .= ", Reporter: {$usrRow['fullname']}"; 
		$reporterQry = " and acc_accident.created_by = {$arrayParams["reporterid"]} ";
	}
	
}

$sql = "SELECT 1 AS link, '' as link2, injury_part as \"Injured body part\", count(injury_part) as total ";
$sql .= "FROM acc_injury_details left join acc_accident on acc_injury_details.accidentid = acc_accident.accidentid ";
$sql .= "WHERE acc_accident.state = 'COMPLETE' AND CAST(accident_time AS DATE) >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND CAST(accident_time AS DATE) <= '".formatDatabaseInputDate($arrayParams["enddate"])."' and acc_accident.accountid = $accountid ";
$sql .= $reporterQry;

$sql .= " group by injury_part";
//echo $sql;
	    	
?>