<?php

$advice = "Select a Start Date and an End Date to get a list of reports Between these two dates. Default period is for last month.";
	    	
$arrayParams["startdate"] = getParam("startdate",getStartLastMonth());
$arrayParams["enddate"] = getParam("enddate",getEndLastMonth());
$arrayParams["reporterid"] = getParam("reporterid",1);


$title = "Incident list detailed report: ".$arrayParams["startdate"]." to ".$arrayParams["enddate"];


$rsAccName = getRS($con,"SELECT  accountname from account where accountid = ".$accountid);
$rowAccName = getRow($rsAccName);
$arrayParams["accountname"] = $rowAccName["accountname"];
$title = "Incident list detailed report: <br/>".$arrayParams["startdate"]." to ".$arrayParams["enddate"];

if ($arrayParams["reporterid"] == 0) {
	$title .= ", All Reporters"; 
	$reporterQry = "";
} else {
	if (isset($_GET["reporterid"])) {
		$usrQry = "select Concat(usr.firstname, ' ', usr.lastname) as fullname from usr where userid = {$arrayParams["reporterid"]} ";
		$usrRs = getRS($con, $usrQry);
		$usrRow = getRow($usrRs);
		$title .= ", Reporter: {$usrRow['fullname']}"; 
		$reporterQry = " and acc_accident.created_by = {$arrayParams["reporterid"]} ";
	}
	
}

$sql = "SELECT '' AS link, '' as link2, ";


$sql .= "CASE WHEN acc_accident.injured_usertype=2 THEN CONCAT(a.lastname, ', ', a.firstname) ELSE CONCAT(u.lastname, ', ', u.firstname) END AS \"Injured Party\", ";


//$sql .= " CONCAT(acc_injured_party.lastname, ', ', acc_injured_party.firstname) AS \"Injured Party\", ";

$sql .= " status as \"Status\", acc_accident_site.site_name as \"Site Name\", accident_type as \"Type\", (CASE when riddor = 'NON-RIDDOR'  THEN 'No' ELSE 'Yes' END) as \"Riddor\", (CASE when ambulance = 0 or ambulance is null THEN 'No' ELSE 'Yes' END) as \"Ambulance\", (CASE when party_injured = 0 or party_injured is null THEN 'No' ELSE 'Yes' END) as \"Injured\", (CASE when first_aid = 0 or first_aid is null  THEN 'No' ELSE 'Yes' END) as \"First Aid\" ";

//$sql .= "from acc_accident left join acc_injured_party on acc_accident.injuredid = acc_injured_party.injuredid ";
$sql .= " from acc_accident left join usr u on acc_accident.userid = u.userid ";
$sql .= " left join usr_assoc a on acc_accident.assoc_userid = a.assoc_userid ";

$sql .= " left join acc_accident_site on acc_accident.siteid = acc_accident_site.siteid ";



$sql .= "WHERE acc_accident.state = 'COMPLETE' AND CAST(accident_time AS DATE) >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND CAST(accident_time AS DATE) <= '".formatDatabaseInputDate($arrayParams["enddate"])."' and acc_accident.accountid = $accountid ";

//if ($arrayParams["accountname"] != "All") {
	//$sql .= " AND acc_accident.accountid = ". $arrayParams["accountid"];
//}

$sql .= $reporterQry;
$sql .= " ORDER BY \"Injured Party\"";
//echo $sql;
	    	
?>