<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

/**
 * accident report editing functionality
 *
 */
// make this a secure page
$secure = true;
require('../../shared/startPage.php');

$pageFiles->addModFunc('bump_list');
$pageFiles->addCoreLibPhp('funcDb/funcDbExForm.php');
$pageFiles->includePhp();
$menuPage = 'index.php';
$pageVars = new loadVars($clsUS, $con);

// Get current month and year number
$reportDate = null;
$m = date('n');
$y = date('Y');
$formDB = null;

// List Page Var Properties
// $Arr = (array)$pageVars;
// echo '<pre>';
// print_r ($Arr);
// echo '</pre>';

if ($pageVars->hacking)
    headerLocation($rootUrl.'#/index.php', '');

if (isAllowed('viewAccidents') || isAllowed('recordAccidents')) {

    // Retrieve Bump Report Date (Date must be in format yyy-mm-dd)
    if (isset($_GET['reportdate'])) {
        list($y, $m, $d) = explode('-', $_GET['reportdate']);
        if (checkdate($m, $d, $y)) {
            $reportDate = new DateTime($_GET['reportdate']);
        }
    }
    else {
        // Retrieve Bump Report Date from POST
        if (isset($_POST['reportdate'])) {
            list($y, $m, $d) = explode('-', $_POST['reportdate']);
            if (checkdate($m, $d, $y)) {
                $reportDate = new DateTime($_POST['reportdate']);
            }
        }
    }

    // Save Report?
    if (buttonClicked('save')) {

        $saveForm = saveBumpReport($con, $_POST, $pageVars, $clsUS);
        if ($saveForm == '') {
            $messages[] = 'Bump Report Saved';
            headerLocation($rootUrl.'/modules/accidents/bump.php?m='.$m.'&y='.$y, $messages);
        }
        else {
            $messages[] = $saveForm;
        }
    }
    else {
        if (buttonClicked('delete')) {
            // TODO: Delete current bump report
            $deleteForm = deleteBumpReport($con, $_POST);
            headerLocation($rootUrl.'/modules/accidents/bump.php?m='.$m.'&y='.$y, $messages);
        }
        else {
            if (buttonClicked('cancel')) {
                headerLocation($rootUrl.'/modules/accidents/bump.php?m='.$m.'&y='.$y, $messages);
            }
            else {
                // LOAD Bump Report record
                if ($reportDate instanceof DateTime) {
                    $accountID = $pageVars->accountid;
                    $formDB = getBumpReport($accountID, $reportDate->format('Y-m-d'), $con);
                }
            }
        }
    }

    // redirect to accident index if $formDB is not set
    if(!isset($formDB)) {
        $messages[] = 'There was an error.';
        headerLocation($rootUrl.'/modules/accidents/bump.php?m='.$m.'&y='.$y, $messages);
    }
    else {
        $title = 'Bump Report - '.$reportDate->format('jS F Y');
        $content = 'page.bumpreport.php';
        // $box[] = '';

    }
}
else {
    trigger_error('Access Denied', E_USER_WARNING);
    headerLocation($rootUrl.'/index.php', false);
}

include('../../layout.php');
