<?php

/**
 * accident report editing functionality
 *
 */
####make this a secure page
$secure = true;
require("../../shared/startPage.php");
$pageFiles->addFunc("select_users");

$pageFiles->addModFunc("accident_edit");
$pageFiles->addModFunc("accidents_popup");
$pageFiles->addModFunc('file_upload');
$pageFiles->addModFunc('accident_comments');
if ($accidents_conf['site_tags'])
    $pageFiles->addModFunc('accident_tags');

$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->addModFunc('accident_hooks');
$pageFiles->includePhp();
$menuPage = "";

$pageVars = new loadVars($clsUS, $con);
if ($pageVars->hacking)
    headerLocation("$rootUrl/index.php", "");

$acc_comment = getStrFromRequest("acc_comment");
$report_id = getStrFromRequest("report_id");

if (isset($_POST['params'])) {
	$params = $_POST['params'];
} else if(isset($_GET['params'])) {
	$params = $_GET['params'];
}

$acc_id = $pageVars->accidentid; //used to get initial accidentid. - so we can test if it was 0 after an insert (for acc_comments)

if ($pageVars->accidentid > 0) {
	$pageFiles->addJsVar('item_id', $pageVars->accidentid);
}
/* get arrays forauto complete - if these get too big then we need to use ajax instead */

if (isAllowed("recordAccidents")) {
    //$box[] = "box.hse_info.php";
 
    if($pageVars->accidentid !== null)
    {
        $formDB = getAccident($con, $pageVars);
        //get injuries
        $injuryDB = getInjuries($con, $pageVars);
        $fAiderDB = getFirstAiders($con, $pageVars);
    }    

    $box[] = "box.file_view.php";
    $content = "page.dashboard_accident_view.php";

    $box[] = "box.view_comments.php";

    $title = "View Incident Report";

} else {
    trigger_error("Access Denied", E_USER_WARNING);

    headerLocation("$rootUrl/index.php", false);
}


include("../../layout.php");
