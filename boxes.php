 <?php 
 
	if (isset($pageFiles) && $pageFiles->box_priority) {
		if (isset($pageFiles) && is_array($pageFiles->box)) {
			foreach($pageFiles->box as $key => $value) {
				include ($value);
				if ($debug && isset($value)) {
					echo "<span class=\"debug\">[box: ".$value."]</span>";
				}
			}
		}
		if (isset($box) && is_array($box)) {
			foreach($box as $key => $value) {
				include ("boxes/$value");
				if ($debug && isset($value)) {
					echo "<span class=\"debug\">[box: ".$value."]</span>";
				}
			}
		}
	} else {
		if (isset($box) && is_array($box)) {
			foreach($box as $key => $value) {
				include ("boxes/$value");
				if ($debug && isset($value)) {
					echo "<span class=\"debug\">[box: ".$value."]</span>";
				}
			}
		}
		if (isset($pageFiles) && is_array($pageFiles->box)) {
			foreach($pageFiles->box as $key => $value) {
				include ($value);
				if ($debug && isset($value)) {
					echo "<span class=\"debug\">[box: ".$value."]</span>";
				}
			}
		}	
	}
		

?>