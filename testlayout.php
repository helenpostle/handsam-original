<?php 
/**
* Layout Page
* 
* This is a complete HTML Page which sets out the page structure
* Other files are included at various points to set the actual content
*/
#if $this_page not set in  main page code set as empty - used for active menu highlight
if (!isset($this_page)) {
	$this_page = "";
}

#sets content div to normal or wide depending on page 
if (!isset($contentId)) {
	$contentId = "content";
}

//initiate menu class
//we do this before the secure page so that the menu still works on the login screen
require_once("$rootPath/handsam_core/library/clsMenu/clsMenu.php");

if (!isset($clsMenu)) {
	$clsMenu = new clsMenu($rootUrl);
}

require_once("$rootPath/shared/menu.php");
require_once("$rootPath/handsam_core/shared/menu.php");
$msg = '';
//now include all module menu includes
foreach ($installed_modules as $mod) {
	require_once("$rootPath/modules/$mod/shared/menu.php");	
	$msg .= "<br />this module is ".$mod;
}

$metakey = "";
$metadesc = "";

//Include the box layout page too
include("boxes/boxlayout.php");

//add all system wide css files:

$pageFiles->addSystemCss("system_1.0.css");
$pageFiles->addSystemCss("tables.content.css");
$pageFiles->addSystemCss("tables.box.css");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<?php include("installations/$installation/layout/head.php"); ?>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<meta http-equiv="Cache-Control" content="no-store,no-cache"/>
	<?php
	//include css files
	if (isset($pageFiles)) $pageFiles->includeCss();

	?>
	
	<?php
	if (isAllowed("editContent")) {
		?>
		<link rel="stylesheet" href="<?php echo $rootUrl;?>/css/admin.css?v=<?php echo $css_version;?>" type="text/css" />
		<?php 
	}
	
	#add safari stylesheet here
	/*
	if (!(strpos($_SERVER['HTTP_USER_AGENT'],'Safari') === false)) { ?>
		<link rel="stylesheet" href="<?php echo $rootUrl;?>/css/safari.css" type="text/css" />
	<?php
	}
	*/
	?>
	
	<script type="text/javascript">
		//set site root from php variable in shared/config.php
		var siteRoot = '<?php echo $rootUrl;?>';
		<?php
		/*
		//set autologout time for different user types
		if (isAllowed("editAccount")) {
			echo "var autoLogoutTime = 1800000";
		} else {
			echo "var autoLogoutTime = 300000";
			//echo "var autoLogoutTime = 30000";
		}
		*/
		$pageFiles->includeJSConf();
		?>
	</script>
	
	<?php
	if (isset($pageFiles)) $pageFiles->loadJsVar();
	?>
        
	<?php include("js/javascript.php"); 
	$userAgent = "";
	if (isset($_SERVER['HTTP_USER_AGENT'])) $userAgent = $_SERVER['HTTP_USER_AGENT'];
	$os = "Mac OS X";
	$version = "";
	$osTest = strpos($userAgent, $os);
	if ($osTest) {
		?>
		<link rel="stylesheet" href="<?php echo $rootUrl;?>/css/mac.css?v=<?php echo $css_version;?>" type="text/css" />
		<?php
	}
	?>
<?php
//include js files
if (isset($pageFiles)) $pageFiles->includeJs();
?>

<script>    
    // unread news alert
<?php 
if($core_conf['unread_news_able'] === true && count($unreadnews) > 0) :                
    echo 'window.unreadnews = '.json_encode($unreadnews).";\r\n";
    // js for opening the popup.
    if($core_conf['unread_news_must_read']===true)
    {
      include('handsam_core/functionality/news_check/js/unread_must_read.php');
    }
    else
    {
      include('handsam_core/functionality/news_check/js/unread.php');
    }
endif;                
?>
</script> 
                
</head>
<?php if ($debug) { ?>
<span class="debug">[Database: <?php echo $dbname ?>]<br/>
[Host: <?php echo $dbhost ?>]<br/>
[Browser: <?php echo $_SERVER['HTTP_USER_AGENT'];?>]<br/>
</span>
<?php } ?>
<body class="<?php if (isset($body_class)) echo $body_class;?>">
<?php
if(isset($core_conf['loading_page_img']) && $core_conf['loading_page_img'] && isLoggedIn()) {
	echo "<div id='loader'><img src='$rootUrl/images/loader5.gif'/></div>";
}
include("installations/$installation/layout/branding.php");
?>

<div id="wrapper" class="clearfix">
	
	
     	
    <div id="navigation"> 	
    
		<?php
		//display menu
		if (isLoggedIn()) {	
			$clsMenu->addMenuItem("/resources.php", "HINTS &amp; RESOURCES", "", "");
		}
		include("installations/$installation/layout/menu.php");
		?>

	</div>
	<?php
	if (($this_page == "terms.php" || $this_page == "about.php") && !((isset($box) && is_array($box) && count($box) > 0) || (isset($pageFiles) && is_array($pageFiles->box) && count($pageFiles->box) > 0))) {
		$contentId = "contentWide";
	}
	?>

	
	<div id="content_wrapper" class="clearfix <?php if (isset($content) && $content == "login.php") echo "login";?>">
    	<div id="<?php echo $contentId;?>">
    	<?php 
    	//echo $pageFiles->module;
    	?>
		<?php 
		include ("$rootPath/content.php");
		echo $msg;
		?>
   		</div>
 		<?php 
	if((isset($box) && is_array($box) && count($box) > 0) || (isset($pageFiles) && is_array($pageFiles->box) && count($pageFiles->box) > 0)) {
 		?>   
 		<div id="boxes">
			<?php include ("$rootPath/boxes.php"); 
			?>
	    </div>
	    <?php 
	}
	?>
	</div>   
</div>
<div id="footer" class="clearfix">
<?php include("installations/$installation/layout/footer.php"); ?>
</div>
	<?php if ($debug) { ?>
		<div id="sql">
			<?php if (isset($arraySQL) && is_array($arraySQL)) {
					foreach($arraySQL as $key => $value) {
						if (strpos($value,"FAILED",0) === 0) {
							echo "<p><b>".nl2br($value)."</b></p><hr/>";
						} else {
							echo "<p>".nl2br($value)."</p><hr/>";
						}
					}
				}
		?>
		</div>
		<div id="clsUS">
			<?php 
			if (isset($clsUS)) {
				var_dump($clsUS);
			}
			?>
		</div>
		
		<div id="css">
			<p>Loaded css files</p>
			<?php
				for ($a=0; $a<count($pageFiles->cssFileLoaded); $a++) {
					echo "<p>".$pageFiles->cssFileLoaded[$a]."<p>";
				}
			?>
			<p>Not Loaded css files</p>
			<?php
				for ($a=0; $a<count($pageFiles->cssFileMissing); $a++) {
					echo "<p style=\"color:red;\">".$pageFiles->cssFileMissing[$a]."<p>";
				}
			?>
		</div>
	<?php } 
	//now record any debugging in the debugging table.
	if (isset($pageVars) && isset($clsUS)) {
		debugLog($con, $clsUS, $pageVars, $arraySQL, "", "", $errorFlag);
	} else if (isset($clsUS)) {
		debugLog($con, $clsUS, "", $arraySQL, "", "", $errorFlag);
	}
require_once("$rootPath/handsam_core/library/security/csrf.php");
	
if(isset($core_conf['loading_page_img']) && $core_conf['loading_page_img'] && isLoggedIn()) {?>
	<script type="text/javascript">
	    if ($.browser.safari) {
			$('#wrapper').show();
	        $('#loader').hide();
      	} else {
			$('#wrapper').fadeIn("slow", function() {
	        	$('#loader').fadeOut("slow", function() {
		        	$(this).remove();
  				});
	      	});
      	}
		
	</script>
	<?php
}
?>
</body>
</html>