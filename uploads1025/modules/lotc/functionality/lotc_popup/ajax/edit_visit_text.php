<?php

####make this a secure page
$secure = true;
$ajaxModule = "lotc";

require("../../../../../shared/startPage.php");
/*
  require("../library/securePage.php");

  ####include other functions
  include("../library/funcRequest.php");
  include("../library/funcDisplay.php");
  include("../library/funcDbEdit.php");
  include("../library/funcDbForm.php");
  include("../lotc/library/clsLoadVars.php");
  include("../lotc/library/clsPageQry.php");

  include("../lotc/library/funcLotcProcessForm.php");
  include("../lotc/library/funcLotcDisplayForm.php");
  include("../lotc/library/funcLotcDisplayVisit.php");
  include("../lotc/library/funcLotcVisits.php");
 */

$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_process_form');


$pageFiles->includePhp();

$pageVars = new loadVars($clsUS, $con);

$tkn = getStrFromRequest("tkn");

if (isAllowed("planVisits")) {
    if ($pageVars->visitid !== null) {

        $visittext = getStrFromRequest("visittext");
        $visitname = getStrFromRequest("visitname");
        $load = getStrFromRequest("load");

        $qry = "select a_visit.*  from a_visit where visitid = {$pageVars->visitid}";
        $pageDb = new pageQry($con, $qry);
        $pageDb->rsItem();

        //for use in determining if the visit date can be edited still. If any stage has been confirmed, then the date cannot be edited
        $visit_state = visit_state($con, $pageVars->visitid, "APPROVED EVC");

        if (buttonClicked("save")) {

            $visit_date = getStrFromRequest("visit_date");

            //date time so add hrs and mins to date field
            $hrs = $_POST["visit_date_hrs"];
            $hrs = sprintf("%02d", $hrs);
            $mins = $_POST["visit_date_mins"];
            $mins = sprintf("%02d", $mins);
            $visit_date = "$visit_date {$hrs}:{$mins}:00";

            $visit_end = getStrFromRequest("visit_end");

            $leaderid = (int) $_POST['leaderid'];

            //date time so add hrs and mins to date field
            $hrs = $_POST["visit_end_hrs"];
            $hrs = sprintf("%02d", $hrs);
            $mins = $_POST["visit_end_mins"];
            $mins = sprintf("%02d", $mins);
            $visit_end = "$visit_end {$hrs}:{$mins}:00";

            $qry = new dbUpdate("a_visit");
            $qry->setParam("visitid", $pageVars->visitid);
            //if ($visit_state->cnt == 0) {
            $qry->setDateTimeVal("visit_date", $visit_date, "Visit Start time and date");
            $qry->setDateTimeVal("visit_end", $visit_end, "Visit End time and date");
            //}
            $qry->setReqStringVal("visitname", $visitname, "visit name");
            $qry->setStringVal("visittext", $visittext, "visit text");
            $qry->setAudit($clsUS->userid);
            //if (dataInVisit($con, $pageVars->visitid)) {
            //$qry->setReqNumberVal("planid",$pageVars->planid,"planid");
            //}

            if (!empty($leaderid)) {

                $qry->setNumberVal("leaderid", $leaderid, 'Leader ID');
            }

            if (!isset($_POST['year_group_id']) || (isset($_POST['year_group_id']) && $_POST['year_group_id'] < 1) || (isset($_POST['year_group_id']) && is_array($_POST['year_group_id']) && count($_POST['year_group_id']) == 0)) {
                $qry->failError .= "Please select at least one year group.\r\n";
            }

            if ($qry->execute($con)) {


                //now record yeargroup for this visit
                if (isset($_POST['year_group_id']) && is_array($_POST['year_group_id']) && count($_POST['year_group_id']) > 0) {
                    //first delete all years that aren't in the post
                    $del_ids = implode(",", $_POST['year_group_id']);
                    $del_qry = "delete from a_visit_year_group where visitid = {$pageVars->visitid} and year_group_id not in($del_ids)";
                    $del_exec = execSQL($con, $del_qry);
                    //now remove any already in db from post
                    $sql = "select * from a_visit_year_group where visitid = {$pageVars->visitid}";
                    $rs = getRS($con, $sql);
                    while ($row = getRow($rs)) {
                        if (in_array($row['year_group_id'], $_POST['year_group_id'])) {
                            $_POST['year_group_id'] = array_diff($_POST['year_group_id'], array($row['year_group_id']));
                        }
                    }
                    //now insert new year groups into db	
                    $year_group_errors = false;
                    foreach ($_POST['year_group_id'] as $post) {
                        $qry2 = new dbInsert("a_visit_year_group");
                        $qry2->setReqNumberVal("visitid", $pageVars->visitid, "visitid");
                        $qry2->setReqNumberVal("year_group_id", $post, "year group id");
                        if (!$qry2->execute($con)) {
                            $year_group_errors = true;
                            $messages[] = $qry2->getError();
                        }
                    }
                }
            } else {
                echo "##ERROR:\n\r";
                echo $qry->getError();
            }
        } else if (buttonClicked("cancel")) {

            echo "cancel";
        } else if (buttonClicked("delete")) {
            $qry = new dbUpdate("a_visit");
            $qry->setParam("visitid", $pageVars->visitid);
            $qry->setReqStringVal("state", "DELETED", "state");
            $qry->setAudit($clsUS->userid);
            if ($qry->execute($con)) {
                echo "deleted";
            } else {
                echo "cancel";
                $messages[] = $qry->getError();
            }
        } else {
            if ($load == 'yes') {
                $qry = "select a_visit.*, concat(usr.firstname,' ',usr.lastname) as leader from a_visit left join usr on  a_visit.leaderid = usr.userid where visitid = {$pageVars->visitid}";
                $pageDb = new pageQry($con, $qry);
                $pageDb->rsItem();
                if ($pageDb->row['visit_date'] != "") {
                    $visit_date = displayLotcDateTime($pageDb->row['visit_date']);
                } else {
                    $visit_date = "Not Set";
                }

                if ($pageDb->row['visit_end'] != "") {
                    $visit_end = displayLotcDateTime($pageDb->row['visit_end']);
                } else {
                    $visit_end = "Not Set";
                }
                //get year groups
                $year_qry = "select year_group.year_text from year_group left join a_visit_year_group on year_group.year_group_id = a_visit_year_group.year_group_id where a_visit_year_group.visitid={$pageVars->visitid}";
                $yearsRs = getRS($con, $year_qry);
                $years_arr = array();
                while ($year_row = getRow($yearsRs)) {
                    $years_arr[] = $year_row['year_text'];
                }
                echo "<div class=\"clearfix\"><strong>Visit Title: </strong><span>{$pageDb->row['visitname']}</span></div><div class=\"clearfix\"><strong>Visit Planner: </strong><span>{$pageDb->row['leader']}</span></div>";
                echo "<div class=\"clearfix year_groups\"><strong>Year Groups: </strong><span>" . implode(",", $years_arr) . "</span></div>";
                echo "<div class=\"clearfix\"><strong>Visit Summary: </strong><span>{$pageDb->row['visittext']}</span></div><div class=\"clearfix narrow\"><strong>Visit Start Date: </strong><span>$visit_date</span><strong>Visit End Date: </strong><span>$visit_end</span></div>";
            } else {
                //list plans
                include("$rootPath/modules/lotc/content/page.visit_edit.php");
            }
        }
    }
}
?>