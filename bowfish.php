<?php
	function getSalt($crypt="blowfish") {
		if ($crypt == "blowfish") {
			$cnt = 22;
		} else {
			$cnt = 9;
		}
		
		$c = explode(" ", ". / a A b B c C d D e E f F g G h H i I j J k K l L m M n N o O p P q Q r R s S t T u U v V w W x X y Y z Z 0 1 2 3 4 5 6 7 8 9");
		$ks = array_rand($c, $cnt);
	
		$s = "";
		foreach($ks as $k) { $s .= $c[$k]; }
	
		return $s;

	
	}
	
	
	
	function hasher($info, $encdata = false, $crypt="blowfish") {
		$strength = "08"; 
		//if encrypted data is passed, check it against input ($info) 
		if ($encdata) {
			if ($crypt == "blowfish") {
				echo "1:".substr($encdata, 0, 60)."<br/>";
				echo "2:".crypt($info, "$2a$".$strength."$".substr($encdata, 60))."<br/>";
				if (substr($encdata, 0, 60) == crypt($info, "$2a$".$strength."$".substr($encdata, 60))) { 
				  return true; 
				} else { 
				  return false; 
				} 
			} else {
				echo "1:".substr($encdata, 0, 34)."<br/>";
				echo "2:".crypt($info, "$1$".substr($encdata, 34))."<br/>";
				if (substr($encdata, 0, 34) == crypt($info, "$1$".substr($encdata, 34))) { 
				  return true; 
				} else { 
				  return false; 
				} 			}
		} else { 
		  //make a salt and hash it with input, and add salt to end 
			$salt = getSalt($crypt);
			echo "salt: $salt<br/>";
			if ($crypt == "blowfish") {
				return crypt($info, "$2a$".$strength."$".$salt).$salt;
			} else {
				return crypt($info, "$1$".$salt).$salt;
			}
		} 
	}
	
	echo CRYPT_BLOWFISH;
	echo "<br/>";
	
	$string1 = "s1lverf0x";
	echo "string: ".$string1;
	echo "<br/>";
	$string2 = hasher($string1, false, "md5");
	echo "string2: ".$string2;
	echo "<br/>";
	if (hasher($string1,$string2, "md5")) {
		echo "true";
	} else {
		echo "false";
	}
	
?>
	
	
	
<?php

echo "hash:";
echo crypt('thereIsAlotofSnowfortheTimeoFyEar!!', "$2a$04$8A4aNQg5dR2dZfCZELthDs$");