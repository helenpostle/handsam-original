 <?php 
/**
 * page that sets the content
 */

if (@ $errorFlag != "") {
	//if an error occured before we got here, display the error page rather than content
	include("content/error.php"); 
} else {
	if (isset($title_crumb)) {
		$title .= $title_crumb;
	}
	
	if (!isset($title) || (	isset($title) && $title == "")) {
		$title = "Welcome to the Handsam System";
	}
	
	if (isset($title) && $title != "") {
		echo "<h1>$title</h1>";
	}
	
	//messages - checks for session_messages from another page and current page messages
	
	
	if (isset($_SESSION["session_messages"]) && is_array($_SESSION["session_messages"])) {
	
		if (isset($messages) && is_array($messages)) {
			$messages = array_merge($messages, $_SESSION["session_messages"]);
		} else {
			$messages = $_SESSION["session_messages"];
		}
		
		unset($_SESSION['session_messages']);
		//session_unregister("session_messages");
			
	}
	if (isset($messages) && is_array($messages)) {
		echo "<div class=\"message_info\"><ul>";
		foreach($messages as $key => $value) {
			if (trim($value) != "") echo "<li class='warning'>".nl2br($value)."</li>";
		}
		echo "</ul></div>";
	}
	
	
	if(isset($content) && $content != "") {
         if (isset($pageFiles) && $pageFiles->module != "") {
	         include ("{$pageFiles->module}/content/$content");
         } else if($content == "login.php") {
	        include("$rootPath/content/login.php");
         } else {
	        include("content/$content"); 
         }

	
		if (@ $errorFlag != "") {
	
			//an error occured in the content page so flag this incase it is important
			//echo("<p class='warning'>WARNING: AN ERROR OCCURRED ON THIS PAGE: $errorFlag</p>");
			echo("<p class='warning'>WARNING: AN ERROR OCCURRED ON THIS PAGE</p>");
		}
	}
}

if ($debug && isset($content)) {
echo "<span class=\"debug\">[content page: \"".$content."\"]</span>";
}
