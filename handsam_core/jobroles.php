<?php

/**
 * Job roles:
 * 	Create, edit and delete job roles for users
 *
 * 	Each role has a title and description
 *
 * 	Presently not limited per account, but code has been commented out for future possibilities
 */

$this_page = "jobroles.php";
 
$secure = true;
 
require("../shared/startPage.php");

$pageFiles->addFunc('jobroles');
$pageFiles->includePhp();

$jobroleid = getIntFromRequest("role_id");

// check permissions 
if($core_conf['jobroles'] === FALSE || !isAllowed("editJobRole"))
{
	// NOPE
	trigger_error("Access Denied", E_USER_WARNING);
	headerLocation("$rootUrl/index.php", false);
}
else // permissions OK...
{
	// if $jobroleid is not specified OR cancel is clicked on the create/edit job role form
	// then show the list of Job Roles
	if ($jobroleid === NULL || buttonClicked("cancel"))
	{
		$content = "jobroleslist.php";
		$title = "Job Roles List";
	}
	else
	{
		// everything checks out, carry on...
		
		// show the form
		$content = "jobrolesedit.php";	
		
		// CRUD to DB
		if(buttonClicked("save"))
		{
			// check that role does not previously exist i.e. is a duplicate
			// $sql = "SELECT role_id FROM core_jobrole WHERE role_title = '".mysql_real_escape_string($_POST['role_title'])."' AND accountid = $accountid;"; // per account
			$sql = "SELECT role_id FROM core_jobrole WHERE role_title = '".mysql_real_escape_string($_POST['role_title'])."';";
			$rsCheck = getRS($con, $sql);
			if (getRow($rsCheck))
			{
				$messages[] = JOBROLETITLE_IN_USE;
			}
			else
			{
				// create a new job role
				if($jobroleid == 0)
				{					
					// not a duplicate...
					$qry = new dbInsert("core_jobrole");
					// $qry->setReqNumberVal("accountid", $accountid, "Account Number"); // per account
				}
				else
				{	// this is an edit
					$qry = new dbUpdate("core_jobrole");
					$qry->setParam("role_id", $jobroleid);
				}
			}
			if (isset($qry)) {
				$qry->setReqStringVal('role_title', $_POST['role_title'], 'Role Title');
				$qry->setReqStringVal('role_desc', $_POST['role_desc'], 'Role Description');
				
				$qry->setReqStringVal('state', $_POST['state'], 'State');
				
				// createdon etc
				$qry->setAudit($clsUS->userid);
				
				if ($qry->execute($con))
				{
					if ($jobroleid == 0) $jobroleid = $qry->getNewID();	
					$messages[] = SAVED;
					// job role is save, show the updated list
					$content = "jobroleslist.php";				
				}
				else
				{
					$messages[] = $qry->getError();	
				}
			}
		}
		elseif(buttonClicked("delete"))
		{
			$qry = new dbUpdate("core_jobrole");
			$qry->setParam('role_id', $jobroleid);
			$qry->setReqStringVal("state", "DELETED", "State");
			
			if (!$qry->execute($con))
			{
				$messages[] = $qry->getError();
			}
			else
			{
				// item deleted, show the list of roles
				$messages[] = DELETED;
				$content = "jobroleslist.php";
				$title = "Job Role List";
			}
		}
	}
}

// Include layout page
include("layout.php");