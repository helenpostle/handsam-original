<?php
/**
* User editing functionality
*
* For admin and super admin users
* content: system_useredit.php, system_userlist.php
* boxes: none
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$this_page = "news_audit.php";

require("../shared/startPage.php");

//$pageFiles->addFunc('user_table');
//$pageFiles->addFunc('user');

$pageFiles->addFunc('news_audit');
$pageFiles->includePhp();

#check if have requested a specific user
$userid = getIntFromRequestArr("userid");
$aid = getIntFromRequestArr("aid");

if (count($aid)==0) {
  $aid = array($clsUS->accountid);
}

$newsid = getIntFromRequestArr("newsid");
$s = getIntFromRequest("s",0);
$title = 'Messages Audit';

$status = 'read';
if($s == 1 || $s == 2)
{
  $status = 'unread';
}

if ((isAllowed("viewNewsAuditSA") && $core_conf['unread_news_audit_SA']) || (isAllowed("viewNewsAudit") && $core_conf['unread_news_audit_account_admin']))
{
  $content = "news_audit.php";
  $audit = new newsAudit($con, $clsUS, $aid, $userid, $newsid);

} else {
	#trying to do something we shouldn't! throw error so it is logged.	
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);

}

include("layout.php");