<?php
/**
* Account editing functionality
* 
* content: accountEdit.php, accountList.php
* boxes:  accountUserList.php, accountlicenceList.php
* 
*/

$this_page = "accounts.php";
/**
 * Include basic stuff
 */
$secure = true;

require("../shared/startPage.php");

####make this a secure page
//$pageFiles->addFunc("jquery_ui");

//$pageFiles->addFunc("accountlist_tables");

$pageFiles->addFunc("account_cluster");
$pageFiles->addFunc("account_cluster_manager");
$pageFiles->includePhp();

#check if have requested a specific account and get other params
$id = getIntFromRequest("id");

if ( isAllowed("editAccount"))
{

  if ($id !== null)
  {	
    $content = "clusterEdit.php";
    $cluster = new AccountCluster($id);
	$cluster_manager = new AccountClusterManager($id);
    $title = "Edit Account Cluster";
    if (buttonClicked("save")) 
    {
      if($cluster->save($con))
      {
        headerLocation("$rootUrl/handsam_core/account_cluster.php",$messages);
      }// else {
	//     headerLocation("$rootUrl/test.php",$messages);
	//  }
//	headerLocation("$rootUrl/handsam_core/test.php",$messages);
    }
    else if(buttonClicked("delete"))
    {
      if ($cluster->delete($con))
      {
        headerLocation("$rootUrl/handsam_core/account_cluster.php",$messages);
      }
    }
    else if(buttonClicked("cancel"))
    {
      headerLocation("$rootUrl/handsam_core/account_cluster.php",$messages);
    }
  } 
  else 
  {
    #no accountid set - so view accountlist instead?
    $content = "clusterList.php";
//    $sql = "select a.*, u.firstname, u.lastname, u.username from account_cluster a left join  usr u on a.manager = u.userid where a.state != 'DELETED'";
	 //new sql because cluster managers now in different table
	 $sql = "select a.*, u.firstname, u.lastname, u.username, acm.manager from account_cluster a join account_cluster_manager acm on acm.cluster_id = a.id left join usr u on acm.manager = u.userid where a.state != 'DELETED'";
//echo "<br />before we add the cluser bit on ".$sql;
	//if they are cluster manager, add where statement
	if(isset($clsUS->account_clusters) && count($clsUS->account_clusters)>0){
	  $sql .= " and a.id in (".implode(',',$clsUS->account_clusters).") ";
	}
	//echo "<br />after checking if they are a cluster manager ".$sql;
	$result = getRS($con, $sql);   
//$result = ''; 
    $title = "Account Cluster list";
    $contentId = "contentWide";	
  }


} 
else
{
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);
}

#Include layout page
include("layout.php");
?>