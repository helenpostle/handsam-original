<?php 
/**
 * Shows a form for updating a user
 */

	echo startFormTable($_SERVER["PHP_SELF"], "hdm_frm_a", "", USERS_FORM_TITLE, USERS_FORM_INFO);
	echo frmHiddenField($userid,"userid");
	if (isset($return)) echo frmHiddenField($return,"return");
	//echo formNotes(USERS_FORM_TITLE, USERS_FORM_INFO);
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	if ($userid == 0) {
		//enter username & password for new user
		echo frmTextField($rowUser,"username",100,"Email",true);
		//echo frmTextField($rowUser,"password",20,"Password",true);
		
		#echo account name if super admin adding to another account
		if (isAllowed("editAccount") && isset($accountid) && $accountid != $clsUS->accountid) {
			$rsAccount = getRS($con,"select accountname from account  where accountid =".$accountid);
			$rowAccount = getRow($rsAccount); 

			echo frmRowNoInput($rowAccount["accountname"],"Account Name");
		} elseif(isAllowed("editAccount") && !isset($accountid) && $accountid != $clsUS->accountid) {
			echo "help";
		}
	} else if(isAllowed("editUsers")){
		echo frmTextField($rowUser,"username",100,"Email",true);
		
		//if ($clsUS->userid == 1 || $clsUS->userid == 2 || $clsUS->userid ==4 || $clsUS->userid == 2921 /*simon*/ || $clsUS->userid ==2409 /*sarahd*/ || $clsUS->userid ==1877 /*simon*/ || $clsUS->userid ==1878 /*kath*/ || $clsUS->userid ==2410 /*sarahm*/ || $clsUS->userid ==2411 /*elizabeth*/) {
		//	echo frmRowNoInput($rowUser["password"],"Password");
		//}
		
		if (isAllowed("handsamOnly")) {
			//echo frmRowNoInput($rowUser["password"],"Password");
		}
	} else  {
		//non editable
		echo frmRowNoInput($rowUser["username"],"Email");
		if (isAllowed("editAccount") && $rowUser['accountid'] != $clsUS->accountid) {
			//non editable
			echo frmRowNoInput($rowUser["accountname"],"Account Name");
		}
	}
	
	echo frmTextField($rowUser,"firstname",50,"First Name",true);
	echo frmTextField($rowUser,"lastname",50,"Last Name",true);
	if (!isset($self) || !$self) {
		echo frmSelectArray($rowUser, "state" , $state, "State" , true)	;
	}
	
	
	
	
	if (isset($accountid)) echo frmHiddenField($accountid,"accountid");
	
	//if ($accountid == $clsUS->accountid and isAllowed("editAccount") && $SA_cnt == 0 && (!isset($self) || !$self)) {
		//superadmin user with no licences then query usertype
		//$SA_usertype_qry = "select usr_usrtype.usertype from usr_usrtype where userid = $userid";
		//$SA_usertype_rs= getRS($con, $SA_usertype_qry);
		//$SA_usertype_row = getRow($SA_usertype_rs);
		//echo frmSelectArrayValue($SA_usertype_row['usertype'], "usertype" , $userTypes, "User Type" , true);
	//} else {
		//if (!isset($self) || !$self || isset($aid) && $aid == $clsUS->accountid && isAllowed("editAccount")) {
			//remove last part of the above condition because we don't wan t anbyone to be able to edit their own usertype.(i think!)
		if (!isset($self) || !$self) {
			//if we are a superadmin user and users are in our account then give the option for superadmin usertypes - only 2 at the moment  - access all or access incident only.
			if (isset($aid) && $aid == $clsUS->accountid && isAllowed("editAccount")) {
				$qryAOpt = "SELECT t.usrtype as usertype FROM usrtype t left join usr_usrtype ut on t.usrtype = ut.usertype left join usr u on u.userid = ut.userid  WHERE u.userid = $userid AND t.appid = 1 and t.tasklistid=1";
				$rsAOpt = getRS($con,$qryAOpt);
				$rowAOpt = getRow($rsAOpt);
				$appUserTypes = array("SuperAdmin" => "Super Admin (access all areas)", "Incident SuperAdmin" => "Super Admin (access Incident Book ONLY)");
				echo frmSelectArrayValue($rowAOpt["usertype"], "sa_usertype" , $appUserTypes, "SuperAdmin usertype" , true);
			}
			//usetypes select boxes
			//first can select a usertype for each tasklist:
			if (isset($aid) && $aid == $clsUS->accountid && isAllowed("editAccount")) {
				if (in_array("tasklist", $installed_modules)) {
					if($userid == 0) {
						$qryTLOpt = "SELECT distinct t.tasklistid ,t.tasklistname, 'None' AS usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid WHERE a.accountid = {$clsUS->accountid}";
					} else {
						$qryTLOpt = "SELECT distinct   t.tasklistid ,t.tasklistname, ut.usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid LEFT JOIN usr u ON u.accountid = a.accountid LEFT OUTER JOIN usr_usrtype ut ON u.userid = ut.userid AND t.tasklistid = ut.tasklistid WHERE u.userid = $userid";
					}
					$rsTLOpt = getRS($con,$qryTLOpt);
					
					while ($rowTLOpt = getRow($rsTLOpt)) {
						echo frmSelectArrayValue($rowTLOpt["usertype"], "usertype_".$rowTLOpt["tasklistid"] , $userTypes, $rowTLOpt["tasklistname"]. " List"  , true);
					}
				}
				
				
			} else {
				if (in_array("tasklist", $installed_modules)) {
					if($userid == 0) {
						$qryTLOpt = "SELECT distinct t.tasklistid ,t.tasklistname, 'None' AS usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid WHERE a.accountid = $accountid";
					} else {
						$qryTLOpt = "SELECT distinct   t.tasklistid ,t.tasklistname, ut.usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid LEFT JOIN usr u ON u.accountid = a.accountid LEFT OUTER JOIN usr_usrtype ut ON u.userid = ut.userid AND t.tasklistid = ut.tasklistid WHERE u.userid = $userid";
					}
					$rsTLOpt = getRS($con,$qryTLOpt);
					
					while ($rowTLOpt = getRow($rsTLOpt)) {
						echo frmSelectArrayValue($rowTLOpt["usertype"], "usertype_".$rowTLOpt["tasklistid"] , $userTypes, $rowTLOpt["tasklistname"]. " List"  , true);
					}
				}
			}
			
			//next can select a usertype for each app  from tasks app
			//first get apps for this account
			
			$qryAcApp = "Select distinct a.appid, a.appname from accountlicence l left join apps a on l.appid = a.appid where l.accountid = $accountid and a.appid != 1 and l.state = 'ACTIVE' and l.end_date > NOW() and l.start_date < NOW()";
			//echo $qryAcApp;
			$rsAcApp = getRS($con, $qryAcApp);
			
			while ($rowAcApp = getRow($rsAcApp)) {
				if ($aid != 1) {
					if($userid == 0) {
						$qryAOpt = "SELECT distinct a.appid ,a.appname, 'None' AS usertype FROM apps a LEFT JOIN accountlicence l ON a.appid = l.appid WHERE l.accountid = $accountid and a.appid = ".$rowAcApp['appid'];
					} else {
						$qryAOpt = "SELECT t.usrtype as usertype FROM usrtype t left join usr_usrtype ut on t.usrtype = ut.usertype left join usr u on u.userid = ut.userid  WHERE u.userid = $userid AND t.appid = ".$rowAcApp['appid'];
					}
					$rsAOpt = getRS($con,$qryAOpt);
					if (getResultRows($rsAOpt)) {
						$rowAOpt = getRow($rsAOpt);
						$qryUserType = "select usrtype from usrtype where appid = ".$rowAcApp['appid'];
						$rsUT = getRS($con, $qryUserType);
						$appUserTypes = array("None" => "No Access");
						while ($rowAUT= getRow($rsUT)) {
							$appUserTypes[$rowAUT['usrtype']] = $rowAUT['usrtype'];
						}
						echo frmSelectArrayValue($rowAOpt["usertype"], "app_usertype_".$rowAcApp["appid"] , $appUserTypes, $rowAcApp["appname"]. " App"  , true);
					} else {
						$qryAOpt = "SELECT distinct a.appid ,a.appname, 'None' AS usertype FROM apps a LEFT JOIN accountlicence l ON a.appid = l.appid WHERE l.accountid = $accountid and a.appid = ".$rowAcApp['appid'];
						$rsAOpt = getRS($con,$qryAOpt);
						$rowAOpt = getRow($rsAOpt);
						$qryUserType = "select usrtype from usrtype where appid = ".$rowAcApp['appid'];
						$rsUT = getRS($con, $qryUserType);
						$appUserTypes = array("None" => "No Access");
						while ($rowAUT= getRow($rsUT)) {
							$appUserTypes[$rowAUT['usrtype']] = $rowAUT['usrtype'];
						}
						echo frmSelectArrayValue($rowAOpt["usertype"], "app_usertype_".$rowAcApp["appid"] , $appUserTypes, $rowAcApp["appname"]. " App"  , true);
					}
					/*
						
					
					
					while ($rowAOpt = getRow($rsAOpt)) {
						$qryUserType = "select usrtype from usrtype where appid = ".$rowAOpt['appid'];
						$rsUT = getRS($con, $qryUserType);
						$appUserTypes = array("None" => "No Access");
						while ($rowAUT= getRow($rsUT)) {
							$appUserTypes[$rowAUT['usrtype']] = $rowAUT['usrtype'];
						}
						echo frmSelectArrayValue($rowAOpt["usertype"], "app_usertype_".$rowAOpt["appid"] , $appUserTypes, $rowAOpt["appname"]. " App"  , true);
					}
					
					*/
				} else {
					$qryAOpt = "SELECT t.usrtype as usertype FROM usrtype t left join usr_usrtype ut on t.usrtype = ut.usertype left join usr u on u.userid = ut.userid  WHERE u.userid = $userid AND t.appid = 1";
					$rowAOpt = getRow($rsAOpt);
					$appUserTypes = array("SuperAdmin" => "Super Admin (access all areas)", "Incident SuperAdmin" => "Super Admin (access Incident Book ONLY)");
					echo frmSelectArrayValue($rowAOpt["usertype"], "app_usertype_1" , $appUserTypes, "SuperAdmin usertype" , true);

				}
			}
			
			
			
		}
	//}

	if ($userid != 0 && isAllowed("editUsers")) {
		echo frmShowAudit($rowUser,$con);	
	}
	
	echo frmRowNoInput($rowUser["logincnt"], "Log in count");
	
	if ($rowUser["lastlogin"] != NULL) {
		echo frmRowNoInput(displayDateTime($rowUser["lastlogin"]), "Last logged in");
	}
	
	
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($userid != 0 && (!isset($self) || !$self))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this user?", "Click here to delete this user");
	
	echo endFormTable();
	
	?>


