<?php
/**
* User editing functionality
*
* For admin and super admin users
* content: useredit.php, userlist.php
* boxes: none
*/
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../shared/startPage.php");

$pageFiles->addFunc('email_alert_checkbox');
$pageFiles->addFunc('user_table');
$pageFiles->addFunc('user');
$pageFiles->addFunc('email');

$pageFiles->includePhp();

#check if have requested a specific user
$userid = getIntFromRequest("userid2");
if ($userid === null) $userid = getIntFromRequest("userid");
$aid = getIntFromRequest("accountid2");
if ($aid === null) $aid = getIntFromRequest("accountid");
$return = getStrFromRequest("return");

if ($userid == $clsUS->userid) {
	$self = true;
} else {
	$self = false;
}



if ($self) {
	$title = "My Details";
} else {
	$title = "Edit User";
}

#set which user list includes file to include
$userlist = 'userlist.php';
if(isAllowed("editAccount")) $userlist = 'superadmin_list.php';

if ($userid !== null && (isAllowed("editUsers") || $self)) {
	
	#we can do this if it is ourself or we have permission to edit other users.
	$content = "useredit.php";
	$sqlUser = "select usr.*, usr_usrtype.usertype, account.accountname from usr left join account on usr.accountid = account.accountid left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.userid = $userid";
	//$sqlUser = "select usr.*, usr_usrtype.usertype, usr_usrtype.tasklistid, account.accountname from usr left join account on usr.accountid = account.accountid left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.userid = $userid";
	$rsUser = getRS($con,$sqlUser);
	$rowUser = getRow($rsUser); 
	if ((isAllowed("editAccount")) || (isAllowed("editUsers") && $userid == 0) || (isAllowed("editUsers") && $rowUser["accountid"] == $clsUS->accountid) || $self) {
	
		if ($userid != 0) {
			$aid = $rowUser["accountid"];
		}
	
		//check if the accountid is an admin account
		if ($aid == $clsUS->accountid && isAllowed("editAccount")) {
			$admin_account_user = true;
			//this is the superadmin account so do a check for licences for this account
			/*
			$SA_qry = "select distinct appid from accountlicence where accountlicence.accountid = $aid ";
			$SA_rs = getRS($con, $SA_qry);
			$SA_cnt = getResultRows($SA_rs);
			*/
		} else {
			$admin_account_user = false;
		}


		
		
		
		
		#work out account id	
		if (isAllowed("editAccount") && isset($aid)) {
			$accountid = $aid;
		} else {
			$accountid = $clsUS->accountid;	
		}
		
		#which user types
		if ($self && !isAllowed("editAccount")) {
			$userTypes = array();
		} else {
			if (isAllowed("editHandsamSuperAdmin") && $accountid == $clsUS->accountid) {
				$userTypes = array("None" => "No Access", "HandsamSuperAdmin" => "HandsamSuperAdmin", "SuperAdmin" => "SuperAdmin");
				
			} elseif (isAllowed("editAccount") && isset($aid) && $aid == $clsUS->accountid) {
				$userTypes = array("None" => "No Access", "SuperAdmin" => "SuperAdmin");
			} elseif (isAllowed("editAccount") && isset($aid)) {
				$userTypes = array("None" => "No Access", "Administrator" => "Administrator", "User" => "User");
			} elseif (isAllowed("editAccount")) {
				$userTypes = array("SuperAdmin" => "SuperAdmin");
			} else {
				$userTypes = array("None" => "No Access", "Administrator" => "Administrator", "User" => "User");
			}
				
		} 
		
		if (buttonClicked("save") && editUserCheck($clsUS, $userid, $accountid, $con)) {
			if ($userid == 0) {
				#check that a user doesn't exist with this name
				$rsCheck = getRS($con,"select * from usr where username = '".$_POST["username"]."'");
				if (getRow($rsCheck)) {
					$messages[] = USERNAME_IN_USE;	
				} else {			
					$qry = new dbInsert("usr");
					$qry->setReqNumberVal("accountid",$accountid,"Account id");
					
				}
			} else {
				$qry = new dbUpdate("usr");
				$qry->setParam("userid",$userid);
			}
			
			if (isset($qry)) {
				
				if (isAllowed("editUsers")) {	
					$qry->setReqEmailVal("username",$_POST["username"],"Email Username");
				}
				$qry->setReqStringVal("firstname",$_POST["firstname"],"First Name");
				$qry->setReqStringVal("lastname",$_POST["lastname"],"Last Name");
				if (!$self) {
					$qry->setReqStringVal("state",$_POST["state"],"State");
				}
				$qry->setAudit($clsUS->userid);
				if ($qry->execute($con)) {
					if ($userid == 0) {
						//email them their first random password
						$passwd = new clsUserPassword($homePath, $password_arr);
						$messages[] = $passwd->emailPassword($_POST["username"], $con, $adminMail);

						$userid = $qry->getNewID();
						
						//now add their unique_string
						$qry = new dbUpdate('usr');
						$qry->setParam("userid",$userid);
						$qry->setNullParam('unique_string');
						$qry->setStringParam('unique_string',"", "or");
						$qry->setTableColVal('unique_string', 'concat(username, accountid, firstname, userid)', 'unique string');
						if (!$qry->execute($con)) {
							$messages[] = $qry->getError();
						}
											
					}
					if (!$self) {
						execSQL($con,"delete from usr_usrtype where userid = $userid");
						if ($admin_account_user) {
							$usrtype = 'SuperAdmin';
							if (isAllowed("editHandsamSuperAdmin")) {
								$usrtype = getStrFromRequest("usertype");
							} else if(isAllowed("editAccount")){
								$usrtype = getStrFromRequest("sa_usertype");
							}
							execSQL($con,"insert into usr_usrtype (userid, tasklistid, usertype) values ($userid,1,'$usrtype')");
							$usrtype = '';						
							
						}
						if (in_array("tasklist", $installed_modules)) {
								$qryTL = "select distinct tasklist.tasklistid,tasklist.tasklistname from tasklist left join accountlicence on tasklist.tasklistid = accountlicence.tasklistid where accountlicence.accountid = $accountid AND accountlicence.state = 'ACTIVE' AND accountlicence.end_date > NOW()";
								$rsTL = getRS($con,$qryTL);
								
								while ($rowTL = getRow($rsTL)) {
									$usrtype = getStrFromRequest("usertype_".$rowTL["tasklistid"]);	
									if ($usrtype != "None") {
										execSQL($con,"insert into usr_usrtype (userid, tasklistid, usertype) values ($userid,".$rowTL["tasklistid"].",'$usrtype')");
									}
									
								}
						}
						
						//now app users except tasks app
						$qryTL = "select distinct appid from accountlicence where accountlicence.accountid = $accountid and appid != 1";
						$rsTL = getRS($con,$qryTL);
						while ($rowTL = getRow($rsTL)) {
							$usrtype = getStrFromRequest("app_usertype_".$rowTL["appid"]);	
							if ($usrtype != "None") {
								execSQL($con,"insert into usr_usrtype (userid, usertype) values ($userid,'$usrtype')");
							}
							
						}
					}								
					$messages[] = SAVED;
					
					$contentId = "contentWide";
					$content = $userlist;
					if (isAllowed("editAccount") && $return == "users") {
						//do nothing else
					} else if($admin_account_user) {
						headerLocation("my_account.php",true );

					} else if ($accountid != $clsUS->accountid) {
						headerLocation("accounts.php?accountid=$accountid", true);
						
					} else if ($return == "myDetails") {
						$content = "useredit.php";
					}
				} else {
					$messages[] = $qry->getError();	
					//$messages[] = $qry->getSql();	
				}
			}
		} else if (buttonClicked("delete")) {
			#cannot delete self
			if (!$self && editUserCheck($clsUS, $userid, $accountid, $con)) {
				$qry = new dbUpdate("usr");
				$qry->setParam("userid",$userid);
				$qry->setReqStringVal("state","DELETED","State");
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#change content to list
					$messages[] = DELETED;
					$contentId = "contentWide";
					$content = $userlist;
					$title = "User List";
					if (isAllowed("editAccount") && $return == "users") {
						//do nothing else
					} else if($admin_account_user) {
						headerLocation("my_account.php",true );
					} elseif ($accountid != $clsUS->accountid) {
						headerLocation("accounts.php?accountid=$accountid", true);
					}
				}
			} else {
				trigger_error("Access Denied",E_USER_WARNING);
			}
	 	} else if (buttonClicked("cancel")) {
			$contentId = "contentWide";
			$content = $userlist;
			$title = "User List";
			if (isAllowed("editAccount") && $return == "users") {
				//do nothing else
			} else if($admin_account_user) {
				headerLocation("my_account.php",true );
			} else if ($accountid != $clsUS->accountid) {
				headerLocation("accounts.php?accountid=$accountid",true );
			} else if ($return == "myDetails") {
				$content = "useredit.php";
			}
		} else if (buttonClicked("reset_pass")) {
			$passwd = new clsUserPassword($homePath, $password_arr);
			$messages[] = $passwd->emailPassword($rowUser['username'], $con, $adminMail);

		}
		if (!$self && $userid>0 && $content == "useredit.php") $pageFiles->addBox("$rootPath/handsam_core/boxes/box.accounts.reset_user_password.php");

	} else {
		$messages[] = NOT_EDIT_USER;
		headerLocation("users.php", $messages);
	}
	
} else if (isAllowed("editUsers")) {
	#no userid set - are we allowed to view the userlist?
	$contentId = "contentWide";
	$content = $userlist;
	$title = "User List";	
} else {
	#trying to do something we shouldn't! throw error so it is logged.	
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);
}

#Include layout page
if (isset($userid) && $userid == $clsUS->userid && $return == "myDetails") {
	$this_page = "users.php?userid=".$userid."&return=myDetails";
} else {
	$this_page = "users.php";
}
include("layout.php");
?>