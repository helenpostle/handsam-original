<?php
$this->addCoreLibPhp("funcDb/funcDbForm.php");
$this->addCoreLibPhp("fckeditor/fckeditor.php");

$this->addFuncCss("forms_1.34.css");

$this->addFuncJs("form.cal_2.0.js");
$this->addFuncJs("ie.select_width.js");
$this->addFunc("jquery_ui");

$this->csrf_form = 'form';

?>