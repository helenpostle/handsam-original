$(document).ready(function(){
	
	
	function cleanDatepicker() {
	   var old_fn = $.datepicker._updateDatepicker;
	
	   $.datepicker._updateDatepicker = function(inst) {
	      old_fn.call(this, inst);
	
	      var buttonPane = $(this).datepicker("widget").find(".ui-datepicker-buttonpane");
	
	      $("<button type='button' class='ui-datepicker-clean ui-state-default ui-priority-primary ui-corner-all'>Usun</button>").appendTo(buttonPane).click(function(ev) {
	          $.datepicker._clearDate(inst.input);
	      }) ;
	   }
	}
	
	
	
	//cleanDatepicker();
	
	/*date*/
	$( "input.date" ).datepicker({ 
		dateFormat: 'D, dd M yy', 
		showButtonPanel: true,
		beforeShow: function( input ) {
		setTimeout(function() {
		  var buttonPane = $( input )
		    .datepicker( "widget" )
		    .find( ".ui-datepicker-buttonpane" );
		  
		  $( "<button>", {
		    text: "Clear",
		    click: function() {
		      $.datepicker._clearDate( input );
		    }
		  }).appendTo( buttonPane );
		}, 1 );
		}	
      });
	
	$('input.date').live('click', function() {
		$(this).datepicker({
			dateFormat: 'D, dd M yy',
			showOn:'focus'
		}).focus();
	});
	
	$("img.date_img").live('click', function() { 
		if ($(this).prev("input.date").datepicker({dateFormat: 'D, dd M yy'})) {
			$(this).prev("input.date").datepicker( "show" );
		}
	});	
	
	
	/*date_time*/
	$( "input.date_time" ).datepicker({ 
		dateFormat: 'D, dd M yy', 
		showButtonPanel: true,
		beforeShow: function( input ) {
		setTimeout(function() {
		  var buttonPane = $( input )
		    .datepicker( "widget" )
		    .find( ".ui-datepicker-buttonpane" );
		  
		  $( "<button>", {
		    text: "Clear",
		    click: function() {
		      $.datepicker._clearDate( input );
		    }
		  }).appendTo( buttonPane );
		}, 1 );
		}	
      });
	
	
	$('input.date_time').live('click', function() {
		$(this).datepicker({
			dateFormat: 'D, dd M yy',
			showOn:'focus'
		}).focus();
	});


	$("img.date_img").live('click', function() {
		if ($(this).prev("input.date_time").datepicker({dateFormat: 'D, dd M yy'})) {
			$(this).prev("input.date_time").datepicker( "show" );
		}
	});	
	
	
	
});