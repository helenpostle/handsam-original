$(document).ready(function(){

	$.datepicker._gotoToday = function(id) {
	    var target = $(id);
	    var inst = this._getInst(target[0]);
	    if (this._get(inst, 'gotoCurrent') && inst.currentDay) {
	            inst.selectedDay = inst.currentDay;
	            inst.drawMonth = inst.selectedMonth = inst.currentMonth;
	            inst.drawYear = inst.selectedYear = inst.currentYear;
	    }
	    else {
	            var date = new Date();
	            inst.selectedDay = date.getDate();
	            inst.drawMonth = inst.selectedMonth = date.getMonth();
	            inst.drawYear = inst.selectedYear = date.getFullYear();
	            this._setDateDatepicker(target, date);
	            this._selectDate(id, this._getDateDatepicker(target));
	    }
	    this._notifyChange(inst);
	    this._adjustDate(target);
	}	
	
		
	/*date*/
	$( "input.date" ).datepicker({ 
		dateFormat: 'D, dd M yy', 
		showButtonPanel: true,
		beforeShow: function( input ) {
		setTimeout(function() {
		  var buttonPane = $( input )
		    .datepicker( "widget" )
		    .find( ".ui-datepicker-buttonpane" );
		  
		  $( "<button>", {
		    text: "Clear",
		    click: function() {
		      $.datepicker._clearDate( input );
		    }
		  }).appendTo( buttonPane );
		}, 1 );
		}	
      });
	
	$('input.date').live('click', function() {
		$(this).datepicker({
			dateFormat: 'D, dd M yy',
			showOn:'focus'
		}).focus();
	});
	
	$("img.date_img").live('click', function() { 
		if ($(this).prev("input.date").datepicker({dateFormat: 'D, dd M yy'})) {
			$(this).prev("input.date").datepicker( "show" );
		}
	});	
	
	
	/*date_time*/
	$( "input.date_time" ).datepicker({ 
		dateFormat: 'D, dd M yy', 
		showButtonPanel: true,
		beforeShow: function( input ) {
		setTimeout(function() {
		  var buttonPane = $( input )
		    .datepicker( "widget" )
		    .find( ".ui-datepicker-buttonpane" );
		  
		  $( "<button>", {
		    text: "Clear",
		    click: function() {
		      $.datepicker._clearDate( input );
		    }
		  }).appendTo( buttonPane );
		}, 1 );
		}	
      });
	
	
	$('input.date_time').live('click', function() {
		$(this).datepicker({
			dateFormat: 'D, dd M yy',
			showOn:'focus'
		}).focus();
	});


	$("img.date_img").live('click', function() {
		if ($(this).prev("input.date_time").datepicker({dateFormat: 'D, dd M yy'})) {
			$(this).prev("input.date_time").datepicker( "show" );
		}
	});	
	
	
	
});