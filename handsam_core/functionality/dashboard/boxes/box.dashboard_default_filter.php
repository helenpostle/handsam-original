<?php 
/**
 * lists lotc changes in form state 
 * 
 */
 
boxTop("Report Filter");

	$filter_array = array();
	
	//get account list	array
		$filter_array[0] = array();
		$qry = "SELECT  accountid AS a, accountname AS b FROM account  WHERE accountid != 1  ORDER BY accountname desc";
		$result = getRS($con, $qry);
		while ($row = getRow($result)) {
			$filter_array[0][$row['a']] = $row['b'];
		}	
	//$filter_array['date'] = array("created" => "Report Date", "accident_date" => "Accident Date");
	//$filter_array['month'] = array(1 => "January", 2 => "February",3 =>  "March", 4 => "April", 5 => "May", 6 => "June", 7 => "July", 8 => "August", 9 => "September", 10 => "October", 11 => "November", 12 => "December");
	
	//year array
	/*
	$this_year = date('Y', time());
	$start_year = 2010;
	//create year and month drop downs
	$filter_array['year'] = array();
	while ($start_year <= $this_year) {
		$filter_array['year'][$start_year] = $start_year;
		$start_year++;
	}
	*/
	$selected = "";
	foreach ($filter_array as $filter_name => $filter_name_array) {
		$qs = $qs_base;
		echo "<div>";
		
		echo "<label for=\"$filter_name\">Account</label>";
		//echo "<label for=\"$filter_name\">$filter_name</label>";
	
		echo "<select id=\"accountid\">";
		//echo "<select id=\"$filter_name\">";
		
		$qs[$filter_name] = 0;
		$filter_link = "?filter=".rawurlencode(serialize($qs));
		if ($filter_name != 'date') {
			echo "<option value=\"$filter_link\">All {$filter_name}s</option>";
		}
		foreach ($filter_array[$filter_name] as $filter_key => $filter_value) {
			$selected = "";
			$qs[$filter_name] = $filter_key;
			if (strval($qs_base[$filter_name]) == strval($filter_key)) { 
				$selected = "selected";
				echo $filter_name." : ".$qs_base[$filter_name]." : ".$filter_key;
			} else {
				$selected = "";
			}
			$filter_link = "?params=".rawurlencode(serialize($qs));
			echo "<option $selected value=\"$filter_link\">$filter_value</option>";
		}
		echo "</select>";
		echo "</div>";
	}
	
boxBottom();
?>
	