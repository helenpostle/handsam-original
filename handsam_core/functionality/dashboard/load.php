<?php
$this->addCoreLibPhp("funcDate/funcDate.php");

$this->addCoreLibPhp("funcReporting/funcReports.php");

$this->addCoreLibJs("jquery/jquery-ui-1.8.14.custom.min.js");
$this->addCoreLibJs("jquery/plugins/jquery.tablesorter.js");
$this->addFuncJs("dashboard_1.2.js");
$this->addFuncCss("dashboard_1.2.css");
?>