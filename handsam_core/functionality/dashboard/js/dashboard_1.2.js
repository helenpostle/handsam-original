$(document).ready(function(){
 
    $('#dashboard #tab_tasklist table.table_1 th select, #dashboard #tab_tasklist table.table_2 th select').change(function() {
        var rep_id = $(this).attr("class");
        $('#dashboard #tab_tasklist table.table_1 td.col_' + rep_id).attr('class', 'col_' + rep_id).html("<a href='#'></a>");
        $('#dashboard #tab_tasklist table.table_2 td.col_' + rep_id).attr('class', 'col_' + rep_id).html("<a href='#'></a>");
        rep_id = rep_id - 1;
        var rep = window.dashboard_report[rep_id];
        rep['params']['catid'] = $(this).val();
        dash_cnt ++;
        addLoading();
        $.ajax({
            url: siteRoot + report[rep_id],
            type: "POST",
            data: { dashboard_report: rep },
            dataType: "json",
            success: function(result) {
                load_cnt ++;
                getTitles(result);
                getValues(result, load_cnt, dash_cnt);

            },
            error: function(jqXHR, textStatus, errorThrown) {
                //alert(errorThrown);
            }
        });
        
    });
    
    $('#dashboard').tabs();
    var dash_cnt = dashboard_report.length;
    var load_cnt = 0;
    if(dashboard_report.length > 0) {
        
        if (report.length > 0) {
            addLoading();
            $.each( report, function( k, v ) {
                $.ajax({
                    url: siteRoot + v,
                    type: "POST",
                    data: { dashboard_report: window.dashboard_report[k] },
                    dataType: "json",
                    success: function(result) {
                        load_cnt ++;
                        getTitles(result);
                        getValues(result, load_cnt, dash_cnt);
                        
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                        //alert(errorThrown);
                    }
                });
                
            });
            
            //formatTables();
        }
    }
    
    $(".table_sort").tablesorter({  
        //sortList: [[0,0]],
 
        debug: false 
    });    
     
});

function addLoading() {
    $('h1').append("<span class=\"loading\">loading</span>");
    
}

function removeLoading() {
    $('span.loading').remove();
}
    
function getTitles(result) {
    if ($('ul.params').find('li').length == 0) {
        $.each(result.title, function(el, val){
            $('ul.params').append('<li><span class="title">' + el + ': </span><span class="value">' + val + '.</span></li>');
        });
    }
}

function getValues(result, load_cnt, dash_cnt) {
    $.each(result.data, function(tab, tabdata){ 
        $.each(tabdata, function(table, row){
            $.each(row, function(rowid, col){
                if (result.type && result.type == 'create') {
                    var rowHtml = $('tr.row_0').html();
                    rowHtml = "<tr class=\"row_" + rowid + "\">" + rowHtml + "</tr>";
                    $('table.table_' + table).append(rowHtml);                        
                }
                $.each(col, function(colid, data){
                    cellValue(tab, table, rowid, colid, data.result, data.link, data.cell_class);
                });  
            });  
        });
        $(".acc_list").trigger("update");
    });
    if (load_cnt == dash_cnt) removeLoading();
}

function cellValue (tab, table, row, col, val, link, cell_class) {
    if (!link) link = '#';
    if (val == null) val = '';
    var cell = $('div#tab_' + tab).find('table.table_' + table).find('tr.row_' + row).find('td.col_' + col);
    cell.html("<a href=\" " + link + "\">" + val + "</a>");
    if (cell_class && cell_class.length > 0) cell.addClass(cell_class);
}