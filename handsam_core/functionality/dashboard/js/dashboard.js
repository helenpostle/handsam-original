$(document).ready(function(){

	//apply class to report cells depending on what's in the cell
		$('td.percent a').each(function(index) {
			var cell_class;
			if ($(this).html() === "") {
				cell_class = "none";
			} else if (parseFloat($(this).html()) < 50) {
			    cell_class = "low";
			} else if (parseFloat($(this).html()) < 75) {
			    cell_class = "med";
		    } else {
			    cell_class = "high";
		    }
		    $(this).parent().addClass(cell_class);
		});
		
		$('td.incident a').each(function(index) {
			var cell_class;
			if (parseFloat($(this).html()) > 0) {
			    cell_class = "incident_red";
			}
		    $(this).parent().addClass(cell_class);
		});
		
		
	$("#acc_list").tablesorter({  

        debug: false 
    });    
 	$("#acc_list2").tablesorter({  
		sortList: [[0,0]],
        debug: false
    });    
    
	$('#accountid').change(function(event){
		alert($(this).val());
		window.location = $(this).val();
	});
     
});



