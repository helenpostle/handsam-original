$(document).ready( function() {
	$('#usertype_filter').change(function(event){
	var usertype = $(this).val();
		window.location = "?usertype=" + usertype;
	});
	

	$("#username_filter_go").click(function() {
		window.location = "?username=" + $("#username_filter").val();
	})
	
	
	//reset username filter
	$('#reset_username').click(function(event){
		window.location = "?username=";
	});	
	
	//first name filter
	$("#firstname_filter_go").click(function() {
		window.location = "?firstname=" + $("#firstname_filter").val();
	})
	
	
	//reset first name filter
	$('#reset_firstname').click(function(event){
		window.location = "?firstname=";
	});	
	
	//last name filter
	$("#lastname_filter_go").click(function() {
		window.location = "?lastname=" + $("#lastname_filter").val();
	})
	
	
	//reset last name filter
	$('#reset_lastname').click(function(event){
		window.location = "?lastname=";
	});	
	
	
	
});