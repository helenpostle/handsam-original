<?php
// News check functionality
// on login checks for any unread news messages.

// unread news class
$this->addFuncPhp('clsNewsAudit.php');
$this->addCoreLibJs("jquery/plugins/jquery.tablesorter.js");
$this->addFuncJs("news_audit.js");
$this->addModuleBox("box.news_audit_filter.php");

$this->addFuncCss('news_audit_1.0.css');
