<?php

// ajax 
// mark news stories as unread

$secure = true;
$ajaxModule = "handsam_core";

require("../../../../shared/startPage.php");
$pageFiles->includePhp();

#check if have requested a specific user

$newsid = getIntFromRequest("newsid");


$sql1 = "SELECT c.contentid, c.title, c.summarytext, c.contenttext, c.created, c.edited 
            FROM content AS c WHERE  c.contentid = $newsid ";
            
$result = getRecSet($sql1);
$row = getRowAssoc($result);
$row['summary'] = showAbstract($row['contenttext'], 250);

echo json_encode($row);
        

exit;