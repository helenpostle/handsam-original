$(document).ready(function()     {         
    $("#user_list").tablesorter({  
        debug: false 
    });
    
    $('#aid').change(function(event){
      var sel = $(this).val();
      var userid = $('#userid').val();
      
      
       $.post(siteRoot + '/handsam_core/functionality/news_audit/ajax/select_users.php', {aid: sel, userid: userid}, function(data){
	  			//$("#inner_popup").html(data);
	  			$("#userid").parent().html(data);
			});	
      
    });
    
    $("#content, #contentWide").prepend("<div id=\"news_dialog\"></div>");
    
    
    $('.news_link').click(function(event){
      var newsid = $(this).attr('rel');
      
      
      $.ajax({
            url: siteRoot + '/handsam_core/functionality/news_audit/ajax/news.php',
            type: "POST",
            data: {newsid: newsid},
            dataType: "json",
            success: function(data) {
               var dateParts = data.created.split(' ')[0].split("-");
               var storydate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
               var html = '<div class="popupstory" style="margin-bottom:18px;"><h2>'+data.title+'</h2><p><em>'+storydate.toDateString()+'</em></p><div class="news_list">'+data.contenttext+' </div></div>';
               $('#news_dialog').html(html).dialog({ 
                  title: 'Message Content',
                  width: 700,
                  height: 630,
                  modal: true,
                  buttons: [
                      {
                      text: "Close",
                      click: function() {
                          $( this ).dialog('close');
                          }
                      }
                  ]
                });	
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                //alert(errorThrown);
            }
        });
      
      return false;   
    });
});
