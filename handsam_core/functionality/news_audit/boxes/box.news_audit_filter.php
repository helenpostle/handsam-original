<?php 
/**
 * lists lotc changes in form state 
 * 
 */
 
boxTop("Message Audit Filters", "news_audit");

echo startSimpleForm($_SERVER["PHP_SELF"]);
echo frmSelectArrayValue($s,'s',array("Read Messages", "Unread Messages", "Unread &amp; Read Messages"),"Status", false);

if (isAllowed("viewNewsAuditSA"))
{
  $sql = "select accountid as a, accountname as b from account where state='ACTIVE' order by accountname asc";
  echo frmMultiSelectQueryArr($aid, 'aid', $sql, $con, 'Account', false);
}
  
$sql = "select usr.userid as a, CONCAT(usr.firstname,' ',usr.lastname) as b from usr  where usr.state='ACTIVE' and usr.accountid in (".implode(',', $aid).") order by usr.lastname, usr.firstname asc";
echo frmMultiSelectQueryArr($userid, 'userid', $sql, $con, 'Users', false);

$sql = "SELECT c.contentid as a, c.title as b FROM content AS c JOIN content AS c2 ON c.parentid = c2.contentid
            WHERE c2.superpage = 'news' AND c.state = 'PUBLISHED' order by c.created desc ";
echo frmMultiSelectQueryArr($newsid, 'newsid', $sql, $con, 'Messages', false);

   


echo frmButton("Apply","apply");
echo endFormTable2();

	
boxBottom();
?>
	