<?php

/**
 * Who has read what
 *
 * @author c slinn
 *          29-01-2014
 */
class newsAudit {
        
    protected $userid = array();
    protected $accountid = array();
    protected $newsid = array();
    protected $paramSql = array();
    
    public $users = array();
    public $read = array();
    public $unread = array();
    public $news = array();
    
    public $rs;
    
    public function __construct($con, $clsUs, $accountid, $userid, $newsid)
    {
      $this->userid = $userid;
      $this->accountid = $accountid;
      $this->newsid = $newsid;
      $this->getParamSql($clsUs);
      $this->getNews($con);
      $this->getRead($con);
      $this->getUsers($con);
      
    }
    
    
    protected function getUsers($con)
    {
      $sql = "select a.accountname, u.lastlogin, u.accountid, u.userid, u.firstname, u.lastname, u.username from usr as u left join account as a on u.accountid = a.accountid where ".$this->paramSql['userid']." and ".$this->paramSql['accountid']." and u.state='ACTIVE' order by lastname, firstname asc";
      $rs = getRS($con,$sql);
      while ($row=getRow($rs))
      {
        $row['read'] = array();
        $row['unread'] = array();
        if (isset($this->read[$row['userid']]))
        {
          $row['read'] = $this->read[$row['userid']];
        }
        //$row['unread'] = array_diff_key($this->news, $row['read']);
        $row['unread'] =  $row['read'] + $this->news;
        $this->users[$row['userid']] = $row;
        
      }
      
    }
    
    protected function getNews($con)
    {
      $sql = "SELECT c.contentid, c.title, c.created 
            FROM content AS c 
            JOIN content AS c2 
                ON c.parentid = c2.contentid
            WHERE c2.superpage = 'news'
            AND ".$this->paramSql['newsid']."
             AND c.state = 'PUBLISHED' ";
      $rs = getRS($con,$sql);
      while ($row=getRow($rs))
      {
        $this->news[$row['contentid']] = $row;
      }
    }
    
    protected function getParamSql($clsUs)
    {
      if(count($this->userid)>0)
      {
        $this->paramSql['userid'] = " u.userid in(".implode(',',$this->userid).") ";
      }
      else
      {
        $this->paramSql['userid'] = " 1=1 ";
      }
      if(count($this->newsid)>0)
      {
        $this->paramSql['newsid'] = " c.contentid in(".implode(',',$this->newsid).") ";
      }
      else
      {
        $this->paramSql['newsid'] = " 1=1 ";
      }
      if(count($this->accountid)>0 && isAllowed("viewNewsAuditSA"))
      {
        $this->paramSql['accountid'] = " u.accountid in(".implode(',',$this->accountid).") ";
      }
      else
      {
        $this->paramSql['accountid'] = " u.accountid = {$clsUs->accountid} ";
      }
    }
    
    public function getRead($con)
    {
      
      $sql = "SELECT c.title, c.created, nu.the_date, nu.user_id, c.contentid 
              FROM content as c left join news_users as nu on nu.content_id = c.contentid
              left join usr as u on nu.user_id = u.userid
              left join account as ac on u.accountid = ac.accountid 
              WHERE ".implode(' and ', $this->paramSql) ."
              ORDER BY nu.the_date;";
      
      $rs = getRS($con, $sql);     
      while ($row = getRow($rs))
      {
        $this->read[$row['user_id']][$row['contentid']] = $row;
      }   
      
      
    }
    
    /**
     * Return the latest story
     * false on error
     */
    public function getUnread($clsUs, $con)
    {
        $qry = "SELECT content.*, usr.username FROM content 
                INNER JOIN usr 
                    ON content.created_by = usr.userid 
                WHERE content.state = 'PUBLISHED' 
                AND superpage = 'news'  
                ORDER BY content.created DESC
                LIMIT 1;";
        $res = getRecSet($qry);
        if($res != false)
        {
            $row = getRowAssoc($res);
            if(!in_array($res->contentid, $this->read_stories))
                return $row;
        }
        
        return $res;
    }
    
   
    
}

