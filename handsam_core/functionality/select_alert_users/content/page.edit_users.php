<?php
//arrays for select boxes:


echo startFormTable($_SERVER["PHP_SELF"],"clearfix", "", "Edit User", "Please edit the details of the user below.  Don't forget to click the 'save' button to save your changes.");
//echo formNotes(PLAN_FORM_TITLE, PLAN_FORM_INFO);


//Personal info
echo frmHiddenField($pageVars->edit_userid,"edit_userid");
echo frmHiddenField($pageVars->edit_usertype,"edit_usertype");

echo "<div class=\"col\">";
	echo frmTextField($formRow,"firstname",100,"First Name",true);
	echo frmTextField($formRow,"lastname",100,"Last Name",true);
	echo frmTextField($formRow,"address1",100,"Address 1",true);
	echo frmTextField($formRow,"address2",100,"Address 2",false);
	echo frmTextField($formRow,"address3",100,"Address 3",false);
	echo frmTextField($formRow,"city",100,"City",false);
echo "</div>";
echo "<div class=\"col\">";
	echo frmTextField($formRow,"postcode",100,"Postcode",true);
	echo frmTextField($formRow,"tel1",100,"Telephone 1",true);
	echo frmTextField($formRow,"tel2",100,"Telephone 2",false);
	echo frmTextField($formRow,"age",100,"Age",true);
	echo frmRadioArray($formRow,"sex",$sex_arr,"Sex");
echo "</div>";

echo frmButtonHelp("Save","save", "Click here to save changes");
echo frmButtonHelp("Cancel","cancel", "Click here to cancel changes");
echo frmHiddenField($tkn,"tkn");

echo endFormTable();

?>