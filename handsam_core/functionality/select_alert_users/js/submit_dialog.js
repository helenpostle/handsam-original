$.extend({


	submit_dialog : function (settings, popupLoadedCalback, callbackfunc, cancel_callbackfunc) {
		settings = jQuery.extend({
			txt: 'none'
		},settings);
		//alert (settings.delete_url);
		//CLOSING POPUP
		//Click the x event!
			
		var popupStatus = 0; 

		/*		
		function loadPopup(){
		//loads popup only if it is disabled
			if(popupStatus==0){
				$("#backgroundPopup").css({
					"opacity": "0.7"
				});
				$("#backgroundPopup").fadeIn("slow");
				$("#popup").fadeIn("slow");
				popupStatus = 1;
				
			}
			$.post(settings.load_url, settings.params, function(data){
	  			$("#inner_popup").html(data);
	  			formSubmit();
			});	

			//$('#inner_popup').load(settings.load_url + settings.params , '', formSubmit);

		}	
		*/
		
		formSubmit();

		function formSubmit() {
	
			if(typeof popupLoadedCalback == 'function'){
				popupLoadedCalback.call(this);
			}
			
			var btn;
			$('#select_users_dialog form input[type=submit]').click(function(){
				btn = '&' + this.name + '=' + this.value;
				//alert("clicked");
	        });			
			$('#select_users_dialog form')
	        .submit(function () { 
		        //alert("submit" + btn);
		        //return false;
				var str = $('#select_users_dialog form').serialize();
				var submit_str = str + btn;
				response = $.ajax({
				      url: settings.load_url,
				      async:		false,
				      global: false,
				      type: "POST",
				      data: submit_str,
				      dataType: "html",
				      success: function(msg){
				         //alert(settings.load_url + "?" + str);
				         
				      }
				   }
				).responseText;
				if (response.search("##ERROR") < 0) {
					if(response == 'cancel') {
						if(typeof cancel_callbackfunc == 'function'){
							cancel_callbackfunc.call(this);
						}
						//disablePopup();
					} else if(response == 'deleted') {
						str += "&load=yes" + response;
						txt = $.ajax({
							  url: settings.load_url,
							  async:		false,
							  global: false,
							  type: "GET",
							  data: str,
							  dataType: "html",
							  success: function(msg){
								 //disablePopup();
								 //alert(settings.load_url + "?" + str);
								 
							  }
						   }
						).responseText;
						alert('Deleted');
						$(settings.elem).html(txt);
						$(this).unbind();
						
						if(typeof callbackfunc == 'function'){
							callbackfunc.call(this);
						}
						/*
						alert('Deleted');
						$(settings.elem).html(txt);
						$(this).unbind();
						
						if(typeof callbackfunc == 'function'){
							callbackfunc.call(this);
						}
						//window.location = settings.delete_url;
						*/
					} else {
						str += "&load=yes" + response;
						//alert(str);
						//alert(settings.load_url);
						txt = $.ajax({
							  url: settings.load_url,
							  async:		false,
							  global: false,
							  type: "POST",
							  data: str,
							  dataType: "html",
							  success: function(msg){
								 //disablePopup();
								 //alert(settings.load_url + "?" + str);
								 
							  }
						   }
						).responseText;
						//alert('Saved');
						//alert(txt);
						//$(settings.elem).html(txt);
						$(this).unbind();
						if(typeof callbackfunc == 'function'){
							callbackfunc.call(this);
						}
					}
				
				} else {

					alert(response.replace('##ERROR:', 'Some details were missing: \n'));

				}
				//$(this).unbind();
				return false; 
			});
		}
		
	}
	
});	