$(document).ready(function() {
	$("#content").prepend("<div id=\"select_users_dialog\"></div>");
	
	$("#select_users_dialog").delegate("td.edit_usr", "click", function(){
		var tab_id = $(this).parentsUntil('div.ui-tabs-panel').parent().attr('id');
		var tab_index = $(this).parentsUntil('div.ui-tabs-panel').parent().index();
		var edit_data = $(this).parent().find("input");
		var edit_userid = edit_data.val();
		var edit_usertype = edit_data.attr("class");
		var qs = {edit_userid: edit_userid, edit_usertype: edit_usertype};
		$.post(siteRoot + "/handsam_core/functionality/select_alert_users/ajax/edit_user.php", {edit_userid: edit_userid, edit_usertype:edit_usertype}, function(data){
			$('#' + tab_id).html(data);
			$.submit_dialog({
				txt: 'text here',
				load_url: siteRoot + '/handsam_core/functionality/select_alert_users/ajax/edit_user.php',
				params: qs,
				delete_url: siteRoot + '/modules/accidents/index.php'
			}, "", function () {
				$.post(siteRoot + "/handsam_core/functionality/select_alert_users/ajax/select_users.php", {tab_num:tab_index}, function(data){
					$('#' + tab_id).html(data);
				});				
			}, function () {
				//alert("cancel");

				$.post(siteRoot + "/handsam_core/functionality/select_alert_users/ajax/select_users.php", {tab_num:tab_index}, function(data){
					$('#' + tab_id).html(data);
				});				
			});
		});	
	});	
});

/**
 *
 * @param array of tabs to load, e.g. users, superadmins, asoccuser, otheruser
 */
function select_users(num, fn, fn_close, actions)
{
	var accountid = $("#accountid").val();
    $.post(siteRoot + "/handsam_core/functionality/select_alert_users/ajax/select_users_sections.php", 
        {
            accountid : accountid,
            data : actions.join(".")
        }, 
        function(data) {
            selectBox.getData(data);
			var the_html = selectBox.render();
			$("#select_users_dialog").html(the_html).dialog({
                //autoOpen: false,
                width: 700,
                height: 700,
                draggable: true, // disable the dialogs default drag we will be using the tabs titlebar instead
                modal: true,
                buttons: {
                    'Close': function() {
                        if (typeof fn_close == "function") {
                            fn_close(num);
                        }  
						$(this).html('');
                        $(this).dialog('close').dialog('destroy'); 
						
                    }
                },
                open: function() {
                    if ($.browser.msie && $.browser.version.substr(0,1)<7) $('#content select').hide();	
                    $('.ui-dialog-titlebar').hide(); // hide the default dialog titlebar
                    if (typeof fn == "function") fn(num);

                    setTimeout(function(){
                        $('#select_users_dialog').dialog('close').dialog('destroy');
                    }, 200000);
                },
                close: function() {
                    if ($.browser.msie && $.browser.version.substr(0,1)<7) $('#content select').show();
                    $('.ui-dialog-titlebar').show(); // in case you have other ui-dialogs on the page, show the titlebar 
                }
            }).parent().draggable().tabs();
    });
}

// model to hold the tab data before 
selectBox = {};

selectBox.tabs = [];

selectBox.content = [];

/**
 * Processes the received JSON from the server
 * json is an array of objects { tab : "Tab Heading", content : "<html>content</html" }
 */
selectBox.getData = function(json)
{	
	this.tabs = [];
	this.content = [];
    data_obj = JSON.parse(json);

    for(i in data_obj)
    {
        this.addTab(data_obj[i])
    }
}

/**
 * add a new tab to the select users popup
 * 
 * tabobj is an object like { tab : "Tab Heading", content : "<p>html content goes here" }
 */
selectBox.addTab = function(tabobj)
{
    this.tabs.push(tabobj.tab);
    this.content.push(tabobj.content);
}

/**
 * Return html formatted for the user select popup
 * 
 * Doesn't actually print to page to perhaps render isn't the best name, but I 
 * claim artistic licence.
 */
selectBox.render = function()
{
    // create the tab headings
    ul_html = '<ul>';
    for(i in this.tabs)
        ul_html += '<li><a href="#tabs-'+i+'">'+this.tabs[i]+'</a></li>';
    ul_html += '</ul>';
    
    cn_html = '';
    for(j in this.content)
    {
        cn_html += '<div id="tabs-'+j+'">';
        cn_html += this.content[j];
        cn_html += '</div>';
    }
    
    return ul_html + cn_html;
}

