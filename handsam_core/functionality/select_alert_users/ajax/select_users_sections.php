<?php
$ajaxModule = "handsam_core";
//error_reporting(E_ALL); 
// ini_set("display_errors", 1); 
require("../../../../shared/startPage.php");

$pageFiles->addCoreLibPhp("clsLoadVars/clsLoadVars.php");

$pageFiles->includePhp();
$pageVars = new loadVars($clsUS,$con);

$tab = getIntFromRequest("tab_num");
$actions = getStrFromRequest('data');
 
$actions = explode('.', $actions); 

//if ($pageVars->hacking) die();
if (isAllowed("selectUsers")) 
{
    $json = array();
    // go through each action, and call the necessary function
    foreach($actions as $usertype)
    {
        $tocall = 'tab_'.$usertype;
        if(function_exists($tocall))
        {
            // return each function's output into the json array
            $json[] = $tocall($tab);
        }
    }
    
    // send back json encoded array.
    echo json_encode($json);    
    exit;
}

/**
 * Gets all the super admins in a tab
 * @return type 
 */
function tab_superadmin()
{ 
    $cols['firstname'] = "First Name";
    $cols['lastname'] = "Last Name";
    $cols['username'] = "Username";
    $qry = "SELECT usr.username, usr.firstname, usr.lastname, usr.userid AS the_id, 1 AS source FROM usr WHERE accountid = '1' and state = 'ACTIVE'  ORDER BY usertype, lastname;";
    
    // array to return
    $json = array();
    // tab heading
    $json['tab'] = 'Super Administrators';
    $json['content'] = tab_table($qry, $cols);
   
    return $json;
}

// gets all the account users in a tab.
function tab_users()
{
    global $pageVars;
    
    $cols['firstname'] = "First Name";
    $cols['lastname'] = "Last Name";
    $cols['username'] = "Username";

    $qry = "SELECT usr.username, usr.firstname, usr.lastname, usr.userid AS the_id, 1 AS source FROM usr WHERE accountid = '".$pageVars->accountid."' and state = 'ACTIVE'  ORDER BY lastname;";
    
    // array to return
    $json = array();
    // tab heading
    $json['tab'] = 'Users';
    $json['content'] = tab_table($qry, $cols);
   
    return $json;

}

// tab of associated users
// @todo finish, not working
function tab_assocusers()
{
    $cols[] = "";

    $qry = "";
    
    // array to return
    $json = array();
    // tab heading
    $json['tab'] = 'Associated Users';
    $json['content'] = tab_table($qry, $cols);
   
    return $json;
}


// tab for other user 
function tab_otheruser()
{
    $html = "<form>";
    $html .= '<p>You may add an user that is not registered on the system to receive alerts.</p>';
    $html .= '<p>Please give a name and email address for the new recipient.</p>';
    $html .= '<label for="username">Name:</label>';
    $html .= '<input type="text" name="user_name" value="" id="user_name" ><br />';
    $html .= '<label for="user_email">Email Address:</label>';
    $html .= '<input type="text" name="user_email" value="" id="user_email" ><br />';
    $html .= '<input type="submit" id="submit_add_user" value="Add user to alert" >';
    $html .= "</form>";

    // array to return
    $json = array();
    // tab heading
    $json['tab'] = 'Other Users';
    $json['content'] = $html;
   
    return $json;
}

/**
 * tab for user roles
 * Gets global roles and account specific
 * @return array with keys 'tab' and 'content'
 */
function tab_jobroles()
{
    global $pageVars;
    
    $cols['role_title'] = 'Role';
    $cols['role_desc'] = 'Description';
    
    /*
	$sql = "SELECT cj.role_id AS the_id, cj.role_title, cj.role_desc, 1 AS source ,
                u.username
            FROM core_jobrole AS cj
            JOIN core_usr_jobrole AS cuj
                ON cuj.jobrole_id = cj.role_id
            JOIN usr AS u ON 
                u.userid = cuj.usr_id
            WHERE cj.accountid = '".$pageVars->accountid."' 
                OR cj.accountid = '0'
            GROUP BY cj.role_id;";
	*/		
    $sql = "SELECT cj.role_id AS the_id, cj.role_title, cj.role_desc, 1 AS source
            FROM core_jobrole AS cj WHERE cj.accountid = '".$pageVars->accountid."' 
                OR cj.accountid = '0' GROUP BY cj.role_id;";			
    
    // array to return
    $json = array();
    $json['tab'] = 'Job Roles';
    $json['content'] = tab_table($sql, $cols, 'select_jobroles_table');
   
    return $json;
}

// tab for other user 
function tab_accountmanager()
{
	global $core_conf;
    $html = '<p>You may add any of the Account Manager types below.</p>';
    $html .= '<p>This means that the respective Account Manager will be emailed the alerts for their accounts</p>';
    // table header
    $html .= '<table  class="select_am_table"><thead><tr>';
    $html .= '<th class="manager_type">Account Manager type</th>';
    $html .= '</tr></thead><tbody>';
	$html .= "<tr><td class=\"sel_am\">".ACCOUNTS_ACCOUNT_MANAGER;
	$html .= '<input type="hidden" name="row_1" value="1" class="" id="row_1"/>';
	$html .= "</td></tr>";
	if ($core_conf['acc_manager_2']) {
		$html .= "<tr><td class=\"sel_am\">".ACCOUNTS_ACCOUNT_MANAGER_2;
		$html .= '<input type="hidden" name="row_2" value="2" class="" id="row_2"/>';
		$html .= "</td></tr>";
	}
	$html .= '</tbody><tfoot><tr><td></td></tr></tfoot></table>';
    // array to return
    $json = array();
    // tab heading
    $json['tab'] = 'Account Managers';
    $json['content'] = $html;
   
    return $json;
}

// formats a table from given sql query and table columns
function tab_table($sql, $cols, $table_class = 'select_users_table')
{
    $rs = getRecSet($sql);
    
    // return value
    $table = '';
    
    // table header
    $table .= '<table  class="'.$table_class.'"><thead><tr>';
    foreach($cols as $colkey => $col) 
        $table .= '<th class="'.$colkey.'">'.$col.'</th>';
    $table .= '</tr></thead><tbody>';
    
    // table body
    
    $cnt = 0;
    while($row = getRow($rs))
    {
        $cnt++;
        $table .= "<tr>";
        foreach ($cols as $colkey => $col) 
        {
            $table .= "<td class=\"sel_usr\">{$row[$colkey]}";
            if ($colkey == 0) 
                $table .= '<input type="hidden" name="row_'.$cnt.'" value="'.$row['the_id'].'" class="'.$row['source'].'" id="row_'.$cnt.'"/>';
            $table .= "</td>";
        }
        $table .= "</tr>";
    }
    
    // table footer
    $table .= '</tbody><tfoot><tr><td colspan="'.count($cols).'"></td><td></td></tr></tfoot></table>';
    
    
    return $table;
}

/*
   When writing a new formatting function, it must follow this convention

function tab_FUNCTION_NAME()
{
    // array to return
    $json = array();
    $json['tab'] = TAB_HEADING;
    $json['content'] = TAB_CONTENT;
   
    return $json;
}


*/