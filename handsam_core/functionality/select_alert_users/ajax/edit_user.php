<?php
$ajaxModule = "handsam_core";
//error_reporting(E_ALL); 
//ini_set("display_errors", 1); 
require("../../../../shared/startPage.php");

$pageFiles->addCoreLibPhp("clsLoadVars/clsLoadVars.php");

$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->includePhp();
$pageVars = new loadVars($clsUS,$con);

if ($pageVars->hacking) die();

$tkn = getStrFromRequest("tkn");

if (isAllowed("editAssocUsers")) {
	if ($pageVars->edit_userid !== null) {
		if ($pageVars->edit_usertype == 2) {
			$qry = "select usr_assoc.* from usr_assoc where assoc_userid = {$pageVars->edit_userid}";
		} else {
			$qry = "select usr.* from usr where userid = {$pageVars->edit_userid}";
		}
		$formDB = getRS($con,$qry);
		$formRow = getRow($formDB);
		
		$load = getStrFromRequest("load");

		if (buttonClicked("save")) {
			
			if ($pageVars->edit_userid == 0 && $pageVars->edit_usertype == 2) {
				$qry = new dbInsert("usr_assoc");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
			} else {
				
				if ($pageVars->edit_usertype == 2) {
					$qry = new dbUpdate("usr_assoc");
					$qry->setParam("assoc_userid", $pageVars->edit_userid);				
				} else {
					$qry = new dbUpdate("usr");
					$qry->setParam("userid", $pageVars->edit_userid);				
				}
			}		
			//personal details
			$qry->setReqStringVal("firstname",$_POST["firstname"],"First Name");
			$qry->setReqStringVal("lastname",$_POST["lastname"],"Last Name");
			$qry->setReqStringVal("address1",$_POST["address1"],"Address 1");
			$qry->setStringVal("address2",$_POST["address2"],"Address 2");
			$qry->setStringVal("address3",$_POST["address3"],"Address 3");
			$qry->setStringVal("city",$_POST["city"],"City");
			$qry->setReqStringVal("postcode",$_POST["postcode"],"Postcode");
			$qry->setReqStringVal("tel1",$_POST["tel1"],"Telephone 1");
			$qry->setStringVal("tel2",$_POST["tel2"],"Telephone 2");
			$qry->setReqNumberVal("age",$_POST["age"],"Age");	
			if (isset($_POST["sex"])) {	
				$qry->setReqStringVal("sex",$_POST["sex"],"Sex");
			} else {
				$qry->setReqStringVal("sex","","Sex");
			}

			$qry->setAudit($clsUS->userid);
			if ($qry->execute($con)) {
				if ($pageVars->edit_userid == 0) {
					$pageVars->edit_userid = $qry->getNewID();	
				}
				//echo any parameters here that will be passed back to this page when successful
				echo "&edit_userid=".$pageVars->edit_userid."&edit_usertype=".$pageVars->edit_usertype;
			} else {
				echo "##ERROR: ";
				echo $qry->getError();
			}			
			
			
		} else if (buttonClicked("cancel")) {
			
			echo "cancel";
			
		} else {
			if ($load == 'yes') {
				if ($pageVars->edit_usertype == 2) {
					$qry = "select usr_assoc.* from usr_assoc where assoc_userid = {$pageVars->edit_userid}";
				} else {
					$qry = "select usr.* from usr where userid = {$pageVars->edit_userid}";
				}
				/*
				$formDB = new pageQry($con, $qry);
				$formDB->rsItem();
				echo $qry;
				echo "<div class=\"col\">";
					echo frmTextAreaNonEdit($formDB->row,"firstname","First Name");
					echo frmTextAreaNonEdit($formDB->row,"lastname","Last Name");
					echo frmTextAreaNonEdit($formDB->row,"address1","Address 1");
					echo frmTextAreaNonEdit($formDB->row,"address2","Address 2");
					echo frmTextAreaNonEdit($formDB->row,"address3","Address 3");
					echo frmTextAreaNonEdit($formDB->row,"city","City");
				echo "</div>";
				echo "<div class=\"col\">";
					echo frmTextAreaNonEdit($formDB->row,"postcode","Postcode");
					echo frmTextAreaNonEdit($formDB->row,"tel1","Telephone 1");
					echo frmTextAreaNonEdit($formDB->row,"tel2","Telephone 2");
					echo frmTextAreaNonEdit($formDB->row,"age","Age");
					echo frmTextAreaNonEdit($formDB->row,"sex","Sex");
					echo frmHiddenField($pageVars->edit_userid,"edited_id");
					echo frmHiddenField($pageVars->edit_usertype,"edit_usertype");

				echo "</div>";
				*/
			} else {			
				//show form
				
				include("$rootPath/handsam_core/functionality/select_users/content/page.edit_users.php");
			}
		}
	}
}
?>