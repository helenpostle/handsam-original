<?php

/**
* adds email alert to the usr table for a given userid
*
* For admin and super admin users
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = "handsam_core";

require("../../../../shared/startPage.php");

$pageFiles->includePhp();
$tkn = getStrFromRequest("tkn");


#check if have requested a specific user
$userid = getIntFromRequest("userid");
header("Cache-Control: no-cache");
if ($userid !== null && isAllowed("editUsers")) {
	
	#need to make sure that this user id is in the session account if we are dealing with a school admin
	
	
	$qry = "select usr.* from usr inner join account on usr.accountid = account.accountid where usr.userid = $userid";
	$rsUser = getRS($con,$qry);
	$rowUser = getRow($rsUser);
	
	
	 
	if ((isAllowed("editAccount")) || (isAllowed("editUsers") && $rowUser["accountid"] == $clsUS->accountid)) {		
				
		$qry = new dbUpdate("usr");
		$qry->setParam("userid",$userid);
		$qry->setReqNumberVal("late_task_email_alert",0,"late task email alert");
				
		if (!$qry->execute($con)) {
			echo  "Error removing email alerts to user: $userid: ".$qry->getError();
		} else {
			echo "Email notification of late task removed for this user";
		}

	}
	
}
?>