$(document).ready(function(){
	var script_url = siteRoot + "/handsam_core/functionality/email_alert_checkbox/ajax/";
	var frm_tygwyn = $("#frm_tygwyn").val();
	$('#user_list :checkbox').click(function(event){
		if ($(this).is(':checked')) {
			addEmailAlert($(this).val(), script_url, frm_tygwyn);
		} else {
			rmEmailAlert($(this).val(), script_url, frm_tygwyn);
		}
	});
	$('#user_list2 :checkbox').click(function(event){
		if ($(this).is(':checked')) {
			addEmailAlert($(this).val(), script_url, frm_tygwyn);
		} else {
			rmEmailAlert($(this).val(), script_url, frm_tygwyn);
		}
	});
	
	
	function addEmailAlert(user_id, script_url, frm_tygwyn) {
		$.post(script_url + 'add_email_alert.php', {userid:user_id, frm_tygwyn:frm_tygwyn}, function(data){
  			alert(data);
		});
	}
	
		
	function rmEmailAlert(user_id, script_url, frm_tygwyn) {
		$.post(script_url + 'rm_email_alert.php', {userid:user_id, frm_tygwyn:frm_tygwyn}, function(data){
  			alert(data);
		});
	}
});