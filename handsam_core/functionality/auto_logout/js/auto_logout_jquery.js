$(document).ready(function(){
	var newUrl = $.query.set("logout", "true").set("autologout", "true");
	
    $(document).idleTimeout({
      inactivity: autoLogout_inactivity,
      sessionAlive: false,
      noconfirm: autoLogout_noconfirm,
      redirect_url: newUrl,
      logout_url: newUrl
    });
});