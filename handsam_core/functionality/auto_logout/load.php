<?php
$this->addCoreLibJs("jquery/plugins/jquery-querystring.js");
$this->addCoreLibJs("jquery/jquery-ui-1.8.14.custom.min.js");
$this->addFuncJs("jquery-idletimeout.js");
$this->addFuncJs("auto_logout_jquery.js");
$this->addConfJs("auto_logout_jquery.conf.php");
$this->addFuncCss("auto_logout.css");
?>