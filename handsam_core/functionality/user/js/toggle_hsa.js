$(document).ready(function() {
	
    toggle_hsa();
	
    $('#HSA').click(function(event){
        toggle_hsa();
    });
    if($('#key_holder').attr('checked')) 
            {
                keyholder.ticked = true;
                keyholder.add_required();
                keyholder.show_address();
           }
        else
            {
                $('#contact_details .col').hide();
			
            }
    
    // toggle the address
    $('#key_holder').change(function(){
        if($(this).is(':checked'))
            {
                keyholder.ticked = true;
                keyholder.add_required();
                keyholder.show_address();
            }
        else         
            {
                keyholder.ticked = false;
                keyholder.not_required();
            }
    });
    
    $('#toggle_showhide').click(function(){
        if(keyholder.ticked == false)
            {
                if(keyholder.open == false) 
                    keyholder.show_address();
                else
                    keyholder.hide_address();
            }
    });
	
	
	$( "#contact_details input" ).each(function( index ) {
		if ($(this).attr('value') != '') {
			keyholder.show_address();
			return false;
		}
	});
    
});

function toggle_hsa() {
    if ($('#HSA').is(':checked')) {
        $('#SA_usrtype').parent().hide();
        $('#SA_usrtype option').removeAttr("selected");
    } else {
        $('#SA_usrtype').parent().show();
    }
}

// key holder 
// keep it all in an object
var keyholder = {};

keyholder.ticked = false;

keyholder.open = false;

keyholder.els = ['#address1', '#city', '#postcode', '#tel1'];

keyholder.add_required = function ()
{    
    for(el in this.els)
        {
            $(this.els[el]).parent('div.frm_row').removeClass('optional').addClass('required');
            $(this.els[el]).siblings('label').append('<span>* required field</span>');
        }
}

keyholder.not_required = function ()
{    
    for(el in this.els)
        {
            $(this.els[el]).parent('div.frm_row').removeClass('required').addClass('optional');
            $(this.els[el]).siblings('label').children('span').remove();
        }
}

keyholder.show_address = function()
{
    keyholder.open = true;
    
    $('#contact_details .col').show();
    $('#contact_details').addClass('bordered');
    $('#contact_details').css({'border' : '1px solid #000', 'border-top' : '0px'} );
}

keyholder.hide_address = function()
{
    keyholder.open = false;
    
    $('#contact_details .col').hide();
    $('#contact_details').removeClass('bordered');
    $('#contact_details').css({'border' : '0px'} );
}