<?php
if (isAllowed("uploadHandsamDocs") || isAllowed("uploadAdminDocs") || isAllowed("uploadUserDocs")) {
	$this->addFuncJs("uploads.js");
	$this->csrf = 'ajax';

}
?>
