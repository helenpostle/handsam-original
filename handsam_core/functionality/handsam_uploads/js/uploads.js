function attachDoc(docid, docname, formDiv, item) {
	var cnt = 0;
	var liId = 'justAdded' + docid;
	while (window.opener.document.getElementById(liId)) {
		cnt++;
		liId += "." + cnt;
	}
		
	docDiv = window.opener.document.getElementById(formDiv);
	docDiv.innerHTML += "<li id=\"" + liId + "\">\n" + docname + "<input type='hidden' name='" + item +"AddId[]' value='" + docid + "'/><input type='hidden' name='" + item +"AddFilename[]' value='" + docname + "'/><img src=\"" + siteRoot + "/images/delete.png\" class=\"delete_attached\" alt=\"unattach document or file\" title=\"Unattach this file\" /></li>\n"; 
	alert("Please remember, once you have finished attaching any documents you must save the task!"); 
	window.close();
}

function unattach(id) {
	docLi = document.getElementById(id);
	docLi.style.display = 'none';
	docLi.innerHTML = '';
}	

function deleteFromDB(id, taskfileid, item) {
	docLi = document.getElementById(id);
	docLi.style.display = 'none';
	docLi.innerHTML = "<input type='hidden' name='" + item + "DelId[]' value='" + taskfileid + "'/>";
}

function openLink() {
	docURL = document.getElementById('url').value;
	window.open(docURL,"handsamLink","toolbar=yes,status=yes,scrollbars=yes, resize=yes, menubar=yes");
}

function attachItemToPage(rootUrl, itemid, item_type, contentid) {
	var frm_tygwyn = $("#frm_tygwyn").val();
	url = rootUrl + "/handsam_core/ajax/attachItem.php"; // The server-side script
	$.post(url, {itemid:itemid, type:item_type, contentid:contentid,  frm_tygwyn:frm_tygwyn}, function(data){
			alert(data);
			if (window.opener) {
				window.opener.location.reload(true);
			} else {
				window.location.reload(true);
			}

	});
}
	
function removeItem(rootUrl, itemid, item_type, contentid) {
	var frm_tygwyn = $("#frm_tygwyn").val();
	url = rootUrl + "/handsam_core/ajax/removeItem.php"; // The server-side script
	$.post(url, {itemid:itemid, type:item_type, contentid:contentid,  frm_tygwyn:frm_tygwyn}, function(data){
			alert(data);
			if (window.opener) {
				window.opener.location.reload(true);
			} else {
				window.location.reload(true);
			}

	});
}


$(document).ready(function(){
	/**
	 *	Removes an attached document or link from the document/link box
	 *
	 */
	$('img.delete_attached').live('click', function(){
		var parent = $(this).parent();
		parent.hide();
		
		var del_input = parent.children('input[type="hidden"]');
		if(del_input.attr('name') == 'docId[]') {
			del_input.attr('name', 'docDelId[]');
			var del_input_val = del_input.parent('li').attr('id').replace('doc', '');
			del_input.val(del_input_val);
		}
		if(del_input.attr('name') == 'docAddId[]') del_input.remove();
		if(del_input.attr('name') == 'linkId[]') {
			del_input.attr('name', 'linkDelId[]');
			var del_input_val = del_input.parent('li').attr('id').replace('doc', '');
			del_input.val(del_input_val);
		}
		if(del_input.attr('name') == 'linkAddId[]') del_input.remove();
		
	});
});