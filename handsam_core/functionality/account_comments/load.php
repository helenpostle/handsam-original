<?php
$this->addCoreLibJs("jquery/plugins/jquery.popUp.js");
$this->addFuncJs("account_comments.popup.js");
$this->addFuncJs("account_comments.display.js");
$this->addFuncCss("popup.css");
$this->addFuncCss("comments.css");

$this->csrf = 'ajax';

?>