<?php
####make this a secure page
$secure = true;
$ajaxModule = "handsam_core";

require("../../../../../shared/startPage.php");
/*
require("../library/securePage.php");

####include other functions
include("../library/funcRequest.php");
include("../library/funcDisplay.php");
include("../library/funcDbEdit.php");
include("../library/funcDbForm.php");
include("../lotc/library/clsLoadVars.php");
include("../lotc/library/clsPageQry.php");

include("../lotc/library/funcLotcProcessForm.php");
include("../lotc/library/funcLotcDisplayForm.php");
include("../lotc/library/funcLotcDisplayVisit.php");
include("../lotc/library/funcLotcVisits.php");
*/

$pageFiles->addModFunc('lotc_visits');
$pageFiles->addModFunc('lotc_form');
$pageFiles->addModFunc('lotc_display_visit');
$pageFiles->addModFunc('lotc_process_form');


$pageFiles->includePhp();

$pageVars = new loadVars($clsUS,$con);

if (isAllowed("planVisits")) {
	if ($pageVars->visitid !== null) {
		
		$visittext = $_POST["visittext"];
		$visitname = $_POST["visitname"];
		$load = getStrFromRequest("load");
		
		$qry = "select a_visit.*  from a_visit where visitid = {$pageVars->visitid}";
		$pageDb = new pageQry($con, $qry);
		$pageDb->rsItem(); 
		
		//for use in determining if the visit date can be edited still. If any stage has been confirmed, then the date cannot be edited
		$visit_state = visit_state($con, $pageVars->visitid, "APPROVED EVC");
		
		if (buttonClicked("save")) {

				$visit_date = $_POST["visit_date"];
			
				//date time so add hrs and mins to date field
				$hrs = $_POST["visit_date_hrs"];
				$hrs = sprintf("%02d", $hrs);
				$mins = $_POST["visit_date_mins"];
				$mins = sprintf("%02d", $mins);
				$visit_date = "$visit_date {$hrs}:{$mins}:00";
				
				$visit_end = $_POST["visit_end"];
			
				//date time so add hrs and mins to date field
				$hrs = $_POST["visit_end_hrs"];
				$hrs = sprintf("%02d", $hrs);
				$mins = $_POST["visit_end_mins"];
				$mins = sprintf("%02d", $mins);
				$visit_end = "$visit_end {$hrs}:{$mins}:00";

				$qry = new dbUpdate("a_visit");
				$qry->setParam("visitid",$pageVars->visitid);
				//if ($visit_state->cnt == 0) {
					$qry->setDateTimeVal("visit_date",$visit_date,"Visit Start time and date");
					$qry->setDateTimeVal("visit_end",$visit_end,"Visit End time and date");
				//}
				$qry->setReqStringVal("visitname",$visitname,"visit name");
				$qry->setStringVal("visittext",$visittext,"visit text");
				$qry->setAudit($clsUS->userid);
				if (dataInVisit($con, $pageVars->visitid)) {
					$qry->setReqNumberVal("planid",$pageVars->planid,"planid");
				}
			
				if ($qry->execute($con)) {
				} else {
					$messages[] = $qry->getError();	
				}

		} else if (buttonClicked("cancel")) {
			
			echo "cancel";
			
		} else if (buttonClicked("delete")) {
			$qry = new dbUpdate("a_visit");
			$qry->setParam("visitid",$pageVars->visitid);
			$qry->setReqStringVal("state","DELETED","state");
			$qry->setAudit($clsUS->userid);
			if ($qry->execute($con)) {
				echo "deleted";
			} else {
				echo "cancel";
				$messages[] = $qry->getError();	
			}
			
			
			
		} else {
			if ($load == 'yes') {
				$qry = "select * from a_visit where visitid = {$pageVars->visitid}";
				$pageDb = new pageQry($con, $qry);
				$pageDb->rsItem(); 
				if ($pageDb->row['visit_date'] != "") {
					$visit_date = displayLotcDateTime($pageDb->row['visit_date']);
				} else {
					$visit_date = "Not Set";
				}
				
				if ($pageDb->row['visit_end'] != "") {
					$visit_end = displayLotcDateTime($pageDb->row['visit_end']);
				} else {
					$visit_end = "Not Set";
				}

				
				echo "<strong>Visit Start Date: </strong>$visit_date<br/><strong>Visit End Date: </strong>$visit_end<br/><strong>Visit Title: </strong>{$pageDb->row['visitname']}<br/><strong>Visit Summary: </strong>{$pageDb->row['visittext']}";
			} else {			
				//list plans
				include("$rootPath/modules/lotc/content/page.visit_edit.php");
			}
		}
	}
}
?>