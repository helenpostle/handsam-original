<?php
$secure = true;

$ajaxModule = "handsam_core";

require("../../../../shared/startPage.php");
$pageFiles->includePhp();
$commentid = getIntFromRequest("commentid");
$accountid = getIntFromRequest("accountid");
$commenttext = getStrFromRequest("commenttext");
$load = getStrFromRequest("load");
$tkn = getStrFromRequest("tkn");

if (isAllowed("editAccount")) {
		//now need to check if editing a comment, it is their comment
		if ($commentid !== null) {
			
			$qry = "select * from account_comments where commentid = $commentid and created_by = {$clsUS->userid}";
			$rsCheck = getRS($con, $qry);
			if ($commentid == 0 || getResultRows($rsCheck) > 0) {
				$row = getRow($rsCheck);
				
				
				if (buttonClicked("save")) {
					if ($commentid == 0) {
						$qry = new dbInsert("account_comments");
						$qry->setReqNumberVal("accountid",$accountid, "accountid");
					} else {
						$qry = new dbUpdate("account_comments");
						$qry->setParam("commentid",$commentid);
					}
					
					$qry->setReqStringVal("commenttext",$_POST['commenttext'],"comment text");
					$qry->setAudit($clsUS->userid);
					//echo $qry->getSql();
					if ($qry->execute($con)) {
					} else {
						echo "##ERROR: \r\n";
						echo $qry->getError();	
					}
		
				} else if (buttonClicked("cancel")) {
					
					echo "cancel";	
					
				} else if (buttonClicked("delete")) {
					
					if ($commentid > 0) {
						$qry = new dbDelete("account_comments");
						$qry->setParam("commentid",$commentid);
						$qry->setParam("accountid",$accountid);
						if ($qry->execute($con)) {
						} else {
							$messages[] = $qry->getError();	
						}
					}
					
				//} else if ($load != 'yes') {
				} else {
					if ($load == 'yes') {
						echo "<h3> Comments </h3>";
						echo "<a class=\"add_comment\" href=\"?accountid=$accountid&amp;commentid=0\">add comment</a>";
						$sql = "SELECT usr.firstname, usr.lastname, account_comments.* FROM account_comments LEFT JOIN usr ON account_comments.created_by = usr.userid WHERE account_comments.accountid = $accountid order by account_comments.created desc limit 0,8";
						$comments = getRS($con, $sql);
						echo "<ul id=\"comment_list\">";
						if (getResultRows($comments) > 0) {
							while ($row = getRow($comments)) {
								echo "<li class=\"clearfix\">";
								echo "<strong>{$row['firstname']} {$row['lastname']}</strong> : <em>".displayDateTime($row['created'])."</em>";
								//display edit button if comment is the users own
								//if ($row['created_by'] == $clsUS->userid) {
									//echo " <a class=\"add_comment\" href=\"?commentid={$row['commentid']}&amp;accountid=$accountid\">edit</a>";
								//}
								
								echo displayParagraphs($row['commenttext']);
								echo "</li>";
							}
							echo "</ul>";
							//count comments - idf more than 8 show all
							$comment_qry = "select commentid from account_comments where accountid = $accountid";
							$comment_rs = getRS($con, $comment_qry);
							if (getResultRows($comment_rs) > 8) {
								echo "<a class=\"show_all\" href=\"#\">show all</a>";
							}
						}						
					} else {
						//list plans
						include("$rootPath/handsam_core/content/page.comment_edit.php");
					}
						
				}
			}
				
		}
}
?>