<?php
/**
* displays a list of authrities for the autocomplete field in account form
*
* For super admin users
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$ajaxModule = "handsam_core";

require("../../../../shared/startPage.php");

$pageFiles->includePhp();

$term = getStrFromRequest("term");

$qry = "select auth_name from authority where auth_name like('$term%')";
$rs = getRS ($con, $qry);
$arr = array();
while ($row = getRow($rs)) {
	$arr[] = "\"{$row['auth_name']}\"";
}

echo "[".implode(",",$arr)."]";

//echo json_encode($arr);		
?>