$(document).ready(function() {
	var auth_url = siteRoot + '/handsam_core/functionality/autocomplete_authority/ajax/authority.php';

	$("#authority").autocomplete({
		source: auth_url
	});
});