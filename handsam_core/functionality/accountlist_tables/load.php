<?php
$this->addCoreLibJs("jquery/jquery-ui-1.8.14.custom.min.js");
$this->addCoreLibJs("jquery/plugins/jquery.tablesorter.js");
$this->addCoreLibJs("jquery/plugins/jquery.uitablefilter.js");
$this->addFuncJs("acc_list_tablesorter.js");
$this->addFuncJs("acc_list_filters.js");
$this->addFuncJs("acc_list_export.js");
$this->addFuncCss("acc_list_filters_1.1.css");

?>