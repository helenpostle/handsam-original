$(document).ready( function() {

	//module filter
	$('#mod_filter').change(function(event){
	var mod_id = $(this).val();
		window.location = "?appid=" + mod_id;
	});
	
	
	//tasklist filter
	$('#tasklist_filter').change(function(event){
	var tl_id = $(this).val();
		window.location = "?appid=1&tasklist=" + tl_id;
	});

	
	//school type  filter
	$('#sch_type_filter').change(function(event){
	var sch_type_filter = $(this).val();
		window.location = "?sch_type_filter=" + sch_type_filter;
	});
		
	//school status  filter
	$('#sch_status_filter').change(function(event){
	var sch_status_filter = $(this).val();
		window.location = "?sch_status_filter=" + sch_status_filter;
	});
		
	//school pupils  filter
	$('#school_pupils_filter').change(function(event){
	var school_pupils_filter = $(this).val();
		window.location = "?school_pupils_filter=" + school_pupils_filter;
	});
		
	
	//manager filter
	$('#man_filter').click(function(event){
		if ($(this).val() == "View my accounts") {
			window.location = "?manfilter=Mine";
		} else {
			window.location = "?manfilter=All";
		}
	});	

	//school authority / region  filter	
	var auth_url = siteRoot + '/handsam_core/functionality/accountlist_tables/ajax/authority.php';

	$( "#authority_filter" ).autocomplete({
		source: auth_url,
			close: function(event, ui) {
			var authority_filter = this.value;
			window.location = "?authority_filter=" + authority_filter;
		}
	});
	
	
	
	//reset authority filter
	$('#reset_authority').click(function(event){
		window.location = "?authority_filter=ALL";
	});	
	
	// account name filter
	var theTable = $('#acc_list')
	var acc_name_url = siteRoot + '/handsam_core/functionality/accountlist_tables/ajax/acc_name.php';
		
	$("#acc_name_filter").keyup(function() {
		$.uiTableFilter( theTable, this.value, 'Account name' );
		$.post(acc_name_url, { accname: this.value } );
	})
	
	//account cluster  filter
	$('#acc_cluster_filter').change(function(event){
	var acc_cluster_filter = $(this).val();
		window.location = "?acc_cluster=" + acc_cluster_filter;
	});
	
	
	//reset acc name filter
	$('#reset_acc_name').click(function(event){
		$.post(acc_name_url, { accname: '' }, function(data) {
			window.location = window.location.href;
		});
	});	
	

	
	// account postcode filter
	var theTable = $('#acc_list')
	var acc_postcode_url = siteRoot + '/handsam_core/functionality/accountlist_tables/ajax/acc_postcode.php';
		
	$("#acc_postcode_filter").keyup(function() {
		$.uiTableFilter( theTable, this.value, 'Postcode' );
		$.post(acc_postcode_url, { accpostcode: this.value } );
	})
	
	
	//reset acc postcode filter
	$('#reset_acc_postcode').click(function(event){
		$.post(acc_postcode_url, { accpostcode: '' }, function(data) {
			window.location = window.location.href;
		});
	});	
	
	
	// account dfe filter
	var theTable = $('#acc_list')
	var acc_dfe_url = siteRoot + '/handsam_core/functionality/accountlist_tables/ajax/acc_dfe.php';
		
	$("#acc_dfe_filter").keyup(function() {
		$.uiTableFilter( theTable, this.value, 'Dfe number' );
		$.post(acc_dfe_url, { accdfe: this.value } );
	})
	
	
	//reset acc dfe filter
	$('#reset_acc_dfe').click(function(event){
		$.post(acc_dfe_url, { accdfe: '' }, function(data) {
			window.location = window.location.href;
		});
	});	
	
	
	
	//reset filter
	$('#reset_filter').click(function(event){
		$.post(acc_name_url, { accname: '' }, function(data) {
			$.post(acc_postcode_url, { accpostcode: '' }, function(data) {
				$.post(acc_dfe_url, { accdfe: '' }, function(data) {
					window.location = "?manfilter=All&tasklist=0&appid=0&school_pupils_filter=ALL&sch_status_filter=ALL&sch_type_filter=ALL&authority_filter=ALL&acc_cluster=ALL";
				});
			});
		});
	});	

	
	
});