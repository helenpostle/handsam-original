<?php
$ajaxModule = "handsam_core";
//error_reporting(E_ALL); 
// ini_set("display_errors", 1); 
require("../../../../shared/startPage.php");

$pageFiles->addCoreLibPhp("clsLoadVars/clsLoadVars.php");

$pageFiles->includePhp();
$pageVars = new loadVars($clsUS,$con);

$tab = getIntFromRequest("tab_num");

//if ($pageVars->hacking) die();
if (isAllowed("selectUsers")) {
	//set each tab query and title
	$qry = array();
	$tabs = array();
	$cols = array();
	$cols[0]['firstname'] = "First Name";
	$cols[0]['lastname'] = "Last Name";
	$cols[0]['username'] = "Username";
	$tabs[0] = "Account Users";
	$qry[0] = "select usr.username, usr.firstname, usr.lastname, usr.userid, 1 as source from usr where accountid={$pageVars->accountid} and state = 'ACTIVE' ";
	$qry[0] .= " order by lastname ";
	
	$cols[1]['firstname'] = "First Name";
	$cols[1]['lastname'] = "Last Name";
	$tabs[1] = "Associated Users";
	$qry[1] = "select '' as username, usr_assoc.firstname, usr_assoc.lastname, usr_assoc.assoc_userid as userid, 2 as source from usr_assoc where accountid={$pageVars->accountid} and state = 'ACTIVE' order by lastname ";
	if ($tab > 0) {
		include("../content/select_users_tab.php");
	} else {
		include("../content/select_users.php");
	}
}
?>