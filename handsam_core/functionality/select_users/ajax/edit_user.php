<?php
$ajaxModule = "handsam_core";
//error_reporting(E_ALL); 
//ini_set("display_errors", 1); 
require("../../../../shared/startPage.php");

$pageFiles->addCoreLibPhp("clsLoadVars/clsLoadVars.php");

$pageFiles->addCoreLibPhp("funcDb/funcDbExForm.php");
$pageFiles->includePhp();
$pageVars = new loadVars($clsUS,$con);

if ($pageVars->hacking) die();

$tkn = getStrFromRequest("tkn");

if (isAllowed("editAssocUsers")) {
	if ($pageVars->edit_userid !== null) {
		if ($pageVars->edit_usertype == 2) {
			$qry = "select usr_assoc.* from usr_assoc where assoc_userid = {$pageVars->edit_userid}";
		} else {
			$qry = "select usr.* from usr where userid = {$pageVars->edit_userid}";
		}
		$formDB = getRS($con,$qry);
		$formRow = getRow($formDB);
		
		$load = getStrFromRequest("load");

		if (buttonClicked("save")) {
			
			if ($pageVars->edit_userid == 0 && $pageVars->edit_usertype == 2) {
                $req_arr = $core_conf['select_assoc_users_required'];
				$qry = new dbInsert("usr_assoc");
				$qry->setReqNumberVal("accountid",$pageVars->accountid,"Accountid");
			} else {
				
				if ($pageVars->edit_usertype == 2) {
                    $req_arr = $core_conf['select_assoc_users_required'];
					$qry = new dbUpdate("usr_assoc");
					$qry->setParam("assoc_userid", $pageVars->edit_userid);				
				} else {
                    $req_arr = $core_conf['select_users_required'];
					$qry = new dbUpdate("usr");
					$qry->setParam("userid", $pageVars->edit_userid);				
				}
			}		
			//personal details
			if (in_array('firstname', $req_arr)) {
                $qry->setReqStringVal("firstname",$_POST["firstname"],"First Name");
            } else {
                $qry->setStringVal("firstname",$_POST["firstname"],"First Name");
            }
            if (in_array('lastname', $req_arr)) {
                $qry->setReqStringVal("lastname",$_POST["lastname"],"Last Name");
            } else {
                $qry->setStringVal("lastname",$_POST["lastname"],"Last Name");
            }
            if (in_array('address1', $req_arr)) {
                $qry->setReqStringVal("address1",$_POST["address1"],"Address 1");
            } else {
                $qry->setStringVal("address1",$_POST["address1"],"Address 1");
            }
            if (in_array('address2', $req_arr)) {
                $qry->setReqStringVal("address2",$_POST["address2"],"Address 2");
            } else {
                $qry->setStringVal("address2",$_POST["address2"],"Address 2");
            }
            if (in_array('address3', $req_arr)) {
                $qry->setReqStringVal("address3",$_POST["address3"],"Address 3");
            } else {
                $qry->setStringVal("address3",$_POST["address3"],"Address 3");
            }
            if (in_array('city', $req_arr)) {
                $qry->setReqStringVal("city",$_POST["city"],"City");
            } else {
                $qry->setStringVal("city",$_POST["city"],"City");
            }
            if (in_array('postcode', $req_arr)) {
                $qry->setReqStringVal("postcode",$_POST["postcode"],"Postcode");
            } else {
                $qry->setStringVal("postcode",$_POST["postcode"],"Postcode");
            }
            if (in_array('tel1', $req_arr)) {
                $qry->setReqStringVal("tel1",$_POST["tel1"],"Telephone 1");
            } else {
                $qry->setStringVal("tel1",$_POST["tel1"],"Telephone 1");
            }
            if (in_array('tel2', $req_arr)) {
                $qry->setReqStringVal("tel2",$_POST["tel2"],"Telephone 2");
            } else {
                $qry->setStringVal("tel2",$_POST["tel2"],"Telephone 2");
            }
			if (in_array('age', $req_arr)) {
                $qry->setReqNumberVal("age",$_POST["age"],"Age");
            } else {
                if ($_POST["age"] > 0) {
                    $qry->setNumberVal("age",$_POST["age"],"Age");
                } else {
                    $qry->setNullVal("age");
                }
            }
			if (in_array('sex', $req_arr)) {
                if (isset($_POST["sex"])) {	
                    $qry->setReqStringVal("sex",$_POST["sex"],"Sex");
                } else {
                    $qry->setReqStringVal("sex","","Sex");
                }              
            } else {
                if (isset($_POST["sex"])) {	
                    $qry->setStringVal("sex",$_POST["sex"],"Sex");
                } else {
                    $qry->setStringVal("sex","","Sex");
                }              
            }

			$qry->setAudit($clsUS->userid);
			if ($qry->execute($con)) {
				if ($pageVars->edit_userid == 0) {
					$pageVars->edit_userid = $qry->getNewID();	
				}
				//echo any parameters here that will be passed back to this page when successful
				echo "&edit_userid=".$pageVars->edit_userid."&edit_usertype=".$pageVars->edit_usertype;
			} else {
				echo "##ERROR: ";
				echo $qry->getError();
			}			
			
			
		} else if (buttonClicked("cancel")) {
			
			echo "cancel";
			
		} else {
			if ($load == 'yes') {
				if ($pageVars->edit_usertype == 2) {
					$qry = "select usr_assoc.* from usr_assoc where assoc_userid = {$pageVars->edit_userid}";
				} else {
					$qry = "select usr.* from usr where userid = {$pageVars->edit_userid}";
				}

			} else {			
				//show form
				if ($pageVars->edit_usertype == 2) {
                    //assoc users
                    include("$rootPath/handsam_core/functionality/select_users/content/page.edit_assoc_users.php");
                } else {
                    include("$rootPath/handsam_core/functionality/select_users/content/page.edit_users.php");
                }
			}
		}
	}
}
?>