$(document).ready(function() {
	$("#content").prepend("<div id=\"select_users_dialog\"></div>");
	
	$("#select_users_dialog").delegate("td.edit_usr", "click", function(){
		var tab_id = $(this).parentsUntil('div.ui-tabs-panel').parent().attr('id');
		var tab_index = $(this).parentsUntil('div.ui-tabs-panel').parent().index();
		var edit_data = $(this).parent().find("input");
		var edit_userid = edit_data.val();
		var edit_usertype = edit_data.attr("class");
		var qs = {edit_userid: edit_userid, edit_usertype: edit_usertype};
		$.post(siteRoot + "/handsam_core/functionality/select_users/ajax/edit_user.php", {edit_userid: edit_userid, edit_usertype:edit_usertype}, function(data){
			$('#' + tab_id).html(data);
			$.submit_dialog({
				txt: 'text here',
				load_url: siteRoot + '/handsam_core/functionality/select_users/ajax/edit_user.php',
				params: qs,
				delete_url: siteRoot + '/modules/accidents/index.php'
			}, "", function () {
				$.post(siteRoot + "/handsam_core/functionality/select_users/ajax/select_users.php", {tab_num:tab_index}, function(data){
					$('#' + tab_id).html(data);
				});				
			}, function () {
				//alert("cancel");

				$.post(siteRoot + "/handsam_core/functionality/select_users/ajax/select_users.php", {tab_num:tab_index}, function(data){
					$('#' + tab_id).html(data);
				});				
				
				
								
			});	

		});	
	});
	

		
});

function select_users(num, fn, fn_close) {
		$.post(siteRoot + "/handsam_core/functionality/select_users/ajax/select_users.php", {}, function(data) {
		    $("#select_users_dialog").html(data).tabs().dialog({
		       //autoOpen: false,
		        width: 700,
		        height: 700,
		        draggable: false, // disable the dialogs default drag we will be using the tabs titlebar instead
		        modal: true,
		        buttons: {
		            'Close': function() {
			            if (typeof fn_close == "function") {
				            fn_close(num);
			            }             
		                $(this).dialog('close').dialog('destroy').tabs('destroy');                  
		            }
		        },
		        open: function() {
			        if ($.browser.msie && $.browser.version.substr(0,1)<7) $('#content select').hide();	
		            $('.ui-dialog-titlebar').hide(); // hide the default dialog titlebar
		            if (typeof fn == "function") fn(num);

		            setTimeout(function(){
					    $('#select_users_dialog').dialog('close').dialog('destroy').tabs('destroy');
					}, 200000);
		        },
		        close: function() {
			        if ($.browser.msie && $.browser.version.substr(0,1)<7) $('#content select').show();
		            $('.ui-dialog-titlebar').show(); // in case you have other ui-dialogs on the page, show the titlebar 
					
		            
		        }
		    }).parent().draggable().tablesorter({  
	        	debug: false
		    });     ; // the ui-tabs element (#tabdlg) is the object parent, add these allows the tab to drag the dialog around with it
		   	$(".select_users_table").tablesorter({  
		        debug: false
		    }); 
			
		});
	}