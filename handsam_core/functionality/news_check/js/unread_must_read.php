<?php // unread news javascript ?>



$(document).ready(function() {
    $("#content, #contentWide").prepend("<div id=\"select_users_dialog\"></div>");
    var news_data = window.unreadnews; 
    var html = '';
    for(var i in news_data)
    {
        var dateParts = news_data[i].created.split(' ')[0].split("-");
        var storydate = new Date(dateParts[0], dateParts[1] - 1, dateParts[2]);
        html += '<div class="popupstory" style="margin-bottom:18px;"><h2>'+news_data[i].title+'</h2><p><em>'+storydate.toDateString()+'</em></p><div class="news_list">'+news_data[i].contenttext +' </div><a href="#" class="markasread" id="story-'+news_data[i].contentid+'" title="This story will not be shown again">Mark as Read</a></div>';
    }
    $('#select_users_dialog').html(html).dialog({ 
        title: 'Your Unread News',
        width: 700,
        height: 630,
        modal: true,
        dialogClass: "no-close",
        
    });
    $('a.markasread').button().click(function(e){
        e.preventDefault();            
        var elm = $(this);        
        var contentid = elm.attr('id').split('-')[1];

        $.get(siteRoot+'/handsam_core/functionality/news_check/ajax/news_check.php',
            {
                obj : [ { contentid : contentid } ]
            },
            function(){
                elm.html('<span class="ui-button-text">Read</span>');
                elm.removeClass('markasread');
                elm.addClass('read');
                var to_read = $('a.markasread').length;
                if(to_read == 0)
                {
                  $('#select_users_dialog').dialog( "close" );
                }
            }
        );            
    });
});