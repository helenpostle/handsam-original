<?php

// ajax 
// mark news stories as unread

$secure = true;
$ajaxModule = "handsam_core";

require("../../../../shared/startPage.php");
$pageFiles->includePhp();

$obj = $_GET['obj'];

$news = new Unreadnews();

foreach($obj as $rec)
    $news->setUnread($rec['contentid']);

exit;