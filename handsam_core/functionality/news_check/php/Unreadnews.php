<?php

/**
 * Unread news model
 * 
 * manages the user's unread news stories
 *
 * @author Mei
 */
class Unreadnews {
    
    protected $con;
    protected $clsUS; // userid, lastvisit

    /**
     * Sets the database connection
     * and user object
     * 
     * @global object $con 
     */    
    public function __construct()
    {
        global $con;
        
        $this->con = $con;
        $this->clsUS = $_SESSION['clsUS'];
    }
    
    /**
     * Returns all news unread by the user
     * 
     * @return array
     */
    public function getUnread($limit=0)
    {
        $news = array();
        $limit_sql = "";
        if ($limit>0)
        {
          $limit_sql = "LIMIT $limit";
        }
        $sql1 = "SELECT c.contentid, c.title, c.summarytext, c.contenttext, c.created, c.edited 
            FROM content AS c 
            JOIN content AS c2 
                ON c.parentid = c2.contentid
            WHERE c2.superpage = 'news'
            AND c.state = 'PUBLISHED' 
            AND c.contentid NOT IN ( SELECT nu.content_id FROM news_users AS nu WHERE nu.user_id = 1504)
            ORDER BY c.created DESC $limit_sql;";
            
        $result = getRecSet($sql1);
        while($row = getRowAssoc($result)) {
            $row['summary'] = showAbstract($row['contenttext'], 250);
            $news[] = $row;
            
        }
        
        return $news;
    }
    
    /**
     * Sets a news item as read by that user
     * 
     * @param int $contentid id of content table
     */
    public function setUnread($contentid)
    {
        $sql = "INSERT INTO news_users (content_id, user_id, the_date, rating) VALUES ($contentid, {$this->clsUS->userid}, NOW(), 0);";
        executeSql($sql);
    }
}
