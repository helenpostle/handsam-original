$(document).ready(function(){
	$("#navigation li").mouseenter (function(){
		$(this).children('ul').css({'display': 'block', 'z-index': '20'});
	});
	$("#navigation li").mouseleave (function(){
		if ($(this).hasClass('current_page_item')) {
			$(this).children('ul').css({'z-index': '1'});
		} else {
			$(this).children('ul').css({'display': 'none', 'z-index': '0'});
		}
	});
});