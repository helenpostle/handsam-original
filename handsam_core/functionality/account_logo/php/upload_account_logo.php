<?php
$ajaxModule = "handsam_core";
if (isset($_POST["PHPSESSID"])) {
	session_id($_POST["PHPSESSID"]);
}

require("../../../../shared/startPage.php");


####make this a secure page
$secure = true;
//require("../../../../library/securePage.php");
//include("../../../../library/funcDbEdit.php");


$pageFiles->addFunc('files');
$pageFiles->addFunc('images');
$pageFiles->csrf_form = '';
$pageFiles->includePhp();

//include("../../../library/funcFiles/funcFiles.php");
//include("../../../library/funcImages/funcCropImage.php");
//include("../../../library/funcRequest/funcRequest.php");

//$swfuploads_uploads set in config
//$max_uploads = number of uploads set in config
//$thumbnails array set in config to describe different thumbnail dimensions and folders


/* Note: This thumbnail creation script requires the GD PHP Extension.  
	If GD is not installed correctly PHP does not render this page correctly
	and SWFUpload will get "stuck" never calling uploadSuccess or uploadError
 */

ini_set("html_errors", "0");
// Check the upload
if (isAllowed("editAccount")) {

	if (!isset($_FILES["Filedata"]) || !is_uploaded_file($_FILES["Filedata"]["tmp_name"]) || $_FILES["Filedata"]["error"] != 0) {
		echo "ERROR:invalid upload";
		exit(0);
	}
	
	$accountid = getIntFromRequest("item_id");
	$filename = $_FILES["Filedata"]["name"]; 
	
	//get existing logo filename for this account 
	$qryLogo = "SELECT accountlogo FROM account where account.accountid = ".$accountid;
	$rsLogo = getRS($con, $qryLogo);
	$rowLogo = getRow($rsLogo);
	
	
	
	$ext = strtolower(getExtension($filename));
	$dbFilename = $accountid."_".rmExtension($filename);
	$qry = new dbUpdate("account");
	$qry->setParam("accountid",$accountid);
	$qry->setReqStringVal("accountlogo",$dbFilename.$ext,"filename");
	$qry->setAudit($clsUS->userid);
	if ($qry->execute($con)) {
		$dbInsert = true;
	} else {
		$dbInsert = true;
	}
	
	//check for database error. If none then now create thumbnails for this image
	
	if ($dbInsert) {
		//would get last inserted file_id from db. We will use random one instead
		$file_id = $accountid;
		//do not add to session for superadmin!
		//$clsUS->accountlogo = $accountid.$ext;
	
		foreach ($swf_thumbnails as $thumbnail) {
			$filetype = true;
			//first check for that the folder exists and make it if it doesn't
			if (!file_exists($thumbnail[2])) {
				mkdir($thumbnail[2], 0700);
			}
			//first delete any logos for this account
			deleteFile($rowLogo['accountlogo'], $swf_thumbnails['logo'][2]."/");
	
			//here we create cropped image. We are using the file_id as the filename to be saved. You may want to use a combination of the filename, file_id and item_id, depending on what vdata is stored in the database.
			$filetype = CropImage($_FILES["Filedata"]["tmp_name"], $dbFilename, $thumbnail[2], $thumbnail[0], $thumbnail[1]);
		}
		
		if ($filetype) echo "FILEID:".$dbFilename.$filetype;
	}
	
}
?>