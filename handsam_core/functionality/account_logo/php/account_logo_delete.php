<?php
$ajaxModule = "handsam_core";
if (isset($_POST["PHPSESSID"])) {
	session_id($_POST["PHPSESSID"]);
}

require("../../../../shared/startPage.php");


####make this a secure page
$secure = true;
//require("../../../../library/securePage.php");
//include("../../../../library/funcDbEdit.php");
$pageFiles->addFunc('files');
$pageFiles->addFunc('images');
$pageFiles->csrf_form = '';
$pageFiles->includePhp();

//include("../../../library/funcImages/funcCropImage.php");
//include("../../../library/funcFiles/funcFiles.php");
//include("../../../library/funcRequest/funcRequest.php");

//$swfuploads_uploads set in config
//$max_uploads = number of uploads set in config
//$thumbnails array set in config to describe different thumbnail dimensions and folders


/* Note: This thumbnail creation script requires the GD PHP Extension.  
	If GD is not installed correctly PHP does not render this page correctly
	and SWFUpload will get "stuck" never calling uploadSuccess or uploadError
 */

ini_set("html_errors", "0");

// Check the upload

if (isAllowed("editAccount")) {
	
	//first get logo filename

	$accountid = getIntFromRequest("accountid");
	
	$qryLogo = "SELECT accountlogo FROM account where account.accountid = ".$accountid;
	$rsLogo = getRS($con, $qryLogo);
	$rowLogo = getRow($rsLogo);
	
	$qry = new dbUpdate("account");
	$qry->setParam("accountid",$accountid);
	$qry->setNullVal("accountlogo");
	$qry->setAudit($clsUS->userid);
	//echo $qry->getSql();
	if ($qry->execute($con)) {
		deleteFile($rowLogo['accountlogo'], $swf_thumbnails['logo'][2]."/");
		
	}
	
}
?>