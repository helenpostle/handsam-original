$(document).ready(function() {
	
	$(function(){
	
	    $('.swfupload-control').swfupload({
			upload_url: siteRoot + "/handsam_core/functionality/account_logo/php/upload_account_logo.php",
			post_params: {"item_id": item_id, "PHPSESSID": sessionid},
			// File Upload Settings
			file_size_limit : "2 MB",	// 2MB
			file_types : "*.jpg;*.gif;*.png",
			file_types_description : "JPG Images",
			file_upload_limit : "0",
	    
			// Button Settings
			button_image_url : siteRoot + "/handsam_core/functionality/account_logo/images/SmallSpyGlassWithTransperancy_17x18.png",
			button_placeholder_id : "spanButtonPlaceholder",
			button_width: 180,
			button_height: 18,
			button_text : '<span class="button">Select Images <span class="buttonSmall">(2 MB Max)</span></span>',
			button_text_style : '.button { font-family: Helvetica, Arial, sans-serif; font-size: 12pt; } .buttonSmall { font-size: 10pt; }',
			button_text_top_padding: 0,
			button_text_left_padding: 18,
			button_window_mode: SWFUpload.WINDOW_MODE.TRANSPARENT,
			button_cursor: SWFUpload.CURSOR.HAND,
			
			// Flash Settings
			flash_url : siteRoot + "/handsam_core/library/swfupload/swf/swfupload.swf",
	
			custom_settings : {
				upload_target : "divFileProgressContainer",
				thumbScriptUrl: siteRoot + "/handsam_core/functionality/account_logo/php/thumbnail.php"
			},
			
			// Debug Settings
			debug: false
	        
	    });
	    
	    
	    // assign our event handlers
	    $('.swfupload-control')
	    
			.bind('fileQueueError', function(event, file, errorCode, message){
				var swfu = $.swfupload.getInstance(this);
				fileQueueError(file, errorCode, message, swfu);
			})
	    
			.bind('fileDialogComplete', function(event, numFilesSelected, numFilesQueued){
				var swfu = $.swfupload.getInstance(this);
				fileDialogComplete(numFilesSelected, numFilesQueued, swfu);
			})
	    
	    
			.bind('uploadProgress', function(event, file, bytesLoaded){
				var swfu = $.swfupload.getInstance(this);
				uploadProgress(file, bytesLoaded, swfu);
			})

			.bind('uploadError', function(event, file, errorCode, message){
				var swfu = $.swfupload.getInstance(this);
				uploadError(file, errorCode, message, swfu);
			})
			
			.bind('uploadSuccess', function(event, file, serverData){
				var swfu = $.swfupload.getInstance(this);
				uploadSuccess(file, serverData, swfu);
				
			})
			
			.bind('uploadComplete', function(event, file){
				var swfu = $.swfupload.getInstance(this);
				uploadComplete(file, swfu);
				// upload has completed, lets try the next one in the queue
				$(this).swfupload('startUpload');
					
			})
			
			.bind('fileQueued', function(event, file){
				// start the upload since it's queued
				$(this).swfupload('startUpload');
			})
	});		
});