$(document).ready( function() {

	
	//date range filter
	var range_format = "dd/mm/yy";
	var newToDate = "";
	var d2 = null;
	var newFromDate = "";
	var d1 = null;

	/*date to*/
	$( "input.date_to" ).datepicker({ 
		dateFormat: range_format,
		showButtonPanel: true,
		beforeShow: function( input ) {
		setTimeout(function() {
		  var buttonPane = $( input )
		    .datepicker( "widget" )
		    .find( ".ui-datepicker-buttonpane" );
		  
		  $( "<button>", {
		    text: "Clear",
		    click: function() {
		      $.datepicker._clearDate( input );
		    }
		  }).appendTo( buttonPane );
		}, 1 );
		},
		onClose: function(dateText, inst) {
			/*
			newToDate = dateText.split('/');
			d2 = new Date(newToDate[2], newToDate[1]-1, newToDate[0]);
			newFromDate = $('input.date_from').datepicker( "getDate" );
			 
			if (newFromDate != null && newFromDate != "") {
			 	d1 = new Date(newFromDate);
		 	} else {
			 	d1 = null;
		 	}
		 	
			if (d1<d2 || newFromDate == null || newToDate == "") {
				if (newToDate == "") {
					window.location = "?to_date=reset";
				} else {
					window.location = "?to_date=" + newToDate[2] + "-" + newToDate[1] + "-" + newToDate[0];
				}
			} else {
				alert("The \"from\" date is after the \"to\" date. Please enter a new date in one of the fields for the filter fo work.");
			}
			*/
		} 
	});
	
	$('input.date_to').live('click', function() {
		$(this).datepicker({
			dateFormat: range_format,
			showOn:'focus'
		}).focus();
	});
	
	$("img.date_to_img").live('click', function() { 
		if ($(this).prev("input.date_to").datepicker({dateFormat: range_format})) {
			$(this).prev("input.date_to").datepicker( "show" );
		}
	});	
	
	//process the date fields and reload page with date query string
	$('#from_date_go, #to_date_go').click(function(event){		
		
		ToDate = $('#to_date_filter').val();
		FromDate = $('#from_date_filter').val();
		if (FromDate != null && FromDate != "") {
			newFromDate = FromDate.split('/');
		 	d1 = new Date(FromDate);
	 	} else {
		 	d1 = null;
	 	}
		
		if (ToDate != null && ToDate != "") {
			newToDate = ToDate.split('/');
		 	d2 = new Date(ToDate);
	 	} else {
		 	d2 = null;
	 	}
		
		var qs = "";
		if (d1<d2 || d2 == null || d2 == null) {
			if (newToDate == "" || newToDate == null) {
				qs += "?to_date=reset";
			} else {
				//newToDate = dateText.split('/');
				qs += "?to_date=" + newToDate[2] + "-" + newToDate[1] + "-" + newToDate[0];
			}
			if (newFromDate == "" || newFromDate == null) {
				qs += "&from_date=reset";
			} else {
				//newFromDate = dateText.split('/');
				qs += "&from_date=" + newFromDate[2] + "-" + newFromDate[1] + "-" + newFromDate[0];
			}
			window.location = qs;
		} else {
			alert("The \"from\" date is after the \"to\" date. Please enter a new date in one of the fields for the filter fo work.");
		}
	});
		
	
	//reset to date
	$('#reset_to_date').click(function(event){
		window.location = "?to_date=reset";
	});	

	
	/*date from*/
	$( "input.date_from" ).datepicker({ 
		dateFormat: range_format,
		showButtonPanel: true,
		beforeShow: function( input ) {
		setTimeout(function() {
		  var buttonPane = $( input )
		    .datepicker( "widget" )
		    .find( ".ui-datepicker-buttonpane" );
		  
		  $( "<button>", {
		    text: "Clear",
		    click: function() {
		      $.datepicker._clearDate( input );
		    }
		  }).appendTo( buttonPane );
		}, 1 );
		},
		onClose: function(dateText, inst) {
		} 
	});
	
	$('input.date_from').live('click', function() {
		$(this).datepicker({
			dateFormat: range_format,
			showOn:'focus'
		}).focus();
	});
	
	$("img.date_from_img").live('click', function() { 
		if ($(this).prev("input.date_from").datepicker({dateFormat: range_format})) {
			$(this).prev("input.date_from").datepicker( "show" );
		}
	});	

	

	//reset from date
	$('#reset_from_date').click(function(event){
		window.location = "?from_date=reset";
	});	
	
	
	
	
	//select date range filter
	$('#select_date_filter').change(function(event){
	var drf = $(this).val();
		window.location = "?date_range=" + drf;
	});
	

	//module filter
	$('#mod_filter').change(function(event){
	var mod_id = $(this).val();
		window.location = "?appid=" + mod_id;
	});
	
	
	//tasklist filter
	$('#tasklist_filter').change(function(event){
	var tl_id = $(this).val();
		window.location = "?appid=1&tasklist=" + tl_id;
	});

	
	//manager filter
	$('#man_filter').click(function(event){
		if ($(this).val() == "View my accounts") {
			window.location = "?manfilter=Mine";
		} else {
			window.location = "?manfilter=All";
		}
	});	

	
	// account name filter
	var theTable = $('#acc_list')
	var acc_name_url = siteRoot + '/handsam_core/functionality/accountlist_tables/ajax/acc_name.php';
		
	$("#acc_name_filter").keyup(function() {
		$.uiTableFilter( theTable, this.value, 'Account name' );
		$.post(acc_name_url, { accname: this.value } );
	})
	
	
	//reset acc name filter
	$('#reset_acc_name').click(function(event){
		$.post(acc_name_url, { accname: '' }, function(data) {
			window.location = window.location.href;
		});
	});	
	
	//select state filter
	$('#state_filter').change(function(event){
	var sf = $(this).val();
		window.location = "?state=" + sf;
	});
	
});