<?php
/***
** View reports with an account per row
** 
** 
***/
echo "<ul class=\"params\"></ul>";
echo "<div id=\"dashboard\" class=\"clearfix\">";
echo "<ul class=\"tabs\">";
foreach($tabs as $tab => $tab_title) {
    echo "<li><a href=\"#tab_$tab\">$tab_title</a></li>";
}
echo "</ul>";

foreach($tabs as $tab => $tab_title) {
    echo "<div id=\"tab_$tab\">";
    $table_cnt = 0;
    
    foreach ($rows[$tab] as $table_key => $table) {
        $table_cnt ++;
        $sorting_class = "table_sort";
        if ($core_conf['dashboard_group_select'] && $tab == 'tasklist' && $table_cnt == 1) $sorting_class = "";
        ?>
            
        <table class="<?php echo $sorting_class; ?> acc_list table_<?php echo $table_cnt;?>" id="">
            <thead>
                <tr>
                    <?php
                    $cnt = 0;
                    foreach ($cols[$tab][$table_key] as $col_key => $col_name) {
                        $cnt ++;
                        $class = "nolink";
                        if ($cnt == 1) $class = " start nolink";
                        if ($cnt == count($cols[$tab][$table_key])) $class = "end nolink";
                        echo "<th class=\"$col_key $class\">$col_name</th>";

                    }
                    ?>

                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($rows[$tab][$table_key])) {
                    foreach ($rows[$tab][$table_key] as $row_key => $row_name) {
                        echo "<tr class=\"row_".str_replace(" ", "_", $row_key)."\">";
                        //echo "<td class=\"start account_name \">$row_name</td>";
                        $cnt = 0;
                        foreach ($cols[$tab][$table_key] as $col_key => $col_name) {
                            $cnt ++;
                            $td_class = "";
                        //for ($i=1; $i<$cnt; $i++) {
                            if ($cnt == 1) { 
                                echo "<td class=\"start account_name col_$col_key\">$row_name</td>";
                            } else {
                                if ($cnt == count($cols[$tab][$table_key])) $td_class = "end ";
                                echo "<td class=\"$td_class col_$col_key\"><a href=\"dashboard.php#\"></a></td>";
                            }
                        //}
                        }
                        echo "</tr>";
                    }
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td class="start"></td>
                    <?php
                    if ($cnt > 2) echo "<td colspan=\"".($cnt-2)."\"></td>";
                    ?>
                    <td class="end"></td>
                </tr>
            </tfoot>

        </table>   
        <?php
    }
    echo "</div>";
}
echo "</div>";
?>