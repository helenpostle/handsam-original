<?php
/***
** Shows the list of licences in the system
** 
** 
***/

?>
<table id="acc_list" class="acc_list">
	<caption class="hidden"><?php echo $title;?></caption>
	<thead>
		<tr>
			<th class="start lid nolink ">LID</th>
			<th class=" nolink ">Module</th>
			<th class=" tasklistname nolink ">Tasklist</th>
			<th class=" nolink ">Technical Renewal Date</th>
			<th class=" nolink ">State</th>
			<th class=" nolink "></th>
			<th class="state  nolink end"></th>
		</tr>
	</thead>
	<tbody>

<?php
$last_accid = array();
while ($row = getRow($result)) {
	$row_link1 = "";
	$row_link2 = "";
	$row_text1="";
	$row_text2="";
	if ($row["appname"] == "Tasks") {
		if (isAllowed("editOurTasks",1 , $row['tasklistid'])) $row_link1 = "$rootUrl/modules/tasklist/tasks.php?licenceid=".$row['licenceid']."&amp;accountid=1";
		if (isAllowed('myTasks')) $row_link2 = "$rootUrl/modules/tasklist/mytasks.php?licenceid=".$row['licenceid']."&amp;accountid=1";
		$row_text1=">> Manage Tasks";
		$row_text2=">> My Tasks";
	}	
	if ($row["appname"] == "Accident Book") {
		if (isAllowed("recordAccidents")) $row_link1 = "$rootUrl/modules/accidents/index.php";
		$row_text1=">> View Incident Log";
	}	
	if ($row["appname"] == "Training Log") {
		if (isAllowed("viewTraining")) $row_link2 = "$rootUrl/modules/training/my_training.php";
		$row_text2=">> View My Training";
		if (isAllowed("viewTraining")) $row_link1 = "$rootUrl/modules/training/index.php";
		$row_text1=">> Manage Training";
	}	
	?>
	<tr>
		<td class="start"><a href=""><?php echo displayText($row["licenceid"]) ?></a></td>
		<td class=""><a href=""><?php echo displayText($row["appname"]) ?></a></td>
		<td class=""><a href=""><?php echo displayText($row["tasklistname"]) ?></a></td>
		<td class=""><a href=""><?php echo displayDate($row["end_date"]) ?></a></td>
		<td class=" state"><a href=""><?php echo displayText($row["state"]) ?></a></td>
		<td class=" ">
		<?php
		if ($row_link1 != "") echo "<a href=\"$row_link1\">$row_text1</a>";
		?>
		</td>
		<td class="end ">
		<?php
		if ($row_link2 != "") echo "<a href=\"$row_link2\">$row_text2</a>";
		?>
		</td>
	</tr>
	<?php 
}?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
 			<td class="end"></td>
 		</tr>
  	</tfoot>

</table>

