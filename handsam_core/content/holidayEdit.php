<?php 
/**
 * Shows a form for updating a user
 */
 

if (isset($holidayid)) { 
	$rsHoliday = getRS($con,"select * from licenceholiday where holidayid = $holidayid");
	$rowHoliday = getRow($rsHoliday);
	echo startFormTable($_SERVER["PHP_SELF"], "", "",HOLIDAY_FORM_TITLE, HOLIDAY_FORM_INFO);
	//echo frmLegend($frmTitle);
	//echo formNotes(HOLIDAY_FORM_TITLE, HOLIDAY_FORM_INFO);
	echo frmHiddenField($holidayid,"holidayid");
	echo frmHiddenField($licenceid,"licenceid");
	echo frmHiddenField($tasklistid,"tasklistid");
	echo frmTextField($rowHoliday,"title",40,"Holiday Title",false);
	if ($holidayid == 0) {
		echo frmDateFieldNonDb (time(), "holidaystart", "Start Date", true);
		echo frmDateFieldNonDb (time(), "holidayend", "End Date", true);
	} else {
		echo frmDateField($rowHoliday,"holidaystart","Start date",true);
		echo frmDateField($rowHoliday,"holidayend","End date",true);
	}	
	if ($accountid != 0 && isAllowed("editAccount")) {
		echo frmShowAudit($rowHoliday,$con);	
	}?>
	
	
<?php
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($licenceid != 0 && isAllowed("editAccount"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this holiday?", "Click here to delete this holiday");
	
	echo endFormTable();
}
?>