<?php

/**
 * Shows a form for updating a user
 */

echo startFormTable($_SERVER["PHP_SELF"], "hdm_frm_a", "", USERS_FORM_TITLE, USERS_FORM_INFO);
echo frmHiddenField($userid, "userid");
if(isset($return))
    echo frmHiddenField($return, "return");

$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
if($userid == 0)
{
    //enter username & password for new user
    echo frmTextField($rowUser, "username", 100, "Email", true);

    #echo account name if super admin adding to another account
    if(isAllowed("editAccount") && isset($accountid) && $accountid != $clsUS->accountid)
    {
        $rsAccount = getRS($con, "SELECT accountname FROM account WHERE accountid = " . $accountid);
        $rowAccount = getRow($rsAccount);

        echo frmRowNoInput($rowAccount["accountname"], "Account Name");
        if($core_conf['user_payroll']) echo frmTextField($rowUser, 'payroll', 30, 'Payroll Number', true);
    }
    else if(isAllowed("editAccount") && isset($accountid) && $accountid == $clsUS->accountid && $core_conf['user_payroll'])
    {
      echo frmTextField($rowUser, 'payroll', 30, 'Payroll Number', false);
    }
    elseif(isAllowed("editAccount") && !isset($accountid) && $accountid != $clsUS->accountid)
    {
        //echo "help";
    }
    else if(isAllowed("editUsers"))
    {
        if($core_conf['user_payroll']) echo frmTextField($rowUser, 'payroll', 30, 'Payroll Number', true);
    }
}
else if(isAllowed("editUsers"))
{
    echo frmTextField($rowUser, "username", 100, "Email", true);
    if($core_conf['user_payroll']) {
      if (isset($accountid) && $accountid == $clsUS->accountid && $accountid == 1) {
        echo frmTextField($rowUser, 'payroll', 30, 'Payroll Number', false);
      } else {
        echo frmTextField($rowUser, 'payroll', 30, 'Payroll Number', true);
      }
    }
}
else
{
    //non editable
    echo frmRowNoInput($rowUser["username"], "Email");
    if(isAllowed("editAccount") && $rowUser['accountid'] != $clsUS->accountid)
    {
        //non editable
        echo frmRowNoInput($rowUser["accountname"], "Account Name");
    }
}

echo frmTextField($rowUser, "firstname", 50, "First Name", true);
echo frmTextField($rowUser, "lastname", 50, "Last Name", true);

if(!$self)
{
    echo frmSelectArray($rowUser, "state", $state, "State", true);
}

// include multiselect for core_SA_usrtype select box
// get sA usertypes for this user
$qry_SA = "select * from core_SA_usr_usrtype where userid = $userid";
$qry_RS = getRS($con, $qry_SA);

//query for sa usertypes multi select
$qry_core_SA_usrtype = "select SA_usrtype as a, SA_usrtype_title as b from core_sa_usrtype";

// display if in superadmin account (1) and not a HandsamSuperAdmin user
if(isAllowed('editHandsamSuperAdmin') && isset($aid) && $aid == 1 && !$self)
{
    $hsa_val = 0;
    if(isset($_POST['HSA']))
    {
        $hsa_val = $_POST['HSA'];
    }
    else if(isset($_POST['bt_save']) && $_POST['bt_save'])
    {
        $hsa_val = 0;
    }
    else if($rowUser['usertype'] == 'HandsamSuperAdmin')
    {
        $hsa_val = 1;
    }
    echo frmCheckBoxNonDb($hsa_val, 'HSA', 'Handsam Super Admin');
}

if(isset($aid) && $aid == 1 && !$self && isAllowed("editAccount"))
    echo frmMultiSelectQuery($qry_RS, 'SA_usrtype', $qry_core_SA_usrtype, $con, 'select super admin usertypes', true);



if(isset($accountid))
    echo frmHiddenField($accountid, "accountid");

if(!isset($self) || !$self)
{
    if(in_array("tasklist", $installed_modules))
    {
        if($userid == 0)
        {
            $qryTLOpt = "SELECT distinct t.tasklistid ,t.tasklistname, 'None' AS usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid WHERE a.accountid = $accountid and a.end_date > NOW()";
        }
        else
        {
            $qryTLOpt = "SELECT distinct   t.tasklistid ,t.tasklistname, ut.usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid LEFT JOIN usr u ON u.accountid = a.accountid LEFT OUTER JOIN usr_usrtype ut ON u.userid = ut.userid AND t.tasklistid = ut.tasklistid WHERE u.userid = $userid and a.end_date > NOW()";
        }
        $rsTLOpt = getRS($con, $qryTLOpt);

        while ($rowTLOpt = getRow($rsTLOpt))
        {
            echo frmSelectArrayValue($rowTLOpt["usertype"], "usertype_" . $rowTLOpt["tasklistid"], $userTypes, $rowTLOpt["tasklistname"] . " List", true);
        }
    }

    //next can select a usertype for each app  from tasks app
    //first get apps for this account

    $qryAcApp = "Select distinct a.appid, a.appname from accountlicence l left join apps a on l.appid = a.appid where l.accountid = $accountid and a.appid != 1 and l.state = 'ACTIVE' and l.end_date > NOW() and l.start_date < NOW()";
    //echo $qryAcApp;
    $rsAcApp = getRS($con, $qryAcApp);

    while ($rowAcApp = getRow($rsAcApp))
    {
        //if ($aid != 1) {
        if($userid == 0)
        {
            $qryAOpt = "SELECT distinct a.appid ,a.appname, 'None' AS usertype FROM apps a LEFT JOIN accountlicence l ON a.appid = l.appid WHERE l.accountid = $accountid and a.appid = " . $rowAcApp['appid'];
        }
        else
        {
            $qryAOpt = "SELECT t.usrtype as usertype FROM usrtype t left join usr_usrtype ut on t.usrtype = ut.usertype left join usr u on u.userid = ut.userid  WHERE u.userid = $userid AND t.appid = " . $rowAcApp['appid'];
        }
        $rsAOpt = getRS($con, $qryAOpt);
        if(getResultRows($rsAOpt))
        {
            $rowAOpt = getRow($rsAOpt);
            $qryUserType = "select usrtype from usrtype where appid = " . $rowAcApp['appid'];
            $rsUT = getRS($con, $qryUserType);
            $appUserTypes = array("None" => "No Access");
            while ($rowAUT = getRow($rsUT))
            {
                if($rowAcApp['appid']==2)
                {
                    //lotc usertype config file check
                    if($rowAUT['usrtype']=='LOTC Governor' && $lotc_conf['governor_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if($rowAUT['usrtype']=='LOTC Finance' && $lotc_conf['finance_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if($rowAUT['usrtype']=='LOTC Principal' && $lotc_conf['principal_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if($rowAUT['usrtype']=='LOTC Deputy Principal' && $lotc_conf['deputy_principal_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if($rowAUT['usrtype']=='evc - no alerts' && $lotc_conf['evc_no_alerts_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if ($rowAUT['usrtype']=='evc' || $rowAUT['usrtype']=='visit planner' )
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                }
                else
                {
                    $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
            }
        }
        else
        {
            $qryAOpt = "SELECT distinct a.appid ,a.appname, 'None' AS usertype FROM apps a LEFT JOIN accountlicence l ON a.appid = l.appid WHERE l.accountid = $accountid and a.appid = " . $rowAcApp['appid'];
            $rsAOpt = getRS($con, $qryAOpt);
            $rowAOpt = getRow($rsAOpt);
            $qryUserType = "select usrtype from usrtype where appid = " . $rowAcApp['appid'];
            $rsUT = getRS($con, $qryUserType);
            $appUserTypes = array("None" => "No Access");
            while ($rowAUT = getRow($rsUT))
            {
                if($rowAcApp['appid']==2)
                {
                    //lotc usertype config file check
                    if($rowAUT['usrtype']=='LOTC Governor' && $lotc_conf['governor_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if($rowAUT['usrtype']=='LOTC Finance' && $lotc_conf['finance_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if($rowAUT['usrtype']=='LOTC Principal' && $lotc_conf['principal_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if($rowAUT['usrtype']=='LOTC Deputy Principal' && $lotc_conf['deputy_principal_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if($rowAUT['usrtype']=='evc - no alerts' && $lotc_conf['evc_no_alerts_usr']===true) 
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                    else if ($rowAUT['usrtype']=='evc' || $rowAUT['usrtype']=='visit planner' )
                    {
                      $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                    }
                }
                else
                {
                    $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
            }
        }
        // HAND-52, if new && config is true && is Training Log app then show Training Staff by default
        if($userid == 0 && ($training_conf['new_user_training_staff'] = true) && $rowAcApp["appid"] == 4)
            $rowAOpt["usertype"] = 'Training Staff';
        echo frmSelectArrayValue($rowAOpt["usertype"], "app_usertype_" . $rowAcApp["appid"], $appUserTypes, $rowAcApp["appname"] . " App", true); // same line twice, so moved here.
        //}
    }

    // add a job role multi select
    if($core_conf['jobroles'] === true)
    {
        // Get the preselected values in a recordset
        $sql = "SELECT jobrole_id AS role_id FROM core_usr_jobrole WHERE usr_id = '$userid';";
        $rs = getRS($con, $sql);
        // field to compare
        $field = 'role_id';
        // query to get all possible job role values
        $qry = "SELECT role_id AS a, role_title AS b FROM core_jobrole WHERE state <> 'DELETED' ORDER BY created ASC;";
        $title = 'Select Job Role(s)';
        echo frmMultiSelectQuery($rs, $field, $qry, $con, $title, FALSE);
    }
    
    // key holder
    if($core_conf['key_holder'] === true)
    {
        echo frmCheckBox($rowUser, 'key_holder', 'Is this user an emergency contact?');
    }
    
    // address & contact details
    echo '<div id="toggle_showhide"><span>Contact Details &#x25BC;</span></div>';
    echo '<div id="contact_details">';
    echo '<div class="col">';
    echo frmTextField($rowUser,"address1",100,"Address 1",false);
    echo frmTextField($rowUser,"address2",100,"Address 2",false);
    echo frmTextField($rowUser,"address3",100,"Address 3",false);
    echo frmTextField($rowUser,"city",100,"City",false);
    echo '</div>';
    echo '<div class="col">';
    echo frmTextField($rowUser,"postcode",100,"Postcode",false);
    echo frmTextField($rowUser,"tel1",100,"Telephone 1",false);
    echo frmTextField($rowUser,"tel2",100,"Telephone 2",false);
    echo '</div>';
    echo '</div>';
}


if($userid != 0 && isAllowed("editUsers")){
    echo frmShowAudit($rowUser, $con);
    
}

if ($userid > 0){
    frmRowNoInput($userid, "Log in count");
}

if ($userid ==  $clsUS->userid) echo frmRowNoInput($clsUS->visits, "Log in count");


if($rowUser["lastlogin"] != NULL)
{
    echo frmRowNoInput(displayDateTime($rowUser["lastlogin"]), "Last logged in");
}


echo frmButtonHelp("Save", "save", "Click here to save changes");
echo frmButtonHelp("Cancel", "cancel", "Click here to cancel");
if($userid != 0 && (!isset($self) || !$self))
    echo frmButtonConfirmHelp("Delete", "delete", "Are you sure you want to delete this user?", "Click here to delete this user");

echo endFormTable();
