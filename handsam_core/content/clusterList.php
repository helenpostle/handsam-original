<?php
/***
** Shows the list of account clusters in the system
** 
** 
***/


if (isAllowed("editAccount")) {
	?>
	<div class="add_acc" ><a href="account_cluster.php?id=0">>> Add a new cluster</a></div>

  <table class="acc_list" id="acc_list">
    <caption class="hidden"><?php echo $title;?></caption>
    <thead>
      <tr>
        <th class="start">ID</th>           
        <th class="accountname">Cluster Title</th>             
        <th class="accountname ">Cluster Manager</th>         
        <th class="state end">State</th>
      </tr>
    </thead>
    <tbody>

  <?php
  while ($row = getRow($result)) {
    $link = "account_cluster.php?id={$row['id']}";
	$sql = "SELECT manager, concat(firstname,' ', lastname) as name FROM account_cluster_manager join usr on usr.userid = manager WHERE cluster_id =".$row['id'];
	$list = '';
	$rs_sel = getRS($con, $sql);
	while ($mrow = getRow($rs_sel)) {
	    $list .= $mrow['name']."<br />";
	}
    ?>
    <tr>
      <td class="start accountname"><a href="<?php echo $link;?>"><?php echo displayText($row["id"]) ?></a></td>
      <td class=" accountname"><a href="<?php echo $link;?>"><?php echo displayText($row["title"]) ?></a></td>
      <td class=" accountname"><a title='<?php echo displayText($list);?>' href="<?php echo $link;?>"><?php echo displayText($list) ?></a></td>
      <td class="end accountname"><a href="<?php echo $link;?>"><?php echo displayText($row["state"]) ?></a></td>

      </tr>
      <?php 
  }
 
  ?>
    </tbody>
    <tfoot>
      <tr>
        <td class="start"></td>
        <td></td>
        <td></td>
        <td class="end"></td>
      </tr>
      </tfoot>
  </table>
  <?php
   
}
?>