<?php
/**
 * Shows a list of files and form for uploading a file
 * 
 */
if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
	$count_col = "t.taskid";	
} else {
	$count_col = "t.accounttaskid";	
}
$qry_f = "select f.extra_text, f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by, count(".$count_col.") as cnt FROM files f left join task_files t on f.fileid = t.fileid  where f.fileid = $fileid group by f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by";

$rsFiles = getRS($con,$qry_f);
$rowFile = getRow($rsFiles);

//extra query to check for lotc files. If this is an lotc file, can't delete

$qry_lotc = "select count(fileid) as cnt from lotc_files where fileid = $fileid";
$rsLotc = getRS($con, $qry_lotc);
$rowLotc = getRow($rsLotc);


if (!isset($uploaded_filename) || $uploaded_filename == "") {
	$uploaded_filename = $rowFile['filename'];
        echo startFileFormTable($_SERVER["PHP_SELF"], "", UPLOAD_FORM_TITLE, UPLOAD_FORM_INFO_EDIT);
}
else
{
    echo startFileFormTable($_SERVER["PHP_SELF"], "", UPLOAD_FORM_TITLE, UPLOAD_FORM_INFO);
}
//echo frmLegend('Upload File');
echo frmHiddenField($filetype,"filetype");
echo frmHiddenField($fileid,"fileid");
echo frmHiddenField($contentid,"contentid");
echo frmHiddenField($edittype,"edit");
echo frmHiddenField($accountid,"accountid");
//echo frmHiddenField($uploaded_filename,"uploaded_filename");
echo frmHiddenField(70000000,"MAX_FILE_SIZE");


if (isset($uploaded_filename) || $uploaded_filename != "") {
	echo "<div class=\"frm_row clearfix\"><span class=\"label\">Filename</span><span class=\"value\"><div class=\"upload\"><a title=\"download this document\" href=\"downloadDocument.php?id=".$fileid."\" title=\"\">".$uploaded_filename."</a></div></span></div>";
}

$fileFieldLabel = "Select File";
$fileFieldReq = true;
if (isset($uploaded_filename) || $uploaded_filename != "") 
{
    $fileFieldLabel = "Select a New File";
    $fileFieldReq = false;
}
echo frmFileField("filename", $fileFieldLabel, $fileFieldReq);

echo frmHiddenField($uploaded_filename, 'uploaded_filename');
if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
    $doc_cat_tmp = $docCategory;
    unset($doc_cat_tmp['QGP']);
	echo frmSelectArray($rowFile, "filecategory" , $doc_cat_tmp, "Category" , true);	
}

if(isset($documents_conf['file_upload_year']) && $documents_conf['file_upload_year'])
{
  echo frmTextField($rowFile,"title",100,"Year / Title",true);
}
else 
{
  echo frmTextField($rowFile,"title",100,"Title",true);
}

if(isset($documents_conf['file_upload_location']) && $documents_conf['file_upload_location'])
{
   echo frmTextArea($rowFile, "extra_text", 5, "Location", false);
}



//echo frmTextArea($rowFile,"description",5,"Description",false);
if ($fileid != 0 && ((isAllowed("uploadAdminDocs") && $filetype == "adminDoc") || isAllowed("uploadHandsamDocs"))) {
	echo frmShowAudit($rowFile,$con);	
}
echo frmButtonHelp("Save","save", "Click here to save changes");
echo frmButtonHelp("Cancel","cancel", "Click here to cancel upload");
if ($fileid != 0 && $rowFile['cnt'] == 0 && $rowLotc['cnt'] == 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this file?", "Click here to delete this link");
	
echo endFormTable();


?>