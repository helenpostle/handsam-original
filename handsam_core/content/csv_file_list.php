<?php 
/**
 * list uploaded csv import files
 */

?>
<p>
  <a href="<?php echo "$rootUrl/handsam_core/tools.php?tool=3&amp;fileid=0";?>">Upload csv</a>
</p> 
		<table class="acc_list">
		<thead>
			<tr>
		    	<th class="nolink start accountname">CSV Title</th>
		    	<th class="nolink accountname">Filename</th>
		    	<th class="nolink cat">Date Uploaded</th>
				<th class="end centre"></th>
			</tr>
		</thead>
		<tbody class="noStrip">	
		<?php
		while ($row = getRow($rsFiles)) {
      $link_title = "Edit and replace this csv document";
      $link = "$rootUrl/handsam_core/tools.php?tool=3&amp;fileid=".$row['id'];
			echo "<tr>\n";
			echo "<td class=\"start\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"$link\">".textSummary($row['title'], 40)."</a></td>\n";
			echo "<td class=\"\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"$link\">".textSummary($row['filename'], 30)."</a></td>\n";
			echo "<td class=\"\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"$link\">".displayDateTime($row['created'])."</a></td>\n";
      echo "<td class=\"end\"></td>";
			echo "</tr>\n";
		} ?>

		</tbody>
		<tfoot>
			<tr>
				<td class="start"></td>
				<td class=""></td>
				<td class=""></td>
				<td class="end"></td>
	  		</tr>
	  	</tfoot>
		
		</table>		