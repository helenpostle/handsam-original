<?php
/***
** Shows the list of users in the system
** @todo make it so clicking on colunm headings reorders list? or add dropdown to view only a certain usertype? or alphabetical listing?
** i.e. make it easier for when there are loads of users
***/


//show only handsam super admin users to handsam super admin users only
if (isAllowed("viewHandsamSuperAdmin")) {
	$qry = "SELECT usr.* FROM usr where accountid = ".$clsUS->accountid." and usr.state != 'DELETED' and usertype = 'HandsamSuperAdmin' order by lastname";	
	$result = getRS($con, $qry);
	?>
	<h2>Handsam Super Admin Users</h2>
	<table  id="user_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start username nolink ">Username</th>
				<th class="">First Name</th>
				<th class="">Last Name</th>
				<th class="">Email Alerts</th>
				<th class="end">State</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	$last_usr = array();
	while ($row = getRow($result)) {
			if (!in_array($row['userid'],$last_usr)) {
			$last_usr[] = $row['userid'];
			if ($row["late_task_email_alert"] == 1) {
				$checked = " checked ";
			} else {
				$checked = "";
			}
		?>
				<tr>
					<td class="start"><a href="users.php?userid=<?php echo $row["userid"] ?>&amp;return=users"><?php echo $row["username"] ?></a></td>
					<td class=""><?php echo displayText($row["firstname"]) ?></td>
					<td class=""><?php echo displayText($row["lastname"]) ?></td>
					<td class="state"><input class="table_check_box" type="checkbox" value="<?php echo $row["userid"]; ?>" name="email_alert[]" <?php echo $checked; ?>/></td>
					<td class="end"><?php echo displayText($row["state"]) ?></td>
				</tr>
		<?php 
		}
	} ?>
		</tbody>
		<tfoot>
			<tr>
				<td class="start"><a href="users.php?userid=0&amp;return=users&amp;accountid=1">Add a new user</a></td>
				<td colspan="3"></td>
				<td class="end"></td>
	  		</tr>
	  	</tfoot>
	
	</table>
	<?php
}

//show non handsam super admin users
if (isAllowed("editAccount")) {
	$qry = "SELECT usr.* FROM usr left join core_SA_usr_usrtype csuu on usr.userid = csuu.userid  where usr.accountid = ".$clsUS->accountid." and usr.state != 'DELETED' and csuu.SA_usrtype is not null order by  lastname";
	
	$result = getRS($con, $qry);
	?>
	<h2>Other Super Admin Users</h2>
	<table  id="user_list2" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start username nolink ">Username</th>
				<th class="">First Name</th>
				<th class="">Last Name</th>
				<th class="">Email Alerts</th>
				<th class="end">State</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	$last_usr = array();
	while ($row = getRow($result)) {
			if (!in_array($row['userid'],$last_usr)) {
			$last_usr[] = $row['userid'];
			$usertype = displayText($row["usertype"]);
			if ($row["late_task_email_alert"] == 1) {
				$checked = " checked ";
			} else {
				$checked = "";
			}
		?>
				<tr>
					<td class="start"><a href="users.php?userid=<?php echo $row["userid"] ?>&amp;return=users"><?php echo $row["username"] ?></a></td>
					<td class=""><?php echo displayText($row["firstname"]) ?></td>
					<td class=""><?php echo displayText($row["lastname"]) ?></td>
					<td class="state"><input class="table_check_box" type="checkbox" value="<?php echo $row["userid"]; ?>" name="email_alert[]" <?php echo $checked; ?>/></td>
					<td class="end"><?php echo displayText($row["state"]) ?></td>
				</tr>
		<?php 
		}
	} ?>
		</tbody>
		<tfoot>
			<tr>
				<td class="start"><a href="users.php?userid=0&amp;return=users&amp;accountid=1">Add a new user</a></td>
				<td colspan="3"></td>
				<td class="end"></td>
	  		</tr>
	  	</tfoot>
	
	</table>
	<?php
	echo addCsrfField();
}
?>

