<?php

/**
 * List of Job Roles
 *
 * Show a list of all roles for this account, with a link to create a new one.
 *
 */

$this_page = "jobroleslist.php";

//$sql = "SELECT role_id, role_title, role_desc FROM core_jobrole WHERE accountid = $accountid ORDER BY created ASC;";  // per account
$sql = "SELECT role_id, role_title, role_desc FROM core_jobrole WHERE state <> 'DELETED' ORDER BY created ASC;";

$result = getRS($con, $sql);

?>
<table id="jobroles_list" class="acc_list">
	<caption class="hidden"><?php echo $title;?></caption>
	<thead>
		<tr>
			<th class="start ">Job Role</th>
			<th class="end ">Role Description</th>
		</tr>
	</thead>
	<tbody>
<?php
while ($row = getRow($result)) :
?>
	<tr>
		<td class="start catname"><a href="jobroles.php?role_id=<?php echo $row['role_id']; ?>" title="Edit this job role"><?php echo $row['role_title'] ?></a></td>
		<td class="end state"><?php echo $row['role_desc']; ?></td>
	</tr>
<?php
endwhile;
?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"><a href="jobroles.php?role_id=0">Add a new Job Role</a></td>
			<td class="end"></td>
  		</tr>
  	</tfoot>

</table>


