<?php
//this is a generic tools content page
echo "<h2>Module Tools Menu</h2>";
echo "<p>Choose an option from the list below:</p>\n";
foreach ($installed_modules as $mod) {
	if (file_exists("$rootPath/modules/$mod/tools/menu.php")) include("$rootPath/modules/$mod/tools/menu.php");
}

echo "<h3>File Management</h3>";
echo "<ul>";
echo "<li><a href=\"?tool=1\">Clean up files</a></li>";
echo "<li><a href=\"?tool=2\">Missing files</a></li>";
echo "</ul>";

echo "<h3>CSV Import</h3>";
echo "<ul>";
echo "<li><a href=\"?tool=3\">Import users from CSV</a></li>";
echo "</ul>";


?>