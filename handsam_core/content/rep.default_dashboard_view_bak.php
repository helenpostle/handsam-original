<?php
/***
** View reports with an account per row
** 
** 
***/
$cols = count($dashboard->reports);
echo "<ul class=\"params\">$subtitle</ul>";

//all non-account rows
?>
<table class="acc_list" id="acc_list">
	<thead>
		<tr>
			<th class="account_name start nolink">
            <?php
            if (isset($dashboard->col_headings[1][1])) {
                echo $dashboard->col_headings[1][1];
            } else {
                echo ACCOUNT_TYPE_2;
            }
            ?>
            </th>
			<?php 
			$col_cnt = 0;
			//get each report name
			foreach ($dashboard->reports as $a_report) {
				$col_cnt++;
				echo "<th class=\"";
				if ($col_cnt == $cols) echo "end";
				echo " nolink\">{$a_report->title2}</th>";
			}
			?>
		</tr>
	</thead>
	<tbody>

<?php
	//foreach ($dashboard->ids as $acc_id) {
	for ($i=0;$i<count($dashboard->ids); $i++) {
		$acc_id = $dashboard->ids[$i];
		//once we get a valid accountid then stop the loop because we'll assume that all the following rows are valid accounts
		if (is_numeric($acc_id)) break;
		echo "<tr class=\"";
		if ($acc_id == 'all') {
			echo " highlight_2";
		} else if (!is_numeric($acc_id)) {
			 echo " highlight ";
		}
		echo "\">";
		$rep_cnt = 0;
		foreach ($dashboard->reports as $report) {
			if (isset($report->result[$acc_id])) {
				$dbLink = "?report_id={$report->result[$acc_id]['link_report']}&amp;group_id=0&amp;accountid=$acc_id&amp;params=".urlencode(serialize($report->params));
			} else {
				$dbLink = "#";
			}
			if ($rep_cnt == 0) echo "<td class=\"start account_name \"><a href=\"$dbLink\">".$dashboard->id_names[$i]."</a></td>";
			$rep_cnt++;
			//echo "<td class=\"{$report_item->report_class} report_$rep_id\"><a href=\"?report_id={$report_item->link_report_id}&amp;params=".urlencode(serialize($report_item->params))."\">".$report_item->result['result']."</a></td>";
			echo "<td class=\" ";
			if ($rep_cnt == $cols) echo "end";
			echo " ".$report->td_class."\">";
			if (isset($report->result[$acc_id])) {
				echo "<a href=\"?report_id={$report->result[$acc_id]['link_report']}&amp;accountid=$acc_id&amp;params=".urlencode(serialize($report->params))."\">".$report->result[$acc_id]['result']."</a>";
			} else {
				echo "<a href=\"#\"></a>";
			}
			echo "</td>";
		}
		echo "</tr>";
	}
?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td colspan="<?php echo $cols-1;?>"></td>
 			<td class="end"></td>
 		</tr>
  	</tfoot>

</table>

<?php
//accounts only table;
?>

<table class="acc_list" id="acc_list2">
	<thead>
		<tr>
			<th class="account_name start nolink">Account</th>
			<?php 
			$col_cnt = 0;
			//get each report name
			foreach ($dashboard->reports as $a_report) {
				$col_cnt++;
				echo "<th class=\"";
				if ($col_cnt == $cols) echo "end";
				echo " nolink\">{$a_report->title2}</th>";
			}
			?>
		</tr>
	</thead>
	<tbody>

<?php
	//foreach ($dashboard->ids as $acc_id) {
	for ($i=0;$i<count($dashboard->ids); $i++) {
		$acc_id = $dashboard->ids[$i];
		if (!is_numeric($acc_id)) continue;
		$rep_cnt = 0;
		foreach ($dashboard->reports as $report) {
			if (isset($report->result[$acc_id])) {
				$dbLink = "?report_id={$report->result[$acc_id]['link_report']}&amp;group_id=0&amp;accountid=$acc_id&amp;params=".urlencode(serialize($report->params));
			} else {
				$dbLink = "#";
			}
			if ($rep_cnt == 0) echo "<td class=\"start account_name \"><a href=\"$dbLink\">".$dashboard->id_names[$i]."</a></td>";
			$rep_cnt++;
			//echo "<td class=\"{$report_item->report_class} report_$rep_id\"><a href=\"?report_id={$report_item->link_report_id}&amp;params=".urlencode(serialize($report_item->params))."\">".$report_item->result['result']."</a></td>";
			echo "<td class=\" ";
			if ($rep_cnt == $cols) echo "end";
			echo " ".$report->td_class."\">";
			if (isset($report->result[$acc_id])) {
				echo "<a href=\"?report_id={$report->result[$acc_id]['link_report']}&amp;accountid=$acc_id&amp;params=".urlencode(serialize($report->params))."\">".$report->result[$acc_id]['result']."</a>";
			} else {
				echo "<a href=\"#\"></a>";
			}
			echo "</td>";
		}
		echo "</tr>";
	}
?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td colspan="<?php echo $cols-1;?>"></td>
 			<td class="end"></td>
 		</tr>
  	</tfoot>

</table>