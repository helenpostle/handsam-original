<?php
/***
** Shows the list of accounts in the system
** 
** 
***/


//select filter by module/app
$qry = "select * from apps ";
$rsApps = getRS($con, $qry);
if (getResultRows($rsApps) > 0) {
	echo "<div class=\"filter\">";
	echo "<label for=\"mod_filter\">Module: </label>";
	echo "<select id=\"mod_filter\">";
	echo "<option value=\"0\"";
	if ($appid == 0) echo " selected ";
	echo ">All Modules</option>";
	while ($rowList = getRow($rsApps)) {
		echo "<option value=\"".$rowList['appid']."\"";
		if ($appid == $rowList['appid']) echo " selected ";
		echo ">".$rowList['appname']."</option>";
	}
	echo "</select>";
	echo "</div>";
}

foreach ($installed_modules as $mod) {
	if (file_exists("$rootPath/modules/$mod/shared/accountlist_inc.php")) require_once("$rootPath/modules/$mod/shared/accountlist_inc.php");	
}


//select filter by account manager
$qry = "SELECT count(accountid) as cnt FROM  account where account_manager={$clsUS->userid}";;
$rsManager = getRS($con, $qry);
$rowManager = getRow($rsManager);
if ($rowManager['cnt'] > 0) {
	echo "<div class=\"filter\">";
	echo "<label for=\"man_filter\">Account manager: </label>";
	if ($manfilter == "All" || $manfilter == "") {
		echo "<input type=\"button\" value=\"View my accounts\" name=\"man_filter\" id=\"man_filter\"/>";
	} else {
		echo "<input type=\"button\" value=\"View all accounts\" name=\"man_filter\" id=\"man_filter\"/>";
	}
	echo "</div>";
}


//select filter by school type
	echo "<div class=\"filter\">";
	echo "<label for=\"sch_type_filter\">".ACCOUNT_TYPE_1.": </label>";
	echo "<select id=\"sch_type_filter\">";
	echo "<option value=\"ALL\"";
	//if ($sch_type_id == 0) echo " selected ";
	if ($sch_type_filter == 0) echo " selected ";
	echo ">All Schools Types</option>";
	Foreach ($school_type as $key=>$val) {
		echo "<option value=\"$key\"";
		if ($sch_type_filter == $key) echo " selected ";
		echo ">$val</option>";
	}
	echo "</select>";
	echo "</div>";

//select filter by school status
	echo "<div class=\"filter\">";
	echo "<label for=\"sch_status_filter\">".ACCOUNT_TYPE_2.": </label>";
	echo "<select id=\"sch_status_filter\">";
	echo "<option value=\"ALL\"";
	if ($sch_status_filter == 0) echo " selected ";
	echo ">All Schools Status</option>";
	Foreach ($school_status as $key=>$val) {
		echo "<option value=\"$key\"";
		if ($sch_status_filter == $key) echo " selected ";
		echo ">$val</option>";
	}
	echo "</select>";
	echo "</div>";


//select filter by school pupils
	echo "<div class=\"filter\">";
	echo "<label for=\"school_pupils_filter\">".ACCOUNT_TYPE_3.": </label>";
	echo "<select id=\"school_pupils_filter\">";
	echo "<option value=\"ALL\"";
	if ($school_pupils_filter == 0) echo " selected ";
	echo ">All ".ACCOUNT_TYPE_3."s</option>";
	Foreach ($school_pupils as $key=>$val) {
		echo "<option value=\"$key\"";
		if ($school_pupils_filter == $key) echo " selected ";
		echo ">$val</option>";
	}
	echo "</select>";
	echo "</div>";

	
//select filter by region / authority
	echo "<div class=\"filter\">";
	echo "<label for=\"authority_filter\">Authority / Region: </label>";
	echo "<input name=\"authority_filter\" id=\"authority_filter\" value=\"$authority_filter\" type=\"text\"/>";
	echo "<img id=\"reset_authority\" src=\"$rootUrl/images/delete.png\"/>";
	echo "</div>";

	if ($core_conf['account_name_filter']) {
	//select filter by account name
		echo "<div class=\"filter\">";
		echo "<label for=\"acc_name_filter\">Account Name: </label>";
		echo "<input name=\"acc_name_filter\" id=\"acc_name_filter\" value=\"$acc_name_filter\" type=\"text\"/>";
		echo "<img id=\"reset_acc_name\" src=\"$rootUrl/images/delete.png\"/>";
		echo "</div>";
	}
	
  /*
	if ($core_conf['account_cluster_filter']) {
		//select filter by account cluster
		echo "<div class=\"filter\">";
		echo "<label for=\"acc_cluster_filter\">".ACCOUNT_TYPE_4.": </label>";
		echo "<select id=\"acc_cluster_filter\">";
		echo "<option value=\"ALL\"";
		if ($acc_cluster_filter == 0) echo " selected ";
		echo ">All ".ACCOUNT_TYPE_4."s</option>";
		Foreach ($school_cluster as $key=>$val) {
			echo "<option value=\"$key\"";
			if ($acc_cluster_filter == $key) echo " selected ";
			echo ">$val</option>";
		}
		echo "</select>";
		echo "</div>";
	}
   * 
   */
  if ($core_conf['account_cluster_filter']) {
		//select filter by account cluster
		echo "<div class=\"filter\">";
		echo "<label for=\"acc_cluster_filter\">Account Cluster: </label>";
		echo "<select id=\"acc_cluster_filter\">";
		echo "<option value=\"0\"";
		if ($acc_cluster_filter == 0) echo " selected ";
		echo ">All Account Clusters</option>";
    $cluster_qry = "SELECT c.id AS a, c.title AS b FROM account_cluster c  WHERE c.state != 'DELETED' AND c.state != 'INACTIVE' $cluster_filter_qry ORDER BY c.title ASC";
    $clusterRs = getRS($con, $cluster_qry);
		while ($school_cluster = getRow($clusterRs))
    {
			echo "<option value=\"{$school_cluster['a']}\"";
			if ($acc_cluster_filter == $school_cluster['a']) echo " selected ";
			echo ">{$school_cluster['b']}</option>";
		}
		echo "</select>";
		echo "</div>";
	}
	
//select filter by account postcode
	echo "<div class=\"filter\">";
	echo "<label for=\"acc_postcode_filter\">Postcode: </label>";
	echo "<input name=\"acc_postcode_filter\" id=\"acc_postcode_filter\" value=\"$acc_postcode_filter\" type=\"text\"/>";
	echo "<img id=\"reset_acc_postcode\" src=\"$rootUrl/images/delete.png\"/>";
	echo "</div>";

	
//select filter by account dfe
	echo "<div class=\"filter\">";
	echo "<label for=\"acc_dfe_filter\">Dfe Number: </label>";
	echo "<input name=\"acc_dfe_filter\" id=\"acc_dfe_filter\" value=\"$acc_dfe_filter\" type=\"text\"/>";
	echo "<img id=\"reset_acc_dfe\" src=\"$rootUrl/images/delete.png\"/>";
	echo "</div>";

	
//reset all
	echo "<div class=\"filter\">";
	echo "<label for=\"reset_filter\">Reset All Filters: </label>";
	echo "<input type=\"button\" value=\"Reset\" name=\"reset_filter\" id=\"reset_filter\"/>";
	echo "</div>";

//export
	echo "<div class=\"filter\">";
	echo "<label for=\"export\">Export CSV File: </label>";
	echo "<input type=\"button\" value=\"Export\" name=\"export\" id=\"export\"/>";
	echo "</div>";
	
?>

<?php
if (isAllowed("handsamOnly") || ($core_conf['add_accounts'] == true && isAllowed("editAccount"))) {
	?>
	<div class="add_acc" ><a href="accounts.php?accountid=0&amp;tasklist=<?php echo $tasklist;?>">>> Add a new account</a></div>
	<?php
}
?>
<table class="acc_list" id="acc_list">
	<caption class="hidden"><?php echo $title;?></caption>
	<thead>
		<tr>
			<th class="start accountname nolink ">Account name</th>
                    <?php if($core_conf['cost_code']): ?>
			<th class=" nolink">Cost Code</th>
                    <?php else: ?>
			<th class=" nolink">Dfe number</th>
                    <?php endif; ?>
			<th class="address nolink">Address</th>
			<th class=" nolink">Postcode</th>
			<?php
			if (($appid > 0 && $appid != 1) || $appid ==1 && $tasklist > 0) {
				?>
				<th class="licence nolink">licence</th>
				<?php
			}
			?>
			<th class="state end nolink">State</th>
		</tr>
	</thead>
	<tbody>

<?php
$last_accid = array();
while ($row = getRow($result)) {
	if (!in_array($row['accountid'], $last_accid)) {
		$last_accid[] = $row['accountid'];
	
		?>
		<tr>
			<td class="start accountname"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist;  ?>"><?php echo displayText($row["accountname"]) ?></a></td>
			<td class="address"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist; ?>"><?php
                            if($core_conf['cost_code'])
                                echo displayText($row["cost_code"]);
                            else
                                echo displayText($row["school_dfe"]);
                                
                            ?></a></td>
			<td class="address"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist; ?>"><?php echo displayText($row["address1"]) ?>, <?php echo displayText($row["town"]) ?></a></td>
			<td class="address"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist; ?>"><?php echo displayText($row["postcode"]) ?> </a></td>
			<?php
			if (($appid > 0 && $appid != 1) || $appid ==1 && $tasklist > 0) {
				?>
				<td class="licence"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist;  ?>"><?php echo displayDate($row["licence_end"]) ?></a></td>
				<?php
			}
			?>
			<td class="end state"><a href="accounts.php?accountid=<?php echo $row['accountid']."&amp;tasklist=".$tasklist;  ?>"><?php echo displayText($row["state"]) ?></a></td>
		</tr>
		<?php 
	} 
}?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<?php
			if (($appid > 0 && $appid != 1) || $appid ==1 && $tasklist > 0) {
				?>
				<td></td>
				<?php
			}
			?>
			<td></td>
			<td></td>
			<td></td>
 			<td class="end"></td>
 		</tr>
  	</tfoot>

</table>
<?php
if (isAllowed("handsamOnly") || ($core_conf['add_accounts'] == true && isAllowed("editAccount"))) {
	?>
	<div class="add_acc" ><a href="accounts.php?accountid=0&amp;tasklist=<?php echo $tasklist;?>">>> Add a new account</a></div>
	<?php
}
?>