<?php
/***
** View reports with an account per row
** 
** 
***/
echo "<ul class=\"params\">$subtitle</ul>";
?>
<table class="acc_list" id="acc_list">
	<thead>
		<tr>
			<th class="account_name start nolink">Date</th>
			<th class="nolink">Category</th>
			<th class="nolink">Description</th>
			<th class="nolink end">Username</th>			
		</tr>
	</thead>
	<tbody>

<?php
    $this_report = array_pop($dashboard->reports);
    //var_dump($this_report);
    //die();
    if ($this_report && count($this_report->result) > 0) {
        foreach ($this_report->result as $report_item) {
            echo "<tr>";
            echo "<td class=\"start account_name \"><a href=\"\">".$report_item['actiondate']."</a></td>";
            echo "<td class=\"\"><a href=\"#\">".$report_item['category_name']."</a></td>";
            echo "<td class=\"\"><a href=\"#\">".$report_item['customtext']."</a></td>";
            echo "<td class=\"end \"><a href=\"#\">".$report_item['username']."</a></td>";
            echo "</tr>";
        }
    }
?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td colspan="2"></td>
 			<td class="end"></td>
 		</tr>
  	</tfoot>

</table>