<?php
/**
 * Shows a list of files and form for uploading a file
 * 
 */


if (!isset($uploaded_filename) || $uploaded_filename == "") {
	$uploaded_filename = $rowFile['filename'];
}
echo startFileFormTable($_SERVER["PHP_SELF"], "", UPLOAD_FORM_TITLE, UPLOAD_FORM_INFO);
//echo frmLegend('Upload File');

echo frmHiddenField($filetype,"filetype");
echo frmHiddenField($fileid,"fileid");
echo frmHiddenField($contentid,"contentid");
echo frmHiddenField($edittype,"edit");
echo frmHiddenField($accountid,"accountid");

echo frmTextAreaNonEdit($rowFile,"title","Title");
echo "<div class=\"frm_row clearfix\"><span class=\"label\">Uploaded Filename</span><span class=\"value\"><div class=\"upload\"><a title=\"Download this document\" href=\"downloadDocument.php?id=".$fileid."\" title=\"\">".$uploaded_filename."</a></div></span></div>";
echo frmTextAreaNonEdit($rowFile,"filecategory","File category");

echo frmShowAudit($rowFile,$con);	

echo frmButtonHelp("Back","cancel", "Click here to go back");
	
echo endFormTable();


?>