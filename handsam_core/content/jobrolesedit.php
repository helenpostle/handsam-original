<?php

/**
 * Job Roles CRUD Form
 */

$this_page = "jobrolesedit.php";

if(isset($jobroleid))
{
	// get any existing values
	$sql = "SELECT * FROM core_jobrole WHERE role_id = '$jobroleid'";
	$rsJobRole = getRS($con, $sql);
	$rowJobRole = getRow($rsJobRole);
	
	echo startFormTable($_SERVER["PHP_SELF"], "", "", JOBROLES_FORM_TITLE, JOBROLES_FORM_INFO);
	
	// hidden fields
	echo frmHiddenField($jobroleid, 'role_id');
	// echo frmHiddenField($accountid, 'accountid'); per account job roles
	
	// title & description
	echo frmTextField($rowJobRole, 'role_title', 100, "The Job Role's title", true);
	echo frmTextArea($rowJobRole, 'role_desc', 5, "A description of the Job Role", true);
	
	// default state
	echo frmSelectArray($rowJobRole, "state" , array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE"), "State" , true);
	
	// if has permissions, show the audit
	if ($jobroleid != 0 && isAllowed("editJobRole")) echo frmShowAudit($rowJobRole, $con);
	
	// buttons
	echo frmButtonHelp("Save", "save", "Click to save changes");
	echo frmButtonHelp("Cancel", "cancel", "Click to cancel and go to the Job Role listing");
	
	if ($jobroleid != 0) echo frmButtonConfirmHelp("Delete", "delete", "Are you sure you want to delete this Job Role?", "Click to delete this category");
	
	echo endFormTable();
}