<?php
/***
** Shows the list of licences in the system
** 
** 
***/

//now get links to all current licences and display in a menu

//select filter by module/app
$qry = "select * from apps ";
$rsApps = getRS($con, $qry);
if (getResultRows($rsApps) > 0) {
	echo "<div class=\"filter\">";
	echo "<label for=\"mod_filter\">Module: </label>";
	echo "<select id=\"mod_filter\">";
	echo "<option value=\"0\"";
	if ($appid == 0) echo " selected ";
	echo ">All Modules</option>";
	while ($rowList = getRow($rsApps)) {
		echo "<option value=\"".$rowList['appid']."\"";
		if ($appid == $rowList['appid']) echo " selected ";
		echo ">".$rowList['appname']."</option>";
	}
	echo "</select>";
	echo "</div>";
}

foreach ($installed_modules as $mod) {
	if (file_exists("$rootPath/modules/$mod/shared/accountlist_inc.php")) require_once("$rootPath/modules/$mod/shared/accountlist_inc.php");	
}


//select filter by account manager
$qry = "SELECT count(accountid) as cnt FROM  account where account_manager={$clsUS->userid}";;
$rsManager = getRS($con, $qry);
$rowManager = getRow($rsManager);
if ($rowManager['cnt'] > 0) {
	echo "<div class=\"filter\">";
	echo "<label for=\"man_filter\">Account manager: </label>";
	if ($manfilter == "All" || $manfilter == "") {
		echo "<input type=\"button\" value=\"View my accounts\" name=\"man_filter\" id=\"man_filter\"/>";
	} else {
		echo "<input type=\"button\" value=\"View all accounts\" name=\"man_filter\" id=\"man_filter\"/>";
	}
	echo "</div>";
}


//select filter by account name
	echo "<div class=\"filter\">";
	echo "<label for=\"acc_name_filter\">Account Name: </label>";
	echo "<input name=\"acc_name_filter\" id=\"acc_name_filter\" value=\"$acc_name_filter\" type=\"text\"/>";
	echo "<img id=\"reset_acc_name\" src=\"$rootUrl/images/delete.png\"/>";
	echo "</div>";

//state filter
	echo "<div class=\"filter\">";
	echo "<label for=\"state_filter\">Licence State: </label>";
	echo "<select id=\"state_filter\">";
	echo "<option value=\"ALL\"";
	if ($state_filter == "ALL") echo " selected ";
	echo ">All States</option>";
	echo "<option value=\"ACTIVE\"";
	if ($state_filter == "ACTIVE") echo " selected ";
	echo ">Active</option>";
	echo "<option value=\"NEW\"";
	if ($state_filter == "NEW") echo " selected ";
	echo ">New</option>";
	echo "</select>";
	echo "</div>";

	
	
	
	echo "<div id=\"date_filter_group\" class=\"clearfix\">";
//from date
	$licence_date_from = strftime("%d/%m/%Y",time());
	echo "<div class=\"filter\">";
	echo "<label for=\"from_date_filter\">From Date: </label>";
	echo "<input name=\"from_date_filter\" id=\"from_date_filter\" readonly=\"true\" class=\"date_from\" value=\"".displayDate($from_date)."\"/>";
	echo "<img class=\"date_from_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.from_date_filter\"/>";
	echo "<img id=\"from_date_go\" src=\"$rootUrl/images/accept.png\"/>";
	echo "<img id=\"reset_from_date\" src=\"$rootUrl/images/delete.png\"/>";
	echo "</div>";


//to date
	$licence_date_to = strftime("%d/%m/%Y",time());
	echo "<div class=\"filter\">";
	echo "<label for=\"to_date_filter\">To Date: </label>";
	echo "<input name=\"to_date_filter\" id=\"to_date_filter\" readonly=\"true\" class=\"date_to\" value=\"".displayDate($to_date)."\"/>";
	echo "<img class=\"date_to_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.to_date_filter\"/>";
	echo "<img id=\"to_date_go\" src=\"$rootUrl/images/accept.png\"/>";
	echo "<img id=\"reset_to_date\" src=\"$rootUrl/images/delete.png\"/>";
	echo "</div>";


//select date range filter
	echo "<div class=\"filter\">";
	echo "<label for=\"select_date_filter\">Date Range: </label>";
	echo "<select id=\"select_date_filter\">";
	echo "<option value=\"billing_date\"";
	if ($select_date_filter == "billing_date") echo " selected ";
	echo ">Billing Date</option>";
	echo "<option value=\"end_date\"";
	if ($select_date_filter == "end_date") echo " selected ";
	echo ">Renewal Date</option>";
	echo "</select>";
	echo "</div>";

	
	echo "</div>"; //close date filter group div
	
?>

<table id="acc_list" class="acc_list">
	<caption class="hidden"><?php echo $title;?></caption>
	<thead>
		<tr>
			<th class="start lid nolink ">LID</th>
			<th class=" accountname nolink ">Account name</th>
			<th class=" nolink ">Module</th>
			<th class=" tasklistname nolink ">Tasklist</th>
			<th class=" nolink ">Technical Renewal Date</th>
			<th class=" nolink ">Licence Billing Date</th>
			<th class="state  nolink end">State</th>
		</tr>
	</thead>
	<tbody>

<?php
$last_accid = array();
while ($row = getRow($result)) {
	
	?>
	<tr>
		<td class="start"><a href="?licenceid=<?php echo $row['licenceid']?>"><?php echo displayText($row["licenceid"]) ?></a></td>
		<td class="accountname"><a href="?licenceid=<?php echo $row['licenceid']?>"><?php echo displayText($row["accountname"]) ?></a></td>
		<td class=""><a href="?licenceid=<?php echo $row['licenceid']?>"><?php echo displayText($row["appname"]) ?></a></td>
		<td class=""><a href="?licenceid=<?php echo $row['licenceid']?>"><?php echo displayText($row["tasklistname"]) ?></a></td>
		<td class=""><a href="?licenceid=<?php echo $row['licenceid']?>"><?php echo displayDate($row["end_date"]) ?></a></td>
		<td class=""><a href="?licenceid=<?php echo $row['licenceid']?>">
		<?php 
		//if ($row['contract_length'] > 0) {
			echo displayDate($row["next_billing_date"]);
		//} 
		?>
		</a></td>
		<td class="end state"><a href="?licenceid=<?php echo $row['licenceid']?>"><?php echo displayText($row["state"]) ?></a></td>
	</tr>
	<?php 
}?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
			<td></td>
 			<td class="end"></td>
 		</tr>
  	</tfoot>

</table>

