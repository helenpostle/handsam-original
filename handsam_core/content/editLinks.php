<?php
/**
 * Shows a list of files and form for uploading a file
 * 
 */
if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
	$count_col = "t.taskid";	
} else {
	$count_col = "t.accounttaskid";	
}

$rsLinks = getRS($con,"select l.title, l.url, l.linkid, l.linkcategory, l.description, l.created, l.edited, l.created_by, l.edited_by, count(".$count_col.") as cnt FROM links l left join task_links t on l.linkid = t.linkid where l.linkid = $linkid group by l.title, l.url, l.linkid, l.linkcategory, l.description, l.created, l.edited, l.created_by, l.edited_by");
$rowLink = getRow($rsLinks);


//extra query to check for lotc links. If this is an lotc link, can't delete

$qry_lotc = "select count(linkid) as cnt from lotc_links where linkid = $linkid";
$rsLotc = getRS($con, $qry_lotc);
$rowLotc = getRow($rsLotc);

echo startFileFormTable($_SERVER["PHP_SELF"], "", "", LINKS_FORM_TITLE, LINKS_FORM_INFO);
//echo frmLegend('Edit Link');
echo frmHiddenField($filetype,"filetype");
echo frmHiddenField($linkid,"linkid");
echo frmHiddenField($accountid,"accountid");
echo frmHiddenField($contentid,"contentid");
echo frmHiddenField($edittype,"edit");
//echo formNotes(LINKS_FORM_TITLE, LINKS_FORM_INFO);
if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
    $doc_cat_tmp = $docCategory;
    unset($doc_cat_tmp['QGP']);    
	echo frmSelectArray($rowLink, "linkcategory" , $doc_cat_tmp, "Category" , true);	
}

echo frmTextField($rowLink,"url",200,"Url",true);
echo frmTextField($rowLink,"title",100,"Title",true);
echo "<span class=\"goToUrl\" <a href=\"#\" onclick=\"openLink();\">Go to URL in a new window</a></span>";
//echo frmTextArea($rowLink,"description",5,"Description",false);
if ($linkid != 0 && ((isAllowed("uploadAdminDocs") && $filetype == "adminDoc") || isAllowed("uploadHandsamDocs"))) {
	echo frmShowAudit($rowLink,$con);	
}
echo frmButtonHelp("Save","save", "Click here to save changes");
echo frmButtonHelp("Cancel","cancel", "Click here to cancel upload");
if ($linkid != 0 && $rowLink['cnt'] == 0 && $rowLotc['cnt'] == 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this link?", "Click here to delete this link");
	
echo endFormTable();


?>