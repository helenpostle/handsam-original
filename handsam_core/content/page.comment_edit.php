<?php 
/**
 * Shows a form for updating a lotc comment
 */
 
if (isAllowed("editAccount")) {
 
	echo startFormTable($_SERVER["PHP_SELF"], "", "comment");
	echo frmLegend('Edit Comment');
	echo frmHiddenField($clsUS->userid,"userid");
	echo frmHiddenField($accountid,"accountid");
	echo frmHiddenField($commentid,"commentid");
	echo frmTextArea($row,"commenttext","5","Comment",true);
			
	if ($commentid != 0) {
		echo frmShowAudit($row,$con);	
	}
	
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($row['created_by'] == $clsUS->userid) echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this comment?", "Click here to delete this comment");
	echo frmHiddenField($tkn,"tkn");
	echo endFormTable();
}
?>