<?php
/**
 * Shows a list of files and form for uploading a file
 * 
 */

echo startFileFormTable($_SERVER["PHP_SELF"], "", "", LINKS_FORM_TITLE, LINKS_FORM_INFO);
//echo frmLegend('Edit Link');
echo frmHiddenField($filetype,"filetype");
echo frmHiddenField($linkid,"linkid");
echo frmHiddenField($accountid,"accountid");
echo frmHiddenField($contentid,"contentid");
echo frmHiddenField($edittype,"edit");
//echo formNotes(LINKS_FORM_TITLE, LINKS_FORM_INFO);
echo frmTextAreaNonEdit($rowLink, 'linkcategory', 'Link category');
echo frmTextAreaNonEdit($rowLink,"url","Url");
echo frmTextAreaNonEdit($rowLink,"title","Title");
echo "<span class=\"goToUrl\" <a href=\"#\" onclick=\"openLink();\">Go to URL in a new window</a></span>";
echo frmShowAudit($rowLink,$con);	
echo frmButtonHelp("Back","cancel", "Click here to go back");
echo endFormTable();


?>