<?php 
/**
 * Shows a form for editing a licence
 */
 

if (isset($licenceid) && isAllowed("editAccount")) { 
	$licenceDuration =  $rowlicence['end_date'] - $rowlicence['start_date'];
	echo startFormTable($_SERVER["PHP_SELF"], "", "", "Module: {$rowlicence['appname']}", "");
	echo frmHiddenField($accountid,"accountid");
	echo frmHiddenField($licenceid,"licenceid");
	echo frmHiddenField($tasklist,"tasklist");
	echo frmHiddenField($appid,"appid");
	
	if ($rowlicence["tasklistname"] != "") {
		echo frmRowNoInput($rowlicence["tasklistname"],"Tasklist: ", "title");
	}
	echo frmTextAreaNonEdit($rowlicence,'state' ,"Licence State");
	if ($licenceid == 0) {
		$billing_date = 0;
		echo frmDateFieldNonDb ($start_date, "start_date", "Technical Start Date", true);
		echo frmDateFieldNonDb ($billing_date, "billing_date", "Initial/Last Billing Date", false);
	} else {
		echo frmDateField($rowlicence,"start_date","Technical Start date",true);
	//new biling date field
		if ($rowlicence['billing_date'] == "" || $rowlicence['billing_date'] == null ) {
			$billing_date = time();
		} else {
			$billing_date = strtotime($rowlicence['billing_date']);
		}
		echo frmDateFieldNonDb ($billing_date, "billing_date", "Initial/Last Billing Date", false);	
	}

	
	//new contract length field
	echo frmTextField($rowlicence,"contract_length",20,"Remaining Contract Length (years)",false);
	
	if ($accountid != 0 && isAllowed("editAccount")) {
		echo frmShowAudit($rowlicence,$con);	
	}
	
	if (file_exists("$rootPath/modules/{$rowlicence['folder']}/shared/licence_conf.php")) {
		include("$rootPath/modules/{$rowlicence['folder']}/shared/licence_conf.php");
	}		
	
	if ($rowlicence['state'] == 'NEW' && $rowlicence['appid'] > 1) echo frmButtonHelp("Activate","activate", "Click here to activate licence");
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($licenceid != 0 && isAllowed("editAccount"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this licence?", "Click here to delete this licence");
	
	echo endFormTable();
}
?>