<?php 
/**
 * list uploaded documents on server
 */
$categoryCol = false;
$qs = "?filetype=".$filetype;
#links only used for superAdmin for now but structure in place for other users to add links
if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
	
	$ifSuperAdminAccountId = "";
	if (isAllowed("uploadHandsamDocs")) {
		if ($filetype == "handsamDoc") {
			$categoryCol = true;
			$qry = "SELECT l.mid, l.title, l.url, l.linkid, l.linkcategory, count(t.taskid) as cnt FROM links l left join task_links t on l.linkid = t.linkid WHERE l.linktype = 'handsamDoc' and l.accountid = $accountid group by l.mid, l.title, l.url, l.linkid, l.linkcategory ORDER BY l.title asc";
		} else {
			$qs = "?filetype=".$filetype."&accountid=".$accountid;
			$ifSuperAdminAccountId = "&accountid=".$accountid;
			$qry = "SELECT l.mid, l.title, l.url, l.linkid, l.linkcategory, count(t.accounttaskid) FROM links l left join task_links t on l.linkid = t.linkid WHERE l.linktype = 'adminDoc' and l.accountid = $accountid group by l.mid, l.title, l.url, l.linkid, l.linkcategory ORDER BY l.title asc";
		}
		
	} else if (isAllowed("uploadAdminDocs")) {
		if ($filetype == "adminDoc") {
			$qry = "SELECT l.mid, l.title, l.url, l.linkid, l.linkcategory, count(t.accounttaskid) FROM links l left join task_links t on l.linkid = t.linkid WHERE l.linktype = 'adminDoc' and l.accountid = $accountid group by l.mid, l.title, l.url, l.linkid, l.linkcategory ORDER BY l.title asc";
		} else {
			$qry = "SELECT l.mid, l.title, l.url, l.linkid, l.linkcategory, count(t.accounttaskid) FROM links l left join task_links t on l.linkid = t.linkid WHERE l.linktype = 'userDoc' and l.accountid = $accountid and l.userid = $userid group by l.mid, l.title, l.url, l.linkid, l.linkcategory ORDER BY l.title asc";
		}
	
		
		
	} else if (isAllowed("uploadUserDocs")) {
			$qry = "SELECT l.mid, l.title, l.url, l.linkid, l.linkcategory, count(t.accounttaskid) FROM links l left join task_links t on l.linkid = t.linkid WHERE l.linktype = 'userDoc' and l.accountid = $accountid and l.userid = $userid group by l.mid, l.title, l.url, l.linkid, l.linkcategory ORDER BY l.title asc";
	}
	
	$result = getRS($con, $qry);
	if (getResultRows($result) > 0) {

		echo "<p class=\"info\">Click on the link below to edit its details. </p><p>Click on the attach button to attach it to the current task.</p><p class=\"\"><strong> You must then save all task changes in the main window! </strong></p>";	?>
		
		<table class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
		    	<th class="nolink start">Link Title</th>
		    	<th class="nolink">URL</th>
		    	<th class="nolink lid">Used</th>
		    	<?php
		    	if ($categoryCol) {
			    	echo "<th class=\"nolink cat\">Category</th>";
		    	}
		    	?>
				<th class="end centre"></th>
			</tr>
		</thead>
		<tbody class="noStrip">	
		<?php
		while ($row = getRow($result)) {
			if ($row['linkcategory'] == "") {
				$lcat = "no category";
			} else {
				$lcat = textSummary($docCategory[$row['linkcategory']], 14);
			}
            if ($row['mid'] > 0) {
                $link_title = "View this Handsam link";
            } else {
                $link_title = "Edit this link";
            }			
			echo "<tr>\n";
			echo "<td class=\"start\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"editLinks.php?linkid=".$row['linkid']."&filetype=".$filetype.$ifSuperAdminAccountId.$contentIdLink."\">".textSummary($row['title'], 30)."</a></td>\n";
			echo "<td class=\"\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"editLinks.php?linkid=".$row['linkid']."&filetype=".$filetype.$ifSuperAdminAccountId.$contentIdLink."\">".textSummary($row['url'], 30)."</a></td>\n";

			echo "<td class=\"\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"editLinks.php?linkid=".$row['linkid']."&filetype=".$filetype.$ifSuperAdminAccountId.$contentIdLink."\">".$row['cnt']."</a></td>\n";

			if ($categoryCol) {
				echo "<td class=\"\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"editLinks.php?linkid=".$row['linkid']."&filetype=".$filetype.$ifSuperAdminAccountId.$contentIdLink."\">".$lcat."</a></td>\n";
			}
			if ($edittype == "task") {
				//adds file to task edit form
				echo "<td class=\"end\"><a onclick=\"attachDoc(".$row['linkid'].", '".str_replace("'", "", $row['title'])."','adminLinks', 'link');\" class=\"tableLink attachLink\" title=\"\" href=\"#\">Attach</a></td>\n";
			} else {
				//ajax adds file page
				echo "<td class=\"end\"><a onclick=\"attachItemToPage('".$rootUrl."', ".$row['linkid'].", 'link', ".$contentid.");\" class=\"tableLink attachLink\" title=\"\" href=\"#\">Attach</a></td>\n";
			}
			
			echo "</tr>\n";
		} ?>

		</tbody>
		<tfoot>
			<tr>
				<td class="start"></td>
				<td class=""></td>
				<td class=""></td>
		    	<?php
		    	if ($categoryCol) {
					echo "<td></td>";
		    	}
		    	?>
				<td class="end"></td>
	  		</tr>
	  	</tfoot>
		
		</table>		
		
		<?php
		echo addCsrfField();
		
	} else {
		echo "<p>There are no links available. <a href=\"editLinks.php".$qs."\">Add a link here.</a>";
	}

}
?>	