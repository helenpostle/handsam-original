<?php 
/**
 * Shows a form for updating a user
 */
if (isAllowed("editAccount") && isset($accountid)) {
	
	if ($rowAccount['accountlogo'] != "" && file_exists("$rootPath/account_uploads/account_logo/".$rowAccount['accountlogo'])) {
		$thumb = "<img src=\"$rootUrl/account_uploads/account_logo/".$rowAccount['accountlogo']."\"/><a class=\"deleteThumb\" href=\"?accountid=$accountid\">[delete]</a>";
	} else {
		$thumb = "";
	}	

	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	
	
	//$tasklist_qry = "select tasklistid as a, tasklistname as b from tasklist";
	echo startFormTable($_SERVER["PHP_SELF"], "", "", "Edit Account");
	//echo frmLegend('Edit Account');
	
	echo "<div class=\"col acc1 clearfix\">";
	
	echo frmHiddenField($accountid,"accountid");
	echo frmHiddenField($tasklist,"tasklist"); // declared in ../accounts.php
	echo frmHiddenField("United Kingdom","country");
	echo frmTextField($rowAccount,"accountname",50,"Account name",true);
        
        if($core_conf['cost_code'])
            echo frmTextField($rowAccount, 'cost_code', 30, 'Cost Code', true);
	
	if ($accountid != 1) echo frmSelectArray($rowAccount, "school_type" , $school_type, ACCOUNT_TYPE_1, true);
	echo frmSelectArray($rowAccount, "school_status" , $school_status, ACCOUNT_TYPE_2 , true);
	if ($accountid != 1) echo frmSelectArray($rowAccount, "school_pupils" , $school_pupils, ACCOUNT_TYPE_3 , true);
	
  //if ($accountid != 1 && isset($core_conf['school_cluster']) && $core_conf['school_cluster']) echo frmSelectArray($rowAccount, "school_cluster" , $school_cluster, ACCOUNT_TYPE_4 , true);
  
  if (isset($core_conf['school_cluster']) && $core_conf['school_cluster'])
  {
    $cluster_qry = "SELECT c.id AS a, c.title AS b FROM account_cluster c  WHERE c.state != 'DELETED' AND c.state != 'INACTIVE'  ORDER BY c.title ASC";
    echo frmSelectQuery($rowAccount, "cluster_id", $cluster_qry, $con, 'Account Cluster', false);
    
  }
	
	
	echo frmTextField($rowAccount,"address1",50,"Address 1",true);
	echo frmTextField($rowAccount,"address2",50,"Address 2",false);
	echo frmTextField($rowAccount,"town",30,"Town",true);
	echo frmTextField($rowAccount,"county",30,"County",false);
	echo frmTextField($rowAccount,"postcode",10,"Post Code",true);

	if ($accountid != 1) {
	
		//new school dfe nuber field
		echo frmTextField($rowAccount,"school_dfe",50,"Dfe Number",false);
	
		//new authority field - use jquery autocomplete for this field
		echo frmTextField($rowAccount,"authority",50,"Authority",false);
		
		//new account contact field
		echo frmTextArea($rowAccount,"account_contact",3,"Account Contact",false);
	}
	
	echo frmTextField($rowAccount,"email",100,"Email address",true);
	echo frmTextField($rowAccount,"phone",20,"Telephone",true);
	
// new account manager drop down
	$account_manager_qry = "SELECT usr.userid AS a, usr.username AS b FROM usr LEFT JOIN core_SA_usr_usrtype ON usr.userid = core_SA_usr_usrtype.userid WHERE (usr.state != 'DELETED' AND usr.state != 'INACTIVE') AND core_SA_usr_usrtype.SA_usrtype = 'SuperAdmin'  ORDER BY usr.username ASC";
	if ($accountid != 1) echo frmSelectQuery($rowAccount, "account_manager", $account_manager_qry, $con, ACCOUNTS_ACCOUNT_MANAGER, false);
	
	// 2nd account manager drop down
	if ($accountid != 1 && $core_conf['acc_manager_2'] === true) echo frmSelectQuery($rowAccount, "account_manager_2", $account_manager_qry, $con, ACCOUNTS_ACCOUNT_MANAGER_2, false);
	
	// Upload logo
	echo frmSelectArray($rowAccount, "state" , $state, "State" , true);
	if ($accountid > 0) {
		echo "<div id=\"accountlogo\">";
		echo "<div class=\"optional\"><label for=\"upload_logo\">Upload logo</label><div id=\"upload_logo\" class=\"swfupload-control\"><span id=\"spanButtonPlaceholder\"></span></div></div>";
		echo "<div id=\"thumbnail\">$thumb</div>";
		echo "<div id=\"divFileProgressContainer\"></div>";
		echo "</div>";
	}
	
	// account edit history
	if ($accountid != 0 && isAllowed("editAccount")) {
		echo frmShowAudit($rowAccount,$con);	
	}
	
	// save cancel delete
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($accountid != 0 && isAllowed("editAccount") && $accountid != 1)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this account?", "Click here to delete this account");

	echo "</div>";
	
		echo "<div class=\"col acc2 clearfix\">";
	
	//comments area
  if ($rowAccount)
  {
    echo "<div class=\"comments\">";
    echo "<h3> Comments </h3>";

    if ($accountid > 0) {

      echo "<a class=\"add_comment\" href=\"0\">add comment</a>";

      $sql = "SELECT usr.firstname, usr.lastname, account_comments.* FROM account_comments LEFT JOIN usr ON account_comments.created_by = usr.userid WHERE account_comments.accountid = $accountid order by account_comments.created desc limit 0,8";
      $comments = getRS($con, $sql);
      echo "<ul id=\"comment_list\">";

      if (getResultRows($comments) > 0) {
        while ($rowComment = getRow($comments)) {
          echo "<li class=\"clearfix\">";
          echo "<strong>{$rowComment['firstname']} {$rowComment['lastname']}</strong> : <em>".displayDateTime($rowComment['created'])."</em>";
          //display edit button if comment is the users own
          //if ($rowComment['created_by'] == $clsUS->userid) {
            //echo " <a class=\"add_comment\" href=\"?accountid=$accountid&amp;commentid={$rowComment['commentid']}\">edit</a>";
          //}

          echo displayParagraphs($rowComment['commenttext']);
          echo "</li>";
        }
      } else {
        echo "<li> No Comments </li>";
      }
      echo "</ul>";
      //count comments - idf more than 8 show all
      $comment_qry = "select commentid from account_comments where accountid = $accountid";
      $comment_rs = getRS($con, $comment_qry);
      if (getResultRows($comment_rs) > 8) {
        echo "<a class=\"show_all\" href=\"#\">show all</a>";
      }
    } else {
      //new account contact field
      echo frmTextAreaNonDb("comment",5,"Enter the first comment below:",false, $a_comment);
    }
    echo "</div>";
  }
	echo "</div>";
	
	echo endFormTable();
} 

?>