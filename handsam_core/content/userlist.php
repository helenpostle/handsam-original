<?php
/***
** Shows the list of users in the system
** @todo make it so clicking on colunm headings reorders list? or add dropdown to view only a certain usertype? or alphabetical listing?
** i.e. make it easier for when there are loads of users
***/

if (isAllowed("editUsers")) {
	$qry = "SELECT usr.*, usr_usrtype.usertype FROM usr left join account on usr.accountid = account.accountid left join usr_usrtype on usr.userid = usr_usrtype.userid where account.accountid = ".$clsUS->accountid." and usr.state != 'DELETED'  order by usr_usrtype.usertype, lastname";
	
	$result = getRS($con, $qry);
	
			
	?>
	<table  id="user_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start username nolink ">Username</th>
				<th class="">First Name</th>
				<th class="">Last Name</th>
				<th class="">Email Alerts</th>
				<th class="end">State</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	$last_usr = array();
	while ($row = getRow($result)) {
			if (!in_array($row['userid'],$last_usr)) {
			$last_usr[] = $row['userid'];
			$usertype = displayText($row["usertype"]);
			if ($row["late_task_email_alert"] == 1) {
				$checked = " checked ";
			} else {
				$checked = "";
			}
		?>
				<tr>
					<td class="start"><a href="users.php?userid=<?php echo $row["userid"] ?>&amp;return=users"><?php echo $row["username"] ?></a></td>
					<td class=""><?php echo displayText($row["firstname"]) ?></td>
					<td class=""><?php echo displayText($row["lastname"]) ?></td>
					<td class="state"><input class="table_check_box" type="checkbox" value="<?php echo $row["userid"]; ?>" name="email_alert[]" <?php echo $checked; ?>/></td>
					<td class="end"><?php echo displayText($row["state"]) ?></td>
				</tr>
		<?php 
		}
	} ?>
		</tbody>
		<tfoot>
			<tr>
				<td class="start"><a href="users.php?userid=0&amp;return=users">Add a new user</a></td>
				<td colspan="3"></td>
				<td class="end"></td>
	  		</tr>
	  	</tfoot>
	
	</table>
	<?php
	echo addCsrfField();
}
?>

