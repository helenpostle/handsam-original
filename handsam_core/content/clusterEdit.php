<?php 
/**
 * Shows a form for updating an account cluster
 */
if (isAllowed("editAccount") && isset($cluster)) {
		
	
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");
	
	
	//echo startFormTable($_SERVER["PHP_SELF"], "", "", "Edit Account Cluster");
	echo startFormTable('https://handsam.org.uk/handsam_core/test.php',"","","Edit Account Cluster");
	echo frmHiddenField($id,"id");
    echo frmTextField($cluster->cluster,"title",50,"Title",true);
    echo frmTextArea($cluster->cluster,"description",5,"Description",false);
  // get list of people who can be a cluster manager
	$manager_qry = "SELECT usr.userid AS a, usr.username AS b FROM usr LEFT JOIN core_SA_usr_usrtype ON usr.userid = core_SA_usr_usrtype.userid WHERE (usr.state != 'DELETED' AND usr.state != 'INACTIVE') AND core_SA_usr_usrtype.SA_usrtype = 'SuperAdmin'  ORDER BY usr.username ASC";
	//$rs = getRS($con, $manager_qry);
	//get a list of the people who are cluster managers for this organisation
	//$rs = $cluster_manager->_getDetails();
	$cluster_manager_qry = "SELECT manager FROM handsam.account_cluster_manager where cluster_id = ".$id;
	$rs = getRS($con, $cluster_manager_qry);
	
	echo frmMultiSelectQuery($rs, "manager", $manager_qry, $con, "Cluster Manager", false);
//	echo frmSelectQuery($cluster->cluster, "manager", $manager_qry, $con, "Cluster Manager", false);
    echo frmSelectArray($cluster->cluster, "state" , $state, "State" , true);
  
    if ($id != 0) {
		echo frmShowAudit($cluster->cluster,$con);	
	}
    echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($id != 0)	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this cluster?", "Click here to delete this cluster");
  
  	
	echo endFormTable();
} 
echo $errorFlag;
?>