<?php
/**
 * Shows a list of files and form for uploading a file
 * 
 */
if (isset($import_content)) {
  echo $import_content;
} else {
  if (!isset($uploaded_filename)){
    $uploaded_filename = $rowFile['filename'];
  }
  echo startFileFormTable($_SERVER["PHP_SELF"], "", "Upload CSV file", "Select a csv file to upload");


  //echo frmLegend('Upload File');
  echo frmHiddenField($fileid,"fileid");
  echo frmHiddenField($tool,"tool");
  //echo frmHiddenField($uploaded_filename,"uploaded_filename");
  echo frmHiddenField(70000000,"MAX_FILE_SIZE");


  if (isset($uploaded_filename) || $uploaded_filename != "") {
    echo "<div class=\"frm_row clearfix\"><span class=\"label\">Filename</span><span class=\"value\"><div class=\"upload\"><a title=\"download this document\" href=\"downloadDocument.php?id=".$fileid."\" title=\"\">".$uploaded_filename."</a></div></span></div>";
  }

  $fileFieldLabel = "Select File";
  $fileFieldReq = true;
  if (isset($uploaded_filename) || $uploaded_filename != "") 
  {
      $fileFieldLabel = "Select a New File";
      $fileFieldReq = false;
  }
  echo frmFileField("filename", $fileFieldLabel, $fileFieldReq);

  echo frmHiddenField($uploaded_filename, 'uploaded_filename');

  echo frmTextField($rowFile,"title",100,"Title",true);
  echo frmTextArea($rowFile,"description",5,"Description",false);
  echo frmShowAudit($rowFile,$con);	

  echo frmButtonHelp("Save","save", "Click here to save changes");
  echo frmButtonHelp("Import","import", "Click here to run CSV import");
  echo frmButtonHelp("View","view", "Click here to view file");
  echo frmButtonHelp("Cancel","cancel", "Click here to cancel upload");
  //echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this file?", "Click here to delete this link");

  echo endFormTable();

}


?>