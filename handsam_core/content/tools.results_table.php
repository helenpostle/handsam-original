<?php
//process a tools results array into a table
if (isset($at_result) && is_array($at_result)) {
	echo "<p><a href=\"$rootUrl/handsam_core/tools.php\">Return to Admin Tools Menu Page</a></p>";
	if ($accidents_conf['send_alerts']) {
		echo startSimpleForm();	
		echo frmHiddenField($module,"mod");
		echo frmHiddenField($tool,"tool");
		echo frmHiddenField(1,"send_alert");
		echo frmButtonConfirmHelp("Send Alert","send","Are you sure you want to send these alerts now?", "Click here to send these alerts");
		echo endFormTable2();
		echo "<br/>";
	} else {
		echo "<strong>These alerts are disabled in the modules configuration file.</strong><p> These are the alerts that would be sent if it was enabled.</p>";
	}
	if (isset($at_result[0])) {
		echo "<table  id=\"result_list\" class=\"acc_list\">";
		echo "<thead>";
		echo "<tr>";
		//first get array keys of one array within the array and use these as th
		$cnt = 1;
		$footer = "";
		$col_cnt = (isset($at_result[0]) ? count($at_result[0]) : 0);
		foreach ($at_result[0] as $k => $v) {
			$th_class = "";
			if ($cnt == 1) $th_class = "start";
			if ($cnt == $col_cnt) $th_class = "end";
			$cnt ++;
			echo "<th class=\"$th_class ".str_replace(" ", "-", $k)." \">$k</th>";
			$footer .= "<td class=\"$th_class\"></td>";
		}
		echo "</tr>";
		echo "</thead>";
		echo "<tbody>";
		
		foreach ($at_result as $row) {
			echo "<tr>";
			$cnt = 1;
			foreach ($row as $key => $val) {
				$td_class = "";
				if ($cnt == 1) $td_class = "start";
				if ($cnt == $col_cnt) $td_class = "end";
				$cnt ++;
				echo "<td class=\"$td_class\">";
				if ($key == "userid") {
					echo "<a href=\"$rootUrl/handsam_core/system_users.php?userid=$val\">$val</a>";
				} else {
					echo $val;
				}
				echo "</td>";
			}		
			echo "</tr>";
		}
		echo "</tbody>";
		echo "<tfoot>";
		echo "<tr>";
		echo $footer;
		echo "</tr>";
		echo "</tfoot>";
		echo "</table>";
	}
}
?>