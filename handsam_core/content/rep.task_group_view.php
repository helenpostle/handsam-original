<?php
/***
** View reports with an account per row
** 
** 
***/
echo "<ul class=\"params\">$subtitle</ul>";
?>
<table class="acc_list" id="acc_list">
	<thead>
		<tr>
			<th class="account_name start nolink">Task Category</th>
			<th class="nolink">% completed</th>
			<th class="nolink">number completed</th>
			<th class="nolink">total number</th>
			<th class="nolink">% allocated</th>
			<th class="end nolink"></th>
		</tr>
	</thead>
	<tbody>

<?php
		$this_report = array_pop($dashboard->reports);
				//var_dump($this_report);
				//die();
		if ($this_report && count($this_report->result) > 0) {
			foreach ($this_report->result as $report_item) {
				echo "<tr>";
				echo "<td class=\"start account_name \"><a href=\"\">".$report_item['category_name']."</a></td>";
				echo "<td class=\"percent\"><a href=\"#\">".$report_item['completed']."</a></td>";
				echo "<td class=\"number \"><a href=\"#\">".$report_item['num_completed']."</a></td>";
				echo "<td class=\"number \"><a href=\"#\">".$report_item['total']."</a></td>";
				echo "<td class=\"percent \"><a href=\"#\">".$report_item['allocated']."</a></td>";
				echo "<td class=\"end\"></td>";
				echo "</tr>";
			}
		}
?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td colspan="4"></td>
 			<td class="end"></td>
 		</tr>
  	</tfoot>

</table>