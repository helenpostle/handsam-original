<?php
echo startSimpleForm();	
echo frmHiddenField($module,"mod");
echo frmHiddenField($tool,"tool");

foreach ($folders as $key => $val) {
	echo "<h3>Directory: $key</h3>";
	if (array_key_exists($key, $extra_files) && count($extra_files[$key]) > 0) {
		echo "<ul>";
		foreach ($extra_files[$key] as $afile) {
			echo "<li>$afile</li>";
		}
		echo "</ul>";
	} else {
		echo "<p>No Extra Files</p>";
	}
}

echo frmButtonConfirmHelp("Delete Files","delete","Are you sure you want to delete these files?", "Click here to delete these files");
echo endFormTable2();
echo "<br/>";


?>