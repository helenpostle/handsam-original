<?php 
/**
 * Shows a form for updating a user
 */

if (isAllowed("editAccount")) {

	echo startFormTable($_SERVER["PHP_SELF"], "", "", USERS_FORM_TITLE, USERS_FORM_INFO);
	echo frmHiddenField($userid,"userid");
	
	if (isset($return)) echo frmHiddenField($return, "return");
	
	//echo formNotes(USERS_FORM_TITLE, USERS_FORM_INFO);
	$state = array("ACTIVE" => "ACTIVE", "INACTIVE" => "INACTIVE");

	if ($userid == 0) {
	} else {
		echo frmTextField($rowUser,"username",100,"Email",true);		
	}
        
        if($core_conf['user_payroll'])
            echo frmTextField($rowUser, 'payroll', 30, 'Payroll Number', true);
        
	echo frmTextField($rowUser,"firstname",50,"First Name",true);
	echo frmTextField($rowUser,"lastname",50,"Last Name",true);
	
	if ($clsUS->userid != $rowUser['userid']) {
		echo frmSelectArray($rowUser, "state" , $state, "State" , true)	;
	}
	
	
	
	
	if (isset($accountid)) echo frmHiddenField($accountid,"accountid");
	
	if ($rowUser['accountid'] != 1) {	
		$userTypes = array("None" => "No Access", "Administrator" => "Administrator", "User" => "User");
		if (in_array("tasklist", $installed_modules)) {
			if($userid == 0) {
				$qryTLOpt = "SELECT distinct t.tasklistid ,t.tasklistname, 'None' AS usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid WHERE a.accountid = $accountid";
			} else {
				$qryTLOpt = "SELECT distinct   t.tasklistid ,t.tasklistname, ut.usertype FROM tasklist t LEFT JOIN accountlicence a ON t.tasklistid = a.tasklistid LEFT JOIN usr u ON u.accountid = a.accountid LEFT OUTER JOIN usr_usrtype ut ON u.userid = ut.userid AND t.tasklistid = ut.tasklistid WHERE u.userid = $userid";
			}
			$rsTLOpt = getRS($con,$qryTLOpt);
			
			while ($rowTLOpt = getRow($rsTLOpt)) {
				//echo $rowTLOpt["usertype"];
				echo frmSelectArrayValue($rowTLOpt["usertype"], "usertype_".$rowTLOpt["tasklistid"] , $userTypes, $rowTLOpt["tasklistname"]. " List"  , true);
			}
		}
	}
	
	//next can select a usertype for each app  from tasks app
	//first get apps for this account
	
	$qryAcApp = "Select distinct a.appid, a.appname from accountlicence l left join apps a on l.appid = a.appid where l.accountid = $accountid and a.appid != 1 and l.state = 'ACTIVE' and l.end_date > NOW() and l.start_date < NOW()";
	//echo $qryAcApp;
	$rsAcApp = getRS($con, $qryAcApp);
	
	while ($rowAcApp = getRow($rsAcApp)) {
		if ($rowUser['accountid'] != 1) {
			if($userid == 0) {
				$qryAOpt = "SELECT distinct a.appid ,a.appname, 'None' AS usertype FROM apps a LEFT JOIN accountlicence l ON a.appid = l.appid WHERE l.accountid = $accountid and a.appid = ".$rowAcApp['appid'];
			} else {
				$qryAOpt = "SELECT t.usrtype as usertype FROM usrtype t left join usr_usrtype ut on t.usrtype = ut.usertype left join usr u on u.userid = ut.userid  WHERE u.userid = $userid AND t.appid = ".$rowAcApp['appid'];
			}
			$rsAOpt = getRS($con,$qryAOpt);
			if (getResultRows($rsAOpt)) {
				$rowAOpt = getRow($rsAOpt);
				$qryUserType = "select usrtype from usrtype where appid = ".$rowAcApp['appid'];
				$rsUT = getRS($con, $qryUserType);
				$appUserTypes = array("None" => "No Access");
				while ($rowAUT = getRow($rsUT))
        {
            if($rowAcApp['appid']==2)
            {
                //lotc usertype config file check
                if($rowAUT['usrtype']=='LOTC Governor' && $lotc_conf['governor_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if($rowAUT['usrtype']=='LOTC Finance' && $lotc_conf['finance_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if($rowAUT['usrtype']=='LOTC Principal' && $lotc_conf['principal_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if($rowAUT['usrtype']=='LOTC Deputy Principal' && $lotc_conf['deputy_principal_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if($rowAUT['usrtype']=='evc - no alerts' && $lotc_conf['evc_no_alerts_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if ($rowAUT['usrtype']=='evc' || $rowAUT['usrtype']=='visit planner' )
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
            }
            else
            {
                $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
            }
        }
			} else {
				$qryAOpt = "SELECT distinct a.appid ,a.appname, 'None' AS usertype FROM apps a LEFT JOIN accountlicence l ON a.appid = l.appid WHERE l.accountid = $accountid and a.appid = ".$rowAcApp['appid'];
				$rsAOpt = getRS($con,$qryAOpt);
				$rowAOpt = getRow($rsAOpt);
				$qryUserType = "select usrtype from usrtype where appid = ".$rowAcApp['appid'];
				$rsUT = getRS($con, $qryUserType);
				$appUserTypes = array("None" => "No Access");
				while ($rowAUT = getRow($rsUT))
        {
            if($rowAcApp['appid']==2)
            {
                //lotc usertype config file check
                if($rowAUT['usrtype']=='LOTC Governor' && $lotc_conf['governor_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if($rowAUT['usrtype']=='LOTC Finance' && $lotc_conf['finance_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if($rowAUT['usrtype']=='LOTC Principal' && $lotc_conf['principal_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if($rowAUT['usrtype']=='LOTC Deputy Principal' && $lotc_conf['deputy_principal_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if($rowAUT['usrtype']=='evc - no alerts' && $lotc_conf['evc_no_alerts_usr']===true) 
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
                else if ($rowAUT['usrtype']=='evc' || $rowAUT['usrtype']=='visit planner' )
                {
                  $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
                }
            }
            else
            {
                $appUserTypes[$rowAUT['usrtype']] = ucfirst($rowAUT['usrtype']);
            }
        }
			}
			echo frmSelectArrayValue($rowAOpt["usertype"], "app_usertype_".$rowAcApp["appid"] , $appUserTypes, $rowAcApp["appname"]. " App"  , true);
				
		}
	}
	
	// add a job role multi select
	if($core_conf['jobroles'] === true)
	{
		// Get the preselected values in a recordset
		$sql = "SELECT jobrole_id AS role_id FROM core_usr_jobrole WHERE usr_id = '$userid';";
		$rs = getRS($con, $sql);
		// field to compare
		$field = 'role_id';
		// query to get all possible job role values
		$qry = "SELECT role_id AS a, role_title AS b FROM core_jobrole WHERE state <> 'DELETED' ORDER BY created ASC;";
		$title = 'Select Job Role(s)';
		echo frmMultiSelectQuery($rs, $field, $qry, $con, $title, FALSE);
	}

	if ($userid != 0 && isAllowed("editUsers")) {
		echo frmShowAudit($rowUser,$con);	
	}
	
	echo frmRowNoInput($rowUser["logincnt"], "Log in count");
	
	if ($rowUser["lastlogin"] != NULL) {
		echo frmRowNoInput(displayDateTime($rowUser["lastlogin"]), "Last logged in");
	}
	
	
	echo frmButtonHelp("Save","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($userid != 0 && (!isset($self) || !$self))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this user?", "Click here to delete this user");
	
	echo endFormTable();
	
}
