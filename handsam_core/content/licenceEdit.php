<?php 
/**
 * Shows a form for editing a licence
 */
 

if (isset($licenceid)) { 
	$rslicence = getRS($con,"select * from accountlicence where licenceid = $licenceid");
	$rowlicence = getRow($rslicence);
	$licenceDuration =  $rowlicence['end_date'] - $rowlicence['start_date'];
	
	//get module name from appid:
	$rsmod = getRS($con,"select * from apps where appid = $appid");
	$rowmod = getRow($rsmod);
	$mod_name = $rowmod['appname'];
	echo startFormTable($_SERVER["PHP_SELF"], "", "", "Module: $mod_name", "");
	//echo frmLegend($frmTitle);
	//echo formNotes(LICENCE_FORM_TITLE, LICENCE_FORM_INFO);
	echo frmHiddenField($accountid,"accountid");
	echo frmHiddenField($licenceid,"licenceid");
	echo frmHiddenField($tasklistid,"tasklistid");
	echo frmHiddenField($appid,"appid");
	
	echo frmTextAreaNonEdit($rowlicence,'state' ,"Licence State");
	if ($licenceid == 0) {
		//$billing_date = 0;
		echo frmDateFieldNonDb ($start_date, "start_date", "Technical Start Date", true);
		echo frmDateFieldNonDb ($billing_date, "billing_date", "Initial/Last Billing Date", false);
	} else {
		echo frmDateField($rowlicence,"start_date","Technical Start date",true);
	//new biling date field
		if ($rowlicence['billing_date'] == "" || $rowlicence['billing_date'] == null ) {
			$billing_date = time();
			$billing_date = 0;
		} else {
			$billing_date = strtotime($rowlicence['billing_date']);
		}
		echo frmDateFieldNonDb ($billing_date, "billing_date", "Initial/Last Billing Date", false);	
	}

	
	//new contract length field
	if ($licenceid == 0) { 
		echo frmTextFieldNonDb("contract_length",20,"Remaining Contract Length (years)",false, $contract_len);
	} else {
		echo frmTextField($rowlicence,"contract_length",20,"Remaining Contract Length (years)",false);
	}
	
	if ($accountid != 0 && isAllowed("editAccount")) {
		echo frmShowAudit($rowlicence,$con);	
	}
	
	if (file_exists("$rootPath/modules/{$rowmod['folder']}/shared/licence_conf.php")) {
		include("$rootPath/modules/{$rowmod['folder']}/shared/licence_conf.php");
	}	
	
	if ($rowlicence['state'] == 'NEW' && $rowlicence['appid'] > 1) {
		echo frmButtonHelp("Activate","activate", "Click here to activate licence");
	} else if($rowlicence['state'] == 'NEW' && $rowlicence['appid'] == 1) {
		if ($cnt_licence > 1) {
			echo frmButtonConfirmHelp("Supply With Previous Data","submit_users","This will copy over all assigned users, account administrator notes, and account administrator resources from the previous licence. Are you sure you want to do this now? You cannot edit the holiday or licence periods once this is done", "Click here to supply tasks");
		}

		echo frmButtonConfirmHelp("Supply tasks","supply","Are you sure you want to do this now? You cannot edit the holiday or licence periods once this is done", "Click here to supply tasks");
	}
	echo frmButtonHelp("Save Changes","save", "Click here to save changes");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	if ($licenceid != 0 && isAllowed("editAccount"))	echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this licence?", "Click here to delete this licence");
	
	echo endFormTable();
}
?>