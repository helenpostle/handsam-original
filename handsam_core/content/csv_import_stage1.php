<?php
if ($rowFile['account_arr'] != "") {
  $db_acc_arr = json_decode($rowFile['account_arr'], true);
}

$csv = new parseCSV();
$csv->auto("$to_file_path/".$rowFile['filename']);
#populate array of accountnames
foreach ($csv->data as $key => $row){
  if (!in_array($row[$accountname_fld], $accounts_arr)){
    $accounts_arr[] = $row[$accountname_fld];
  }
}   
echo startSimpleForm($_SERVER["PHP_SELF"], "", "csvform","Select accounts", "Marry up the CSV account names with the database account names");
echo frmHiddenField($fileid,"fileid");
  echo frmHiddenField($tool,"tool");
?>
<table class="acc_list" style='select option[val="0"]: color: red;'>
  <thead>
        <tr><th class="start">CSV Account Name</th><th class="end">Database Match</th></tr>
  </thead>
  <tbody class="noStrip">
          <?php
          $i=0;
          foreach($accounts_arr as $account){
            $account = preg_replace('/[^(\x20-\x7F)]*/','', $account);
            echo "<tr><td class=\"start\">$account<input type=\"hidden\" name=\"csv_name[]\" value=\"$account\"/></td>";
            
            //get closest match from db
            $qry = "select accountname from account where accountname like '%".mysql_real_escape_string($account)."%' limit 0,1";
            $rs = getRS($con,$qry);
            $acRow = getRow($rs);
            
            //now get select array for all accounts
            $qry = "select accountid, state, accountname from account order by accountname asc";
            $rs = getRS($con,$qry);
            
            ?>
            <td class="end"><select  name="account_name[]">
            <option value="0">ACCOUNT MISSING</span></option>
            <?php
            while ($seRow = getRow($rs)) {
              echo "<option value=\"".$seRow['accountid']."\"";
              if (isset($_POST['account_name'][$i]) && $_POST['account_name'][$i] == $seRow['accountname']) {
                echo " selected ";
              } else {
                if (isset($db_acc_arr) && isset($db_acc_arr[$account]) && $db_acc_arr[$account]==$seRow['accountid'] ) echo " selected ";
                if ($acRow['accountname'] == $seRow['accountname']) echo " selected ";
              }
              echo ">";
              echo $seRow['accountname'];
              echo "</option>";
              
            }
            echo "</select></td></tr>";
            $i++;
          }
          ?>
    </tbody>
        <tfoot>
			<tr>
				<td class="start"></td>
				<td class="end"></td>
	  		</tr>
	  	</tfoot>
      </table>
<?php
  echo frmButtonHelp("save &amp; Continue","import_stage2", "Click here to continue import with these settings");
  echo frmButtonHelp("Cancel","cancel", "Click here to cancel upload");
  echo endFormTable();
?>