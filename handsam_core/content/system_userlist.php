<?php
/***
** Shows the list of users in the system
** @todo make it so clicking on colunm headings reorders list? or add dropdown to view only a certain usertype? or alphabetical listing?
** i.e. make it easier for when there are loads of users
***/


//select filter by user type
//first get appids of installed apps

$app_names = "'".implode("','", $installed_modules)."'";
$appQry = "select appid from apps where folder in ($app_names)";

$qry = "select * from usrtype where appid in ($appQry) and usrtype != 'SuperAdmin'";

$rsUt = getRS($con, $qry);

echo "<div class=\"filter\">";
echo "<label for=\"usertype_filter\">User Type: </label>";
echo "<select id=\"usertype_filter\">";
echo "<option value=\"All\"";
if ($usertype == 0) echo " selected ";
echo ">All User Types</option>";
while ($rowList = getRow($rsUt)) {
	echo "<option value=\"{$rowList['usrtype']}\"";
	if ($usertype == $rowList['usrtype']) echo " selected ";
	echo ">{$rowList['usrtype']}</option>";
}
echo "</select>";
echo "</div>";


// filter by account name
echo "<div class=\"filter\">";
echo "<label for=\"username_filter\">Username: </label>";
echo "<input name=\"username_filter\" id=\"username_filter\" value=\"$username_filter\" type=\"text\"/>";
echo "<img id=\"username_filter_go\" src=\"$rootUrl/images/accept.png\"/>";
echo "<img id=\"reset_username\" src=\"$rootUrl/images/delete.png\"/>";
echo "</div>";

// filter by first name
echo "<div class=\"filter\">";
echo "<label for=\"firstname_filter\">First name: </label>";
echo "<input name=\"firstname_filter\" id=\"firstname_filter\" value=\"$firstname_filter\" type=\"text\"/>";
echo "<img id=\"firstname_filter_go\" src=\"$rootUrl/images/accept.png\"/>";
echo "<img id=\"reset_firstname\" src=\"$rootUrl/images/delete.png\"/>";
echo "</div>";

// filter by last name
echo "<div class=\"filter\">";
echo "<label for=\"lastname_filter\">Last name: </label>";
echo "<input name=\"lastname_filter\" id=\"lastname_filter\" value=\"$lastname_filter\" type=\"text\"/>";
echo "<img id=\"lastname_filter_go\" src=\"$rootUrl/images/accept.png\"/>";
echo "<img id=\"reset_lastname\" src=\"$rootUrl/images/delete.png\"/>";
echo "</div>";


if (isAllowed("editAccount")) {
	$qry = "SELECT usr.*, usr_usrtype.usertype FROM usr left join account on usr.accountid = account.accountid left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.state != 'DELETED' and usr.accountid != 1 $usertype_qry $username_filter_qry $firstname_filter_qry $lastname_filter_qry $cluster_qry order by usr_usrtype.usertype, lastname";
//} else {
	//$qry = "SELECT usr.*, usr_usrtype.usertype FROM usr left join account on usr.accountid = account.accountid left join usr_usrtype on usr.userid = usr_usrtype.userid where account.accountid = ".$clsUS->accountid." and usr.state != 'DELETED'  order by usr_usrtype.usertype, lastname";
//}

	
	$result = getRS($con, $qry);
	
			
	?>
	<table  id="user_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start username nolink ">Username</th>
				<th class="">First Name</th>
				<th class="">Last Name</th>
				<th class="">Email Alerts</th>
				<th class="end">State</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	$last_usr = array();
	while ($row = getRow($result)) {
			if (!in_array($row['userid'],$last_usr)) {
			$last_usr[] = $row['userid'];
			$usertype = displayText($row["usertype"]);
			if ($row["late_task_email_alert"] == 1) {
				$checked = " checked ";
			} else {
				$checked = "";
			}
		?>
				<tr>
					<td class="start"><a href="system_users.php?userid=<?php echo $row["userid"] ?>"><?php echo $row["username"] ?></a></td>
					<td class=""><?php echo displayText($row["firstname"]) ?></td>
					<td class=""><?php echo displayText($row["lastname"]) ?></td>
					<td class="state"><input class="table_check_box" type="checkbox" value="<?php echo $row["userid"]; ?>" name="email_alert[]" <?php echo $checked; ?>/></td>
					<td class="end"><?php echo displayText($row["state"]) ?></td>
				</tr>
		<?php 
		}
	} ?>
		</tbody>
		<tfoot>
			<tr>
				<td class="start"></td>
				<td colspan="3"></td>
				<td class="end"></td>
	  		</tr>
	  	</tfoot>
	
	</table>
	<?php
}
?>