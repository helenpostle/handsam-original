<?php
/***
** View reports with an account per row
** 
** 
***/
echo "<ul class=\"params\"></ul>";
echo "<div id=\"dashboard\" class=\"clearfix\">";
echo "<ul class=\"tabs\">";
foreach($tabs as $tab => $tab_title) {
    echo "<li><a href=\"#tab_$tab\">$tab_title</a></li>";
}
echo "</ul>";
echo "My test page";

/*$cluster_qry="";
  if(isset($clsUS->account_clusters) && count($clsUS->account_clusters)>0 && isset($core_conf['account_cluster_SA_access']) && $core_conf['account_cluster_SA_access'])
  {
    $cluster_qry = " and account.cluster_id in (".implode(',',$clsUS->account_clusters).") ";
  }
  
  if(isset($params['school_cluster']) && $params['school_cluster']>0)
  {
    $cluster_qry = " and account.cluster_id = {$params['school_cluster']} ";
  }
//var_dump($core_conf);
// echo "The cluster qry is ".$cluster_qry;
$include_file_sql = "SELECT accountname, school_status, acc_accident.accidentid as id, acc_accident.state, acc_accident.accountid as accountid, (CASE WHEN acc_accident.injured_usertype=2 THEN CONCAT(ua.lastname, ', ', ua.firstname) ELSE CONCAT(u.firstname, ', ',u.lastname) END) AS injured_party, status, acc_accident_site.site_name, accident_type, (CASE when riddor = 'NON-RIDDOR'  THEN 'No' ELSE 'Yes' END) as riddor, (CASE when ambulance = 0  THEN 'No' ELSE 'Yes' END) as ambulance, (CASE when party_injured = 0  THEN 'No' ELSE 'Yes' END) as injured, (CASE when first_aid = 0  THEN 'No' ELSE 'Yes' END) as first_aid, days_off, cost ";

$include_file_sql .= "FROM acc_accident LEFT JOIN acc_injured_party ON acc_accident.injuredid = acc_injured_party.injuredid
						LEFT JOIN usr u ON acc_accident.userid = u.userid
						LEFT JOIN usr_assoc AS ua ON acc_accident.assoc_userid = ua.assoc_userid 
						LEFT JOIN acc_accident_site ON acc_accident.siteid = acc_accident_site.siteid
						LEFT JOIN account ON acc_accident.accountid = account.accountid ";

$include_file_sql .= "WHERE  CAST(accident_time AS DATE) >= '2017-01-01' AND CAST(accident_time AS DATE) <= '2017-11-01' $school_type_qry $school_pupils_qry $riddor_sql $acc_sql $manager_qry $school_cluster_qry $cluster_qry";

$include_file_sql .= " ORDER BY \"Injured Party\"";

echo $include_file_sql;*/

foreach($tabs as $tab => $tab_title) {
    echo "<div id=\"tab_$tab\">";
    $table_cnt = 0;
    
    foreach ($rows[$tab] as $table_key => $table) {
        $table_cnt ++;
        $sorting_class = "table_sort";
        if ($core_conf['dashboard_group_select'] && $tab == 'tasklist' && $table_cnt == 1) $sorting_class = "";
        ?>
            
        <table class="<?php echo $sorting_class; ?> acc_list table_<?php echo $table_cnt;?>" id="">
            <thead>
                <tr>
                    <?php
                    $cnt = 0;
                    foreach ($cols[$tab][$table_key] as $col_key => $col_name) {
                        $cnt ++;
                        $class = "nolink";
                        if ($cnt == 1) $class = " start nolink";
                        if ($cnt == count($cols[$tab][$table_key])) $class = "end nolink";
                        echo "<th class=\"$col_key $class\">$col_name</th>";

                    }
                    ?>

                </tr>
            </thead>
            <tbody>
                <?php
                if (isset($rows[$tab][$table_key])) {
                    foreach ($rows[$tab][$table_key] as $row_key => $row_name) {
                        echo "<tr class=\"row_".str_replace(" ", "_", $row_key)."\">";
                        //echo "<td class=\"start account_name \">$row_name</td>";
                        $cnt = 0;
                        foreach ($cols[$tab][$table_key] as $col_key => $col_name) {
                            $cnt ++;
                            $td_class = "";
                        //for ($i=1; $i<$cnt; $i++) {
                            if ($cnt == 1) { 
                                echo "<td class=\"start account_name col_$col_key\">$row_name</td>";
                            } else {
                                if ($cnt == count($cols[$tab][$table_key])) $td_class = "end ";
                                echo "<td class=\"$td_class col_$col_key\"><a href=\"dashboard.php#\"></a></td>";
                            }
                        //}
                        }
                        echo "</tr>";
                    }
                }
                ?>
            </tbody>
            <tfoot>
                <tr>
                    <td class="start"></td>
                    <?php
                    if ($cnt > 2) echo "<td colspan=\"".($cnt-2)."\"></td>";
                    ?>
                    <td class="end"></td>
                </tr>
            </tfoot>

        </table>   
        <?php
    }
    echo "</div>";
}
echo "</div>";
?>