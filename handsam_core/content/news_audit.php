<?php
if ((isAllowed("viewNewsAuditSA") && $core_conf['unread_news_audit_SA']) || (isAllowed("viewNewsAudit") && $core_conf['unread_news_audit_account_admin']))
{		
	?>
	<table  id="user_list" class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
				<th class="start">Account</th>
				<th class="">First Name</th>
				<th class="">Last Name</th>
				<th class="">Message</th>
				<th class="">Published</th>
				<th class="end">Read</th>
			</tr>
		</thead>
		<tbody>
	
	<?php
	foreach ($audit->users as $u=>$val)
  {
    foreach ($val[$status] as $row)
    {
      if ($s==1 && isset($row["the_date"]))
      {
        //only display unread
        continue;
      }
      ?>
          <tr>
            <td class="start"><?php echo displayText($audit->users[$u]["accountname"]) ?></td>
            <td class=""><a class="user_link" title="<?php echo $audit->users[$u]["username"] ?>" href="users.php?userid=<?php echo $u ?>&amp;return=users"><?php echo displayText($audit->users[$u]["firstname"]) ?></a></td>
            <td class=""><a class="user_link" title="<?php echo $audit->users[$u]["username"] ?>" href="users.php?userid=<?php echo $u ?>&amp;return=users"><?php echo displayText($audit->users[$u]["lastname"]) ?></a></td>
            <td class="state"><a rel='<?php echo $row["contentid"]?>' class="news_link" title="View Message" href="../news.php?id=<?php echo $row["contentid"]?>&amp;return=audit"><?php echo showAbstract($row["title"],50) ?></a></td>
            <td class="state"><a rel='<?php echo $row["contentid"]?>' class="news_link" title="View Message" href="../news.php?id=<?php echo $row["contentid"]?>&amp;return=audit"><?php echo displayDate($row["created"],50) ?></a></td>
            <td class="end"><a rel='<?php echo $row["contentid"]?>' class="news_link" title="View Message" href="../news.php?id=<?php echo $row["contentid"]?>&amp;return=audit"><?php 
            if (isset($row["the_date"]))
            {
              echo displayDate($row["the_date"]);
            }
            else
            {
              echo "-";
            }
            
            ?></a></td>
          </tr>
      <?php 
    }
	} 
  ?>
		</tbody>
		<tfoot>
			<tr>
				<td class="start"></td>
				<td colspan="4"></td>
				<td class="end"></td>
	  		</tr>
	  	</tfoot>
	
	</table>
	<?php
	//echo addCsrfField();
}
?>

