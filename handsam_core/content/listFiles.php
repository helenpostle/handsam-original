<?php 
/**
 * list uploaded documents on server
 */
$categoryCol = false;
$qs = "?filetype=".$filetype;
$ifSuperAdminAccountId = "";
if (isAllowed("uploadHandsamDocs")) {
	if ($filetype == "handsamDoc") {
		$categoryCol = true;
		
		$qry = "SELECT files.mid, files.title, files.filename, files.fileid, files.filecategory, count(task_files.taskid) as cnt FROM files left join task_files on files.fileid = task_files.fileid WHERE files.filetype = 'handsamDoc' and files.accountid = $accountid and files.filecategory in('".implode("','",array_flip($docCategory))."') group by files.mid, files.title, files.filename, files.fileid, files.filecategory ORDER BY files.title,files.filename desc";
	} else if ($filetype == "adminDoc") {
		//$qs = "?filetype=".$filetype."&accountid=".$accountid;
		$ifSuperAdminAccountId = "&accountid=".$accountid;
		$qry = "SELECT files.mid, files.title, files.filename, files.fileid, files.filecategory, count(task_files.accounttaskid) as cnt FROM files left join task_files on files.fileid = task_files.fileid WHERE files.filetype = 'adminDoc' and files.accountid = $accountid group by files.mid, files.title, files.filename, files.fileid, files.filecategory ORDER BY files.title,files.filename desc";
	} else {
		//$qs = "?filetype=".$filetype."&accountid=".$accountid;
		$ifSuperAdminAccountId = "&accountid=".$accountid;
		$qry = "SELECT files.mid, files.title, files.filename, files.fileid, files.filecategory, count(task_files.accounttaskid) as cnt FROM files left join task_files on files.fileid = task_files.fileid WHERE files.filetype = 'userDoc' and files.accountid = $accountid and userid = $userid group by files.mid, files.title, files.filename, files.fileid, files.filecategory ORDER BY files.title,files.filename desc";
	}
		
		
	
} else if (isAllowed("uploadAdminDocs")) {
	if ($filetype == "adminDoc") {
		$qry = "SELECT files.mid, files.title, files.filename, files.fileid, files.filecategory, count(task_files.accounttaskid) as cnt FROM files left join task_files on files.fileid = task_files.fileid WHERE files.filetype = 'adminDoc' and files.accountid = $accountid group by files.mid, files.title, files.filename, files.fileid, files.filecategory ORDER BY files.title,files.filename desc";
	} else {
		$qry = "SELECT files.mid, files.title, files.filename, files.fileid, files.filecategory, count(task_files.accounttaskid) as cnt FROM files left join task_files on files.fileid = task_files.fileid WHERE files.filetype = 'userDoc' and files.accountid = $accountid and userid = $userid group by files.mid, files.title, files.filename, files.fileid, files.filecategory ORDER BY files.title,files.filename desc";
	}

	
	
} else if (isAllowed("uploadUserDocs")) {
		$qry = "SELECT files.mid, files.title, files.filename, files.fileid, files.filecategory, count(task_files.accounttaskid) as cnt FROM files left join task_files on files.fileid = task_files.fileid WHERE files.filetype = 'userDoc' and files.accountid = $accountid and userid = $userid group by files.mid, files.title, files.filename, files.fileid, files.filecategory ORDER BY files.title,files.filename desc";
}

	$result = getRS($con, $qry);
	if (getResultRows($result) > 0) {

		echo "<p class=\"info\">Click on the file name below to edit its details or view the file. </p><p>Click on the attach button to attach the file to the current ".$edittype.".</p>";
		if ($edittype == "task") {
			echo "<p class=\"\"><strong> You must then save all task changes in the main window! </strong></p>";	
		}?>
		
		<table class="acc_list">
		<caption class="hidden"><?php echo $title;?></caption>
		<thead>
			<tr>
		    	<th class="nolink start">Document Title</th>
		    	<th class="nolink">Filename</th>
		    	<th class="nolink lid">Used</th>
		    	<?php
		    	if ($categoryCol) {
			    	echo "<th class=\"nolink cat\">Category</th>";
		    	}
		    	?>
				<th class="end centre"></th>
			</tr>
		</thead>
		<tbody class="noStrip">	
		<?php
		while ($row = getRow($result)) {
			if ($row['filecategory'] == "") {
				$dcat = "no category";
			} else {
        $dcat = "";
        if (isset($docCategory[$row['filecategory']]))
        {
          $dcat = textSummary($docCategory[$row['filecategory']], 14);
        }
			}
            if ($row['mid'] > 0) {
                $link_title = "View this Handsam document";
            } else {
                $link_title = "Edit and replace this document";
            }
			echo "<tr>\n";
			echo "<td class=\"start\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"browser.php?fileid=".$row['fileid']."&filetype=".$filetype.$ifSuperAdminAccountId.$contentIdLink."\">".textSummary($row['title'], 40)."</a></td>\n";
			echo "<td class=\"\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"browser.php?fileid=".$row['fileid']."&filetype=".$filetype.$ifSuperAdminAccountId.$contentIdLink."\">".textSummary($row['filename'], 30)."</a></td>\n";
			echo "<td class=\"\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"browser.php?fileid=".$row['fileid']."&filetype=".$filetype.$ifSuperAdminAccountId.$contentIdLink."\">".$row['cnt']."</a></td>\n";
			if ($categoryCol) {
				echo "<td class=\"\"><a class=\"rowRollOver\" title=\"$link_title\" href=\"browser.php?fileid=".$row['fileid']."&filetype=".$filetype.$ifSuperAdminAccountId.$contentIdLink."\">".$dcat."</a></td>\n";
			}
			if ($edittype == "task") {
				//adds file to task edit form
				echo "<td class=\"end\"><a onclick=\"attachDoc(".$row['fileid'].", '".addslashes($row['title'])."', 'adminDoc', 'doc');\" class=\"tableLink btn attachDoc\" title=\"Attach this File to the Task\" href=\"#\">Attach</a></td>\n";
			} else {
				//ajax adds file page
				echo "<td class=\"end\"><a onclick=\"attachItemToPage('".$rootUrl."', ".$row['fileid'].", 'file', ".$contentid.");\" class=\"tableLink\" title=\"\" href=\"#\">Attach</a></td>\n";
			}
			echo "</tr>\n";
		} ?>

		</tbody>
		<tfoot>
			<tr>
				<td class="start"></td>
				<td class=""></td>
				<td class=""></td>
		    	<?php
		    	if ($categoryCol) {
					echo "<td></td>";
		    	}
		    	?>
				<td class="end"></td>
	  		</tr>
	  	</tfoot>
		
		</table>		
		
		<?php	
		echo addCsrfField();
	
	} else {
		echo "<p>There are no documents available. <a href=\"browser.php".$qs."\">Upload a document here.</a>";
	}
?>	