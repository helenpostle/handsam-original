<?php
/***
** View reports with an account per row
** 
** 
***/
echo "<ul class=\"params\">$subtitle</ul>";
?>
<table class="acc_list" id="acc_list">
	<thead>
		<tr>
			<th class="start nolink">ID</th>
			<th class="width5 nolink">Injured Party</th>
			<th class="nolink">Status</th>
			<th class="width5 nolink">Site Name</th>
			<th class="nolink">Type</th>
			<th class="nolink">State</th>
			<th class="nolink">Injured</th>
			<?php
			/*
			<th class="width1 nolink">First Aid</th>
			*/
			?>
			<th class="nolink">Ambulance</th>
			<th class="nolink">Days Off</th>
			<th class="end nolink">Cost</th>
		</tr>
	</thead>
	<tbody class="report">

<?php
		$this_report = array_pop($dashboard->reports);
				//var_dump($this_report);
				//die();
		if ($this_report && count($this_report->result) > 0) {
			foreach ($this_report->result as $report_item) {
				$row_link = "$rootUrl/modules/accidents/accident.php?accountid=".$report_item['accountid']."&amp;accidentid=".$report_item['id'];
				echo "<tr>";
				echo "<td class=\"start account_name \"><a href=\"$row_link\">".$report_item['id']."</a></td>";
				echo "<td class=\" account_name \"><a href=\"$row_link\">".$report_item['Injured Party']."</a></td>";
				echo "<td class=\"\"><a href=\"$row_link\">".ucfirst(strtolower($report_item['Status']))."</a></td>";
				echo "<td class=\" \"><a href=\"$row_link\">".$report_item['Site Name']."</a></td>";
				echo "<td class=\" \"><a href=\"$row_link\">".ucfirst(strtolower($report_item['Type']))."</a></td>";
				echo "<td class=\" \"><a href=\"$row_link\">".ucfirst(strtolower($report_item['state']))."</a></td>";
				echo "<td class=\" \"><a href=\"$row_link\">".$report_item['Injured']."</a></td>";
				//echo "<td class=\" \"><a href=\"$row_link\">".$report_item['First Aid']."</a></td>";
				echo "<td class=\" \"><a href=\"$row_link\">".$report_item['Ambulance']."</a></td>";
				echo "<td class=\" \"><a href=\"$row_link\">".$report_item['Days Off']."</a></td>";
				echo "<td class=\" end\"><a href=\"$row_link\">".$report_item['Cost']."</a></td>";
				echo "</tr>";
			}
		}
?>
	</tbody>
	<tfoot>
		<tr>
			<td class="start"></td>
			<td colspan="8"></td>
 			<td class="end"></td>
 		</tr>
  	</tfoot>

</table>