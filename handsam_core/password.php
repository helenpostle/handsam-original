<?php
$this_page = "password.php";
/**
* change password form 
* content: password.php
* boxes: none
*/
####make this a secure page
$secure = true;

require("../shared/startPage.php");

$pageFiles->addFunc('email');

$pageFiles->includePhp();


//specify page content
$title = "Change Password";		//the <title> tag and <h1> page title
$content = "password.php"; 
if (buttonClicked("save")) {
	//sanitize inputed data here as not sanitized in changepassword method
	$oldp = getStrFromRequest("oldpass");
	$pas1 = getStrFromRequest("pass1");
	$pas2 = getStrFromRequest("pass2");
	$passwd = new clsUserPassword($homePath, $password_arr);
	$passwd_m = $passwd->changePassword($con,$oldp, $pas1, $pas2, $clsUS->userid, $clsUS->username);
	$messages[] = $passwd_m;
	if ($passwd_m == PASSWORD_CHANGED) {
		$content = "";
	} else {
		$content = "password.php";
	}
}

$pageFiles->addBox("$rootPath/handsam_core/boxes/box.account.password_help.php");
 
 
 
 
//include layout.
include ("layout.php");

?>