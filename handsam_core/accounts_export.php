<?php
/**
* Account editing functionality
* 
* content: accountEdit.php, accountList.php
* boxes:  accountUserList.php, accountlicenceList.php
* 
*/
$this_page = "accounts.php";
/**
 * Include basic stuff
 */
$secure = true;

require("../shared/startPage.php");

####make this a secure page


$pageFiles->includePhp();

#check if have requested a specific account and get other params
$accountid = getIntFromRequest("accountid");
$licenceid = getIntFromRequest("licenceid");

$tasklist = getIntFromRequest("tasklist");
$appid = getIntFromRequest("appid");
$manfilter = getStrFromRequest("manfilter");
$sch_type_filter = getStrFromRequest("sch_type_filter");
$sch_status_filter = getStrFromRequest("sch_status_filter");
$school_pupils_filter = getStrFromRequest("school_pupils_filter");
$authority_filter = getStrFromRequest("authority_filter");

$new_filter_title = "";

//add appid to session
if ($appid != "" || $appid === 0) {
	$_SESSION['appid'] = $appid;
} else {
	$appid = $_SESSION['appid'];
}
//add tasklist to session
if ($tasklist != "" || $tasklist === 0) {
	$_SESSION['tasklist'] = $tasklist;
} else {
	$tasklist = $_SESSION['tasklist']; 
}



//manager filter processing
if ($manfilter != "") {
	$_SESSION['manager_filter'] = $manfilter;
} else {
	$manfilter = $_SESSION['manager_filter']; 
}

if ($manfilter == "Mine") {
	$account_manager_qry = " and account_manager={$clsUS->userid} ";
	$new_filter_title .= $clsUS->firstname." ".$clsUS->lastname."_Accounts-";
} else {
	$account_manager_qry = "";
	$new_filter_title .= "All_Accounts-";
}

//school type filter processing
if ($sch_type_filter != "") {
	$_SESSION['sch_type_filter'] = $sch_type_filter;
} else {
	$sch_type_filter = $_SESSION['sch_type_filter']; 
}

if ($sch_type_filter == "ALL" || $sch_type_filter == "") {
	$sch_type_filter_qry = "";
} else {
	$sch_type_filter_qry = " and school_type='$sch_type_filter' ";
	$new_filter_title .= "{$school_type[$sch_type_filter]}-";
}

//school status filter processing
if ($sch_status_filter != "") {
	$_SESSION['sch_status_filter'] = $sch_status_filter;
} else {
	$sch_status_filter = $_SESSION['sch_status_filter']; 
}

if ($sch_status_filter == "ALL" || $sch_status_filter == "") {
	$sch_status_filter_qry = "";
} else {
	$sch_status_filter_qry = " and school_status='$sch_status_filter' ";
	$new_filter_title .= "{$school_status[$sch_status_filter]}-";
}

//school pupils filter processing
if ($school_pupils_filter != "") {
	$_SESSION['school_pupils_filter'] = $school_pupils_filter;
} else {
	$school_pupils_filter = $_SESSION['school_pupils_filter']; 
}

if ($school_pupils_filter == "ALL" || $school_pupils_filter == "") {
	$sch_pupils_filter_qry = "";
} else {
	$sch_pupils_filter_qry = " and school_pupils='$school_pupils_filter' ";
	$new_filter_title .= "{$school_pupils[$school_pupils_filter]}-";
}

//school pupils filter processing
if ($authority_filter != "") {
	$_SESSION['authority_filter'] = $authority_filter;
} else {
	$authority_filter = $_SESSION['authority_filter']; 
}

if ($authority_filter == "" || $authority_filter == "ALL") {
	$authority_filter_qry = "";
} else {
	$authority_filter_qry = " and authority='$authority_filter' ";
	$new_filter_title .= "$authority_filter-";
}


//acc name filter
if (isset($_SESSION['acc_name'])) {
	$acc_name_filter_qry = " and accountname like '{$_SESSION['acc_name']}%' ";
} else {
	$acc_name_filter_qry = "";
}

//acc postcode filter
if (isset($_SESSION['acc_postcode']) && $_SESSION['acc_postcode'] != "") {
	$acc_postcode_filter_qry = " and postcode like '{$_SESSION['acc_postcode']}%' ";
	$acc_postcode_filter = $_SESSION['acc_postcode'];
} else {
	$acc_postcode_filter_qry = "";
	$acc_postcode_filter = "";
}


//acc dfe filter
if (isset($_SESSION['acc_dfe']) && $_SESSION['acc_dfe'] != "") {
	$acc_dfe_filter_qry = " and school_dfe like '{$_SESSION['acc_dfe']}%' ";
	$acc_dfe_filter = $_SESSION['acc_dfe'];
} else {
	$acc_dfe_filter_qry = "";
	$acc_dfe_filter = "";
}

$new_filter_qry = $account_manager_qry.$sch_type_filter_qry.$sch_status_filter_qry.$sch_pupils_filter_qry.$authority_filter_qry.$acc_name_filter_qry.$acc_postcode_filter_qry.$acc_dfe_filter_qry;

//now inclusde all php files here
$pageFiles->includePhp();

#we can do this if  we have permission to edit accounts.
if (isAllowed("editAccount")) {
		
	#no accountid set - so view accountlist instead?
	$order = "account.accountname";
	
	//bit naughty - this is some tasklist module functionality - so accounts can bee filtered by tasklist.	
	$tasklist_qry = "";
	$appid_ary = "";		
	if (in_array("tasklist", $installed_modules) && $appid == "1") {
		if ($tasklist > 0) {
			$tasklist_qry = " and accountlicence.tasklistid = $tasklist";
		}
	} 
	
	if($appid > 0)  {
		$app_qry = " and accountlicence.appid = $appid ";
	} 
	
	$qry = "SELECT accountlicence.licenceid, accountlicence.state, tasklist.tasklistname, apps.appname, account.accountid,account.accountname, account.address1,account.town, account.postcode, account.state, account.school_type, account.school_status, account.school_pupils, account.authority, accountlicence.end_date as licence_end, accountlicence.start_date as licence_start, accountlicence.billing_date as licence_billing";
	$qry .= " FROM account left join accountlicence on account.accountid = accountlicence.accountid ";
	$qry .=" LEFT JOIN tasklist ON accountlicence.tasklistid = tasklist.tasklistid ";
	$qry .=" LEFT JOIN apps ON accountlicence.appid = apps.appid ";
	$qry .= " where account.accountid != 1 and account.state != 'DELETED' $tasklist_qry $app_qry $new_filter_qry ";

	$result = getRS($con, $qry);
	
	$export = "Account, Address, Module, Tasklist, Licence Start, Billing Date, School Type, School Status, School Pupils, Authority/Region \n ";
	$export = ",,,,,,,,\n";
	while ($row = getRow($result)) {
		$export .= "\"".displayText($row['accountname'])."\",";
		$export .= "\"".displayText($row['address1']).", ".displayText($row['town']).", ".displayText($row['postcode'])." \",";
		$export .= "\"".$row['appname']."\",";
		$export .= "\"".$row['tasklistname']."\",";
		$export .= "\"".$row['licence_start']."\",";
		$export .= "\"".$row['licence_billing']."\",";
		$export .= "\"".$row['school_type']."\",";
		$export .= "\"".$row['school_status']."\",";
		$export .= "\"".$row['school_pupils']."\",";
		$export .= "\"".$row['authority']."\"\n";
	}
	//die($qry);
	//die($export);
	//$new_filter_title = str_replace(" ", "_", $new_filter_title);
		
	// Output to browser with appropriate mime type, you choose ;)
	////header("Content-type: text/x-csv");
	header("Content-type: text/csv");
	////header("Content-type: application/csv");
	header("Content-Disposition: attachment; filename=accounts_export.csv");
	echo $export;
	exit;

} else {
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);
}
?>