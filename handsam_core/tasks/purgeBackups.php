<?php
/** This task removes old backups from the backup folder and from S3 */

doLog("TITLE","PURGE BACKUPS:");
//$messages[] = "<br><b>PURGE BACKUPS:</b>";
	if ($backupDaysToKeep > 0) {
	$ms = $backupDaysToKeep * 24 * 60 * 60;
	$delDate = time() - $ms;
	doLog("INFO","Deleting Files From Before ".date("d-M-Y",$delDate));
	//$messages[] = "Deleting Files From Before ".date("d-M-Y",$delDate);
	
	if (is_dir($backupPath)) {
	    if ($dh = opendir($backupPath)) {
	        while (($file = readdir($dh)) !== false) {
		        if ($file != "." && $file != "..") {
			        list($y, $m, $d, $h, $min, $s, $ext) = sscanf($file, $dbname."_%d-%d-%d-%d-%d-%d%s");
			        $filedate = mktime(0, 0, 0, $m, $d, $y);
			        if ($ext == ".gz" && checkdate($m,$d,$y)) {
				        if ($filedate < $delDate && $d > 1) {
					        //$messages[] = "File $file made on $d $m $y  deleted";
					        $filepath = $backupPath."/".$file;
					        if (unlink ($filepath)) {
						        doLog("INFO","File $file made on $d $m $y  deleted");
					        } else {
						        doLog("ERROR","File $file made on $d $m $y  error deleting");
					        }
				        }
			        }
	        	}
	        }
	        closedir($dh);
	    }
	}
} else {
	doLog("ERROR","Days to keep NOT CONFIGURED");
	//$messages[] = "Days to keep NOT CONFIGURED";
}

?>