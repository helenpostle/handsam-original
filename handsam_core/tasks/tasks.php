<?php
/**
* nightly tasks page
*/
ini_set('max_execution_time', '0'); // 0 = no limit.

require(dirname(dirname(dirname(__FILE__)))."/shared/config.php");
require("$rootPath/shared/messages.php");

require("$rootPath/handsam_core/library/funcError/funcError.php");

//any errors which occur to be handled by this function
if (!$debug) {
	set_error_handler("myErrorHandler");
}

require("$rootPath/handsam_core/library/funcPage/funcGeneral.php");
require("$rootPath/handsam_core/library/funcDb/funcDb.mySQL.php");
include("$rootPath/handsam_core/library/funcMail/funcMail.php");
include("$rootPath/handsam_core/library/funcPage/funcRequest.php");
include("$rootPath/handsam_core/library/funcDisplay/funcDisplay.php");
include("$rootPath/handsam_core/library/funcDb/funcDbEdit.php");	
require_once("$rootPath/handsam_core/library/funcError/funcLog.php");
require_once("$rootPath/handsam_core/library/clsPage/clsPageLoadFiles.php");

require_once("$rootPath/handsam_core/library/htmlpurifier-4.3.0-standalone/HTMLPurifier.standalone.php");
include("$rootPath/shared/htmlpurifier.config.php");

$allow_html_purifier = new HTMLPurifier($html_purifier_config);
$nohtml_page_purifier = new HTMLPurifier($nohtml_purifier_config);


$errorFlag = ""; 
$message = "";
$varOut="";

//make a connection to the database
$con = getConnection($dbhost,$dbname,$dbuser,$password);

/**
 * include any tasks to process below here.
 * available variables:
 * $con - as usual.
 * $messages[] - add to this array any result to send in final admin email.
 */

foreach ($installed_modules as $mod) {
	//if ($mod != "tasklist" && $mod != "lotc") {
		echo "<h1>including $mod tasks </h1>";
		if (file_exists("$rootPath/modules/$mod/tasks/tasks.php")) include("$rootPath/modules/$mod/tasks/tasks.php");
	//}
}


include("backupDatabase.php");
include("purgeBackups.php");
include("rotateErrorLog.php");
//include("rotateApacheErrorLog.php");
//include("rotateApacheAccessLog.php");

/**
* Now loop through $messages[] and send a report to the Administrator.
*/

// Delete Old TASK  Logs not error logs!

if (is_dir($logPath)) {
	
	$ms = $logDaysToKeep * 24 * 60 * 60;
	
	$delDate = time() - $ms;
	doLog("DEBUG","Deleting Task.log Files From Before ".date("d-M-Y",$delDate));
	
	if ($dh = opendir($logPath)) {
		while (($file = readdir($dh)) !== false) {
			if ($file != "." && $file != "..") {
				
				list($y, $m, $d, $ext) = sscanf($file, "task.%d-%d-%d%s");
				$filedate = mktime(0, 0, 0, $m, $d, $y);
				
				if ($ext == ".log" && checkdate($m,$d,$y)) {
					if ($filedate < $delDate) {
						//$messages[] = "File $file made on $d $m $y  deleted";
						$filepath = $logPath."/".$file;
						unlink ($filepath);
					}
				}
			}
		}
		if (file_exists($logPath."/tasks.php")) {
			unlink($logPath."/tasks.php");
		}
		closedir($dh);
	}
	//now email logs files to handsamsupport
	$todaylogFile = $logPath."/task.".date("Y-m-d").".log";
	sendMail($logMail,$adminMail,"Handsam: $installation_ref Nightly Task Log File ".date("Y-m-d"), file_get_contents($todaylogFile));

} else {
	doLog("ERROR","No Log Directory Found");	
}

$body = "";  
if (isset($messages) && is_array($messages)) {
	foreach($messages as $key => $value) {
		echo nl2br($value)."<br>"; //echo to screen
		$body .= nl2br($value)."<br>";
		
	}
}
/*
if (sendHTMLMail($logMail, $adminMail, "Handsam: $installation_ref", $body)) {
	echo "message: $body<br/> sent to : $logMail<br/> subject: Handsam: $installation_ref<br/> from: $adminMail";
} else {
	echo "mail error";
}
*/
?>



	





