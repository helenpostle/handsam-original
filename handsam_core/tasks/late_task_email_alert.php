<?php
/**
 * Send email alerts for late tasks
 * This task backs up the database to the backup folder
 */
 
doLog("INFO","LATE TASK EMAIL ALERTS:");
//$messages[] = "<br/><b>LATE TASK EMAIL ALERTS:</b>\n";

#go through all users which have accounttasks which are late, uncompleted and haven't had an email alert sent, where the assigned_to user is set to receive email alerts

$qry = "select distinct usr.* from accounttask left join usr on accounttask.assignedto = usr.userid where accounttask.state = 'ACTIVE' and actiondate < NOW() and accounttask.late_task_email_alert = 0 AND accounttask.assignedto > 0  ORDER BY userid";


$result = getRS($con, $qry);

while ($row = getRow($result)) {
		
	
	#now go through the user and select their late tasks
	#now go through the user and select their late tasks
	$taskQry = "select distinct usr.*, accounttask.*  from accounttask left join usr on accounttask.assignedto = usr.userid ";
	$taskQry .= "LEFT JOIN accountlicence ON accounttask.licenceid = accountlicence.licenceid ";
	$taskQry .= " where accounttask.state = 'ACTIVE' and actiondate < NOW() and accounttask.late_task_email_alert = 0 AND accountlicence.state = 'ACTIVE' AND accounttask.assignedto > 0  and usr.userid = {$row['userid']} ORDER BY actiondate";
	
	$taskRS = getRS($con, $taskQry);
	
	#if the task hasn't been processed and a reminder sent
	if ($row['late_task_email_alert'] == 1) {

		$accounttaskid_arr = array();
		$taskStr = "";		
		while ($taskRow = getRow($taskRS)) {
			$taskStr .= "<p><b>".$taskRow['tasktext']."</b><br/>Please <a href=\"$rootUrl/mytasks.php?accounttaskid=".$taskRow['accounttaskid']."\" title=\"go to www.handsam.org.uk\">Click here</a> to view this task.</p>";
			$accounttaskid_arr[] = $taskRow['accounttaskid'];
		}		
			
		$body = "<p>Hi ".ucwords($row['firstname']).", <br/>the following health and safety tasks are late:</p>$taskStr<p>If you do not wish to receive these email alerts, please contact your school Handsam administrator.</p>";
		
		
		if (sendHTMLMail($row['username'], $alertsMail, ALERT_LATE_TASK_TITLE, $body)) {
			#now update accounttask to mail sent for this task
			//$messages[] = "<br/>Username: ".$row['username']." :: userid: ".$row['userid']." :: accounttaskid: ".$row['accounttaskid']." :: EMAIL SENT<br/>\n";
			
			doLog("INFO","Username: ".$row['username']." :: userid: ".$row['userid']." :: accounttaskid: ".implode(',',$accounttaskid_arr)." :: EMAIL SENT");
			
			$qry = new dbUpdate("accounttask");
			$qry->setParamInArray("accounttaskid",$accounttaskid_arr);
			$qry->setReqNumberVal("late_task_email_alert",1,"late task email alert");
					
			if ($qry->execute($con)) {
				//$messages[] = "<br/>Accounttaskid: ".$row['accounttaskid']." :: Updated: late_task_email_alert: ".$qry->getSql()."<br/>\n";
				doLog("INFO","Accounttaskid: ".implode(',',$accounttaskid_arr)." :: Updated: late_task_email_alert: ".$qry->getSql());
			} else  {
				//$messages[] = "<br/>Accounttaskid: ".$row['accounttaskid']." :: ERROR UPDATING: late_task_email_alert: ".$qry->getSql()." :: ".$qry->getError()."<br/>\n";
				doLog("INFO","Accounttaskid: ".implode(',',$accounttaskid_arr)." :: ERROR UPDATING: late_task_email_alert: ".$qry->getSql()." :: ".$qry->getError());
			}
	
	
		} else {
			//$messages[] = "<br/>Username: ".$row['username']." :: userid: ".$row['userid']." :: accounttaskid: ".$row['accounttaskid']." :: EMAIL FAILURE\n";
			doLog("INFO","Username: ".$row['username']." :: userid: ".$row['userid']." :: accounttaskid: ".implode(',',$accounttaskid_arr)." :: EMAIL FAILURE");
		echo "<br/>$body<br/>from: $alertsMail<br/>To: ".$row['username']."<br/>".ALERT_LATE_TASK_TITLE."<br/><br/><br/>";
		//die();
		}
	} else {
		#update any new out of date tasks to late_task_email_alert value = 99 so that when a user's email setting activated, they don't get sent old out of date task reminders
		$accounttaskid_arr = array();
		while ($taskRow = getRow($taskRS)) {
			$accounttaskid_arr[] = $taskRow['accounttaskid'];
		}		
		$qry = new dbUpdate("accounttask");
		$qry->setParamInArray("accounttaskid",$accounttaskid_arr);
		$qry->setReqNumberVal("late_task_email_alert",99,"late task email alert");
				
		if (!$qry->execute($con)) {
			//$messages[] = "<br/>Accounttaskid: ".$row['accounttaskid']." :: Updated: late_task_email_alert: ".$qry->getSql()."<br/>\n";
			doLog("INFO","Accounttaskid: ".implode(',',$accounttaskid_arr)." :: ERROR UPDATING: late_task_email_alert: ".$qry->getSql()." :: ".$qry->getError());
		}
	}
		
		
}
?>
