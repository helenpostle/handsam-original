<?php
/** This task rotates the custom error logs */


//$messages[] = "<br><b>ROTATE ERROR LOG:</b>";
doLog("TITLE","ROTATE APACHE  ACCESS LOG:");
$logFile = "$logDir/$installation_ref"."-access_log";
$logFileDate = "$logDir/$installation_ref"."-access_log.".date("Y-m-d").".apachelog";

if (is_dir($logPath)) {
	#rotate if there are enough errors
	if (filesize($logFile) > $logSizeToRotate) {
		#move current file to date

		if (rename($logFile,$logFileDate)) {
			//sendMail($logMail,$adminMail,"Handsam: $installation_ref Log File $logFileDate", file_get_contents($logFileDate));

			//$messages[] = "Renaming $logFile to $logFileDate";
			doLog("INFO","Renaming $logFile to $logFileDate");
			#make new file
			if (touch($logFile)) {
			 	 $fp = @fopen ($logFile, "a+");
			 	 $log = "";
			 	 @fwrite ($fp,$log);
			 	 @fclose($fp);
			 	 //this resets the apache logging to use new file
			 	 exec("/usr/bin/killall -HUP httpd");

				//$messages[] = "Initializing New log File";
				doLog("INFO","Initializing New log File");
			} else {
				//$messages[] = "<font color='red'>Failed to initialise new log file</font>";
				doLog("ERROR","Failed to initialise new log file");
			}
		} else {
			//$messages[] = "<font color='red'>Failed to rename $logFile to $logFileDate</font>";
			doLog("ERROR","Failed to rename $logFile to $logFileDate");
		}
	}
	
#delete old files
$ms = $logDaysToKeep * 24 * 60 * 60;

$delDate = time() - $ms;
//$messages[] = "Deleting Log Files From Before ".date("d-M-Y",$delDate);
doLog("INFO","Deleting Log Files From Before ".date("d-M-Y",$delDate));

    if ($dh = opendir($logPath)) {
        while (($file = readdir($dh)) !== false) {
	        if ($file != "." && $file != "..") {
            	
		        list($y, $m, $d, $ext) = sscanf($file,  $installation_ref."-access_log.%d-%d-%d%s");
		        $filedate = mktime(0, 0, 0, $m, $d, $y);
		        
		        if (($ext == ".log" || $ext == ".apachelog" ) && checkdate($m,$d,$y)) {
			        if ($filedate < $delDate) {
				        //$messages[] = "File $file made on $d $m $y  deleted";
				        $filepath = $logPath."/".$file;
				        if (unlink ($filepath)) {
					        doLog("INFO","File $filepath made on $d $m $y  deleted");
				        } else {
					        doLog("ERROR","File $filepath made on $d $m $y  error deleting");
				        }
			        }
		        }
        	}
        }
        closedir($dh);
    }

} else {
	//$messages[] = "No Log Directory Found";	
	doLog("ERROR","No Log Directory Found");
}


?>