<?php

/**
* User editing functionality
*
* For admin and super admin users
* content: useredit.php, userlist.php
* boxes: none
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
 
require("../shared/startPage.php");

$pageFiles->includePhp();

#check if have requested a specific user

if (isAllowed("superAdminModules")) {
	
	$qry = "select accountlicence.state, accountlicence.licenceid, tasklist.tasklistname, tasklist.tasklistid, accountlicence.end_date, apps.appname from accountlicence left join tasklist on accountlicence.tasklistid = tasklist.tasklistid  left join apps on apps.appid = accountlicence.appid where accountlicence.accountid=1";
	$result = getRS($con, $qry);
	$title = "My Modules";
	$content = "my_modules.php";
	$contentId = "contentWide";	

} else {
	#trying to do something we shouldn't! throw error so it is logged.	
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);

}

include("layout.php");
?>