<?php

$school_status_str = "'".implode("','",$school_status)."'";
	    	
$this->arrayParams["startdate"] = getParam("startdate",getStartLastMonth());
$this->arrayParams["enddate"] = getParam("enddate",getEndLastMonth());
$this->arrayParams["group_id"] = getParam("group_id",1);
$this->arrayParams["accountid"] = getParam("accountid",1);
$this->arrayParams["school_type"] = getParam("school_type",'ALL');

$arrayParams = $this->arrayParams;

$this->title['Date Range'] = $arrayParams["startdate"]." to ".$arrayParams["enddate"];

//get group name:
$sql = "select * from task_category_group where group_id = ".$arrayParams["group_id"];
$rs = getRS($con,$sql);
$row = getRow($rs);

//get accountname
$status_flag = false;
foreach ($school_status as $key => $val) {
	if ($arrayParams["accountid"] == $key) {
		$this->title['Account'] = "'$val' Accounts";
		$status_flag = true;
	}
}

if ($arrayParams["accountid"] == "all" && $status_flag == false){
	$this->title['Account'] = "All Accounts";
	$status_flag = true;
}

if ($status_flag == false && $arrayParams["accountid"] > 0) {
	$sql = "select accountname from account where accountid = ".$arrayParams["accountid"];
	$rs = getRS($con,$sql);
	$row = getRow($rs);
	$this->title['Account'] = $row['accountname'];
}

/*
if ($arrayParams["accountid"] == "LAET") {
	$this->title['Account'] = "LAET Accounts";
} else if ($arrayParams["accountid"] == "AET"){
	$this->title['Account'] = "AET Accounts";
} else if ($arrayParams["accountid"] == "all"){
	$this->title['Account'] = "All Accounts";
} else {
	$sql = "select accountname from account where accountid = ".$arrayParams["accountid"];
	$rs = getRS($con,$sql);
	$row = getRow($rs);
	$this->title['Account'] = $row['accountname'];
}
*/


if ($this->arrayParams["school_type"] == 'ALL') {
	$school_type_qry = "";
	$this->title['School Type'] = "All School Types";
} else  {
	$school_type_qry = " and school_type = '".$this->arrayParams["school_type"]."' ";
	$this->title['School Type'] = ucfirst(strtolower($this->arrayParams["school_type"]));
}

//account manager
$this->arrayParams["account_manager"] = getParam("account_manager",0);
if ($this->arrayParams["account_manager"] > 0) {
	$manager_qry = " and account_manager = ".$this->arrayParams["account_manager"];
	$this->title['Accounts managed by'] = "Me";
} else {
	$manager_qry = "";
	$this->title['Accounts managed by'] = "All managers";
}

$group_sql = "";
if ($arrayParams["group_id"] > 0) {
	$group_sql = " WHERE tc.group_id = ".$arrayParams["group_id"]." ";
	//get groupname
	$group_name_sql = "select * from task_category_group where group_id = ".$arrayParams["group_id"];
	$group_name_rs = getRS($con, $group_name_sql);
	$group_row = getRow($group_name_rs);
	$this->title['Group'] = $group_row['title'];
} else {
	$this->title['Group'] = "All Groups";
}
	
//if ($arrayParams["accountid"] == "LAET" || $arrayParams["accountid"] == "AET") {
if (in_array($arrayParams["accountid"], $school_status)) {
	
	//change to school_type
	$include_file_sql = "SELECT tc.categoryid AS id, tc.categoryname as category_name, ROUND(SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS on_time, COUNT(act.accounttaskid) AS total, SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) AS num_on_time, ROUND(SUM(CASE WHEN act.assignedto > 0  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS allocated FROM account acc RIGHT JOIN (SELECT categoryid, categoryname, assignedto, school_status,accountid, state, group_id, actiondate, completedate, accounttaskid FROM rep_acc_task_cat_group WHERE   actiondate > '".formatDatabaseInputDate($arrayParams["startdate"])."' AND actiondate < '".formatDatabaseInputDate($arrayParams["enddate"])."' AND school_status in ('".$arrayParams["accountid"]."') $school_type_qry $manager_qry) act ON acc.accountid = act.accountid RIGHT JOIN taskcategory tc ON act.categoryid = tc.categoryid $group_sql group by category_name order by category_name ASC";
	
} else if ($arrayParams["accountid"] == "all") {
	//both types hard coded
	$include_file_sql = "SELECT tc.categoryid AS id, tc.categoryname as category_name, ROUND(SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS on_time, COUNT(act.accounttaskid) AS total, SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) AS num_on_time, ROUND(SUM(CASE WHEN act.assignedto > 0  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS allocated FROM account acc RIGHT JOIN (SELECT categoryid, categoryname, assignedto, school_status,accountid, state, group_id, actiondate, completedate, accounttaskid FROM rep_acc_task_cat_group WHERE   actiondate > '".formatDatabaseInputDate($arrayParams["startdate"])."' AND actiondate < '".formatDatabaseInputDate($arrayParams["enddate"])."' AND school_status in ($school_status_str) $school_type_qry $manager_qry) act ON acc.accountid = act.accountid RIGHT JOIN taskcategory tc ON act.categoryid = tc.categoryid $group_sql group by category_name order by category_name ASC";
	
} else {
	$arrayParams["accountid"] = intval($arrayParams["accountid"]);
	$include_file_sql = "SELECT tc.categoryid AS id, tc.categoryname as category_name, ROUND(SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS on_time, COUNT(act.accounttaskid) AS total, SUM(CASE WHEN act.completedate <= act.actiondate  THEN 1 ELSE 0 END) AS num_on_time, ROUND(SUM(CASE WHEN act.assignedto > 0  THEN 1 ELSE 0 END) / COUNT(act.accounttaskid) *100 ,1) AS allocated FROM account acc RIGHT JOIN (SELECT categoryid, categoryname, assignedto, school_status,accountid, state, group_id, actiondate, completedate, accounttaskid FROM rep_acc_task_cat_group WHERE   actiondate > '".formatDatabaseInputDate($arrayParams["startdate"])."' AND actiondate < '".formatDatabaseInputDate($arrayParams["enddate"])."' AND accountid = ".$arrayParams["accountid"].") act ON acc.accountid = act.accountid  RIGHT JOIN taskcategory tc ON act.categoryid = tc.categoryid $group_sql group by category_name order by category_name ASC";


} 
?>