<?php

$school_status_str = "'".implode("','",$school_status)."'";
	    	
$this->arrayParams["startdate"] = getParam("startdate",getStartLastMonth());
$this->arrayParams["enddate"] = getParam("enddate",getEndLastMonth());
$this->arrayParams["riddor"] = getParam("riddor",1);
$this->arrayParams["school_type"] = getParam("school_type",'ALL');

$arrayParams = $this->arrayParams;

$this->title['Date Range'] = $arrayParams["startdate"]." to ".$arrayParams["enddate"];

if ($this->arrayParams["school_type"] == 'ALL') {
	$school_type_qry = "";
	$this->title['School Type'] = "All School Types";
} else  {
	$school_type_qry = " and account.school_type = '".$this->arrayParams["school_type"]."' ";
	$this->title['School Type'] = ucfirst(strtolower($this->arrayParams["school_type"]));
}

//get accountname
$acc_sql = "";

$status_flag = false;
foreach ($school_status as $key => $val) {
	if ($arrayParams["accountid"] == $key) {
		$this->title['Account'] = "'$val' Accounts";
		$acc_sql = " and account.school_status = '$key' ";
		$status_flag = true;
	}
}

if ($arrayParams["accountid"] == "all" && $status_flag == false){
	$this->title['Account'] = "All Accounts";
	$acc_sql = " and account.school_status in ($school_status_str) ";
	$status_flag = true;
}

if ($status_flag == false && $arrayParams["accountid"] > 0) {
	$sql = "select accountname from account where accountid = ".$arrayParams["accountid"];
	$rs = getRS($con,$sql);
	$row = getRow($rs);
	$this->title['Account'] = $row['accountname'];
	$acc_sql = " and acc_accident.accountid = ".$arrayParams["accountid"];
}

/*
if ($arrayParams["accountid"] == "LAET") {
	$this->title['Account'] = "LAET Accounts";
	$acc_sql = " and account.school_status = 'LAET' ";
} else if ($arrayParams["accountid"] == "AET"){
	$this->title['Account'] = "AET Accounts";
	$acc_sql = " and account.school_status = 'AET' ";
} else if ($arrayParams["accountid"] == "all"){
	$this->title['Account'] = "All Accounts";
	$acc_sql = " and account.school_status in ('LAET', 'AET') ";
} else {
	$sql = "select accountname from account where accountid = ".$arrayParams["accountid"];
	$rs = getRS($con,$sql);
	$row = getRow($rs);
	$this->title['Account'] = $row['accountname'];
	$acc_sql = " and acc_accident.accountid = ".$arrayParams["accountid"];
}
*/

if ($arrayParams["riddor"] == 0) {
	$this->title['Riddor Reportable'] = "Non-riddor incidents";
	$riddor_sql = " and acc_accident.riddor = 'NON-RIDDOR'";
} else if ($arrayParams["riddor"] == 1){
	$this->title['Riddor Reportable'] = "Riddor incidents";
	$riddor_sql = " and acc_accident.riddor = 'RIDDOR'";
} else {
	$riddor_sql = "";
	$this->title['Riddor Reportable'] = "Riddor and Non-riddor incidents.";
	
}

//account manager
$this->arrayParams["account_manager"] = getParam("account_manager",0);
if ($this->arrayParams["account_manager"] > 0) {
	$manager_qry = " and account.account_manager = ".$this->arrayParams["account_manager"];
	$this->title['Accounts managed by'] = "Me";
} else {
	$manager_qry = "";
	$this->title['Accounts managed by'] = "All managers";
}

$include_file_sql = "SELECT accountname, school_status, acc_accident.accidentid as id, acc_accident.state, acc_accident.accountid as accountid, CONCAT(acc_injured_party.lastname, ', ', acc_injured_party.firstname) AS \"Injured Party\", status as \"Status\", acc_accident_site.site_name as \"Site Name\", accident_type as \"Type\", (CASE when riddor = 'NON-RIDDOR'  THEN 'No' ELSE 'Yes' END) as \"Riddor\", (CASE when ambulance = 0  THEN 'No' ELSE 'Yes' END) as \"Ambulance\", (CASE when party_injured = 0  THEN 'No' ELSE 'Yes' END) as \"Injured\", (CASE when first_aid = 0  THEN 'No' ELSE 'Yes' END) as \"First Aid\", days_off as \"Days Off\", cost as \"Cost\" ";
$include_file_sql .= "from acc_accident left join acc_injured_party on acc_accident.injuredid = acc_injured_party.injuredid left join acc_accident_site on acc_accident.siteid = acc_accident_site.siteid left join account on acc_accident.accountid = account.accountid ";
$include_file_sql .= "WHERE  CAST(accident_time AS DATE) >= '".formatDatabaseInputDate($arrayParams["startdate"])."' AND CAST(accident_time AS DATE) <= '".formatDatabaseInputDate($arrayParams["enddate"])."' $school_type_qry $riddor_sql $acc_sql $manager_qry";

$include_file_sql .= " ORDER BY \"Injured Party\"";
	
?>