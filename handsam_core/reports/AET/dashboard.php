<?php
	if ($report_id == 0) {
	
		$dashboard = new dashboardReport($rootPath, $params, $installation);
		$dashboard->arrayParams["group_id"] = 1;
		$dashboard->runReport(1, $con, "Curriculum");
	
		$dashboard->arrayParams["group_id"] = 2;
		$dashboard->runReport(1, $con, "First Aid");
		
		$dashboard->arrayParams["group_id"] = 3;
		$dashboard->runReport(1, $con, "Leadership &amp;  Staff");
		
		$dashboard->arrayParams["group_id"] = 4;
		$dashboard->runReport(1, $con, "Office");
		
		$dashboard->arrayParams["group_id"] = 5;
		$dashboard->runReport(1, $con, "Site");
		

		$dashboard->arrayParams["riddor"] = 0;
		$dashboard->runReport(3, $con, "Open Non-Riddor Incidents");

		$dashboard->arrayParams["riddor"] = 1;
		$dashboard->runReport(4, $con, "Open Riddor Incidents");
		
		$dashboard->arrayParams["riddor"] = 2;
		$dashboard->runReport(6, $con, "All Incidents");
		
		$content = "rep.default_dashboard_view.php";
		
		$subtitle = $dashboard->getTitle();
		$pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.params.php");
		
	} else {
		
		$params['accountid'] = $accountid;
	
		$dashboard = new dashboardReport($rootPath, $params, $installation);
		//$dashboard->arrayParams["group_id"] = 1;
		$dashboard->runReport($report_id, $con, "test");

		$content = $dashboard->content_view;
		$subtitle = $dashboard->getTitle();
		$pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.params.php");
		if ($report_id==2 && $accountid>0) $pageFiles->addBox("$rootPath/handsam_core/boxes/box.rep.dashboard_links.php");

			
	}
?>