<?php

$this_page = "account_dashboard.php";
/**
 * Account specific dashboard
 * 
 * Just the same as ./dashboard.php, just locked to a single acount
 * 
 */
// make this a secure page
$secure = true;

require("../shared/startPage.php");

$pageFiles->addFunc('dashboard');

$pageFiles->includePhp();

//adds plurals to strings
$Inflector = new Inflector();

$report_id = getIntFromRequest('report_id');

$accountid = $clsUS->accountid;

$tabs = array();

if(isset($_POST['params']))
{
    $params = __unserialize($_POST['params']);
    $qsfrm = $_POST['params'];
    if(is_array($params))
    {
        foreach($params as $p_k => $p_v)
        {
            if(!isset($_POST[$p_k]))
                $_POST[$p_k] = $p_v;
        }
    }
} 
elseif(isset($_GET['params']))
{
    $qsfrm = $_GET['params'];
    $params = __unserialize(stripslashes($_GET['params']));
    if(is_array($params))
    {
        foreach($params as $p_k => $p_v)
        {
            if(!isset($_GET[$p_k]))
                $_GET[$p_k] = $p_v;
        }
    }
}
else
{
    $params = array();
    $qsfrm = "";
    $params["startdate"] = getParam("startdate", getStartLastWeek());
    $params["enddate"] = getParam("enddate", getEndLastWeek());
}

if(isAllowed("viewAccountDashboard"))// @todo viewAccountDashboard
{
    $title = "Dashboard";
    include("$rootPath/installations/$installation/reports/ac_dashboard.php");
}
//include layout.
include ("layout.php");
