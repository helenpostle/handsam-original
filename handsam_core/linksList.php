<?php
/**
* Standard Index Page
*/
//$this_page = "index.php";
$content = "";
//include basic stuff
$secure = true;
require_once("../shared/startPage.php");
$this_page = "linksList.php";

$pageFiles->addFunc('handsam_uploads');

//make this a secure page

$pageFiles->includePhp();

//specify page content


$filetype = getStrFromRequest("filetype");

if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
	$title = "Links";		
	$content = "listLinks.php";
	$userid = $clsUS->userid;
	$accountid = $clsUS->accountid;
	$linkid = setQSInt("linkid");
	$contentid = setQSInt("contentid");
	$edittype = getStrFromRequest("edit");
	if ($edittype != "page") {
		$edittype = "task";
	}
	#for passing page and contentid between pages for adding links/files to content webpages
	if (isAllowed("editContent")) {
		$contentIdLink = "&contentid=".$contentid."&edit=".$edittype;
	} else {
		$contentIdLink = "";
	}
	if (isAllowed("editTask") && $filetype != "handsamDoc") {
		$accountid = getIntFromRequest("accountid");
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);
}

//include layout.
include ("layout_FB.php");

?>