<?php
/**
* licence editing functionality
* content: licenceEdit.php
* boxes: licenceholidays.php
*/

/**
 * Include basic stuff
 */

####make this a secure page
$secure = true;
 
require("../shared/startPage.php");

$pageFiles->addFunc('licence');

$pageFiles->includePhp();

#check if have requested a specific user
$accountid = getIntFromRequest("accountid");
$licenceid = getIntFromRequest("licenceid");
$tasklistid = getIntFromRequest("tasklistid");
$appid = getIntFromRequest("appid");
$title = "";


if (isAllowed("editAccount")) {
	
	//get account title for page title
	$qry = "SELECT accountname FROM account where accountid = ".$accountid;
	$result = getRS($con, $qry);
	$row = getRow($result);
	$title = $row['accountname'];
	$billing_date = time();
	$contract_len = "";
	if ($licenceid == 0) {
		//add new licence/renew licence
		if ($tasklistid > 0) {
			$qry = "SELECT * FROM accountlicence left join apps on apps.appid = accountlicence.appid where accountlicence.accountid = ".$accountid." and tasklistid = $tasklistid and state != 'DELETED' order by licenceid desc LIMIT 0,1";
			$result = getRS($con, $qry);
			$row = getRow($result);
			$appid = $row['appid'];
		} else if ($appid > 0) {
			$qry = "SELECT * FROM accountlicence left join apps on apps.appid = accountlicence.appid where accountlicence.accountid = ".$accountid." and accountlicence.appid = $appid and accountlicence.appid != 1 and state != 'DELETED' order by licenceid desc LIMIT 0,1";
			$result = getRS($con, $qry);
			$row = getRow($result);
		}
		//echo $qry;
		$appname = $row['appname'];
		//if old licence still current, sewt start date of new licence to be day of old licence end_date
		if (strtotime($row['end_date']) > strtotime(strftime("%Y-%m-%d",time()))) {
			$frmTitle = "$appname : Renew licence";
			//start date (for date field) = current licence end date
			$start_date = (strtotime($row['end_date']));
		} else {
			//otherwise show create link
			$frmTitle = "$appname : Create licence";
			$start_date = time();
		}
		
		//set last billing date from old licence too:
		$billing_date = strtotime($row['billing_date']);
		$contract_len = $row['contract_length'] - 1;
		if ($contract_len < 0) $contract_len = 0;
		
	} else {
		//get state of licence
		$lic_qry = "SELECT state FROM accountlicence where licenceid = $licenceid";
		$lic_rs = getRS($con,$lic_qry);
		$lic_row = getRow($lic_rs);
		if ($tasklistid > 0) {
			$qry = "SELECT * FROM accountlicence left join apps on accountlicence.appid = apps.appid where accountid = $accountid and tasklistid = (select tasklistid from accountlicence where licenceid = $licenceid) and state != 'DELETED'";
			
			if ($lic_row['state'] == 'NEW') {
				$box[] = "box.licenceholidays.php";				
				if (in_array("tasklist", $installed_modules)) $pageFiles->addBox("$rootPath/modules/tasklist/boxes/box.supplytasks.php");
			} else {
				$pageFiles->addBox("$rootPath/modules/tasklist/boxes/box.tasklist_link.php");
			}
		} else {
			$qry = "SELECT * FROM accountlicence left join apps on apps.appid = accountlicence.appid where licenceid = $licenceid and state != 'DELETED'";
		}
		//echo $qry;
		$rs_licence = getRS($con, $qry);
		$cnt_licence = getResultRows($rs_licence);
		$row_licence = getRow($rs_licence);
		$appname = $row_licence['appname'];
		$appid = $row_licence['appid'];
		$frmTitle = "$appname : Edit licence: licence Id ".$licenceid;
	}
	$content = "licenceEdit.php";
	
	if (buttonClicked("cancel")) {
		#return to accounts page
		if (isAllowed("editAccount") && $clsUS->accountid == $accountid) {
			//is the superadmin account
			headerLocation ("my_account.php", false);
		} else {
			headerLocation ("accounts.php?accountid=$accountid", false);
		}
	} 
	
	
	if ($licenceid !== null) {

		if (buttonClicked("save") || buttonClicked("activate") || buttonClicked("supply") || buttonClicked("submit_users")) {
							
			if ($licenceid == 0) {
				$qry = new dbInsert("accountlicence");
				if ($tasklistid > 0) {
					$qry->setReqNumberVal("tasklistid",$tasklistid,"tasklist id");
					$appid = 1;					
				}
				$qry->setReqNumberVal("appid",$appid,"app id");				
			} else {
				$qry = new dbUpdate("accountlicence");
				$qry->setParam("licenceid",$licenceid);				
			}
			if (isset($_POST['bt_activate'])) {
				$qry->setReqStringVal("state","ACTIVE","state");
			} else if ($licenceid == 0){
				$qry->setReqStringVal("state","NEW","state");
			}
			
			if (isset($qry)) {
				#calculate end date from start date plus 1 year duration
				$END_YEAR = strftime("%Y",strtotime($_POST["start_date"])) + 1;
				$END_DATE = strftime("%a, %d %b %Y",(strtotime(strftime("%d %b ".$END_YEAR,strtotime($_POST["start_date"])))));
				$qry->setReqDateVal("start_date", $_POST["start_date"], "Start date");
				$qry->setDateVal("billing_date", $_POST["billing_date"], "Billing date");
				$qry->setNumberVal("contract_length",$_POST["contract_length"],"Billing period");
				$qry->setReqNumberVal("accountid",$_POST["accountid"],"Account id");
				$qry->setReqDateVal("end_date", $END_DATE, "End date");
				
				//now process any module specific conf that is posted from the form $rootPath/modules/{$rowmod['folder']}/shared/licence_conf.php
				
				//get array of name/value pairs
				
				if (isset($_POST['mod_conf_name']) && count($_POST['mod_conf_name']) > 0) {
					$mod_conf_arr = array();
					for ($i=0; $i<count($_POST['mod_conf_name']); $i++) {
						$mod_conf_arr[$_POST['mod_conf_name'][$i]] = $_POST['mod_conf_val_'.$i];
					}
					
					$qry->setReqStringVal("conf",json_encode($mod_conf_arr),"conf");
				}
				
			
				$qry->setAudit($clsUS->userid);
				
				if ($qry->execute($con)) {
					if ($licenceid == 0) {
						$licenceid = $qry->getNewID();	
					}
					$messages[] = SAVED;
                                        
					//now do app specific function if licence made active
					if (function_exists("licence_active_app_$appid") && buttonClicked("activate")) {
                                            call_user_func_array("licence_active_app_$appid", array($con, $_POST["accountid"], $clsUS));
					}
					
					if (buttonClicked("supply")) headerLocation ("$rootUrl/modules/tasklist/supplytasks.php?accountid=$accountid&licenceid=$licenceid", false);
					if (buttonClicked("submit_users")) headerLocation ("$rootUrl/modules/tasklist/supplytasks.php?accountid=$accountid&licenceid=$licenceid&submit_users=1", false);
					if (isAllowed("editAccount") && $clsUS->accountid == $accountid) {
						//is the superadmin account
						headerLocation ("my_account.php", $messages);
					} else {
						headerLocation ("accounts.php?accountid=$accountid", $messages);
					}
					
				} else {
					$messages[] = $qry->getError();	
				}
			}
			
		} else if (buttonClicked("delete")) {
			#cannot delete self
				$qry = new dbUpdate("accountlicence");
				$qry->setParam("licenceid",$licenceid);
				$qry->setReqStringVal("state","DELETED","State");
				$qry->setAudit($clsUS->userid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#return to accounts page
					$messages[] = DELETED;
					if (isAllowed("editAccount") && $clsUS->accountid == $accountid) {
						//is the superadmin account
						headerLocation ("my_account.php", false);
					} else {
						headerLocation ("accounts.php?accountid=$accountid", false);
					}
				}
		} 
	}
	
} else {
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);
}

if (isAllowed("editAccount")) { 
	$this_page = "accounts.php";
} else {
	$this_page = "tasks.php";
}

#Include layout page
include("layout.php");
?>