<?php
/**
* Securely Download a file from the database
*/
//MUST ADD SECURITUY AND CHECK FILE EXISTS
$content = "";
//include basic stuff

//make this a secure page
$secure = false;

include ("../shared/startPage.php");

$pageFiles->includePhp();
//specify page content
$title = "Download Failes";		//the <title> tag and <h1> page title

//get the id 
$id = getIntFromRequest("id");
$accounttaskid = getIntFromRequest("accounttaskid");

$rsCheckContent = getRS($con,"select * from content_files where fileid = $id");
$cnt = getResultRows($rsCheckContent);


if ($cnt > 0 || isAllowed("uploadHandsamDocs") || isAllowed("uploadAdminDocs") || isAllowed("uploadUserDocs")) {

	//get the record set
	if ($cnt > 0 || isAllowed("uploadHandsamDocs")) {
		$sql = "select * from files where fileid = $id";
	} else {
		//first get the filetype:
		$sql = "select filetype from files where fileid = $id";
		$rs = getRS($con,$sql);
		$row = getRow($rs);
		if($row['filetype'] == 'handsamDoc') {
			if ($accounttaskid>0) {
				$sql = "select f.* from files f left join task_files tf on f.fileid = tf.fileid left join accounttask at on tf.taskid = at.taskid where f.fileid = $id and at.accounttaskid = $accounttaskid";
			} else {
				$sql = "select distinct f.* from files f left join task_files tf on f.fileid = tf.fileid left join accounttask at on tf.taskid = at.taskid where f.fileid = $id and f.accountid = {$clsUS->accountid}";
			}
		} else if ($row['filetype'] == 'adminDoc' || $row['filetype'] == 'userDoc') {
			if ($accounttaskid>0) {
				$sql = "select f.* from files f left join task_files tf on f.fileid = tf.fileid left join accounttask at on tf.accounttaskid = at.accounttaskid where f.fileid = $id and at.accounttaskid = $accounttaskid";
			} else {
				$sql = "select distinct f.* from files f left join task_files tf on f.fileid = tf.fileid left join accounttask at on tf.accounttaskid = at.accounttaskid where f.fileid = $id and f.accountid = {$clsUS->accountid}";
			}
		} else {
			$sql = "";
		}
			
	}
	
	$rsFile = getRS($con, $sql);
	if ($rowFile = getRow($rsFile)) {
		//download the file
		$type = $rowFile["filetype"];
		$filename = $rowFile["filename"];
		
		
		if ($type == "handsamDoc") {
			$path = $handsamdocs_fp;
		} else if ($type == "adminDoc") {
			$path = $admindocs_fp;
		} else if ($type == "userDoc") {
			$path = $userdocs_fp;
		} else if ($type == "accountLotcDoc") {
			$path = $accountLotcDocs_fp;
		}
		
		$file = $path.$filename;
		// send the right headers
		
		header("Content-Type: application/octet-stream");
		//header("Content-Length: " . filesize($path));
		header('Content-Disposition: attachment; filename="'.$filename.'"');
		
		if (@file_exists($file)) echo file_get_contents($file);
		exit;
	}
}