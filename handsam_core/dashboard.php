<?php
ini_set('max_execution_time', '0'); // 0 = no limit.

$this_page = "dashboard.php";
/**
* change password form 
* content: password.php
* boxes: none
*/
####make this a secure page
$secure = true;

require("../shared/startPage.php");

$pageFiles->addFunc('dashboard');

$pageFiles->includePhp();

//adds plurals to strings
$Inflector = new Inflector();

$report_id = getIntFromRequest('report_id');

$accountid = getStrFromRequest('accountid');
$urn = getIntFromRequest("urn");
$tabs = array();
if ($urn == 1) {
    $unread = new Unreadnews();
    if($core_conf['unread_news_must_read']===true)
    {
      $unreadnews = $unread->getUnread();
    }
    else
    {
      $unreadnews = $unread->getUnread($core_conf['unread_news_items']);
    }      
    
}
$cluster_qry="";
  if(isset($clsUS->account_clusters) && count($clsUS->account_clusters)>0 && isset($core_conf['account_cluster_SA_access']) && $core_conf['account_cluster_SA_access'])
  {
    $cluster_qry = " and account.cluster_id in (".implode(',',$clsUS->account_clusters).") ";
  }
  
  if(isset($params['school_cluster']) && $params['school_cluster']>0)
  {
    $cluster_qry = " and account.cluster_id = {$params['school_cluster']} ";
  }
 
if (isset($_POST['params'])) {
	$params = __unserialize($_POST['params']);
	$qsfrm = $_POST['params'];
	if (is_array($params)) {
		foreach ($params as $p_k => $p_v) {
			if (!isset($_POST[$p_k])) $_POST[$p_k] = $p_v;
		}
	}
} else if(isset($_GET['params'])) {
	$qsfrm = $_GET['params'];
	$params = __unserialize(stripslashes($_GET['params']));
	if (is_array($params)) {
		foreach ($params as $p_k => $p_v) {
			if (!isset($_GET[$p_k])) $_GET[$p_k] = $p_v;
		}
	}
} else {
	$params = array();
	$qsfrm = "";
    $params["startdate"] = getParam("startdate",getStartLastWeek());
    $params["enddate"] = getParam("enddate",getEndLastWeek());
    $params["school_type"] = getParam("school_type",'ALL');
    $params["school_pupils"] = getParam("school_pupils",'ALL');
    $params["school_cluster"] = getParam("school_cluster",'ALL');

}

$params["cluster_qry"] = $cluster_qry;

if (isAllowed("viewDashboard")) {
	$title = "Dashboard";
	include("$rootPath/installations/$installation/reports/dashboard.php");
	
}
//include layout.
include ("layout.php");

?>