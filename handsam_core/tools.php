<?php
/**
* Standard  Page
*/
ini_set('max_execution_time', '0'); // 0 = no limit.
//$this_page = "tools.php";
$admin_tools = true;
$at_result = array();
//include basic stuff
//make this a secure page
$secure = true;
include ("../shared/startPage.php");

$module = getStrFromRequest("mod");
$tool = getIntFromRequest("tool", 0);

$pageFiles->addFunc('content');
$pageFiles->addFunc('email');
$pageFiles->addFunc('user_table');
$pageFiles->addFunc('user');

//specify page content

$pageFiles->includePhp();

$title = "Admin Tools page";

if (isAllowed("handsamOnly")) {

	if ($module == "") {
		if ($tool == 0) {
			$content = "tools.index.php";
		} else {
			switch ($tool) {
				case 1:
				include("$rootPath/handsam_core/tools/tool.clean_files.php");
				break;
				case 2:
				include("$rootPath/handsam_core/tools/tool.missing_files.php");
				break;
        case 3:
				include("$rootPath/handsam_core/tools/tool.csv_import.php");
				break;
			}
		}
	} elseif (file_exists("$rootPath/modules/$module/tools/tools.php")) {
		include("$rootPath/modules/$module/tools/tools.php"); 
	}

	


	//include layout.
	include ("layout.php");
}

?>