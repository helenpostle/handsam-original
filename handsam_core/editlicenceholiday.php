<?php
/**
* edit licence holiday periods
* content: holidayEdit.php
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../shared/startPage.php");


$pageFiles->includePhp();


#check if have requested a specific holiday period
$holidayid = getIntFromRequest("holidayid");
$licenceid = getIntFromRequest("licenceid");
$tasklistid = getIntFromRequest("tasklistid");
$title = "";


if (isAllowed("editAccount")) {
	
	//get account title for page title
	$qry = "SELECT account.accountid, account.accountname as accountname FROM account inner join accountlicence on account.accountid = accountlicence.accountid where accountlicence.licenceid = ".$licenceid;
	$result = getRS($con, $qry);
	$row = getRow($result);
	$title = $row['accountname'];
	//get account id for header return to licence page 
	$accountid = $row['accountid'];
	if ($holidayid == 0) {
		$frmTitle = "Add licence holiday period";
	} else {
		$frmTitle = "Edit licence holiday period";
	}
	$content = "holidayEdit.php";
	
	if (buttonClicked("cancel")) {
		#return to licence page 
		headerLocation ("licence.php?accountid=$accountid&licenceid=$licenceid&tasklistid=$tasklistid", false);
	} 
	
	
	if ($licenceid !== null) {

		if (buttonClicked("save")) {
							
			if ($holidayid == 0) {
				$qry = new dbInsert("licenceholiday");
			} else {
				$qry = new dbUpdate("licenceholiday");
				$qry->setParam("holidayid",$holidayid);
			}
		
			if (isset($qry)) {
				$holiday_start = $_POST["holidaystart"];
				$holiday_end = $_POST["holidayend"];
				if (strtotime($holiday_end) < strtotime($holiday_start)) {
					$holiday_end = "error";
					$messages[] = HOLIDAY_END_ERROR;
				}
				$qry->setReqDateVal("holidaystart", $holiday_start, "Holiday Start");
				$qry->setReqNumberVal("licenceid",$_POST["licenceid"],"licence id");
				
				$qry->setReqDateVal("holidayend", $holiday_end, "Holiday End");
				$qry->setStringVal("title",$_POST["title"],"Holiday title");
			
				$qry->setAudit($clsUS->userid);
				
				if ($qry->execute($con)) {
					if ($holidayid == 0) {
						$holidayid = $qry->getNewID();	
					}
					$messages[] = SAVED;
					#return to licence page 
					headerLocation ("licence.php?accountid=$accountid&licenceid=$licenceid&tasklistid=$tasklistid", $messages);					
				} else {
					$messages[] = $qry->getError();	
				}
			}
			
		} else if (buttonClicked("delete")) {
				$qry = new dbDelete("licenceholiday");
				$qry->setParam("holidayid",$holidayid);
				if (!$qry->execute($con)) {
					$messages[] = $qry->getError();
				} else {
					#return to licence page 
					$messages[] = DELETED;
					headerLocation ("licence.php?accountid=$accountid&licenceid=$licenceid&tasklistid=$tasklistid", $messages);
				}
		} 
	}
	
} else {
	headerLocation("$rootUrl/index.php",false);
	
	//trigger_error("Access Denied",E_USER_WARNING);
}

if (isAllowed("editAccount")) { 
	$this_page = "accounts.php";
} else {
	$this_page = "tasks.php";
}
#Include layout page
include("layout.php");
?>