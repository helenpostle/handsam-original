<?php
/**
* User editing functionality
*
* For admin and super admin users
* content: system_useredit.php, system_userlist.php
* boxes: none
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$this_page = "system_users.php";

require("../shared/startPage.php");

$pageFiles->addFunc('email_alert_checkbox');
$pageFiles->addFunc('user_table');
$pageFiles->addFunc('user');

$pageFiles->includePhp();

#check if have requested a specific user
$userid = getIntFromRequest("userid");
$aid = getIntFromRequest("accountid");
$return = getStrFromRequest("return");

$usertype = getStrFromRequest("usertype");

$username = getStrFromRequestSession("username");
$firstname = getStrFromRequestSession("firstname");
$lastname = getStrFromRequestSession("lastname");


//add usertype to session
if ($usertype == null) {
	$usertype = '';
	if (isset($_SESSION['usertype'])) {
		$usertype = $_SESSION['usertype']; 
	} else {
		$_SESSION['usertype'] = $usertype;
	}
	
} else {
	$_SESSION['usertype'] = $usertype;
}


if ($usertype == '' || $usertype == 'All') {
	$usertype_qry = "";
	$usertype_title = "All Usertypes ";
} else {
	$usertype_qry = " and usr_usrtype.usertype = '$usertype' ";
	$usertype_title = "Usertype: $usertype ";
}

//username filter
if ($username != "") {
	$username_filter = $username;
	$username_filter_qry = " and username like '{$username}%' ";
} else {
	$username_filter_qry = "";
	$username_filter = "";
}

//first name filter
if ($firstname != "") {
	$firstname_filter = $firstname;
	$firstname_filter_qry = " and firstname like '{$firstname}%' ";	
} else {
	$firstname_filter_qry = "";
	$firstname_filter = "";
}

//last name filter
if ($lastname != "") {
	$lastname_filter = $lastname;
	$lastname_filter_qry = " and lastname like '{$lastname}%' ";	
} else {
	$lastname_filter_qry = "";
	$lastname_filter = "";
}


$cluster_qry="";
//$cluster_filter_qry="";
if(isset($clsUS->account_clusters) && count($clsUS->account_clusters)>0 && isset($core_conf['account_cluster_SA_access']) && $core_conf['account_cluster_SA_access'])
{
  $cluster_qry = " and account.cluster_id in (".implode(',',$clsUS->account_clusters).") ";
  //$cluster_filter_qry=" and c.id in (".implode(',',$clsUS->account_clusters).") ";
}

if ($userid !== null && $userid !== 0 && (isAllowed("editAccount"))) {

	#we can do this if it is ourself or we have permission to edit other users.
	
	$sqlUser = "select usr.*, usr_usrtype.usertype, account.accountname from usr left join account on usr.accountid = account.accountid left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.userid = $userid $cluster_qry";
	
	//$sqlUser = "select usr.*, usr_usrtype.usertype, usr_usrtype.tasklistid, account.accountname from usr left join account on usr.accountid = account.accountid left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.userid = $userid";
	$rsUser = getRS($con,$sqlUser);
	$rowUser = getRow($rsUser); 
	//get account name and display
  $title = "";
  $content="";
  if ($rowUser)
  {
    $content = "system_useredit.php";
    $rsAccount = getRS($con,"select accountname from account  where accountid =".$rowUser['accountid']);
    $rowAccount = getRow($rsAccount); 

    $title = "Edit User: {$rowAccount["accountname"]}";
  }
	
	if ((isAllowed("editAccount")) || (isAllowed("editUsers") && $userid == 0) || (isAllowed("editUsers") && $rowUser["accountid"] == $clsUS->accountid) || $self) {
	
		$accountid = $rowUser["accountid"];
	
		if (buttonClicked("save")) {
			if ($userid == 0) {
			} else {
				#check that a user doesn't exist with this name
				$rsCheck = getRS($con,"select * from usr where username = '".$_POST["username"]."' and userid != $userid");
				if (getRow($rsCheck)) {
					$messages[] = USERNAME_IN_USE;	
				} else {
					$qry = new dbUpdate("usr");
					$qry->setParam("userid",$userid);
				}
			}
			
			if (isset($qry)) {
				
				if (isAllowed("editUsers")) {	
					$qry->setReqEmailVal("username",$_POST["username"],"Email Username");
					$qry->setReqEmailVal("email",$_POST["username"],"Email Username");
				}
				$qry->setReqStringVal("firstname",$_POST["firstname"],"First Name");
				$qry->setReqStringVal("lastname",$_POST["lastname"],"Last Name");
                                
                                if($core_conf['user_payroll'])
                                    $qry->setReqStringVal('payroll', $_POST['payroll'], 'Payroll Number');
				
				if ($clsUS->userid != $userid) {
					$qry->setReqStringVal("state",$_POST["state"],"State");
				}
				
				$qry->setAudit($clsUS->userid);
				
				if ($qry->execute($con)) {
										
					if ($rowUser['accountid'] != 1) {
						
						
						//now deal with normal usertypes
						//loop through each instaled tasklist
						if (in_array("tasklist", $installed_modules)) {
							$qryTL = "select distinct tasklist.tasklistid,tasklist.tasklistname from tasklist left join accountlicence on tasklist.tasklistid = accountlicence.tasklistid where accountlicence.accountid = $accountid AND accountlicence.state = 'ACTIVE' AND accountlicence.end_date > NOW()";
							$rsTL = getRS($con,$qryTL);
							
							//execSQL($con,"delete from usr_usrtype where userid = $userid");
							while ($rowTL = getRow($rsTL)) {
								$tl_qry = " select usertypeid as a, usertype as b from usr_usrtype where userid = $userid and tasklistid = {$rowTL["tasklistid"]}";
								$usrtype = getStrFromRequest("usertype_".$rowTL["tasklistid"]);	
								if ($usrtype == "None") $usrtype = "";
								$tl_ut = array();
								$tl_ut[] = $usrtype;
								$tlUsertype = new dbMultiAssign($tl_ut, $tl_qry, $con);
								//loop through the del items array and del using the key value				
							    foreach ($tlUsertype->del as $del_id => $item ) {
									$qryTL = new dbDelete("usr_usrtype");
									$qryTL->setParam("usertypeid",$del_id);
									$qryTL->setParam("userid",$userid);
									if (!$qryTL->execute($con)) {
										$messages[] = $qryTL->getError();
									}
								}
								
								//loop through the add array and insert usertype for this SA user
							    foreach ($tlUsertype->add as $add_id ) {
								    if ($add_id != "") {
										$qryTL = new dbInsert("usr_usrtype");
										$qryTL->setReqStringVal("usertype",$add_id,"usertype");
										$qryTL->setReqNumberVal("userid",$userid,"SA userid");
										$qryTL->setReqNumberVal("tasklistid",$rowTL["tasklistid"],"tasklist id");
										if (!$qryTL->execute($con)) {
											$messages[] = $qryTL->getError();
										}
									}
								}
								
								
							}
						}
						
						//now loop through each app except tasks app
						$qryTL = "select distinct appid from accountlicence where accountlicence.accountid = $accountid and appid != 1";
						$rsTL = getRS($con,$qryTL);
						$ap_ut = array();
						while ($rowTL = getRow($rsTL)) {
							//build array of posts:
							$usrtype = getStrFromRequest("app_usertype_".$rowTL["appid"]);	
							if ($usrtype == "None") $usrtype = "";
							$ap_ut[] = $usrtype;
							//execSQL($con,"insert into usr_usrtype (userid, usertype) values ($userid,'$usrtype')");							
						}
						
						$ap_qry = "select usertypeid as a, usertype as b from usr_usrtype where userid=$userid and tasklistid is null";
						$apUsertype = new dbMultiAssign($ap_ut, $ap_qry, $con);
						//loop through the del items array and del using the key value				
					    foreach ($apUsertype->del as $del_id => $item ) {
							$qryTL = new dbDelete("usr_usrtype");
							$qryTL->setParam("usertypeid",$del_id);
							$qryTL->setParam("userid",$userid);
							if (!$qryTL->execute($con)) {
								$messages[] = $qryTL->getError();
							}
						}
						
						//loop through the add array and insert usertype for this SA user
					    foreach ($apUsertype->add as $add_id ) {
							if ($add_id != "") {
								$qryTL = new dbInsert("usr_usrtype");
								$qryTL->setReqStringVal("usertype",$add_id,"usertype");
								$qryTL->setReqNumberVal("userid",$userid,"SA userid");
								if (!$qryTL->execute($con)) {
									$messages[] = $qryTL->getError();
								}
							}
						}
						
						
						// save the user's job role(s)
						if($core_conf['jobroles'] === true) {
							if(!isset($_POST['role_id']) || !is_array($_POST['role_id'])) {
								$_POST['role_id'] = array("");
							}
							$jr_qry = " SELECT id as a, jobrole_id as b FROM core_usr_jobrole WHERE usr_id = $userid";
							$jr = new dbMultiAssign($_POST['role_id'], $jr_qry, $con);
							//loop through the del items array and del using the key value				
						    foreach ($jr->del as $del_id => $item ) {
								$qryJR = new dbDelete("core_usr_jobrole");
								$qryJR->setParam("id",$del_id);
								$qryJR->setParam("usr_id",$userid);
								if (!$qryJR->execute($con)) {
									$messages[] = $qryJR->getError();
								}
							}
							
							//loop through the add array and insert usertype for this SA user
						    foreach ($jr->add as $add_id ) {
							    if ($add_id != "") {
									$qryJR = new dbInsert("core_usr_jobrole");
									$qryJR->setReqNumberVal("jobrole_id",$add_id,"jobrole id");
									$qryJR->setReqNumberVal("usr_id",$userid,"jobrole userid");
									if (!$qryJR->execute($con)) {
										$messages[] = $qryJR->getError();
									}
								}
							}
						}
						
						// /user job role
					}		
								
					$messages[] = SAVED;
					$contentId = "contentWide";
					$content = "system_userlist.php";
						
				} else {
					$messages[] = $qry->getError();	
					//$messages[] = $qry->getSql();	
				}
			}
		} else if (buttonClicked("delete")) {
			#cannot delete self
			$qry = new dbUpdate("usr");
			$qry->setParam("userid",$userid);
			$qry->setReqStringVal("state","DELETED","State");
			if (!$qry->execute($con)) {
				$messages[] = $qry->getError();
			} else {
				#change content to list
				$messages[] = DELETED;
				$contentId = "contentWide";
				$content = "system_userlist.php";
				$title = "User List";
			}
	 	} else if (buttonClicked("cancel")) {
			$contentId = "contentWide";
			$content = "system_userlist.php";
			$title = "User List";
		}  
	} else {
		$messages[] = NOT_EDIT_USER;
		headerLocation("system_users.php", $messages);
	}
	
} else if (isAllowed("editAccount")) {
	#no userid set - are we allowed to view the system_userlist?
	$contentId = "contentWide";
	$content = "system_userlist.php";
	$title = "User List";	
	
	


} else {
	#trying to do something we shouldn't! throw error so it is logged.	
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);

}

include("layout.php");