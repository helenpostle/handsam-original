<?php
if ($clsUS->accountlogo != "" && !isAllowed("editAccount")) {
	echo "<div id=\"account_logo\"><img src=\"$rootUrl/account_uploads/account_logo/".$clsUS->accountlogo."\"/></div>";
} else if (isAllowed("editAccount") && isset($accountid) && $accountid > 0) {
	//query to get logo filename
	$qryLogo = "SELECT accountlogo FROM account where account.accountid = ".$accountid;
	$rsLogo = getRS($con, $qryLogo);
	$rowLogo = getRow($rsLogo);
	
	if ($rowLogo['accountlogo'] != "" && file_exists("$rootPath/account_uploads/account_logo/{$rowLogo['accountlogo']}")) {
		echo "<div id=\"account_logo\" class=\"clearfix\"><img src=\"$rootUrl/account_uploads/account_logo/{$rowLogo['accountlogo']}\"/></div>";
	} else {
		echo "<div id=\"no_logo\"></div>";
	}
} else {
	echo "<div id=\"no_logo\"></div>";
}?>
<div id="loggedin">
	<a id="logout1" href="?<?php echo $_SERVER["QUERY_STRING"];?>&amp;logout=true">logout</a>
	<h2>My Handsam</h2>
	<p class="username">Logged in as: <span><?php echo $clsUS->username; ?></span></p>
	<p class="account">Account: <span><?php echo $clsUS->accountname; ?></span></p>
	<p class="visits">Visits: <span><?php echo $clsUS->visits ?></span></p>
	<p class="last_visit">Last Visit: <span><?php echo displayDateTime($clsUS->lastvisit) ?></span></p>
	<!--//
	<p class="support">Technical Support:<br/><span>email: </span><a href="mailto:info@handsam.co.uk">info@handsam.co.uk</a> | <span>tel: </span>01785220763 | <span>mob:</span> 07740980391</p>//-->
</div>