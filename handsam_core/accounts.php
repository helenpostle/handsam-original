<?php
/**
* Account editing functionality
* 
* content: accountEdit.php, accountList.php
* boxes:  accountUserList.php, accountlicenceList.php
* 
*/

$this_page = "accounts.php";
/**
 * Include basic stuff
 */
$secure = true;

require("../shared/startPage.php");

####make this a secure page
$pageFiles->addFunc("jquery_ui");

$pageFiles->addFunc("autocomplete_authority");
$pageFiles->addFunc("account_comments");
$pageFiles->addFunc("accountlist_tables");
$pageFiles->addFunc('email');

$pageFiles->includePhp();

#check if have requested a specific account and get other params
$accountid = getIntFromRequest("accountid2");
if ($accountid === null) $accountid = getIntFromRequest("accountid");
$licenceid = getIntFromRequest("licenceid");
$supplyTask = getStrFromRequest("supplyTask");
$a_comment = getStrFromRequest("comment");
$logo_id = getStrFromRequest("logo_id");

$tasklist = getIntFromRequest("tasklist");
$appid = getIntFromRequest("appid");
$manfilter = getStrFromRequest("manfilter");
$sch_type_filter = getStrFromRequest("sch_type_filter");
$sch_status_filter = getStrFromRequest("sch_status_filter");
$school_pupils_filter = getStrFromRequest("school_pupils_filter");
$authority_filter = getStrFromRequest("authority_filter");
$acc_cluster_filter = getStrFromRequest("acc_cluster");

//add appid to session
if ($appid != "" || $appid === 0) {
	$_SESSION['appid'] = $appid;
} else {
	if (isset($_SESSION['appid'])) { 
		$appid = $_SESSION['appid'];
	} 
}
//add tasklist to session
if ($tasklist != "" || $tasklist === 0) {
	$_SESSION['tasklist'] = $tasklist;
} else {
	if (isset($_SESSION['tasklist'])) { 
		$tasklist = $_SESSION['tasklist']; 
	} 
}

//manager filter processing
if ($manfilter != "") {
	$_SESSION['manager_filter'] = $manfilter;
} else {
	if (isset($_SESSION['manager_filter'])) { 
		$manfilter = $_SESSION['manager_filter']; 
	} 
}

if ($manfilter == "Mine") {
	$account_manager_qry = " and account_manager={$clsUS->userid} ";
	$account_manager_title = ": My Accounts ";
} else {
	$account_manager_qry = "";
	$account_manager_title = ": All Accounts ";
}

//school type filter processing
if ($sch_type_filter != "") {
	$_SESSION['sch_type_filter'] = $sch_type_filter;
} else {
	if (isset($_SESSION['sch_type_filter'])) { 
		$sch_type_filter = $_SESSION['sch_type_filter']; 
	} 
}

if ($sch_type_filter == "ALL" || $sch_type_filter == "") {
	$sch_type_filter_qry = "";
	$sch_type_filter_title = "";
} else {
	$sch_type_filter_qry = " and school_type='$sch_type_filter' ";
	$sch_type_filter_title = " : {$school_type[$sch_type_filter]}";
}

//school status filter processing
if ($sch_status_filter != "") {
	$_SESSION['sch_status_filter'] = $sch_status_filter;
} else {
	if (isset($_SESSION['sch_status_filter'])) { 
		$sch_status_filter = $_SESSION['sch_status_filter']; 
	} 
}

if ($sch_status_filter == "ALL" || $sch_status_filter == "") {
	$sch_status_filter_qry = "";
	$sch_status_filter_title = "";
} else {
	$sch_status_filter_qry = " and school_status='$sch_status_filter' ";
	$sch_status_filter_title = " : {$school_status[$sch_status_filter]}";
}

//school pupils filter processing
if ($school_pupils_filter != "") {
	$_SESSION['school_pupils_filter'] = $school_pupils_filter;
} else {
	if (isset($_SESSION['school_pupils_filter'])) { 
		$school_pupils_filter = $_SESSION['school_pupils_filter']; 
	} 
}

if ($school_pupils_filter == "ALL" || $school_pupils_filter == "") {
	$sch_pupils_filter_qry = "";
	$sch_pupils_filter_title = "";
} else {
	$sch_pupils_filter_qry = " and school_pupils='$school_pupils_filter' ";
	$sch_pupils_filter_title = " : {$school_pupils[$school_pupils_filter]}";
}

//school pupils filter processing
if ($authority_filter != "") {
	$_SESSION['authority_filter'] = $authority_filter;
} else {
	if (isset($_SESSION['authority_filter'])) { 
		$authority_filter = $_SESSION['authority_filter']; 
	} 
}

if ($authority_filter == "" || $authority_filter == "ALL") {
	$authority_filter_qry = "";
	$authority_filter_title = "";
} else {
	$authority_filter_qry = " and authority='$authority_filter' ";
	$authority_filter_title = " : $authority_filter";
}


//acc name filter

if ($core_conf['account_name_filter'] && isset($_SESSION['acc_name']) && $_SESSION['acc_name'] != "") {
	$acc_name_filter_qry = " and accountname like '{$_SESSION['acc_name']}%' ";
	$acc_name_filter = $_SESSION['acc_name'];
} else {
	$acc_name_filter_qry = "";
	$acc_name_filter = "";
}


//acc cluster filter
//school pupils filter processing
if ($acc_cluster_filter != "") {
	$_SESSION['acc_cluster'] = $acc_cluster_filter;
} else {
	if (isset($_SESSION['acc_cluster'])) { 
		$acc_cluster_filter = $_SESSION['acc_cluster']; 
	} 
}

if ($core_conf['account_cluster_filter'] && isset($_SESSION['acc_cluster']) && $_SESSION['acc_cluster'] != "" && $_SESSION['acc_cluster'] != 0) {
	$acc_cluster_filter_qry = " and cluster_id = {$_SESSION['acc_cluster']}";
	$acc_cluster_filter = $_SESSION['acc_cluster'];
} else {
	$acc_cluster_filter_qry = "";
	$acc_cluster_filter = "";
}



//acc postcode filter
if (isset($_SESSION['acc_postcode']) && $_SESSION['acc_postcode'] != "") {
	$acc_postcode_filter_qry = " and postcode like '{$_SESSION['acc_postcode']}%' ";
	$acc_postcode_filter = $_SESSION['acc_postcode'];
} else {
	$acc_postcode_filter_qry = "";
	$acc_postcode_filter = "";
}


//acc dfe filter
if (isset($_SESSION['acc_dfe']) && $_SESSION['acc_dfe'] != "") {
	$acc_dfe_filter_qry = " and school_dfe like '{$_SESSION['acc_dfe']}%' ";
	$acc_dfe_filter = $_SESSION['acc_dfe'];
} else {
	$acc_dfe_filter_qry = "";
	$acc_dfe_filter = "";
}

$new_filter_qry = $account_manager_qry.$sch_type_filter_qry.$sch_status_filter_qry.$sch_pupils_filter_qry.$authority_filter_qry.$acc_name_filter_qry.$acc_postcode_filter_qry.$acc_dfe_filter_qry.$acc_cluster_filter_qry;

$new_filter_title = $account_manager_title;
$title = "";

//set account cluster query parts for use in list and edit content pages.
//restricts the access to accounts in a cluster if enabkled in config.
$cluster_qry="";
$cluster_filter_qry="";
if(isset($clsUS->account_clusters) && count($clsUS->account_clusters)>0 && isset($core_conf['account_cluster_SA_access']) && $core_conf['account_cluster_SA_access'])
{
  $cluster_qry = " and account.cluster_id in (".implode(',',$clsUS->account_clusters).") ";
  $cluster_filter_qry=" and c.id in (".implode(',',$clsUS->account_clusters).") ";
}

if ($accountid > 0) {
	$pageFiles->addJsVar('item_id', $accountid);
	$pageFiles->addJsVar('sessionid', session_id());
	$pageFiles->addFunc('account_logo');
	$pageFiles->addFunc('email_alert_checkbox');
}

//now inclusde all php files here
//$pageFiles->includePhp();

#we can do this if  we have permission to edit accounts.
if (($accountid > 0 && isAllowed("editAccount")) || ($accountid === null && isAllowed("editAccount")) || ($accountid === 0 && isAllowed("handsamOnly")) || ($accountid === 0 && isAllowed("editAccount") && $core_conf['add_accounts'] == true) ) {
		
	if ($supplyTask == "yes") {
		//add tasks to account
		$tasks = "SELECT * FROM account left join accountlicence on account.accountid = accountlicence.accountid left join task on accountlicence.tasklistid = task.tasklistid where account.accountid = ".$accountid." order by task.taskmonth, task.taskday";
		$taskResult = getRS($con, $tasks);
		$execution_error = false;
		while ($rowTask = getRow($taskResult)) {
			$qry = new dbInsert("accounttask");
			$qry->setReqNumberVal("taskid",$rowTask["taskid"],"Task id");
			$qry->setReqNumberVal("accountid",$accountid,"Account id");
			$qry->setReqNumberVal("categoryid",$rowTask["categoryid"],"Category id");
			$qry->setReqNumberVal("licenceid",$licenceid,"licence id");
			$qry->setReqNumberVal("taskmonth",$rowTask["taskmonth"],"Task month");
			$qry->setReqNumberVal("taskday",$rowTask["taskday"],"Task day");
			$qry->setReqStringVal("tasktext",$rowTask["tasktext"],"Task text");		
			$qry->setAudit($clsUS->userid);
			if (!$qry->execute($con)) {
				$execution_error = true;
				$messages[] = ERROR_SAVED." to task id:".$rowTask["taskid"];
			}
		}
		if ($execution_error == false) {
			$messages[] = TASKS_SUPPLIED;
		}
		//update licence state to active
		$qry = new dbUpdate("accountlicence");
		$qry->setParam("licenceid",$licenceid);
		$qry->setReqStringVal("state","ACTIVE","state");
		if ($qry->execute($con)) {
			$messages[] = licence_ACTIVE;
		}
	}


	if ($accountid !== 0) {
		//get account  title for page title
		$qry = "SELECT accountname FROM account where accountid='$accountid'";
		$result = getRS($con, $qry);
		$row = getRow($result);
		$title = displayText($row['accountname']);
	} else {
		$title = "New Account";
	}	
	
	if (buttonClicked("cancel")) {
		headerLocation("accounts.php?tasklist=$tasklist", true);
	} else {
	
	
		if ($accountid !== null) {
      #display edit form
			$content = "accountedit.php";
      
      //query for use in accountedit.php content page.
			$rsAccount = getRS($con,"select account.*, accountlicence.tasklistid as tasklistid from account left join accountlicence on account.accountid = accountlicence.accountid where account.accountid = $accountid $cluster_qry");
      
      $rowAccount = getRow($rsAccount);
      
      //if ($rowAccount)
      //{
        
			
        
        if (buttonClicked("save")) {
          #check that an account list doesn't exist with this name
          $rsCheck = getRS($con,"select * from account where accountname = '".escapeString($_POST["accountname"])."' and accountid != '$accountid'");
          if (getRow($rsCheck)) {
            $messages[] = ACCOUNTNAME_IN_USE;	
          } else {

            if ($accountid == 0) {
              $qry = new dbInsert("account");
            } else {
              $qry = new dbUpdate("account");
              $qry->setParam("accountid",$accountid);
            }

            // "post accountname is : ".$_POST["accountname"];

            if (isset($qry)) {

              //check that authority is in authority table:
              if (isset($_POST['authority']) && trim($_POST['authority']) != "") {
                $auth_qry = "select auth_name from authority where auth_name = '".escapeString($_POST['authority'])."'";
                $auth_rs = getRS($con, $auth_qry);

                if (getResultRows($auth_rs) == 0) {
                  $qry->failError .= "Authority name not recognised. Please try again or leave blank";
                } else {
                  $qry->setReqStringVal("authority",$_POST["authority"],"Authority");
                }
              }

              //new account contact field
              if (isset($_POST['account_contact'])) $qry->setStringVal("account_contact",$_POST["account_contact"],"Account Contact");

              //new account manager field
              if (isset($_POST['account_manager']) && $_POST["account_manager"] > 0) {
                $qry->setNumberVal("account_manager",$_POST["account_manager"],"Account manager");
              }

              //* new 2nd account manager field
              if (isset($_POST["account_manager_2"]) && $_POST["account_manager_2"] > 0) {
                $qry->setNumberVal("account_manager_2",$_POST["account_manager_2"],"2nd Account manager");
              }
              // */

              $qry->setReqStringVal("accountname",$_POST["accountname"],"Account name");
                                                  if($core_conf['cost_code'])
                                                      $qry->setReqStringVal('cost_code', $_POST['cost_code'], 'Cost Code');

              if ($accountid !=1) {
                $qry->setReqStringVal("school_type",$_POST["school_type"],"School Type");
                if (isset($_POST["school_pupils"])) $qry->setReqStringVal("school_pupils",$_POST["school_pupils"],"School Pupils");
                //if (isset($core_conf['school_cluster']) && $core_conf['school_cluster']) $qry->setReqStringVal("school_cluster",$_POST["school_cluster"],"School Cluster");

                if (isset($core_conf['school_cluster']) && $core_conf['school_cluster']) 
                {
                  if ($_POST["cluster_id"]>0)
                  {
                    $qry->setStringVal("cluster_id",$_POST["cluster_id"],"Account Cluster");
                  }
                  else
                  {
                    $qry->setNullVal("cluster_id"); 
                  }
                }


                $qry->setStringVal("school_dfe",$_POST["school_dfe"],"School Dfe Number");
                $qry->setReqStringVal("state",$_POST["state"],"state");
              }
              $qry->setReqStringVal("school_status",$_POST["school_status"],"School Status");

              $qry->setReqStringVal("address1",$_POST["address1"],"Address 1");
              $qry->setStringVal("address2",$_POST["address2"],"Address 2");
              $qry->setReqStringVal("town",$_POST["town"],"Town");
              $qry->setStringVal("county",$_POST["county"],"County");
              $qry->setReqStringVal("postcode",$_POST["postcode"],"Postcode");
              $qry->setReqStringVal("country",$_POST["country"],"Country");
              $qry->setReqStringVal("email",$_POST["email"],"Email");
              $qry->setReqStringVal("phone",$_POST["phone"],"phone");
              //$qry->setReqStringVal("tasklist",$_POST["tasklist"],"Task list");


              $qry->setAudit($clsUS->userid);

              //echo "SQL: ".$qry->getSQL();

              if ($qry->execute($con)) {


                if ($accountid == 0) {
                  $accountid = $qry->getNewID();
                  //now check for an initial comment if this is a new account
                  if (isset($_POST['comment']) && trim($_POST['comment'] != "")) {
                    $qry_comment = new dbInsert("account_comments");
                    $qry_comment->setAudit($clsUS->userid);
                    $qry_comment->setStringVal("commenttext",$_POST["comment"],"Comment");
                    $qry_comment->setStringVal("accountid",$accountid,"acountid");
                    if (!$qry_comment->execute($con)) {
                      $messages[] = $qry_comment->getError();	
                    }
                  }

                }
                $messages[] = SAVED;
                $content = "accountedit.php";
              } else {
                $messages[] = $qry->getError();	
              }
            }
          }
        } else if (buttonClicked("delete") && $accountid != 1) {

            $qry = new dbUpdate("account");
            $qry->setParam("accountid",$accountid);
            $qry->setReqStringVal("state","DELETED","State");
            if (!$qry->execute($con)) {
              $messages[] = $qry->getError();
            } else {
              #change content to list
              $messages[] = DELETED;
              $content = "accountlist.php";
              $title = "Account list $new_filter_title";
              $contentId = "contentWide";
            }
        } else if (buttonClicked("add_tasklist") && $_POST["tasklistid"] > 0) {
          $start_date = strftime("%a, %d %b %Y",time());
          $end_date = strftime("%a, %d %b %Y",mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1));

          $n_qry = new dbInsert("accountlicence");
          $n_qry -> setReqNumberVal("accountid",$accountid,"Account id");
          $n_qry->setReqNumberVal("tasklistid",$_POST["tasklistid"],"Task list");	
          $n_qry->setReqStringVal("appid",1,"App ID");	
          $n_qry->setReqDateVal("end_date", $end_date, "End date");
          $n_qry->setReqDateVal("start_date", $start_date, "Start date");
          $n_qry->setAudit($clsUS->userid);
          if ($n_qry->execute($con)) {
            $messages[] = SAVED;
            $content = "accountedit.php";

          } else {
            $messages[] = $n_qry->getError();	
          }
        } else if (buttonClicked("add_app") && $_POST["appid"] > 0) {
          $start_date = strftime("%a, %d %b %Y",time());
          $end_date = strftime("%a, %d %b %Y",mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1));

          $n_qry = new dbInsert("accountlicence");
          $n_qry -> setReqNumberVal("accountid",$accountid,"Account id");
          $n_qry->setReqStringVal("appid",$_POST["appid"],"App ID");	
          $n_qry->setReqDateVal("end_date", $end_date, "End date");
          $n_qry->setReqDateVal("start_date", $start_date, "Start date");
          $n_qry->setAudit($clsUS->userid);
          if ($n_qry->execute($con)) {
            $messages[] = SAVED;
            $content = "accountedit.php";

          } else {
            $messages[] = $n_qry->getError();	
          }
        } else if(buttonClicked("reset_pass")) {
          $content = "accountedit.php";
          //now select all users for this account and set new passwords
          $userQry  = "select username from usr where accountid  = $accountid and state = 'ACTIVE'";
          $userRs = getRS($con, $userQry);
          While ($userRow = getRow($userRs)) {
            $passwd = new clsUserPassword($homePath, $password_arr);
            $messages[] = $passwd->emailPassword($userRow['username'], $con, $adminMail); 
          }
        } 
      //}
			
		} else {
			#no accountid set - so view accountlist instead?
			$content = "accountlist.php";
			
      //bit naughty - this is some tasklist module functionality - so accounts can bee filtered by tasklist.			
			if (in_array("tasklist", $installed_modules) && $appid == "1") {
				if ($tasklist > 0) {
					$tasklist_qry = " and accountlicence.tasklistid = $tasklist ";
					$tasklist_title = "";
				} else {
					$tasklist_qry = "";
					$tasklist_title = "All Tasklists";
				}

				$qry = "SELECT account.school_dfe, account.cost_code, account.accountid,account.accountname, account.address1,account.town, account.postcode, account.state, tasklist.tasklistname, max(end_date) as licence_end";
				$qry .= " FROM account left join accountlicence on account.accountid = accountlicence.accountid left join tasklist on accountlicence.tasklistid = tasklist.tasklistid ";
				//$qry .= " where account.accountid != 1 and account.state != 'DELETED' and appid=1 $tasklist_qry $new_filter_qry group by account.school_dfe, account.accountid,account.accountname, account.address1, account.town, account.postcode, account.state, tasklist.tasklistname order by account.accountname";		
				$qry .= " where  account.state != 'DELETED' and appid=1 $tasklist_qry $new_filter_qry $cluster_qry group by account.school_dfe, account.accountid,account.accountname, account.address1, account.town, account.postcode, account.state, tasklist.tasklistname order by account.accountname";		
				$result = getRS($con, $qry);
			
				if ($tasklist > 0) {
					$row = getRow($result);
					$tasklist_title = $row['tasklistname'];
					resetRS($result);
				}
	
			} else {
				$qry = "SELECT account.school_dfe, account.cost_code, account.accountid,account.accountname, account.address1,account.town, account.postcode, account.state, max(end_date) as licence_end";
				$qry .= " FROM account left join accountlicence on account.accountid = accountlicence.accountid ";
				//$qry .= " where account.accountid != 1 and account.state != 'DELETED' $new_filter_qry ";
				$qry .= " where account.state != 'DELETED' $new_filter_qry $cluster_qry ";
				if ($appid > 0) $qry .= " and accountlicence.appid = $appid ";
				$qry .= " group by account.school_dfe, account.accountid,account.accountname, account.address1, account.town, account.postcode, account.state order by account.accountname";
			}
			
			
			$result = getRS($con, $qry);
			

			
			//$title = "Account list : $tasklist_title";
			$title = "Account list $new_filter_title";
			$contentId = "contentWide";	
		}
		
		#display list of account users in side box if editing account.
		if ($accountid > 0 && $content == "accountedit.php" && $rowAccount) {
			
			$pageFiles->box_priority = true;
			$pageFiles->addBox("$rootPath/handsam_core/boxes/box.accounts.billing_date.php");
			if($core_conf['key_holder']) $pageFiles->addBox("$rootPath/handsam_core/boxes/box.accountKeyHolder.php");
			$pageFiles->addBox("$rootPath/handsam_core/boxes/box.accountUserList.php");
			if ($accountid != 1) {
				$pageFiles->addBox("$rootPath/handsam_core/boxes/box.accounts.reset_all_password.php");
			}

			//$box[] = "box.accounts.billing_date.php";
			//$box[] = "box.accountUserList.php";
			//add app box - only display if there is an app that hasnn't been selected yet
            
            $installed_appids = implode(',',array_flip($installed_modules));
			$qry_apps = "select appid as a, appname as b from apps  where appid != 1 and appid not in (SELECT appid FROM accountlicence WHERE accountid = $accountid and appid > 0 and state !='DELETED') and appid in($installed_appids) ";
			
			//echo $qry_apps;
			
			$rsApps = getRS($con,$qry_apps);
			$count_apps = getResultRows($rsApps);
			if ($count_apps > 0) {
				$pageFiles->addBox("$rootPath/handsam_core/boxes/box.accounts.add_app.php");
			}
			
			$reverse_modules = array_reverse($installed_modules);
			foreach ($reverse_modules as $mod) {
				if (file_exists("$rootPath/modules/$mod/shared/account_inc.php")) require_once("$rootPath/modules/$mod/shared/account_inc.php");	
			}
			
			$box[] = "box.accounts.acc_app_licence.php";

		}	

	}
} else {
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);
}

#Include layout page
include("layout.php");
?>