<?php
/**
* Standard Index Page
*/
//$this_page = "index.php";
$content = "";
//include basic stuff
$secure = true;
require_once("../shared/startPage.php");
$this_page = "editLinks.php";


//make this a secure page
$pageFiles->addFunc('handsam_uploads');


$pageFiles->includePhp();
//specify page content



if (isAllowed("uploadHandsamDocs") || isAllowed("uploadAdminDocs") || isAllowed("uploadUserDocs")) {
	$title = "HandSaM File Upload Manager";		
	$userid = $clsUS->userid;
	$accountid = $clsUS->accountid;
	$linkid = setQSInt("linkid");
	$filetype = getStrFromRequest("filetype");
	$contentid = setQSInt("contentid");
	$edittype = getStrFromRequest("edit");
	if ($edittype != "page") {
		$edittype = "task";
	}

    if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
        $count_col = "t.taskid";	
    } else {
        $count_col = "t.accounttaskid";	
    }

    $rsLinks = getRS($con,"select l.title, l.url, l.linkid, l.linkcategory, l.description, l.created, l.edited, l.created_by, l.edited_by, mid, linktype, count(".$count_col.") as cnt FROM links l left join task_links t on l.linkid = t.linkid where l.linkid = $linkid group by l.title, l.url, l.linkid, l.linkcategory, l.description, l.created, l.edited, l.created_by, l.edited_by, mid, linktype");
    $rowLink = getRow($rsLinks);

    //extra query to check for lotc links. If this is an lotc link, can't delete

    $qry_lotc = "select count(linkid) as cnt from lotc_links where linkid = $linkid";
    $rsLotc = getRS($con, $qry_lotc);
    $rowLotc = getRow($rsLotc);
    
    if ($rowLink['mid'] > 0 && $rowLink['linktype'] == 'handsamDoc') {
        $content = "view_link.php";
    } else {
        $content = "editLinks.php";
    }
      
#for passing page and contentid between pages for adding links/files to content webpages
	if (isAllowed("editContent")) {
		$contentIdLink = "&contentid=".$contentid."&edit=".$edittype;
	} else {
		$contentIdLink = "";
	}
	if (isAllowed("editTask") && $filetype != "handsamDoc") {
		$accountid = getIntFromRequest("accountid");
	}
	if (buttonClicked("cancel")) {
		#return to file list
		if ($filetype == "adminDoc" && isAllowed("uploadHandsamDocs")) {
			headerLocation("linksList.php?filetype=$filetype&accountid=$accountid".$contentIdLink,false);
		} else {
			headerLocation("linksList.php?filetype=$filetype".$contentIdLink,false);
		}
		
	} else {
	
	
		if ($linkid !== null) {
			
			if (buttonClicked("save") && $content == "editLinks.php") {
								
					if ($linkid == 0) {
						$qry = new dbInsert("links");
						$qry->setReqStringVal("linktype",$_POST["filetype"],"Filetype");
					} else {
						$qry = new dbUpdate("links");
						$qry->setParam("linkid",$linkid);
					}
				
					if (isset($qry)) {
						$qry->setReqStringVal("url",$_POST["url"],"URL");
						$qry->setReqStringVal("title",$_POST["title"],"Title");
						$qry->setReqNumberVal("accountid",$accountid,"Account id");
						$qry->setReqNumberVal("userid",$userid,"User id");
						if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
							$qry->setStringVal("linkcategory",$_POST["linkcategory"],"Link Category");
						}
						
						$qry->setAudit($clsUS->userid);
						
						if ($qry->execute($con)) {
							if ($linkid == 0) {
								$linkid = $qry->getNewID();	
							}
							$messages[] = SAVED;
							if ($filetype == "adminDoc" && isAllowed("uploadHandsamDocs")) {
								headerLocation("linksList.php?filetype=$filetype&accountid=$accountid".$contentIdLink,$messages);
							} else {
								headerLocation("linksList.php?filetype=$filetype".$contentIdLink,$messages);
							}
						} else {
							$messages[] = $qry->getError();	
						}
					}
			} else if (buttonClicked("delete")) {
				#first we need to check if this file is attached to anything
				$rsCheck = getRS($con,"select * from task_links where linkid = $linkid");
				$rsCheckContent = getRS($con,"select * from content_links where linkid = $linkid");
				if (getRow($rsCheck)) {
					$messages[] = LINK_ATTACHED_TO_TASK;	
				} else if (getRow($rsCheckContent)) {
					$messages[] = FILE_ATTACHED_TO_PAGE;	
				} else {
			
					$qry = new dbDelete("links");
					$qry->setParam("linkid",$linkid);
					$qry->setParam("accountid",$accountid);
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					} else {
						#change content to list
						$linkid = 0;
						$messages[] = DELETED;
						if ($filetype == "adminDoc" && isAllowed("uploadHandsamDocs")) {
							headerLocation("linksList.php?filetype=$filetype&accountid=$accountid".$contentIdLink,$messages);
						} else {
							headerLocation("linksList.php?filetype=$filetype".$contentIdLink,$messages);
						}
					}
				}
			} 
			
			
			
		}
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);
}

//include layout.
include ("layout_FB.php");

?>