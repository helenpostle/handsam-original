<?php
//function to display an image.

function displayImage($path) {

	if ($path === false) {
		header("HTTP/1.1 500 Internal Server Error");
		echo "No ID";
		exit(0);
	}
	
	if (!file_exists($path)) {
		header("HTTP/1.1 404 Not found");
		exit(0);
	}
	
	
	
	#get image type
	list($width, $height,$type) = getimagesize($path);
	
	switch ($type) {
		case 1:
		    $image=imagecreatefromgif($path);
			header('Content-Type: image/gif');
		    imagegif($image);
		    break;
		case 2:
		    $image=imagecreatefromjpeg($path);
			header('Content-Type: image/jpeg');
		    imagejpeg($image);
		    break;
		case 3:
		    $image=imagecreatefrompng($path);
			imagealphablending($image, true); // setting alpha blending on
			imagesavealpha($image, true); // save alphablending setting (important)	    break;
			header('Content-Type: image/png');
		    imagepng($image);
			break;
	}	
}
?>