<?php
//function to crop image. filepath = uploaded filepath; filename = filename  to be saved; $thumbFilePath = filepath for new image; w =width; h =height; 

function CropImage($filePath, $filename, $thumbFilePath, $w, $h) {
	global $cropQuality;
	$repeat_file=0;
	list($width, $height,$type) = getimagesize($filePath);
	if (!$width || !$height) {
		echo "ERROR:Invalid width or height";
		exit(0);
	}
	
	if ($type > 3 || $type < 1 ) {
		echo "ERROR:Please select a jpg, gif or png file. Filetype:$type";
		exit(0);
		return false;
	}
	if ($width<=$w && $height<=$h) {
		$new_width=$width;
		$new_height=$height;
	} else {
		if ($width<=$height) $x=$w/$width;
		if ($height<$width) $x=$h/$height;
		$new_width=$width*$x;
		$new_height=$height*$x;
	}
	
	
	switch ($type) {
		case 1:
		    $image=imagecreatefromgif($filePath);
		    $filetype = ".gif";
			//$image=imagecreatefromjpeg($filePath);
			if (!$image) {
				echo "ERROR:could not create image handle ".$filePath;
				exit(0);
			}
			$resized_image=imagecreatetruecolor($w,$h);
			imagealphablending($resized_image, false);
			/*
			if (!@imagefilledrectangle($resized_image, 0, 0, $new_width-1, $new_height-1, 0)) {	// Fill the image black
				echo "ERROR:Could not fill new image";
				exit(0);
			}
			*/
			
			// get and reallocate transparency-color
			$transindex = imagecolortransparent($image);
			if($transindex >= 0) {
				$transcol = imagecolorsforindex($image, $transindex);
				$transindex = imagecolorallocatealpha($resized_image, $transcol['red'], $transcol['green'], $transcol['blue'], 127);
				imagefill($resized_image, 0, 0, $transindex);
			}
			
			imagecopyresampled($resized_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
			
			// restore transparency
			if($transindex >= 0) {
				imagecolortransparent($resized_image, $transindex);
				for($y=0; $y<$new_height; ++$y) {
					for($x=0; $x<$new_width; ++$x) {
						if(((imagecolorat($resized_image, $x, $y)>>24) & 0x7F) >= 100) {
							imagesetpixel($resized_image, $x, $y, $transindex);
						}
					}
				}
			}
			
			// save GIF
			imagetruecolortopalette($resized_image, true, 255);
			imagesavealpha($resized_image, false);
					
			imagegif($resized_image, $thumbFilePath."/".$filename.".gif");
		    break;
		case 2:
		    $image=imagecreatefromjpeg($filePath);
		    $filetype = ".jpg";
			//$image=imagecreatefromjpeg($filePath);
			if (!$image) {
				echo "ERROR:could not create image handle ".$filePath;
				exit(0);
			}
			$resized_image=imagecreatetruecolor($w,$h);
			if (!@imagefilledrectangle($resized_image, 0, 0, $new_width-1, $new_height-1, 0)) {	// Fill the image black
				echo "ERROR:Could not fill new image";
				exit(0);
			}
			imagecopyresampled($resized_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
			//the 90 here is the new image quality. 
			imagejpeg($resized_image, $thumbFilePath."/".$filename.".jpg", 90);
		    break;
		case 3:
		    $image=imagecreatefrompng($filePath);
		    $filetype = ".png";
			//$image=imagecreatefromjpeg($filePath);
			if (!$image) {
				echo "ERROR:could not create image handle ".$filePath;
				exit(0);
			}
			$resized_image=imagecreatetruecolor($w,$h);
			imagealphablending($resized_image, false);
			if (!@imagefilledrectangle($resized_image, 0, 0, $new_width-1, $new_height-1, 0)) {	// Fill the image black
				echo "ERROR:Could not fill new image";
				exit(0);
			}
			imagecopyresampled($resized_image, $image, 0, 0, 0, 0, $new_width, $new_height, $width, $height);
			
			imagesavealpha($resized_image, true);
			
			//the 1 here is the new image quality. 0=high 9 = low
			imagepng($resized_image, $thumbFilePath."/".$filename.".png", 1);
		    break;
	}	

	imagedestroy($image);
	imagedestroy($resized_image);
	return $filetype;
}
?>