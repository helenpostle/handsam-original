<?php

/**
 * Some miscellaneous useful functions 
 *
 */
function getPageRoot() {
	//global $_SERVER;
	$script = $_SERVER["SCRIPT_NAME"];
	$reduced = substr($script, 0, strrpos($script, "/") + 1);
	return "http://".$_SERVER["SERVER_NAME"].$reduced;
}

//sends to this page but also saves message in session messages to be displayed on this new page
function headerLocation($url,$messages="") {
	//echo "I am passing messages to the session";
	if ($messages) {
		$_SESSION["session_messages"] = $messages;
	}
	header ("Location: $url");
	die();
}


function setQSInt($param) {
	$param = getIntFromRequest($param);
	if ($param == "") {
		$param = 0;
	}
	return $param;
}

#for writing url does nothing at the moment!

function displayLink($page, $id, $params, $title = "") {
	global $seo_urls;
	global $rootUrl;
	if ($seo_urls) {
		if ($title == "") {
			$title = "index";
		} else {
			//$title = str_replace(" ","_",$title);
			$trans = array(" " => "_","'" => "" ,"," => "", "." => "", "\"" => "");
			$title = strtr($title,$trans);
		}
		$title = urlencode($title);
		$result = $page."/".$id."/".$title.".html";
		if ($params != "") {
			$result .= "?".$params;
		}
	} else {
		$result = $page.".php?id=".$id;
		if ($params != "") {
			$result .= "&".$params;
		}	
		
	}
	return $rootUrl."/".$result;
}	

#for writing url does nothing at the moment!

function displayNormalLink($page, $id, $params, $title = "") {
	global $rootUrl;

	$result = $page.".php?id=".$id;
	if ($params != "") {
		$result .= "&".$params;
	}	
		
	return $rootUrl."/".$result;
}

//alternative to file_get_contents because od the fire wall isues with this

function h_file_get_contents($file) {
	global $fgc_host;
	if ($fgc_host == "localhost") {
		$data = file_get_contents($file);
	} else {
		$opts = array(
		  'http'=>array(
		    'header'=>"Host: $fgc_host\r\n" 
		  )
		);
		
		$context = stream_context_create($opts);
		
		
		$data = file_get_contents($file, false, $context);
	}
	return $data;
}
		
?>