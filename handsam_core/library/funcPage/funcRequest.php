<?php
/**
 * Get an integer value from the request if there is one.
 * Check POST first, then GET
 */
 
function getIntFromRequest($field, $default=null) {
	if (isset($_POST[$field])) {
		if (settype($_POST[$field],"integer")) {
			return $_POST[$field];
		} else {
			return $default;
		}
	} else if (isset($_GET[$field])) {
		if (settype($_GET[$field],"integer")) {
			return $_GET[$field];
		} else {
			return $default;
		}
	} else {
		return $default;
	}
}


function getIntFromRequestArr($field, $default=null)
{
  $arr = array();
  if (isset($_POST[$field])) {
    if(is_array($_POST[$field]))
    {
      foreach($_POST[$field] as $val)
      {
        if (settype($val,"integer")) {
          $arr[] = $val;
        } else {
          $arr[] = $default;
        }
      }
    }
  } 
  return $arr;
}

//same as below but without the mysql escape string. used for repopulating form fields so don't want to escape now as we'll esacape when entering into db.
function getFrmStrFromRequest($field, $return = null, $type="any") {
	global $allow_html_purifier;
	global $nohtml_page_purifier;
	global $get_req;
	global $post_req;
	//global $system_security;
	if (isset($_POST[$field]) && ($type=="any" || $type=="post")) {
		$req = $post_req->getVarSerString($field);
		$clean_html = $allow_html_purifier->purify( $req );		
		return $clean_html;
	} else if (isset($_GET[$field]) && ($type=="any" || $type=="get")) {
		$req = $get_req->getVarSerString($field);
		$clean_html = $nohtml_page_purifier->purify( $req );		
		return $clean_html;
	} else {
		return $return;
	}
}

function getStrFromRequest($field, $return = null, $type="any") {
	global $nohtml_page_purifier;
	global $allow_html_purifier;
	global $get_req;
	global $post_req;
	//global $system_security;
	if (isset($_POST[$field]) && ($type=="any" || $type=="post")) {
		$req = $post_req->getVarSerString($field);
		$clean_html = $allow_html_purifier->purify( $req );		
		return escapeString($clean_html);
	} else if (isset($_GET[$field]) && ($type=="any" || $type=="get")) {
		$req = $get_req->getVarSerString($field);
		$clean_html = $nohtml_page_purifier->purify( $req );		
		return escapeString($clean_html);
	} else {
		return $return;
	}
}

//adds the request as a session variable
function getStrFromRequestSession($field, $return = null) {
	global $nohtml_page_purifier;
	global $allow_html_purifier;
	global $get_req;
	global $post_req;
	global $cookie_req;
	if (isset($_POST[$field])) {
		$req = $post_req->getVarString($field);
		$clean_html = $allow_html_purifier->purify( $req );		
		return escapeString($clean_html);
	} else if (isset($_GET[$field])) {
		$req = $get_req->getVarSerString($field);
		$clean_html = $nohtml_page_purifier->purify( $req );		
		return escapeString($clean_html);
	} else {
		if (isset($_SESSION[$field])) return escapeString($cookie_req->getVarAlpha($field));
	}
}

/**
 * Has a certain button been clicked 
 * Use this function in case want to change to using images for buttons.
 */
function buttonClicked($buttonname) {
	if (isset($_POST["bt_$buttonname"])) {
		return true;
	} else {
		return false;
	}	
}

/** Returns a date formatted YYYY-MM-DD */
function getDateFromRequest($field) {
	/*
	if (isset($_POST[$field])) {
			return $_POST[$field];
	} else if (isset($_GET[$field])) {
			return $_GET[$field];
	} else {
		return null;
	}
	*/
	$fldDay = $field."_day";
	$fldMonth = $field."_month";
	$fldYear = $field."_year";
	
	if (isset($_POST[$fldDay]) && $_POST[$fldDay] != "") {
			$d = $_POST[$fldDay];
			$m = $_POST[$fldMonth];
			$y = $_POST[$fldYear];
			//if (checkdate($m,$d,$y)) {
				return "$y-$m-$d";
			//} else {
			//    return null;
			//}
			
	} else if (isset($_GET[$fldDay]) && $_GET[$fldDay] != "") {
			$d = $_GET[$fldDay];
			$m = $_GET[$fldMonth];
			$y = $_GET[$fldYear];
			//if (checkdate($m,$d,$y)) {
				return "$y-$m-$d";
			//} else {
			//	return null;
			//}
		
	} else {
		return null;
	}
	

	
}

function clean_input($str) {
	return mysql_real_escape_string($str);
}

function validEmail($email) {
   $isValid = true;
   $atIndex = strrpos($email, "@");
   if (is_bool($atIndex) && !$atIndex){
      $isValid = false;
   } else {
      $domain = substr($email, $atIndex+1);
      $local = substr($email, 0, $atIndex);
      $localLen = strlen($local);
      $domainLen = strlen($domain);
      if ($localLen < 1 || $localLen > 64)
      {
         // local part length exceeded
         $isValid = false;
      }
      else if ($domainLen < 1 || $domainLen > 255)
      {
         // domain part length exceeded
         $isValid = false;
      }
      else if ($local[0] == '.' || $local[$localLen-1] == '.')
      {
         // local part starts or ends with '.'
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $local))
      {
         // local part has two consecutive dots
         $isValid = false;
      }
      else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/', $domain))
      {
         // character not valid in domain part
         $isValid = false;
      }
      else if (preg_match('/\\.\\./', $domain))
      {
         // domain part has two consecutive dots
         $isValid = false;
      }
      else if(!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',
                 str_replace("\\\\","",$local)))
      {
         // character not valid in local part unless 
         // local part is quoted
         if (!preg_match('/^"(\\\\"|[^"])+"$/',
             str_replace("\\\\","",$local)))
         {
            $isValid = false;
         }
      }
      if ($isValid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A")))
      {
         // domain not found in DNS
         $isValid = false;
      }
   }
   return $isValid;
}

/**
 * Checks if a string is a valid URL
 * Actually checks that the url returns 200 status code, instead of a regex check
 *
 * @var string URL string to check
 * @return bool true on success
 */
function validURL($url)
{
	$headers = get_headers($url, 1);
	list($http, $code, $phrase) = explode(' ', $headers[0]);
	if($code == '200' && strtolower($phrase) == 'ok') return true;
	
	return false;
}

function cleanKey($key) {
    preg_match("/^[A-Za-z][A-Za-z0-9]*$/",$key,$arr);
    return $arr[0]; 
}

function cleanUrl($key) {
    preg_match("/^[\(\)\/\'\"\,\.\-\$\&\�\s@\?#_a-zA-Z\d]+$/",$key,$arr);
    if (isset($arr[0])) return $arr[0];
}

//replace unserialize() with this to avoid offset error
function __unserialize($sObject) {
	$sObject =  stripslashes($sObject);
    $__ret =preg_replace('!s:(\d+):"(.*?)";!se', "'s:'.strlen('$2').':\"$2\";'", $sObject );
    return @unserialize($__ret);
}