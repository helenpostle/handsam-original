<?php

/**
 * Register hooks and provide hook call points at different location in the code.  
 * 
 * Is included in web/shared/startPage.php and instantiated as $hook
 * 
 * Each page will need to include the hooks dir/file (which includes the functions and their registration). 
 * For example, in a module it would be 
    // hook functions
    require('hooks/hooks.php');
 * 
 */
class Hook {
    
    /**
     * static array of functions to be called by hook name and priority
     * 
     * @var array
     */
    public static $hooks = array();
    
    /**
     * register a hook
     * 
     * All hook function should be prefixed with hook_
     * 
     * e.g. register a function hook_propogate_task_save() to be called after saving a task, with standard priority
     * 
     * $hook->register_hook('task_save', 'propogate_task_save')
     * 
     * The hook_propogate_task_save() function should be defined before the hook is registered
     * 
     * @param string $hook_name
     * @param string $function_name
     * @param int $priority 
     */
    public function register_hook($hook_location_name, $function_name, $priority = 5)
    {
        self::$hooks[$hook_location_name][$priority][] = $function_name;
    }
    
    
    /**
     * Run a hook from deep somewhere in the codebase
     * 
     * e.g. Create a hook after a task is saved
     * 
     * $hook->run_hook('task_save', $taskid)
     * 
     * the first argument must be the location name (used when registering the hook) 
     * and any other values can then be passed
     * 
     * @return mixed
     */
    public function run_hook()
    {
        $num_args = func_num_args();
        if ($num_args == 0)
            return;
        
        //Get function arguments
        $arg_list = func_get_args();
        
        $hook_name = array_shift($arg_list);
        
        $func_args = 0;
        
       
        // This hook doesn't exist!
        if (!array_key_exists($hook_name, self::$hooks))
            return $func_args;
        
        // Sort by priority
        ksort(self::$hooks[$hook_name]);

        foreach(self::$hooks[$hook_name] as $priority)
        {
            //Call each hook function separately
            foreach($priority as $hook_function)
            {
                $call_func = 'hook_' . $hook_function;
                
                if (!function_exists($call_func))
                    continue;

                // Hook should have a return value
                $func_args = call_user_func_array($call_func, $arg_list);
            }
        }
        
        return $func_args;
    }    

} // EOC