<?php
	function afterSaveTask($type, $data, $old_data, $con) {
		global $receive_error;
		// if it's the tasks table we also need to update/insert accounttask
		// check if it was an update or an insert
		if($type == 'update') {
			// UPDATE
			$qry = new dbUpdate("accounttask");
			$qry->setParam('taskid',$data['taskid']);
			$qry->setStringParamNotEqual('state','COMPLETED');
			$qry->setStringVal('tasktext',$data['tasktext'], 'tasktext');
			if ($data['categoryid'] != $old_data['categoryid']) $qry->setNumberVal('categoryid',$data['categoryid'], 'categoryid');
			if(in_array($data['state'], array('INACTIVE', 'DELETED'))) {   
                            //only updates state if it has been change on the sync server
                            if ($data['state'] != $old_data['state']) $qry->setStringVal('state','DELETED1', 'state');
			} else {
                            //only updates state if it has been change on the sync server
                            if ($data['state'] != $old_data['state']) $qry->setStringVal('state',$data['state'], 'state');
			}
			
			// if the state changes from inactive to active, nothing happens,
			// although it probably should change all future tasks to Active,
			// but it's been this way for 6+ years so leave it for now. MG 21/11/12			
			// state not generic
			$qry->setAudit(0);
			if (!$qry->execute($con)) {
				//echo " :: ".$qry->getError();
				$receive_error = true;
			} else {
				//echo $qry->getSql();
			}
			
		} elseif($type == 'insert') {
			// INSERT
			#first get all active licenses
			$qryActiveLicences = "select accountlicence.accountid as accountid, accountlicence.licenceid as licenceid from accountlicence left join account on accountlicence.accountid = account.accountid where accountlicence.end_date > NOW() and accountlicence.state = 'ACTIVE' and accountlicence.tasklistid = ".$data['tasklistid'];
			$resultActiveLicences = getRS($con, $qryActiveLicences);
			while ($rowActiveLicence = getRow($resultActiveLicences)) {
                            #check that licence doesn't already have this taskid
                            $rsCheck = getRS($con,"select taskid from accounttask where licenceid = '".$rowActiveLicence['licenceid']."' and taskid = ".$data['taskid']);

                            //echo "select taskid from accounttask where licenceid = ".$rowActiveLicence['licenceid']." and taskid = ".$data['taskid'];
                            if (getResultRows($rsCheck) == 0) {
                                #for each active license without this taskid, add this new task with action date today
                                $qry = new dbInsert("accounttask");
                                $qry->setReqNumberVal("taskid",$data['taskid'],"Task id");
                                $qry->setReqNumberVal("accountid",$rowActiveLicence['accountid'],"Account id");
                                $qry->setReqNumberVal("categoryid",$data['categoryid'],"Category");
                                $qry->setReqNumberVal("licenceid",$rowActiveLicence['licenceid'],"licence id");
                                $qry->setStringVal("customtext",$data["tasktext"],"Custom Task text");		
                                $qry->setStringVal("tasktext",$data["tasktext"],"Task text");		
                                $qry->setReqStringVal("state",$data['state'],"state");		
                                $qry->addValue("actiondate", "NOW()");
                                $qry->setAudit(0);
                                if (!$qry->execute($con)) {
                                        //echo " :: ".$qry->getError();
                                        $receive_error = true;
                                } else {
                                        //echo $qry->getSql();
                                }
                            }
			}

			
		}
	}
	




	function afterSaveFiles($type, $data, $old_data, $con) {
		global $receive_error;
		
		if ($type == 'delete') {
		
			// delete from  all other tables which uses that file.
			// task_files, content_files, lotc_files, systemcourse_files
			$tables = array('task_files', 'content_files', 'lotc_files', 'systemcourse_files', 'qgp_file_cat', 'qgp_file_tags');
			foreach($tables as $t) {
				$qry = new dbDelete($t);
				$qry->setParam('fileid', $data['fileid']);
				if (!$qry->execute($con)) {
					//echo " :: ".$qry->getError();
					$receive_error = true;
				} else {
					//echo $qry->getSql();
				}			
			}
		}

    }
	
	function afterSaveLinks($type, $data, $old_data, $con) {
		global $receive_error;
        if ($type == 'delete') {
            // delete from  all other tables which uses that file.
            // task_files, content_files, lotc_files, systemcourse_files
            $tables = array('task_links', 'content_links', 'lotc_links', 'systemcourse_links');
            foreach($tables as $t) {
                $qry = new dbDelete($t);
                $qry->setParam('linkid', $data['linkid']);
                if (!$qry->execute($con)) {
                    //echo " :: ".$qry->getError();
                    $receive_error = true;
                } else {
                    //echo $qry->getSql();
                }			
            }
        }
    }	
    
    
 	function afterSaveQgpCat($type, $data, $old_data, $con) {
 		global $receive_error;
		// delete from  qgp_file_cat
        if ($type == 'delete') {
            $qry = new dbDelete('qgp_file_cat');
            $qry->setParam('catid', $data['catid']);
            if (!$qry->execute($con)) {
               //echo " :: ".$qry->getError();
                $receive_error = true;
            } else {
                //echo $qry->getSql();
            }
        }

    }
    
    function afterMultiSaveFiles($id_arr, $con) {
        
        if (is_array($id_arr) && count($id_arr) > 0) {
            //delete all other files which have an mid but not in this array.
            $qry = new dbDelete('files');
            $qry->setParamNotInArray('mid',$id_arr);
            $qry->setParamGT('mid',0);
            if (!$qry->execute($con)) {
                //echo " :: ".$qry->getError();
                $receive_error = true;
            } else {
                //echo $qry->getSql();
            }			
           
        }
        
    }
    
    
    function afterMultiSaveLinks($id_arr, $con) {
        
        if (is_array($id_arr) && count($id_arr) > 0) {
            //delete all other files which have an mid but not in this array.
            $qry = new dbDelete('links');
            $qry->setParamNotInArray('mid',$id_arr);
            $qry->setParamGT('mid',0);
            if (!$qry->execute($con)) {
                //echo " :: ".$qry->getError();
                $receive_error = true;
            } else {
                //echo $qry->getSql();
            }			
           
        }
        
    }    
    
    /**
     * After a new tag is created, adds it to all accounts that are entitled to all tags.
     * 
     * @param string $type insert|update|delete
     * @param type $new_data newly inserted data
     * @param type $old_data overwritten data
     * @param object $con
     */
    function afterSaveQgpTag($type, $new_data, $old_data, $con)
    {
        global $rootPath;
        if($type == 'insert')
        {
            //var_dump($new_data, $old_data);
        
            // after saving a tag, need to update the licence/tag table.
            require_once($rootPath.'/handsam_core/library/qgp/clsQgpTags.php');
            require_once($rootPath.'/modules/qgp/library/clsQgpTagsLicence.php');

            // passing null as fileid is not available nor needed
            $qgptag = new qgpTags(null);

            $qgptag->saveLicenceTag($new_data['tagid']);
        }
    }