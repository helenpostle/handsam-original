<?php

/**
 * recieve rules:
 * 1. if there we are sent 0ld_val and new_val, we only update the tables for each value if they are different
 * eg. so the sync server has to actively change the value of state, for it to change it here. 
 * 2. for dependency objects - state isn't updated because, but other fields are - because we have no new / old comparison for these
 * 
 * function dbUpdate has more comments on this - and the afterSave func too, and funcAfterSave.php file
 */

class Receive{
    
    protected $_raw_data;
    
    public $_data;

    public $_table;
    
    /**
     * the name of the primary key field
     * @var string
     */

    public $_primary_key_field;
	
    /**
     * the value of the primary key field
     * @var int
     */
    public $_primary_key_value;
    
    protected $_pwd;    
    
    public $_fk = array();
	
    /**
     * construct the where part of a query usinmg foreign keys - used to identify records where the primary key isn't passed over
     *
     */
    protected $fk_qry = array();
    
    protected $_delete;
    
    /**
     * Array of tables from which records can be deleted
     * @var array
     */
    protected $_delete_tables = array('files', 'task_files', 'links', 'task_links', 'qgp_cat');
	
    /**
    * Is this an object within an update? If it is, then the processing is slightly different - the state field is ignored for updates 
    *
    */
    protected $obj;


    /**
    * a list of table names that get processed after the main object is processed, instead of before
    * for tables that depend on the main object instead of the other way round
    */
    protected $post_objects = array('task_files', 'task_links', 'qgp_file_cat', 'qgp_file_tags');
    
    public function __construct($obj=false, $multi=false) {
        if ($obj) {
            $this->obj = true;
            $this->_raw_data = $obj;	
            $this->_data = $obj;					
        } else {
            if ($multi == false) {
                $this->obj = false;
                $this->_raw_data = @file_get_contents('php://input');	
                $this->_data = json_decode($this->_raw_data, true);		
                $this->_pwd = $this->_data['password'];
                $this->_delete = $this->_data['delete'];
            } else {
                $this->_data = $multi;
                $this->obj = false;              
            }
        }
 
        $this->_table = $this->_data['table'];
        
        // if the primary key is not in the data array, then set to empty string
        if(isset($this->_data['pk_fld'])) {
            $this->_primary_key_field = $this->_data['pk_fld'];
        } else {
            $this->_primary_key_field = '';
        }
        if(isset($this->_data['pk_val']) && $this->_data['pk_val'] > 0) {
            $this->_primary_key_value = $this->_data['pk_val'];
        } else {
            $this->_primary_key_value = null;
        }
        if(isset($this->_data['fk'])) $this->_fk = $this->_data['fk'];
    }
    
	
    /**
     * changes the table value 
     * 
     */	
    public function table($str) {
            $this->_table = $str;
    }
	
	
    /**
     * Authenticate the call 
     * 
     * Returns true or false
     */
    public function auth()
    {
        //first check ip
        //if ($_SERVER['REMOTE_ADDR'] != '46.236.8.105') return false;
        // compare the salted and hashed password with the stored value
        return (crypt($this->_pwd, Config::getItem('prop_salt')) === Config::getItem('prop_hash'));
    }

	//objects that the main synced object is dependent on
	public function processPreObjects($con, $data) {
		if (isset($this->_data['objects']) && count($this->_data['objects']) > 0) {
			$taskfiles = array();
			foreach ($this->_data['objects'] as $key => $obj) {
				if (!in_array($obj['table'], $this->post_objects)) {
					//new recieve object
					$rec = new Receive($obj);					
					// log the call
					$rec->save($con);
					unset($this->_data['objects'][$key]);
				}
			}
		}
	}


	//objects that are dependent on the main synced object
	public function processPostObjects($con) {
        $taskfiles = array();
		$tasklinks = array();		
 		$qgpcats = array();
        $qgptags = array();
        if (isset($this->_data['objects']) && count($this->_data['objects'])) {
            foreach ($this->_data['objects'] as $obj) {
                //new recieve object
                if ($obj['table'] == 'task_files') {				
                    $taskfiles = $this->syncTaskFiles($obj, $con, $taskfiles);
                }			
                if ($obj['table'] == 'task_links') {				
                    $tasklinks = $this->syncTaskLinks($obj, $con, $tasklinks);
                }			
                if ($obj['table'] == 'qgp_file_cat') {				
                    $qgpcats = $this->syncQgpCats($obj, $con, $qgpcats);
                }			
                if ($obj['table'] == 'qgp_file_tags') {				
                    $qgptags = $this->syncQgpTags($obj, $con, $qgptags);
                }			
            }
        //3. now we need to delete all of the files for this task that aren't synced and have mid>0
		}
        if (isset($this->_data['sync_title']) && $this->_data['sync_title'] != 'qgp')  $this->deleteTaskFiles($taskfiles, $this->_data['new_val'][$this->_primary_key_field], $con);
        if (isset($this->_data['sync_title']) && $this->_data['sync_title'] != 'qgp') $this->deleteTaskLinks($tasklinks, $this->_data['new_val'][$this->_primary_key_field], $con);
        if (isset($this->_data['sync_title']) && $this->_data['sync_title'] == 'qgp') $this->deleteQgpCats($qgpcats, $this->_data['new_val'][$this->_primary_key_field], $con);
        if (isset($this->_data['sync_title']) && $this->_data['sync_title'] == 'qgp') $this->deleteQgpTags($qgptags, $this->_data['new_val'][$this->_primary_key_field], $con);
	}
	
	
    /**
     * Save the passed over data.
     *  
     */
    public function save($con) {
	global $receive_error;
        $this->beforeSave($con);
         // data to insert
        $rec = FALSE;
        $record = $this->_data;
        // if the PKF is set, we need to check if the record exists
        if($this->_primary_key_value > 0) {
            $rec = $this->getLocalPkVal($con, $this->_table, $this->_primary_key_field);
            // $record will be passed to the sql, so don't want the PKV updated
            unset($record[$this->_primary_key_field]);
        } else if(count($this->_fk) > 0) {
            //use foreign keys to check for a record - this is for syncing depenency tables
            $qry = "select * from ".$this->_table." where ".implode(" and ", $this->fk_qry);
            $rs = getRS($con,$qry);
            switch (getResultRows($rs)) {
                    case 1:
                    //do nothing
                    return;
                    break;

                    case 0:
                    //insert so $rec = false;
                    $rec = false;
                    break;

                    default:
                    //an error because there's more than 1 record!
                    //echo " :: More then one record on remote server!";
                    return;
                    break;
            }
        }
        
        // update or insert, depending on if the record existed
        if($rec == FALSE) {
            if(isset($this->_data['new_val'][$this->_primary_key_field])) {
                $record['mid'] = $this->_data['new_val'][$this->_primary_key_field];
            }
			
			
            $this->dbInsert($con, $record);
            //$this->logQuery();
                        
            // change the PKV sent from Roger to the newly created value
            // in case there's any cascading queries (e.g. task and accounttask)
            $this->_data['new_val'][$this->_primary_key_field] = get_last_insert_id();
            //echo "last insertid: ".get_last_insert_id();
            $type = 'insert';
			
        } else {
            $this->dbUpdate($con, $record, $rec);
            $this->_data['new_val'][$this->_primary_key_field] = $rec;
            $type = 'update';            
            //$this->logQuery();
        }
        
        $this->afterSave($type, $con);
    }
    
    /**
     * Checks and changes to be made before the save
     * 
     */    
    public function beforeSave($con) {
        $this->resolveForeignKeys($con);
    }
    
    /**
     *  if there are any foreign keys
     *  we need to overwrite current value with their local values
     */
    public function resolveForeignKeys($con) {
        if(count($this->_fk) > 0) {
            foreach($this->_fk as $table => $key) {
                //parentid of content table is different:
                if ($table == 'content' && $key == 'parentid') {
                    $rec = $this->getLocalPkVal($con, $table, 'contentid', 'new_val', 'parentid');
                } else {
                    //first for new_val
                    $rec = $this->getLocalPkVal($con, $table, $key);
                }
                if (isset($this->_data['new_val'][$key])) {
                    $this->_data['new_val'][$key] = $rec;
                    $this->fk_qry[] = " $key = $rec ";
                }
                //now for old_val             
                if (isset($this->_data['old_val'][$key])) {
                    $rec = $this->getLocalPkVal($con, $table, $key, 'old_val');
                    $this->_data['old_val'][$key] = $rec;
                }
            }
        } 
    }
    
    /**
     * Functions to be run after the main save
     * dependent on the table
     * 
     * @param string $type db operation, either 'insert' or 'update'
     */
    public function afterSave($type, $con) {
        switch ($this->_table) {
            case 'task':
                afterSaveTask($type, $this->_data['new_val'], $this->_data['old_val'], $con);
                break;
            case 'files':
                afterSaveFiles($type, $this->_data['new_val'], $this->_data['old_val'], $con);
                break;
            case 'links':
                afterSaveLinks($type, $this->_data['new_val'], $this->_data['old_val'], $con);
                break;
            case 'qgp_cat':
                afterSaveQgpCat($type, $this->_data['new_val'], $this->_data['old_val'], $con);
                break;
            case 'qgp_tags':
                afterSaveQgpTag($type, $this->_data['new_val'], $this->_data['old_val'], $con);
                break;
        }		
    }
    
    
    
    /**
     * Special case for tasks table, after the main save
     * 
     * Also update accounttask
     */
    public function afterMultiSave($id_arr, $con) {
        switch ($this->_table) {
            case 'task':
                //afterSaveTask($type, $this->_data['new_val'], $this->_data['old_val'], $con);
                break;
            case 'files':
                afterMultiSaveFiles($id_arr, $con);
                break;
            case 'links':
                afterMultiSaveLinks($id_arr, $con);
                break;
        }		
    }    
    
    /**
     * Delete the record 
     * either by mid or foreign keys
     * 
     * only from tables that are explicitly set to accept deletetions
     */
    public function delete($con) {
		global $receive_error;
        if(!in_array($this->_table, $this->_delete_tables)) return false;
        
        // delete by PK/mid
        if($this->_primary_key_value > 0) {
            $rec = $this->getLocalPkVal($con, $this->_table, $this->_primary_key_field);
            $this->_data['new_val'][$this->_primary_key_field] = $rec;
            $qry = new dbDelete($this->_table);
            $qry->setParam($this->_primary_key_field,$rec);

            foreach ($this->_data['fields'] as $field) {
                    if (isset($this->_data['new_val'][$field])) {
                            if ($field != $this->_primary_key_field) $qry->setParam($field,$this->_data['new_val'][$field]);
                    }
            }

            //echo " :: ".$qry->getSql();
            if (!$qry->execute($con)) {
                    //echo " :: ".$qry->getError();
                    $receive_error = true;
            }
			
        } else {
            // delete by set foreign keys
            $this->resolveForeignKeys($con);	  
            $qry = new dbDelete($this->_table);
            foreach ($this->_fk as $field) {
                    if ($field != $this->_primary_key_field) $qry->setParam($field,$this->_data['new_val'][$field]);
            }

            //echo " :: ".$qry->getSql();
            if (!$qry->execute($con)) {
                    //echo " :: ".$qry->getError();
                    $receive_error = true;
            }
        }

        //$this->logQuery();
        
        $this->afterSave('delete', $con);
        
        //return $q;
    }

        
    /**
     * Log the database query 
     */
    public function logQuery()
    {
        $orig_qry = $this->_db->query;
        
        if($orig_qry !== '')
        {
            $mid = $this->_data['data'][$this->_primary_key_field];

            $log_q = 'INSERT INTO query_log 
                        (query, timestamp, curl_log_rec_mid) 
                        VALUES (:query, NOW(), :curl_mid)';

            // replace the :keys with the values,
            // mimicking an old fashioned sql query
            foreach($this->_db->params as $key => $val)
                $orig_qry = preg_replace('/:'.$key.'\b/', "'".$val."'", $orig_qry);

            $log_p = array('query' => $orig_qry,
                            'curl_mid' => $mid,
                        );

            $this->_db->modify($log_q, $log_p);
            
            $this->_db->query = '';
        }
    }
    
    

    /**
     * Return the local Primary Key value
     * 
     * Can be used to check if a local record exists, 
     * or when a record has a FK that needs localising
     * The primary key is matched against the value in the _data array
     * 
     * @param string $table
     * @param string $pk name of the primary key field
     */
    public function getLocalPkVal($con, $table, $tablepk, $val_type='new_val', $table_srfk = false) {
        global $receive_error;
        if(!isset($this->_data[$val_type][$tablepk])) return false;
        
        // get local taskid
        $sql = "SELECT $tablepk AS pk FROM $table WHERE mid = ".$this->_data[$val_type][$tablepk];
        
        //self refernceing foreign key
        if ($table_srfk) $sql = "SELECT $tablepk AS pk FROM $table WHERE mid = ".$this->_data[$val_type][$table_srfk];
        //echo " :: ".$sql." :: ";
        if ($rs = getRS($con, $sql)) {
            $row = getRow($rs);
            //$this->logQuery($sql);		
            return $row['pk'];
        } else {
            $receive_error = true;
        }
    }
    
    /**
     * If the row is to be deleted
     * @return bool
     */
    public function isDelete() {
        return $this->_delete;
    }
	
	
    private function dbInsert($con, Array $data = array()) {
        global $receive_error;
        $qry = new dbInsert($this->_table);

        //now loop through the fields, check for differences between old and new values and sync if they are different. 
        //this means that only changed values are updated

        foreach ($this->_data['fields'] as $field) {
            if (isset($this->_data['new_val'][$field])) {
                if ($field != $this->_primary_key_field) $qry->setStringVal($field,$this->_data['new_val'][$field],$field);
                if ($field == $this->_primary_key_field) $qry->setNumberVal('mid',$this->_data['new_val'][$field],'mid');
                if ($field == 'tasktext') $qry->setStringVal('customtext',$this->_data['new_val'][$field],'customtext');
                if ($field == 'tasklistname') $qry->setStringVal('customtext',$this->_data['new_val'][$field],'customtext');
                if ($field == 'categoryname') $qry->setStringVal('customtext',$this->_data['new_val'][$field],'customtext');
            }
        }
        $qry->setAudit(0);
        if ($this->_table != "qgp_file_tags" && $this->_table != "qgp_file_cat") {
            $qry->setStringVal("edited_by",0);
            $qry->addValue("edited","NOW()");
        }
        
        //add audit fields.
        //echo " :: ".$qry->getSql();
        if (!$qry->execute($con)) {
            //echo " :: ".$qry->getError();
            $receive_error = true;
        }
    }
	
	//need to change this now!
    public function dbUpdate($con, Array $data = array(), $pk) {
        global $receive_error;
        $qry = new dbUpdate($this->_table);
        $qry->setParam($this->_primary_key_field,$pk);

        //now loop through the fields, check for differences between old and new values and sync if they are different. 
        //this means that only changed values are updated

        //we won't update any 'state' fields for  a contained object
        //we only update fields where the old_val and new_val is different or if the old_val isn't set

        foreach ($this->_data['fields'] as $field) {
            if (isset($this->_data['new_val'][$field])) {
                if ($this->obj) {
                    if ($field != $this->_primary_key_field && $field != 'state') $qry->setStringVal($field,$this->_data['new_val'][$field],$field);
                } else {
                    if (isset($this->_data['old_val'][$field]) && $this->_data['old_val'][$field] != $this->_data['new_val'][$field]) {
                        if ($field != $this->_primary_key_field) $qry->setStringVal($field,$this->_data['new_val'][$field],$field);
                    } else if(!isset($this->_data['old_val'][$field])) {
                        if ($field != $this->_primary_key_field) $qry->setStringVal($field,$this->_data['new_val'][$field],$field);
                    }
                }
            }

        }

        $qry->setAudit(0);

        //add audit fields.
        //echo " :: ".$qry->getSql();
        if (!$qry->execute($con)) {
            //echo " :: ".$qry->getError();
            $receive_error = true;
        }

    }	
	
	
	protected function deleteTaskFiles($taskfiles, $taskid, $con) {
            global $receive_error;
            if (count($taskfiles) > 0) {
                $qry = "delete tf.* from task_files tf left join files f  on tf.fileid = f.fileid where tf.taskid = $taskid and f.filetype = 'handsamDoc' and tf.fileid  not in (".implode(',', $taskfiles).") and f.mid>0 and filetype='handsamDoc'";
            } else {
                $qry = "delete tf.* from task_files tf left join files f  on tf.fileid = f.fileid where tf.taskid = $taskid and f.filetype = 'handsamDoc' and f.mid>0  and filetype='handsamDoc'";
            }
            //echo " :: $qry";
            $exec = execSQL($con, $qry);
            if ($exec != '') {
                //echo " :: $exec";
                $receive_error = true;
            }

           
	}
	
	
	protected function deleteTaskLinks($tasklinks, $taskid, $con) {
		global $receive_error;
		if (count($tasklinks) > 0) {
			$qry = "delete tf.* from task_links tf left join links f  on tf.linkid = f.linkid where tf.taskid = $taskid and f.linktype = 'handsamDoc' and tf.linkid  not in (".implode(',', $tasklinks).") and f.mid>0  and linktype='handsamDoc'";
        } else {
            $qry = "delete tf.* from task_links tf left join links f  on tf.linkid = f.linkid where tf.taskid = $taskid and f.linktype = 'handsamDoc'  and f.mid>0  and linktype='handsamDoc'";
        }
        //echo " :: $qry";
        $exec = execSQL($con, $qry);
        if ($exec != '') {
            //echo " :: $exec";
            $receive_error = true;
        }

	}	
    
	
	protected function syncTaskFiles($obj, $con, $taskfiles) {
		//this object is a file object with the taskid added 
		//we need to sync it as a 'files' object without the taskid
		//then we need to sync a task_files object 
		//then we need to delete all other task_files for this task that are mid>0  - this is handled seperately 
		
		$rec = new Receive($obj);
		//1. sync as a files obj
		$rec->table('files');
		$fileid = $rec->_data['new_val']['fileid'];
		$rec->save($con);
		$taskfiles[] = $rec->_data['new_val'][$rec->_primary_key_field];
		
		//2. sync task_files - remove all files info from this object, add taskid, and sync etc
		$rec->table('task_files');
		$rec->_primary_key_field = 'taskfileid';
		$rec->_primary_key_value = 0;
		$rec->_fk = array("files" => "fileid", "task" => "taskid");
		$rec->_data['fields'] = array("fileid","taskid");
		$rec->_data['old_val'] = array("taskid" => $this->_primary_key_value);		
		$rec->_data['new_val'] = array("fileid" => $fileid, "taskid" => $this->_primary_key_value);
		$rec->_data['pk_fld'] = 'taskfileid';
		$rec->_data['pk_val'] = 0;
		$rec->_data['mid'] = 0;					
		$rec->save($con);				
		//$taskid = $rec->_data['new_val']['taskid'];
        return $taskfiles;
	}
	
	
	protected function syncTaskLinks($obj, $con, $tasklinks) {
		//this object is a link object with the taskid added 
		//we need to sync it as a 'links' object without the taskid
		//then we need to sync a task_links object 
		//then we need to delete all other task_links for this task that are mid>0  - this is handled seperately 
		
		$rec = new Receive($obj);
		//1. sync as a links obj
		$rec->table('links');
		$linkid = $rec->_data['new_val']['linkid'];
		$rec->save($con);
		$tasklinks[] = $rec->_data['new_val'][$rec->_primary_key_field];
		
		//2. sync task_files - remove all links info from this object, add taskid, and sync etc
		$rec->table('task_links');
		$rec->_primary_key_field = 'tasklinkid';
		$rec->_primary_key_value = 0;
		$rec->_fk = array("links" => "linkid", "task" => "taskid");
		$rec->_data['fields'] = array("linkid","taskid");
		$rec->_data['old_val'] = array("taskid" => $this->_primary_key_value);		
		$rec->_data['new_val'] = array("linkid" => $linkid, "taskid" => $this->_primary_key_value);
		$rec->_data['pk_fld'] = 'tasklinkid';
		$rec->_data['pk_val'] = 0;
		$rec->_data['mid'] = 0;					
		$rec->save($con);				
		//$taskid = $rec->_data['new_val']['taskid'];
        return $tasklinks;
	}	
    
    
    
    /*
     * method to sync qgp cats
     */
	protected function syncQgpCats($obj, $con, $qgpcats) {
		//this object is a file object with the taskid added 
		//we need to sync it as a 'files' object without the taskid
		//then we need to sync a task_files object 
		//then we need to delete all other task_files for this task that are mid>0  - this is handled seperately 
		
		$rec = new Receive($obj);
		//1. sync as a qgp cat obj
		$rec->table('qgp_cat');
		$catid = $rec->_data['new_val']['catid'];
		$rec->save($con);
		$qgpcats[] = $rec->_data['new_val'][$rec->_primary_key_field];
		
		//2. sync qgp_file_cat - remove all cat info from this object, add fileid, and sync etc
		$rec->table('qgp_file_cat');
		$rec->_primary_key_field = 'cat_fileid';
		$rec->_primary_key_value = 0;
		$rec->_fk = array("files" => "fileid", "qgp_cat" => "catid");
		$rec->_data['fields'] = array("fileid","catid");
		$rec->_data['old_val'] = array("fileid" => $this->_primary_key_value);		
		$rec->_data['new_val'] = array("catid" => $catid, "fileid" => $this->_primary_key_value);
		$rec->_data['pk_fld'] = 'cat_fileid';
		$rec->_data['pk_val'] = 0;
		$rec->_data['mid'] = 0;					
		$rec->save($con);				
		//$fileid = $rec->_data['new_val']['fileid'];
        return $qgpcats;
	}
	
	protected function deleteQgpCats($qgpcats, $fileid, $con) {
            global $receive_error;
            if (count($qgpcats) > 0) {
                $qry = "delete tf.* from qgp_file_cat tf left join files f  on tf.fileid = f.fileid where tf.fileid = $fileid and f.filetype = 'handsamDoc' and f.filecategory = 'QGP' and tf.catid  not in (".implode(',', $qgpcats).") and f.mid>0";
            } else {
                $qry = "delete tf.* from qgp_file_cat tf left join files f  on tf.fileid = f.fileid where tf.fileid = $fileid and f.filetype = 'handsamDoc' and f.filecategory = 'QGP' and f.mid>0";
            }
            //echo " :: $qry";
            $exec = execSQL($con, $qry);
            if ($exec != '') {
                //echo " :: $exec";
                $receive_error = true;
            }

            
	}
	    
    
    
    /*
     * method to sync qgp cats
     */
	protected function syncQgpTags($obj, $con, $qgptags) {
		
		$rec = new Receive($obj);
		//1. sync as a qgp cat obj
		$rec->table('qgp_tags');
		$tagid = $rec->_data['new_val']['tagid'];
		$rec->save($con);
		$qgptags[] = $rec->_data['new_val'][$rec->_primary_key_field];
		
		//2. sync qgp_file_tags - remove all tag info from this object, add fileid, and sync etc
		$rec->table('qgp_file_tags');
		$rec->_primary_key_field = 'tag_fileid';
		$rec->_primary_key_value = 0;
		$rec->_fk = array("files" => "fileid", "qgp_tags" => "tagid");
		$rec->_data['fields'] = array("fileid","tagid");
		$rec->_data['old_val'] = array("fileid" => $this->_primary_key_value);		
		$rec->_data['new_val'] = array("tagid" => $tagid, "fileid" => $this->_primary_key_value);
		$rec->_data['pk_fld'] = 'tag_fileid';
		$rec->_data['pk_val'] = 0;
		$rec->_data['mid'] = 0;					
		$rec->save($con);				
        return $qgptags;
	}
	
	protected function deleteQgpTags($qgptags, $fileid, $con) {
            global $receive_error;
            if (count($qgptags) > 0) {
                $qry = "delete tf.* from qgp_file_tags tf left join files f  on tf.fileid = f.fileid where tf.fileid = $fileid and f.filetype = 'handsamDoc' and f.filecategory = 'QGP' and tf.tagid  not in (".implode(',', $qgptags).") and f.mid>0";
            } else {
                $qry = "delete tf.* from qgp_file_tags tf left join files f  on tf.fileid = f.fileid where tf.fileid = $fileid and f.filetype = 'handsamDoc' and f.filecategory = 'QGP' and f.mid>0";
            }
            //echo " :: $qry";
            $exec = execSQL($con, $qry);
            if ($exec != '') {
                //echo " :: $exec";
                $receive_error = true;
            }

            
	}
	        
}
?>