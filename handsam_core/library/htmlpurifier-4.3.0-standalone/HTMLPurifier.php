<?php
include("{$this->rootPath}/handsam_core/library/htmlpurifier-4.3.0-standalone/HTMLPurifier.standalone.php");
function loadHtmlPurifierConfig($root_path) {
	global $html_purifier_config;

	include("{$root_path}/shared/htmlpurifier.config.php");
}
loadHtmlPurifierConfig($this->rootPath);
?>