<?php
 /**
  * Some functions to return specific dates
  * TODO: Standardise Date Formats
  */
  
  function getLastMonday() {
	$arrDate = getdate();
	
	//echo "day is " . $arrDate["wday"];
	$days = 6 + $arrDate["wday"];
	
	$lastmonday = time() - ($days * 24 * 60 * 60);
	//return strftime('%Y-%m-%d',$lastmonday);	  
	return strftime('%a, %d %b %Y',$lastmonday);	  
  }
  
  function getStartLastMonth() {
	$arrDate = getdate();
	$month = $arrDate["mon"] - 1;
	$year = $arrDate["year"];  
	
	if ($month == 0) {
		$month = 12;
		$year = $year - 1;
	}
	
	return strftime('%a, %d %b %Y',strtotime("$year-$month-01"));
  }
  
  function getEndLastMonth() {
	$arrDate = getdate();
	$month = $arrDate["mon"];
	$year = $arrDate["year"];  
	
	$startdate = strtotime("$year-$month-01") - (24 * 60 * 60);
	return strftime('%a, %d %b %Y',$startdate);	
  }
  
  function getStartLastYear() {
	 $arrDate = getdate();
	 $year = $arrDate["year"]-1;
	 
	 return  "$year-01-01";
  }
  
  function getEndLastYear(){
	 $arrDate = getdate();
	 $year = $arrDate["year"]-1;
	 
	 return  "$year-12-31";
  }
  
  function formatDatabaseInputDate($val) {
	 $date = strtotime($val);
	 return strftime("%Y-%m-%d",$date);  
	 
  }


function getStartLastWeek() {	
	$mon = date("Y-m-d", strtotime("Previous week"));
	return strftime('%a, %d %b %Y',strtotime($mon));
}
  
function getEndLastWeek() {
	$fri = date("Y-m-d", strtotime("Previous Sunday"));
	return strftime('%a, %d %b %Y',strtotime($fri));
}

function newgetEndLastWeek() {
    if (date('N', time()) == 7){
	   $fri = date(now());
	} else {
	   $fri = date("Y-m-d", strtotime("Previous Sunday"));
	}
	
	return strftime('%a, %d %b %Y',strtotime($fri));
}

//used in the repeating task class
function getWeekNumbers($startdate, $enddate) {
    
    $start = date("W", strtotime($startdate));
    $end = date("W", strtotime($enddate));
     
    //adjust to user expecttations, so that any date at end of dec is in week 52 and not week 1
    if (date("m", strtotime($enddate)) == '12' && $end == '01') $end = 52;
    if (date("m", strtotime($startdate)) == '12' && $start == '01') $start = 52;

    if (strtotime($startdate) < strtotime($enddate) && $start == $end) {
        return range(1, 52, 1);
    } else if (strtotime($startdate) < strtotime($enddate) && $start > $end) {
        return array_merge(range($start, 52, 1), range(1, $end, 1));
    } else if (strtotime($startdate) < strtotime($enddate)) {
        return range($start, $end);
    } else if (strtotime($enddate) < strtotime($startdate)) {
        return array_merge(range($start, 52, 1), range(1, $end, 1));
    } else {
        return array();
    }
}

//used in the repeating task class
function getMonthNumbers($startdate, $enddate) {
    $start = date("m", strtotime($startdate));   
    $end = date("m", strtotime($enddate)); 
    if ($start <= $end) return range($start, $end ,1);
    if ($end < $start) {
        return array_merge(range($start, 12, 1), range(1, $end, 1));
    }
}
  
function getMonthByNumber($number,$year) {
    return date("F",strtotime('+ '.$number.' weeks', mktime(0,0,0,1,1,$year,-1)));
}
  ?>