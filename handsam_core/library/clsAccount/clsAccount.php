<?php

/**
 * Controls an single account
 */


class Account {
    
    /**
     * id in database table
     * @var int db id
     */
    private $id;
    
    /**
     * Holds the record from the DB
     * Set to private, so __get will be called by $account->getProperty
     * @var int id of the database row 
     */
    private $row = array();
    
    public function __construct($id) 
    {
        $this->setAccountid($id);
        
        $this->_loadAccount();
    }
    
    public function setAccountid($id)
    {
        if(is_int($id)) $this->id = $id;
    }
    
    /**
     * Get Magic Method
     * 
     * Used like $myuser->firstname which will return accountname
     * 
     * http://www.php.net/manual/en/language.oop5.overloading.php#object.get
     * 
     * Returns an index of the $row property
     * @param string $name
     * @return string or null 
     */
    public function __get($name)
    {
        $name = strtolower($name);
        if(isset($this->row[$name])) return $this->row[$name];

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }
    
    /**
     * Deletes the account.
     * Set `state` to DELETED
     * @return empty string or mysql error
     */
    public function delete()
    {
        $sql = "UPDATE account SET state = 'DELETED' WHERE accountid = '".$this->id."';";
        return executeSql($sql); 
    }

        /**
     * Gets all the info from the db for the specific account. 
     * Loads it into the $row property
     */
    protected function _loadAccount()
    {
        $sql = "SELECT * FROM account WHERE accountid = '".$this->id."';";
        $this->row = getRowAssoc(getRecSet($sql));
        //var_dump($this->row);
    }
    
}
// EOC