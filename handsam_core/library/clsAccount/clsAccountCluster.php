<?php

class AccountCluster {
    
    /**
     * User id in usr table
     * @var int
     */
    private $id;
    //private $title;
    private $description;
    private $manager;
    private $created;
    private $created_by;
    private $edited;
    private $edited_by;
    private $state;
    /**
     * cluster details
     * @var array
     */
    public $cluster = array();
    
    /**
     * Constructor
     * @param int user id 
     */
    public function __construct($id=0) {
        $this->id = $id;
        
        $this->_getDetails();
    }
    
    /**
     * Get Magic Method
     * 
     * Used like $myaccount->accountname which will return accountname
     * 
     * http://www.php.net/manual/en/language.oop5.overloading.php#object.get
     * 
     * Returns an index of the $row property
     * @param string $name
     * @return string or null 
     */
    public function __get($name)
    {
        $name = strtolower($name);
        if(isset($this->cluster[$name])) return $this->cluster[$name];
        /*
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
         * 
         */
        return null;
    }
    
    /**
     * Loads user details from the db
     * stores them in $this->user 
     */
    private function _getDetails()
    {
        $sql = "SELECT * FROM account_cluster WHERE id = ".$this->id;
        $res = getRecSet($sql);
        $this->cluster = getRowAssoc($res);
    }

    
    public function save($con)
    {
      global $clsUS,$messages;
      $rsCheck = getRS($con, "select * from account_cluster where title = '{$_POST["title"]}' and state='ACTIVE' and id != {$this->id}");
      if(getRow($rsCheck))
      {
          $messages[] = 'This title is already in use. Please enter another title';
      }
      else
      {
        if ($this->id == 0) {
          $qry = new dbInsert("account_cluster");
        } else {
          $qry = new dbUpdate("account_cluster");
          $qry->setParam("id",$this->id);
        }
		//delete the current managers for this cluster
		$sql = "delete from account_cluster_manager where cluster_id = {$this->id}";
	    execSQL($con, $sql);
		//first go through multi select drop down and create a new entry
		//in account_cluster_manager for each one
		$managers = $_POST["manager"];
		if ($managers){
		  //at least one manager has been selected
		  foreach ($managers as $value){
		     $sql = "insert into account_cluster_manager (cluster_id, manager) values (".$this->id.",".$value.");";
			 execSQL($con, $sql);
		  }
		}
       // $qry->setNumberVal("manager",$_POST["manager"],"cluster manager");
        $qry->setReqStringVal("title",$_POST["title"],"cluster title");
        $qry->setStringVal("description",$_POST["description"],"cluster description");
        $qry->setReqStringVal("state",$_POST["state"],"state");
        $qry->setAudit($clsUS->userid);
        if ($qry->execute($con)) {
          $this->id = $qry->getNewID();
          $this->_getDetails();
          $messages[] = "Cluster details saved";
          return true;
        }else {
        return false;
		$messages[] = "There was a problem saving the cluster details";
		}
      }     
    }
    
    public function delete($con)
    {
      global $clsUS,$messages;
      $qry = new dbUpdate("account_cluster");
      $qry->setParam("id",$this->id);
      $qry->setReqStringVal("state",'DELETED',"state");
      $qry->setAudit($clsUS->userid);
      if ($qry->execute($con)) {
        $messages[] = "Cluster details deleted.";
          //update account table
        $qry = new dbUpdate("account");
        $qry->setParam("cluster_id",$this->id);
        $qry->setNullVal("cluster_id");
        $qry->setAudit($clsUS->userid);
        if ($qry->execute($con)) {
          $messages[]= "All accounts that were assigned to this cluster no longer have a cluster assigned to them";
          return true;
        }
      }
      return false;
    }
    
    
    public function __toString()
    {
        return 'User object: '.var_export($this->user, true);
    }
    
} // EOC