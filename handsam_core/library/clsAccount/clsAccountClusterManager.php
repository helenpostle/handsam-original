<?php

class AccountClusterManager {
    
    /**
     * User id in usr table
     * @var int
     */
    private $id;
    private $cluster_id;
    private $manager;
    private $created;
    private $created_by;
    private $edited;
    private $edited_by;
	private $state;

    /**
     * cluster details
     * @var array
     */
    public $cluster_manager = array();
    
    /**
     * Constructor
     * @param int user id 
     */
    public function __construct($id=0) {
        $this->id = $id;
        
      //  $this->_getDetails();
    }
    
    /**
     * Get Magic Method
     * 
     * Used like $myaccount->accountname which will return accountname
     * 
     * http://www.php.net/manual/en/language.oop5.overloading.php#object.get
     * 
     * Returns an index of the $row property
     * @param string $name
     * @return string or null 
     */
    public function __get($name)
    {
        $name = strtolower($name);
        if(isset($this->cluster_manager[$name])) return $this->cluster_manager[$name];
        /*
        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
         * 
         */
        return null;
    }
    
    /**
     * Loads cluster manager details from the db
     * stores them in $this->cluster_manager 
     */
    private function _getDetails()
    {
        $sql = "SELECT manager FROM account_cluster_manager WHERE cluster_id = ".$this->id;
        //$res = getRecSet($sql);
        $this->cluster_manager = $sql;
		return $sql;
    }
	
	public function getNameList($id)
	{
	   $sql = "SELECT manager, concat(firstname,' ', lastname) as name FROM account_cluster_manager join usr on usr.userid = manager WHERE cluster_id =".$this->id;
	   $rs = getRS($con, $sql);
	   $list = '';
	   while ($row = getRow($rs_sel)) {
	      $list .= "<br />".$row['name'];
	   }
	   return $list;
	}

    
    public function save($con)
    {
      global $clsUS,$messages;
      //first we need to delete any entries in account_cluster_manager
	  $sql = "delete from account_cluster_manager where cluster_id = {$id}";
	  execSQL($con, $sql);
	  //now for each selection in the drop down list, we need to add a new
	  //entry in the table
	  $managers = $_POST["manager"];
	  return $managers;
	  
    }
    
    public function delete($con)
    {
      global $clsUS,$messages;
      $qry = new dbUpdate("account_cluster");
      $qry->setParam("id",$this->id);
      $qry->setReqStringVal("state",'DELETED',"state");
      $qry->setAudit($clsUS->userid);
      if ($qry->execute($con)) {
        $messages[] = "Cluster details deleted.";
          //update account table
        $qry = new dbUpdate("account");
        $qry->setParam("cluster_id",$this->id);
        $qry->setNullVal("cluster_id");
        $qry->setAudit($clsUS->userid);
        if ($qry->execute($con)) {
          $messages[]= "All accounts that were assigned to this cluster no longer have a cluster assigned to them";
          return true;
        }
      }
      return false;
    }
    
    
    public function __toString()
    {
        return 'User object: '.var_export($this->user, true);
    }
    
} // EOC