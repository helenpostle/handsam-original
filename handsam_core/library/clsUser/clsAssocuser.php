<?php

class Assocuser {
    
    /**
     * Associated user id in usr_assoc table
     * @var int
     */
    private $user_id;
    
    /**
     * Associated user details
     * @var array
     */
    private $user = array();
    
    /**
     * Constructor
     * @param int user id 
     */
    public function __construct($user_id) {
        $this->user_id = $user_id;
        
        $this->_getUserDetails();
    }
    
    /**
     * Get Magic Method
     * 
     * Used like $myassocuser->accountname which will return accountname
     * 
     * http://www.php.net/manual/en/language.oop5.overloading.php#object.get
     * 
     * Returns an index of the $row property
     * @param string $name
     * @return string or null 
     */
    public function __get($name)
    {
        $name = strtolower($name);
        if(isset($this->user[$name])) return $this->user[$name];

        $trace = debug_backtrace();
        trigger_error(
            'Undefined property via __get(): ' . $name .
            ' in ' . $trace[0]['file'] .
            ' on line ' . $trace[0]['line'],
            E_USER_NOTICE);
        return null;
    }
    
    /**
     * Loads user details from the db
     * stores them in $this->user 
     */
    private function _getUserDetails()
    {
        $sql = "SELECT * FROM usr_assoc WHERE assoc_userid = '".$this->user_id."' LIMIT 1;";
        $res = getRecSet($sql);
        $this->user = getRowAssoc($res);
    }

    
} // EOC