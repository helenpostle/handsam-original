<?php

class clsUserSession {

    var $userid;
    var $accountid;
    var $accountname;
    var $accountlogo;
    var $schoolstatus;
    var $licences = array();
    var $firstname;
    var $lastname;
    var $password;
    var $username;
    var $state;
    var $usertype;
    var $visits = 0;
    var $lastvisit;
    var $security = array();
    var $security_no_task_list = array();
    //var $security_app = array();
    var $numSearches = 0;
    var $visitid;
    var $modules = array();
    var $conf = array();
    var $loggedon = false;

    // constructor 
    function clsUserSession($username, $password, $con, $token = "")
    {
        //how many days after expiry users can login
        global $daysOfGrace, $homePath, $password_arr;
        if(isset($username) || (isset($token) && $token != ""))
        {
            if($token != "")
            {
                $this->userid = $this->checkToken($con, $token);
            }
            else
            {
                $this->username = $username;
                $passwd = new clsUserPassword($homePath, $password_arr);
                $this->userid = $passwd->checkPassword($con, $password, $username);
            }
            
            //returns userid if password validates else 0
            if($this->userid > 0)
            {
                $qry = "SELECT usr.*, accountlicence.end_date, account.accountname, account.accountlogo, account.school_status 
                FROM usr LEFT JOIN account ON usr.accountid = account.accountid 
                LEFT JOIN accountlicence ON account.accountid = accountlicence.accountid 
                LEFT JOIN usr_usrtype ON usr.userid = usr_usrtype.userid 
                LEFT JOIN usrtype ON usr_usrtype.usertype = usrtype.usrtype 
                WHERE usr.userid = {$this->userid} AND usr.state != 'INACTIVE'and account.state = 'ACTIVE' AND ((accountlicence.end_date > DATE_SUB(Now(), INTERVAL " . $daysOfGrace . " DAY) AND accountlicence.appid = usrtype.appid and accountlicence.state = 'ACTIVE') OR usr.accountid = 1)";
                
                $result = getRS($con, $qry);

                if(getResultRows($result) > 0)
                {
                    $row = getRow($result);

                    //load variables
                    $this->userid = $row["userid"];
                    $this->accountlogo = $row["accountlogo"];
                    $this->accountid = $row["accountid"];
                    $this->accountname = $row["accountname"];
                    $this->schoolstatus = $row["school_status"];
                    $this->username = $row["username"];
                    $this->firstname = $row["firstname"];
                    $this->lastname = $row["lastname"];
                    //$this->password = $row["password"];
                    //$this->usertype = $row["usertype"];
                    $this->loggedon = true;
                    $this->visits = $row["logincnt"];
                    $this->lastvisit = $row["lastlogin"];

                    // new query that takes into account the state of the licence and if it has ended!
                    $pqry = "SELECT DISTINCT a.licenceid, a.appid, usr_security.* , usr_usrtype.tasklistid
			FROM accountlicence a LEFT JOIN usr  ON a.accountid = usr.accountid 
			LEFT JOIN usr_usrtype ON usr.userid = usr_usrtype.userid
			LEFT JOIN usr_security ON usr_usrtype.usertype = usr_security.usrtype
			LEFT JOIN usrtype ON  usr_usrtype.usertype = usrtype.usrtype AND (usr_usrtype.tasklistid = a.tasklistid OR usr_usrtype.tasklistid IS NULL)
			LEFT JOIN  accountlicence b ON usrtype.appid = b.appid 
			WHERE usr.userid = " . $this->userid . " AND a.licenceid = b.licenceid  AND a.state='ACTIVE' AND a.end_date > NOW() ;";

                    $presult = getRS($con, $pqry);
                    
                    while ($prow = getRow($presult))
                    {
                        //add value to array for each permission
                        $this->security[$prow['appid']][$prow["tasklistid"]][] = $prow["security"];
                        $this->security_no_task_list[] = $prow["security"];
                    }

                    // now get permisions for SuperAdmin usertypes - accountid=1
                    if($this->accountid == 1)
                    {
                        $pqry = "select us.* from usr_security us left join core_SA_usr_usrtype csuu on us.usrtype = csuu.SA_usrtype where csuu.userid = " . $this->userid;
                        $presult = getRS($con, $pqry);
                        while ($prow = getRow($presult))
                        {
                            $this->security_no_task_list[] = $prow["security"];
                        }
                    }
					
					//is this user a cluster manager
					$cqry = "SELECT cluster_id FROM handsam.account_cluster_manager where manager = " . $this->userid;
					$cresult = getRS($con, $cqry);
					while ($crow = getRow($cresult)){
					   $this->account_clusters[] = $crow["cluster_id"];
					}

                    //now get permisions for HandsamSuperAdmin usertypes - where usr.usertype = 'HandsamSuperAdmin'
                    if($this->accountid == 1 && $row["usertype"] == 'HandsamSuperAdmin')
                    {
                        $pqry = "select us.* from usr_security us where usrtype='HandsamSuperAdmin'";
                        $presult = getRS($con, $pqry);
                        while ($prow = getRow($presult))
                        {
                            $this->security_no_task_list[] = $prow["security"];
                        }
                    }

                    $qry = "SELECT licenceid FROM accountlicence WHERE accountid = {$this->accountid} and state = 'ACTIVE'";
                    $lresult = getRS($con, $qry);
                    while ($lrow = getRow($lresult))
                    {
                        //add value to array for each permission
                        $this->licences[] = $lrow["licenceid"];
                    }

                    //now current licence config
                    $qry = "SELECT al.licenceid, apps.folder, al.conf FROM accountlicence al left join apps on al.appid = apps.appid WHERE al.accountid = {$this->accountid} and al.state = 'ACTIVE' and al.start_date < NOW() and al.end_date > NOW()";
                    $rs_conf = getRS($con, $qry);
                    while ($row_conf = getRow($rs_conf))
                    {
                        $this->conf[$row_conf['folder']] = json_decode($row_conf['conf'], true);
                    }
                    
                    //increment login
                    execSQL($con, "UPDATE usr SET logincnt = logincnt + 1, lastlogin = NOW() WHERE userid = " . $row["userid"]);
                    $s = 1;
                }
                else
                {
                    $s = 0;
                }
            }
            else
            {
                $s = 0;
            }
            if($this->userid > 0)
            {
                $sql = "INSERT INTO visit (userid, username, visittime,success,ipaddress, sessionid) VALUES (" . $this->userid . ",'" . $username . "',NOW(),$s,'" . $_SERVER["REMOTE_ADDR"] . "','" . session_id() . "')";
                execSQL($con, $sql);
                $this->visitid = get_last_insert_id();
            }
        }
    }

    function getFullName()
    {
        return "$this->firstname $this->lastname";
    }

    function isAllowed($p, $appid = 0, $tasklistid = 0)
    {

        //returns true if they have $p
        if($tasklistid > 0)
        {
			if(isset($this->security[$appid]) && array_key_exists($tasklistid, $this->security[$appid]))
			{
				return in_array($p, $this->security[$appid][$tasklistid]);
			}
			return false;
            //if(isset($this->security[$appid]) && ($this->security[$appid] && !array_key_exists($tasklistid, $this->security[$appid])) || !is_array($this->security[$appid][$tasklistid])) return false;
            
            
            //return in_array($p, $this->security[$appid][$tasklistid]);
        } else if($appid > 0)
        {
            if(!is_array($this->security[$appid]))
                return false;
            return in_array($p, $this->security[$appid]);
        } else
        {
            return in_array($p, $this->security_no_task_list);
        }
    }

    function isLicence($l)
    {
        //returns true if they have $p
        return in_array($l, $this->licences);
    }

    function inactivateAccount($con)
    {
        $sql = "UPDATE usr SET state = 'INACTIVE' WHERE username = '" . $this->username . "'";
        execSQL($con, $sql);
    }

    function remember_me($con)
    {
        global $messages;
        global $protocol;
        $token = $this->genToken(39, false);
        $sql = "UPDATE usr SET token = '$token' WHERE userid = '" . $this->userid . "'";
        if(execSQL($con, $sql) == "")
        {
            if($protocol == "https")
            {
                setcookie("cooktoken", $token, time() + 60 * 60 * 24 * 100, "/", "", true, true);
            }
            else
            {
                setcookie("cooktoken", $token, time() + 60 * 60 * 24 * 100, "/", "", false, true);
            }
            $meassages[] = "Failed to save token";
        }
    }

    function unremember_me($con)
    {
        global $protocol;
        global $messages;
        $token = "";
        $sql = "UPDATE usr SET token = '$token' WHERE userid = '" . $this->userid . "'";
        if(execSQL($con, $sql) == "")
        {
            if($protocol == "https")
            {
                setcookie("cooktoken", '', time() - 3600, "/", "", true, true);
            }
            else
            {
                setcookie("cooktoken", '', time() - 3600, "/", "", true, true);
            }
        }
        else
        {
            $meassages[] = "Failed to save null token";
        }
    }

    function genToken($len = 32, $md5 = true)
    {

        # Array of characters, adjust as desired
        $chars = array(
            'Q', '@', '8', 'y', '%', '^', '5', 'Z', '(', 'G', '_', 'O', '`',
            'S', '-', 'N', '<', 'D', '{', '}', '[', ']', 'h', ';', 'W', '.',
            '/', '|', ':', '1', 'E', 'L', '4', '&', '6', '7', '#', '9', 'a',
            'A', 'b', 'B', '~', 'C', 'd', '>', 'e', '2', 'f', 'P', 'g', ')',
            '?', 'H', 'i', 'X', 'U', 'J', 'k', 'r', 'l', '3', 't', 'M', 'n',
            '=', 'o', '+', 'p', 'F', 'q', '!', 'K', 'R', 's', 'c', 'm', 'T',
            'v', 'j', 'u', 'V', 'w', ',', 'x', 'I', '$', 'Y', 'z', '*'
        );

        # Array indice friendly number of chars; empty token string
        $numChars = count($chars) - 1;
        $token = '';

        # Create random token at the specified length
        for($i = 0; $i < $len; $i++)
            $token .= $chars[mt_rand(0, $numChars)];

        # Should token be run through md5?
        if($md5)
        {
            # Number of 32 char chunks
            $chunks = ceil(strlen($token) / 32);
            $md5token = '';

            # Run each chunk through md5
            for($i = 1; $i <= $chunks; $i++)
                $md5token .= md5(substr($token, $i * 32 - 32, 32));

            # Trim the token
            $token = substr($md5token, 0, $len);
        } return $token;
    }

    function checkToken($con, $token)
    {
        $qry = "select userid from usr where token='$token'";
        $rs = getRS($con, $qry);
        $row = getRow($rs);
        if($row['userid'] > 0)
        {
            return $row['userid'];
        }
        else
        {
            return 0;
        }
    }

	function getUniqueString($con) {
        global $messages;
		$qry1 = "select unique_string from usr where userid={$this->userid}";
		$rs = getRS($con, $qry1);
		$row = getRow($rs);
		if ($row['unique_string'] != '') {
            $md5 = md5($row['unique_string']);
            return $md5;
        } else {
            //create the unique struing and try again
            $qry = new dbUpdate('usr');
            $qry->setParam("userid",$this->userid);
            $qry->setNullParam('unique_string');
            $qry->setStringParam('unique_string',"", "or");
            $qry->setTableColVal('unique_string', 'concat(username, accountid, firstname, userid)', 'unique string');
            if (!$qry->execute($con)) {
                
				$messages[] = $qry->getError();
			} else {
                return $this->getUniqueString($con);
            }
        }		
	}
	

}
