<?php
/*file upload function */
function qgpUpload($clsUS, $pageVars, $con, $to_file_path) {
	global $messages;
	#get filename and previously uploaded filename from form
	$filename = $_FILES['filename']['name'];
	$uploaded_filename = $_POST['uploaded_filename'];
	if ($filename != "") {
		#if filename not empty then upload file
		$file_path=$_FILES['filename']['tmp_name'];
		//die($file_path);
		$upload_file = uploadFile($filename, $file_path, $to_file_path);
		
		if($upload_file) {
			//die();
			#if upload successful check for previously uploaded filename
			if ($uploaded_filename != "" && $uploaded_filename != $upload_file) {
				#delete this 
				$deleteFile = deleteFile($uploaded_filename, $to_file_path);
			}
			#now set previously uploaded filename as this uploaded filename - incase form doesn't validate then user doesn't have to re-select a new file
			$filename = $upload_file;
			
		} else {
			$messages[] = ERROR_FILE_UPLOAD;
			return false;
		}
			
		
	} else if ($uploaded_filename != "") {
		$filename = $uploaded_filename;
	}
	if ($pageVars->fileid == 0) {
		$qry = new dbInsert("files");
	} else {
		$qry = new dbUpdate("files");
		$qry->setParam("fileid",$pageVars->fileid);
	}

	if (isset($qry)) {
		if (!isset($_POST['catid'])) {
			$qry->failError .= "Please select at least one category.\r\n";
		}
		$qry->setReqStringVal("filename",$filename,"filename");
		$qry->setReqStringVal("title",$_POST["title"],"Title");
		$qry->setStringVal("description",$_POST["description"],"Description");
		$qry->setReqStringVal("filetype",'handsamDoc',"file type");
		$qry->setReqStringVal("filecategory", $_POST['filecategory'], "file category");
		$qry->setReqNumberVal("accountid",1,"Account id");
		$qry->setAudit($clsUS->userid);
		
		if ($qry->execute($con)) {
			if ($pageVars->fileid == 0) {
				$pageVars->fileid = $qry->getNewID();	
			}
			$messages[] = SAVED;
			return $pageVars->fileid;
			
		} else {
			$messages[] = $qry->getError();	
			//echo $qry->getError();
			return $filename;
		}
	}
	return false;
}


function qgpDelete($pageVars, $con, $to_file_path) {
	global $messages;
	$qry = new dbDelete("files");
	$qry->setParam("fileid",$pageVars->fileid);
	if (!$qry->execute($con)) {
		$messages[] = $qry->getError();
	}
	//$fileid = 0;
	if(deleteFile($_POST['uploaded_filename'], $to_file_path)) {
		
		$messages[] = DELETED;
		return $pageVars->fileid;
		

	} else {
		$messages[] = ERROR_FILE_DELETE;
		return false;
	}			

}


function countFileUsed($fileid, $con) {
	$qry = "select (count(t.taskid) + count(lotc_files.fileid)) as cnt ";
	$qry .= " FROM files f left join task_files t on f.fileid = t.fileid left join lotc_files on f.fileid = lotc_files.fileid left join content_files on f.fileid = content_files.fileid where f.fileid = $fileid ";
	$rs = getRS($con,$qry);
	$row =  getRow($rs);
	return $row['cnt'];
}

function deleteFromObj($fileid, $con) {
    global $messages;
    $qry = dbDelete('task_files');
    $qry->setParam("fileid",$fileid);
    if (!$qry->execute($con)) {
        $messages[] = $qry->getError();
    } else {
        $messages[] = "Deleted from tasks";
    }
    $qry = dbDelete('lotc_files');
    $qry->setParam("fileid",$fileid);
    if (!$qry->execute($con)) {
        $messages[] = $qry->getError();
    } else {
        $messages[] = "Deleted from lotc";
    }
    $qry = dbDelete('content_files');
    $qry->setParam("fileid",$fileid);
    if (!$qry->execute($con)) {
        $messages[] = $qry->getError();
    } else {
        $messages[] = "Deleted from content";
    }
}
?>