<?php
/**
 * Class to load variables from GET, POST, user session depending on security
 */
class loadVars {

	var $userid;
	var $accountid;
	var $edit_userid;
	var $edit_usertype;
	var $hacking = false;
	
	
	function loadVars($clsUS,$con) {
		if (isAllowed("editAccount")) {
			$this->userid = getIntFromRequest("userid");
			$this->accountid = getIntFromRequest("accountid",$clsUS->accountid);
		} else {
			$this->userid = $clsUS->userid;
			$this->accountid = $clsUS->accountid;
		}
		
		$this->edit_usertype = getIntFromRequest("edit_usertype");
		$this->edit_userid = $this->checkEditUserId($clsUS,$con);
	}
	
	function checkEditUserId($clsUS,$con) {
		$id = getIntFromRequest('edit_userid');
		if ($id === 0) return 0;
		if ($id === null) return "";
		if ($id === "") return "";
		if (isAllowed("adminAccidents")) {
			return $id;
		} else if ($id > 0) {
			if ($this->edit_usertype == 1) $qry = "select accountid, created_by from usr where userid = $id";
			if ($this->edit_usertype == 2) $qry = "select accountid, created_by from usr_assoc where assoc_userid = $id";
			//die($qry);
			$rs = getRS($con,$qry);
			$row = getRow($rs);
			if (isAllowed("editAssocUsers") && $row['accountid'] == $clsUS->accountid){
				return $id;
			}
		}
		$this->hacking = true;
		return null;
	}	
		
	
}
	
?>