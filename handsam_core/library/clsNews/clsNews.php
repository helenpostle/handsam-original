<?php

/**
 * Description of news
 *
 * @author Mei Gwilym
 *          21-0-2013
 */
class Usernews {
        
    protected $userid;
    protected $read_stories = array();
    
    public function __construct($userid)
    {
        $this->userid = $userid;
        $this->getRead();
    }
    
    public function getRead()
    {
        $sql = "SELECT content_id, user_id, the_date, rating
                FROM news_users
                WHERE user_id = {$this->userid};";
        $rs = getRecSet($sql);
        
        foreach($rs as $recordset)
        {
            $row = getRowAssoc($recordset);
            $this->read_stories[] = $row->content_id;
        }            
    }
    
    /**
     * Return the latest story
     * false on error
     */
    public function getLatest()
    {
        $qry = "SELECT content.*, usr.username FROM content 
                INNER JOIN usr 
                    ON content.created_by = usr.userid 
                WHERE content.state = 'PUBLISHED' 
                AND superpage = 'news'  
                ORDER BY content.created DESC
                LIMIT 1;";
        $res = getRecSet($qry);
        if($res != false)
        {
            $row = getRowAssoc($res);
            if(!in_array($res->contentid, $this->read_stories))
                return $row;
        }
        
        return $res;
    }
    
    /**
     * Return a story by its ID
     * @param int $id
     */
    public function getById($id)
    {
        
    }
    
    /**
     * Get all unread stories
     * @return array
     */
    public function getUnread()
    {
        
    }
    
    /**
     * Get archives
     * @return array of news title, date and url
     */
    public function getArchive()
    {
        
    }
    
}

