<?php

/**
 * Functions for use on ./securePage.php
 */

function isUserAllowed($owner, $ownerp, $fullp)
{
    if(isset($_SESSION['clsUS']))
    {
        $clsUS = $_SESSION['clsUS'];

        return $clsUS->isUserAllowed($owner, $ownerp, $fullp);
    }
    else
    {
        return false;
    }
}

function isAllowed($p, $a = 0, $t = 0)
{
    if(isset($_SESSION['clsUS']))
    {
        $clsUS = $_SESSION['clsUS'];
        return $clsUS->isAllowed($p, $a, $t);
    }
    else
    {
        return false;
    }
}

function isLicence($l)
{
    if(isset($_SESSION['clsUS']))
    {
        $clsUS = $_SESSION['clsUS'];
        return $clsUS->isLicence($l);
    }
    else
    {
        return false;
    }
}

/**
 * If logged in or not
 * @return bool
 */
function isLoggedIn()
{
    return isset($_SESSION['clsUS']);
}

/**
 * Count number of login failures
 * 
 * @global string $messages
 * @global type $max_login_attempts
 * @global type $adminMail
 * @param type $con
 * @param type $clsUS
 * @return boolean 
 */
function countFailure($con, $clsUS)
{
    global $messages;
    global $max_login_attempts;
    global $adminMail;

    $user = $clsUS->username;

    if(!isset($_SESSION["failCount$user"]))
    {
        $_SESSION["failCount$user"] = 0;
    }
    $_SESSION["failCount$user"] = $_SESSION["failCount$user"] + 1;

    if($_SESSION["failCount$user"] == $max_login_attempts)
    {
        $clsUS->inactivateAccount($con);
		
		//remove any cookie tokens
		if(isset($_COOKIE['cooktoken']))
		{
			$sql = "UPDATE usr SET token = '' WHERE token = '".escapeString($token)."'";
			execSQL($con, $sql);
			setcookie("cooktoken", '', time() - 3600, "/", "", true, true);
		}
        $messages[] = EXCEEDED_MAX_LOGINS;
        
        $ipaddress = (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) ? $_SERVER['HTTP_X_FORWARDED_FOR'] : $_SERVER['REMOTE_ADDR'];
        
        $body = getEmailTemplate('max_login_attempts', $user, $ipaddress);

        sendMail($user, $adminMail, "Account Deativated", $body);

        return true;
    }
    return false;
}

/**
 * Logs a user in and adds the class to the session
 * 
 * @param object $con database connection
 * @param string $username username
 * @param string $password password
 * @param bool $remember true or false to remember login
 * @param string $token user token
 * 
 * @return boolean true if logged in, false if not.
 */
function doLogin($con, $username, $password, $remember, $token = "")
{
    global $messages;
    $clsUS = new clsUserSession($username, $password, $con, $token);

    if($clsUS->loggedon)
    {
        if($remember)
        {
            $clsUS->remember_me($con);
        }
		else 
		{
			$clsUS->unremember_me($con);
		}
        $_SESSION['clsUS'] = $clsUS;
        session_regenerate_id(true);
        return true;
    }
    else
    {
        if(!countFailure($con, $clsUS))
        {
            //above returns true if has inactovated account
            $messages[] = USER_NOT_FOUND;
        }

        return false;
    }
}