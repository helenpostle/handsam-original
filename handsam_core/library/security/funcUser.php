<?php
#checks if the logged in user has permission to edit a user 

function editUserCheck($clsUS, $userid, $accountid, $con) {
	#non superAdmin editing same account user
	if (isAllowed("editHandsamSuperAdmin")) return true;
	
	if (isAllowed("editAccount") && $clsUS->accountid == 1 && $accountid > 1) return true;

	if (isAllowed("editAccount") && $clsUS->accountid == 1 && $accountid == 1) {
		if ($userid == 0) return true;
		#check if userid is a handsamsuperadmin
		$qry = "select usr_usrtype.usertype from usr_usrtype where userid = $userid";
		$rs = getRS($con, $qry);
		while ($row = getRow($rs)) {
			if ($row['usertype'] == "HandsamSuperAdmin") return false;
		}
		return true;
	}
	#super admin editing non super admin account
	if ($clsUS->accountid > 1 && $clsUS->accountid == $accountid) return true;
	if (!isAllowed("editAccount") && isAllowed("editUsers") && $clsUS->accountid == $accountid) return true;
	
	return false;
	
}
?>