<?php

// login functions
// moved out from this file, MG 28/02/2013
include('funcSecurity.php');

//update visits table with latest datetime
if(!$debug && $protocol == "https")
    INI_Set('session.cookie_secure', true);

$f_password = getStrFromRequest('f_password', "", "post");
$f_login = getStrFromRequest('f_login', "", "post");

if(isset($_SERVER['HTTP_REFERER']) && @$_SERVER['HTTP_REFERER'] !== null)
{
    $referer = $_SERVER['HTTP_REFERER'];
}
else
{
    $referer = $_SERVER['SCRIPT_NAME'] . "?" . $_SERVER['QUERY_STRING'];
}
$referer = explode("/", $referer);
$pageCameFrom = $referer[count($referer) - 1];

include("$rootPath/handsam_core/library/clsPassword/clsUserPassword.php");
include("$rootPath/handsam_core/library/clsPassword/clsPassword.php");
include("$rootPath/handsam_core/library/clsUser/clsUserSession.php");
include("$rootPath/handsam_core/library/clsUser/clsUser.php"); // MG, 10/10/12
include("$rootPath/handsam_core/library/clsUser/clsAssocuser.php"); // MG, 10/10/12

//offline catch
if (isset($restrict_ip) && count($restrict_ip) > 0 && !in_array($_SERVER['REMOTE_ADDR'], $restrict_ip) && (!isset($this_page) || $this_page != "maintenance.php")) {
    $pageFiles->addFunc('content');
    $pageFiles->includePhp();
    include ("layout_offline.php");
    die();
}
//
//start session stuff
if(!$debug && $protocol == "https")
{
    session_cache_limiter('none');
    session_start();
}
else if($debug)
{
    session_cache_limiter('none');
    session_start();
}
else if($protocol == "http")
{
    session_cache_limiter('none');
    session_start();
}
else
{
    session_cache_limiter('none');
    session_start();
}

//check if are logging on.
$_SESSION["cameFrom"] = $pageCameFrom;

$cookieloggedout = false;

// unread news checker
$pageFiles->addFunc('news_check');
$pageFiles->includePhp();
$unreadnews = null;
$unread = null;

if(isset($_POST["f_login"]))
{
    if(!doLogin($con, $f_login, $f_password, isset($_POST["rememberme"])))
    { // do nothing
        $secure = true;
    } else {       
        $unread = new Unreadnews();
        if($core_conf['unread_news_must_read']===true)
        {
          $unreadnews = $unread->getUnread();
        }
        else
        {
          $unreadnews = $unread->getUnread($core_conf['unread_news_items']);
        }       
    }
}
else if(isset($_GET["f_login"]))
{
    if(!doLogin($con, $f_login, $f_password, false))
    { //do nothing
        $secure = true;
    } else {
        $unread = new Unreadnews();
        if($core_conf['unread_news_must_read']===true)
        {
          $unreadnews = $unread->getUnread();
        }
        else
        {
          $unreadnews = $unread->getUnread($core_conf['unread_news_items']);
        }           
    }
}
else if(isset($_GET["logout"]))
{
    if(isset($_SESSION["clsUS"]) && is_object($_SESSION["clsUS"]))
    {
        //now delete csrf token - can't use class because its not loaded yet			
        $qry = "delete from csrf_token where userid=" . $_SESSION["clsUS"]->userid;
        execSQL($con, $qry);
    }

	/*
	//this has been replaced so that the cookie only fills in the username field on the login form. Logging out will not change this.
	//must untick remember me box to opt out
    if(isset($_COOKIE['cooktoken']) && isset($_SESSION["clsUS"]))
    {
        $clsUS = $_SESSION["clsUS"];
        $clsUS->unremember_me($con);
        $cookieloggedout = true;
    }
	*/
    //record logout time in  visit table;
    if(isset($_SESSION["clsUS"]) && $_SESSION["clsUS"]->visitid > 0)
    {
        $debug_sql = "UPDATE visit set loggedout = NOW() where visitid = {$_SESSION["clsUS"]->visitid} and userid = {$_SESSION["clsUS"]->userid}";
        execSQL($con, $debug_sql);
    }
    session_regenerate_id(true);
    unset($_SESSION['clsUS']);

    $messages[] = LOGOUT;
    if(isset($_GET["autologout"]))
    $messages[] = AUTO_LOGOUT;
}

/** 
 * on login check for unread messages
 * 
 */

if(!isset($_SESSION['clsUS']))
{
    //we are not logged in. 
    //check if cookie set and if so login from that
	
	//This has been replaced with a server side autocomolete func - so that the cookie will result int he username field being filled in on the login form.
	/*
    if(!$cookieloggedout && isset($_COOKIE['cooktoken']) && $_COOKIE['cooktoken'] != "")
    {
        if(!doLogin($con, "", "", true, $_COOKIE['cooktoken']))
        {
            $messages[] = CANT_REMEMBER;
        }
    }
	*/
    if(!isset($secure))
    {
        $secure = false;
    }

    //if still not logged in and is all secure, or specific request, include login page and die!
    if(( $secure || isset($_GET["login"])) && !isset($_SESSION['clsUS']))
    {
        $pageFiles = new clsLoadPageFiles($rootPath, $rootUrl, $jQuery, "");
        $title = "$siteName LOGIN";
        $content = "login.php";
        //$box[] = "login.php";
        //$title = $_POST['f_login'];
        include("layout.php");
        die;
    }
}

if(isset($_SESSION["clsUS"]))
{
    $clsUS = $_SESSION["clsUS"];
    $visit_sql = "UPDATE visit set last_accessed = NOW() where visitid = {$clsUS->visitid} and userid = {$clsUS->userid}";
    execSQL($con, $visit_sql);
}
