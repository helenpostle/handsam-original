<?php
if (isset($ajaxModule) && $ajaxModule != ''&& isLoggedIn()) {
	
	$csrf_tok = new csrfSecurity('ajax');
	$csrf_tok->check_token($con);
	if ($csrf_tok->verify == false) {
		headerLocation("$rootUrl/index.php", $messages);
		die();
	}
}

?>