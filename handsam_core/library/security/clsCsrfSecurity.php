<?php
class csrfSecurity {
	var $token;
	var $type;
	var $userid;
	var $verify = false;
	
	
	function csrfSecurity($type)	{
		$this->type = $type;
	}
	
	function setToken($clsUS, $con) {
		if (isset($clsUS) && $clsUS->userid > 0) {
			$this->userid = $clsUS->userid;
		} else {
			$this->userid = 0;
		}
		//delete all old tokens more than an hour old
		$qry = "delete from csrf_token where DATE_ADD(created, INTERVAL 4 HOUR) < NOW() or (userid=".$this->userid." and sessionid != '".session_id()."')";
		execSQL($con, $qry);
		
		//now check for an existing token:
		$qry = "select * from csrf_token where sessionid='".session_id()."' and userid=".$this->userid;
		$rs = getRS($con,$qry);
		if ($row = getRow($rs)) {
			$this->token = $row['token'];
		} else {
			$this->token = $this->genToken();
			//set new token
			$qry = "insert into csrf_token (token, sessionid, userid, created) values ('{$this->token}','".session_id()."', {$this->userid}, NOW())";
			execSQL($con, $qry); 
		}
		
	}

	function check_token($con) {
		if ($this->type == "ajax") {
			$csrf_tok = getStrFromRequest('tkn', null, 'post');
		} else {
			$csrf_tok = getStrFromRequest('frm_tygwyn', null, 'post');
		}
			
		//only check for tokens less than an hour old.
		$qry = "select * from csrf_token where sessionid='".session_id()."' and token='$csrf_tok' and DATE_ADD(created, INTERVAL 4 HOUR) > NOW()";
		$rs = getRS($con,$qry);
		if ($row = getRow($rs)) $this->verify = true;
		//don't delete because we may do more ajax requests on this page
	}

	
	function genToken( $len = 32, $md5 = true ) {
	 	 
	    # Array of characters, adjust as desired
	    $chars = array(
	        'Q', '@', '8', 'y', '%', '^', '5', 'Z', '(', 'G', '_', 'O', '`',
	        'S', '-', 'N', '<', 'D', '{', '}', '[', ']', 'h', ';', 'W', '.',
	        '/', '|', ':', '1', 'E', 'L', '4', '&', '6', '7', '#', '9', 'a',
	        'A', 'b', 'B', '~', 'C', 'd', '>', 'e', '2', 'f', 'P', 'g', ')',
	        '?', 'H', 'i', 'X', 'U', 'J', 'k', 'r', 'l', '3', 't', 'M', 'n',
	        '=', 'o', '+', 'p', 'F', 'q', '!', 'K', 'R', 's', 'c', 'm', 'T',
	        'v', 'j', 'u', 'V', 'w', ',', 'x', 'I', '$', 'Y', 'z', '*'
	    );
	 
	    # Array indice friendly number of chars; empty token string
	    $numChars = count($chars) - 1; $token = '';
	 
	    # Create random token at the specified length
	    for ( $i=0; $i<$len; $i++ )
	        $token .= $chars[ mt_rand(0, $numChars) ];
	 
	    # Should token be run through md5?
	    if ( $md5 ) {
	 
	        # Number of 32 char chunks
	        $chunks = ceil( strlen($token) / 32 ); $md5token = '';
	 
	        # Run each chunk through md5
	        for ( $i=1; $i<=$chunks; $i++ )
	            $md5token .= md5( substr($token, $i * 32 - 32, 32) );
	 
	        # Trim the token
	        $token = substr($md5token, 0, $len);
	 
	    } return $token;
	}	
	
}

?>