<?php

if (isset($pageFiles) && $pageFiles->csrf == 'ajax') {
	
	if (isLoggedIn()) {
		//set csrf token for ajax requests or forms.
		$csrf_tok = new csrfSecurity("ajax");
		$csrf_tok->setToken($clsUS, $con);
		echo "<input id=\"tygwyn\" type=\"hidden\" name=\"tygwyn\" value=\"{$csrf_tok->token}\"/>";
		//$csrf_tok->createPassword();
	}
}

?>