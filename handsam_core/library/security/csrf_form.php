<?php
if (isLoggedIn()) {
	
	$csrf_tok = new csrfSecurity('form');
	$csrf_tok->check_token($con);
	if ($csrf_tok->verify == false) die();
}

?>