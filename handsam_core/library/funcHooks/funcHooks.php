<?php

/**
 * Hook Function to be called from modules.
 * 
 * This function can be placed anywhere in code, 
 * to hook in and execute extra functionality at that point in the execution flow. 
 * 
 * The function inculdes the module's hook file, and execute the function specified
 * 
 * @param string the current module name
 * @param string function to call
 * @param string extra arguments can be passed to the function after the first two
 */
function call_hook($mod_name, $hook)
{
    global $rootPath;
    
    // check for module
    if(!is_dir($rootPath.'/modules/'.$mod_name)) die('no such module: '.$mod_name);
    
    // only include once 
    require_once $rootPath.'/modules/'.$mod_name.'/hooks/hooks.php';
    
    $args = array();
    if(func_num_args() > 2)
    {
        $args = func_get_args(); 
        
        // get rid of the first two args
        array_shift($args);
        array_shift($args);
    }
    
    // check and call the function, pass any extra arguments
    if(function_exists($hook))
        $hook($args);
}