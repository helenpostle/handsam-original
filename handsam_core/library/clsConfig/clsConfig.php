<?php

/**
 * Simple config class
 * 
 * for storing of key=>values pairs.
 *  
 */
class Config {
    
    private static $_values = array();
    
    public static function getItem($key)
    {
        if(isset(self::$_values[$key]))
        {
            return self::$_values[$key];
        }
        
        return FALSE;
    }
    
    public static function setItem($key, $item)
    {
        self::$_values[$key] = $item;
    }
    
} 

//EOC