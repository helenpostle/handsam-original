<?php
/**
 * class for changing , validating and creating password for a user
 * 
 * 
 */


class clsUserPassword {
	
	var $ski = "";
	var $password_arr = "";
	var $userid = "";
	var $password = "";
	var $homePath = "";
	var $crypt = "";
	
	function clsUserPassword($homePath, $password_arr) {
		global $core_conf;
		$this->crypt = $core_conf['crypt'];
		$this->password_arr = $password_arr;
		$this->homePath = $homePath;
	}
	
	
	function checkPassword($con, $password, $username, $userid=0) {
		$clean_passwd = escapeString($password);
		$clean_username = escapeString($username);
		$qry = "SELECT userid, password FROM usr WHERE usr.username = '$clean_username' AND usr.state = 'ACTIVE'";
		if ($userid>0) $qry .= " and userid=$userid";
		
		$result = getRS($con, $qry);
		$row = getRow($result);
		
		if ($this->hasher($clean_passwd, $row['password'])) {
			$this->userid = $row['userid'];
			$this->password = $clean_passwd;
			return $row['userid'];
		} else {
			return 0;
		}
	}
	
	function changePassword($con,$oldp, $pas1, $pas2, $userid, $username) {
		if ($this->checkPassword($con, $oldp, $username, $userid) > 0) {
			//now validate password
			$password = new Password($this->password_arr);
			$score = $password->scorePassword($pas1);
			if ($password->validatePassword($pas1)) {
				if ($pas1 != $pas2) {
					return PASSWORD_NOT_MATCHED;
				} else {
					$hash_password = $this->hasher($pas1);
					execSQL($con, "Update usr set password = '$hash_password' where userid = ".$this->userid);
					$this->password = $pas1;
					return PASSWORD_CHANGED;
				}
			} else {
				return PASSWORD_SHORT;
			}
			
		} else {
			return PASSWORD_WRONG;
		};

	}
	
	
	function emailPassword($to, $con, $adminMail) {
		if (validEmail($to)) {
      $to = escapeString($to);
			//create new password for this user:
			$password = new Password($this->password_arr);
			$new_password = $password->generatePassword();
			$hash_password = $this->hasher($new_password);
			
			$rsUser = getRS($con,"select userid from usr  where username = '".$to."'");
			$rowUser = getRow($rsUser); 
			if ($rowUser['userid']) {
				
				$udQry = new dbUpdate("usr");
				$udQry->setParam("userid",$rowUser['userid']);
				$udQry->setReqStringVal("password",$hash_password,"Password");
				if (!$udQry->execute($con)) {
					return "Failed to update password";
				} else {
          $title = NEW_PASSWORD_SUBJECT;

          $message = getEmailTemplate('email_password', $to, $new_password);
                                
					if (sendMail($to, $adminMail, $title, $message)){
						return PASSWORD_SENT;
					} else {
						return "Error sending password. Please contact the administrator.";
					}
				}
				
				
			} else {
				return "We cannot find this username";
			}
		} else {
			return "We cannot find this username";
		}	
	}
	
	
	function getSalt() {
		if ($this->crypt == "blowfish") {
			$cnt = 22;
		} else {
			$cnt = 9;
		}
		
		$c = explode(" ", ". / a A b B c C d D e E f F g G h H i I j J k K l L m M n N o O p P q Q r R s S t T u U v V w W x X y Y z Z 0 1 2 3 4 5 6 7 8 9");
		$ks = array_rand($c, $cnt);
	
		$s = "";
		foreach($ks as $k) { $s .= $c[$k]; }
	
		return $s;

	
	}
	
	
	
	function hasher($info, $encdata = false) {
		$strength = "08"; 
		//if encrypted data is passed, check it against input ($info) 
		if ($encdata) {
			if ($this->crypt == "blowfish") {
				//echo "1:".substr($encdata, 0, 60)."<br/>";
				//echo "2:".crypt($info, "$2a$".$strength."$".substr($encdata, 60))."<br/>";
				if (substr($encdata, 0, 60) == crypt($info, "$2a$".$strength."$".substr($encdata, 60))) { 
				  return true; 
				} else { 
				  return false; 
				} 
			} else {
				//echo "1:".substr($encdata, 0, 34)."<br/>";
				//echo "2:".crypt($info, "$1$".substr($encdata, 34))."<br/>";
				if (substr($encdata, 0, 34) == crypt($info, "$1$".substr($encdata, 34))) { 
				  return true; 
				} else { 
				  return false; 
				} 			}
		} else { 
		  //make a salt and hash it with input, and add salt to end 
			$salt = $this->getSalt();
			//echo "salt: $salt<br/>";
			if ($this->crypt == "blowfish") {
				return crypt($info, "$2a$".$strength."$".$salt).$salt;
			} else {
				return crypt($info, "$1$".$salt).$salt;
			}
		} 
	}
	
	
	function updatePasswords($con, $accountid=0, $userid=0) {
		global $meassages;
		$qry = "select userid, accountid, password from usr ";
		if ($userid > 0) {
			$qry .= " where userid = $userid";
		} else if ($accountid>0) {
			$qry .= " where accountid = $accountid ";
		}
		$first_part = "$1$";
		if ($this->crypt == "blowfish") $first_part = "$2a$";
		$rs = getRS($con,$qry);
		while ($row=getRow($rs)) {
			if (stripos($row['password'], $first_part) === false) {
				$hash_password = $this->hasher($row['password']);
				//echo $hash_password;
				$udQry = new dbUpdate("usr");
				$udQry->setParam("userid",$row['userid']);
				$udQry->setReqStringVal("password",$hash_password,"Password");
				if (!$udQry->execute($con)) {
					$meassages[] =  "Failed to update password for userid: {$row['userid']} - {$udQry->getSQL()}";
				} else {
					$meassages[] = "Password updated for userid: {$row['userid']}";
				}
			} else {
				$meassages[] = "password already migrated for userid: {$row['userid']}";
			}
		}
	}
		

	
	
}