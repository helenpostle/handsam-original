<?php

  /** 
   *  A Custom Error Handling Function.
   *  Rather than showing Errors on the Page, 
   *  it logs the error and sets some variables which can be used later.
   */
function myErrorHandler($errno, $errstr, $errfile, $errline) {
	switch ($errno) {
   		case E_ERROR:
  			//still have to abort for real errors.
   			echo "<b>FATAL</b> [$errno] $errstr<br />\n";
   			//echo "  Fatal error in line $errline of file $errfile";
   			//echo ", PHP " . PHP_VERSION . " (" . PHP_OS . ")<br />\n";
   			echo "Aborting...<br />\n";
   			logError($errno, $errstr, $errfile, $errline, false);
   			exit(1);
   			break; 
   		case 2048:	
   			//this is 'STRICT STANDARDS' error reporting, we don't need it really.
   		case E_NOTICE:
   			//log notices but don't flag error.
   			logError($errno, $errstr, $errfile, $errline, false);
   			break;
   		case E_WARNING:
   			//log notices but don't flag error.
   			logError($errno, $errstr, $errfile, $errline, true);
   			break;
   		default:
   			//log error and set flag to change content
   			logError($errno, $errstr, $errfile, $errline, true);
   			
   			break;
  	
 	}
	return true;	
	
}
  
  /** 
   *  log Errors to a file
   *
   *  if flag = true then set a flag which can be checked to see if an error has occured.
   */
function logError($errno, $errstr, $errfile, $errline, $flag) {
	global $errorFlag;
	global $logDir;
	//global $PHP_SELF;
	global $clsUS;
	global $installation_ref;
	global $errorlevels;
	$PHP_SELF = $_SERVER['PHP_SELF'];
	if (isset($clsUS)) {
		$accountid = $clsUS->accountid;
		$userid = $clsUS->userid;
	} else {		
		$accountid = 0;
		$userid = 0;
		
	}
	
	if ($flag) {
		 $errorFlag = $errstr;
	}
	$file = "$logDir/$installation_ref"."_error.log";
	 
	$fp = @fopen ($file, "a+");
	$log = date("d M Y G:i:s")."\t".$errorlevels[$errno]."\t$PHP_SELF \t $errstr on Line $errline of $errfile \t accountid: ".$accountid."\t userid: ".$userid."\n";
	//echo "<br/><br/>$log";
	@fwrite ($fp,$log);
	@fclose($fp);
}	


  
 /*
function handleShutdown() {
        $error = error_get_last();
        if($error !== NULL){
            //$info = "[SHUTDOWN] file:".$error['file']." | ln:".$error['line']." | msg:".$error['message'] .PHP_EOL;
            logError(1, $error['message'], $error['file'], $error['line'], true);
            //yourPrintOrMailFunction($info);
        } else{
            //yourPrintOrMailFunction("SHUTDOWN");
        }
    }
*/
?>