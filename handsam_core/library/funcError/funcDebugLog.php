<?php
/* debug logging function
*/


function debugLog($con, $clsUS, $pageVars, $qry, $hacking_arr, $state, $errorFlag) {
	global $debug_logging, $debug_logging_userids, $debug_logging_accountids;
	if($debug_logging) {
		if (isLoggedin() && (in_array($clsUS->userid, $debug_logging_userids) || in_array($clsUS->accountid, $debug_logging_accountids) || (count($debug_logging_userids) == 0 && count($debug_logging_accountids) == 0))) {
			$debug_qry = new dbInsert("debugging");
			$debug_qry->setNumberVal("visitid",$clsUS->visitid,"visitid");
			$debug_qry->setNumberVal("userid",$clsUS->userid,"userid");
			$debug_qry->setNumberVal("accountid",$clsUS->accountid,"accountid");
			$debug_qry->setStringVal("url",$_SERVER['PHP_SELF'],"url");
			$debug_qry->addValue("created","NOW()");
			if ($state != "") $debug_qry->setStringVal("state","HACKING","state");
			if (isset($arraySQL) && is_array($arraySQL)) $debug_qry->setStringVal("qry",implode("\n",$arraySQL),"sql_array");
			if ($errorFlag != "") $debug_qry->setStringVal("errors",$errorFlag,"errors");
			if (isset($pageVars)) {
				//$page_vars = print_r($pageVars, true);
				$page_vars = serialize($pageVars);
				$debug_qry->setStringVal("page_vars",$page_vars,"page_vars");
			}
			if (isset($_POST)) {
				//$page_vars = print_r($pageVars, true);
				$page_post = serialize($_POST);
				$debug_qry->setStringVal("post",$page_post,"page_post");
			}
			if (isset($_GET)) {
				//$page_vars = print_r($pageVars, true);
				$page_get = serialize($_GET);
				$debug_qry->setStringVal("page_get",$page_get,"page_get");
			}
			
			$debug_qry->setStringVal("hacking",serialize($hacking_arr),"hacking");
			execSQL($con, $debug_qry->getSQL());
			return false;
			
			$debug_qry->execute($con);
		}
	}
}
