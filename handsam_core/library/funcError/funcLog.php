<?php
function doLog($type,$msg) {
	global $messages, $debug, $logPath, $logfile_username;
	$logFile = $logPath."/task.".date("Y-m-d").".log";
	
	
	//log everything but titles and debug if not in debug mode
	//if (($debug == true || $type != "DEBUG")) {
		//echo "<br/><br/>$logFile $type \t $msg";
		$new_file = true;
		if (file_exists($logFile)) $new_file = false;
		$fp = @fopen($logFile,"a");	//open for writing, at end of file.
		@fwrite($fp,date("r")." \t $type \t $msg \n");	
		@fclose($fp);
		if ($new_file) {
			//@chown($logFile, $logfile_username); // this causes an error so removed
	 	 	@chgrp($logFile, $logfile_username);
	 	 	@chmod($logFile, 0775);

		}
	//}
	
	if ($type == "ERROR") {
		$msg = "<font color='red'>$msg</font>";	
	} else if ($type == "DEBUG") {
		$msg = "<i>$msg</i>";	
	} else if ($type =="TITLE") {
		$msg = "<b>$msg</b>";	
	}
	
	
	//this is for the output and email.
	if ($debug == true || $type != "DEBUG") {
		$messages[] = $msg;
	}
}


?>