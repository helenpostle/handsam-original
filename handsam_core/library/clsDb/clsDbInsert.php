<?php
/**
 * Builds an SQL insert statement
 */
 
 class dbInsert extends dbAction {
	var $cols = "";	//the list of cols
	var $vals = ""; //the list of vals
	var $requireWhere = false;
	
	
	//add values to update string
	function addValue($name,$value) {
		$this->cols = $this->cols.", $name";
		$this->vals = $this->vals.", $value";
	}
	
	function setAudit($usrname) {
		$this->setStringVal("created_by",$usrname);
		$this->addValue("created","NOW()");	
	}
	//return update string
	function getSql() {
		if (strlen($this->cols) == 0) {
			return "";
		} else {
			return "insert into ".$this->table." (".substr($this->cols,1).") values (".substr($this->vals,1).")";
		} 
	}	
}
?>
