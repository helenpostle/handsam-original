<?php
/**
 * Abstract-ish Class which is extended to make the three kinds of SQL statements.
 */

class dbAction {
	
	var $table = "";	  //table name we are updating
	var $failError = "";  //any error which will stop action working
	var $where = "";	  //sql where clause
	var $newID;			  //the insert id for an auto_number insert
	var $requireWhere = true;
	var $limit = ""; //limit clause for delete
	
	function dbAction($t) {
		//initiates the class
		$this->table = $t;
	}
	
	/**
	 * adds a value to the string as an update or insert as appropriate.
	 * overridden in individual classes.
	 */
	function addValue($name,$value) {
	}
	
	/**
	 * Set a limit after the  where clause
	 */
	function setLimit($value) {
	}

	/**
	 * records the date and username who did the action
	 * overridden in individual classes.
	 */
	function setAudit($usrname) {
	}
	
	/**
	 * Set a string parameter in the where clause
	 */
	function setStringParam($name,$value, $and="and") {
		$this->setParam($name,"'$value'", $and);
	}

		
	/**
	 * Set a string parameter in the where clause
	 */
	function setStringParamNotEqual($name,$value) {
		$this->setParamNotEqual($name,"'$value'");
	}
	
	/**
	 * Set a parameter in the where clause
	 */
	function setParam($name,$value, $and="and") {
		if (strlen($this->where) == 0) {
			$this->where = "where $name = $value";
		} else {
			$this->where .= " $and $name = $value";
		}
	}
	/**
	 * Set a null parameter in the where clause
	 */
	function setNullParam($name) {
		if (strlen($this->where) == 0) {
			$this->where = "where $name is null";
		} else {
			$this->where .= " and $name is null";
		}
	}
	
	/**
	 * Set a parameter in the where clause
	 */
	function setParamInArray($name,$array) {
		if (strlen($this->where) == 0) {
			$this->where = "where $name in (".implode(',', $array).") ";
		} else {
			$this->where .= " and $name in (".implode(',', $array).") ";
		}
	}

  /**
	 * Set a parameter in the where clause
	 */
	function setStringParamInArray($name,$array) {
		if (strlen($this->where) == 0) {
			$this->where = "where $name in ('".implode("','", $array)."') ";
		} else {
			$this->where .= " and $name in ('".implode("','", $array)."') ";
		}
	}

	/**
	 * Set a parameter NOT in the where clause
	 */
	function setParamNotInArray($name,$array) {
		if (count($array) == 0) return;
		if (strlen($this->where) == 0) {
			$this->where = "where $name not in (".implode(',', $array).") ";
		} else {
			$this->where .= " and $name not in (".implode(',', $array).") ";
		}
	}


	/**
	 * Set a parameter in the where != clause
	 */
	function setParamNotEqual($name,$value) {
		if (strlen($this->where) == 0) {
			$this->where = "where $name != $value";
		} else {
			$this->where .= " and $name != $value";
		}
	}
	
	/**
	 * Set a parameter graeter than
	 */
	function setParamGT($name,$value) {
		if (strlen($this->where) == 0) {
			$this->where = "where $name > $value";
		} else {
			$this->where .= " and $name > $value";
		}
	}
	
	
	function setReqNumberVal($name, $value, $title, $custom_err="") {
		if ($value === "") {
			if ($custom_err == "") {
				$this->failError .= "Please enter a value for $title.\r\n";
			} else {
				$this->failError .= "$custom_err\r\n";
			}
		} else {
			$this->setNumberVal($name, $value, $title);
		}
	}
	
	function setReqPositiveNumberVal($name, $value, $title) {
		if ($value == "" || !($value > 0)) {
			$this->failError .= "Please enter a positive numeric value for $title.\r\n";
		} else {
			$this->setNumberVal($name, $value, $title);
		}
	}
	
	function setNumberVal($name, $value, $title) {
		//check it is a number here if not throw error
		if ($value == "") $value = 0;
		if (is_numeric($value)) {
			$this->addValue($name,$value);
		} else {
			$this->failError .= "$title must be a valid number.\r\n";
		}
	}

	
	function setTableColVal($name, $value, $title) {
		//check it is a number here if not throw error
		if ($value != "") {
			$this->addValue($name,$value);
		} else {
			$this->failError .= "$title must be a valid table column.\r\n";
		}
	}
	
	/**
	 * Set a required date value
	 */
	function setReqDateVal($name, $value, $title, $param="") {
		//check all parts of date supplied
		if ($value == "") {
			$this->failError .= "You must enter a valid date for $title\r\n";	
		} else {
			$this->setDateVal($name, $value, $title, $param);
		}
	}
	/**
	 * Set a date value which can be null
	 *
	 * @var string
	 * @var string in the format: Sat, 05 Jan 2012 (php code: D, d M Y)
	 * @var string Human readable name of the element
	 * @var string
	 *
	 * @return void
	 */
	function setDateVal($name, $value, $title, $param="") {
		if ($value == "") {
			$this->setNullVal($name);
		} else if ($value == "DATE_LATE") {
			$this->failError .= "You must enter a date for $title that is before the account licence expiry date on ".displayDate($param).".\r\n";	
			
		} else {
			$date = strtotime($value);
			$dcheck = strftime("%a, %d %b %Y",$date); // e.g. Sat, 05 Jan 2012
			//check all parts of date supplied
			if ($dcheck == $value) {
				$prepval = "'".strftime("%Y-%m-%d",$date)."'";
				$this->addValue($name,$prepval);
			} else {
				$this->failError .= "You must enter a valid date for $title: $value\r\n";
				$this->failError .= 'date '.$date.' dcheck '.$dcheck.'<br>';	
			}
		}
	}

	/**
	 * Set a required date and time value
	 */
	function setReqDateTimeVal($name, $value, $title, $param="") {
		//check all parts of date supplied
		if ($value == "") {
			$this->failError .= "You must enter a valid date for $title.\r\n";	
		} else {
			$this->setDateTimeVal($name, $value, $title, $param);
		}
	}
	
	/**
	 * Set a date and time value which can be null
	 */
	function setDateTimeVal($name, $value, $title, $param="") {
		if ($value == "") {
			$this->setNullVal($name);
		} else if ($value == "DATE_LATE") {
			$this->failError .= "You must enter a date for $title that is before the account licence expiry date on ".displayDate($param).".\r\n";	
			
		} else {
			$date = strtotime($value);
			$dcheck =  strftime("%a, %d %b %Y %H:%M:%S",$date);
			//check all parts of date supplied
			if ($dcheck == $value) {
				$prepval = "'".strftime("%Y-%m-%d %H:%M",$date)."'";
				$this->addValue($name,$prepval);
			} else {
				$this->failError .= "You must enter a valid date for $title.\r\n";	
			}
		}
	}	
	
		
	function setNullVal($name) {
		$this->addValue($name,"NULL");
	}
	
	function setStringVal($name, $value) {
		global $nohtml_page_purifier;
		//prepare string value for insert or update
		//$value = strip_tags($value);
		$clean_str = $nohtml_page_purifier->purify( $value );		
		$prepval = "'".escapeString(trim($clean_str))."'";
		$this->addValue($name, $prepval);
	}
	
	function setReqStringVal($name,$value,$title) {
		if ($value != "") {
			$this->setStringVal($name,$value);
		} else{
			$this->failError .= "Please enter a value for $title.\r\n";
		}
	}
	
	function setHtmlVal($name, $value) {
		global $allow_html_purifier;
		//prepare string value for insert or update
		//$value = strip_tags($value);
		$clean_html = $allow_html_purifier->purify( $value );		
		$prepval = "'".escapeString(trim($clean_html))."'";
		$this->addValue($name,$prepval);
	}
	
	function setReqHtmlVal($name,$value,$title) {
		if ($value != "") {
			$this->setHtmlVal($name,$value);
		} else{
			$this->failError .= "Please enter a value for $title.\r\n";
		}
	}
	
	
	function setEmailVal($name, $value) {
		if (validEmail($value)) {
			$this->setStringVal($name,$value);
		}
	}
	
	function setReqEmailVal($name,$value,$title) {
		if ($value != "") {
			$this->setEmailVal($name,$value);
		} else{
			$this->failError .= "Please enter a valid email for $title.\r\n";
		}
	}
	
	function setPassReqStringVal($name,$value,$title) {
		if ($value != "") {
			//$this->addValue($name,"password('$value')");
			//$this->addValue($name,$value);
			$this->setStringVal($name,$value);
		} else {
			$this->failError .= "Please enter a value for $title.\r\n";
		}
	}	
	
	function setBooleanVal($name,$value,$title) {
		global $booleanTrue;
		global $booleanFalse;
		
		if ($booleanTrue == 1) {
			if (isset($value) && $value) {
				$this->setNumberVal($name, 1, "Boolean Field Error");
			} else {
				$this->setNumberVal($name, 0, "Boolean Field Error");
			}
		} else {
			if ($value) {
				$this->setReqStringVal($name, $booleanTrue, "Boolean Field Error");
			} else {
				$this->setReqStringVal($name, $booleanFalse, "Boolean Field Error");
			}
			
		}
	}
	
	function setReqBooleanVal($name,$value,$title) {
		global $booleanTrue;
		global $booleanFalse;
		
		if ($booleanTrue == 1) {
			if ($value) {
				$this->setNumberVal($name, 1, "Boolean Field Error");
			} else {
				$this->failError .= "Please check the $title field.\r\n";
			}
		} else {
			if ($value) {
				$this->setReqStringVal($name, $booleanTrue, "Boolean Field Error");
			} else {
				$this->failError .= "Please check the $title field.\r\n";
			}
			
		}
	}	
	
	function getNewID() {
		return $this->newID;
	}
	
	/** execute returns true if successful. If false, check getError() to see why failed */
	function execute($con, $history=true) {
		global $pageFiles;
        /*
		if (isset($pageFiles) && is_object($pageFiles)) {
			if ($pageFiles->csrf_form == 'form') {
				$csrf_tok = new csrfSecurity('form');
				$csrf_tok->check_token($con);
				//if ($csrf_tok->verify == false) die("Sorry, there has been an error with this form. Please contact the administrator");
				
			};
		}
        */

		if ($this->requireWhere && $this->where == "") {
			$this->failError .= "No WHERE clause set for update/delete query.\r\n";
			return false;
		} else if ($this->failError != "") {
			//there was a validation error if this already contains something.
			return false;
		} else {
			//try to execute the sql here, return false if fail and set error message.
			$result = execSQL($con, $this->getSQL());
			if ($result == "") {
				$this->newID = get_last_insert_id();
				return true;
			} else {
				$this->failError .= "Error executing SQL: ".$result;
				return false;
			}
			
		}
	}
	
	//check this after execute
	function getError() {
		return $this->failError;
	}
	
	//override this in individual action classes
	function getSql() {
		return $this->sql;
	}	
}
?>
