<?php
/**
 * Builds an SQL update statement
 */
class dbUpdate extends dbAction {
	var $sql = "";	//the sql statement that we will build
	
	//add values to update string
	function addValue($name,$value) {
		$this->sql = $this->sql.", $name = $value";
	}
	
	function setAudit($usrname) {
		$this->setStringVal("edited_by",$usrname);
		$this->addValue("edited","NOW()");	
	}
	
	//return update string
	function getSql() {
		if (strlen($this->sql) == 0) {
			return "";
		} else {
			return "update ".$this->table." set ".substr($this->sql,1)." ".$this->where;
		} 
	}
    
    
	/** execute returns true if successful. If false, check getError() to see why failed */
	function execute($con, $history=true) {
		global $pageFiles;
		if (isset($pageFiles) && is_object($pageFiles)) {
			if ($pageFiles->csrf_form == 'form') {
				$csrf_tok = new csrfSecurity('form');
				$csrf_tok->check_token($con);
				//if ($csrf_tok->verify == false) die("Sorry, there has been an error with this form. Please contact the administrator");
				
			};
		}
		if ($this->requireWhere && $this->where == "") {
			$this->failError .= "No WHERE clause set for update/delete query.\r\n";
			return false;
		} else if ($this->failError != "") {
			//there was a validation error if this already contains something.
			return false;
		} else {
			//try to execute the sql here, return false if fail and set error message.
      if ($history == true) $save_history = new dbHistory($this->where, $this->table, $con);
			$result = execSQL($con, $this->getSQL());
			if ($result == "") {
				$this->newID = get_last_insert_id();
                //insert hook here for recording history of some tables.
                if ($history == true) $save_history->save($con);
				return true;
			} else {
				$this->failError .= "Error executing SQL: ".$result;
				return false;
			}
			
		}
	}
    
    
    
}
?>
