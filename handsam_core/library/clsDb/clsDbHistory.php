<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

class dbHistory {
	
	var $table;	  //table name we are saving
	var $where;
    var $tables = array('tasklist', 'taskcategory', 'task', 'accounttask', 'files', 'links', 'training_log', 'content');
    var $save = false;
    var $rs;
    var $rs_items = array(); // an array of items that can be contained by the main $rs such as files within a task
    var $rs_task_items = array(); // an array of items that can be contained by the main $rs such as files within a task
    var $rs_accounttask_items = array(); // an array of items that can be contained by the main $rs such as files within an accounttask
    
	function dbHistory($where, $t, $con) {
		//initiates the class
        if (in_array($t, $this->tables)) {
            $this->table = $t;
            $this->where = $where;
            $this->save = true;
            $this->init($con);
            
        }
	}
    
    
    function init($con) {
        $qry = "select * from {$this->table} {$this->where}";
        $this->rs = getRS($con, $qry);
        $method = $this->table."_items";
        if (method_exists($this, $method)) {
            $this->$method($con);
        }
        
    }
    
    function save($con) {
        if ($this->save) {
            $table = $this->table;
            $this->$table($con);
            
        }
    }
	
    function tasklist($con) {
        global $messages;
        while ($row = getRow($this->rs)) {
            $qry = new dbInsert('tasklist_history');
            $qry->setStringVal("customtext",$row["customtext"],"Custom Tasklist name");
            $qry->setStringVal("tasklistname",$row["tasklistname"],"Tasklist name");
            $qry->setNumberVal("tasklistid",$row["tasklistid"],"Tasklist ID");
            $qry->setNumberVal("mid",$row["mid"],"MID");
            $qry->setStringVal("state",$row["state"],"State");
            $qry = $this->audit($qry, $row);
            if (!$qry->execute($con)) $messages[] = $qry->getError();
        }
    }

    
    function taskcategory($con) {
        global $messages;
        while ($row = getRow($this->rs)) {
            $qry = new dbInsert('taskcategory_history');
            $qry->setNumberVal("mid",$row["mid"],"MID");
            $qry->setNumberVal("tasklistid",$row["tasklistid"],"Tasklist ID");
            $qry->setStringVal("customtext",$row["customtext"],"Custom category name");
            $qry->setStringVal("categoryname",$row["categoryname"],"Category name");
            $qry->setNumberVal("categoryid",$row["categoryid"],"Category ID");
            $qry->setNumberVal("group_id",$row["group_id"],"Group ID");
            $qry->setStringVal("state",$row["state"],"State");
            $qry = $this->audit($qry, $row);
            if (!$qry->execute($con)) $messages[] = $qry->getError();
        }
    }
   
    
    function task($con) {
        global $messages;
        while ($row = getRow($this->rs)) {
            $qry = new dbInsert('task_history');
            $qry->setNumberVal("mid",$row["mid"],"MID");
            $qry->setNumberVal("tasklistid",$row["tasklistid"],"Tasklist ID");
            $qry->setStringVal("customtext",$row["customtext"],"Custom task text");
            $qry->setStringVal("tasktext",$row["tasktext"],"Task text");
            $qry->setNumberVal("categoryid",$row["categoryid"],"Category ID");
            $qry->setNumberVal("taskid",$row["taskid"],"Task ID");
            $qry->setNumberVal("taskday",$row["taskday"],"Task Day");
            $qry->setNumberVal("taskmonth",$row["taskmonth"],"Task Month");
            $qry->setStringVal("state",$row["state"],"State");
            
            if(isset($row['key_task']) && $row['key_task'] == 1) {
                $qry->setNumberVal('key_task', 1, 'Key Task');
            } else {
                $qry->setNumberVal('key_task', 0, 'Key Task');
            }
            
            if(isset($row['key_task_lock']) && $row['key_task_lock'] == 1) {
                $qry->setNumberVal('key_task_lock', 1, 'Key Task Lock');
            } else {
                $qry->setNumberVal('key_task_lock', 0, 'Key Task Lock');
            }            
            
           //get items for this task
            $items = $this->getTaskItems($row['taskid']);
            $qry->setStringVal("items",json_encode($items),"Items");
            $qry = $this->audit($qry, $row);
           if (!$qry->execute($con)) $messages[] = $qry->getError();
       }
   }
    
    function task_items($con) {
        global $messages;
        while ($row = getRow($this->rs)) {
            //get items for this task
            //get files
            $qry = "select files.* from files left join task_files on files.fileid = task_files.fileid where task_files.taskid = {$row['taskid']}";
            $this->rs_items[$row['taskid']]['files'][] = getRS($con, $qry);
            
            //get links
            $qry = "select links.* from links left join task_links on links.linkid = task_links.linkid where task_links.taskid = {$row['taskid']}";
            $this->rs_items[$row['taskid']]['links'][] = getRS($con, $qry);            
        }
        resetRS($this->rs);
    }

    
    function accounttask($con) {
        global $messages;
        while ($row = getRow($this->rs)) {
            $qry = new dbInsert('accounttask_history');
            $qry->setNumberVal("accounttaskid",$row["accounttaskid"],"Account Task ID");
            if ($row["actiondate"] != "") {
                $qry->setStringVal("actiondate",$row["actiondate"],"Action date");
            } else {
                $qry->setNullVal("actiondate");
            }
            if ($row["completedate"] != "") {
                $qry->setStringVal("completedate",$row["completedate"],"Complete date");
            } else {
                $qry->setNullVal("completedate");
            }
            
            if ($row["budget"] != "") {
                $qry->setStringVal("budget",$row["budget"],"budget");
            } else {
                $qry->setNullVal("budget");
            }
            
            if(isset($row['key_task']) && $row['key_task'] == 1) {
                $qry->setNumberVal('key_task', 1, 'Key Task');
            } else {
                $qry->setNumberVal('key_task', 0, 'Key Task');
            }
            
            if(isset($row['key_task_lock']) && $row['key_task_lock'] == 1) {
                $qry->setNumberVal('key_task_lock', 1, 'Key Task Lock');
            } else {
                $qry->setNumberVal('key_task_lock', 0, 'Key Task Lock');
            }            
                        
            $qry->setStringVal("notes",$row["notes"],"Notes");
            $qry->setStringVal("customtext",$row["customtext"],"Custom task text");
            $qry->setStringVal("tasktext",$row["tasktext"],"Task text");
            $qry->setNumberVal("categoryid",$row["categoryid"],"Category ID");
            $qry->setNumberVal("taskid",$row["taskid"],"Task ID");
            $qry->setNumberVal("licenceid",$row["licenceid"],"licence ID");
            $qry->setNumberVal("assignedto",$row["assignedto"],"Assigned to");
            $qry->setNumberVal("completedby",$row["completedby"],"Completed by");
            $qry->setStringVal("state",$row["state"],"State");
            $qry->setStringVal("feedback",$row["feedback"],"Feedback");
            $qry->setNumberVal("late_task_email_alert",$row["late_task_email_alert"],"Late Task Email Alert");
           
            //get items for this task
            $items = $this-> getTaskItems($row['accounttaskid']);
            $qry->setStringVal("items",json_encode($items),"Items");
            $qry = $this->audit($qry, $row);
           if (!$qry->execute($con)) $messages[] = $qry->getError();
       }
   }
    
    
    function accounttask_items($con) {
        global $messages;
        
        while ($row = getRow($this->rs)) {
            
            //get items for this task
            //get accounttask files
            $qry = "select files.* from files left join task_files on files.fileid = task_files.fileid where task_files.accounttaskid = {$row['accounttaskid']}";
            $this->rs_items[$row['accounttaskid']]['files'][] = getRS($con, $qry);          
            //get accounttask links
            $qry = "select links.* from links left join task_links on links.linkid = task_links.linkid where task_links.accounttaskid = {$row['accounttaskid']}";
            $this->rs_items[$row['accounttaskid']]['links'][] = getRS($con, $qry);
            
            if ($row['taskid'] > 0 && !array_key_exists($row['taskid'], $this->rs_task_items)) {
                //get task files
                $qry = "select files.* from files left join task_files on files.fileid = task_files.fileid where task_files.taskid = {$row['taskid']}";
                $this->rs_task_items[$row['taskid']]['files'][] = getRS($con, $qry);
                //$this->rs_items[$row['accounttaskid']]['files'][] = getRS($con, $qry);            
                //get task links
                $qry = "select links.* from links left join task_links on links.linkid = task_links.linkid where task_links.taskid = {$row['taskid']}";
                //$this->rs_items[$row['accounttaskid']]['links'][] = getRS($con, $qry);            
                $this->rs_task_items[$row['taskid']]['links'][] = getRS($con, $qry);
                
            }
        }
        resetRS($this->rs);
        
    }
    
    
    function getTaskItems($taskid) {
        $this->rs_items = array_merge($this->rs_task_items,$this->rs_items);
        $items = array();
        if (array_key_exists($taskid, $this->rs_items)) {
            if (array_key_exists('files', $this->rs_items[$taskid])) {
                 foreach ($this->rs_items[$taskid]['files'] as $key => $file_rs) {
                     //process rs
                     while ($file_row = getRow($file_rs)) {
                         $items['files'][$file_row['fileid']] = array(
                             'fileid' => $file_row['fileid'], 
                             'filename' => $file_row['filename'],
                             'accountid' => $file_row['accountid'],
                             'filetype' => $file_row['filetype'],
                             'title' => $file_row['title'],
                             'description' => $file_row['description'],
                             'filecategory' => $file_row['filecategory'],
                             'mid' => $file_row['mid'],
                             'created' => $file_row['created'],
                             'created_by' => $file_row['created_by'],
                             'edited' => $file_row['edited'],
                             'edited_by' => $file_row['edited_by']
                         );
                     }            
                 }
            }
            if (array_key_exists('links', $this->rs_items[$taskid])) {
                 foreach ($this->rs_items[$taskid]['links'] as $key => $file_rs) {
                     //process rs
                     while ($file_row = getRow($file_rs)) {
                         $items['links'][$file_row['linkid']] = array(
                             'linkid' => $file_row['linkid'], 
                             'url' => $file_row['url'],
                             'accountid' => $file_row['accountid'],
                             'linktype' => $file_row['linktype'],
                             'title' => $file_row['title'],
                             'description' => $file_row['description'],
                             'linkcategory' => $file_row['linkcategory'],
                             'mid' => $file_row['mid'],
                             'created' => $file_row['created'],
                             'created_by' => $file_row['created_by'],
                             'edited' => $file_row['edited'],
                             'edited_by' => $file_row['edited_by']
                         );
                     }            
                 }
            }
        }
        return $items;
    }
    
    
    
    function files($con) {
        global $messages;
        while ($row = getRow($this->rs)) {
            $qry = new dbInsert('file_history');
            $qry->setNumberVal("mid",$row["mid"],"MID");
            $qry->setNumberVal("fileid",$row["fileid"],"File ID");
            $qry->setNumberVal("accountid",$row["accountid"],"Account ID");
            $qry->setStringVal("description",$row["description"],"File Description");
            $qry->setStringVal("title",$row["title"],"File Title");
            $qry->setStringVal("filetype",$row["filetype"],"File Type");
            $qry->setStringVal("filecategory",$row["filecategory"],"File cat");
            $qry->setStringVal("filename",$row["filename"],"Filename");
            $qry = $this->audit($qry, $row);
            if (!$qry->execute($con)) $messages[] = $qry->getError();
        }
    }

    
    function links($con) {
        global $messages;
        while ($row = getRow($this->rs)) {
            $qry = new dbInsert('link_history');
            $qry->setNumberVal("mid",$row["mid"],"MID");
            $qry->setNumberVal("linkid",$row["linkid"],"Link ID");
            $qry->setNumberVal("accountid",$row["accountid"],"Account ID");
            $qry->setStringVal("description",$row["description"],"Link Description");
            $qry->setStringVal("title",$row["title"],"Link Title");
            $qry->setStringVal("linktype",$row["linktype"],"Link Type");
            $qry->setStringVal("linkcategory",$row["linkcategory"],"Link Cat");
            $qry->setStringVal("url",$row["url"],"Url");
            $qry = $this->audit($qry, $row);
            if (!$qry->execute($con)) $messages[] = $qry->getError();
        }
    }

    
    
    
    
    
    function audit($qry, $row) {
        $qry->setNumberVal("created_by",$row["created_by"],"Created By");
        $qry->setStringVal("created", $row["created"], "Created");
        if (($row["edited_by"] !== null)) {
            $qry->setNumberVal("edited_by", $row["edited_by"],"Edited By");
            $qry->setStringVal("edited", $row["edited"], "Edited");
        } else {
            $qry->setNullVal("edited_by");
            $qry->setNullVal("edited");
        }
        return $qry;
       
    }
    
    function training_log($con) {
        global $messages;
        while ($row = getRow($this->rs)) {
            $qry = new dbInsert('training_log_history');
            $qry->setNumberVal("training_id",$row["training_id"],"Training log ID");
            $qry->setNumberVal("accountid",$row["accountid"],"Account ID");
            $qry->setNumberVal("course_id",$row["course_id"],"Course ID");
            $qry->setNumberVal("provider_id",$row["provider_id"],"Provider ID");
            if ($row["course_date"] != "") {
                $qry->setStringVal("course_date",$row["course_date"],"Course date");
            } else {
                $qry->setNullVal("course_date");
            }
            if ($row["renewal_date"] != "") {
                $qry->setStringVal("renewal_date",$row["renewal_date"],"Renewal date");
            } else {
                $qry->setNullVal("renewal_date");
            }
            $qry->setStringVal("comments",$row["comments"],"comments");
            $qry->setNumberVal("cost",$row["cost"],"cost");
            $qry = $this->audit($qry, $row);
            $qry->setNumberVal("trainee_id",$row["trainee_id"],"Trainee ID");
            if ($row["renewal_reminder_date"] != "") {
                $qry->setStringVal("renewal_reminder_date",$row["renewal_reminder_date"],"Renewal reminder date");
            } else {
                $qry->setNullVal("renewal_reminder_date");
            }
            $qry->setStringVal("state",$row["state"],"State");
            $qry->setNumberVal('user_renewal_email', $row["user_renewal_email"], 'user_renewal_email');
            $qry->setNumberVal('admin_renewal_email', $row["admin_renewal_email"], 'admin_renewal_email');
            $qry->setNumberVal('user_refresher_email', $row["user_refresher_email"], 'user_refresher_email');
            $qry->setNumberVal('admin_refresher_email', $row["admin_refresher_email"], 'admin_refresher_email');
            $qry->setNumberVal('pass', $row["pass"], 'pass');
            $qry->setNumberVal('score', $row["score"], 'score');
            $qry->setNumberVal('max_score', $row["max_score"], 'max_score');
            $qry->setNumberVal('csv_import', $row["csv_import"], 'csv_import');
            $qry->setStringVal("certificate",$row["certificate"],"certificate");
            $qry->setStringVal("flag",$row["flag"],"flag");
            
           if (!$qry->execute($con)) $messages[] = $qry->getError();
       }
   }
    
   
   function content($con) {
        global $messages;
        while ($row = getRow($this->rs)) {
            $qry = new dbInsert('content_history');
            $qry->setNumberVal("mid",$row["mid"],"MID");
            $qry->setNumberVal("contentid",$row["contentid"],"Content ID");
            $qry->setReqStringVal("title",$row["title"],"Content Title");
            $qry->setReqHtmlVal("contenttext",$row["contenttext"],"Content Text");
            $qry->setReqStringVal("state",$row["state"],"state");
            $qry->setStringVal("meta_title",$row["meta_title"],"Meta Title");
            $qry->setStringVal("meta_keywords",$row["meta_keywords"],"Meta Keywords");
            $qry->setStringVal("meta_desc",$row["meta_desc"],"Meta Description");
            $qry->setNumberVal("parentid", $row["parentid"], "parent id");
            $qry = $this->audit($qry, $row);
            if (!$qry->execute($con)) $messages[] = $qry->getError();
        }
    }
}
?>
