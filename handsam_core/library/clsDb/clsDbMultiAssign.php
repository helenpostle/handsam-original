<?php
/*
*
* 
*
*/
class dbMultiAssign  {    

	var $form = array(); //from form
	var $db = array();	//from db
	var $del = array();  //ids of items to insert
	var $add = array();	//ids of items to delete
    
    function dbMultiAssign($form_array, $qry, $con) {
	    if (isset($form_array)) $this->form = $form_array;
	    $this->getDbArr($qry, $con);
	    $this->process();
    }
    
    
    
    function process() {
		foreach ($this->form as $i) {
			if (in_array($i, $this->del)) {
				//remove from array
				$this->del = array_diff($this->del, array($i));
			} else {
				//insert tag
				$this->add[] = $i;
			}
		}
	}
   
    
    
     
    //queries db to get array of items
    function getDbArr($qry, $con) {
  	    $rs = getRS($con,$qry);
       	while($row = getRow($rs)) {
			$this->db[$row['a']] = $row['b'];
       	}
       	$this->del = $this->db;
    }
    
    
}
?>