<?php

/**
 * Extends PDO and provides a nice(r) interface.
 * 
 * Taken from http://leftnode.com/entry/the-last-php-pdo-library-you-will-ever-need.html
 * 
 * As this extends the PHP PDO library, it can be used with native PDO methods
 * 
 * Use thus:
 * $db = db::connect()
 * 
 * examples:
 * $sql = "INSERT INTO table (the_name, the_desc) VALUES (:the_name, :the_desc);";
 * $vals = array('the_name' => $the_name_val, 'the_desc' => $the_desc_val);
 * 
 * $db->modify($sql, $vals);
 * 
 * ===
 *  
 * $sql = "SELECT the_name, the_desc FROM the_table WHERE id = :id;";
 * $vals = array('id' => $id_val);
 * 
 * $row = $db->fetch_one($sql, $vals); // $row is an object 
 * 
 */

class db extends PDO {
    
    protected static $_db = null;
    
    public $query;
    
    public $params;
    
    /**
     * Table name
     * @var string
     */
    protected $_table;
    
    public function __construct($dsn, $username, $passwd)
    {
        parent::__construct($dsn, $username, $passwd);
    }
    
    /**
     * Provides a singleton for the DB object
     * @return object DB con
     */
    public static function connect()
    {
        if(self::$_db === null) 
            self::$_db = new db(Config::getItem('db_string'), Config::getItem('dbuser'), Config::getItem('password'));
        
        // set error config
        self::$_db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        return self::$_db;
    }

    public function fetch_all($query, $parameters = array())
    {
        $read_stmt = $this->prepare_and_execute($query, $parameters);

        $fetched_rows = $read_stmt->fetchAll(PDO::FETCH_CLASS);
        $read_stmt->closeCursor();

        unset($read_stmt);
        return($fetched_rows);
    }

    public function fetch_one($query, $parameters = array())
    {
        $read_stmt = $this->prepare_and_execute($query, $parameters);

        $fetched_row = $read_stmt->fetchObject();
        if(!is_object($fetched_row))
        {
            $fetched_row = false;
        }

        $read_stmt->closeCursor();
        unset($read_stmt);
        return($fetched_row);
    }

    public function fetch_column($query, $parameters = array(), $column = 0)
    {
        $column = abs((int) $column);

        $read_stmt = $this->prepare_and_execute($query, $parameters);
        $fetched_column = $read_stmt->fetchColumn($column);

        $read_stmt->closeCursor();
        unset($read_stmt);
        return($fetched_column);
    }

    public function modify($query, $parameters)
    {
        $this->query = $query;
        $this->params = $parameters;
        
        $modify_stmt = $this->prepare_and_execute($query, $parameters);
        
        return($modify_stmt->rowCount());
    }
    
    /**
     * Sets the table name
     * @param type $table
     * @return \db 
     */
    public function setTable($table)
    {
        $this->_table = $table;
        
        return $this;
    }
    

    /**
     * MG
     * 
     * Updates a record with an array of $field => $value
     * 
     * @param type $query
     * @param type $parameters
     * @param array in the format array('fieldname' => 'value')
     * @return \db
     */
    public function update(Array $data = array(), $where_field = null, $where_value = null)
    {
        $data = $this->audit($data);
        $keys = array_keys($data);
        $set = array();
        
        foreach($keys as $k)
            $set[] = $k.' = :'.$k;
        
        $sql = "UPDATE ".$this->_table;
        $sql .= " SET ".implode(', ', $set)." ";
        
        if(!is_null($where_field) && !is_null($where_value)) 
        {
            $sql .= " WHERE ".$where_field." = :where_value";
            $data['where_value'] = $where_value;
        }
        
        return $this->modify($sql, $data);        
    }
    
    /**
     * Insert an array of field => value into the DB
     * @param array $data
     * @return \db
     */
    public function insert(array $data)
    {
        $data = $this->audit($data);
        $fields = array_keys($data);
        
        $sql = "INSERT INTO `".$this->_table."` ";
        $sql .= " (".implode(', ', $fields).") ";
        
        // notice the first ':' before the implode
        // a bit hacky but will do
        $sql .= " VALUES (:".implode(', :', $fields).") ";
        
        return $this->modify($sql, $data);
    }
    
    /**
     * Add the auditing fields
     * Only added to tables which contain them
     * @param array $data 
     */
    public function audit($data)
    {
        if(in_array($this->_table, array('task', 'accounttask', 'taskcategory', 'tasklist')))
        {
            $data['edited'] = date('Y-m-d H:i:s');
            $data['edited_by'] = '0';
        }
        return $data;
    }
    
    /**
     * 
     * @param type $query
     * @param type $parameters
     * @return type 
     */
    private function prepare_and_execute($query, $parameters = array())
    {
        try{
            $prep_stmt = $this->prepare($query); 
            $prep_stmt->execute($parameters);
        
            return($prep_stmt);
        }
        catch (PDOException $err)
        {     
            $this->handleException($err);
            
            return false;
        }
    }
    
    /**
     * Logs the failed query in `query_log`
     * 
     * @param PDOException
     */
    private function handleException(PDOException $e)
    {        
        $ermsg = $e->getMessage();
        $ermsg .= "\r\n\r\nSQL: ".$this->query;
        $ermsg .= "\r\nParams: ".json_encode($this->params);
        $ermsg .= "\r\n\r\nStack Trace: ".  $e->getTraceAsString();
        
        // set to empty to disable the auto logging
        $this->query = '';
        
        $sql = "INSERT INTO query_log (query, timestamp, curl_log_rec_mid, is_error)
                VALUES (:query, NOW(), 0, 1);";
        $data['query'] = $ermsg;
        
        $stmnt = $this->prepare($sql);
        $stmnt->execute($data);
        
        // send a message to handsam support         
        $msg = "There was an error when syncing from Roger.";
        $msg .= "\r\n\r\nThe error is logged in query_log with the id of ".$this->lastInsertId();
        $msg .= " (SELECT * FROM query_log WHERE id = ".$this->lastInsertId().")";
        $msg .= "\r\n\r\n";
        $msg .= $ermsg;
        $msg .= "\r\n\r\nSent: ".date('Y-m-d H:i:s');
        
        // send the message
        mail('it@handsam.co.uk', 'Handsam Sync Error', $msg, 'From: Roger <roger@handsam.org.uk>');
    }

}
