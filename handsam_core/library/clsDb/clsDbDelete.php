<?php
/**
 * Builds an SQL delete statement
 */
class dbDelete extends dbAction {
	var $sql = "";	//the sql statement that we will build
	var $limit = ""; //limit clause for delete
	
	//add values to update string
	
	/**
	 * Set a limit after the  where clause
	 */
	function setLimit($value) {
		if (is_numeric($value)) {
			$this->limit = "limit ".$value;
		}
	}
	
	/**
     * Returns the SQL Statement
     * @return String
     */
	function getSql() {
		if (strlen($this->where) == 0) {
			return "";
		} else {
			return "delete from ".$this->table." ".$this->where." ".$this->limit;
		} 
	}	
}
?>
