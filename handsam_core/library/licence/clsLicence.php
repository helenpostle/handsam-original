<?php

/**
 * accountlicence class 
 * CRUD accountlicence table.
 *
 */
class Licence {
        
    public $licenceid;
    public $accountid;
    public $start_date;
    public $end_date;
    public $tasklistid;
    public $appid;
    public $state;
    public $clsUS;
    
    
    public function __construct($clsUS, $licenceid=0) {
        $this->licenceid = $licenceid;
        $this->clsUS = $clsUS;
    }
    
   
    public function get($con) {
        $qry = "SELECT accountlicence.*, date(accountlicence.start_date) as start_date, date(accountlicence.end_date) as end_date FROM accountlicence left join account on accountlicence.accountid = account.accountid where accountlicence.licenceid = ".$this->licenceid;
		$result = getRS($con, $qry);
		$row = getRow($result);
		$this->start_date = $row['start_date'];
		$this->end_date = $row['end_date'];
		$this->accountid = $row['accountid'];
		$this->tasklistid = $row['tasklistid'];
		$this->appid = $row['appid'];
		$this->state = $row['state'];
    }
    
    
    public function update($con, $val_arr) {
        global $core_conf;
        global $messages;
        $qry = new dbUpdate("accountlicence");
        $qry->setParam("licenceid", $this->licenceid);
        if (isset($val_arr['state'])) $qry->setReqStringVal("state", $val_arr['state'], "State");
        if (isset($val_arr['accountid'])) $qry->setReqNumberVal("accountid", $val_arr['accountid'], "Accountid");
        if (isset($val_arr['start_date'])) $qry->setReqDateVal("start_date", $val_arr['start_date'], "start date");
        if (isset($val_arr['end_date'])) $qry->setReqDateVal("end_date", $val_arr['end_date'], "end date");
        if (isset($val_arr['tasklistid'])) $qry->setNumberVal("tasklistid", $val_arr['tasklistid'], "tasklist id");
        if (isset($val_arr['appid'])) $qry->setReqNumberVal("appid", $val_arr['appid'], "app id");
        if (isset($val_arr['billing_date'])) $qry->setDateVal("billing_date", $val_arr['billing_date'], "billing date");
        if (isset($val_arr['contract_length'])) $qry->setNumberVal("contract_length", $val_arr['contract_length'], "contract length");
        if (isset($val_arr['all_tags'])) $qry->setNumberVal("all_tags", $val_arr['all_tags'], "all tags");	
        //process any module specific conf that is posted from the form $rootPath/modules/{$rowmod['folder']}/shared/licence_conf.php
        //get array of name/value pairs
        if (isset($val_arr['mod_conf_name']) && count($val_arr['mod_conf_name']) > 0) {
            $mod_conf_arr = array();
            for ($i=0; $i<count($val_arr['mod_conf_name']); $i++) {
                $mod_conf_arr[$val_arr['mod_conf_name'][$i]] = $val_arr['mod_conf_val_'.$i];
            }
            $qry->setReqStringVal("conf",json_encode($mod_conf_arr),"conf");
        }
        $qry->setAudit($this->clsUS->userid);
        if(!$qry->execute($con)) {
            $messages[] = $qry->getError();
            return false;
        } else {
            return true;
        }
    }
}

