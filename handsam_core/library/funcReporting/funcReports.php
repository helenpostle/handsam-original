<?php
	function getParam($name,$default) {
		if (getDateFromRequest($name) != null) {
			return getDateFromRequest($name);
		} else if (isset($_POST[$name])) {
			return getStrFromRequest($name);
		} else if (isset($_GET[$name])) {
			return getStrFromRequest($name);
		} else {
			return $default;
		}	
	}
    
    
    
    //calculate a class name depending on the result values
    function percentClass($result) {
        if ($result === "") {
              $cell_class = "none";
          } else if ($result < 50) {
              $cell_class = "low";
          } else if ($result < 75) {
              $cell_class = "med";
          } else {
              $cell_class = "high";
          }
          return $cell_class;
    }
    
    
    //calculate a class name depending on the result values
    function incidentClass($result) {
        $cell_class = "";
        if ($result > 0) $cell_class = "incident_red";
        return $cell_class;
    }    
?>