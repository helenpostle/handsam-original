<?php

//function outputs CSV file from results set ($r) with fields ($fieldList) that have $titleList titles. names the file $filename [.csv]
function downloadAsCSV($rsReport,$filename){
	
	//$total = getResultRows($r);
	header('Content-type: text/csv');
	header("Content-Disposition: attachment; filename=".$filename.".csv");
	$csv = "";

	$fields = mysql_num_fields($rsReport);
	
	$adj = 0;
	if (mysql_field_name($rsReport,0) == 'link' || mysql_field_name($rsReport,0) == 'link2') $adj = 1;
	if (mysql_field_name($rsReport,1) == 'link' || mysql_field_name($rsReport,1) == 'link2') $adj = 2;
	
	for ($i = $adj; $i < $fields; $i++) {
	    $csv .= mysql_field_name($rsReport,$i).",";
	}
	
	$csv = substr($csv,0,strlen($csv) -1);
	$csv .= "\r\n";	
	
	while($rowReport = mysql_fetch_row($rsReport)){
	
		for ($i = $adj; $i < $fields; $i++) {
		    $csv .=  "\"".$rowReport[$i]	."\",";
		}
		
		$csv = substr($csv,0,strlen($csv) -1);
		$csv .= "\r\n";		
	}
	
	echo $csv;
}
?>