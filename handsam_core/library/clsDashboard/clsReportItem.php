<?php
class reportItem {
	
	var $sql;
	var $result = array();
	var $ids = array();
	var $id_names = array();
	var $title1;
	var $title2;
	var $include_file;
	var $content_view;
	var $td_class;
	var $report_id;
	var $params = array();
	
	function reportItem($row,$con,$include_file_sql, $title2, $params) {
		$this->include_file = $row['include_file'];
		$this->content_view = $row['content_view'];
		$this->td_class = $row['td_class'];
		$this->title1 = $row['title'];
		$this->title2 = $title2;
		$this->description = $row['description'];
		$this->report_id = $row['report_id'];
		$this->sql = $include_file_sql;
		$this->params = $params;
		
		//now get result 
		$this->getResult($con);
	}
	
	
	function getResult($con) 
	{
		$rs = getRS($con,$this->sql);
		
		while ($row = getRow($rs)) 
		{
			$this->result[$row['id']] 	= $row;
			$this->ids[] 				= $row['id'];
			if (isset($row['accountname']))	{ 	
				$this->id_names[] = $row['accountname'];
			} else {
				$this->id_names[] = "";
			}
		}
		
		$this->ids = array_unique($this->ids);
		$this->id_names = array_unique($this->id_names);
	}
}
?>