<?php
class dashboardReport {
	
	var $reports = array();
	var $ids = array();
	var $id_names = array();
	var $arrayParams = array();
	var $rootPath;
	var $title = array();
	var $content_view;
	var $folder;
    var $col_headings = array();
	
	function dashboardReport($rootPath, $params, $folder) {
		$this->rootPath = $rootPath;
		$this->arrayParams = $params;
		$this->folder = $folder;
		//get report details from db
	}
	
	
	function runReport($report_id, $con, $title) {
		global $school_status;
		global $school_pupils;
		global $school_type;
		$sql = "select * from rep_report where report_id = $report_id";
		$rs = getRS($con,$sql);
		$rep_row = getRow($rs);
		$this->content_view = $rep_row['content_view'];		
		$include_file = $rep_row['include_file'];
		@include ("{$this->rootPath}/installations/{$this->folder}/reports/$include_file.php");
		
		$this->reports[] = new reportItem($rep_row,$con,$include_file_sql, $title, $this->arrayParams);
		$array_keys = array_keys($this->reports);
		$last_key = end($array_keys);
		$this->ids = array_merge($this->ids, $this->reports[$last_key]->ids);
		$this->ids = array_unique($this->ids);

		$this->id_names = array_merge($this->id_names, $this->reports[$last_key]->id_names);
		$this->id_names = array_unique($this->id_names);
				
	}
	
	function getTitle() {
		$str = "";
		foreach ($this->title as $key=>$val) {
			$str .= "<li><span class=\"title\">$key:</span><span class=\"value\"> $val.</span></li>";
		}
		return $str;
	}
    
    function colHeading($table, $col , $title) {
        $this->col_headings[$table][$col] = $title;
    }
}
?>