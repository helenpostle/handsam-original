<?php
/**
 * Functions to do with the "content" module
 */

#gets the return url to got to after form editing

function formReturn($con, $contentid=0, $parentid = 0, $params = "") {
	if ($params != "") {
		$params = "&".$params;
	}
	if ($contentid == 0) {
		if ($parentid == 0) {
			return "index.php";
		} else {
			$contentid = $parentid;
		}
	}
	
	$cid = $contentid;
	$superpage = "";
	while ($superpage == "") {
		$rsItem = getRS($con,"select superpage, parentid from content where contentid = $contentid");
		$rowItem = getRow($rsItem); 
		$contentid = $rowItem['parentid'];
		$superpage = $rowItem['superpage'];
	}
	$superpage = str_replace ( "_box", "", $superpage);
	$url = $superpage.".php?id=".$cid.$params;
	return $url;

}

function getParentRow($con, $contentid, $parentid) {
	if ($parentid == 0 || $parentid == "") {
		$parentid = $contentid;
	}
	$rsItem = getRS($con,"select * from content where contentid = $parentid");
	$rowItem = getRow($rsItem); 
	return $rowItem;
}

function getParent($con, $contentid, $parentid=null) {
	$superpage = "";
	if ($contentid == 0) {
		$contentid = $parentid;
	}
	$cid = $contentid;
	while ($superpage == "") {
		$rsItem = getRS($con,"select superpage, parentid from content where contentid = $contentid");
		$rowItem = getRow($rsItem); 
		$contentid = $rowItem['parentid'];
		$superpage = $rowItem['superpage'];
	}
	return $superpage;

}

function checkPage($con, $contentid, $page) {
	if ($contentid == 0) {
		return 0;
	}
	if ($page == getParent($con, $contentid, "")) {
		return $contentid;
	} else {
		return 0;
	}
}
#works out if the child is fertile from parent's fertility

function isFertile($con, $id) {
	$superpage = "";
	$cnt = 0;
	while ($superpage == "") {
		$cnt++;
		$rsItem = getRS($con,"select superpage, parentid, fertile from content where contentid = $id");
		$rowItem = getRow($rsItem); 
		$id = $rowItem['parentid'];
		$superpage = $rowItem['superpage'];
		$fertile = $rowItem['fertile'];
	}
	if ($cnt <= $fertile) {
		return true;
	} else {
		return false;
	}

}

function displayEdit($rowContent, $params="", $class="") {
	$result = "";
	if (isAllowed("editContent")) {
		$result = "<span class=\"editItem ".$class."\"><a class=\"admin iconEdit\" href=\"".displayNormalLink("admin.editcontent", $rowContent['contentid'], $params, "edit ".$rowContent['title']." item")."\"  title=\"\">edit</a></span>\n";
	}
	return $result;	
}

function displayEditLink($rowLinks, $params="", $class="") {
	$result = "";
	if (isAllowed("editLinks")) {
		$result = "<a class=\"admin iconEditLink\" href=\"".displayNormalLink("core/admin.editlink", $rowLinks['linkid'], $params, "edit ".$rowLinks['title'])."\"  title=\"\">edit</a>\n";
	}
	return $result;	
}

#spits out crumb trail array with title and url parts seperated
function getCrumbTrail($con, $contentid, $params="") {
	$result = array();
	$cnt = 0;
	$superpage = "";
	while ($superpage == "") {
		#only passes params up one level
		if ($cnt > 1) {
			$params = "";
		}
		$rsItem = getRS($con,"select superpage, parentid, title from content where contentid = $contentid");
		$rowItem = getRow($rsItem); 
		$result[$cnt]['title'] = $rowItem['title'];
		$superpage = $rowItem['superpage'];
		if ($superpage != "") {
			$contentid = 0;
			$result[$cnt]['link'] = displayLink($rowItem['superpage'], $contentid, $params);
		} else {
			$result[$cnt]['link'] = displayLink("PAGE", $contentid, $params);
		}
		$contentid = $rowItem['parentid'];
		$cnt++;
	}
	reset($result);
	for ($i = 0; $i < count($result); $i++) {
		$result[$i]['link'] = str_replace("PAGE", $superpage, $result[$i]['link']);
	}
	$result = array_reverse($result);
	return $result;
}
	
#spits out crumb trail array with title and url parts seperated for gallery page
function getSpecialCrumbTrail($con, $page, $titles, $ids, $params="") {
	$result = array();
	for ($i = 0; $i < count($titles); $i++) {
		$result[$i]['title'] = $titles[$i];
		$result[$i]['link'] = displayLink($page, $ids[$i], $params);
	}
	$result = array_reverse($result);
	return $result;
}



?>