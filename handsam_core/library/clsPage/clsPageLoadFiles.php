<?php
/**
 * Class For Building up files which will load for a page.
 */
class clsLoadPageFiles {
	
	var $phpFile = array();
	var $jsFile = array();
	var $jsConf = array();
	var $jsFileAutoLoad = array();
	var $cssUrl = array();
	var $cssFile = array();
	var $cssFileLoaded = array();
	var $cssFileMissing = array();
	var $rootPath = "";
	var $rootUrl = "";
	var $corePath = "";
	var $coreUrl = "";
	var $folder = "";
	var $jquery = "";
	var $jsVar = array();
	var $modUrl = "";
	var $modPath = "";
	var $module = "";
	var $box = array();
	var $box_priority = false;
	var $css_browsers = array();
	var $css_version;
	var $csrf = "";
	var $csrf_form = "";
	var $installation = "";

	
	//initiate class
	function clsLoadPageFiles($path, $url, $jq, $mod = "") {
		global $css_version, $installation;
		$this->installation = $installation;
		$this->css_version = $css_version;
		$this->rootPath = $path;
		$this->rootUrl = $url;
		$this->corePath = $path."/handsam_core/";
		$this->coreUrl = $url."/handsam_core/";
		$this->jquery = $this->coreUrl."library/jquery/$jq";
		if ($mod == 'handsam_core') {
			$mod = 'handsam_core';
		} else if ($mod == 'core') {
			$mod = "core";
		} else if ($mod != '') {
			$mod = "modules/$mod";
		}
		$this->modUrl = "$url/$mod/";
		$this->modPath = "$path/$mod/";
		$this->module = $mod;
		$this->css_browsers[0]['file'] = "ie6";
		$this->css_browsers[0]['agent'] = "MSIE 6.0";
		$this->css_browsers[1]['file'] = "ie7";
		$this->css_browsers[1]['agent'] = "MSIE 7.0";
		$this->css_browsers[2]['file'] = "ie8";
		$this->css_browsers[2]['agent'] = "MSIE 8.0";
		$this->css_browsers[3]['file'] = "firefox";
		$this->css_browsers[3]['agent'] = "Firefox";
		$this->css_browsers[4]['file'] = "safari";
		$this->css_browsers[4]['agent'] = "Safari";
		$this->css_browsers[5]['file'] = "chrome";
		$this->css_browsers[5]['agent'] = "Chrome";
	}
	
	function autoLoadJquery($jsAutoLoad) {
		$this->jsFileAutoLoad[] = $jsAutoLoad;
	}
	
	function addBoxPriority() {
		$this->box_priority = true;
	}
	
	###### core func ######
	//includes the load.php file for this func, which adds files to this class to be loaded at the appropriate place
	function addFunc($folderName) {
		$pageFiles = $this;
		$this->folder = "functionality/$folderName/";
		include_once($this->corePath.$this->folder."load.php");
		$this->folder = "";
		
	}
	
	###### module func ######
	//includes the load.php file for this func, which adds files to this class to be loaded at the appropriate place
	function addModFunc($folderName) {
		$pageFiles = $this;
		$this->folder = "functionality/$folderName/";
		include_once($this->modPath.$this->folder."load.php");
		$this->folder = "";
		
	}

	
	function addModCss($css) {
		$folder = "css/";
		if ($this->checkFile($this->cssUrl, $this->modUrl.$folder.$css)) {
			$this->cssUrl[] = $this->modUrl.$folder.$css;
			$this->cssFile[] = $this->modPath.$folder.$css;
		}
	}
	
	
	###### add core functionality files ######
	
	//add functionality php file to include
	function addFuncPhp($filename) {
		if ($this->checkFile($this->phpFile, $this->corePath.$this->folder."php/".$filename)) {
			$this->phpFile[] = $this->corePath.$this->folder."php/".$filename;
		}
	}	
	
	//add functionality js file to include
	function addFuncJs($filename) {
		if ($this->checkFile($this->jsFile, $this->coreUrl.$this->folder."js/".$filename)) {
			$this->jsFile[] = $this->coreUrl.$this->folder."js/".$filename;
		}
	}

	//add functionality js file to include
	function addConfJs($filename) {
		if ($this->checkFile($this->jsConf, $this->rootPath."/installations/".$this->installation."/config/jsConf/".$filename)) {
			$this->jsConf[] = $this->rootPath."/installations/".$this->installation."/config/jsConf/".$filename;
		}
	/*
		if ($this->checkFile($this->jsFile, $this->coreUrl."shared/jsConf/".$filename)) {
			$this->jsFile[] = $this->coreUrl."shared/jsConf/".$filename;
		}
		*/
	}

	//add functionality css file
	function addFuncCss($filename) {
		if ($this->checkFile($this->cssUrl, $this->coreUrl.$this->folder."css/".$filename)) {
			$this->cssUrl[] = $this->coreUrl.$this->folder."css/".$filename;
			$this->cssFile[] = $this->corePath.$this->folder."css/".$filename;
		}
	}
	
	
	###### add core library files ######
	
	//add a handsam_core php library file. filename must have folder and filename
	function addCoreLibPhp($filename) {
		if ($this->checkFile($this->phpFile, $this->corePath."library/".$filename)) {
			$this->phpFile[] = $this->corePath."library/".$filename;
		}
		
	}
	
	//add a handsam_core js library file. filename must have folder and filename. Usually a jquery plugin!
	function addCoreLibJs($filename) {
		if ($this->checkFile($this->jsFile, $this->corePath."library/".$filename)) {
			$this->jsFile[] = $this->coreUrl."library/".$filename;
		}
		
	}

	###### Add module functionality files ######
	
        /**
         * Add JS from module functionality dir
         * @param type filename 
         */
	function addModuleFuncJs($filename) {
		if ($this->checkFile($this->jsFile, $this->modUrl."{$this->folder}/js/".$filename)) {
			$this->jsFile[] = $this->modUrl."{$this->folder}js/".$filename;
		}
	}

	function addModuleBox($filename) {
		$this->box[] = $this->modPath."{$this->folder}boxes/".$filename;
	}

	function addModuleFuncCss($filename) {
		if ($this->checkFile($this->cssUrl, $this->modUrl."{$this->folder}/css/".$filename)) {
			$this->cssUrl[] = $this->modUrl."{$this->folder}css/".$filename;
			$this->cssFile[] = $this->modPath."{$this->folder}css/".$filename;
		}
	}
		
	//add functionality js file to include
	function addModConfJs($filename) {
		if ($this->checkFile($this->jsFile, $this->modUrl."shared/jsConf/".$filename)) {
			$this->jsFile[] = $this->modUrll."shared/jsConf/".$filename;
		}
	}

	###### Add module library files  ######
	
	function addModuleLibJs($filename) {
		if ($this->checkFile($this->jsFile, $this->modUrl."library/".$filename)) {
			$this->jsFile[] = $this->modUrl."library/".$filename;
		}
		
	}

	
	//add a general php file specifying the full file path from the root
	function addAPhp($filepath) {
		if ($this->checkFile($this->phpFile, $this->rootPath."/".$filepath)) {
			$this->phpFile[] = $this->rootPath."/".$filepath;
		}
		
	}
	
	
		
	###### Add general js files - provide whole filepath ######
	
	function addJs($filename) {
		if ($this->checkFile($this->jsFile, $filename)) {
			$this->jsFile[] = $filename;
		}
		
	}
	
	function addModuleLibPhp($filename) {
		if ($this->checkFile($this->phpFile, $this->modPath."library/".$filename)) {
			$this->phpFile[] = $this->modPath."library/".$filename;
		}
		
	}
	
	###### Add php vars to js ######
	
	//adds a php variable value to a js var name	
	function addJsVar($varName, $varVal) {
		$this->jsVar[] = array('name' => $varName, 'value' => $varVal);
	}

	###### used to add a general css file - provide whole url
	function addCss($css) {
		if ($this->checkFile($this->cssUrl,$css)) {
			$this->cssUrl[] = $css;
			$this->cssFile[] = "";
		}
	}

	###### used to add a general css file - provide whole url
	function addSystemCss($cssfile) {
		$folder = "/css/";
		if ($this->checkFile($this->cssUrl, $this->rootUrl.$folder.$cssfile)) {
			$this->cssUrl[] = $this->rootUrl.$folder.$cssfile;
			$this->cssFile[] = $this->rootPath.$folder.$cssfile;
		}
	}

	######add a box when provided with whole filepoath
	function addBox($filename) {
		$this->box[] = $filename;
	}

	
	###### Include the page files #######
	
	//include php files at beginning of each page
	function includePhp() {
		foreach ($this->phpFile as $file) {
			//echo "<br/>$file";
			include_once($file);
		}
	}
	
	
	//include php file with js config at before 
	function includeJSConf() {
		foreach ($this->jsConf as $file) {
			//echo "<br/>$file";
			include_once($file);
		}
	}
	
	//include js files - used in layout.php head. if there are js files, automaticaly includes jquery
	function includeJs() {
    echo "<script type=\"text/javascript\" src=\"{$this->jquery}\"></script>\n";
		if (count($this->jsFile) > 0 || count($this->jsFileAutoLoad)) {
			foreach ($this->jsFileAutoLoad as $file) {
				if ($file != "") echo "<script type=\"text/javascript\" src=\"$file?v={$this->css_version}\"></script>\n";
			}
		}
		$this->jsFile = array_reverse($this->jsFile);
		$this->jsFile = array_unique($this->jsFile);
		foreach ($this->jsFile as $file) {
			echo "<script type=\"text/javascript\" src=\"$file?v={$this->css_version}\"></script>\n";
		}
	}

	//include css files - used in layout.php head
	function includeCss() {
		for ($a=0; $a<count($this->cssFile); $a++) {
		//echo $this->cssUrl[$a]."<br/>";
		//foreach ($this->cssFile as $file) {
			if(file_exists($this->cssFile[$a])) {
				$this->cssFileLoaded[] = $this->cssUrl[$a];
				echo "<link rel=\"stylesheet\" href=\"".$this->cssUrl[$a]."?v={$this->css_version}\" type=\"text/css\" />\n";
			} else {
				$this->cssFileMissing[] = $this->cssUrl[$a];
			}
			$url = str_replace(".css", "", $this->cssUrl[$a]);
			$file = str_replace(".css", "", $this->cssFile[$a]);
			if (trim($this->cssFile[$a]) != ""){
				for($i=0; $i<count($this->css_browsers); $i++) {
					if (isset($_SERVER['HTTP_USER_AGENT'])) {
						$pos = strpos($_SERVER['HTTP_USER_AGENT'],$this->css_browsers[$i]['agent']);
						if ($pos > 0) {
							//now because chrome contains safari and Chrome :
							if (($this->css_browsers[$i]['agent'] == "Safari" && strpos($_SERVER['HTTP_USER_AGENT'], "Chrome") == false) || $this->css_browsers[$i]['agent'] != "Safari" ) { 
							
								//echo "<br/>".$_SERVER['HTTP_USER_AGENT']." ::::: ".$this->css_browsers[$i]['agent'];
								//echo "<br/>pos: $pos";
								//echo "<br/>file: ". $file;
								
								//add  functionality for ie6 browser so can load javascript hacks etc
								if ($this->css_browsers[$i]['file'] == "ie6") $this->addFunc("ie6");
								if ($this->css_browsers[$i]['file'] == "ie7") $this->addFunc("ie7");
								
								$new_file = $file."_".$this->css_browsers[$i]['file'].".css";
								//echo "<br/>new_file: ". $new_file;
								$new_url = $url."_".$this->css_browsers[$i]['file'].".css?v={$this->css_version}";						
								if (file_exists($new_file)) {
									$this->cssFileLoaded[] = $new_url;
									echo "<link rel=\"stylesheet\" href=\"$new_url\" type=\"text/css\" />\n";
								} else {
									$this->cssFileMissing[] = $new_url;
								}
							}
						}
					}
				}
			}

		}
	}
	
	
	// loads js variable with php variable values in the head of layout page
        // checks that the var is a js array or JSON object
	function loadJsVar() {
		$str = "";
		foreach ($this->jsVar as $var) {
			if (is_int($var['value']) 
                                || strpos(trim($var['value']), "[") === 0
                                || strpos(trim($var['value']), "{") === 0)
                                 {
				$str .= "var {$var['name']} = {$var['value']}\n";
			} else {
				$str .= "var {$var['name']} = '{$var['value']}'\n";
			}
		}
		if ($str != "") {
			echo "<script type=\"text/javascript\">\n";
			echo $str;
			echo "</script>\n";
		}
		
		
	}
	
	###### other functions ######
	
	//checks for duplicate so only include a file once
	function checkFile($fileArr, $file) {
		if (in_array($file, $fileArr)) return false;
		return true;
	}

	
	//add a custom js script file to include
	function addCustomJs($filename) {
		if ($this->checkFile($this->jsFile, $this->rootUrl."/js/scripts/".$filename)) {
			$this->jsFile[] = $this->rootUrl."/js/scripts/".$filename;
		}
	}	
}
?>