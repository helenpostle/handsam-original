<?php

/**
 * Alert class containing multiple alerts for a user
 * extends Class User 
 */


class UserAlert extends User {
    
 
    private $alerts = array();
    
     /**
     * Gets all the info from the db for the specific user. 
     * Loads it into the $row property
     */
    function newAlert($alertid, $alert_text, $item, $type) {
		$this->alerts[$alertid] = new Alert($alertid, $alert_text, $item, $this->userid, $type);
    }
    	
	function getText() {
		$title = "";
		$txt = "";
		foreach ($this->alerts as $alert) {
			$a = "";
			$a = $alert->getItems();
			if ($a != "") $a = $alert->getAlertText().$a;
			$txt .= $a;
		}
		return $txt;
	}
	
	function getAlert() {
		return $this->alerts;
	}
}
// EOC