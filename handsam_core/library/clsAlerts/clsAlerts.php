<?php

/**
 * Alert class containing an alert and its items
 * 
 */


class Alert {
    
 
    private $alert_id;
	private $alert_text;
	public $items = array();
	private $userid;
	private $item_ids = array();
   
    public function __construct($alert_id, $alert_text, $items, $userid, $type) {
		$this->userid = $userid;
		$this->alert_id = $alert_id;
		$this->alert_text = $alert_text;
		//gets all items in the alert log for this user, and this alert
		$this->getItemIds($type);		
		//only get items that aren't in  the alert log
		$this->items = array_diff_key($items, $this->item_ids);
   }
	
	public function getItems() {
		$txt = "";
		foreach ($this->items as $item) {
			$txt .= $item['title']."\n";
		}
		return $txt;
	}
    
	function getAlertText() {
		return $this->alert_text;
	}
	
	
	function getItemIds($type) {
		//need to get array of alerts that have been sent to this user for this alertid with these incicidentids
		$sql = "select itemid from alert_log where alertid={$this->alert_id} and userid={$this->userid} and alert_type = '$type' and success=1";
		$res = getRecSet($sql);
		while($row = getRowAssoc($res)) {
			$this->item_ids[$row['itemid']] = $row;
		}
		
	}
}