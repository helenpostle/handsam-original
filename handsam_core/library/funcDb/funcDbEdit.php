<?php
/**
 * Groups together the SQL classes for ease of inclusion
 */
require("$rootPath/handsam_core/library/clsDb/clsDbAction.php");
require("$rootPath/handsam_core/library/clsDb/clsDbUpdate.php");
require("$rootPath/handsam_core/library/clsDb/clsDbInsert.php");
require("$rootPath/handsam_core/library/clsDb/clsDbDelete.php");
require("$rootPath/handsam_core/library/clsDb/clsDbHistory.php");
?>