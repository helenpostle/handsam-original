<?php
/**
 * Backup a Mysql database to the path provided. 
 * Filename will be the database name followed by the date.
 * *nux only as requires gzip
 */
 
function backupDatabase($dbhost, $dbname, $dbuser, $dbpass, $backuppath) {
	global $server_os;
	
	if ($server_os == "linux") {
		$file = $dbname."_".date("Y-m-d-H-i-s") . '.gz';
		$command = "/usr/bin/mysqldump -u$dbuser -p$dbpass $dbname | gzip > $backuppath/$file";
	} else {
		$file = $dbname."_".date("Y-m-d-H-i-s") . '.sql';
		$command = "mysqldump -h $dbhost -u $dbuser -p$dbpass $dbname > $backuppath/$file";
	}
	$line = system( $command,$retval );
	if ($retval == 0) {
		doLog("INFO","Database backed up to $backuppath/$file");
		return;
		//return "Database backed up to $backuppath/$file";
	} else {
		doLog("ERROR","Error backing up database to $backuppath/$file: $retval $line : $command");
		//return "<font color='red'>Error backing up database to $backuppath/$file: $retval $line <br/> $command</font>";
		return;
	}
}

?> 