<?php
/**
 * MS SQL Server Database access functions 
 */
 
 
$booleanTrue = 1; 
$booleanFalse = 0; 

$dbCurrentDate = "current_timestamp";

function escapeString($string) {
	return str_replace("'","''",$string);
}

function getConnection($host, $database, $username, $password) {
	$con = mssql_connect($host, $username, $password);
	if ($con) {
		mssql_select_db($database, $con);
		return $con;
	} else {
		die('error in connection settings');
	}
}

function execSQL($con, $sql) {
	
	$result = mssql_query($sql ,$con);
	if ($result) {
		return "";	
	} else {
		return mssql_get_last_message();	 	
	}
}

function getRS($con, $sql) {
	$result = mssql_query($sql ,$con);
	if ($result) {
		return $result;	
	} 
}

function getRow($result) {
	return @mssql_fetch_array($result);
}

function getResultRows($result) {
	$num_rows = @mssql_num_rows($result);
	if ($num_rows > -1) {
		return $num_rows;
	} 
}

function get_last_insert_id() {
	$lq = "SELECT @@idENTITY as id";
	$lastQuery = mssql_query($lq);
	$lastArray = mssql_fetch_array($lastQuery);
	$lastRowid = $lastArray["id"];
	return $lastRowid;
}

?>