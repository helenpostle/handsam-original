<?php
/**
 * Functions for drawing form items with specific values
 */
 
/**
 * A row in the table, with the supplied control and label 
 * @param string the form element
 * @param string title
 * @param string field name
 * @param bool true if the field is required
 * @param string extra classes, space separated
 */
function frmRow($ctrl,$title,$field,$req="", $extra_class="") {
	if ($req == true) {
		$class = "required";
		$title .= "<span>*required field</span>";
	} else {
		$class = "optional";
	}
	$str = "<div class='frm_row ".$class."  $extra_class clearfix'><label for='".$field."'>".$title."</label>".$ctrl;
	if ($ctrl == "") $str .= "&nbsp;";
	$str .= "</div>\n";
	return displayText($str);
}

/** 
 * A row in the table, with the supplied control and label 
 * Mimics a disabled form input
 * @param string $ctrl Value from the db
 * @param string $title The field's title
 * @param string $class optional extra class
 * @return string 
 */
function frmRowNoInput($ctrl,$title, $class="") {
	$str = "	<div class=\"frm_row clearfix $class\"><span class=\"label\">".$title."</span><span class=\"value\">".$ctrl;
	if ($ctrl == "") $str .= "&nbsp;";
	$str .= "</span></div>\n";
	return $str;
}
 

/** 
 * A normal text field 
 * @global bool $allow_html_purifier
 * @param object $rs mysql recordset
 * @param string field name
 * @param int max length of the input
 * @param string field title
 * @param bool if the value is required
 * @param string optional extra classes
 * @return instance of frmRow()
 */
function frmTextField($rs,$field,$len,$title,$req, $class="") {
	global $allow_html_purifier;

	$val = frmValue($rs,$field);
	$str = "<input type=\"text\" class=\"inputText\" name=\"$field\" id=\"$field\" value=\"$val\" maxlength=\"$len\"/>";
	$str = $allow_html_purifier->purify($str);

	return frmRow($str,$title,$field,$req, $class);
}



/** 
 * A normal text field non database
 * @param string field name
 * @param int max length of input
 * @param string field title
 * @param bool true if required
 * @param string field value, optional
 * @param string if the autofill is on or off, on is default
 * @param string optional extra classes
 * @return instance of frmRow()
 */
function frmTextFieldNonDb($field,$len,$title,$req, $val="", $autofill='on', $class="") {
	return frmRow("<input autocomplete=\"$autofill\" type=\"text\" class=\"inputText\" name=\"$field\" id=\"$field\" value=\"$val\" maxlength=\"$len\"/>",$title,$field,$req,$class);
}


/**
 * A normal password text field 
 * @global bool $allow_html_purifier
 * @param object mysql record set
 * @param string field name
 * @param int max length of input
 * @param string field title
 * @param bool true if required
 * @return instance of frmRow()
 */
function frmPwdTextField($rs,$field,$len,$title,$req) {
	global $allow_html_purifier;
	$val = frmValue($rs,$field);
	$str = "<input type=\"password\" class=\"inputText\" name=\"$field\" value=\"$val\" maxlength=\"$len\"/>";
	$str = $allow_html_purifier->purify($str);
	return frmRow($str,$title,$field,$req);
}
 
/** 
 * A text area field 
 * @global bool $allow_html_purifier
 * @global type $nohtml_page_purifier
 * @param object mysql record set
 * @param string field name
 * @param int number of rows in textarea
 * @param string field title
 * @param bool true if required
 * @param string extra classes
 * @return instance of frmRow()
 */
function frmTextArea($rs,$field,$rows,$title,$req, $class="") {
	global $allow_html_purifier;
	global $nohtml_page_purifier;
	$val = frmValue($rs,$field);
	$val = $nohtml_page_purifier->purify($val);
	$str = "<textarea id=\"$field\" name=\"$field\" rows=\"$rows\" class=\"".frmClass($req,$field)."\">$val</textarea>";
	$str = $allow_html_purifier->purify($str);
	return frmRow($str,$title,$field,$req, $class);
	
}

/** 
 * A text area field non db
 * @param string field name
 * @param int number of rows in textarea
 * @param string field title
 * @param bool true if required
 * @param string field value
 * @param bool true if readonly
 * @return instance of frmRow()
 */
function frmTextAreaNonDb($field,$rows,$title,$req, $val="", $readonly=false) {
	$rostr = "";
	if ($readonly) $rostr = " readonly=\"readonly\" ";
	return frmRow("<textarea id=\"$field\" $rostr name=\"$field\" rows=\"$rows\" class=\"".frmClass($req,$field)."\">$val</textarea>",$title,$field,$req);
	
}


/** 
 * A text field not editable
 * @param object mysql record set 
 * @param string field name & id
 * @param string field title
 * @param string extra classes
 * @return instance of frmRowNoInput() 
 */
function frmTextAreaNonEdit($rs,$field,$title, $class="")
{
	$val = frmValue($rs,$field);
	return frmRowNoInput("<span  id=\"$field\" class=\"$field\">$val</span>",$title,$class);	
}

/** 
 * A date field not editable
 * @param object mysql record set
 * @param string field name
 * @param string field title
 * @param string date format
 * @param extra classes
 * @return instance of frmRowNoInput()
 */
function frmDateTimeNonEditFormat($rs,$field,$title, $format, $class="") {
	$val = frmValue($rs,$field);
	if ($val != "") {
		$date = strtotime($val);
		$dcheck =  strftime($format,$date);
	} else {
		$dcheck = "";
	}
	return frmRowNoInput("<span  class=\"$field\">$dcheck</span>",$title,$class);
}

/** 
 * A date field not editable
 * @param object mysql record set
 * @param string field value
 * @param string field title
 * @param string extra classes
 * @return instance of frmRowNoInput()
 */
function frmDateTimeNonEdit($rs,$field,$title, $class="") {
	$val = frmValue($rs,$field);
	if ($val != "") $val = displayDateTime($val);
	return frmRowNoInput("<span  class=\"$field\">$val</span>",$title,$class);
	
}


/** 
 * A text  field not editable with a link
 * @param object mysql record set
 * @param string field value
 * @param string field title
 * @param string link href value
 * @param string extra classes
 * @return instance of frmRowNoInput()
 */
function frmTextAreaNonEditLink($rs,$field,$title, $link, $class="") {
	$val = frmValue($rs,$field);
	return frmRowNoInput("<div  class=\"$field\"><a href=\"$link\">$val</a></div>",$title,$class);
	
}

/**
 * A text field, non-editable
 * Provide array to translate value to displayed value
 * 
 * @param object mysql record set
 * @param string field value
 * @param string field title
 * @param type $array
 * @return instance of frmRowNoInput
 */
function frmTextAreaNonEditArray($rs,$field,$title,$array) {
	$val = frmValue($rs,$field);
	$val2 = "";
	if ($val != "" && array_key_exists($val, $array)) $val2 = $array[$val];
	return frmRowNoInput("<span  class=\"".$field."\">$val2</span>",$title,"");
	
}


/**
 * A select box populated by a query
 *
 * @var object DB recordset
 * @var string the field to use from the $rs recordset
 * @var string An SQL query to fetch the preselected fields
 * @var object DB connection
 * @var string Title of the box
 * @var bool If a required field
 * @var string A class given to the select element
 * 
 */
function frmMultiSelectQuery($rs, $field, $qry, $con, $title, $req, $class = "") {
	$list_ids = array();
	if (isset($_POST[$field])) {
		foreach ($_POST[$field] as $post) { 
			$list_ids[] = $post;
		}
	} else if (isset($_GET[$field])) {
		foreach ($_GET[$field] as $get) { 
			$list_ids[] = $get;
		}
	} else {
		resetRs($rs);
		while ($row = getRow($rs)) { 
			$list_ids[] = $row[$field];
		}
	}
	$rs_sel = getRS($con, $qry);
	
	$str = "<select id=\"$field\" multiple=\"multiple\" name=\"".$field."[]\" class=\"$class multiple ".frmClass($req,$field)."\">\n";
	while ($row = getRow($rs_sel)) {
		$str .= "<option value=\"".$row['a']."\"";
		if (in_array($row['a'], $list_ids)) $str .= " selected";
		$str .= ">".$row['b']."</option>";
	}
	$str .= "</select>";
	
	return frmRow($str, $title, $field, $req);
}

function frmMultiSelectQueryArr($arr, $field, $qry, $con, $title, $req, $class = "") {
	$list_ids = array();
	if (isset($_POST[$field]) && is_array($_POST[$field]) & count($_POST[$field]) > 0) {
		foreach ($_POST[$field] as $post) { 
			$list_ids[] = $post;
		}
	} else if (isset($_GET[$field])) {
		foreach ($_GET[$field] as $get) { 
			$list_ids[] = $get;
		}
	} else {
		foreach ($arr as $get) { 
			$list_ids[] = $get;
		}
	}
	$rs_sel = getRS($con, $qry);
	
	$str = "<select id=\"$field\" multiple=\"multiple\" name=\"".$field."[]\" class=\"$class multiple ".frmClass($req,$field)."\">\n";
	while ($row = getRow($rs_sel)) {
		$str .= "<option value=\"".$row['a']."\"";
		if (in_array($row['a'], $list_ids)) $str .= " selected";
		$str .= ">".$row['b']."</option>";
	}
	$str .= "</select>";
	
	return frmRow($str, $title, $field, $req);
}


/**
 * A select box populated by a query
 */
function frmSelectQuery($rs,$field,$qry,$con,$title,$req, $class="", $disabled=false) {
    $disabled_attr = "";
    if ($disabled) $disabled_attr = " disabled ";
 	$rsSelect = getRS($con,$qry);
	$val = frmValue($rs,$field);
	$str = "<select $disabled_attr name=\"$field\"  id=\"$field\" class=\"".frmClass($req,$field)."\">\n";
	$str .= "<option value=\"\">Pick $title</option>";
	while ($row = getRow($rsSelect)) {
		$str .= "<option value=\"".$row[0]."\"";
		if ($row[0] == $val) {
			$str .= " selected";
		}
		$str .= ">".$row[1]."</option>";
	}
	$str .= "</select>";
	return frmRow($str,$title,$field,$req, $class);

}
/**
 * A select box populated by a query non database
 */
function frmSelectQueryNonDb($field,$qry,$con,$title,$req,$val="",$class="") {
	$rsSelect = getRS($con,$qry);
	$str = "<select name=\"$field\"  id=\"$field\" class=\"".frmClass($req,$field)."\">\n";
	$str .= "<option value=\"\">Pick $title</option>";
	while ($row = getRow($rsSelect)) {
		$str .= "<option value=\"".$row[0]."\"";
		if ($val != "" && $row[0] == $val) $str .= " selected ";
		$str .= ">".$row[1]."</option>";
	}
	$str .= "</select>";
	return frmRow($str,$title,$field,$req,$class);

}

/**
 * A select box populated by an array
 * 
 * @var object $rs database recordset
 * @var string $field field name & id
 * @var array $array key value pairs of options key/value
 * @var string $title title of the select box
 * @var bool $req true if required field
 * @var string $class string of CSS classes
 */
function frmSelectArray($rs,$field,$array,$title,$req,$class="", $disabled=false) {
    $disabled_attr = "";
    if ($disabled) $disabled_attr = " disabled ";
    
	$val = frmValue($rs,$field);
	$str = "<select $disabled_attr name=\"$field\"  id=\"$field\" class=\"$class ".frmClass($req,$field)."\">\n";
    
	$str .= "<option value=\"\">Pick $title</option>";
	foreach ($array as $key => $text) {
	   	$str .= "<option value=\"".$key."\"";
		if ($key == $val) {
			$str .= " selected";
		}
		$str .= ">".$text."</option>";
      }

	$str .= "</select>";
	return frmRow($str,$title,$field,$req, $class);
}




/**
 * A select box populated by an array with no database 
 * 
 * @param string field name attribute
 * @param array key/value pairs
 * @param string label text
 * @param bool required
 * @param string pre-selected value
 * @param string class
 */
function frmSelectArrayNonDb($field,$array,$title,$req,$val="", $class="") {
	$str = "<select name=\"$field\" id=\"$field\" class=\"".frmClass($req,$field)."\">\n";
	$str .= "<option value=\"\">Pick $title</option>";
	foreach ($array as $key => $text) {
	   	$str .= "<option value=\"".$key."\"";
	   	if ($key == $val) {
			$str .= " selected";
		}

		$str .= ">".$text."</option>";
      }

	$str .= "</select>";
	return frmRow($str,$title,$field,$req, $class);
}

/**
 * A select box populated by an array, supplied a value rather than a rs
 */
function frmSelectArrayValue($val,$field,$array,$title,$req) {
	$str = "<select name=\"$field\"  id=\"$field\" class=\"".frmClass($req,$field)."\">\n";
	
	foreach ($array as $key => $text) {
	   	$str .= "<option value=\"".$key."\"";
		if ($key == $val) {
			$str .= " selected";
		}
		$str .= ">".$text."</option>";
      }

	$str .= "</select>";
	return frmRow($str,$title,$field,$req);
}

/**
 * A date field which uses a javascript calendar
 */
function frmDateField($rs,$field,$title,$req) {
	global $rootUrl; 
	$date = strtotime($rs[$field]);
	$dcheck =  strftime("%a, %d %b %Y",$date);
	
	
	$ret = "<input name=\"$field\" id=\"ctl.$field\" readonly=\"true\" class=\"date\" value=\"$dcheck\"/>	";
	$ret .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.$field\"/>";
	
	/*
	$ret .= "style=\"cursor: pointer; border: 0px;\"";
	$ret .= "title=\"Date selector\"/>";
		
	$ret .= "<script type=\"text/javascript\">";
	$ret .= "Calendar.setup({";
	$ret .= "inputField     :    'ctl.$field',";
	$ret .= "ifFormat       :    \"%a, %d %b %Y\",";
	$ret .= " button         :    'trig.$field',";
	$ret .= "step           :    1";
	$ret .= "});";
	$ret .= "</script>	";
	*/
	return frmRow($ret,$title,$field,$req);	
}


/**
 * A date field which uses a javascript calendar and hours text box
 */
 
 
function frmTimeDateField($db_field, $rs,$field,$title,$req) { 
	global $rootUrl; 
	$val = lotcValue($rs,$db_field, $field);
	$hrs = lotcTimeValue($rs,$db_field, $field, "_hrs");
	$mins = lotcTimeValue($rs,$db_field, $field, "_mins");
	if (strtotime($val) > 0) { 
		$date = strtotime($val);
	} else {
		$date = time();
	}
	$dcheck =  strftime("%a, %d %b %Y",$date);
	$ret = "<input name=\"$field\" id=\"$field\" readonly=\"true\" class=\"date_time\" value=\"$dcheck\"/>	";
	$ret .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.$field\"/>";
	$ret .= "<label class=\"hrs\" for=\"{$field}_hrs\">Hrs(24) </label><input maxlength=\"2\" name=\"{$field}_hrs\" id=\"{$field}_hrs\"  class=\"time\" value=\"$hrs\"/>	";
	$ret .= "<label class=\"mins\" for=\"{$field}_mins\">Mins </label><input maxlength=\"2\" name=\"{$field}_mins\" id=\"{$field}_mins\" class=\"time\" value=\"$mins\"/>";
	//$ret .= " style=\"cursor: pointer; border: 0px;\"";
	//$ret .= " title=\"Date selector\"/>";
		
	//$ret .= "<script type=\"text/javascript\">";
	//$ret .= "Calendar.setup({";
	//$ret .= "inputField     :    '$field',";
	//$ret .= "ifFormat       :    \"%a, %d %b %Y\",";
	//$ret .= " button         :    'trig.$field',";
	//$ret .= "step           :    1";
	//$ret .= "});";
	//$ret .= "</script>	";
	
	return frmRow($ret,$title,$field,$req);	
}

 

/**
 * A date field which uses a javascript calendar without recordset
 */
function frmDateFieldNonDb($date,$field,$title,$req = false, $class="") { 
	global $rootUrl;
	if ($date > 0) {
		$dcheck =  strftime("%a, %d %b %Y",$date);
	} else {
		$dcheck = "";
	}
	$ret = "<input name=\"$field\" id=\"ctl.$field\" readonly=\"true\" class=\"date\" value=\"$dcheck\"/>	";
	$ret .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.$field\"/>";
	
	return frmRow($ret,$title,$field,$req, $class);	
}

 /**
 * A date text  field not editable
 */
function frmDateNonEdit($rs,$field,$title, $class="") {
	$date = strtotime($rs[$field]);
	$dcheck =  strftime("%a, %d %b %Y",$date);
	return frmRowNoInput("<div  class=\"datefield $class\">$dcheck</div>",$title,$field,"");	
}

/**
 * A A date editor (day month and year)
 */
			

function frmDateJ2($date,$field,$title,$req) {
	global $rootUrl;
	@list ($year, $month, $day) = @split ('-', $date);
	if ($year == "0000") $year = "";
	if ($day == "00") $day = "";
	
	$months = array("Jan","Feb","Mar","Apr","May","Jun","Jul","Aug","Sep","Oct","Nov","Dec");

	$return = "<input type=\"hidden\" name=\"$field\" id=\"ctl.$field\" value=\"$year/$month/$day\">";
	//$return .= "<table cellspacing=\"0\" cellpadding=\"0\" width=\"180\" border=\"0\">";
	//$return .= "<tr><td width=\"23%\">"; 
	$return .= "<input  id=\"".$field."_day\" maxlength=\"2\" type=\"text\" name=\"".$field."_day\" size=\"2\" value=\"$day\">";
	//$return .= "</td><td width=\"35%\">"; 
	$return .= "<select id=\"".$field."_month\"  name=\"".$field."_month\">";
	$return .= "<option value=\"\"></option>";
	for ($i = 0; $i < 12; $i++) { 
		$return .= "<option value=\"".($i + 1)."\"";
		if ($i + 1 == $month) $return .= " selected";
		$return .= ">".$months[$i]."</option>";
	}
	$return .= "</select></td><td width=\"40%\">"; 
	$return .= "<input size=\"4\" id=\"".$field."_year\" maxlength=\"4\"  type=\"text\" name=\"".$field."_year\" value=\"$year\">";
	//$return .= "</td><td>";
	
	$return .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.$field\"/>";
	
	/*
	$return .= "<img src=\"$rootUrl/handsam_core/functionality/form/js/cal/img.gif\" id=\"trig.$field\"";
	$return .= "style=\"cursor: pointer; border: 0px;\"";
	$return .= "title=\"Date selector\"/>";
	//$return .= "</td></tr></table>";
	
	$return .= "<script type=\"text/javascript\">";
	$return .= "Calendar.setup({";
	$return .= "inputField     :    'ctl.$field',";
	$return .= "inputFieldName     :'$field',";
	$return .= "button         :    'trig.$field',";
	$return .= "step           :    1";
	$return .= "});";
	$return .= "</script>	";
	*/
	
	return frmRow($return,$title,$field,$req);
}


function frmDateJ($rs,$field,$title,$req = false) {
	
	/*
	global $dateFormatMySQLDisplay;
	if ($rs[$field] == "" || $rs[$field] == "0000-00-00") {
		$date = "";
	} else {
		$date = ($rs[$field]);
	}
	*/
	if (isset($_POST[$field."_day"])) {
		$day = $_POST[$field."_day"];
		$month = $_POST[$field."_month"];
		$year = $_POST[$field."_year"];
		$val = "$year-$month-$day";
	} else {
		$val = @$rs[$field];	
	}
	return frmDateJ2($val,$field,$title,$req);
	
}

/**
 * A little function which returns the posted value if set else the db value
 * Useful for redisplaying data on a form even if errors stopped it from being saved.
 * Assumes that the database field has the same name as the form field.
 */
function frmValue($rs,$field) {
	
	$value = getFrmStrFromRequest($field);
	
	if ($value !== null) {
		return $value;
	} else {
		if (isset($rs[$field])) {
			return stripslashes($rs[$field]);
		} else {
			return null;
		}
	}
	/*
	if (isset($_POST[$field])) {
		//only strip slashes from post if they were added automagically.
		if (get_magic_quotes_gpc()==1) {
			return stripslashes($_POST[$field]);
		} else {
			return $_POST[$field];
		}
	} else if(isset($_GET[$field])) {
		//only strip slashes from post if they were added automagically.
		
		return $_GET[$field];
	
		
	} else {
		//always strip slashes from database
		return stripslashes($rs[$field]);
	}
	*/
}

/* Useful for redisplaying data on a form even if errors stopped it from being saved.
 * Assumes that the database field has the same name as the form field.
 * This accepts arrays for field names
 */
function frmValueArr($rs, $i, $field) {
	$field = str_replace("[]", "", $field);
	if (isset($_POST[$field][$i])) {
		//only strip slashes from post if they were added automagically.
		if (get_magic_quotes_gpc()==1) {
			return stripslashes($_POST[$field][$i]);
		} else {
			return $_POST[$field][$i];
		}
	} else if(isset($_GET[$field][$i])) {
		//only strip slashes from post if they were added automagically.
		
		return $_GET[$field][$i];
	
		
	} else {
		//always strip slashes from database
		if (isset($rs[$i][$field])) return stripslashes($rs[$i][$field]);
		return "";
	}
}


/** 
 * A function which changes the class of the input if an empty value was posted
 */
function frmClass($req,$field) {
	if ($req && isset($_POST[$field]) && $_POST[$field] == "") {
		return "fieldreq";
	} else {
		return "field";
	}	
	
}

function frmHiddenField($val,$field) {
	global $allow_html_purifier;
	$str = "<input type=\"hidden\" name=\"$field\" id=\"$field\" value=\"$val\"/>";
	$str = $allow_html_purifier->purify($str);
	return $str;
}

function frmHiddenFieldDb($rs,$field) {
	global $allow_html_purifier;
	$val = frmValue($rs,$field);
	$str = "<input type=\"hidden\" name=\"$field\" id=\"$field\" value=\"$val\"/>";
	$str = $allow_html_purifier->purify($str);
	return $str;
}

/**
 * A True/False Check box
 * Requires global variables to determine value for true and false
 */
function frmCheckBox($rs,$field,$title, $req=false, $disabled=false) {
	global $booleanTrue;
	global $booleanFalse;
    $disabled_attr = "";
	if ($disabled) $disabled_attr = " disabled ";
	$val = frmValue($rs,$field);
	$str = "<input class=\"inputCheckbox\" $disabled_attr type=\"checkbox\" id=\"$field\" name=\"$field\" value=\"$booleanTrue\"";
	if ($val == $booleanTrue) $str .= " checked";
	$str .= "/>";
	return frmRow($str,$title,$field,$req);
}

/**
 * A True/False Check box
 * Requires global variables to determine value for true and false
 */
function frmCheckBoxNonDb($val,$field,$title, $req=false, $class="") {
	global $booleanTrue;
	global $booleanFalse;
	if (isset($_POST[$field])) $val = $_POST[$field];
	$str = "<input class=\"inputCheckbox\" type=\"checkbox\" name=\"$field\" id=\"$field\" value=\"$booleanTrue\"";
	if ($val == $booleanTrue) $str .= " checked";
	$str .= "/>";
	return frmRow($str,$title,$field,$req, $class);
}

/**
 * A Radio box
 */
function frmRadio($field,$val,$checked,$title) {
	$str = "<input type=\"radio\" name=\"$field\" value=\"$val\"";
	if ($checked) $str .= " checked";
	$str .= "/>";
	
	return frmRow($str,$title,$field,true);
}

/**
 * A file field 
 */
function frmFileField($field,$title,$req) {
	return frmRow("<input type=\"file\" size=\"60\" class=\"fileInput\" name=\"$field\"/>",$title,$field,$req);
}


/**
 * A submit button with the name and label supplied
 */
function frmButton($str,$name) {
		$ctr = "<input type=\"submit\" value=\"$str\" name=\"bt_$name\" class=\"inputSubmit\"/>";
		$str = "	<div class=\"frmbutton \">&nbsp;$ctr</div>\n";
	return $str;
}


/**
 * A  button with the name and label supplied
 */
function frmClickButton($str,$name) {
		$ctr = "<input type=\"button\" value=\"$str\" name=\"bt_$name\" class=\"inputSubmit\"/>";
		
	return $ctr;
}

/**
 * A submit button with the name and label supplied and description on the left
 * 
 * @param string $str value
 * @param type $name fieldname
 * @param type $help help text
 * @param type $class 
 * @return string
 */
function frmButtonHelp($str,$name,$help, $class = '') {
		$ctr = "<input type=\"submit\" value=\"$str\" name=\"bt_$name\" class=\"inputSubmit\"/>";
		$str = "<div class=\"frmbutton $class\"><span>".$help."</span>".$ctr."</div>\n";
	return $str;
}

/**
 * A submit button with the name and label supplied, which puts up a javascript confirm dialogue when clicked.
 */
function frmButtonConfirm($str,$name, $confirm) {
		$ctr = "<input type=\"submit\" value=\"$str\" name=\"bt_$name\" class=\"inputSubmit\" onClick=\"return confirm('$confirm');\"/>";
		$str = "	<div class=\"frmbutton \">&nbsp;$ctr</div>\n";
	return $str;
}

/**
 * A submit button with the name and label supplied, which puts up a javascript confirm dialoge when clicked. and a description on the left
 */
function frmButtonConfirmHelp($str,$name, $confirm, $help) {
		$ctr = "<input type=\"submit\" value=\"$str\" name=\"bt_$name\" class=\"inputSubmit\" onClick=\"return confirm('".$confirm."')\"/>";
		$str = "<div class=\"frmbutton \"><span>".$help."</span>".$ctr."</div>\n";
	return $str;
}

/**
 * Special rows to quickly show the audit fields
 */
/**
 * Special rows to quickly show the audit fields
 */
function frmShowAudit($rs, $con) {
	$str = "";
	if ($rs["created"] != null && $rs["created_by"] != null) {
        if ($rs["created_by"] != 0) {
            $rsAudit = getRS($con,"select username from usr where userid = ".$rs["created_by"]);
            $rowAudit = getRow($rsAudit); 
            $str = frmRowNoInput(displayDateTime($rs["created"])." - ".$rowAudit["username"],"Added");
        } else {
            $str = frmRowNoInput(displayDateTime($rs["created"])." - Handsam Sync","Added"); 
        }
	}	
	if ($rs["edited"] != null) {
        if ($rs["edited_by"] != null) {
            $rsAuditEd = getRS($con,"select username from usr where userid = ".$rs["edited_by"]);
            $rowAuditEd = getRow($rsAuditEd);
            if ($rowAuditEd["username"] != "") {
                $username = $rowAuditEd["username"];
            } else {
                $username = "Handsam Sync";
            }
            $str .= frmRowNoInput(displayDateTime($rs["edited"])." - $username","Edited");
        } else {
            $str .= frmRowNoInput(displayDateTime($rs["edited"])." - Handsam Sync","Edited");
        }
	}
 return $str;
} 
 /*
function frmShowAudit($rs) {
	$str = "";
	if ($rs["created"] != null) {
 		$str = frmRowNoInput(displayDateTime($rs["created"])." - ".$rs["created_by"],"Added");
	}	
	if ($rs["edited"] != null) {
 		$str .= frmRowNoInput(displayDateTime($rs["edited"])." - ".$rs["edited_by"],"Edited");	
	}
 return $str;
}
*/

/**
 * A heading row for breaking up forms 
 */
function frmHeadingRow($title) {
	$str = "	<div class=\"heading\">$title</div>\n";
	return $str;
}

/**
 * A blank row for breaking up forms 
 */
/*function frmBlankRow() {
	$str = "	<tr><td colspan=\"2\">&nbsp;</td></td></tr>\n";
	return $str;
}
*/
/**
 * An instructional row
 */
function frmInstructionRow($title) {
	$str = "	<div><span class=\"instruction\">$title</span></div>\n";
	return $str;
}



/**
 * An info row
 */
function frmInfoRow($title) {
	$str = "<p class='forminfo info'>$title</p>\n";
	return $str;
}

/**
 * Start the form - no longer a table!
 */
function startFormTable($action, $class="", $name="", $title="", $notes="") {
	if ($name != "") $name = "_$name";
	$str = "<form class=\"".$class."\" method=\"POST\" name=\"frForm$name\" id=\"frForm$name\" action=\"{$_SERVER["PHP_SELF"]}\">\n";
	$str .=  "<div id=\"form_header\" class=\"clearfix $class\">";
	if ($title != "") $str .= "<h2>$title</h2>";
	if ($notes != "") $str .= "<p class=\"frm_info\">$notes</p>";
	$str .= "</div>";
	
	$str .= "<div id=\"form_body\" class=\"clearfix\">";
	//$str .= "<fieldset class=\"clearfix formtable\">";
	return $str;	
	
}

/**
 * Start a simple form - no header or footer elements!
 */
function startSimpleForm($action="", $class="", $name="", $title="", $notes="") {
	$str = "";
	if ($name != "") $name = "_$name";
	$str .= "<form class=\"".$class."\" method=\"POST\" name=\"frForm$name\" id=\"frForm$name\" action=\"{$_SERVER["PHP_SELF"]}\">\n";
	if ($title != "") $str .= "<h2>$title</h2>";
	if ($notes != "") $str .= "<p class=\"frm_info\">$notes</p>";
	return $str;	
	
}

/**
 * Start the form - no longer a table!
 */
function startFormBox($action="", $class="", $name="", $title="", $notes="") {
	$str = "";
	if ($name != "") $name = "_$name";
	if ($title != "") $str = "<h2>$title</h2>";
	if ($notes != "") $str .= "<p class=\"frm_info\">$notes</p>";
	$str .= "<form class=\"".$class."\" method=\"POST\" name=\"frForm$name\" id=\"frForm$name\" action=\"{$_SERVER["PHP_SELF"]}\">\n";
	//$str .= "<fieldset class=\"clearfix formtable\">";
	return $str;	
	
}

/**
 * Start the form - no longer a table!, allows file uploads
 */
function startFileFormTable($action, $class="", $title="", $notes="") {
	$str = "<form class=\"".$class."\" name=\"frForm\" id=\"frForm\" method=\"POST\" action=\"{$_SERVER["PHP_SELF"]}\"  enctype=\"multipart/form-data\">\n";
	$str .=  "<div id=\"form_header\" class=\"clearfix $class\">";
	if ($title != "") $str .= "<h2>$title</h2>";
	if ($notes != "") $str .= "<p class=\"frm_info\">$notes</p>";
	$str .= "</div>";
	
	$str .= "<div id=\"form_body\" class=\"clearfix\">";
	//$str .= "<fieldset class=\"clearfix formtable\">";
	return $str;	
}


/**
 * insert legend
 */
function frmLegend($title) {
	//return "<legend>".$title."</legend>";
}


/**
 * End the form and the table
 */
function endFormTable() {
	$str = "</div>";
	$str .=  "<div id=\"form_footer\"></div>";
	$str .= addCsrfField();
	$str .= "</form>\n";
	return $str;	
}

function frmEndFormNow() {
	$str = "</div>";
	$str .=  "<div id=\"form_footer\"></div>";
	return $str;	
}

/**
 * End the form without footer
 */
function endFormTable2() {
	$str = addCsrfField();
	$str .= "</form>\n";
	return $str;	
}


/**
 * End the form and the table
 */
function endFormBox() {
	$str = addCsrfField();
	$str .= "</form>\n";
	return $str;	
}


function formNotes($title, $notes) {
	$result = "<div class=\"infoNotes\">";
	$result .= "<h4>".$title."</h4>";
	$result .= "<p class=\"last\">".$notes."</p>";
	$result .= "</div>";
	return $result;
}

function formTopNotes($title, $notes) {
	$result = "<div class=\"infoNotes\" id=\"infoTopNotes\">";
	$result .= "<h4>".$title."</h4>";
	$result .= "<p class=\"last\">".$notes."</p>";
	$result .= "</div>";
	return $result;
}


function frmEditor($rs,$field,$rows,$title,$req, $width=375) {
	global $rootUrl;
	$val = frmValue($rs,$field);
	if ($req == true) {
		$class = "required";
		$str = "	<div class='".$class."'><label for='".$field."'>".$title."<br/>*required</label>";
	} else {
		$class = "required";
		$str = "	<div class='".$class."'><label for='".$field."'>".$title."</label>";
	}
	
	echo $str;
	$oFCKeditor = new FCKeditor($field) ;
	$oFCKeditor->BasePath = $rootUrl.'/handsam_core/library/fckeditor/';
	$oFCKeditor->Value =$val;
	$oFCKeditor->Width  = $width.'px' ;
	$oFCKeditor->Height = (($rows * 20) + 50)."px";
	$oFCKeditor->ToolbarSet = "FreeRangeDefault";

	$oFCKeditor->Create() ;
	echo "</div>";
} 


function addCsrfField() {
	global $clsUS;
	global $con;
	$csrf_tok = new csrfSecurity("form");
	$csrf_tok->setToken($clsUS, $con);
	return "<input id=\"frm_tygwyn\" type=\"hidden\" name=\"frm_tygwyn\" value=\"{$csrf_tok->token}\"/>";
}


/**
 * Add a File Field box
 *
 * Includes a box with all attached files, and a link to add more. 
 * Files can be removed by clicking the remove/delete button
 *
 * @var object mysql resource, a result of a query getting files for the particular item, e.g. training course, task list etc.
 */
function addFileField($filesDB, $add_link=true)
{
	global $rootUrl,
			$fileIds,
			$docDelIds,
			$docAddFilenames,
            $docCategory;
	
	//HandSaMDocs attached docs
	echo "<div class=\"frm_row clearfix\">\n";
	echo "<span class=\"label\">Attached Documents</span>\n";
	echo "<span class=\"value\">\n<span class=\"docsList\">\n<ul id=\"adminDoc\">";
	
	while ($rowAttachedFile = getRow($filesDB))
	{
		$dcat = ($rowAttachedFile['filecategory'] == "") ? "No Category" : $docCategory[$rowAttachedFile['filecategory']];
		
		if (isset($docDelIds) && isset($rowAttachedFile['taskfileid']) && in_array($rowAttachedFile['taskfileid'], $docDelIds))
		{
			// checks if already deleted if form not validated
			echo "<li id=\"doc".$rowAttachedFile['module_fileid']."\"><input type='hidden' name=\"docDelId[]\" value=\"".$rowAttachedFile['taskfileid']."\"/></li>";
		}
		else
		{
			echo "<li id=\"doc".$rowAttachedFile['module_fileid']."\"><a href=\"$rootUrl/handsam_core/downloadDocument.php?id=".$rowAttachedFile['fileid']."\">".$rowAttachedFile['title']."</a> (".$dcat.")<img class=\"delete_attached\" src=\"images/delete.png\" alt=\"unattach document or file\" title=\"Unattach this file\" /><input type=\"hidden\" name=\"docId[]\" value=\"".$rowAttachedFile['fileid']."\"/></li>";
		}
	}
	#this checks for just attached docs that have been attached before - if form hasn't validated
	if (isset($fileIds)) {
		for($i=0; $i < count($fileIds); $i++)
		{
			echo "<li id=\"justAdded".$fileIds[$i]."\">".$docAddFilenames[$i]."<img class=\"delete_attached\" src=\"images/delete.png\" alt=\"unattach document or file\" title=\"Unattach this file\" /><input type=\"hidden\" name=\"docAddFilename[]\" value=\"".$docAddFilenames[$i]." \"/><input type=\"hidden\" name=\"docAddId[]\" value=\"".$fileIds[$i]."\"/></li>";
		}
	}	
        
        // if $_POST
        if(isset($_POST['docAddId']) && count($_POST['docAddId'] > 0))
        {
            $sql = "SELECT fileid, filename, title FROM files WHERE fileid IN (".implode(',', $_POST['docAddId']).");";
            $rs = getRecSet($sql);
            while($rec = getRowAssoc($rs))
            {
                echo '<li id="justAdded'.$rec['fileid'].'">'.$rec['title'].'
                    <input type="hidden" name="docAddId[]" value="'.$rec['fileid'].'">
                    <input type="hidden" name="docAddFilename[]" value="'.$rec['title'].'">
                    <img src="http://dev.handsam/images/delete.png" class="delete_attached" alt="unattach document or file" title="Unattach this file">
                    </li>';
            }
        }
        
	echo "</ul>\n</span>\n</span>\n";
	echo "</div>\n";
	
	if($add_link) {
		//attach docs link
		echo "<div class=\"noLabel\"><a href = \"#\" onclick='openWin(\"$rootUrl/handsam_core/docList.php?filetype=handsamDoc\",840,600,\"fileBrowser\")'>Attach a document</a></div>\n";
	}
}

/**
 * Add a links field box
 *
 * A box with a list of links, with a remove/delete button.
 * A button to add more links
 *
 * @var object mysql resource, a result of a query getting files for the particular item, e.g. training course, task list etc.
 */
function addLinkField($linksDB, $add_link=true)
{
	global $rootUrl,
			$linkIds,
			$linkDelIds,
			$linkAddFilenames,
            $docCategory;	
	
	echo "<div class=\"frm_row clearfix\">\n";
	echo "<span class=\"label\">Added Links</span>\n";
	echo "<span class=\"value\">\n<span class=\"docsList\">\n<ul id=\"adminLinks\">";
	/*
	$qryAttachedLinks = "SELECT task_links.tasklinkid, links.linkid, links.title, links.url, links.linkcategory FROM links left join task_links on links.linkid = task_links.linkid WHERE task_links.taskid = $taskid";
	$resultAttachedLinks = getRS($con, $qryAttachedLinks);
	*/
	while ($rowAttachedLink = getRow($linksDB))
	{
        $dcat = ($rowAttachedLink['linkcategory'] == "") ? "No Category" : $docCategory[$rowAttachedLink['linkcategory']];
		if (isset($linkDelIds) && isset($rowAttachedLink['tasklinkid']) && in_array($rowAttachedLink['tasklinkid'],$linkDelIds))
		{
			// checks if already deleted if form not validated
			echo "<li id=\"doc".$rowAttachedLink['module_linkid']."\"><input type='hidden' name=\"linkDelId[]\" value=\"".$rowAttachedLink['tasklinkid']."\"/></li>";
		}
		else
		{
			echo "<li id=\"doc".$rowAttachedLink['module_linkid']."\"><a target=\"_blank\" href=\"".$rowAttachedLink['url']."\">".$rowAttachedLink['title']."</a> (".$dcat.")<img src=\"images/delete.png\" class=\"delete_attached\" alt=\"unattach document or file\" title=\"Remove this link\" /><input type=\"hidden\" name=\"linkId[]\" value=\"".$rowAttachedLink['linkid']."\"/></li>";
			// 
			// echo "<li id=\"doc".$rowAttachedLink['tasklinkid']."\"><a target=\"_blank\" href=\"".$rowAttachedLink['url']."\">".$rowAttachedLink['title']."</a> (".$docCategory[$rowAttachedLink['linkcategory']].")<img onclick=\"deleteFromDB('doc".$rowAttachedLink['tasklinkid']."',".$rowAttachedLink['tasklinkid'].",'link' );\" src=\"images/delete.png\" alt=\"unattach document or file\"/><input type=\"hidden\" name=\"docId[]\" value=\"".$rowAttachedFile['linkid']."\"/></li>";
		}
	}
	
	// this checks for just attached links that have been attached before - if form hasn't validated
	if (isset($linkIds))
	{
		for($i=0; $i < count($linkIds); $i++)
		{
			echo "<li id=\"justAdded".$linkIds[$i]."\">".$linkAddFilenames[$i]."<img onclick=\"unattach('justAdded".$linkIds[$i]."');\" src=\"images/delete.png\" alt=\"unattach document or file\"/><input type=\"hidden\" name=\"linkAddFilename[]\" value=\"".$linkAddFilenames[$i]." \"/><input type=\"hidden\" name=\"linkAddId[]\" value=\"".$linkIds[$i]."\"/></li>";
			// contains 'old' JS to remove link
			//echo "<li id=\"justAdded".$linkIds[$i]."\">".$linkAddFilenames[$i]."<img onclick=\"unattach('justAdded".$linkIds[$i]."');\" src=\"images/delete.png\" alt=\"unattach document or file\"/><input type=\"hidden\" name=\"linkAddFilename[]\" value=\"".$linkAddFilenames[$i]." \"/><input type=\"hidden\" name=\"linkAddId[]\" value=\"".$linkIds[$i]."\"/></li>";
		}
	}
        
        // if $_POST
        if(isset($_POST['linkAddId']) && count($_POST['linkAddId'] > 0))
        {
            $sql = "SELECT linkid, url, title FROM links WHERE linkid IN (".implode(',', $_POST['linkAddId']).");";
            $rs = getRecSet($sql);
            while($rec = getRowAssoc($rs))
            {
                echo '<li id="justAdded'.$rec['linkid'].'">'.$rec['title'].'
                    <input type="hidden" name="linkAddId[]" value="'.$rec['linkid'].'">
                    <input type="hidden" name="linkAddFilename[]" value="'.$rec['title'].'">
                    <img src="http://dev.handsam/images/delete.png" class="delete_attached" alt="unattach this link" title="Unattach this Link">
                    </li>';
            }
        }
        
	echo "</ul>\n</span>\n</span>\n";
	echo "</div>\n";
        
	if($add_link) {
		//add links link
		echo "<div class=\"noLabel\"><a href=\"#\" onclick='openWin(\"$rootUrl/handsam_core/linksList.php?filetype=handsamDoc\",840,600,\"fileBrowser\")'>Attach a link</a></div>\n";
	}
}


