<?php
$booleanTrue = 1; 
$booleanFalse = 0; 

$arraySQL = array();

/**
 * Wrapper for mysql_real_escape_string()
 * @param string value to be escaped
 * @return escaped value
 */
function escapeString($string) {
	//return addslashes($string);
	//return $string;
	return mysql_real_escape_string($string);
}

/** 
 * Returns a mysql connection
 * @param string host
 * @param string database
 * @param string username
 * @param string password
 * @return mysql connection object
 */
function getConnection($host, $database, $username, $password) {
	$con = mysql_connect($host, $username, $password);
	if ($con) {
		//echo "d:$database";
		if (mysql_select_db($database,$con)) {
			return $con;
		} else {
			die("database select error: ".mysql_error());
		}
	}else{
		die('Could not conntect: ' . mysql_error());
	}
}

/** 
 * Execute an SQL query
 * @global array arraySQL, debugging function to track success or failure
 * @param object connection object
 * @param string SQL statement
 * @return false on fail, empty string on success 
 */
function execSQL($con, $sql) {
	global $arraySQL, $errorFlag;
    global $errorFlag;
	$result = mysql_query($sql ,$con);
	if ($result) {
		$arraySQL[] = "EXECUTED: ".$sql;
		return "";	
	} else {
		$arraySQL[] = "FAILED: ".$sql;
		$errorFlag .= 'There was an problem with the SQL query.<br><br>';                
                logError(E_USER_ERROR, mysql_error(), 'funcDb.mySQL.php', '47', false);
                return false;
	}
}

/**
 * Wrapper for execSQL()
 * Does not require con as an argument
 * @global db connection $con
 * @param string $sql SQL
 * @return object
 */
function executeSql($sql)
{
    global $con;
    return execSQL($con, $sql);
}

/** 
 * Return a mysql record set of results
 * @global array arraySQL, debugging function to track success or failure
 * @param object connection object
 * @param string SQL statement
 * @return mysql error on fail, record set on success 
 */
function getRS($con, $sql) 
{
	global $arraySQL, $errorFlag;

        $result = mysql_query($sql, $con);
	if ($result) {
		$arraySQL[] = "EXECUTED: ".$sql;
		return $result;	
	} else {
		$arraySQL[] = "FAILED: ".$sql;
		$errorFlag .= 'There was an problem with the SQL query.<br><br>';                
                logError(E_USER_ERROR, mysql_error(), 'funcDb.mySQL.php', '81', false);
		return false;
	} 
}

/**
 * Wrapper for getRS
 * Does not require the $con as an argument
 * @global DB connection $con
 * @param string $sql query
 * @return mysql resource
 */
function getRecSet($sql)
{
    global $con;
    return getRS($con, $sql);
}

/**
 * Return a single row from a mysql record set
 * @param mysql record set
 * @return single row, as an array (mysql_fetch_array)
 */
function getRow($result) {
	return @mysql_fetch_array($result);
}

/**
 * Return a single row from a mysql record set
 * Identical to getRow()
 * 
 * @param mysql record set
 * @return single row, as an associative array (mysql_fetch_assoc)
 */
function getRowAssoc($result)
{
    return @mysql_fetch_assoc($result);
}

/**
 * Number of rows in a recordset
 * @param object mysql record set
 * @return int number of rows
 */
function getResultRows($result) {
    if(is_resource($result) && stristr(get_resource_type($result), "mysql"))
    {
        $num_rows = @mysql_num_rows($result);
        if($num_rows > -1)
        {
            return $num_rows;
        }
    }
}

/** 
 * Returns the id of the most recently insterted record
 * @return int id 
 */
function get_last_insert_id() {
	return mysql_insert_id();
}

/** 
 * Returns the pointer to the beginning of the mysql record set
 * @param object mysql record set
 */
function resetRS($result) {
	if (getResultRows($result) > 0) {
		mysql_data_seek($result, 0);
	}
}

