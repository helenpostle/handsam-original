<?php
 /**
 *
 * Functions for drawing form items with specific values
 */
 
/*special form functions to display form fields - have added optional extra text field*/

function frmExRow($ctrl,$title,$field,$req,$extra_text, $class="") {
	if (trim($extra_text) == "") return frmRow($ctrl,$title,$field,$req, $class);
	if ($req == true) {
		$class = "required $class";
		$extra_text .= "<span>*required field</span>";
	} else {
		$class = "optional $class";
	}
	if (trim($extra_text) != "") {
		$label_class = "extra_info";
		$extra_text = "<p>$extra_text</p>";
	} else {
		$label_class = "";
	}
	$str = "<div class='frm_row clearfix ".$class."'><div class=\"extra_text\"><label class=\"$label_class\" for='".$field."'>".$title."</label>$extra_text</div>".$ctrl;
	if ($ctrl == "") $str .= "&nbsp;";
	$str .= "</div>\n";
	return displayText($str);
}

/**
 * A normal text field 
 */
function frmExTextField($rs, $arr_key, $field,$len,$title,$req,$extra_text, $class="") {
	if ($arr_key === "") {
		$val = frmValue($rs,$field);
	} else {
		$val = frmValueArr($rs, $arr_key, $field);
	}
	$str = "<input type=\"text\" class=\"inputText\" name=\"$field\" id=\"$field\" value=\"$val\" maxlength=\"$len\"/>";
	return frmExRow($str,$title,$field,$req, $extra_text,$class);
}

/**
* A text area field with no recordset
*/

function frmExTextFieldNonDb($field,$len,$title,$req, $val="", $extra_text, $class="") {
	$str = "<input type=\"text\" class=\"inputText\" name=\"$field\" id=\"$field\" value=\"$val\" maxlength=\"$len\"/>";
	return frmExRow($str,$title,$field,$req,$extra_text,$class);
}


 /**
 * A text area field 
 */
function frmExTextArea($rs,$arr_key,$field,$rows,$title,$req,$extra_text, $class="") {
	if ($arr_key === "") {
		$val = frmValue($rs,$field);
	} else {
		$val = frmValueArr($rs, $arr_key, $field);
	}
	$str = "<textarea id=\"$field\" name=\"$field\" rows=\"$rows\" class=\"".frmClass($req,$field)."\">$val</textarea>";
	return frmExRow($str,$title,$field,$req ,$extra_text, $class);
	
}

/**
 * A select box populated by an array
 */
function frmExSelectArray($rs, $arr_key, $field,$array,$title,$req,$extra_text,$class="") {
	if ($arr_key === "") {
		$val = frmValue($rs,$field);
	} else {
		$val = frmValueArr($rs, $arr_key, $field);
	}
	//$str = "<select name=\"$field\"  id=\"$field\" class=\"$class ".frmClass($req,$field)."\">\n";
	$str = "<select name=\"$field\"  id=\"$field\" class=\"".frmClass($req,$field)."\">\n";
	$str .= "<option value=\"\">Pick $title</option>";
	foreach ($array as $key => $text) {
	   	$str .= "<option value=\"".$key."\"";
	   	
		if ($key == $val && ($val > 0 || $val != "")) {
			$str .= " selected ";
		}
		$str .= ">".$text."</option>";
      }

	$str .= "</select>";
	return frmExRow($str,$title,$field,$req,$extra_text, $class);
}

/**
 * A select box populated by an array, supplied a value rather than a rs
 */
function frmExSelectArrayValue($val,$field,$array,$title,$req,$extra_text, $class="") {
	$str = "<select name=\"$field\"  id=\"$field\" class=\"".frmClass($req,$field)."\">\n";
	foreach ($array as $key => $text) {
	   	$str .= "<option value=\"".$key."\"";
		if ($key == $val) {
			$str .= " selected";
		}
		$str .= ">".$text."</option>";
      }

	$str .= "</select>";
	return frmExRow($str,$title,$field,$req,$extra_text, $class);
}

/**
 * A select box populated by an array, supplied a value rather than a rs
 */
function frmExSelectArrayValueArr($val,$field,$array,$title_arr,$req,$extra_text, $class="") {
  $html = "";
  foreach($title_arr as $n=>$title)
  {
    $str = "<select name=\"".$field."-".$n."\"  id=\"".$field."-".$n."\" class=\"".frmClass($req,$field)."\">\n";
    foreach ($array as $key => $text) {
        $str .= "<option value=\"".$key."\"";
      if ($key == $val[$n]) {
        $str .= " selected";
      }
      $str .= ">".$text."</option>";
        }

    $str .= "</select>";
    $html.= frmExRow($str,$title,$field."-".$n,$req,$extra_text, $class);
  }
  return $html;
}


/**
 * A select box populated by a query
 * 
 * @param obj db recordset
 * @param str field name
 * @param str sql query
 * @param obj db conn
 * @param str title
 * @param bool required
 * @param string css class
 */
function frmExSelectQuery($rs,$field,$qry,$con,$title,$req, $class="") {
	$rsSelect = getRS($con,$qry);
	$val = frmValue($rs,$field);
	$str = "<select name=\"$field\"  id=\"$field\" class=\"".frmClass($req,$field)."\">\n";
	$str .= "<option value=\"\">Pick $title</option>";
	while ($row = getRow($rsSelect)) {
		$str .= "<option value=\"".$row[0]."\"";
		if ($row[0] == $val) {
			$str .= " selected";
		}
		$str .= ">".$row[1]."</option>";
	}
	$str .= "</select>";
	return frmRow($str,$title,$field,$req, $class);

}
/**
 * A date field which uses a javascript calendar
 */
function frmExDateField($rs,$field,$title,$req,$extra_text, $class="") {
	global $rootUrl; 
	$date = strtotime($rs[$field]);
	$dcheck =  strftime("%a, %d %b %Y",$date);
	$ret = "<input name=\"$field\" id=\"ctl.$field\" readonly=\"true\" class=\"date\" value=\"$dcheck\"/>	";
	$ret .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.$field\"";
	
	/*
	$ret .= "<img src=\"$rootUrl/handsam_core/functionality/form/js/cal/img.gif\" id=\"trig.$field\"";
	$ret .= "style=\"cursor: pointer; border: 0px;\"";
	$ret .= "title=\"Date selector\"/>";
		
	$ret .= "<script type=\"text/javascript\">";
	$ret .= "Calendar.setup({";
	$ret .= "inputField     :    'ctl.$field',";
	$ret .= "ifFormat       :    \"%a, %d %b %Y\",";
	$ret .= " button         :    'trig.$field',";
	$ret .= "step           :    1";
	$ret .= "});";
	$ret .= "</script>	";
	*/
	
	return frmExRow($ret,$title,$field,$req,$extra_text, $class);	
}

function frmTimeValue($rs,$field, $part) {
    
	if (isset($_POST[$field.$part]) && is_numeric($_POST[$field.$part])) 
	{
	    //only strip slashes from post if they were added automagically.	    
	    /**
	     * @TODO check server config for magic quotes and refactor. MG, 2012-07-10
	     */
	    if (get_magic_quotes_gpc() == 1){
		    return stripslashes($_POST[$field.$part]);
	    }else{
		    return $_POST[$field.$part];
	    }
	} 
	elseif(isset($_GET[$field.$part]) && is_numeric($_GET[$field])) 
	{
	    //only strip slashes from post if they were added automagically	
	    return $_GET[$part.$field.$part];	
	} 
	elseif(isset($rs[$field]) && !is_null($rs[$field])) 
	{
	    //always strip slashes from database
	
	    if ($part == '_hrs') return strftime("%H", strtotime($rs[$field]));
	    if ($part == '_mins') return strftime("%M", strtotime($rs[$field]));
	}
	else
	{
	
	    if ($part == '_hrs') return strftime("%H");
	    if ($part == '_mins') return strftime("%M");
	}
}

/**
 * A date field which uses a javascript calendar and hours text box
 */
 
 
function frmExTimeDateField($rs,$field,$title,$req,$extra_text, $class="") { 
	global $rootUrl; 
	$val = frmValue($rs, $field);
	$hrs = frmTimeValue($rs,$field, "_hrs");
	$mins = frmTimeValue($rs,$field, "_mins");
	if (strtotime($val) > 0) { 
		$date = strtotime($val);
	} else {
		$date = time();
	}
	$dcheck =  strftime("%a, %d %b %Y",$date);
	$ret = "<input name=\"$field\" id=\"$field\" readonly=\"true\" class=\"date_time\" value=\"$dcheck\"/>	";
	$ret .= "<img class=\"date_img\" src=\"$rootUrl/handsam_core/functionality/form/images/cal2.png\" id=\"trig.$field\"";
	$ret .= "<label class=\"hrs\" for=\"{$field}_hrs\">Hrs(24) </label><input maxlength=\"2\" name=\"{$field}_hrs\" id=\"{$field}_hrs\"  class=\"time\" value=\"$hrs\"/>	";
	$ret .= "<label class=\"mins\" for=\"{$field}_mins\">Mins </label><input maxlength=\"2\" name=\"{$field}_mins\" id=\"{$field}_mins\" class=\"time\" value=\"$mins\"/>";
	
	
	/*
	$ret .= "<img  src=\"$rootUrl/handsam_core/functionality/form/js/cal/img.gif\" id=\"trig.$field\" ";
	//$ret .= " style=\"cursor: pointer; border: 0px;\"";
	$ret .= " title=\"Date selector\"/>";
		
	$ret .= "<script type=\"text/javascript\">";
	$ret .= "Calendar.setup({";
	$ret .= "inputField     :    '$field',";
	$ret .= "ifFormat       :    \"%a, %d %b %Y\",";
	$ret .= " button         :    'trig.$field',";
	$ret .= "step           :    1";
	$ret .= "});";
	$ret .= "</script>	";
	*/
	return frmExRow($ret,$title,$field,$req,$extra_text, $class);	
}


/**
 * A True/False Check box
 * Requires global variables to determine value for true and false
 */
function frmExCheckBox($rs,$field,$title,$extra_text, $class="") {
	global $booleanTrue;
	global $booleanFalse;
	
	$val = frmValue($rs,$field);
	$str = "<input class=\"inputCheckbox\" type=\"checkbox\" id=\"$field\" name=\"$field\" value=\"$booleanTrue\"";
	if ($val == $booleanTrue) $str .= " checked";
	$str .= "/>";
	return frmExRow($str,$title,$field,false,$extra_text, $class);
}


/**
 * A Radio box
 * 
 * @param obj database recordset
 * @param str field name
 * @param arr array of values
 * @param str field title
 * @param str extra text
 * @param str css class space separated
 * 
 * @return string
 */
function frmExRadio($rs, $field,$radio_arr, $title,$extra_text, $class="") {
	$val = frmValue($rs,$field);
	$str = "";
	foreach ($radio_arr as $radio_key => $radio_val) {
		$str .= "<span class=\"radio_label\">$radio_val</span><input class=\"inputRadio\" type=\"radio\" name=\"$field\" id=\"".$field."_".preg_replace('/\s+/', '', $radio_key)."\" value=\"$radio_key\"";
		if ($val == $radio_key) $str .= " checked ";
		$str .= "/>";
	}
	
	return frmExRow($str,$title,$field,true,$extra_text, $class);
}

/**
 * A Radio box no recordset
 */
function frmExRadioNonDb($field, $val, $radio_arr, $title,$extra_text, $class="") {
	$str = "";
	foreach ($radio_arr as $radio_key => $radio_val) {
		$str .= "<span class=\"radio_label\">$radio_val</span><input class=\"inputRadio\" type=\"radio\" name=\"$field\" value=\"$radio_key\"";
		if ($val == $radio_key) $str .= " checked ";
		$str .= "/>";
	}
	
	return frmExRow($str,$title,$field,true,$extra_text, $class);
}

/**
 * A Radio box
 */
function frmRadioArray($rs, $field,$radio_arr, $title, $class="") {
	$val = frmValue($rs,$field);
	$str = "";
	foreach ($radio_arr as $radio_key => $radio_val) {
		$str .= "$radio_val <input class=\"inputRadio\" type=\"radio\" name=\"$field\" value=\"$radio_key\"";
		if ($val == $radio_key) $str .= " checked ";
		$str .= "/>";
	}
	
	return frmRow($str,$title,$field,true, $class);
}

?>