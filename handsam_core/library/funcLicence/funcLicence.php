<?php
/*
 * functions for use on licence.php and other kicence handling pages
 *
*/

/*app specific functions*/
/*LOTC app function to copy forms, fields and plans from m_form to a_form etc*/


/*also copy guidance*/
//SELECT m_form.*, a_form.m_formid, CASE WHEN m_form.formid = a_form.m_formid THEN 1 ELSE 0 END AS copy FROM m_form LEFT JOIN a_form ON m_form.formid = a_form.m_formid AND a_form.accountid = 309 WHERE m_form.planid = 1 AND m_form.state = 'ACTIVE' 
function licence_active_app_2($con, $accountid, $clsUS) {
	global $messages;
	//if this is the first licence, supply all the forms, plans and fields with ACTIVE state
	$qry = "select count(licenceid) as cnt from accountlicence where accountid = $accountid and appid = 2 and state != 'NEW'";
	$rs = getRS($con,$qry);
	$row = getRow($rs);
	if ($row['cnt'] == 1) {
		$state = "ACTIVE";
		$qry = "Select  m_plan.* from m_plan where m_plan.state = 'ACTIVE' and m_plan.planid not in (select planid from a_plan where accountid = $accountid)";
		$rs = getRS($con,$qry);
		//get all published plans
		while ($row = getRow($rs)) {
			$qry = new dbInsert("a_plan");
			$qry->setReqNumberVal("accountid",$accountid,"accountid");
			$qry->setReqNumberVal("m_planid",$row["planid"],"m_planid");
			$qry->setReqStringVal("planname",$row["planname"],"plan name");
			$qry->setStringVal("plantext",$row["plantext"],"plan text");
			$qry->setReqStringVal("state",$state,"state");
			$qry->setAudit($clsUS->userid);
			if ($qry->execute($con)) {
				//$messages[] = "Plan {$row['planname']} copied";
				$planid = $qry->getNewID();
				
				//now get all forms in this plan and copy them
				$qry_frm = "SELECT m_form.* FROM m_form WHERE m_form.planid = {$row['planid']} AND m_form.state = 'ACTIVE'";
				$rs_frm = getRS($con, $qry_frm);
				while ($row_frm = getRow($rs_frm)) {
					$qry_frm = new dbInsert("a_form");
					$qry_frm->setReqNumberVal("accountid",$accountid,"accountid");
					$qry_frm->setReqNumberVal("m_formid",$row_frm["formid"],"m_formid");
					$qry_frm->setNumberVal("planid",$planid,"plan ID");
					$qry_frm->setReqNumberVal("stage",$row_frm["stage"],"stage");
					$qry_frm->setReqStringVal("formname",$row_frm["formname"],"form name");
					$qry_frm->setStringVal("formtext",$row_frm["formtext"],"form text");
					$qry_frm->setReqStringVal("state",$state,"state");
					$qry_frm->setAudit($clsUS->userid);
					if ($qry_frm->execute($con)) {
						//$messages[] = "Plan {$row['planname']} :: Form {$row_frm['formname']} copied";
						$formid = $qry_frm->getNewID();
						
						//now get all fields for this form
						$qry_fld = "select * from m_field where formid = {$row_frm['formid']} and state = 'ACTIVE'";
						$rs_fld = getRS($con, $qry_fld);
						while ($row_fld = getRow($rs_fld)) {
							$qry_fld = new dbInsert("a_field");
							$qry_fld->setReqNumberVal("accountid",$accountid,"accountid");
							$qry_fld->setReqNumberVal("m_fieldid",$row_fld["fieldid"],"m_fieldid");
							$qry_fld->setNumberVal("formid",$formid,"Form ID");
							$qry_fld->setReqStringVal("fieldname",$row_fld["fieldname"],"field name");
							$qry_fld->setStringVal("fieldtext",$row_fld["fieldtext"],"field text");
							$qry_fld->setStringVal("params",$row_fld["params"],"params");
							$qry_fld->setNumberVal("required",$row_fld["required"],"required");
							$qry_fld->setStringVal("default_value",$row_fld["default_value"],"default value");
							$qry_fld->setReqNumberVal("field_typeid",$row_fld["field_typeid"],"field type id");
							$qry_fld->setReqStringVal("state",$state,"state");
							$qry_fld->setReqNumberVal("field_order",$row_fld["field_order"],"field_order");
							$qry_fld->setAudit($clsUS->userid);
							if ($qry_fld->execute($con)) {
								//$messages[] = "Plan {$row['planname']} :: Form {$row_frm['formname']} :: Field {$row_fld['fieldname']} copied";
								
								
							} else {
								//$messages[] = "Plan {$row['planname']} :: Form {$row_frm['formname']} :: Field {$row_fld['fieldname']} ERROR copying:".$qry_fld->getError();
								//$messages[] = $qry_fld->getSql();
							}
			
						}					
					} else {
						//$messages[] = "Plan {$row['planname']} :: Form {$row_frm['formname']} ERROR copying:".$qry_frm->getError();
					}
	
				}
				
			} else {
				//$messages[] = "Plan {$row['planname']} ERROR copying: ".$qry->getError();	
			}
		}
		
		//now copy all guidance to a_lotc_files and a_lotc_links
		
		$qry = "INSERT INTO a_lotc_files (fileid, created, created_by, edited, edited_by, state, accountid)  SELECT  lotc_files.fileid, lotc_files.created, lotc_files.created_by, (SELECT NOW()) AS edited, lotc_files.edited_by,lotc_files.state, (SELECT $accountid) AS accountid FROM lotc_files WHERE state = 'PUBLISHED' AND fileid NOT IN (SELECT fileid FROM a_lotc_files WHERE accountid = $accountid)";
		$insert = execSQL($con, $qry);
		if ($insert != "") $messages[] = $insert;
		
		$qry = "INSERT INTO a_lotc_links (linkid, created, created_by, edited, edited_by, state, accountid)  SELECT  lotc_links.linkid, lotc_links.created, lotc_links.created_by, (SELECT NOW()) AS edited, lotc_links.edited_by,lotc_links.state, (SELECT $accountid) AS accountid FROM lotc_links WHERE state = 'PUBLISHED' AND linkid NOT IN (SELECT linkid FROM a_lotc_links WHERE accountid = $accountid)";
		$insert = execSQL($con, $qry);
		if ($insert != "") $messages[] = $insert;
		
	} else {
		$state = "NEW";
		//supply only new plans update old forms, and plans with new forms and fields
		//$qry_frm = "SELECT m_form.*, a_form.m_formid, CASE WHEN m_form.formid = a_form.m_formid THEN 1 ELSE 0 END AS copy FROM m_form LEFT JOIN a_form ON m_form.formid = a_form.m_formid AND a_form.accountid = $accountid WHERE m_form.planid = {$row['planid']} AND m_form.state = 'ACTIVE'";

		 
	}		
	
}


/**
 * Run when the Training Log module is activated
 *
 * @con object mysql connection
 * @accountid int account id
 * @clsUS object user account
 *
 */
function licence_active_app_4($con, $accountid, $clsUS)
{
	global $messages;
	
	$app_id = 4; // 
	
	// if the module is set for the first time, add each user to the Staff Training usertype
	$qry = "SELECT count(licenceid) AS cnt FROM accountlicence WHERE accountid = $accountid AND appid = $app_id AND state != 'NEW'";
	$rs = getRS($con, $qry);
	$row = getRow($rs);

	if ($row['cnt'] == 1)
	{
		// get a list of all the users who are in this account and are not deleted
		$sql = "SELECT u.userid, ut.usertype FROM usr AS u
					JOIN usr_usrtype AS ut
						ON u.userid = ut.userid
					WHERE u.accountid = $accountid AND u.state <> 'DELETED';";
		$rec_set = getRS($con, $sql);
		$user_types = array();
		$id = 0;
		
		// create an array: array( [userid] => array( ['Training Staff'], ['visit planner'], ['evc'], ...etc ... ), )
		while ($row = getRow($rec_set)) {
			if($row['userid'] != $id)
			{
				$id = $row['userid'];
				$user_types[$id] = array();
				array_push($user_types[$id], $row['usertype']);
			}
			else
			{
				array_push($user_types[$id], $row['usertype']);
			}
		}
		
		// check if they have Staff Training usrype
		foreach($user_types as $id_k => $ut)
		{
			// if not Training Staff, add it
			if(in_array('Training Staff', $ut) == false)
			{
				$sql = "INSERT INTO usr_usrtype (usertype, userid) VALUES ('Training Staff', $id_k);";
				execSQL($con, $sql);
			}
		}
	}
}

//used to get array of name value pairs of licence conf
function getLicenceConf($rowlicence) {
	$conf_arr = json_decode($rowlicence['conf'], true);
	$new_arr = array();
	$i=0;
	if (count($conf_arr) > 0) {
		foreach ($conf_arr as $conf_val) {
			$new_arr["mod_conf_val_".$i] = $conf_val;
			$i++;
		}
	}
	return $new_arr;
}
 