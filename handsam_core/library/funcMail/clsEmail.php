<?php
/**
 * Class For Building An Email to Send.
 */
class clsEmail {
	
	var $fromAddress = "";  
  
	var $toAddress = "";	
	var $returnAddress = "";		
	var $replyAddress = "";		
	var $subject;
	var $message;
	
	var $attachments = "";
	
	var $mime_boundary;

	function clsEmail($to,$from,$subject,$text,$replyAddress="",$returnAddress="") {
		
		$this->toAddress=$to;
		$this->fromAddress=$from;
		$this->subject=$subject;
		$this->message=$text;
		$this->returnAddress=$returnAddress;
		$this->replyAddress=$replyAddress;
		
		$semi_rand = md5(time()); 
		$this->mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
	}
	
	function addTextAttachment($filename,$type, $text) {
		$mime_boundary = $this->mime_boundary;
		$data = base64_encode($text);
		$data = chunk_split($data);
		
		//$data = $text;
		$this->attachments .= "--{$mime_boundary}\n" . 
	                  "Content-Type: {$type};\n" . 
	                  " name=\"{$filename}\"\n" . 
	                  "Content-Transfer-Encoding: base64\n\n" . 
	                 $data . "\n\n" ;
	}
	
	
	
	function addFileAttachment($filename,$type,$file) {
		$data = h_file_get_contents($file); 
		//echo $data;
		//die();
		$this->addTextAttachment($filename,$type,$data);
	}
	
	function addBinaryFileAttachment($filename,$type,$file) {
		$data = file_get_contents($file); 
		//echo $data;
		//die();
		$this->addTextAttachment($filename,$type,$data);
	}
	
	function isMultiPart() {
		return ($this->attachments != "");
	}
	
	function getEmailBody() {
		
		if ($this->isMultiPart()) {
			$mime_boundary = $this->mime_boundary;
			$msg = 	"This is a multi-part message in MIME format.\n\n" . 
	                "--{$mime_boundary}\n" . 
	                "Content-Type:text/html; charset=\"iso-8859-1\"\n" . 
	               	"Content-Transfer-Encoding: 7bit\n\n".$this->getEmailMessage()."\n\n"; 
	        $msg .= $this->attachments;
	        $msg .= "--{$mime_boundary}--\n"; 
	        return $msg;
	        
	    } else {
			return $this->getEmailMessage();
		}	
	}
	
	/**
	 * Override this function in a class which adds headers and footers
	 */
	 
	function getEmailMessage() {
		return $this->message;
	}
	
	function getHeaders() {
		
		$headers = "MIME-Version: 1.0\r\n";
		
		if ($this->isMultiPart()) {
			$mime_boundary = $this->mime_boundary;
			$headers .=  "Content-Type: multipart/mixed;\n" . 
	            " boundary=\"{$mime_boundary}\"\r\n"; 
		} else {
			$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		}
		
		$headers .= 'From:'.$this->fromAddress."\r\n";
		
		if ($this->returnAddress != "") {
			$headers .= "Return-Path: ".$this->returnAddress."\r\n";	
		}
		if ($this->replyAddress != "") {
			$headers .= "Reply-To: ".$this->replyAddress."\r\n";	
		}
		
		return $headers;
	}	
	
	
	function get_file_extension($file) {
		$x = explode('.',$file);
        return array_pop($x);
    }

    function get_mimetype($value='') {

        $ct['htm'] = 'text/html';
        $ct['html'] = 'text/html';
        $ct['txt'] = 'text/plain';
        $ct['asc'] = 'text/plain';
        $ct['bmp'] = 'image/bmp';
        $ct['gif'] = 'image/gif';
        $ct['jpeg'] = 'image/jpeg';
        $ct['jpg'] = 'image/jpeg';
        $ct['jpe'] = 'image/jpeg';
        $ct['png'] = 'image/png';
        $ct['ico'] = 'image/vnd.microsoft.icon';
        $ct['mpeg'] = 'video/mpeg';
        $ct['mpg'] = 'video/mpeg';
        $ct['mpe'] = 'video/mpeg';
        $ct['qt'] = 'video/quicktime';
        $ct['mov'] = 'video/quicktime';
        $ct['avi']  = 'video/x-msvideo';
        $ct['wmv'] = 'video/x-ms-wmv';
        $ct['mp2'] = 'audio/mpeg';
        $ct['mp3'] = 'audio/mpeg';
        $ct['rm'] = 'audio/x-pn-realaudio';
        $ct['ram'] = 'audio/x-pn-realaudio';
        $ct['rpm'] = 'audio/x-pn-realaudio-plugin';
        $ct['ra'] = 'audio/x-realaudio';
        $ct['wav'] = 'audio/x-wav';
        $ct['css'] = 'text/css';
        $ct['zip'] = 'application/zip';
        $ct['pdf'] = 'application/pdf';
        $ct['doc'] = 'application/msword';
        $ct['bin'] = 'application/octet-stream';
        $ct['exe'] = 'application/octet-stream';
        $ct['class']= 'application/octet-stream';
        $ct['dll'] = 'application/octet-stream';
        $ct['xls'] = 'application/vnd.ms-excel';
        $ct['ppt'] = 'application/vnd.ms-powerpoint';
        $ct['wbxml']= 'application/vnd.wap.wbxml';
        $ct['wmlc'] = 'application/vnd.wap.wmlc';
        $ct['wmlsc']= 'application/vnd.wap.wmlscriptc';
        $ct['dvi'] = 'application/x-dvi';
        $ct['spl'] = 'application/x-futuresplash';
        $ct['gtar'] = 'application/x-gtar';
        $ct['gzip'] = 'application/x-gzip';
        $ct['js'] = 'application/x-javascript';
        $ct['swf'] = 'application/x-shockwave-flash';
        $ct['tar'] = 'application/x-tar';
        $ct['xhtml']= 'application/xhtml+xml';
        $ct['au'] = 'audio/basic';
        $ct['snd'] = 'audio/basic';
        $ct['midi'] = 'audio/midi';
        $ct['mid'] = 'audio/midi';
        $ct['m3u'] = 'audio/x-mpegurl';
        $ct['tiff'] = 'image/tiff';
        $ct['tif'] = 'image/tiff';
        $ct['rtf'] = 'text/rtf';
        $ct['wml'] = 'text/vnd.wap.wml';
        $ct['wmls'] = 'text/vnd.wap.wmlscript';
        $ct['xsl'] = 'text/xml';
        $ct['xml'] = 'text/xml';

        $extension = $this->get_file_extension($value);
		$type = "";
        if (!$type = $ct[strtolower($extension)]) {

            $type = 'text/html';
        }

        return $type;
    }	
}
?>
