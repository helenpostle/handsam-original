<?php
/**
* Some standard Email functions
* Always use these to send mails as then we can switch off, log etc when required. 
*
*/

 
/**
 * Sends a plaintext email message
 * 
 * Logs the result 
 * 
 * @global bool $sendMail true to send the mail
 * @param string $to recipient
 * @param string $from sender
 * @param string $title subject 
 * @param string $message body
 * @return boolean
 */
function sendMail($to, $from, $title, $message) {
	global $sendMail;
	
        if($to == 'mei@semantise.com')
        {
            mail('mei@semantise.com', $title, $message, "From:$from");
            return true;
        }       
	if ($sendMail) {
            if (!@mail($to, $title, $message, "From:$from"))
            {
                doLog("DEBUG","Plain Email sending FAILED to: $to; title: $title; message: $message");
                return false;
            }
            else 
            {
                doLog("DEBUG","Plain Email sending SUCCESSFUL to: $to; title: $title;");
                return true;
            }
	} 
	return true;
}

/**
 * Send a HTML email message
 * 
 * Logs the result
 * 
 * @global bool $sendMail true to send the mail
 * @param string $to recipient
 * @param string $from sender
 * @param string $title subject 
 * @param string $message body
 * @return boolean
 */
function sendHTMLMail($to, $from, $title, $message) {
	global $sendMail;
	
	if ($sendMail) {
		
		// To send HTML mail, the Content-type header must be set
		$headers  = 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		$headers .= 'From:'.$from;
		
		
		if (!@mail($to, $title, $message, $headers, "-f".$from)) {
			doLog("DEBUG","HTML Email sending FAILED to: $to; title: $title; message: $message");

			return false;
		} else {
			doLog("DEBUG","HTML Email sending SUCCESSFUL to: $to; title: $title;");
			return true;
		}
	}
	return true;
	
}

/**
* An email with some supplied text in an attachment 

function sendMail($to, $from, $title, $message, $attachtext, $attachname, $attachtype) {
	
	$headers = "From: ".$from; 


	$semi_rand = md5(time()); 
	$mime_boundary = "==Multipart_Boundary_x{$semi_rand}x"; 
	    
	$headers .= "\nMIME-Version: 1.0\n" . 
	            "Content-Type: multipart/mixed;\n" . 
	            " boundary=\"{$mime_boundary}\""; 

	$email_message .= "This is a multi-part message in MIME format.\n\n" . 
	                "--{$mime_boundary}\n" . 
	                "Content-Type:text/html; charset=\"iso-8859-1\"\n" . 
	               "Content-Transfer-Encoding: 7bit\n\n" . $message . "\n\n"; 

	$data = chunk_split(base64_encode($attachtext)); 

	$email_message .= "--{$mime_boundary}\n" . 
	                  "Content-Type: {$attachtype};\n" . 
	                  " name=\"{$fileatt_name}\"\n" . 
	                  //"Content-Disposition: attachment;\n" . 
	                  //" filename=\"$fileatt_name\"\n" . 
	                  "Content-Transfer-Encoding: base64\n\n" . 
	                 $data . "\n\n" . 
	                  "--{$mime_boundary}--\n"; 

	$ok = mail($to, $subject, $email_message, $headers); 

	if($ok) { 
		return ""; 
	} else { 
	   return "Sorry but the email could not be sent. Please go back and try again!"; 
	} 
}

*/
/**
 * This is for sending a clsEmail object
 */
function sendMailObj($mail) {
	global $debug, $sendMail, $messages, $smtpServer, $smtpPort;
	
	
	
	if ($sendMail) {
            
		if (isset($smtpServer) && $smtpServer != "" && $smtpServer != "localhost") {
			SendExternalMail($mail->toAddress, $mail->toAddress, $mail->fromAddress, $mail->fromAddress, $mail->subject, $mail->getEmailBody(), $mail->getHeaders(), $smtpServer, $smtpPort);
		} else {
			if (!mail($mail->toAddress, $mail->subject, $mail->getEmailBody(), $mail->getHeaders(), "-f".$mail->fromAddress)) {
				return false;
			}
		}
		
	}  else {
		$messages[] = "NOT SENDING EMAIL: ".$mail->subject." to ".$mail->toAddress;
	}
	return true;
	
}


function SendExternalMail($ToName, $ToEmail, $FromName, $FromEmail, $Subject, $Body, $Header, $mailServer, $port) { 
	die($Body);
	$SMTP = fsockopen("smtp.$mailServer",  $port);
	//$SMTP = fsockopen("smtp.sitename.com", 25);
	
	$InputBuffer = fgets($SMTP, 1024);
	
	fputs($SMTP, "HELO $mailServer\n"); 
	$InputBuffer = fgets($SMTP, 1024); 
	fputs($SMTP, "MAIL From: $FromEmail\n"); 
	$InputBuffer = fgets($SMTP, 1024); 
	fputs($SMTP, "RCPT To: $ToEmail\n"); 
	$InputBuffer = fgets($SMTP, 1024); 
	fputs($SMTP, "DATA\n"); 
	$InputBuffer = fgets($SMTP, 1024); 
	fputs($SMTP, "$Header"); 
	fputs($SMTP, "From: $FromName <$FromEmail>\n"); 
	fputs($SMTP, "To: $ToName <$ToEmail>\n"); 
	fputs($SMTP, "Subject: $Subject\n\n"); 
	fputs($SMTP, $Body); 
	fputs($SMTP, "QUIT\n"); 
	$InputBuffer = fgets($SMTP, 1024);
	
	fclose($SMTP); 
}


/**
 * Retreive an email template and fill with the supplied values
 * can be overridden per installation
 * 
 * also appends the email signature
 * 
 * Throws an exception if the email template does not exist
 * 
 * @global string $installation current installation
 * @global string $this_mod_dir current module
 * @global string $email_signature to be apended to message
 * @global string $rootPath app root path 
 * 
 * @param string $filename
 * @param mixed one or many values to be passed to the email template (used with vsprintf()).
 * 
 * @return string 
 */
function getEmailTemplate($filename)
{
    global $installation, $this_mod_dir, $email_signature, $rootPath;
    
    $contents = func_get_args();
    array_shift($contents); // lose $filename, keep the rest of the args in $contents
    if ($this_mod_dir == '') $this_mod_dir = 'handsam_core';
    // check if the installation has overridden the default templates
    $installation_path = $rootPath.'/installations/'.$installation.'/templates/'.$this_mod_dir.'/'.$filename.'.txt';
    $default_path = $rootPath.'/modules/'.$this_mod_dir.'/templates/'.$filename.'.txt';
    
    if($this_mod_dir == 'handsam_core' || $this_mod_dir == '')
        $default_path = $rootPath.'/handsam_core/templates/'.$filename.'.txt';
    
    if(file_exists($installation_path))
        $body = file_get_contents($installation_path);
    elseif(file_exists($default_path))
        $body = file_get_contents($default_path);
    else
        throw new Exception('The specified email template file does not exist. Install path'.$installation_path.', module path '.$default_path);
    
    // check for installation email signature
    $sig_file = $rootPath.'/installations/'.$installation.'/templates/email_signature.txt';
    if(file_exists($sig_file))
        $email_signature = file_get_contents($sig_file);
    
    $body = $body."\r\n\r\n--\r\n".$email_signature;
    
    return vsprintf($body, $contents);
}
