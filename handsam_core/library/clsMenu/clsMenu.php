<?php

//require("$rootPath/handsam_core/library/clsMenu/clsMenuItem.php");
require("$rootPath/handsam_core/library/clsMenu/clsSubMenu.php");

/**
 * Class For Building up files which will load for a page.
 */
class clsMenu {
		
	var $menuLink = array();
	var $menuTitle = array();
	var $menuDesc = array();
	var $menuParent = array();
	var $subMenu = array();
	var $rootUrl = "";
	var $thisPage = "";
	
	
	
	//initiate class
	function clsMenu($rootUrl) {
		$this->rootUrl = $rootUrl;
		$this->thisPage = $rootUrl.dirname($_SERVER['PHP_SELF'])."/".basename($_SERVER['PHP_SELF']);
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "") {
			$protocol = "https://";
		} else {
			$protocol = "http://";
		}
		$this->thisPage = $protocol.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
	}
	
	//sets this page from another page - to highlight menu option from sub pages
	
	function setThisPage($page) {
		//$this->thisPage = $this->rootUrl.dirname($_SERVER['PHP_SELF'])."/".$page;
		if (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] != "") {
			$protocol = "https://";
		} else {
			$protocol = "http://";
		}
		$this->thisPage = str_replace(basename($_SERVER['PHP_SELF']), $page, $protocol.$_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF']);
		
	}
	
	//adds a menu item
	function addMenuItem($link, $title, $desc, $parent, $order="") {
		
		if ($parent !== "" && ($parent > 0 || $parent === 0)) {
			if (!array_key_exists($parent, $this->subMenu)) $this->subMenu[$parent] =  new clsSubMenu($this->rootUrl);
			$this->subMenu[$parent]->addSubMenuItem($link, $title, $desc);
		} else {
			if ($order == "") {
				$this->menuLink[] = $this->rootUrl."$link";
				$this->menuTitle[] = $title;
				$this->menuDesc[] = $desc;
				$this->menuParent[] = $parent;
				$order = count($this->menuLink)-1;
			} else {
				$this->shiftOrder($order);
				$this->menuLink[$order] = $this->rootUrl."$link";
				$this->menuTitle[$order] = $title;
				$this->menuDesc[$order] = $desc;
				$this->menuParent[$order] = $parent;
			}
			return $order;
		}
	}
	
	function shiftOrder($order) {
	}
	
	function displayMenu($menuPage) {
		if ($menuPage != "") $this->setThisPage($menuPage);
		$cnt = count($this->menuLink);
		$menu = "";
		if ($cnt > 0) {
			$menu .= "<ul class=\"clearfix\">\n";
			$a_cls = "first_menu";
			//$a_cls = "";
			for ($i=0; $i<$cnt; $i++) {
				$liCls = "";
				if ($this->thisPage == $this->menuLink[$i]) $liCls = "current_page_item";
				//if ($this->menuParent[$i] == "") {
					if ($i > 0) $first_manu = "";
					$menu_tmp = "<a class=\"$a_cls menu\" href=\"".$this->menuLink[$i]."\" title=\"".$this->menuTitle[$i]."\">".$this->menuTitle[$i]."</a>\n";
					//now check for a sub menu
					$submenu = "";
					if (array_key_exists($i, $this->subMenu)) {
						$submenu .=  "<ul class=\"clearfix\">\n";
						
						for ($e=0; $e<count($this->subMenu[$i]->menuLink); $e++) {
							$SuliCls = "";
							if ($this->thisPage == $this->subMenu[$i]->menuLink[$e]) {
								$SuliCls = "current_page_item";
								$liCls = "current_page_item";
							}
							$submenu .= "<li class=\"page_item $SuliCls\">\n<a class=\"\" href=\"".$this->subMenu[$i]->menuLink[$e]."\" title=\"".$this->subMenu[$i]->menuTitle[$e]."\">".$this->subMenu[$i]->menuTitle[$e]."</a>\n";
						}
						$submenu .=  "</ul>\n";
					}
					if ($submenu != "") $liCls .= " sub_menu ";
					$menu .= "<li class=\"page_item $liCls\">\n".$menu_tmp.$submenu."</li>";
				//}
				$a_cls = "";
				//if ($i+1 == $cnt) $menu .= "<li class=\"last_menu\"><a class=\"last_menu_off\"></a></li>";
			}
			$menu .= "</ul>\n";
		}
		return $menu;
	}
	
	
}
?>

