<?php

// get list of job roles and their courses

/*
 * ... = array($jobroleid1 => array(courses => array()))
 */
function getJobRoleCourses()
{
    global $accountid;
    
    $sql = "SELECT j.role_id, tc.course_id 
            FROM core_jobrole AS j
            LEFT JOIN trainingcourse_jobrole AS tj
                ON j.role_id = tj.core_jobrole_id
            JOIN training_course AS tc
                ON tj.training_course_id = tc.course_id
        ;";
    
    $result = getRecSet($sql);
    
    $jrc = array();
    $cur_jr = 0;
    $t_course = array();
    while($row = getRowAssoc($result))
    {
        if($row['role_id'] != $cur_jr)
        {
            if($cur_jr > 0) $jrc[$row['role_id']] = array('training_course' => $t_course);
            
            $t_course = array();
            
        }
        $cur_jr = $row['role_id'];
        $t_course[] = $row['course_id'];
    }
 	if($cur_jr > 0) $jrc[$cur_jr] = array('training_course' => $t_course);   
    return $jrc;
}