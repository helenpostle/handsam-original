<?php
function menuItem($currentPage, $menuPage, $itemPos, $text) {
	if ($currentPage == $menuPage) {
		$currentPageClass = "current_page_item";
	} else {
		$currentPageClass = "";
	}
	if ($itemPos == "first") {
		$result = "<li class=\"page_item ".$currentPageClass."\"><a class=\"first_menu menu\" href=\"$menuPage\" title=\"".$text."\">".$text."</a></li>";
	} else {
		$result = "<li class=\"page_item ".$currentPageClass."\"><a href=\"$menuPage\" class=\"menu\" title=\"".$text."\">".$text."</a></li>";
	}
	return $result;
}


?>