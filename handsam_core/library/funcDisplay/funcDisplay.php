<?php
/**
 * Some functions to output HTML, making sure it is formated corretly
 *
 */


function displayText($txt) {
	return stripslashes($txt);
}


function textSummary($text,$n) {
	$text_length=strlen($text);
	if ($text_length>$n) {
		$text=substr($text,0,$n)."...";
	} 
	return stripslashes($text);
} 


function is_even($a){
	$even = True;
	if ($a % 2 != '0'){
		$even = False;
	}
	return $even;
}
  
function displayParagraphs($txt, $class="") {
	if ($class != "") {
		$class = "class=\"$class\"";
	}
	if (strpos($txt,"<p>") === false) {
		$txt = "<p $class>$txt</p>";	
	}
	return stripslashes($txt);
}

function showAbstract($str, $chars) {
	if (strlen($str) > $chars) {
		$ret = substr($str, 0, $chars);
		$ret = substr($ret, 0, strrpos($ret, " "));
		$ret .= "...";
	} else { 
		$ret = $str;
	}
	$ret = strip_tags($ret);
	return stripslashes($ret);

}

function displaySmallDate($dbdate) {
	//return $dbdate;
	$retDate = "";
	global $dateFormatSmall;
	if ($dbdate != "") {
		$date = strtotime($dbdate);
		$retDate =  strftime($dateFormatSmall,$date);
	}
	return $retDate;
	
}

function displayDate($dbdate) {
	//return $dbdate;
	$retDate = "";
	global $dateFormat;
	if ($dbdate != "") {
		$date = strtotime($dbdate);
		$retDate =  strftime($dateFormat,$date);
	}
	return $retDate;
	
}

function displayDateTime($dbdate) {
	global $datetimeFormat;
	$date = strtotime($dbdate);
	$dcheck =  strftime($datetimeFormat,$date);
	return $dcheck;
	
}

/**
 * Page Navigation for big record sets
 * @param string $link the page name to link to
 * @param string $param any additional url parameters
 * @param number $current the current page number
 * @param number $total the total number of pages
 */
function pageNavigator($link, $param, $current, $total) {
	
	$ret = "<span class=\"navigator\">";
	if ($total > 1) {
		$url = $link."?";
		if ($param != "") {
			$url .= $param."&";
		} 
		
		if ($current > 1) {
			$prev = $current-1;
			$ret .= "<a href=\"".$url."page=1\"> << </a>";
			
			$ret .= "<a href=\"".$url."page=".$prev."\"> < </a>";
	    }
	    
	    $start = $current - 7 > 0 ? $current - 7 : 1;
	    $end = $current + 7 < $total ? $current + 7 : $total;
	    
	    for ($i = $start; $i <= $end; $i++) {
		    if ($i == $current) {
			   $ret .= " <b>$i</b> "; 
		    } else {
				$ret .= "<a href='".$url."page=".$i."'> $i </a>";
			}
		}
	
		if ($current < $total) {
			$next = $current+1;
			$ret .= "<a href='".$url."page=".$next."'> > </a>";
			$ret .= "<a href='".$url."page=".$total."'> >> </a> ";
	    }
	}
    $ret .= "<br/>Viewing page ".$current." of ".$total."</span>";
	
    return $ret;
}


#checks for active link and displays either an active list value or a link
function displayListLink($linkText, $param, $value, $link) {
	if ($param == $value) {
		$result = "<li class=\"active\">".$linkText."</li>";
	} else {
		$result = "<li><a href=\"".$link.$value."\" title=\"".strip_tags($linkText)."\">".$linkText."</a></li>";
	}
return $result;
}

#checks for active link and displays either an active list value or a link
function displayMonthListLink($linkText, $paramYear, $paramMonth, $valueYear, $valueMonth, $link) {
	if ($paramMonth == $valueMonth && $paramYear == $valueYear) {
		$result = "<li><a  class=\"active\" href=\"".$link.$valueMonth."\" title=\"".strip_tags($linkText)."\">".$linkText." ".$valueYear."</a></li>";
	} else {
		$result = "<li><a href=\"".$link.$valueMonth."\" title=\"".strip_tags($linkText)."\">".$linkText." ".$valueYear."</a></li>";
	}
return $result;
}

//added from core files during addition of updatable web pages


function displayTitle1($text) {
	$result = "<h1>".displayText($text)."</h1>\n";
	return $result;
}
	
function displayTitle2($text) {
	$result = "<h2>".displayText($text)."</h2>\n";
	return $result;
}

function displayLongDate($dbdate) {
	//return $dbdate;
	$retDate = "";
	global $lotcDateTimeFormat;
	if ($dbdate != "") {
		$date = strtotime($dbdate);
		$retDate =  strftime($lotcDateTimeFormat,$date);
	}
	return $retDate;
	
}
function displayPosted($date, $pretext="", $posttext="") {
	$result = "<span class=\"posted\">".$pretext.displayLongDate($date).$posttext."</span>\n";
	return $result;
}

/**
 * Return a number with it's ordinal name
 * e.g. 1 -> 1st, 13 -> 13th etc
 * 
 * @param int $num
 * @return string
 */
function ordinal($num) {
    $ones = $num % 10;
    $tens = floor($num / 10) % 10;
    if ($tens == 1) {
        $suff = "th";
    } else {
        switch ($ones) {
            case 1 : $suff = "st"; break;
            case 2 : $suff = "nd"; break;
            case 3 : $suff = "rd"; break;
            default : $suff = "th";
        }
    }
    return $num . $suff;
}

/**
 * Returns array of numbers with ordinals, plain numbers as array keys
 * 
 * @param int $start
 * @param int $end
 * @param int $step step (optional)
 * @return array, key value pairs
 */
function ordinal_range($start, $end, $step = 1)
{
    $final = array();
    $ar = range($start, $end ,$step);
    
    foreach($ar as $num)
    {
        $final[$num] = ordinal($num);
    }
    return $final;
}

/**
 * Creates an array of Months, with their number as key
 * Defaults to full month name
 * 
 * @param string $format the php date format to use, e.g. F, m, M, n
 * @return array
 */
function month_ar($format = 'F')
{
    if(!in_array($format, array('F', 'm', 'M', 'n'))) return false;
    $months = array();
    for($i=1;$i<=12;$i++)
    {
        $months[$i] = date($format, mktime(0,0,0, $i, 1));
    }
    return $months;
}

/**
 * Array of day names, with numbers as key
 * Starts on Monday
 * 
 * @return array
 */
function day_ar()
{
    return array(
        '1' => 'Monday',
        '2' => 'Tuesday',
        '3' => 'Wednesday',
        '4' => 'Thursday',
        '5' => 'Friday',
        '6' => 'Saturday',
        '7' => 'Sunday',
    );
}