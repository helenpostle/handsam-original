<?php
function processCSVImport($con, $csv_id, $to_file_path){
  global $messages;
  global $clsUS;
  global $homePath;
  global $password_arr;
  global $adminMail;
  doLog("TITLE","IMPORT CSV ID: $csv_id");
  $error_flag = false;
  $error_cnt = 0;
  $usr_insert_cnt = 0;
  #go through csv
  $qry = "select f.usertype_arr, f.fields,f.account_arr, f.state, f.title, f.filename, f.id, f.description, f.created, f.edited, f.created_by, f.edited_by FROM csv_import f  where f.id = $csv_id ";
  $rs = getRS($con,$qry);
  $rowCsv = getRow($rs);
  
  $fields = json_decode($rowCsv['fields'], true);
  $accounts = json_decode($rowCsv['account_arr'], true);
  $usertypes = json_decode($rowCsv['usertype_arr'], true);
  $courses = array(45=>'2014-03-31',53=>'2014-03-31');
  #firt process accounts array - TODO later to add new account if needed 
  
  #remove 0 values from account array
  
  $accounts = (array_filter($accounts));
  
  $apps = getAppids($con,$usertypes);
  $usertypes = addAppids($con,$usertypes);
  doLog("TITLE","update licences: $csv_id");
  foreach ($accounts as $aid) {
    updateLicences($con, $apps, $aid, $clsUS, $csv_id, true);
  }
  
  
  $csv = new parseCSV();
  $csv->auto("$to_file_path/".$rowCsv['filename']);
  
  
  doLog("TITLE","Process CSV: $csv_id");
  foreach ($csv->data as $key => $row){
    #first check account exists
    $this_accountname = $row[$fields['accountname']];
    $this_accountname = preg_replace('/[^(\x20-\x7F)]*/','', $this_accountname);
    if (isset($accounts[$this_accountname])) {
      $accountid = $accounts[$this_accountname];
      #check username for user already in system
      $qry = "select * from usr where username = '".mysql_real_escape_string($row[$fields['username']])."'";
      $rsUsr = getRS($con, $qry);
      if ($rowUsr = getRow($rsUsr)) {
        #update this user
        #add usertype for user
      } else {
        #insert user

        $insert = new dbInsert('usr');
        $insert->setStringVal("username",$row[$fields['username']],"username");
        $insert->setStringVal("email",$row[$fields['email']],"email");
        $insert->setStringVal("firstname",$row[$fields['firstname']],"firstname");
        $insert->setStringVal("lastname",$row[$fields['lastname']],"lastname");
        $insert->setStringVal("state","ACTIVE","state");
        $insert->setNumberVal("accountid",$accountid,"accountid");
        $insert->setAudit($clsUS->userid);
        if (isset($row[$fields['int_ref']])) $insert->setNumberVal("int_ref",$row[$fields['int_ref']],"int_ref");
        $insert->setStringVal("csv_import",$csv_id,"csv_import");
        if ($insert->execute($con)){
          $usr_insert_cnt++;
          $userid = $insert->getNewID();
          doLog("INFO","Username: ".$row[$fields['username']]." userid: ".$userid." inserted: ".$insert->getSql());
    
          // email them their first random password
          $passwd = new clsUserPassword($homePath, $password_arr);
          $reply = $passwd->emailPassword($row[$fields['email']], $con, $adminMail);
          doLog("INFO","Username: ".$row[$fields['username']]." userid: ".$userid." PASSWORD: ".$reply);
          $messages[]= $reply;

          // now add their unique_string
          $u_qry = new dbUpdate('usr');
          $u_qry->setParam("userid", $userid);
          $u_qry->setNullParam('unique_string');
          $u_qry->setStringParam('unique_string', "", "or");
          $u_qry->setTableColVal('unique_string', 'concat(username, accountid, firstname, userid)', 'unique string');
          if(!$u_qry->execute($con))
          {
              $messages[] = $u_qry->getError();
          }

          insertUserType($con, $userid, $usertypes, $csv_id);
          insertCourses($con, $courses, $csv_id, $userid, $accountid, $clsUS);
        } else {
          $error_flag = true;
          doLog("ERROR",$insert->getSql());
          $error_cnt++;
        }
      }
    } else {
      doLog("INFO","Account: ".$row[$fields['accountname']]." does not exist. Username: ".$row[$fields['username']]." not inserted");
    }
    
  }
  if($error_flag==true) {
    $messages[] = "Import $csv_id Had $cnt Errors. Please view log file for details";
  } else {
    $messages[] = "Import Successful $usr_insert_cnt users inserted.";
  }
   
}


function updateLicences($con, $apps, $accountid, $clsUS, $csv_id, $etraining=false, $tasklistid=1) {
  global $messages;
  $update_cnt = 0;
  $error_flag = false;
  $error_cnt=0;
  #check for licence for this usertype and account
  foreach($apps as $app) {
    $qry = "select l.conf, l.licenceid, l.appid, l.state from accountlicence l  where  l.accountid = $accountid and l.end_date > now() and state in ('ACTIVE','NEW') and l.appid=$app";
    $rs = getRS($con,$qry);
    if ($row=getRow($rs)) {
      if ($row['state'] == 'NEW' && $app != 1)
      {
        //update to ACTIVE
        $ud = new dbUpdate("accountlicence");
        $ud->setParam("licenceid",$row['licenceid']);
        $ud->setStringVal('state', 'ACTIVE', 'state');
        $ud->setAudit($clsUS->userid);
        if ($ud->execute($con)){
          $update_cnt++;
          doLog("INFO","accountlicence update state: ".$ud->getSql());
        } else {
          $error_flag = true;
          doLog("ERROR",$ud->getSql());
          $error_cnt++;
        }
      }
      if ($etraining == true && $app == 4)
      {
        //update to ACTIVE
        $ud = new dbUpdate("accountlicence");
        $ud->setParam("licenceid",$row['licenceid']);
        $ud->setStringVal('conf', '{"e_training_enabled":"1"}', 'conf');
        $ud->setAudit($clsUS->userid);
        if ($ud->execute($con)){
          $update_cnt++;
          doLog("INFO","accountlicence update state: ".$ud->getSql());
        } else {
          $error_flag = true;
          doLog("ERROR",$ud->getSql());
          $error_cnt++;
        }
      }
    } else {
      #insert a licence startdate as now
      $ud = new dbInsert("accountlicence");
      if($app == 1) {
        $ud->setStringVal('state', 'NEW', 'state');
        $ud->setNumberVal("tasklistid",$tasklistid,"tasklistid");
      } else {
        $ud->setStringVal('state', 'ACTIVE', 'state');
        $ud->setNumberVal("accountid",$accountid,"accountid");
        $ud->setNumberVal("appid",$app,"appid");
        $ud->setAudit($clsUS->userid);
        $ud->setNumberVal("accountid",$accountid,"accountid");
      }
      if($app == 4 && $etraining == true) {
        $ud->setStringVal('conf', '{"e_training_enabled":"1"}', 'conf');
      } else if($app == 4) {
        $ud->setStringVal('conf', '{"e_training_enabled":"0"}', 'conf');
      }
      $ud->setDateVal('start_date', strftime("%a, %d %b %Y",time()), 'start date');
      $ud->setDateVal('end_date', strftime("%a, %d %b %Y",strtotime("+ 1 year")), 'end date');
      $ud->setDateVal('csv_import', $csv_id, 'csv id');
      if ($ud->execute($con)){
        $update_cnt++;
        $id = $ud->getNewID();
        doLog("INFO","accountlicence insert id:$id sql:".$ud->getSql());
      } else {
        $error_flag = true;
        doLog("ERROR",$ud->getSql());
        $error_cnt++;
      }
      
      
    }
  }
  if($error_flag==true) {
    $messages[] = "Update Licences: $csv_id and accountid: $accountid Had $error_cnt Errors. Please view log file for details";
  } else {
    $messages[] = "Update Licences Successful $update_cnt successful queries.";
  }
}
  
function insertUserType($con, $userid, $usertypes, $csv_id, $tasklistid=1) {
  global $messages;
  $update_cnt = 0;
  $error_flag = false;
  $error_cnt=0;
  foreach($usertypes as $ut=>$appid) {
    #check for usertype first
    $qry = "select * from usr_usrtype where userid = $userid and usertype = '$ut'";
    $rs = getRS($con, $qry);
    if (!getRow($rs)) {
      $ud = new dbInsert("usr_usrtype");
      $ud->setNumberVal("userid",$userid,"userid");
      if($appid==1) $ud->setNumberVal("tasklistid",$tasklistid,"tasklistid");
      $ud->setStringVal('usertype', $ut, 'usertype');
      $ud->setNumberVal('csv_import', $csv_id, 'csv id');
      if ($ud->execute($con)){
        $update_cnt++;
        $id = $ud->getNewID();
        doLog("INFO","usertype insert id:$id sql:".$ud->getSql());
      } else {
        $error_flag = true;
        doLog("ERROR",$ud->getSql());
        $error_cnt++;
      }
      
    }
    
  }  
}
  
function getAppids($con,$usertypes) {
  $apps = array();
  $qry = "select distinct appid from  usrtype where usrtype in('".implode("','", $usertypes)."')";
  $rs = getRS($con,$qry);
  While ($row=getRow($rs)) {
    $apps[] = $row['appid'];
  }
  return $apps;
}

function addAppids($con,$usertypes) {
  $apps = array();
  $qry = "select appid, usrtype from  usrtype where usrtype in('".implode("','", $usertypes)."')";
  $rs = getRS($con,$qry);
  While ($row=getRow($rs)) {
    $apps[$row['usrtype']] = $row['appid'];
  }
  return $apps;
}


function insertCourses($con, $course_ids, $csv_id, $userid, $accountid, $clsUS, $provider=0) {
  global $messages;
  foreach($course_ids as $id=>$date) {
    
    $sql = "select * from training_course where course_id = $id";
    $rs = getRS($con,$sql);
    $row = getRow($rs);
    
    #check that this user doesn't already have it:
    $sql = "select training_id from training_log where trainee_id = $userid and course_id = $id and state in ('IN PLANNING', 'IN PROGRESS')";
    $rs = getRS($con, $sql);
    if (!$row == getRow($rs)) {
    
      $qry = new dbInsert("training_log");
      $qry->setReqNumberVal("accountid",$accountid,"Accountid");   
      $qry->setReqNumberVal("trainee_id",$userid,"Trainee");
      $qry->setReqNumberVal("course_id",$id,"Training Course");
      $qry->setReqNumberVal("provider_id",$provider,"Course Provider");			
      $course_date = strftime("%a, %d %b %Y",strtotime($date));
      $renewal_date = strftime("%a, %d %b %Y", strtotime($date.' + '.$row['renewal']));
      $renewal_reminder_date = strftime("%a, %d %b %Y", strtotime($renewal_date.' - '.$row['renewal_reminder']));
      $qry->setReqDateVal("course_date",$course_date,"Course Date");
      $qry->setReqDateVal("renewal_date",$renewal_date,"Renewal Date");
      $qry->setReqDateVal("renewal_reminder_date",$renewal_reminder_date,"Renewal Reminder Date");
      //$qry->setStringVal("comments",$vals["comments"],"Comments");
      //$qry->setNumberVal("cost",$vals["cost"],"Cost");
      $qry->setReqStringVal("state","IN PROGRESS","State");
      $qry->setAudit($clsUS->userid);
      $qry->setReqNumberVal("csv_import",$csv_id,"csv id");			

      if ($qry->execute($con)) {
        $id=$qry->getNewID();
        doLog("INFO","course insert id:$id sql:".$qry->getSql());

      } else {
        doLog("ERROR","course insert sql:".$qry->getSql());
        $messages[] =  $qry->getError();

      }
    } else {
      $qry = new dbUpdate("training_log");
      $qry->setParam("training_id",$row['training_id']);
      		
      $course_date = strftime("%a, %d %b %Y",strtotime($date));
      $renewal_date = strftime("%a, %d %b %Y", strtotime($date.' + '.$row['renewal']));
      $renewal_reminder_date = strftime("%a, %d %b %Y", strtotime($renewal_date.' - '.$row['renewal_reminder']));
      $qry->setReqDateVal("course_date",$course_date,"Course Date");
      $qry->setReqDateVal("renewal_date",$renewal_date,"Renewal Date");
      $qry->setReqDateVal("renewal_reminder_date",$renewal_reminder_date,"Renewal Reminder Date");
      //$qry->setStringVal("comments",$vals["comments"],"Comments");
      //$qry->setNumberVal("cost",$vals["cost"],"Cost");
      $qry->setReqStringVal("state","IN PROGRESS","State");
      $qry->setAudit($clsUS->userid);
      //$qry->setReqNumberVal("csv_import",$csv_id,"csv id");			

      if ($qry->execute($con)) {
        
        doLog("INFO","course update id:".$row['training_id']." sql:".$qry->getSql());

      } else {
        doLog("ERROR","course update id:".$row['training_id']." sql:".$qry->getSql());
        $messages[] =  $qry->getError();

      }
    }
  }
}
?>
