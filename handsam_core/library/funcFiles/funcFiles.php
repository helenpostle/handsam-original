<?php

/** 
 * File uploads functionality
 * 
 *
 */

function uploadFile($filename, $from_file_path, $to_file_path) {
	$ext = getExtension($filename);
	$filenameNoExt = rmExtension($filename);
	 
	
	#first check if file already exists
	$cnt = 0;
	$fullFileName = $to_file_path.$filename;
	while (file_exists($fullFileName)) {
		$cnt++;
		$fullFileName = $to_file_path.$filenameNoExt.".".$cnt.$ext;
		$filename = $filenameNoExt.".".$cnt.$ext;
	}
	//die ("from: ".$from_file_path." to: ".$to_file_path.$filename);
	if (copy ($from_file_path, $to_file_path.$filename)) {
		
		return $filename;	
	} else {
		return false;
	}	
}


function getExtension($filename) {
	$pos=strrpos($filename, ".");
	$extension=substr($filename,$pos);
	return $extension;
	
}


function rmExtension($filename) {
	$ext = strrchr($filename , ".");
	$filename = str_replace($ext , "", $filename);
	return $filename;	
}


function deleteFile($uploaded_filename, $to_file_path) {
	if ($uploaded_filename != "" && file_exists($to_file_path.$uploaded_filename)) {
		return unlink ($to_file_path.$uploaded_filename);
	}
}