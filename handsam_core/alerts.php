<?php
/**
* Standard Index Page
*/
$this_page = "alerts.php";
//include basic stuff
//make this a secure page
$secure = true;
include ("../shared/startPage.php");
$pageFiles->addFunc('content');
$pageFiles->addFunc('history');

//specify page content

$pageFiles->includePhp();

$title = "";

$content = "no_modules.php";
if (isLoggedIn() && isAllowed('editAccount')) {

	//get licences for this account
	$SA_qry = "select distinct folder from accountlicence left join apps on accountlicence.appid = apps.appid where accountlicence.accountid = ".$clsUS->accountid;
	$SA_rs = getRS($con, $SA_qry);
	while ($row = getRow($SA_rs)) {
            $content = "index_modules.php";
            require_once("$rootPath/modules/{$row['folder']}/shared/index_inc.php");	
	}
}
//include layout.
include ("layout.php");

?>