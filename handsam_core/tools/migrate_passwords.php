<?php
/* update user passwords to secure passwords
*
* For admin and super admin users
* content: useredit.php, userlist.php
* boxes: none
*/
ini_set('max_execution_time', '0'); // 0 = no limit.
/**
 * Include basic stuff
 */
####make this a secure page
$secure = false;
$title = "migrate passwords";
require("../../shared/startPage.php");
$pageFiles->csrf_form = '';
$content = "tools.php";
$pageFiles->includePhp();

//if (isAllowed("editHandsamSuperAdmin")) {
	$password = new clsUserPassword($homePath, $password_arr);
	//$password->updatePasswords($con, 0,1);//user id 1 only
	//$password->updatePasswords($con, 1);//account id 1 only
	$password->updatePasswords($con);//whole system 
//}

//include layout.
include ("layout.php");

?>