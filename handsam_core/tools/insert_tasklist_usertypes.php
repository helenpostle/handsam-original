<?php
 error_reporting(E_ALL); 
 ini_set("display_errors", 1); /**
* update user passwords to secure passwords
*
* For admin and super admin users
* content: useredit.php, userlist.php
* boxes: none
*/
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$title = "migrate passwords";
require("../../shared/startPage.php");
$content = "tools.php";
$pageFiles->includePhp();

$admin_type_arr = array(
	1 => "Administrator",
	2 => "evc",
	3 => "Accident Recorder",
	4 => "Training Admin");


$user_type_arr = array(
	1 => "User",
	2 => "visit planner",
	4 => "Training Staff");
	
	
$acc_qry = " > 15";
	
if (isAllowed("editHandsamSuperAdmin")) {
	//first loop through admin users
	$qry = "select userid from usr where usertype = 'Administrator' and accountid $acc_qry";
	$rs = getRS($con,$qry);
	while ($row = getRow($rs)) {
		foreach ($admin_type_arr as $key => $val) {
			$check = "select * from usr_usrtype where userid = {$row['userid']} and usertype = '$val' ";
			if ($key == 1) $check .= " and tasklistid = 1";
			$checkRS = getRS($con, $check);
			if (getResultRows($checkRS) == 0) {
			
				$qry = new dbInsert('usr_usrtype');
				if ($key == 1) $qry->setNumberVal("tasklistid",1,"Task list");
				$qry->setNumberVal("userid",$row['userid'],"Userid");
				$qry->setStringVal("usertype",$val,"usertype");
				if ($qry->execute($con)) {
					$messages[] = "INSERT Userid{$row['userid']} = $val";
				} else {
					$messages[] = "ERROR Userid{$row['userid']} = $val";
				}
			}
		}
	}
	//next loop through  users
	$qry = "select userid from usr where usertype = 'User' and accountid $acc_qry";
	$rs = getRS($con,$qry);
	while ($row = getRow($rs)) {
		foreach ($user_type_arr as $key => $val) {
			$check = "select * from usr_usrtype where userid = {$row['userid']} and usertype = '$val' ";
			if ($key == 1) $check .= " and tasklistid = 1";
			$checkRS = getRS($con, $check);
			if (getResultRows($checkRS) == 0) {
			
				$qry = new dbInsert('usr_usrtype');
				if ($key == 1) $qry->setNumberVal("tasklistid",1,"Task list");
				$qry->setNumberVal("userid",$row['userid'],"Userid");
				$qry->setStringVal("usertype",$val,"usertype");
				if ($qry->execute($con)) {
					$messages[] = "INSERT Userid{$row['userid']} = $val";
				} else {
					$messages[] = "ERROR Userid{$row['userid']} = $val";
				}
			}
		}
	}
}

//include layout.
include ("layout.php");
?>