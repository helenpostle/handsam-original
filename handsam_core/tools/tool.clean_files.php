<?php
if (isAllowed("handsamOnly")) {

	//need to get list of all filenames from db
	$qry = "select filename from files where mid is null";
	$rs = getRS($con,$qry);
	$files = array();
	while ($row = getRow($rs)) {
		$files[] = $row['filename'];
	}

	$extra_files = array();
	$folders = array("Handsam Docs" => $handsamdocs_fp, "User Docs" => $userdocs_fp, "Admin Docs" => $admindocs_fp, "Lotc Docs" => $accountLotcDocs_fp, "Lotc User Docs" => $accountLotcUserDocs_fp, "Incident Docs" => $accountIncidentDocs_fp, "Training Docs" => $accountTrainingDocs_fp);

	foreach ($folders as $key => $val) {
		if (file_exists($val)) {
			$handle = opendir($val);
			while($file = readdir($handle)){
				if($file !== '.' && $file !== '..'){
					if (!in_array($file, $files)) $extra_files[$key][] = $file;
				}
			}
		}
	}	
	
	if (buttonClicked("delete")) {
		$response = array();
		foreach ($folders as $key => $val) {
			
			if (array_key_exists($key, $extra_files) && count($extra_files[$key]) > 0) {
				
				foreach ($extra_files[$key] as $afile) {
					if (unlink($val.$afile)) {
						$response[$key][] = "$afile - deleted successfully";
					} else {
						$response[$key][] = "$afile - delete FAILED";
					}
				}
			}
		}
		$title = "Extra Server Files Deleted";
		$content = "tools.list_deleted_files.php";
		
	} else {
		$title = "Extra Server Files";
		$content = "tools.list_excess_files.php";
	}
}
?>