<?php
ini_set('max_execution_time', '0'); // 0 = no limit.

/* update user unique string if haven't got one
*
* For admin and super admin users
* 
* boxes: none
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

$title = "add unique string to users";
require("../../shared/startPage.php");
$content = "tools.php";
$pageFiles->csrf_form = '';
$pageFiles->includePhp();
//if (isAllowed("editHandsamSuperAdmin")) {
	$qry = new dbUpdate('usr');
	$qry->setNullParam('unique_string');
	$qry->setStringParam('unique_string',"", "or");
	$qry->setTableColVal('unique_string', 'concat(username, accountid, firstname, userid)', 'unique string');
	if (!$qry->execute($con)) {
		$messages[] =$qry->getError();
	}
//}

//include layout.
include ("layout.php");

?>