<?php
if (isAllowed("handsamOnly")) {
  
  require_once("$rootPath/handsam_core/library/funcFiles/parsecsv.lib.php");
  require_once("$rootPath/handsam_core/library/funcFiles/funcFiles.php");
  require_once("$rootPath/handsam_core/library/funcFiles/funcCSVImport.php");
  $userid = $clsUS->userid;
  $fileid = getIntFromRequest("fileid");
  $content = "csv_upload.php";
  $to_file_path = $csv_import_fp;
  
  //general config here
  
  $accounts_arr = array();
  
  $usertype_arr = array('Training Staff'); //can be added to UI later
  $create_account = 0; //false; //can be added to UI later
  
  $fields_arr = array('username'=>'E_mail_id', 'email'=>'E_mail_id', 'accountname'=>'Academy', 'firstname'=>'Forename', 'lastname'=>'Surname', 'int_ref'=>'Emply_ref'); //can be added to UI later
  
   $accountname_fld = $fields_arr['accountname']; 
   
  if ($fileid !== null) {
    $qry_f = "select f.account_arr, f.state, f.title, f.filename, f.id, f.description, f.created, f.edited, f.created_by, f.edited_by FROM csv_import f  where f.id = $fileid ";
    $rsFiles = getRS($con,$qry_f);
    $rowFile = getRow($rsFiles);
    
    if (getResultRows($rsFiles) == 0 || $rowFile['state'] == "READY") {
      //READY FOR IMPORT
      if (buttonClicked("cancel")) {
        #return to file list
        headerLocation("$rootUrl/handsam_core/tools.php?tool=3",false);
      } else 	if (buttonClicked("save")) {
        #INITIAL UPLOAD FILE
        #get filename and previously uploaded filename from form
        $filename = $_FILES['filename']['name'];
        $uploaded_filename = $_POST['uploaded_filename'];
        if ($filename != "") {
          #if filename not empty then upload file
          $file_path=$_FILES['filename']['tmp_name'];
          $upload_file = uploadFile($filename, $file_path, $to_file_path);
          if($upload_file) {
            #if upload successful check for previously uploaded filename
            if ($uploaded_filename != "") {
              #delete this 
              $deleteFile = deleteFile($uploaded_filename, $to_file_path);
            }
            #now set previously uploaded filename as this uploaded filename - incase form doesn't validate then user doesn't have to re-select a new file
            $filename = $upload_file;
          } else {
            $messages[] = ERROR_FILE_UPLOAD;
          }
        } else if ($uploaded_filename != "") {
          $filename = $uploaded_filename;
        }
        if ($fileid == 0) {
          $qry = new dbInsert("csv_import");
        } else {
          $qry = new dbUpdate("csv_import");
          $qry->setParam("id",$fileid);
        }
        if (isset($qry)) {
          $qry->setReqStringVal("filename",$filename,"filename");
          $qry->setReqStringVal("title",$_POST["title"],"Title");
          $qry->setStringVal("description",$_POST["description"],"Description");
          $qry->setNumberVal("create_accounts",$create_account,"Create Account");
          $qry->setStringVal("usertype_arr",json_encode($usertype_arr),"usertypes");
          $qry->setStringVal("fields",json_encode($fields_arr),"fields");
          $qry->setStringVal("state","READY","State");
          $qry->setAudit($clsUS->userid);
          
        
          if ($qry->execute($con)) {
            $messages[] = SAVED;
            if ($fileid == 0) {
              $fileid = $qry->getNewID();
              headerLocation("$rootUrl/handsam_core/tools.php?tool=3&fileid=$fileid",false);
            } else {
              headerLocation("$rootUrl/handsam_core/tools.php?tool=3",false);
            }
          } else {
            $messages[] = $qry->getError();	
          }
        }
      } else 	if (buttonClicked("import")) {
        #RUN IMPORT STAGE 1
        ob_start();
        include("$rootPath/handsam_core/content/csv_import_stage1.php");
        $import_content = ob_get_contents();
        ob_end_clean();
      } else 	if (buttonClicked("import_stage2")) {
        #SAVE ARRAY OF ACCOUNTS 
        $account_names = array();
        $acc_error = false;
        for($i=0; $i<count($_POST['account_name']); $i++) {
          if ($_POST['account_name'][$i] > 1 || $_POST['account_name'][$i]==0) {
            $account_names[$_POST['csv_name'][$i]] = $_POST['account_name'][$i];
          } else {
            $acc_error = true;
          }
        }
       
        if ($acc_error==true) {
         
          $messages[] = "Please select a system account for all the CSV accounts. Not including the Admin account";
          ob_start();
          include("$rootPath/handsam_core/content/csv_import_stage1.php");
          $import_content = ob_get_contents();
          ob_end_clean();
        } else {
          #NOW SAVE ACCOUNT SETTINGS
          $qry = new dbUpdate("csv_import");
          $qry->setParam("id",$fileid);
          $qry->setStringVal("account_arr",json_encode($account_names),"account settings");
          if ($qry->execute($con)) {
            $messages[] = SAVED;
            processCSVImport($con, $rowFile['id'], $to_file_path);
            
          } else {
            $messages[] = "Error saving account settings";
          }
        }
      } else if(buttonClicked("view")) {
        $csv = new parseCSV();

        $csv->auto("$to_file_path/".$rowFile['filename']);
        ob_start();
        ?>

        <style type="text/css" media="screen">
          table { background-color: #BBB; }
          th { background-color: #EEE; }
          td { background-color: #FFF; }
        </style>
        <table border="0" cellspacing="1" cellpadding="3">
          <tr>
            <?php foreach ($csv->titles as $value): ?>
            <th><?php echo $value; ?></th>
            <?php endforeach; ?>
          </tr>
          <?php foreach ($csv->data as $key => $row): ?>
          <tr>
            <?php foreach ($row as $value): ?>
            <td><?php echo $value; ?></td>
            <?php endforeach; ?>
          </tr>
          <?php endforeach; ?>
        </table>
        <?php
        $import_content = ob_get_contents();
        ob_end_clean();
      } else {
        #IMPORT DONE - OPTION TO REVERSE IMPORT

      }
    }
  } else {
    $qry_f = "select f.title, f.filename, f.id, f.description, f.created, f.edited, f.created_by, f.edited_by FROM csv_import f ";
    $rsFiles = getRS($con,$qry_f);

  }
  
  
  
}

	
?>