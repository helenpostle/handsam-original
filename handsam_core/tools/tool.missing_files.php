<?php
if (isAllowed("handsamOnly")) {

	$folders = array("Handsam Docs" => $handsamdocs_fp, "User Docs" => $userdocs_fp, "Admin Docs" => $admindocs_fp, "Lotc Docs" => $accountLotcDocs_fp, "Lotc User Docs" => $accountLotcUserDocs_fp, "Incident Docs" => $accountIncidentDocs_fp, "Training Docs" => $accountTrainingDocs_fp);
	$filetype = array("handsamDoc" => "Handsam Docs", "userDoc" => "User Docs", "adminDoc" => "Admin Docs", "accountLotcDoc" => "Lotc Docs", "accountLotcUserDoc" => "Lotc User Docs", "accountIncidentDoc" => "Incident Docs", "accountTrainingDoc" => "Training Docs");
	$files = array();
	foreach ($filetype as $mk => $mv) {
		$files[$mk] = array();
	}


	//need to get list of all filenames from db
	$qry = "select filetype, fileid, filename from files where mid is null";
	$rs = getRS($con,$qry);

	while ($row = getRow($rs)) {
		$files[$row['filetype']][$row['fileid']] = $row['filename'];
	}

	
	foreach ($folders as $key => $val) {
		if (file_exists($val)) {
			$handle = opendir($val);
			while($file = readdir($handle)){
				if($file !== '.' && $file !== '..'){
					foreach ($filetype as $mk => $mv) {
						$files[$mk] = array_diff($files[$mk], array($file));
					}
				}
			}
		}
	}
	$title = "Missing Server Files";
	$content = "tools.list_missing_files.php";
	
}
?>