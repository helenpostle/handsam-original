<?php
 error_reporting(E_ALL); 
 ini_set("display_errors", 1); /**
* update user passwords to secure passwords
*
* For admin and super admin users
* content: useredit.php, userlist.php
* boxes: none
*/
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
$title = "migrate repeating tasks";
require("../../shared/startPage.php");
$content = "tools.php";
$pageFiles->includePhp();
if (isAllowed("editHandsamSuperAdmin")) {
	//first loop current and future licences
	$qry = "select licenceid, accountid, tasklistid, EXTRACT(YEAR FROM start_date) AS start_date, EXTRACT(YEAR FROM end_date) AS end_date from accountlicence where end_date > NOW() and appid = 1 and tasklistid > 0 and state in ('ACTIVE')";
	$rs = getRS($con,$qry);
	while ($row = getRow($rs)) {
	echo "<br/><br/>lic qry: ".$row['licenceid']." :: ".$qry;
      //select all repeating tasks that this account licence doesn't have as an accounttask_repeating entry
      $qry = "select tr.id, atr.id as atr_id, tr.weekly, tr.month_date,tr.position_in_month,tr.month_day,tr.repeat_duration,tr.repeat_start_month,tr.repeat_start_day,tr.repeat_end_month,tr.repeat_end_day, tr.state, t.tasklistid from task_repeating tr left join accounttask_repeating atr on tr.id = atr.repeatingtaskid and atr.licenceid = {$row['licenceid']} left join task t on tr.id = t.repeatingtaskid where tr.state = 'ACTIVE' and atr.id is null and tasklistid = {$row['tasklistid']} and t.state = 'ACTIVE' group by tr.id, tr.weekly, tr.month_date,tr.position_in_month,tr.month_day,tr.repeat_duration,tr.repeat_start_month,tr.repeat_start_day,tr.repeat_end_month,tr.repeat_end_day, tr.state, atr_id, tasklistid";
      $rs2 = getRS($con,$qry);
      while ($row2 = getRow($rs2)) {
          //insert into accounttask_repeating
          $db = new dbInsert('accounttask_repeating');
          $db->setNumberVal('weekly', $row2['weekly'], 'weekly');
          $db->setNumberVal('month_date', $row2['month_date'], 'month_date');
          $db->setNumberVal('position_in_month', $row2['position_in_month'], 'position_in_month');
          $db->setNumberVal('month_day', $row2['month_day'], 'month_day');
          $db->setNumberVal('repeat_duration', $row2['repeat_duration'], 'repeat_duration');
          $db->setNumberVal('repeat_start_month', $row2['repeat_start_month'], 'repeat_start_month');
          $db->setNumberVal('repeat_start_day', $row2['repeat_start_day'], 'repeat_start_day');
          $db->setNumberVal('repeat_end_month', $row2['repeat_end_month'], 'repeat_end_month');
          $db->setNumberVal('repeat_end_day', $row2['repeat_end_day'], 'repeat_end_day');
          $db->setNumberVal('accountid', $row['accountid'], 'accountid');
          $db->setNumberVal('licenceid', $row['licenceid'], 'licenceid');
          $db->setNumberVal('repeatingtaskid', $row2['id'], 'id');
          $db->setStringVal('state', $row2['state'], 'state');
          
          $db->setNumberVal('repeat_start_year', $row['start_date'], 'start year');
          $db->setNumberVal('repeat_end_year', $row['end_date'], 'end year');
          
          $db->setAudit($clsUS->userid);
          if (!$db->execute($con)) {
              $messages[] = "licenceid: ".$row['licenceid']." :: repeatingtaskid: ".$row2['id']." :: inserting accounttask_repeating: ".$db->getError();
          } else {
              echo "<br/>inserted licenceid: ".$row['licenceid']." :: repeatingtaskid: ".$row2['id'];
              $accounttask_repeatingid = $db->getNewID();
              //update accounttasks in this licence with the same repeatingtaskid
              $db = new dbUpdate('accounttask');
              $db->setParam('repeatingtaskid', $row2['id']);
              $db->setParam('licenceid', $row['licenceid']);
              $db->setNumberVal('accounttask_repeatingid', $accounttask_repeatingid, 'accounttask_repeatingid');
              if (!$db->execute($con)) {
                  $messages[] = "licenceid: ".$row['licenceid']." :: repeatingtaskid: ".$row2['id']." :: updating accounttask: ".$db->getError();
              }
          }
          
          
          
          
          
      }
      
	}
	
}

//include layout.
include ("layout.php");
?>