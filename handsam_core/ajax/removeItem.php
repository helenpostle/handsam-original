<?php
/**
* attach item - file or link to web page
*/


//MUST ADD SECURITUY AND CHECK FILE EXISTS
$content = "";
//include basic stuff
$secure = true;
$ajaxModule = "handsam_core";

include ("../../shared/startPage.php");


//make this a secure page
$pageFiles->includePhp();

//get the id 
$itemid = getIntFromRequest("itemid");
$contentid = getIntFromRequest("contentid");
$type = getStrFromRequest("type");

if (isAllowed("editContent")) {

	if ($type == 'file') {
		$qry = new dbDelete("content_files");
		$qry->setParam("pagefileid",$itemid);
		$qry->setParam("contentid",$contentid);
		if ($qry->execute($con)) {
			echo "File removed from page";
		} else {
			echo "Error removing file: ".$qry->getSql();
		}
	
	} elseif ($type == "link") {
	
		$qry = new dbDelete("content_links");
		$qry->setParam("pagelinkid",$itemid);
		$qry->setParam("contentid",$contentid);
		if ($qry->execute($con)) {
			echo "Link removed from page";
		} else {
			echo "Error removing link";
		}
	
	}

	
}