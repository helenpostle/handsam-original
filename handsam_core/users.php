<?php

/**
 * User editing functionality
 *
 * For admin and super admin users
 * content: useredit.php, userlist.php
 * boxes: none
 */
/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;

require("../shared/startPage.php");

$pageFiles->addFunc('email_alert_checkbox');
$pageFiles->addFunc('user_table');
$pageFiles->addFunc('user');
$pageFiles->addFunc('email');

$pageFiles->includePhp();

#check if have requested a specific user
$userid = getIntFromRequest("userid2");
if($userid === null)
    $userid = getIntFromRequest("userid");
$aid = getIntFromRequest("accountid2");
if($aid === null)
    $aid = getIntFromRequest("accountid");
$return = getStrFromRequest("return");

// if the user is viewing their own account
$self = false;
if($userid == $clsUS->userid)
{
    $self = true;
}

$title = "Edit User";
if($self)
{
    $title = "My Details";
}

#set which user list includes file to include
$userlist = 'userlist.php';
if(isAllowed("editAccount"))
    $userlist = 'superadmin_list.php';

if($userid !== null && (isAllowed("editUsers") || $self))
{
    // we can do this if it is ourself or we have permission to edit other users.
    $content = "useredit.php";

    $sqlUser = "SELECT usr.*, account.accountname FROM usr LEFT JOIN account ON usr.accountid = account.accountid WHERE usr.userid = $userid";
    
    $rsUser = getRS($con, $sqlUser);
    $rowUser = getRow($rsUser);

    if((isAllowed("editAccount") && $rowUser['usertype'] != 'HandsamSuperAdmin') 
            || isAllowed('editHandsamSuperAdmin') 
            || (isAllowed("editUsers") && $userid == 0) 
            || (isAllowed("editUsers") && $rowUser["accountid"] == $clsUS->accountid && $rowUser['usertype'] != 'HandsamSuperAdmin') 
            || $self)
    {

        if($userid != 0)
        {
            $aid = $rowUser["accountid"];
        }

        // check if the accountid is an admin account
        $admin_account_user = false;
        if($aid == $clsUS->accountid && isAllowed("editAccount"))
        {
            $admin_account_user = true;
        }

        // work out account id	
        $accountid = $clsUS->accountid;
        if(isAllowed("editAccount") && isset($aid))
        {
            $accountid = $aid;
        }

        // which user types
        $userTypes = array("None" => "No Access", "Administrator" => "Administrator", "User" => "User");
        if($self && !isAllowed("editAccount"))
        {
            $userTypes = array();
        }

        if(buttonClicked("save") && editUserCheck($clsUS, $userid, $accountid, $con))
        {
            if($userid == 0)
            {
                // check that a user doesn't exist with this name
                $rsCheck = getRS($con, "select * from usr where username = '" . $_POST["username"] . "'");
                if(getRow($rsCheck))
                {
                    $messages[] = USERNAME_IN_USE;
                }
                else
                {
                    $qry = new dbInsert("usr");
                    $qry->setReqNumberVal("accountid", $accountid, "Account id");
                }
            }
            else
            {
                // check that a user doesn't exist with this name
                $rsCheck = getRS($con, "select * from usr where username = '" . $_POST["username"] . "' and userid != $userid");
                if(getRow($rsCheck))
                {
                    $messages[] = USERNAME_IN_USE;
                }
                else
                {
                    $qry = new dbUpdate("usr");
                    $qry->setParam("userid", $userid);
                }
            }

            if(isset($qry))
            {
                if(isAllowed("editUsers"))
                {
                    $qry->setReqEmailVal("username", $_POST["username"], "Email Username");
                    $qry->setReqEmailVal("email", $_POST["username"], "Email Username");
                    if($core_conf['user_payroll']) {
                      if (isset($accountid) && $accountid == 1 && isset($_POST['payroll'])) {
                        $qry->setStringVal('payroll', $_POST['payroll'], 'Payroll Number');
                      } else {
                        $qry->setReqStringVal('payroll', $_POST['payroll'], 'Payroll Number');
                      }
                      
                    }
                }

                $qry->setReqStringVal("firstname", $_POST["firstname"], "First Name");
                $qry->setReqStringVal("lastname", $_POST["lastname"], "Last Name");
                if(!$self)
                {
                    $qry->setReqStringVal("state", $_POST["state"], "State");
                }
                
                // Key Holders
                $address_required = false;
                if($core_conf['key_holder'])
                {
                    //if(isset($_POST['key_holder']) && $_POST['key_holder'] == 1)
					
					if (isset($_POST['key_holder']) && $_POST['key_holder'] == 1) {
						$address_required = TRUE;
						$qry->setBooleanVal('key_holder', $_POST['key_holder'], 'Key Holder');
					} else {
						$qry->setBooleanVal('key_holder', 0, 'Key Holder');
					}
                    
                }
                
                // if the Key Holder is set, address fields are required
                if($address_required == true)
                {
                    $qry->setReqStringVal("address1", $_POST["address1"], "Address 1");
                    $qry->setReqStringVal("city", $_POST["city"], "City");
                    $qry->setReqStringVal("postcode", $_POST["postcode"], "Postcode");
                    $qry->setReqStringVal("tel1", $_POST["tel1"], "Telephone 1");
                }
                else
                {
                    $qry->setStringVal("address1", $_POST["address1"], "Address 1");
                    $qry->setStringVal("city", $_POST["city"], "City");
                    $qry->setStringVal("postcode", $_POST["postcode"], "Postcode");
                    $qry->setStringVal("tel1", $_POST["tel1"], "Telephone 1");
                }
                
                // following are optional either way
                $qry->setStringVal("address2", $_POST["address2"], "Address 2");
                $qry->setStringVal("address3", $_POST["address3"], "Address 3");
                $qry->setStringVal("tel2", $_POST["tel2"], "Telephone 2");
                
                // if an admin account user, then check that they've got an SA usertype selected
                if($admin_account_user && !$self)
                {
                    if(!isset($_POST['HSA']) && (!isset($_POST['SA_usrtype']) || !is_array($_POST['SA_usrtype'])))
                        $qry->failError .= "Please select at least one ADMIN usertype.\r\n";

                    if(isset($_POST['HSA']) && $_POST['HSA'] == 1 && isAllowed('editHandsamSuperAdmin'))
                    {
                        $qry->setReqStringVal("usertype", "HandsamSuperAdmin", "usertype");
                    }
                    else
                    {
                        $qry->setReqStringVal("usertype", "User", "usertype");
                    }
                }

                $qry->setAudit($clsUS->userid);
                if($qry->execute($con))
                {
                    if($userid == 0)
                    {
                        // email them their first random password
                        $passwd = new clsUserPassword($homePath, $password_arr);
                        $messages[] = $passwd->emailPassword($_POST["username"], $con, $adminMail);

                        $userid = $qry->getNewID();

                        // now add their unique_string
                        $qry = new dbUpdate('usr');
                        $qry->setParam("userid", $userid);
                        $qry->setNullParam('unique_string');
                        $qry->setStringParam('unique_string', "", "or");
                        $qry->setTableColVal('unique_string', 'concat(username, accountid, firstname, userid)', 'unique string');
                        if(!$qry->execute($con))
                        {
                            $messages[] = $qry->getError();
                        }
                    }
                    if(!$self)
                    {
                        // first deal with the SA usertypes if they are a SA user
                        if($admin_account_user)
                        {
                            // if (isset($_POST['SA_usrtype'])) {
                            $SA_qry = "select SA_usr_usrtype as a, SA_usrtype as b from core_SA_usr_usrtype where userid = $userid";

                            $SA_POST_ARR = array();
                            if(isset($_POST['SA_usrtype']))
                                $SA_POST_ARR = $_POST['SA_usrtype'];
                            $saUsertype = new dbMultiAssign($SA_POST_ARR, $SA_qry, $con);

                            // loop through the del items array and del using the key value				
                            foreach($saUsertype->del as $del_id => $item)
                            {
                                $qrySA = new dbDelete("core_SA_usr_usrtype");
                                $qrySA->setParam("SA_usr_usrtype", $del_id);
                                if(!$qrySA->execute($con))
                                {
                                    $messages[] = $qrySA->getError();
                                }
                            }

                            // loop through the add array and insert usertype for this SA user
                            foreach($saUsertype->add as $add_id)
                            {
                                if($add_id != "")
                                {
                                    $qrySA = new dbInsert("core_SA_usr_usrtype");
                                    $qrySA->setReqStringVal("SA_usrtype", $add_id, "SA usertype");
                                    $qrySA->setReqNumberVal("userid", $userid, "SA userid");
                                    if(!$qrySA->execute($con))
                                    {
                                        $messages[] = $qrySA->getError();
                                    }
                                }
                            }
                            //}
                        }

                        // now deal with normal usertypes
                        // loop through each installed tasklist
                        if(in_array("tasklist", $installed_modules))
                        {
                            $qryTL = "SELECT DISTINCT tasklist.tasklistid, tasklist.tasklistname FROM tasklist LEFT JOIN accountlicence ON tasklist.tasklistid = accountlicence.tasklistid WHERE accountlicence.accountid = $accountid AND accountlicence.state = 'ACTIVE' AND accountlicence.end_date > NOW()";
                            $rsTL = getRS($con, $qryTL);
                            
                            while ($rowTL = getRow($rsTL))
                            {
                                $tl_qry = " SELECT usertypeid AS a, usertype AS b FROM usr_usrtype WHERE userid = $userid AND tasklistid = {$rowTL["tasklistid"]}";
                                $usrtype = getStrFromRequest("usertype_" . $rowTL["tasklistid"]);
                                if($usrtype == "None")
                                    $usrtype = "";
                                $tl_ut = array();
                                $tl_ut[] = $usrtype;
                                $tlUsertype = new dbMultiAssign($tl_ut, $tl_qry, $con);
                                
                                // loop through the del items array and del using the key value				
                                foreach($tlUsertype->del as $del_id => $item)
                                {
                                    $qryTL = new dbDelete("usr_usrtype");
                                    $qryTL->setParam("usertypeid", $del_id);
                                    $qryTL->setParam("userid", $userid);
                                    if(!$qryTL->execute($con))
                                    {
                                        $messages[] = $qryTL->getError();
                                    }
                                }

                                // loop through the add array and insert usertype for this  user
                                foreach($tlUsertype->add as $add_id)
                                {
                                    if($add_id != "")
                                    {
                                        $qryTL = new dbInsert("usr_usrtype");
                                        $qryTL->setReqStringVal("usertype", $add_id, "usertype");
                                        $qryTL->setReqNumberVal("userid", $userid, "SA userid");
                                        $qryTL->setReqNumberVal("tasklistid", $rowTL["tasklistid"], "tasklist id");
                                        if(!$qryTL->execute($con))
                                        {
                                            $messages[] = $qryTL->getError();
                                        }
                                    }
                                }
                            }
                        }

                        // now loop through each app except tasks app
                        $qryTL = "SELECT DISTINCT appid FROM accountlicence WHERE accountlicence.accountid = $accountid AND appid != 1";
                        $rsTL = getRS($con, $qryTL);
                        $ap_ut = array();
                        while ($rowTL = getRow($rsTL))
                        {
                            //build array of posts:
                            $usrtype = getStrFromRequest("app_usertype_" . $rowTL["appid"]);
                            if($usrtype == "None")
                                $usrtype = "";
                            $ap_ut[] = $usrtype;                           			
                        }

                        $ap_qry = "select usertypeid as a, usertype as b from usr_usrtype where userid=$userid and tasklistid is null";
                        $apUsertype = new dbMultiAssign($ap_ut, $ap_qry, $con);
                        // loop through the del items array and del using the key value				
                        foreach($apUsertype->del as $del_id => $item)
                        {
                            $qryTL = new dbDelete("usr_usrtype");
                            $qryTL->setParam("usertypeid", $del_id);
                            $qryTL->setParam("userid", $userid);
                            if(!$qryTL->execute($con))
                            {
                                $messages[] = $qryTL->getError();
                            }
                        }

                        // loop through the add array and insert usertype for this  user
                        foreach($apUsertype->add as $add_id)
                        {
                            if($add_id != "")
                            {
                                $qryTL = new dbInsert("usr_usrtype");
                                $qryTL->setReqStringVal("usertype", $add_id, "usertype");
                                $qryTL->setReqNumberVal("userid", $userid, "SA userid");
                                if(!$qryTL->execute($con))
                                {
                                    $messages[] = $qryTL->getError();
                                }
                            }
                        }
                    }
                    
                    // save the user's job role(s)
                    if($core_conf['jobroles'] === true)
                    {
                        if(!isset($_POST['role_id']))
                        {
                            $_POST['role_id'] = array("");
                        }
                        $jr_qry = " SELECT id as a, jobrole_id as b FROM core_usr_jobrole WHERE usr_id = $userid";
                        $jr = new dbMultiAssign($_POST['role_id'], $jr_qry, $con);
                        
                        // loop through the del items array and del using the key value				
                        foreach($jr->del as $del_id => $item)
                        {
                            $qryJR = new dbDelete("core_usr_jobrole");
                            $qryJR->setParam("id", $del_id);
                            $qryJR->setParam("usr_id", $userid);
                            if(!$qryJR->execute($con))
                            {
                                $messages[] = $qryJR->getError();
                            }
                        }

                        // loop through the add array and insert job role for this user
                        foreach($jr->add as $add_id)
                        {
                            if($add_id != "")
                            {
                                $qryJR = new dbInsert("core_usr_jobrole");
                                $qryJR->setReqNumberVal("jobrole_id", $add_id, "jobrole id");
                                $qryJR->setReqNumberVal("usr_id", $userid, "jobrole userid");
                                if(!$qryJR->execute($con))
                                {
                                    $messages[] = $qryJR->getError();
                                }
                            }
                        }
                    }

                    // /user job role

                    $messages[] = SAVED;

                    $contentId = "contentWide";
                    $content = $userlist;
                    
                    if(isAllowed("editAccount") && $return == "users")
                    {
                        //do nothing else
                    }
                    else if($admin_account_user)
                    {
                        headerLocation("my_account.php", true);
                    }
                    else if($accountid != $clsUS->accountid)
                    {
                        headerLocation("accounts.php?accountid=$accountid", true);
                    }
                    else if($return == "myDetails")
                    {
                        $content = "useredit.php";
                    }
                }
                else
                {
                    $messages[] = $qry->getError();	
                }
            }
        }
        else if(buttonClicked("delete"))
        {
            // cannot delete self
            if(!$self && editUserCheck($clsUS, $userid, $accountid, $con))
            {
                $qry = new dbUpdate("usr");
                $qry->setParam("userid", $userid);
                $qry->setReqStringVal("state", "DELETED", "State");
                if(!$qry->execute($con))
                {
                    $messages[] = $qry->getError();
                }
                else
                {
                    #change content to list
                    $messages[] = DELETED;
                    $contentId = "contentWide";
                    $content = $userlist;
                    $title = "User List";
                    if(isAllowed("editAccount") && $return == "users")
                    {
                        //do nothing else
                    }
                    else if($admin_account_user)
                    {
                        headerLocation("my_account.php", true);
                    }
                    elseif($accountid != $clsUS->accountid)
                    {
                        headerLocation("accounts.php?accountid=$accountid", true);
                    }
                }
            }
            else
            {
                trigger_error("Access Denied", E_USER_WARNING);
            }
        }
        else if(buttonClicked("cancel"))
        {
            $contentId = "contentWide";
            $content = $userlist;
            $title = "User List";
            if(isAllowed("editAccount") && $return == "users")
            {
                //do nothing else
            }
            else if($admin_account_user)
            {
                headerLocation("my_account.php", true);
            }
            else if($accountid != $clsUS->accountid)
            {
                headerLocation("accounts.php?accountid=$accountid", true);
            }
            else if($return == "myDetails")
            {
                $content = "useredit.php";
            }
        }
        else if(buttonClicked("reset_pass"))
        {
            $passwd = new clsUserPassword($homePath, $password_arr);
            $messages[] = $passwd->emailPassword($rowUser['username'], $con, $adminMail);
        }
        if(!$self && $userid > 0 && $content == "useredit.php")
            $pageFiles->addBox("$rootPath/handsam_core/boxes/box.accounts.reset_user_password.php");
    } else
    {
        $messages[] = NOT_EDIT_USER;
        headerLocation("users.php", $messages);
    }
}
else if(isAllowed("editUsers"))
{
    // no userid set - are we allowed to view the userlist?
    $contentId = "contentWide";
    $content = $userlist;
    $title = "User List";
}
else
{
    // trying to do something we shouldn't! throw error so it is logged.	
    headerLocation("$rootUrl/index.php", false);
    // trigger_error("Access Denied",E_USER_WARNING);
}

// Include layout page
if(isset($userid) && $userid == $clsUS->userid && $return == "myDetails")
{
    $this_page = "users.php?userid=" . $userid . "&return=myDetails";
}
else
{
    $this_page = "users.php";
}
include("layout.php");
