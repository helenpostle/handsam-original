<?php
//menu page for core
if (isLoggedIn()) {	
	//general menu items
	$my_details_menu = $clsMenu->addMenuItem("/handsam_core/myDetails.php", "MY DETAILS", "", "");
	$clsMenu->addMenuItem("/handsam_core/myDetails.php", "MY DETAILS", "", $my_details_menu);
	$clsMenu->addMenuItem("/handsam_core/password.php", "CHANGE PASSWORD", "", $my_details_menu);
	if (isAllowed("editUsers")) {
		$clsMenu->addMenuItem("/handsam_core/users.php", "MY ACCOUNT USERS", "", $my_details_menu);
	} 
	if (isAllowed("superAdminModules")) {
		$my_modules_menu = $clsMenu->addMenuItem("/handsam_core/my_modules.php", "MY MODULES", "", "");
		$clsMenu->addMenuItem("/handsam_core/my_modules.php", "MY MODULES", "", $my_modules_menu);
		$clsMenu->addMenuItem("/handsam_core/my_account.php", "MY LICENCES", "", $my_modules_menu);
	}
	if (isAllowed("editAccount")) {
		$account_admin_menu = $clsMenu->addMenuItem("/handsam_core/accounts.php", "MANAGE ACCOUNTS", "", "");
		$clsMenu->addMenuItem("/handsam_core/accounts.php", "ACCOUNTS", "", $account_admin_menu);
		if (isset($core_conf['school_cluster']) && $core_conf['school_cluster'])
    {
      $clsMenu->addMenuItem("/handsam_core/account_cluster.php", "ACCOUNT CLUSTERS", "", $account_admin_menu);
    }
		$clsMenu->addMenuItem("/handsam_core/licences.php", "LICENCES", "", $account_admin_menu);
		$clsMenu->addMenuItem("/handsam_core/system_users.php", "SYSTEM USERS", "", $account_admin_menu);
        if($core_conf['news_menu_link'])  $clsMenu->addMenuItem("/news.php", "NEWS", "", $account_admin_menu);
		if($core_conf['jobroles']) $clsMenu->addMenuItem("/handsam_core/jobroles.php", "JOB ROLES", "", $account_admin_menu);
	}
	//$clsMenu->addMenuItem($menu_url."?logout=true", "LOGOUT&nbsp;", "", "");
} else {
	$clsMenu->addMenuItem($menu_url."?login=true", "LOGIN&nbsp;", "", "");
}
?>