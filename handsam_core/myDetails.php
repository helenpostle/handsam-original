<?php

/**
* User editing functionality
*
* For admin and super admin users
* content: useredit.php, userlist.php
* boxes: none
*/

/**
 * Include basic stuff
 */
####make this a secure page
$secure = true;
 
require("../shared/startPage.php");

$pageFiles->addFunc('email_alert_checkbox');

$pageFiles->includePhp();

#check if have requested a specific user
$userid = $clsUS->userid;
$accountid = $clsUS->accountid;
$self = true;
//$return = getStrFromRequest("return");

$title = "My Details";


if ($userid !== null) {
	#we can do this if it is ourself or we have permission to edit other users.
	$content = "useredit.php";
	$sqlUser = "select usr.*, usr_usrtype.usertype, usr_usrtype.tasklistid, account.accountname from usr left join account on usr.accountid = account.accountid left join usr_usrtype on usr.userid = usr_usrtype.userid where usr.userid = $userid";
	$rsUser = getRS($con,$sqlUser);
	$rowUser = getRow($rsUser); 

		
	#which user types
	$userTypes = array();
	
	if (buttonClicked("save") && isset($_POST["username"])) {

		#check that a user doesn't exist with this name
		$rsCheck = getRS($con,"select * from usr where username = '".$_POST["username"]."' and userid != ".$clsUS->userid);
		if (getRow($rsCheck)) {
			$messages[] = USERNAME_IN_USE;	
		} else {
			$qry = new dbUpdate("usr");
			$qry->setParam("userid",$clsUS->userid);
		}				
		
		if (isset($qry)) {
			
			$qry->setReqEmailVal("username",$_POST["username"],"Email Username");
			$qry->setReqEmailVal("email",$_POST["username"],"Email Username");
			$qry->setReqStringVal("firstname",$_POST["firstname"],"First Name");
			$qry->setReqStringVal("lastname",$_POST["lastname"],"Last Name");
			
			$qry->setAudit($clsUS->userid);
			
			if ($qry->execute($con)) {
				$clsUS->username = $_POST["username"];
				$messages[] = SAVED;
				$content = "useredit.php";
				$title = "My Details";
					
			} else {
				$messages[] = $qry->getError();	
			}
		}
	} else if (buttonClicked("cancel")) {
		$content = "useredit.php";
		$title = "My Details";

	}  
	
} else {
	#trying to do something we shouldn't! throw error so it is logged.	
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);

}

include("layout.php");
?>