<?php
/**
* Standard Index Page
*/
//$this_page = "index.php";
$content = "";
//include basic stuff
$secure = true;
include ("../shared/startPage.php");
$this_page = "docList.php";


//make this a secure page
$pageFiles->addFunc('handsam_uploads');


$pageFiles->includePhp();

//specify page content



if (isAllowed("uploadHandsamDocs") || isAllowed("uploadAdminDocs") || isAllowed("uploadUserDocs")) {
	$title = "Documents on server";		
	$content = "listFiles.php";
	$userid = $clsUS->userid;
	$accountid = $clsUS->accountid;
	$fileid = setQSInt("fileid");
	$contentid = setQSInt("contentid");
	$filetype = getStrFromRequest("filetype");
	$edittype = getStrFromRequest("edit");
	$contentid = setQSInt("contentid");
	if ($edittype != "page") {
		$edittype = "task";
	}
	#for passing page and contentid between pages for adding links/files to content webpages
	if (isAllowed("editContent")) {
		$contentIdLink = "&amp;contentid=".$contentid."&amp;edit=".$edittype;
	} else {
		$contentIdLink = "";
	}
	if (isAllowed("editTask") && $filetype != "handsamDoc") {
		//$accountid = getIntFromRequest("accountid");
	}

} else {
	trigger_error("Access Denied",E_USER_WARNING);
}

//include layout.
include ("layout_FB.php");

?>