<?php

/**
 * Hooks for handsam_core code. 
 */

/**
 * File save
 * @param int $fileid PK of `files` table
 */
function hook_propogate_file_save($fileid)
{
    $file = new Propogate();
    
    $file->setTable('files')
         ->setFields(array('fileid' => 'required', 'accountid' => 'required',
                'filename', 'filetype', 'title', 'filecategory', 
             ))
         ->setPrimarykey('fileid')
         ->getRecordById($fileid);
    
    $file->send();
}

$hook->register_hook('file_save', 'propogate_file_save');

function hook_propogate_file_delete($fileid)
{
    $file = new Propogate();
    
    $file->setTable('files')
         ->setFields(array('fileid' => 'required', 'accountid' => 'required'
             ))
         ->setPrimarykey('fileid')
         ->setDelete(true)
         ->getRecordById($fileid);
    
    $file->send();
}

$hook->register_hook('file_delete', 'propogate_file_delete');