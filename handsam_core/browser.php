<?php
/**
* Standard Index Page
*/
//$this_page = "index.php";
$content = "";
//include basic stuff
$secure = true;
include ("../shared/startPage.php");
$this_page = "browser.php";


//make this a secure page
$pageFiles->addFunc('handsam_uploads');
$pageFiles->addFunc('files');

$pageFiles->includePhp();

if (isAllowed("uploadHandsamDocs") || isAllowed("uploadAdminDocs") || isAllowed("uploadUserDocs")) {

	$title = "HandSaM File Upload Manager";		
	$content = "browser.php";
	$userid = $clsUS->userid;
	$accountid = $clsUS->accountid;
	$fileid = setQSInt("fileid");
	$filetype = getStrFromRequest("filetype");
	$contentid = setQSInt("contentid");
	$edittype = getStrFromRequest("edit");
    
	if ($edittype != "page") {
		$edittype = "task";
	}
    
    if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
	$count_col = "t.taskid";	
    } else {
        $count_col = "t.accounttaskid";	
    }
    $qry_f = "select f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by, f.mid, f.filetype, count(".$count_col.") as cnt FROM files f left join task_files t on f.fileid = t.fileid  where f.fileid = $fileid group by f.title, f.filename, f.fileid, f.filecategory, f.description, f.created, f.edited, f.created_by, f.edited_by, f.mid, f.filetype";

    $rsFiles = getRS($con,$qry_f);
    $rowFile = getRow($rsFiles);

    //extra query to check for lotc files. If this is an lotc file, can't delete

    $qry_lotc = "select count(fileid) as cnt from lotc_files where fileid = $fileid";
    $rsLotc = getRS($con, $qry_lotc);
    $rowLotc = getRow($rsLotc);

    if ($rowFile['mid'] > 0 && $rowFile['filetype'] == 'handsamDoc') {
        $content = "view_file.php";
    } else {
        $content = "browser.php";
    }
    
    
	#for passing page and contentid between pages for adding links/files to content webpages
	if (isAllowed("editContent")) {
		$contentIdLink = "&contentid=".$contentid."&edit=".$edittype;
	} else {
		$contentIdLink = "";
	}
	if (isAllowed("uploadHandsamDocs") && $filetype == "handsamDoc") {
		$filetype = "handsamDoc";
		$to_file_path = $handsamdocs_fp;
	} else if (isAllowed("uploadAdminDocs") && $filetype == "adminDoc"){
		$filetype = "adminDoc";
		$to_file_path = $admindocs_fp;
	} else {
		$filetype = "userDoc";
		$to_file_path = $userdocs_fp;
	}
	if (isAllowed("editTask") && $filetype != "handsamDoc") {
		//$accountid = getIntFromRequest("accountid");
	}
	//echo $to_file_path;
	if (buttonClicked("cancel")) {
		#return to file list
		if ($filetype == "adminDoc" && isAllowed("uploadHandsamDocs")) {
			headerLocation("docList.php?filetype=$filetype&accountid=$accountid".$contentIdLink,false);
		} else {
			headerLocation("docList.php?filetype=$filetype".$contentIdLink,false);
		}
		
	} else {
	
		if ($fileid !== null) {
			//$content = "taskcategoryedit.php";	
			
			if (buttonClicked("save") && $content == "browser.php") {
					#get filename and previously uploaded filename from form
					$filename = $_FILES['filename']['name'];
					$uploaded_filename = $_POST['uploaded_filename'];
					if ($filename != "") {
						#if filename not empty then upload file
						$file_path=$_FILES['filename']['tmp_name'];
						//die($file_path);
						$upload_file = uploadFile($filename, $file_path, $to_file_path);
						if($upload_file) {
							//die();
							#if upload successful check for previously uploaded filename
							if ($uploaded_filename != "") {
								#delete this 
								$deleteFile = deleteFile($uploaded_filename, $to_file_path);
							}
							#now set previously uploaded filename as this uploaded filename - incase form doesn't validate then user doesn't have to re-select a new file
							$filename = $upload_file;
						} else {
							$messages[] = ERROR_FILE_UPLOAD;
						}
							
						
					} else if ($uploaded_filename != "") {
						$filename = $uploaded_filename;
					}
					if ($fileid == 0) {
						$qry = new dbInsert("files");
						$qry->setReqStringVal("filetype",$_POST["filetype"],"Filetype");
					} else {
						$qry = new dbUpdate("files");
						$qry->setParam("fileid",$fileid);
					}
				
					if (isset($qry)) {
						$qry->setReqStringVal("filename",$filename,"filename");
						$qry->setReqStringVal("title",$_POST["title"],"Title");
						//$qry->setStringVal("description",$_POST["description"],"Description");
						$qry->setReqNumberVal("accountid",$accountid,"Account id");
						$qry->setReqNumberVal("userid",$userid,"User id");
						if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
							$qry->setStringVal("filecategory",$_POST["filecategory"],"File Category");
						}
						
						$qry->setAudit($clsUS->userid);
						
						if ($qry->execute($con)) {
							if ($fileid == 0) {
								$fileid = $qry->getNewID();	
							}
							$messages[] = SAVED;
							if ($filetype == "adminDoc" && isAllowed("uploadHandsamDocs")) {
								headerLocation("docList.php?filetype=$filetype&accountid=$accountid".$contentIdLink,$messages);
							} else {
								headerLocation("docList.php?filetype=$filetype".$contentIdLink,$messages);
							}
							#change content to list
							//$content = "taskcategorylist.php";
							
						} else {
							$messages[] = $qry->getError();	
						}
					}
			} else if (buttonClicked("delete")) {
				#first we need to check if this file is attached to anything
				$rsCheckTask = getRS($con,"select * from task_files where fileid = $fileid");
				$rsCheckContent = getRS($con,"select * from content_files where fileid = $fileid");
				
				if (getRow($rsCheckTask)) {
					$messages[] = FILE_ATTACHED_TO_TASK;	
				} else if (getRow($rsCheckContent)) {
					$messages[] = FILE_ATTACHED_TO_PAGE;	
				} else {

					$qry = new dbDelete("files");
					$qry->setParam("fileid",$fileid);
					$qry->setParam("accountid",$accountid);
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					}
					$fileid = 0;
					if(deleteFile($_POST['uploaded_filename'], $to_file_path)) {
						$messages[] = DELETED;
						if ($filetype == "adminDoc" && isAllowed("uploadHandsamDocs")) {
							headerLocation("docList.php?filetype=$filetype&accountid=$accountid".$contentIdLink,$messages);
						} else {
							headerLocation("docList.php?filetype=$filetype".$contentIdLink,$messages);
						}
					} else {
						$messages[] = ERROR_FILE_DELETE;
					}
				}
			} 
			
			
			
		} else {
			#nothing here
		}
	}
} else {
	trigger_error("Access Denied",E_USER_WARNING);
}

//include layout.
include ("layout_FB.php");

?>