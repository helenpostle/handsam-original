<?php 
/**
 * show licence history for APPS OTHER THAN TASKS APP
 */
 
//display a new box for each app

//first get apps for this account
//get modules for this installation

$appList = implode("','",$installed_modules);

$qry = "select distinct accountlicence.appid, apps.appname, apps.folder from apps left join accountlicence on apps.appid = accountlicence.appid where accountid = $accountid and accountlicence.appid != 1  and apps.folder in ('$appList')";
$appRs = getRS($con, $qry);

While ($appRow = getRow($appRs)) {
	//we may want to link to a specific module licenec page instead of the core licencepage.
	//so test if the module licence page exists
	$link_page = "$rootUrl/handsam_core/licence.php";
	if (file_exists("$rootPath/modules/".$appRow['folder']."/".$appRow['folder']."_licence.php")) $link_page = "$rootUrl/modules/".$appRow['folder']."/".$appRow['folder']."_licence.php";
	
	boxTop( $appRow['appname']." Licences");
	$qry = "SELECT accountlicence.* FROM accountlicence where accountid = ".$accountid." and accountlicence.state != 'DELETED' and appid = ".$appRow['appid']." order by  end_date asc";
	$result = getRS($con, $qry);
	$thisTasklist = 0;
	
	$renewLicence = false;
	//echo "<p class=\"info\">Select the <strong>CURRENT</strong> licence to view the tasks that go with that licence. ";
	if (isAllowed("editAccount")) {
		echo "Select a <strong>NEW</strong> licence to edit the licence dates";
	}
	echo "</p>";
	?>
	<table class="boxList">
		<caption class="hidden"><?php echo $title;?> licence History</caption>
		<thead>
			<tr>
				<th class="licenceid nolink">ID</th>
				<th class="state nolink">State</th>
				<th class="licence nolink">Start</th>
				<th class="licence nolink">End</th>
			</tr>
		</thead>
		<tbody>
			<?php
			while ($row = getRow($result)) {
				#only if the licence state==new can we still edit the licence
				#'edit licence' page also has 'edit  holiday periods' link
				if ($row['state'] == "NEW" && isAllowed("editAccount")) {
					$licenceLink = "$link_page?licenceid={$row["licenceid"]}&accountid={$accountid}&appid={$appRow['appid']}";
					?>
					<tr>
						<td class="licenceid"><a class="arrow" href="<?php echo $licenceLink;?>"><?php echo $row["licenceid"]; ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink;?>"><?php echo $row['state']; ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink;?>"><?php echo displayDate($row['start_date']); ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink;?>"><?php echo displayDate($row['end_date']); ?></a></td>
					</tr>
					<?php 
				} elseif($row['state'] == "ACTIVE"){
					#show link to tasks for this licence
					$licenceLink = "$link_page?licenceid={$row["licenceid"]}&accountid={$accountid}&appid={$appRow['appid']}";
					if (strtotime($row['start_date']) < time() && strtotime($row['end_date']) > time()) {
						$this_state = "CURRENT";
					} else if(strtotime($row['start_date']) > time())  {
						$this_state = "NEW";
					} else {
						$this_state = "ARCHIVED";
					}
					?>
					<tr>
						<td class="licenceid"><a class="arrow" href="<?php echo $licenceLink; ?>"><?php echo $row["licenceid"]; ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo $this_state; ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo displayDate($row['start_date']); ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo displayDate($row['end_date']); ?></a></td>
					</tr>
					<?php 
				} /*elseif($row['state'] == "NEW"){
					#show link to tasks for this licence
					$licenceLink = "tasks.php?licenceid=".$row["licenceid"];
					?>
					<tr>
						<td class="licenceid"><a class="arrow" href="<?php echo $licenceLink; ?>"><?php echo $row["licenceid"]; ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo $row['state']; ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo displayDate($row['start_date']); ?></a></td>
						<td class="licence"><a href="<?php echo $licenceLink; ?>"><?php echo displayDate($row['end_date']); ?></a></td>
					</tr>
				<?php 
				}*/
				#these variables store the latest licence details for use below
				$end_date = $row['end_date'];
				$currentState = $row['state'];
				$lastlicence = $row["licenceid"];
				if ($currentState != 'NEW') $renewLicence = true;
			}
			
			?> 
		</tbody>
		<tfoot>
		<?php
		if (isAllowed("editAccount")) {
			if ($renewLicence) {
				if (strtotime($end_date) > strtotime(strftime("%d/%m/%Y",time()))) {
					echo "<tr><td colspan=\"4\"><a href=\"licence.php?accountid=".$accountid."&licenceid=0&appid=".$appRow['appid']."\" title=\"Renew licence\">Renew licence</a></td></tr>";
				} else {
					echo "<tr><td colspan=\"4\"><a class=\"page\" href=\"licence.php?accountid=".$accountid."&licenceid=0&appid=".$appRow['appid']."\" title=\"Create licence\">Renew licence</a></td></tr>";
				}
			} else  {
				#show create licence button
				//echo "<tr><td colspan=\"4\"><a class=\"page\" href=\"licence.php?accountid=".$accountid."&licenceid=0\" title=\"Create licence\">Create licence</a></td></tr>";
			}
		}
	
			?>
		</tfoot>
	</table>
	<?php
		boxBottom();
}
?>
	