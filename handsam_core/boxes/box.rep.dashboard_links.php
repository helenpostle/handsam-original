<?php 
/**
 * lists holiday periods for selected licence
 * click on holiday period to edit it, or create a new one
 */
if (isset($accountid) && $accountid > 0) {
    boxTop("Further Links");
    echo "<ul class=\"side_nav\">";

    //get all licences within date period:
    $lic_qry = "select licenceid, tasklistname, accountname from accountlicence right join tasklist on accountlicence.tasklistid = tasklist.tasklistid left join account on  accountlicence.accountid = account.accountid where (date(start_date) < '".formatDatabaseInputDate($params['enddate'])."' and date(end_date) > '".formatDatabaseInputDate($params['startdate'])."') and accountlicence.accountid = $accountid and accountlicence.state='ACTIVE' and appid=1";
    
    $lic_rs = getRS($con, $lic_qry);
    while ($lic_row = getRow($lic_rs)) {
        echo "<li><a target=\"_blank\" href=\"$rootUrl/modules/tasklist/tasks.php?accountid=$accountid&amp;licenceid=".$lic_row['licenceid']."&amp;group_id=".$report_id."\">View ".$lic_row['accountname']." - ".$lic_row['tasklistname']." tasklist</a></li>";
    }

    echo "</ul>";
    boxBottom();
}
?>
	