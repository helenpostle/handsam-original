<?php 
/**
 * reset user password
 */
	boxTop("Reset this user's password");
	if (isAllowed("editUsers")) {
		
		echo "<p class=\"info\">By clicking the button below you will reset this user's password to a randomly generated value. They will then be sent an email containing their new password. Once they login they can change this to a more memorable value.</p>";
		echo startFormTable($_SERVER["PHP_SELF"]);
		
		
		//echo $qry;
		echo frmButtonConfirm("Reset Password","reset_pass","Are you sure you want to reset the password for this user?");
		echo frmHiddenField($userid,"userid2");
		echo frmHiddenField($aid,"accountid2");
	
		echo endFormTable();
	
	}
	boxBottom();
?>	