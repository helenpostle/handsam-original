<?php 
/**
 * lists holiday periods for selected licence
 * click on holiday period to edit it, or create a new one
 */
boxTop("Report Parameters");
echo startSimpleForm('', 'clearfix');
echo "<p class=\"info\">Change the parameters for these reports by selecting different values from the options below. </p>";

//get RS for school type select filter below
if (isset($params["school_pupils"]) && $params["school_pupils"] != 'ALL') {
    $s_type_qry = "where school_pupils = '{$params["school_pupils"]}'";
} else {
    $s_type_qry = "";
}
$qry1 = "select distinct school_type, CASE when school_type = 'UNKNOWN' THEN 'Other' ELSE school_type END as st from account $s_type_qry order by st asc";
$rs1 = getRs($con, $qry1);


//get RS for school pupils select filter below
$qry2 = "select distinct school_pupils, CASE when school_pupils = 'UNKNOWN' THEN 'Other' ELSE school_pupils END as sp from account order by sp asc";
$rs2 = getRs($con, $qry2);


//get RS for school cluster select filter below
//$qry3 = "select distinct school_cluster from account";
//get RS for school cluster select filter below
$cluster_filter_qry="";
if(isset($clsUS->account_clusters) && count($clsUS->account_clusters)>0 && isset($core_conf['account_cluster_SA_access']) && $core_conf['account_cluster_SA_access'])
{
  $cluster_filter_qry=" and c.id in (".implode(',',$clsUS->account_clusters).") ";
}
$qry3 = "SELECT c.id AS a, c.title AS b FROM account_cluster c  WHERE c.state != 'DELETED' AND c.state != 'INACTIVE' $cluster_filter_qry  ORDER BY c.title ASC";
$rs3 = getRs($con, $qry3);

foreach ($params as $key=>$val) {
	if ($key == "startdate") {
		echo frmDateFieldNonDb(strtotime($params["startdate"]),"startdate","Start Date",false);
	} else if ($key == "enddate") {
		echo frmDateFieldNonDb(strtotime($params["enddate"]),"enddate","End Date",false);
	} else if ($key == "school_type") {

		
		//select filter by school type
		echo "<div class=\"frm_row optional clearfix\">";
		echo "<label for=\"school_type\">School Type: </label>";
		echo "<select id=\"school_type\" name=\"school_type\">";
		echo "<option value=\"ALL\"";
		//if ($sch_type_id == 0) echo " selected ";
		if ($params["school_type"] == 0) echo " selected ";
		echo ">All Schools Types</option>";
        /*
		Foreach ($school_type as $key=>$val) {
			echo "<option value=\"$key\"";
			if ($dashboard->arrayParams["school_type"] == $key) echo " selected ";
			echo ">$val</option>";
		}
         */        
        while ($row1 = getRow($rs1)) {
            if(isset($school_type[$row1['school_type']])) {
                echo "<option value=\"{$row1['school_type']}\"";
                if ($params["school_type"] == $row1['school_type']) echo " selected ";
                echo ">{$school_type[$row1['school_type']]}</option>";
            }
        }
  
		echo "</select>";
		echo "</div>";
	} else if ($key == "school_pupils") {

		
		//select filter by school pupils or phase (cardiff)
		echo "<div class=\"frm_row optional clearfix\">";
		echo "<label for=\"school_pupils\">".ACCOUNT_TYPE_3.": </label>";
		echo "<select id=\"school_pupils\" name=\"school_pupils\">";
		echo "<option value=\"ALL\"";
		//if ($sch_type_id == 0) echo " selected ";
		if ($params["school_pupils"] == 0) echo " selected ";
		echo ">All ".$Inflector->pluralize(ACCOUNT_TYPE_3)."</option>";
        /*
		Foreach ($school_pupils as $key=>$val) {
			echo "<option value=\"$key\"";
			if ($dashboard->arrayParams["school_pupils"] == $key) echo " selected ";
			echo ">$val</option>";
		}
        */
        while ($row2 = getRow($rs2)) {
            if(isset($school_pupils[$row2['school_pupils']])) {
                echo "<option value=\"{$row2['school_pupils']}\"";
                if ($params["school_pupils"] == $row2['school_pupils']) echo " selected ";
                echo ">{$school_pupils[$row2['school_pupils']]}</option>";
            }
        }
		echo "</select>";
		echo "</div>";
        
        
    } else if ($key == "school_cluster"  && isset($core_conf['school_cluster']) && $core_conf['school_cluster']) {

		/*
		//select filter by school pupils or phase (cardiff)
		echo "<div class=\"frm_row optional clearfix\">";
		echo "<label for=\"school_cluster\">".ACCOUNT_TYPE_4.": </label>";
		echo "<select id=\"school_cluster\" name=\"school_cluster\">";
		echo "<option value=\"ALL\"";
		//if ($sch_type_id == 0) echo " selected ";
		if ($params["school_cluster"] == 0) echo " selected ";
		echo ">All ".$Inflector->pluralize(ACCOUNT_TYPE_4)."</option>";
  
        while ($row3 = getRow($rs3)) {
            if(isset($school_cluster[$row3['school_cluster']])) {
                echo "<option value=\"{$row3['school_cluster']}\"";
                if ($params["school_cluster"] == $row3['school_cluster']) echo " selected ";
                echo ">{$school_cluster[$row3['school_cluster']]}</option>";
            }
        }
		echo "</select>";
		echo "</div>";
    */
    echo "<div class=\"frm_row optional clearfix\">";
		echo "<label for=\"school_cluster\">".ACCOUNT_TYPE_4.": </label>";
		echo "<select id=\"school_cluster\" name=\"school_cluster\">";
		echo "<option value=\"ALL\"";
		//if ($sch_type_id == 0) echo " selected ";
		if ($params["school_cluster"] == 0) echo " selected ";
		echo ">All ".$Inflector->pluralize(ACCOUNT_TYPE_4)."</option>";
  
        while ($row3 = getRow($rs3)) {
            //if(isset($school_cluster[$row3['school_cluster']])) {
                echo "<option value=\"{$row3['a']}\"";
                if ($params["school_cluster"] == $row3['a']) echo " selected ";
                echo ">{$row3['b']}</option>";
            //}
        }
		echo "</select>";
		echo "</div>";
    
	} else if($key == "account_manager"  && $report_id < 2) {

		
		//select filter by account manager
		$qry = "SELECT count(accountid) as cnt FROM  account where account_manager={$clsUS->userid}";;
		$rsManager = getRS($con, $qry);
		$rowManager = getRow($rsManager);
		if ($rowManager['cnt'] > 0) {
			echo "<div class=\"filter\">";
			echo "<label for=\"account_manager\">View my accounts: </label>";
			if ($params["account_manager"] == $clsUS->userid) {
				echo "<input class=\"checkbox\" type=\"checkbox\" checked=\"checked\" value=\"{$clsUS->userid}\" name=\"account_manager\" id=\"account_manager\"/>";
			} else {
				echo "<input class=\"checkbox\"  type=\"checkbox\" value=\"{$clsUS->userid}\" name=\"account_manager\" id=\"account_manager\"/>";
			}
			echo "</div>";
		}


		
		
	} else {	
		echo frmHiddenField($val, $key);
	}
}
if ($report_id > 0 || $report_id === 0) echo frmHiddenField($report_id, "report_id");
if (isset($accountid) && $accountid > 0) echo frmHiddenField($accountid, "accountid");

echo frmButton("run report","run");
echo endFormTable2();

if ($report_id > 0 || $report_id === 0) {
    echo "<div class=\"side_nav\"><a href=\"$rootUrl/handsam_core/dashboard.php?params=".urlencode(serialize($params))."\">Return to overview</a></div>";
}
boxBottom();
?>
	