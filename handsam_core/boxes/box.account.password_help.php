<?php 
/**
 * reset user password
 */
	boxTop("Password Criteria");
	//if (isAllowed("editAccount")) {
		
		echo "<p class=\"info\">If you decide to change your password, it must meet the following criteria:<ul class=\"list\"><li>Be between 8 and 16 characters long</li><li>Contain at least 5 letters</li><li>1 of these letters must be uppercase</li><li>1 of these letters must be lowercase</li><li>Contain at least 1 number</li></ul></p>";
		
	
	//}
	boxBottom();
?>	