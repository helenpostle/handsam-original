<?php 
/**
 * add  app to the account
 */
	boxTop("Reset ALL Users' Passwords");
	if (isAllowed("editAccount")) {
		
		echo "<p class=\"info\">By clicking the button below you will reset all of this accounts users' passwords to  randomly generated values. They will then be sent an email containing their new password. Once they login they can change this to a more memorable value.</p>";
		echo startFormTable($_SERVER["PHP_SELF"]);
		
		
		//echo $qry;
		echo frmButtonConfirm("Reset Password","reset_pass","Are you sure you want to reset ALL the passwords for this account?");
		echo frmHiddenField($accountid,"accountid2");
	
		echo endFormTable();
	
	}
	boxBottom();
?>	