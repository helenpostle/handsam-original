<?php 
/**
 * lists holiday periods for selected licence
 * click on holiday period to edit it, or create a new one
 */
	boxTop("Holiday Periods");
	$qry = "SELECT * FROM licenceholiday where licenceid = ".$licenceid." order by holidaystart asc";
	$result = getRS($con, $qry);

?>
<p class="info">Add date periods when you don't want tasks to be assigned <br/> (for example: holiday times)  </p>
<table class="boxList">
	<caption class="hidden"><?php echo $title;?> licence Holiday Periods</caption>
	<thead>
		<tr>
			<th class="licence nolink">Holiday</th>
			<th class="licence nolink">Start</th>
			<th class="licence nolink">End</th>
		</tr>
	</thead>
	<tbody>
		<?php
		while ($row = getRow($result)) {
			?>
			<tr>
				<td class="licence"><a href="editlicenceholiday.php?holidayid=<?php echo $row["holidayid"]; ?>&licenceid=<?php echo $licenceid;?>&amp;tasklistid=<?php echo $tasklistid;?>"><?php echo $row['title']; ?></a></td>
				<td class="licence"><a href="editlicenceholiday.php?holidayid=<?php echo $row["holidayid"]; ?>&licenceid=<?php echo $licenceid;?>&amp;tasklistid=<?php echo $tasklistid;?>"><?php echo displayDate($row['holidaystart']); ?></a></td>
				<td class="licence"><a href="editlicenceholiday.php?holidayid=<?php echo $row["holidayid"]; ?>&licenceid=<?php echo $licenceid;?>&amp;tasklistid=<?php echo $tasklistid;?>"><?php echo displayDate($row['holidayend']); ?></a></td>
			</tr>
			<?php 

		}
		?> 
	</tbody>
	<tfoot>
	<?php
	echo "<tr><td colspan=\"3\"><a href=\"editlicenceholiday.php?licenceid=".$licenceid."&holidayid=0&tasklistid=$tasklistid\" title=\"Add new holiday period\">Add new holiday period</a></td></tr>";

		?>
	</tfoot>
</table>
<?php
	boxBottom();
?>
	