<?php 
/**
 * add tasklist to the selected account
 */
boxTop("Add Task List Licence");
if (isAllowed("editAccount")) {
	$qry = "select tasklist.tasklistid as a, tasklist.tasklistname as b from tasklist  where tasklist.tasklistid <> ALL(select tasklistid from accountlicence where accountid = $accountid and tasklistid is not null) ";
	echo "<p class=\"info\">Add a new tasklist to this account</p>";
	echo startFormTable($_SERVER["PHP_SELF"]);
	echo frmLegend('');
	echo frmSelectQueryNonDb("tasklistid", $qry,$con,"Task list",true);
	//echo $qry;
	echo frmButtonHelp("Add Tasklist","add_tasklist", "Click here to add tasklist");
	echo frmHiddenField($accountid,"accountid");

	echo endFormTable();

}
boxBottom();
?>
	