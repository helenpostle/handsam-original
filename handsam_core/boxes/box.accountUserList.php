<?php 
/**
 * list of users in this account.
 */
	boxTop("Account Users");
	//$qry = "SELECT usr.late_task_email_alert, usr.userid, username, usr_usrtype.usertype, state FROM usr left join usr_usrtype on usr.userid = usr_usrtype.userid where accountid = ".$accountid."  and (usr.state = 'ACTIVE' or usr.state = 'INACTIVE' or usr.state = 'DELETED') and usr.usertype != 'HandsamSuperAdmin' order by usr.userid,usr_usrtype.usertype asc";
	$qry = "SELECT usr.late_task_email_alert, usr.userid, username, state FROM usr where accountid = ".$accountid."  and (usr.state = 'ACTIVE' or usr.state = 'INACTIVE' or usr.state = 'DELETED') and (usr.usertype != 'HandsamSuperAdmin' or usr.usertype is null) order by usr.firstname, usr.lastname asc";
	$result = getRS($con, $qry);

?>
<table id="user_list" class="boxList">
	<caption class="hidden">Account Users</caption>
	<thead>
		<tr>
	    	<th class="username">&nbsp;Username</th>
			<th class="state">State</th>
			<th class="state">Email</th>
		</tr>
	</thead>
	<tbody>
	<tr>
			<td colspan="3"><a href="users.php?accountid=<?php echo $accountid;?>&userid=0">Add New User</a></td>
		</tr>

<?php
$i = 0;
$last_usr = array();
while ($row = getRow($result)) {
	if (!in_array($row['userid'],$last_usr)) {
	$last_usr[] = $row['userid'];
	
	$i++;
	#click on user to edit his/her details
	#add a new user link too
	if ($row["late_task_email_alert"] == 1) {
		$checked = " checked ";
	} else {
		$checked = "";
	}
	?>
		<tr>
			<td class="username"><a class="arrow" title="<?php echo $row["username"];?>" href="users.php?userid=<?php echo $row["userid"]; ?>&accountid=<?php echo $accountid;?>"><?php echo textSummary($row["username"],10); ?></a></td>
			<td class="state"><a title="<?php echo $row["username"];?>" href="users.php?userid=<?php echo $row["userid"]; ?>&accountid=<?php echo $accountid;?>"><?php echo displayText($row["state"]) ?></a></td>
			<td class="state"><input class="table_check_box" type="checkbox" value="<?php echo $row["userid"]; ?>" name="email_alert[]" <?php echo $checked; ?>/></td>
		</tr>
	<?php 
	}
} ?>
	</tbody>
	<tfoot>
		<tr>
			<td colspan="3"><a href="users.php?accountid=<?php echo $accountid;?>&userid=0">Add New User</a></td>
		</tr>
	</tfoot>
</table>



<?php
	boxBottom();
?>
	