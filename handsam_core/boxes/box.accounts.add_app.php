<?php 
/**
 * add  app to the account
 */
	boxTop("Add an Application Licence");
	if (isAllowed("editAccount")) {
		
		echo "<p class=\"info\">Add a new application to this account</p>";
		echo startFormTable($_SERVER["PHP_SELF"]);
		echo frmLegend('');
		echo frmSelectQueryNonDb("appid", $qry_apps,$con,"Application",true);
		//echo $qry;
		echo frmButtonHelp("Add Application","add_app", "Click here to add app");
		echo frmHiddenField($accountid,"accountid");
	
		echo endFormTable();
	
	}
	boxBottom();
?>	