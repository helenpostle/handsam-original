<?php 
/**
 * lists holiday periods for selected licence
 * click on holiday period to edit it, or create a new one
 */
boxTop("Report Parameters");
echo startSimpleForm('', 'clearfix');
echo "<p class=\"info\">Change the parameters for these reports by selecting different values from the options below. </p>";



foreach ($params as $key=>$val) {
	if ($key == "startdate") {
		echo frmDateFieldNonDb(strtotime($params["startdate"]),"startdate","Start Date",false);
	} else if ($key == "enddate") {
		echo frmDateFieldNonDb(strtotime($params["enddate"]),"enddate","End Date",false);
	}
}
if ($report_id > 0 || $report_id === 0) echo frmHiddenField($report_id, "report_id");
if (isset($accountid) && $accountid > 0) echo frmHiddenField($accountid, "accountid");

echo frmButton("run report","run");
echo endFormTable2();

if ($report_id > 0 || $report_id === 0) {
    echo "<div class=\"side_nav\"><a href=\"$rootUrl/handsam_core/dashboard.php?params=".urlencode(serialize($params))."\">Return to overview</a></div>";
}
boxBottom();
?>
	