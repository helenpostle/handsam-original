<?php 
/**
 * lists holiday periods for selected licence
 * click on holiday period to edit it, or create a new one
 */
$qry = "SELECT tl.tasklistname, l.*, a.appname, DATE_ADD(l.billing_date, INTERVAL 1 YEAR) as next_billing_date, CASE when NOW() > DATE_SUB(DATE_ADD(l.billing_date, INTERVAL 1 YEAR), INTERVAL 30 DAY) then 'warn' ELSE '' END as warning FROM accountlicence l left join apps a on l.appid = a.appid left join tasklist tl on l.tasklistid = tl.tasklistid where l.accountid = ".$accountid." and l.state = 'ACTIVE' and NOW() < DATE_ADD(DATE_ADD(l.billing_date, INTERVAL 10 DAY), INTERVAL 1 YEAR) order by l.billing_date asc";
$result = getRS($con, $qry);

if (getResultRows($result) > 0) {
	boxTop(ACCOUNTBILLING_DATE);

	?>
	<p class="info">Here are the next billing dates for this account</p>
	<table class="boxList">
		<caption class="hidden"><?php echo $title;?> licence Billing Dates</caption>
		<thead>
			<tr>
				<th class="licence ">Licence</th>
				<th class="licence ">Module</th>
				<th class="licence ">Next Billing</th>
			</tr>
		</thead>
		<tbody>
			<?php
			while ($row = getRow($result)) {
				if ($row['billing_date'] != '') {
					if ($row['appid'] == 1) {
						$edit_licence_link = "licence.php?licenceid={$row["licenceid"]}&amp;accountid={$row["accountid"]}&amp;tasklistid={$row["tasklistid"]}";
					} else if($row['appid'] == 5) {
						$edit_licence_link = "$rootUrl/modules/qgp/qgp_licence.php?licenceid={$row["licenceid"]}&amp;accountid={$row["accountid"]}";
					} else {
						$edit_licence_link = "licence.php?licenceid={$row["licenceid"]}&amp;accountid={$row["accountid"]}";
					}
					?>
					<tr class="<?php echo $row['warning']; ?>">
						<td class="licence "><a class="arrow" href="<?php echo $edit_licence_link;?>"><?php echo $row['licenceid']; ?></a></td>
						<td class="licence "><a href="<?php echo $edit_licence_link;?>">
						<?php 
						if ($row['appid'] == 1) {
							echo displayText($row['tasklistname']); 
						} else {
							echo displayText($row['appname']); 
						}
						?>
						</a></td>
						<td class="licence "><a href="<?php echo $edit_licence_link;?>"><?php echo displayDate($row['next_billing_date']); ?></a></td>
					</tr>
					<?php
				}
	
			}
			?> 
		</tbody>
		<tfoot>
		<?php
		echo "<tr><td colspan=\"3\"></td></tr>";
	
			?>
		</tfoot>
	</table>
	<?php
	boxBottom();
}
?>