<?php
ini_set('max_execution_time', '0'); // 0 = no limit.

$this_page = "dashboard.php";
/**
* change password form 
* content: password.php
* boxes: none
*/
####make this a secure page
$secure = true;

require("../shared/startPage.php");

$pageFiles->addFunc('dashboard');

$pageFiles->includePhp();

//adds plurals to strings
$Inflector = new Inflector();

$report_id = getIntFromRequest('report_id');
$accountid = getStrFromRequest('accountid');
$tabs = array();

if (isset($_POST['params'])) {
	$params = __unserialize($_POST['params']);
	$qsfrm = $_POST['params'];
	if (is_array($params)) {
		foreach ($params as $p_k => $p_v) {
			if (!isset($_POST[$p_k])) $_POST[$p_k] = $p_v;
		}
	}
} else if(isset($_GET['params'])) {
	$qsfrm = $_GET['params'];
	$params = __unserialize(stripslashes($_GET['params']));
	if (is_array($params)) {
		foreach ($params as $p_k => $p_v) {
			if (!isset($_GET[$p_k])) $_GET[$p_k] = $p_v;
		}
	}
} else {
	$params = array();
	$qsfrm = "";
    $params["startdate"] = getParam("startdate",getStartLastWeek());
    $params["enddate"] = getParam("enddate",getEndLastWeek());
    $params["school_type"] = getParam("school_type",'ALL');
    $params["school_pupils"] = getParam("school_pupils",'ALL');
    $params["school_cluster"] = getParam("school_cluster",'ALL');
}

if (is_string($accountid)) {
    //$params["school_type"] = $accountid;
}


if (isAllowed("viewDashboard")) {
	$title = "Dashboard";
	include("$rootPath/installations/$installation/reports/tasklist_detail.php");
} else if(isAllowed("viewAccountDashboard")) {
    $accountid = $clsUS->accountid;
    $title = "Dashboard";
	include("$rootPath/installations/$installation/reports/ac_tasklist_detail.php");
}
//include layout.
include ("layout.php");

?>
