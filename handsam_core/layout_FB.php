<?php 
/**
* Layout Page for file browser
* 
* This is a complete HTML Page which sets out the page structure
* Other files are included at various points to set the actual content
*/
#if $this_page not set in  main page code set as empty - used for active menu highlight
if (!isset($this_page)) {
	$this_page = "";
}
#sets content div to normal or wide depending on page 
if (!isset($contentId)) {
	$contentId = "content";
}
#include menu function
include("$rootPath/handsam_core/library/funcDisplay/funcMenu.php");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>

	<?php include("installations/$installation/layout/fb_head.php"); ?>

	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1">
	<link rel="stylesheet" href="<?php echo $rootUrl;?>/css/filebrowser.css?v=<?php echo $css_version;?>" type="text/css" />
	<link rel="stylesheet" href="<?php echo $rootUrl;?>/css/tables.content.css?v=<?php echo $css_version;?>" type="text/css" />
	<?php
	//include css files
	if (isset($pageFiles)) $pageFiles->includeCss();
	?>
	<script type="text/javascript">
		//set site root from php variable in shared/config.php
		var siteRoot = '<?php echo $rootUrl;?>';
	</script>
	
	<?php
	if (isset($pageFiles)) $pageFiles->loadJsVar();
	if (isset($pageFiles)) $pageFiles->includeJs();
	?>		
	<?php include("js/javascript.php"); ?>


</head>
<?php if ($debug) { ?>
<span class="debug">[Database: <?php echo $dbname ?>]<br/>
[Host: <?php echo $dbhost ?>]<br/></span>
<?php } ?>
<body>
<?php include("installations/$installation/layout/fb_branding.php"); ?>
<!--//
	<div id="branding">
    	<img id="handsamlogo" src="<?php echo $rootUrl;?>/images/handsam3.gif" alt="HandSaM - Health and Safety Management System" />
    </div>
//-->
<div id="wrapper" class="clearfix">
    
    <div id="navigation"> 	
    	<ul>
    		<?php
    		if (isLoggedIn()){
    	  		echo "<li><a class=\"first_menu menu\" onclick='window.close();'>close window</a></li>";
    	  		if ($filetype == "adminDoc" && isAllowed("uploadHandsamDocs")) {
    	  			echo menuItem($this_page."?filetype=".$filetype."&accountid=".$accountid, "docList.php?filetype=".$filetype."&accountid=".$accountid, "", "Document List");
    	  			echo menuItem($this_page."?filetype=".$filetype."&accountid=".$accountid, "browser.php?filetype=".$filetype."&accountid=".$accountid, "", "Upload Documents");
	  			} else {
    	  			echo menuItem($this_page."?filetype=".$filetype, "docList.php?filetype=".$filetype.$contentIdLink, "", "Document List");
    	  			echo menuItem($this_page."?filetype=".$filetype, "browser.php?filetype=".$filetype.$contentIdLink, "", "Upload Documents");
	  			}
    	  		if ($filetype == "handsamDoc" && isAllowed("uploadHandsamDocs")) {
	    	  		echo menuItem($this_page."?filetype=".$filetype, "linksList.php?filetype=".$filetype.$contentIdLink, "", "Links List");
	    	  		echo menuItem($this_page."?filetype=".$filetype, "editLinks.php?filetype=".$filetype.$contentIdLink, "", "Add Link");
    	  		}
	  		}
			?>
	       	<li class="last_menu"><a class="last_menu_off"></a></li>
		</ul>
	</div>
	<div id="content_wrapper" class="clearfix">
    	<div id="<?php echo $contentId;?>">
         <?php include ("content.php"); ?>
   		</div>
	</div>   
</div>
<div id="footer">
<?php include("installations/$installation/layout/fb_footer.php"); ?>
</div>
<?php
require_once("$rootPath/handsam_core/library/security/csrf.php");
?>

</body>
</html>