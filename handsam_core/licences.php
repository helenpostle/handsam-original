<?php
/**
* LICENCES LIST  editing functionality
* 
* content: licences_edit, licences_list.php
* boxes:  
* 
*/
$this_page = "licences.php";
/**
 * Include basic stuff
 */
$secure = true;

require("../shared/startPage.php");

$_SESSION['return_page'] = "licences.php";
####make this a secure page
$pageFiles->addFunc("jquery_ui");
$pageFiles->addFunc('licence');
$pageFiles->addFunc("licencelist_table");

$pageFiles->includePhp();

#check if have requested a specific account and get other params
$accountid = getIntFromRequest("accountid");
$licenceid = getIntFromRequest("licenceid");

$tasklist = getIntFromRequest("tasklist");
$appid = getIntFromRequest("appid",0);
$manfilter = getStrFromRequest("manfilter");

//select renewal date or billing date filter
$select_date_filter = getStrFromRequest("date_range");
if ($select_date_filter != "") {
	$_SESSION['select_date_filter'] = $select_date_filter;
} elseif (isset($_SESSION['select_date_filter'])) {
	$select_date_filter = $_SESSION['select_date_filter'];
} else {
	$select_date_filter = "end_date";
} 


//to date value
$to_date = getStrFromRequest("to_date");


if ($to_date == "reset") {
	$_SESSION['to_date'] = "";
	$to_date = "";
} elseif ($to_date != "") {
	$_SESSION['to_date'] = $to_date;
} elseif (isset($_SESSION['to_date'])) {
	$to_date = $_SESSION['to_date'];
}

$to_date_qry = "";
if ($to_date != "") {
	if ($select_date_filter == "end_date") {
		$to_date_qry = " and end_date < '$to_date'";
	} elseif ($select_date_filter == "billing_date") {
		$to_date_qry = " and DATE_ADD(accountlicence.billing_date,INTERVAL 1 YEAR) < '$to_date'";
	} 
}


//from date value

$from_date = getStrFromRequest("from_date");
if ($from_date == "reset") {
	$_SESSION['from_date'] = "";
	$from_date = "";
} elseif ($from_date != "") {
	$_SESSION['from_date'] = $from_date;
} else {
	if (isset($_SESSION['from_date'])) $from_date = $_SESSION['from_date'];
} 

$from_date_qry = "";
if ($from_date != "") {
	if ($select_date_filter == "end_date") {
		$from_date_qry = " and end_date > '$from_date'";
	} elseif ($select_date_filter == "billing_date") {
		$from_date_qry = " and DATE_ADD(accountlicence.billing_date,INTERVAL 1 YEAR) > '$from_date'";
	}
}

//add appid to session
if ($appid != "" || $appid === 0) {
	$_SESSION['appid'] = $appid;
} else {
	$appid = $_SESSION['appid'];
}
//add tasklist to session
if ($tasklist != "" || $tasklist === 0) {
	$_SESSION['tasklist'] = $tasklist;
} else {
	$tasklist = isset($_SESSION['tasklist']) ? $_SESSION['tasklist'] : ''; 
}



//manager filter processing
if ($manfilter != "") {
	$_SESSION['manager_filter'] = $manfilter;
} else {
	if (isset($_SESSION['manager_filter'])) {
		$manfilter = $_SESSION['manager_filter'];
	} else {
		$_SESSION['manager_filter'] = "";
	}
}

if ($manfilter == "Mine") {
	$account_manager_qry = " and account_manager={$clsUS->userid} ";
	$account_manager_title = ": My Accounts ";
} else {
	$account_manager_qry = "";
	$account_manager_title = ": All Accounts ";
}

//acc name filter
if (isset($_SESSION['acc_name'])) {
	$acc_name_filter_qry = " and accountname like '{$_SESSION['acc_name']}%' ";
	$acc_name_filter = $_SESSION['acc_name'];
} else {
	$acc_name_filter_qry = "";
	$acc_name_filter = "";
}


//licence state filter
$state_filter = getStrFromRequest("state");

if ($state_filter != "") {
	$_SESSION['licence_state_filter'] = $state_filter;
} elseif (isset($_SESSION['licence_state_filter'])) {
	$state_filter = $_SESSION['licence_state_filter'];
} else {
	$state_filter = "";
} 

//licence state filter
$state_filter_qry = "";
if ($state_filter == "ALL") {
	$state_filter_qry = "";
} else if ($state_filter != "") {
	$state_filter_qry = " and accountlicence.state = '$state_filter' ";
}

$new_filter_qry = $account_manager_qry.$acc_name_filter_qry;

$new_filter_title = $account_manager_title;
$title = "";

//now inclusde all php files here
$pageFiles->includePhp();

$cluster_qry="";
//$cluster_filter_qry="";
if(isset($clsUS->account_clusters) && count($clsUS->account_clusters)>0 && isset($core_conf['account_cluster_SA_access']) && $core_conf['account_cluster_SA_access'])
{
  $cluster_qry = " and account.cluster_id in (".implode(',',$clsUS->account_clusters).") ";
  //$cluster_filter_qry=" and c.id in (".implode(',',$clsUS->account_clusters).") ";
}

#we can do this if  we have permission to edit accounts.
if (isAllowed("editAccount")) {
		
	if (buttonClicked("cancel")) {
		headerLocation("licences.php", true);
	} else {
		if ($licenceid !== null) {
			$content = "licences_edit.php";
			$qry = "select accountlicence.*, apps.appname, apps.folder, tasklist.tasklistname from accountlicence left join tasklist on accountlicence.tasklistid = tasklist.tasklistid left join apps on accountlicence.appid = apps.appid left join account on accountlicence.accountid = account.accountid where accountlicence.licenceid = $licenceid $cluster_qry";
			//echo $qry;
			$rslicence = getRS($con, $qry);
			$rowlicence = getRow($rslicence);
      $title = "";
      if ($rowlicence)
      {
        //get account name, licenceid, module and tasklist for info
        $qry = "select accountname from account where accountid = {$rowlicence['accountid']}";
        $rs = getRS($con, $qry);
        $row = getRow($rs);
        $title = "Edit Licence: {$row['accountname']}";
      }
			#display edit form
			if (buttonClicked("save")) {
								
				$qry = new dbUpdate("accountlicence");
				$qry->setParam("licenceid",$licenceid);
				$qry->setReqStringVal("state",$_POST["state"],"state");
			
				if (isset($qry)) {
					#calculate end date from start date plus 1 year duration
					$END_YEAR = strftime("%Y",strtotime($_POST["start_date"])) + 1;
					$END_DATE = strftime("%a, %d %b %Y",(strtotime(strftime("%d %b ".$END_YEAR,strtotime($_POST["start_date"])))));
					$qry->setReqDateVal("start_date", $_POST["start_date"], "Start date");
					$qry->setDateVal("billing_date", $_POST["billing_date"], "Billing date");
					$qry->setNumberVal("contract_length",$_POST["contract_length"],"Billing period");
					//$qry->setReqNumberVal("accountid",$_POST["accountid"],"Account id");
					$qry->setReqDateVal("end_date", $END_DATE, "End date");
					
					//now process any module specific conf that is posted from the form $rootPath/modules/{$rowmod['folder']}/shared/licence_conf.php
					
					//get array of name/value pairs
					
					if (isset($_POST['mod_conf_name']) && count($_POST['mod_conf_name']) > 0) {
						$mod_conf_arr = array();
						for ($i=0; $i<count($_POST['mod_conf_name']); $i++) {
							$mod_conf_arr[$_POST['mod_conf_name'][$i]] = $_POST['mod_conf_val_'.$i];
						}
						
						$qry->setReqStringVal("conf",json_encode($mod_conf_arr),"conf");
					}
				
					$qry->setAudit($clsUS->userid);
					
					if ($qry->execute($con)) {
						$messages[] = SAVED;
						//now do app specific function if licence made active
						if (function_exists("licence_active_app_$appid") && $_POST["state"] == 'ACTIVE') {
							call_user_func_array("licence_active_app_$appid", array($con, $_POST["accountid"], $clsUS));
						}
						
						#return to accounts page
						headerLocation ("licences.php", $messages);
						
					} else {
						$messages[] = $qry->getSql();	
					}
				}
				
			} else if (buttonClicked("delete")) {
				#cannot delete self
					$qry = new dbUpdate("accountlicence");
					$qry->setParam("licenceid",$licenceid);
					$qry->setReqStringVal("state","DELETED","State");
					if (!$qry->execute($con)) {
						$messages[] = $qry->getError();
					} else {
						#return to accounts page
						$messages[] = DELETED;
						headerLocation ("licences.php", $messages);
					}
			} 


			
		} else {
			#no licenceid set - so view licences_list instead?
			$content = "licences_list.php";
			
			//bit naughty - this is some tasklist module functionality - so licences can be filtered by tasklist.			
			if (in_array("tasklist", $installed_modules) && $appid == "1") {
				if ($tasklist > 0) {
					$tasklist_qry = " and accountlicence.tasklistid = $tasklist ";
					$tasklist_title = "";
				} else {
					$tasklist_qry = "";
					$tasklist_title = "All Tasklists";
				}

				$qry = "SELECT account.accountid,account.accountname, accountlicence.state, accountlicence.licenceid, tasklist.tasklistname, DATE_ADD(accountlicence.billing_date,INTERVAL 1 YEAR) AS next_billing_date, accountlicence.end_date, accountlicence.contract_length, apps.appname";
				$qry .= " FROM account left join accountlicence on account.accountid = accountlicence.accountid left join tasklist on accountlicence.tasklistid = tasklist.tasklistid ";
				$qry .= " left join apps on apps.appid = accountlicence.appid ";
				$qry .= " where licenceid>0 and account.accountid != 1 and account.state != 'DELETED' and accountlicence.appid=1 $tasklist_qry $new_filter_qry $to_date_qry $from_date_qry $state_filter_qry order by billing_date asc, end_date asc";			
			} else {
				$qry = "SELECT account.accountid,account.accountname, accountlicence.state, accountlicence.licenceid, DATE_ADD(accountlicence.billing_date,INTERVAL 1 YEAR) AS next_billing_date, accountlicence.end_date, accountlicence.contract_length, apps.appname, tasklist.tasklistname ";
				$qry .= " FROM account left join accountlicence on account.accountid = accountlicence.accountid ";
				$qry .= " left join apps on apps.appid = accountlicence.appid ";
				$qry .= " left join tasklist on tasklist.tasklistid = accountlicence.tasklistid ";
				$qry .= " where licenceid>0 and account.accountid != 1 and account.state != 'DELETED' $new_filter_qry $to_date_qry $from_date_qry $state_filter_qry $cluster_qry";
				if ($appid > 0) $qry .= " and accountlicence.appid = $appid ";
				$qry .= " order by billing_date asc, end_date asc ";
			}
			
			//echo $qry;
			$result = getRS($con, $qry);
			
			if ($tasklist > 0) {
				$row = getRow($result);
				$tasklist_title = $row['tasklistname'];
				resetRS($result);
			}
			
			//$title = "Account list : $tasklist_title";
			$title = "Licence list ";
			$contentId = "contentWide";	
		}
	}
} else {
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);
}

#Include layout page
include("layout.php");
?>