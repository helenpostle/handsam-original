<?php
/**
* Account editing functionality
* 
* content: accountEdit.php, accountList.php
* boxes:  accountUserList.php, accountlicenceList.php
* 
*/

$this_page = "accounts.php";
/**
 * Include basic stuff
 */
$secure = true;

require("../shared/startPage.php");

####make this a secure page
$pageFiles->addFunc("jquery_ui");

$pageFiles->addFunc("account_comments");
$pageFiles->addFunc("accountlist_tables");
$pageFiles->addFunc('email');

$pageFiles->includePhp();

#check if have requested a specific account and get other params
$accountid = $clsUS->accountid;
$licenceid = getIntFromRequest("licenceid");
$supplyTask = getStrFromRequest("supplyTask");
$tasklist = getIntFromRequest("tasklist");
$title = "";

if ($accountid > 0) {
	$pageFiles->addJsVar('item_id', $accountid);
	$pageFiles->addJsVar('sessionid', session_id());
	$pageFiles->addFunc('email_alert_checkbox');
}

//now inclusde all php files here
//$pageFiles->includePhp();

#we can do this if  we have permission to edit accounts.
if (isAllowed("editAccount")) {
		
	if ($supplyTask == "yes") {
		//add tasks to account
		$tasks = "SELECT * FROM account left join accountlicence on account.accountid = accountlicence.accountid left join task on accountlicence.tasklistid = task.tasklistid where account.accountid = ".$accountid." order by task.taskmonth, task.taskday";
		$taskResult = getRS($con, $tasks);
		$execution_error = false;
		while ($rowTask = getRow($taskResult)) {
			$qry = new dbInsert("accounttask");
			$qry->setReqNumberVal("taskid",$rowTask["taskid"],"Task id");
			$qry->setReqNumberVal("accountid",$accountid,"Account id");
			$qry->setReqNumberVal("categoryid",$rowTask["categoryid"],"Category id");
			$qry->setReqNumberVal("licenceid",$licenceid,"licence id");
			$qry->setReqNumberVal("taskmonth",$rowTask["taskmonth"],"Task month");
			$qry->setReqNumberVal("taskday",$rowTask["taskday"],"Task day");
			$qry->setReqStringVal("tasktext",$rowTask["tasktext"],"Task text");		
			$qry->setAudit($clsUS->userid);
			if (!$qry->execute($con)) {
				$execution_error = true;
				$messages[] = ERROR_SAVED." to task id:".$rowTask["taskid"];
			}
		}
		if ($execution_error == false) {
			$messages[] = TASKS_SUPPLIED;
		}
		//update licence state to active
		$qry = new dbUpdate("accountlicence");
		$qry->setParam("licenceid",$licenceid);
		$qry->setReqStringVal("state","ACTIVE","state");
		if ($qry->execute($con)) {
			$messages[] = licence_ACTIVE;
		}
	}


	//get account  title for page title
	$qry = "SELECT accountname FROM account where accountid='$accountid'";
	$result = getRS($con, $qry);
	$row = getRow($result);
	$title = displayText($row['accountname']);
	
	if (buttonClicked("cancel")) {
		headerLocation("my_account.php", true);
	} else {
	
	
		if ($accountid !== null) {
			#display edit form
			$content = "accountedit.php";
			
			if (buttonClicked("save")) {
				#check that an account list doesn't exist with this name
				$rsCheck = getRS($con,"select * from account where accountname = '".escapeString($_POST["accountname"])."' and accountid != '$accountid'");
				if (getRow($rsCheck)) {
					$messages[] = ACCOUNTNAME_IN_USE;	
				} else {
								
					if ($accountid == 0) {
						$qry = new dbInsert("account");
					} else {
						$qry = new dbUpdate("account");
						$qry->setParam("accountid",$accountid);
					}
					
					// "post accountname is : ".$_POST["accountname"];
				
					if (isset($qry)) {
						
						//check that authority is in authority table:
						if (trim($_POST['authority']) != "") {
							$auth_qry = "select auth_name from authority where auth_name = '".escapeString($_POST['authority'])."'";
							$auth_rs = getRS($con, $auth_qry);
							
							if (getResultRows($auth_rs) == 0) {
								$qry->failError .= "Authority name not recognised. Please try again or leave blank";
							} else {
								$qry->setReqStringVal("authority",$_POST["authority"],"Authority");
							}
						}
						
						//new account contact field
						$qry->setStringVal("account_contact",$_POST["account_contact"],"Account Contact");
						
						//newe account manager field
						if ($_POST["account_manager"] > 0) {
							$qry->setNumberVal("account_manager",$_POST["account_manager"],"Account manager");
						}
						
						
						$qry->setReqStringVal("accountname",$_POST["accountname"],"Account name");

						if ($accountid !=1) {
							$qry->setReqStringVal("school_type",$_POST["school_type"],"School Type");
							$qry->setReqStringVal("school_status",$_POST["school_status"],"School Status");
							$qry->setReqStringVal("school_pupils",$_POST["school_pupils"],"School Pupils");
							
							$qry->setStringVal("school_dfe",$_POST["school_dfe"],"School Dfe Number");
						}
						
						$qry->setReqStringVal("address1",$_POST["address1"],"Address 1");
						$qry->setStringVal("address2",$_POST["address2"],"Address 2");
						$qry->setReqStringVal("town",$_POST["town"],"Town");
						$qry->setStringVal("county",$_POST["county"],"County");
						$qry->setReqStringVal("postcode",$_POST["postcode"],"Postcode");
						$qry->setReqStringVal("country",$_POST["country"],"Country");
						$qry->setReqStringVal("email",$_POST["email"],"Email");
						$qry->setReqStringVal("phone",$_POST["phone"],"phone");
						//$qry->setReqStringVal("tasklist",$_POST["tasklist"],"Task list");
						$qry->setReqStringVal("state",$_POST["state"],"state");
					
						$qry->setAudit($clsUS->userid);
						
						//echo "SQL: ".$qry->getSQL();
						
						if ($qry->execute($con)) {
							$messages[] = SAVED;
							$content = "accountedit.php";
						} else {
							$messages[] = $qry->getError();	
						}
					}
				}
			} else if (buttonClicked("add_tasklist") && $_POST["tasklistid"] > 0) {
				$start_date = strftime("%a, %d %b %Y",time());
				$end_date = strftime("%a, %d %b %Y",mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1));
				
				$n_qry = new dbInsert("accountlicence");
				$n_qry -> setReqNumberVal("accountid",$accountid,"Account id");
				$n_qry->setReqNumberVal("tasklistid",$_POST["tasklistid"],"Task list");	
				$n_qry->setReqStringVal("appid",1,"App ID");	
				$n_qry->setReqDateVal("end_date", $end_date, "End date");
				$n_qry->setReqDateVal("start_date", $start_date, "Start date");
				$n_qry->setAudit($clsUS->userid);
				if ($n_qry->execute($con)) {
					$messages[] = SAVED;
					$content = "accountedit.php";
					
				} else {
					$messages[] = $n_qry->getError();	
				}
			} else if (buttonClicked("add_app") && $_POST["appid"] > 0) {
				$start_date = strftime("%a, %d %b %Y",time());
				$end_date = strftime("%a, %d %b %Y",mktime(0, 0, 0, date("m"),   date("d"),   date("Y")+1));
				
				$n_qry = new dbInsert("accountlicence");
				$n_qry -> setReqNumberVal("accountid",$accountid,"Account id");
				$n_qry->setReqStringVal("appid",$_POST["appid"],"App ID");	
				$n_qry->setReqDateVal("end_date", $end_date, "End date");
				$n_qry->setReqDateVal("start_date", $start_date, "Start date");
				$n_qry->setAudit($clsUS->userid);
				if ($n_qry->execute($con)) {
					$messages[] = SAVED;
					$content = "accountedit.php";
					
				} else {
					$messages[] = $n_qry->getError();	
				}
			} else if(buttonClicked("reset_pass")) {
				/*
				$content = "accountedit.php";
				//now select all users for this account and set new passwords
				$userQry  = "select username from usr where accountid  = $accountid and state = 'ACTIVE'";
				$userRs = getRS($con, $userQry);
				While ($userRow = getRow($userRs)) {
					$passwd = new clsUserPassword($homePath, $password_arr);
					$messages[] = $passwd->emailPassword($userRow['username'], $con, $adminMail); 
				}
				*/
			} 

			
		}
			
			$pageFiles->box_priority = true;
			//$pageFiles->addBox("$rootPath/handsam_core/boxes/box.accounts.billing_date.php");
			
			$pageFiles->addBox("$rootPath/handsam_core/boxes/box.accountUserList.php");
			//$pageFiles->addBox("$rootPath/handsam_core/boxes/box.accounts.reset_all_password.php");

			//add app box - only display if there is an app thar hasnn't been selected yet
			$installed_appids = implode(',',array_flip($installed_modules));
			$qry_apps = "select appid as a, appname as b from apps  where appid != 1 and appid != 5 and appid not in (SELECT appid FROM accountlicence WHERE accountid = $accountid and appid > 0 and state != 'DELETED') and appid in($installed_appids) ";
			
			//echo $qry_apps;
			
			$rsApps = getRS($con,$qry_apps);
			$count_apps = getResultRows($rsApps);
			if ($count_apps > 0) {
				$pageFiles->addBox("$rootPath/handsam_core/boxes/box.accounts.add_app.php");
			}
			
			$reverse_modules = array_reverse($installed_modules);
			foreach ($reverse_modules as $mod) {
				if (file_exists("$rootPath/modules/$mod/shared/account_inc.php") && $mod == "tasklist") require_once("$rootPath/modules/$mod/shared/account_inc.php");	
			}
			
			$box[] = "box.accounts.acc_app_licence.php";


	}
} else {
	headerLocation("$rootUrl/index.php",false);
	//trigger_error("Access Denied",E_USER_WARNING);
}

#Include layout page
include("layout.php");
?>