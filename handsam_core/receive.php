<?php

/** 
 * Receives the request from Roger. 
 *
 *   
 */
 //log all errors as the text displayed on this page is logged back in roger
error_reporting(E_ALL);
ini_set("display_errors", 1); 
ini_set('memory_limit', '-1');
ini_set('max_execution_time', '0');
// default header is internal error
header("HTTP/1.1 500 Internal Server Error", true, 500);

//todo

//1. better logging of queries in array or json obj TICK
//2. tidy up all code so far in taskadmin, hooks, clsReceive TICK
//3. add multi select hooks to task admin
set_error_handler("recErrorHandler");
register_shutdown_function( "fatal_handler" );

//the only errors that won't get caught are parse errors.

$response_arr = array();

//custom fatal error handler for sync recieve
function fatal_handler() {
	global $response_arr;
	$error = error_get_last();
	if( $error !== NULL) {
		$errno   = $error["type"];
		$errfile = $error["file"];
		$errline = $error["line"];
		$errstr  = $error["message"];
		$response_arr['ERROR'][] = $error;
		header("HTTP/1.1 500 Internal Server Error", true, 500);
		echo json_encode($response_arr);
		var_dump($error);
	}
}


function recErrorHandler($error_level, $error_message, $error_file, $error_line, $error_context) {
	global $response_arr;
	$error = "lvl: " . $error_level . " | msg:" . $error_message . " | file:" . $error_file . " | ln:" . $error_line;
	$response_arr['ERROR'][] = $error;
	header("HTTP/1.1 500 Internal Server Error", true, 500);
}

function myArrFilter($string) {
  return stripos($string, 'EXECUTED:  ') === false;
}

require("../shared/startPage.php");
// handsam sync
$pageFiles->csrf_form = false;
$pageFiles->addFunc('receive');

//$receive_error = false;

$pageFiles->includePhp();

// get the data and log the call
$rec = new Receive();

// authentication
if($rec->auth() === false) {
    error_log('Authentication failed at handsam_core/library/receive.php');
    header('HTTP/1.0 401 Unauthorized');
    die('Not authenticated');
}
$response_arr['FAILED']= array();
$response_arr['ERROR'] = array();
if (array_key_exists('multi', $rec->_data)) {
    //this is sync object contains multiple items - such as a whole tasklist of tasks, or all files
    
    $id_arr = array(); // an array of primary key ids for each item 
    $sync_error_flag = false;
    foreach ($rec->_data['multi'] as $rec_data) {
        ob_start();
        $rec_item = new Receive(false, $rec_data);
        if (isset($rec_item->_data['objects']) && count($rec_item->_data['objects']) > 0) {
            $rec_item->processPreObjects($con, $rec_item->_data['objects']);
        }
        $rec_item->save($con);
        $rec_item->processPostObjects($con);
        //logging that's returned to sync server
        if (isset($arraySQL) && is_array($arraySQL)) {
            foreach($arraySQL as $key => $value) {
                if (strpos($value,"FAILED",0) === 0) {
                    $response_arr['FAILED'][$rec_item->_table][$rec_item->_primary_key_value][] = $value;
                }
            }
            $arraySQL = array();
        }
        $id_arr[] = $rec_item->_primary_key_value;
        $page_txt = ob_get_contents();
        if ($page_txt != '') $response_arr['ERROR'][$rec_item->_table][$rec_item->_primary_key_value][] = $page_txt;
        ob_end_clean();

        if (@ $errorFlag != "") {
            $response_arr['ERROR'][$rec_item->_table][$rec_item->_primary_key_value][] = $errorFlag;
            $errorFlag = "";
        }
        
        if ((isset($response_arr['FAILED'][$rec_item->_table][$rec_item->_primary_key_value]) && count($response_arr['FAILED'][$rec_item->_table][$rec_item->_primary_key_value])) > 0 || (isset($response_arr['ERROR'][$rec_item->_table][$rec_item->_primary_key_value]) && count($response_arr['ERROR'][$rec_item->_table][$rec_item->_primary_key_value]) > 0))   {
            $sync_error_flag = true;
        }
        //$new_arr = array_map("unserialize", array_unique(array_map("serialize", $response_arr[$rec_item->_table][$rec_item->_primary_key_value]['EXECUTED'])));
        //$response_arr[$rec_item->_table][$rec_item->_primary_key_value]['EXECUTED'] = array_filter($new_arr, 'myArrFilter');
    }
    $rec->afterMultiSave($id_arr, $con);
    if ($sync_error_flag == true)   {
        header("HTTP/1.1 500 Internal Server Error", true, 500);
    } else {
        header("HTTP/1.1 200 OK", true, 200);
    }
    
    echo json_encode($response_arr);
} else {
    ob_start();
    $response_arr['FAILED'] = array();
    //$response_arr['EXECUTED'] = array();
    $response_arr['ERROR'] = array();
    if (isset($rec->_data['objects']) && count($rec->_data['objects']) > 0) {
        $rec->processPreObjects($con, $rec->_data['objects']);
    }
    // delete or save
    if($rec->isDelete() === true) {
        $rec->delete($con);
    } else {
        $rec->save($con);
    }
    //for items that depend on the main item - such as task_files and task_links we will need the new taskid if it was an insert
    $rec->processPostObjects($con);
    
    //logging that's returned to sync server
    if (isset($arraySQL) && is_array($arraySQL)) {
        foreach($arraySQL as $key => $value) {
            if (strpos($value,"FAILED",0) === 0) {
                $response_arr['FAILED'][] = $value;
            } else {
                //$response_arr['EXECUTED'][] = $value;
            }
        }
    }

    $page_txt = ob_get_contents();
    if ($page_txt != '') $response_arr['ERROR'][] = $page_txt;
    ob_end_clean();

    if (@ $errorFlag != "") $response_arr['ERROR'][] = $errorFlag;

    if (count($response_arr['FAILED']) > 0 || count($response_arr['ERROR']) > 0)   {
        header("HTTP/1.1 500 Internal Server Error", true, 500);
    } else {
        header("HTTP/1.1 200 OK", true, 200);
    }
    //$new_arr = array_map("unserialize", array_unique(array_map("serialize", $response_arr['EXECUTED'])));
    //$response_arr['EXECUTED'] = array_filter($new_arr, 'myArrFilter');
    echo json_encode($response_arr);
    
}


?>