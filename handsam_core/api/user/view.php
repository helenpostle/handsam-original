<?php
/**
* training log link to external web app course
*
*/

####make this a secure page
$secure = false;
require("../../../shared/startPage.php");
$pageFiles->csrf_form = '';


$pageFiles->includePhp();

$hash = getStrFromRequest('trainee');
$mod = getStrFromRequest('mod');

//get usr details
//add a timescale clause to this query so that it only lasts for x mins
//$qry = "select e.con_id, u.firstname, u.lastname, a.accountname, a.school_status, a.accountlogo from ext_web_app_con e left join usr u on e.userid = u.userid left join account a on u.accountid = a.accountid  where  e.unique_str = '$hash' order by con_id desc;";
$qry = "select e.con_id, u.firstname, u.lastname, a.accountname, a.school_status, a.accountlogo from ext_web_app_con e left join usr u on e.userid = u.userid left join account a on u.accountid = a.accountid  where e.state = 'NEW' and e.unique_str = '$hash' and e.module='$mod' order by e.con_id desc limit 0,1";

$rs = getRS($con,$qry);
$row = getRow($rs);

if ($row) {
	
	if (isset($installation_logo_url) && is_array($installation_logo_url) && count($installation_logo_url) > 1) {
		if (array_key_exists($row['school_status'], $installation_logo_url)) {
			$mainlogo = $installation_logo_url[$row['school_status']];
		} else {
            $mainlogoarray = array_values($installation_logo_url);
			$mainlogo = array_shift($mainlogoarray);
		}
	} else if (isset($installation_logo_url) && is_array($installation_logo_url)) {
		$mainlogo = array_shift(array_values($installation_logo_url));
	} else if(isset($installation_logo_url)) {
		$mainlogo = $installation_logo_url;
	} else {
		$mainlogo = "";
	}
	

	//var_dump($installation_logo_url[0]);
	if ($row['accountlogo'] != "") {
		$accountlogo = "$rootUrl/account_uploads/account_logo/{$row['accountlogo']}";
	} else {
		$accountlogo = "";
	}
	
	//now update the record  state to CHECKED
		
	$qry = new dbUpdate('ext_web_app_con');
	$qry->setParam("con_id",$row['con_id']);
	if ($online_course_mode == "LIVE") $qry->setStringVal("state","CHECKED","State");
	$qry->addValue("checked","NOW()");
	//echo $qry->getSQL();
	if ($qry->execute($con)) {
		$json = array("firstname" => $row['firstname'], "lastname" => $row['lastname'], "account" => $row['accountname'], "mainlogo" => $mainlogo, "accountlogo" => $accountlogo, "installation" => $installation);
		echo json_encode($json);
	}


} else {
	//trigger_error("Access Denied",E_USER_WARNING);

	//headerLocation("$rootUrl/index.php",false);
}
?>