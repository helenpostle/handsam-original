<?php
/**
* training log link to external web app course
*
*/

####make this a secure page
$secure = false;
require("../../../shared/startPage.php");
$pageFiles->csrf_form = '';


$pageFiles->includePhp();

$hash = getStrFromRequest('trainee');
$mod = getStrFromRequest('mod');

echo "<br />the trainee is ".$hash;
echo "<br />the module was ".$mod;

echo "<p>Testing the training app sending results back to Handsam app</p>";
//get usr details
//add a timescale clause to this query so that it only lasts for x mins
//$qry = "select e.con_id, u.firstname, u.lastname, a.accountname, a.school_status, a.accountlogo from ext_web_app_con e left join usr u on e.userid = u.userid left join account a on u.accountid = a.accountid  where  e.unique_str = '$hash' order by con_id desc;";
$qry = "select e.con_id, u.firstname, u.lastname, a.accountname, a.school_status, a.accountlogo from ext_web_app_con e left join usr u on e.userid = u.userid left join account a on u.accountid = a.accountid  where e.state = 'NEW' and e.unique_str = '$hash' and e.module='$mod' order by e.con_id desc limit 0,1";
//echo "<br />the query is ".$qry;
$rs = getRS($con,$qry);
$row = getRow($rs);

if ($row) {
	
	if (isset($installation_logo_url) && is_array($installation_logo_url) && count($installation_logo_url) > 1) {
		if (array_key_exists($row['school_status'], $installation_logo_url)) {
			$mainlogo = $installation_logo_url[$row['school_status']];
		} else {
            $mainlogoarray = array_values($installation_logo_url);
			$mainlogo = array_shift($mainlogoarray);
		}
	} else if (isset($installation_logo_url) && is_array($installation_logo_url)) {
		$mainlogo = array_shift(array_values($installation_logo_url));
	} else if(isset($installation_logo_url)) {
		$mainlogo = $installation_logo_url;
	} else {
		$mainlogo = "";
	}
	

	//var_dump($installation_logo_url[0]);
	if ($row['accountlogo'] != "") {
		$accountlogo = "$rootUrl/account_uploads/account_logo/{$row['accountlogo']}";
	} else {
		$accountlogo = "";
	}
	
	//now update the record  state to CHECKED
		
//	$qry = new dbUpdate('ext_web_app_con');
//	$qry->setParam("con_id",$row['con_id']);
//	if ($online_course_mode == "LIVE") $qry->setStringVal("state","CHECKED","State");
//	$qry->addValue("checked","NOW()");
//	//echo $qry->getSQL();
	//if ($qry->execute($con)) {
		$json = array("firstname" => $row['firstname'], "lastname" => $row['lastname'], "account" => $row['accountname'], "mainlogo" => $mainlogo, "accountlogo" => $accountlogo, "installation" => $installation);
		echo json_encode($json);
	//}
echo "<br />found the record";
//now we're going to set some post variables to post to the testresult page

$_POST['name'] = '30d7674b833798039229bb0c15877a21';
$_POST['module'] = 'fire';
$_POST['pass'] = 1;
$_POST['score'] = 17;
$_POST['max_score'] = 17;
$_POST['certificate'] = 'http://handsam.etrainingcentre.org/certificate/30d7674b833798039229bb0c15877a21/fire';
//




echo var_dumP($_POST);
//NOW SEND TO THE TEST RESULT PAGE
//headerlocation("$rootUrl/modules/training/testresult.php");
//)


} else {
	//trigger_error("Access Denied",E_USER_WARNING);
echo "<br />couldn't find that record";
//	//headerLocation("$rootUrl/index.php",false);
}
?>



<form action="//handsam.org.uk/modules/training/testresult.php" method="get">
<input type="text" value="30d7674b833798039229bb0c15877a21" name="name" readonly />
<input type="text" value="fire" name="module" readonly/>
<input type="text" value="1" name="pass" readonly>
<input type="text" value="17" name="score" readonly/>
<input type="text" value="17" name="max_score" readonly/>
<input type="text" value="http://handsam.etrainingcentre.org/certificate/30d7674b833798039229bb0c15877a21/fire" name="certificate" readonly/>
<input type="submit" value="Click this button to update your training record" />
</form>