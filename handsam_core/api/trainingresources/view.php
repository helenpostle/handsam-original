<?php

/**
 * Supply Links and Files for a training course, via API
 *
 * returns JSON
 */

#### make this a secure page
$secure = false;
require("../../../shared/startPage.php");

$pageFiles->includePhp();

$hash = getStrFromRequest('trainee');
$mod = getStrFromRequest('mod');

if($hash == '' && $mod == '') die('no get');

// this query can only return files if the hash and mod are genuine
$sql = "SELECT DISTINCT f.filename, f.title, f.fileid FROM ext_web_app_con AS e
		JOIN training_log AS tl
			ON e.userid = tl.trainee_id
		JOIN training_course tc
			on tl.course_id = tc.course_id
		JOIN systemcourse_files AS scf
			ON tc.course_id = scf.training_id
		LEFT JOIN files AS f
			ON scf.fileid = f.fileid
		WHERE e.unique_str = '$hash' AND tc.module = '$mod';";

		
$rs = getRS($con,$sql);

$files = array();
while($row = getRow($rs))
{
	$files[] = array('url' => $rootUrl.'/modules/training/resources.php?fileid='.$row['fileid'].'&trainee='.$hash.'&mod='.$mod,
					 'id' => $row['fileid'],
					 'filename' => $row['filename'],
					 'title' => $row['title']
					 );
}

$sql = "SELECT DISTINCT l.url, l.title FROM ext_web_app_con AS e
		JOIN training_log AS tl
			ON e.userid = tl.trainee_id
		JOIN training_course tc
			on tl.course_id = tc.course_id
		JOIN systemcourse_links AS scl
			ON tc.course_id = scl.training_id
		LEFT JOIN links AS l
			ON scl.linkid = l.linkid
		WHERE e.unique_str = '$hash' AND tc.module = '$mod';";
		
$rs = getRS($con,$sql);

$links = array();
while($row = getRow($rs))
{
	$links[] = array('url' => $row['url'], 'title' => $row['title']);
}
header('Content-type: application/json');
echo json_encode(array( 'files' => $files, 'links' => $links ));
