<?php
/***
** Shows the home page content items
** 
** 
***/
if($unread == true):
?>
<script>
$(function(){
    $('a.markasread').button().click(function(e){
        e.preventDefault();            
        var elm = $(this);        
        var contentid = elm.attr('id').split('-')[1];

        $.get(siteRoot+'/handsam_core/functionality/news_check/ajax/news_check.php',
            {
                obj : [ { contentid : contentid } ]
            },
            function(){
                elm.html('Marked as read');
            }
        );            
    });
});
</script>
<?php
endif;

$item_count = getResultRows($rsContent);
if ($item_count > 1) 
{
    while ($rowContent = getRow($rsContent)) {
        echo "<div class=\"item\">";
        echo displayTitle2($rowContent['title']);
        echo displayPosted($rowContent['created'], "");
        if ($rowContent['mid'] == null) echo displayEdit($rowContent);
        echo displayText($rowContent['contenttext']);
        echo "</div>";
    }
} 
else if ($item_count == 1) 
{
    $rowContent = getRow($rsContent);
    
    echo "<div class=\"item\">";
    echo displayPosted($rowContent['created'], "");
    if ($rowContent['mid'] == null) echo displayEdit($rowContent);
    echo displayText($rowContent['contenttext']);
    // has the user read this story?
    if($unread == true) echo '<p><a href="#" class="markasread" id="story-'.$contentid.'">Mark this story as Read</a></p>';
    echo "</div>";
} 
else 
{
    echo "<div class=\"item\">";
    echo displayParagraphs("There are no news entries to view at the moment.");
    echo "</div>";	
}
