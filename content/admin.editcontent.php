<?php 
/**
 * Shows a form for updating a user
 */
if (isAllowed("editContent")) {
	//include("js/fckeditor/fckeditor.php") ;
	if (isset($contentid)) { 
		$state = array("UNPUBLISHED" => "UNPUBLISHED", "PUBLISHED" => "PUBLISHED");
		$homepage = array("NONE" => "Don't display", "TITLE" => "Display title", "SUMMARY" => "Display summary text", "FULLTEXT" => "Display full text");
		echo startFormTable($_SERVER["PHP_SELF"]);
		echo frmLegend('Edit Content');
		echo formNotes(STATIC_CONTENT_FORM_TITLE, STATIC_CONTENT_FORM_INFO);
		echo frmHiddenField($contentid,"id");
		echo frmHiddenField($page,"page");
		if ($contentid == 0) {
			echo frmHiddenField($parentid,"parentid");
		} else {
			echo frmHiddenFieldDb($rowContent,"parentid");
		}
		echo frmTextField($rowContent,"title",100,"Title",true);
		if ($rowContent['superpage'] == "") {
			if ($contentid == 0) {
				echo frmSelectArrayValue("PUBLISHED", "state" , $state, "State" , true);
			} else {
				echo frmSelectArray($rowContent, "state" , $state, "State" , true);
			}
		} else {
			echo frmHiddenField("PUBLISHED","state");
		}
		
		frmEditor($rowContent,"contenttext",20,"Content",true,$fckWidth);
				
		echo frmTextField($rowContent,"meta_title",100,"Meta Title",false);
		echo frmTextArea($rowContent,"meta_keywords",4,"Meta Keywords",false);
		echo frmTextArea($rowContent,"meta_desc",4,"Meta Description",false);
			
		
		
		
		//echo frmTextArea($rowContent,"summary",20,"Summary Text",true,"fck");
		//echo frmTextArea($rowContent,"text",20,"Content",true,"fck");
		
		if ($contentid != 0) {
			echo frmShowAudit($rowContent, $con);	
		}?>
		
		
	<?php
		echo frmButtonHelp("Save","save", "Click here to save changes");
		echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
		if ($contentid != 0 && $rowContent['superpage'] == "") {
			echo frmButtonConfirmHelp("Delete","delete","Are you sure you want to delete this item?", "Click here to delete this item");
		}
		
		echo endFormTable();
	} 
}
?>