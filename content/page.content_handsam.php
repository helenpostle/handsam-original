<?php
/***
** Shows the home page content items
** 
** 
***/
while ($rowContent = getRow($rsContent)) {
	echo "<div class=\"item\">";
	if (isAllowed("handsamOnly")) echo displayEdit($rowContent);
	echo displayText($rowContent['contenttext']);
	echo "</div>";
} 
?>
