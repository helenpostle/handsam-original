<?php 
/**
 * Shows a form for editing a task category
 */
	echo startFormTable($_SERVER["PHP_SELF"]);
	echo formNotes(MESSAGE_FORM_TITLE, MESSAGE_FORM_INFO);
	echo frmHiddenField($return,"return");
	echo frmTextFieldNonDb("subject",30,"Subject",true, $subject);

	echo frmTextArea("","messagetext",5,"Message",true);
	
	echo frmButtonHelp("Send","send", "Send to the HandSaM Administrator");
	echo frmButtonHelp("Cancel","cancel", "Click here to cancel");
	
	echo endFormTable();
?>


