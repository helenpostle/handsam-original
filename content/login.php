<?php
/*
 * login page
 */
  
//need to get all query string params and turn them into hidde fields 
$hiddenFld = "";

foreach ($_GET as $key => $val) {
	$clean_val = strip_tags(getStrFromRequest($key));
	$hiddenFld .= "<input type=\"hidden\" name=\"$key\" id=\"$key\"  value=\"$clean_val\"/>\n";
}

$hiddenFld = $allow_html_purifier->purify($hiddenFld);	
$username = "";
$autocomplete = 'off';	
$checked = false;
if (isset($core_conf['remember_me']) && $core_conf['remember_me'] === true)
{
	if(isset($_COOKIE['cooktoken']))
	{
		//get username
		$qry = "select username from usr where token = '".escapeString($_COOKIE['cooktoken'])."'";
		$rs = getRS($con, $qry);
		$row = getRow($rs);
		$username = $row['username'];
		$checked = true;
	}
}
	
?>
<p class="info">If you cannot remember your user name please email Handsam Client Support at <a href="mailto:info@handsam.co.uk">info@handsam.co.uk</a></p>
<p><a href="<?php echo $rootUrl;?>/forgot.php"> Click here if you have forgotten your password </a></p>

<form method="post" action="<?php echo($_SESSION["cameFrom"])?>" name="loginform" autocomplete="<?php echo $autocomplete;?>" id="loginform">
	<fieldset>
			<div class="required">
				<label for="f_login">Username</label>
				<input autocomplete="<?php echo $autocomplete;?>" title="username" class="inputText"  id="f_login" type="text" name="f_login" value="<?php echo $username;?>"/>
			</div>
			<div class="required"> 
				<label for="f_password" >Password</label>
				<input autocomplete="<?php echo $autocomplete;?>"  title="password" class="inputText" type="password" name="f_password"/>
			</div>
			<?php
			//this isn't what they wanted! Bt they may ask later!
			
				if (isset($core_conf['remember_me']) && $core_conf['remember_me'] === true)
				{
				?>
				<div class="optional clearfix">
				<label for="rememberme">Remember me</label>
				<input title="remember me" <?php if($checked===true) echo "checked='checked'";?>class="inputCheckbox" type="checkbox" name="rememberme" value="yes"/>
				<div style="width: 270px; float: right;margin: -40px 20px 0px 0px;">Do not tick if you are using a shared computer. By ticking this box you accept that a cookie token will be stored by your browser.</div>
				</div>
				<?php
				}
				
				?>
			<div class="optional clearfix" style="margin-bottom: 5px;">
				<label for="submit"></label>
				<input title="submit"  class="inputSubmit" type="submit" name="submit" value="log in"/>
				<div style="width: 340px; float: right;margin: -22px 0px 0px 0px;">
				<a class="page formM" href="<?php echo $rootUrl;?>/forgot.php" title="Have you forgotten your password?">Forgotten your password?</a>
				</div>
			</div>
			
			<?php echo $hiddenFld;?>

	</fieldset>
</form>

<script language="JavaScript" type="text/JavaScript">
document.forms[0].f_login.focus();
</script>

