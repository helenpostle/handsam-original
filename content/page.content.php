<?php
/***
** Shows the home page content items
** 
** 
***/
if (isset($index_modules_content) && is_array($index_modules_content)) {
	foreach ($index_modules_content as $mod_content) {
		echo $mod_content;
	}
}

while ($rowContent = getRow($rsContent)) {
    echo displayTitle2($rowContent['title']);
	echo "<div class=\"item\">";
	echo displayEdit($rowContent);
	echo displayText($rowContent['contenttext']);
	echo "</div>";
} 
?>
