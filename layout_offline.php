<?php 
/**
* Layout Page
* 
* This is a complete HTML Page which sets out the page structure
* Other files are included at various points to set the actual content
*/
#if $this_page not set in  main page code set as empty - used for active menu highlight
if (!isset($this_page)) {
	$this_page = "";
}

#sets content div to normal or wide depending on page 
if (!isset($contentId)) {
	$contentId = "content";
}

$metakey = "";
$metadesc = "";

//Include the box layout page too
include("boxes/boxlayout.php");

//add all system wide css files:

$pageFiles->addSystemCss("system_1.0.css");
$pageFiles->addSystemCss("tables.content.css");
$pageFiles->addSystemCss("tables.box.css");

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">

<head>

<?php include("installations/$installation/layout/head.php"); ?>
	<meta http-equiv="Content-Type" content="text/html; charset=iso-8859-1"/>
	<meta http-equiv="Cache-Control" content="no-store,no-cache"/>
	<?php
	//include css files
	if (isset($pageFiles)) $pageFiles->includeCss();

	?>
        
	<?php
	$userAgent = "";
	if (isset($_SERVER['HTTP_USER_AGENT'])) $userAgent = $_SERVER['HTTP_USER_AGENT'];
	$os = "Mac OS X";
	$version = "";
	$osTest = strpos($userAgent, $os);
	if ($osTest) {
		?>
		<link rel="stylesheet" href="<?php echo $rootUrl;?>/css/mac.css?v=<?php echo $css_version;?>" type="text/css" />
		<?php
	}
	?>
                
</head>

<body class="">
<?php
include("installations/$installation/layout/branding.php");
?>

<div id="wrapper" class="clearfix">
	
	
     	
    <div id="navigation"> 	
   

	</div>
	<?php
	if (($this_page == "terms.php" || $this_page == "about.php") && !((isset($box) && is_array($box) && count($box) > 0) || (isset($pageFiles) && is_array($pageFiles->box) && count($pageFiles->box) > 0))) {
		$contentId = "contentWide";
	}
	?>

	
	<div id="content_wrapper" class="clearfix">
    	<div id="<?php echo $contentId;?>">
            <h1>The system is currently offline.</h1>
            <div class="item">
                <p>We are carrying out some essential maintenance on the system so it is currently ofline. </p><p>We are sorry for any inconvenience caused by this. The system will be back online soon.</p>
        
            </div>
   		</div>
 
 		<div id="boxes">
			
	    </div>

	</div>   
</div>
<div id="footer" class="clearfix">
<?php include("installations/$installation/layout/footer.php"); ?>
</div>
</body>
</html>