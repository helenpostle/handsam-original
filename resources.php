<?php
/**
* Standard Index Page
*/
$this_page = "resources.php";
$content = "";
//include basic stuff
//make this a secure page
$secure = false;
include ("shared/startPage.php");

$pageFiles->addFunc('content');
//add tasklist file browser functions for admin 
if (isAllowed("editContent")) $pageFiles->addFunc('handsam_uploads');

$pageFiles->addFunc('ua-parser');

$pageFiles->includePhp();

//specify page content
	$qry = "SELECT content.*, usr.username FROM content inner join usr on content.created_by = usr.userid where content.state = 'PUBLISHED'  and superpage = 'resources' order by content.created desc";
	$rsContent = getRS($con, $qry);
	$rowContent = getRow($rsContent);
	if ($rowContent['meta_title']) {
		$meta_title = displayText($rowContent['meta_title']);
	}
	if ($rowContent['meta_keywords']) {
		$meta_keywords = displayText($rowContent['meta_keywords']);
	}
	if ($rowContent['meta_desc']) {
		$meta_description = displayText($rowContent['meta_desc']);
	}
	$fertile = $rowContent['fertile'];
	$contentid = $rowContent['contentid'];
	$superpage = $rowContent['superpage'];
	$title = $rowContent['title']; 

	resetRS($rsContent);
	$content = "page.content.php";
	$box[] = "list.files.php";
	$box[] = "developer_help.php";
//include layout.
include ("layout.php");

?>