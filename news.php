<?php
/**
* Standard Index Page
*/
$this_page = "news.php";
$content = "";
//include basic stuff
//make this a secure page
$secure = false;
include ("shared/startPage.php");

$pageFiles->addFunc('content');
$pageFiles->includePhp();

$contentid = getIntFromRequest("id");
$year = getIntFromRequest("y");
$mon = getIntFromRequest("m");
$feed = getStrFromRequest("feed");
$page_size = 10;
$page_number = 0;

//get new parent page contentid
$qry1 = "select contentid from content where superpage = 'news' ";
$rs1 = getRS($con, $qry1);
$row1 = getRow($rs1);
$blogid = $row1['contentid'];

$unread = false;

if ($contentid == $blogid) $contentid = 0;
//first get the page number from the contentid

if ($contentid > 0) {
    $contentid = checkPage($con, $contentid, "news");

    // display a single blog item 
    $qry = "SELECT content.*, usr.username FROM content left join usr on content.created_by = usr.userid where content.state = 'PUBLISHED'  and contentid = $contentid  and parentid = $blogid order by content.created desc";
    $rsContent = getRS($con, $qry);
    $rowContent = getRow($rsContent);
    if($rowContent['meta_title']) {
        $meta_title = displayText($rowContent['meta_title']);
    }
    if ($rowContent['meta_keywords']) {
            $meta_keywords = displayText($rowContent['meta_keywords']);
    }
    if ($rowContent['meta_desc']) {
            $meta_description = displayText($rowContent['meta_desc']);
    }
    $news_title = $rowContent['title'];
    resetRS($rsContent);
    
    // check if user has marked this story as read
    if (isset($_SESSION['clsUS'])) {
        $sql = "SELECT * FROM news_users WHERE content_id = $contentid AND user_id = {$_SESSION['clsUS']->userid};";
        $res = getRecSet($sql);
        $row = getRowAssoc($res);
        // no record = unread
        $unread = (is_array($row)) ? false : true;
    }	
} else if ($mon > 0 && $year > 0) {
	//display blog items for the month 
	$qry = "SELECT content.*, usr.username FROM content inner join usr on content.created_by = usr.userid where content.state = 'PUBLISHED'  and EXTRACT(MONTH FROM content.created) = $mon and EXTRACT(YEAR FROM content.created) = $year and parentid = $blogid order by content.created desc";
	$rsContent = getRS($con, $qry);
	$meta_title = "Messages";
	$news_title = "Messages";
} else if ($year > 0) {	
	//display blog items for the year	
	$qry = "SELECT content.*, usr.username FROM content inner join usr on content.created_by = usr.userid where content.state = 'PUBLISHED' and EXTRACT(YEAR FROM content.created) = $year and parentid = $blogid order by content.created desc";
	$rsContent = getRS($con, $qry);
	$meta_title = "Messages";
    $news_title = "Messages";
} else {
	//get the most recent blog item(s)
	$qry = "SELECT content.*, usr.username FROM content inner join usr on content.created_by = usr.userid where content.state = 'PUBLISHED' and parentid = $blogid order by content.created desc limit 0,4";
	$rsContent = getRS($con, $qry);
	$meta_title = "Messages";
    $news_title = "Messages";
}	

//if there is no page number use the value from above


#for side nav
$qryBlogList = "SELECT contentid, created, title FROM content  where content.state = 'PUBLISHED'  and parentid = $blogid order by content.created desc";
$rsBlogSideNav = getRS($con, $qryBlogList);

#now we've got the page details, we can get the first item on that page if no contentid


if ($feed == "rss") {
	//$content = "rss.blog.php";
	//$page = 1;
	//include ("layout_rss.php");
} else {
	$content = "page.news.php";
	$superpage = "news";
        $title = $news_title;   
	if (isAllowed("editContent")) {
		$box[] = "box.news_admin.php";
		$box[] = "box.unpublished_news.php";
	}
	#now also add blog side navigation to the left column	
	$box[] = "box.news_navigation.php";
	//$box[] = "box.rss.php";	
	//include layout.
	include ("layout.php");
}

